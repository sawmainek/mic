import { TestBed } from '@angular/core/testing';

import { MicInvestmentBySectorService } from './mic-investment-by-sector.service';

describe('MicInvestmentBySectorService', () => {
  let service: MicInvestmentBySectorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MicInvestmentBySectorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
