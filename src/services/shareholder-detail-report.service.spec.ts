import { TestBed } from '@angular/core/testing';

import { ShareholderDetailReportService } from './shareholder-detail-report.service';

describe('ShareholderDetailReportService', () => {
  let service: ShareholderDetailReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShareholderDetailReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
