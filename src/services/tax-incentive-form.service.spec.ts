import { TestBed } from '@angular/core/testing';

import { TaxIncentiveFormService } from './tax-incentive-form.service';

describe('TaxIncentiveFormService', () => {
  let service: TaxIncentiveFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaxIncentiveFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
