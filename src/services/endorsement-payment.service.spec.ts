import { TestBed } from '@angular/core/testing';

import { EndorsementPaymentService } from './endorsement-payment.service';

describe('EndorsementPaymentService', () => {
  let service: EndorsementPaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementPaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
