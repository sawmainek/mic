import { TestBed } from '@angular/core/testing';

import { EndorsementReplyService } from './endorsement-reply.service';

describe('EndorsementReplyService', () => {
  let service: EndorsementReplyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementReplyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
