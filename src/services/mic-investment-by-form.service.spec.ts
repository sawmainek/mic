import { TestBed } from '@angular/core/testing';

import { MicInvestmentByFormService } from './mic-investment-by-form.service';

describe('MicInvestmentByFormService', () => {
  let service: MicInvestmentByFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MicInvestmentByFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
