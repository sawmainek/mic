import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('summarytotalreport', true)
})
export class SummaryTotalReportService extends BaseService {  }