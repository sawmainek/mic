import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {
  url: string = "http://forex.cbm.gov.mm/api/latest";

  constructor(private http: HttpClient) { }

  get() {
    return this.http.get(this.url);
  }
}



