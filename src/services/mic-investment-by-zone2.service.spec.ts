import { TestBed } from '@angular/core/testing';

import { MicInvestmentByZone2Service } from './mic-investment-by-zone2.service';

describe('MicInvestmentByZone2Service', () => {
  let service: MicInvestmentByZone2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MicInvestmentByZone2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
