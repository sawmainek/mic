import { TestBed } from '@angular/core/testing';

import { MICPaymentService } from './micpayment.service';

describe('MICPaymentService', () => {
  let service: MICPaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MICPaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
