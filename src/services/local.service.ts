import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LocalService {

    constructor(
        private http: HttpClient
    ) { }

    uniqueArray(list: any[], alias: string = 'id') {
        let values = list || [];
        let unique: any = [];
        values.forEach(x => {
            if (x.id && unique.filter(y => y.id == x.id).length == 0) {
                unique.push(x);
            }
        });
        unique.sort((a, b) => (a.name > b.name) ? 1 : -1);
        return unique;
    }

    getLandType(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/land_type.json').pipe(
            map(result => {
                let land_type = [];
                result.forEach(res => {
                    land_type.push({
                        id: res.name,
                        name: lang == 'mm' ? res.name_mm : res.name
                    })
                });
                return this.uniqueArray(land_type);
            })
        )
    }

    getUnit(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/unit.json').pipe(
            map(result => {
                let unit = [];
                result.forEach(res => {
                    unit.push({
                        id: res.name,
                        name: lang == 'mm' ? res.name_mm : res.name
                    })
                });
                return this.uniqueArray(unit);
            })
        )
    }

    getOrigin(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/origin.json').pipe(
            map(result => {
                let origin = [];
                result.forEach(res => {
                    origin.push({
                        id: res.name,
                        name: lang == 'mm' ? res.name_mm : res.name
                    })
                });
                return this.uniqueArray(origin);
            })
        )
    }

    getType(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/type.json').pipe(
            map(result => {
                let type = [];
                result.forEach(res => {
                    type.push({
                        id: res.name,
                        name: lang == 'mm' ? res.name_mm : res.name
                    })
                });
                return this.uniqueArray(type);
            })
        )
    }

    getType1(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/type_1.json').pipe(
            map(result => {
                let type = [];
                result.forEach(res => {
                    type.push({
                        id: res.name,
                        name: lang == 'mm' ? res.name_mm : res.name
                    })
                });
                return this.uniqueArray(type);
            })
        )
    }

    getCompany(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/company_type.json').pipe(
            map(result => {
                let company = [];
                result.forEach(res => {
                    company.push({
                        id: res.name,
                        name: lang == 'mm' ? res.name_mm : res.name
                    })
                });
                return this.uniqueArray(company);
            })
        )
    }

    getClassification(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/employment_classification.json').pipe(
            map(result => {
                let classification = [];
                result.forEach(res => {
                    classification.push({
                        id: res.name,
                        name: res.name
                    })
                });
                return this.uniqueArray(classification);
            })
        )
    }

    getContact(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/contact.json').pipe(
            map(result => {
                let contact = [];
                result.forEach(res => {
                    contact.push({
                        id: res.id,
                        name: res.name,
                        branch: lang == 'mm' ? res.branch_mm : res.branch,
                        address: lang == 'mm' ? res.address_mm : res.address,
                        phone: lang == 'mm' ? res.phone_mm : res.phone
                    })
                });
                return this.uniqueArray(contact);
            })
        )
    }

    getDate(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/date.json').pipe(
            map(result => {
                let unit = [];
                result.forEach(res => {
                    unit.push({
                        id: res.name,
                        name: lang == 'mm' ? res.name_mm : res.name
                    })
                });
                return this.uniqueArray(unit);
            })
        )
    }
}
