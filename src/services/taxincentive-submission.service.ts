import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('submissiontaxincentive', true)
})
export class TaxincentiveSubmissionService extends BaseService {

}

