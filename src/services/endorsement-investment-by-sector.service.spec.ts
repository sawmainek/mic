import { TestBed } from '@angular/core/testing';

import { EndorsementInvestmentBySectorService } from './endorsement-investment-by-sector.service';

describe('EndorsementInvestmentBySectorService', () => {
  let service: EndorsementInvestmentBySectorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementInvestmentBySectorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
