import { TestBed } from '@angular/core/testing';

import { EndorsementMessageService } from './endorsement-message.service';

describe('EndorsementMessageService', () => {
  let service: EndorsementMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
