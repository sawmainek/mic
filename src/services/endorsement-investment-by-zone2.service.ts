import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('endorsementinvestmentbyzone2', true)
})
export class EndorsementInvestmentByZone2Service extends BaseService {
}