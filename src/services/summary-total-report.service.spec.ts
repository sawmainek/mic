import { TestBed } from '@angular/core/testing';

import { SummaryTotalReportService } from './summary-total-report.service';

describe('SummaryTotalReportService', () => {
  let service: SummaryTotalReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SummaryTotalReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
