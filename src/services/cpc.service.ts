import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CPCService {

    constructor(
        private http: HttpClient
    ) { }

    getSection(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/cpc.json').pipe(
            map(result => {
                let section = [];
                result.forEach(res => {
                    section.push({
                        id: res.section,
                        name: res.section,
                        investment: res.investment
                    })
                });
                return section;
            })
        )
    }

    getDivison(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/cpc.json').pipe(
            map(result => {
                let division = [];
                result.forEach(res => {
                    division.push({
                        id: res.division,
                        name: res.division,
                        section: res.section
                    })
                });
                return division;
            })
        )
    }

    getGroup(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/cpc.json').pipe(
            map(result => {
                let group = [];
                result.forEach(res => {
                    group.push({
                        id: res.group,
                        name: res.group,
                        division: res.division
                    })
                });
                return group;
            })
        )
    }

    getClasses(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/cpc.json').pipe(
            map(result => {
                let classes = [];
                result.forEach(res => {
                    classes.push({
                        id: res.class,
                        name: res.class,
                        group: res.group
                    })
                });
                return classes;
            })
        )
    }

    getSubclasses(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/cpc.json').pipe(
            map(result => {
                let subclasses = [];
                result.forEach(res => {
                    subclasses.push({
                        id: res.subclass,
                        name: res.subclass,
                        class: res.class
                    })
                });
                return subclasses;
            })
        )
    }

}
