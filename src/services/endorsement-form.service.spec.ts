import { TestBed } from '@angular/core/testing';

import { EndorsementFormService } from './endorsement-form.service';

describe('EndorsementFormService', () => {
  let service: EndorsementFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
