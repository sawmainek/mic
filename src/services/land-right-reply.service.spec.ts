import { TestBed } from '@angular/core/testing';

import { LandRightReplyService } from './land-right-reply.service';

describe('LandRightReplyService', () => {
  let service: LandRightReplyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LandRightReplyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
