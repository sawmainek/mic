import { TestBed } from '@angular/core/testing';

import { MICFormService } from './micform.service';

describe('MICFormService', () => {
  let service: MICFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MICFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
