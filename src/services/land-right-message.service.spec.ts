import { TestBed } from '@angular/core/testing';

import { LandRightMessageService } from './land-right-message.service';

describe('LandRightMessageService', () => {
  let service: LandRightMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LandRightMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
