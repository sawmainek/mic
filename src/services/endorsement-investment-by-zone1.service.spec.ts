import { TestBed } from '@angular/core/testing';

import { EndorsementInvestmentByZone1Service } from './endorsement-investment-by-zone1.service';

describe('EndorsementInvestmentByZone1Service', () => {
  let service: EndorsementInvestmentByZone1Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementInvestmentByZone1Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
