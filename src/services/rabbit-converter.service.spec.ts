import { TestBed } from '@angular/core/testing';

import { RabbitConverterService } from './rabbit-converter.service';

describe('RabbitConverterService', () => {
  let service: RabbitConverterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RabbitConverterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
