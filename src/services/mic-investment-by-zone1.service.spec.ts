import { TestBed } from '@angular/core/testing';

import { MicInvestmentByZone1Service } from './mic-investment-by-zone1.service';

describe('MicInvestmentByZone1Service', () => {
  let service: MicInvestmentByZone1Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MicInvestmentByZone1Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
