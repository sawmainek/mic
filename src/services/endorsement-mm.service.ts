import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('endorsementmm', true)
})
export class EndorsementMMService extends BaseService {

}

