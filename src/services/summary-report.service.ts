import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('summaryreport', true)
})
export class SummaryReportService extends BaseService {  }

