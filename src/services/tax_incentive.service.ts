import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('taxincentive', true)
})
export class TaxIncentiveService extends BaseService {

}
