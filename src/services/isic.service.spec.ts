import { TestBed } from '@angular/core/testing';

import { ISICService } from './isic.service';

describe('ISICService', () => {
  let service: ISICService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ISICService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
