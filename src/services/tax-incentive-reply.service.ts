import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('tax-incentive-reply', true)
})
export class TaxIncentiveReplyService extends BaseService {

}
