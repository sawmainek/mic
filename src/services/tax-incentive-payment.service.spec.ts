import { TestBed } from '@angular/core/testing';

import { TaxIncentivePaymentService } from './tax-incentive-payment.service';

describe('TaxIncentivePaymentService', () => {
  let service: TaxIncentivePaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaxIncentivePaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
