import { TestBed } from '@angular/core/testing';

import { MicReportService } from './mic-report.service';

describe('MicReportService', () => {
  let service: MicReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MicReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
