import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('shareholderdetailreport', true)
})
export class ShareholderDetailReportService extends BaseService {

}
