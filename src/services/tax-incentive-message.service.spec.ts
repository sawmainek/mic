import { TestBed } from '@angular/core/testing';

import { TaxIncentiveMessageService } from './tax-incentive-message.service';

describe('TaxIncentiveMessageService', () => {
  let service: TaxIncentiveMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaxIncentiveMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
