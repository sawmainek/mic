import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('landrightinvoice', true)
})
export class LandRightPaymentService extends BaseService {

}
