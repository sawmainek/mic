import { TestBed } from '@angular/core/testing';

import { TaxIncentiveReplyService } from './tax-incentive-reply.service';

describe('TaxIncentiveReplyService', () => {
  let service: TaxIncentiveReplyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaxIncentiveReplyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
