import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { InjectorInstance } from 'src/app/app.module';
import { TokenHash } from 'src/app/core/token-hash';

export function BaseServiceFactory(apiName, apiSecure: boolean = false) {
    return new BaseService(InjectorInstance.get<Store<AppState>>(Store), InjectorInstance.get<HttpClient>(HttpClient), apiName, apiSecure);
}
// @Injectable({
//     providedIn: 'root',
//     useValue:{model:''}
// })
export class BaseService {

    token: any;
    verifyPhone: string;
    user: any;
    student: any = {};
    host: string = environment.host;

    constructor(
        private store: Store<AppState>,
        private http: HttpClient,
        private model: string,
        private secure: boolean
    ) {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.getCurrentUser().subscribe(user => {
            this.user = user;
        });
    }

    getFormGroup(fg?: Array<any>): any {
        return of(fg.sort((a, b) => a.order - b.order));
    }

    getCurrentUser(): Observable<any> {
        return this.store
            .pipe(
                select(currentUser),
                map((result: any) => {
                    this.user = result;
                    return result;
                }));
    }

    get(params: any = {}): Observable<any[]> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.secure = params.secure || this.secure || false;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + (this.secure ? (this.user && this.user.personal_access_token || null) : (this.token.access_token || null))
            })
        };
        return this.http.get<any[]>(`${this.host}/api/${this.model}?${params.keyword ? 'keyword=' + params.keyword + '&' : ''}search=${params.search || ''}&sort=${params.sort || 'id'}&order=${params.order || 'desc'}&page=${params.page || 1}&rows=${params.rows || 12}${params.datatable ? '&datatable=' + params.datatable : ''}`, httpOptions);
    }

    show(id: number, params: any = {}): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.secure = params.secure || this.secure || false;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + (this.secure ? (this.user && this.user.personal_access_token || null) : (this.token.access_token || null))
            })
        };
        return this.http.get<any>(`${this.host}/api/${this.model}/${id}`, httpOptions);
    }


    create(data: any = {}, params: any = {}): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.secure = params.secure || this.secure || false;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + (this.secure ? (this.user && this.user.personal_access_token || null) : (this.token.access_token || null))
            })
        };
        return this.http.post<any>(`${this.host}/api/${this.model}`, data, httpOptions);
    }

    import(data: any[] = [], params: any = {}): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.secure = params.secure || this.secure || false;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + (this.secure ? (this.user && this.user.personal_access_token || null) : (this.token.access_token || null))
            })
        };
        return this.http.post<any>(`${this.host}/api/${this.model}/import/bulk`, data, httpOptions);
    }

    update(id: number, data: any = {}, params: any = {}): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.secure = params.secure || this.secure || false;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + (this.secure ? (this.user && this.user.personal_access_token || null) : (this.token.access_token || null))
            })
        };
        return this.http.put<any>(`${this.host}/api/${this.model}/${id}`, data, httpOptions);
    }

    destroy(id: number, params: any = {}): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.secure = params.secure || this.secure || false;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + (this.secure ? (this.user && this.user.personal_access_token || null) : (this.token.access_token || null))
            })
        };
        return this.http.delete<any>(`${this.host}/api/${this.model}/${id}`, httpOptions);
    }

    upload(data: any = {}, params: any = {}): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.secure = params.secure || this.secure || false;
        const httpOptions = {
            headers: new HttpHeaders({
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + (this.secure ? (this.user && this.user.personal_access_token || null) : (this.token.access_token || null))
            })
        };
        return this.http.post<any>(`${this.host}/api/${this.model}`, data, httpOptions);
    }

    download(path: string, params: any = {}): Observable<string> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        this.secure = params.secure || this.secure || false;
        const httpOptions = {
            headers: new HttpHeaders({
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + (this.secure ? (this.user && this.user.personal_access_token || null) : (this.token.access_token || null))
            })
        };
        return this.http.get<string>(`${this.host}/api/files/${path}`, httpOptions);
    }
}
