import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('submissionendorsement', true)
})
export class EndorsementSubmissionService extends BaseService {

}
