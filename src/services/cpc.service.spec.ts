import { TestBed } from '@angular/core/testing';

import { CPCService } from './cpc.service';

describe('CPCService', () => {
  let service: CPCService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CPCService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
