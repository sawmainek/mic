import { TestBed } from '@angular/core/testing';

import { LocationReportService } from './location-report.service';

describe('LocationReportService', () => {
  let service: LocationReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocationReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
