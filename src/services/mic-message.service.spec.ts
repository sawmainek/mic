import { TestBed } from '@angular/core/testing';

import { MicMessageService } from './mic-message.service';

describe('MicMessageService', () => {
  let service: MicMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MicMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
