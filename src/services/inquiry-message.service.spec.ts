import { TestBed } from '@angular/core/testing';

import { InquiryMessageService } from './inquiry-message.service';

describe('InquiryMessageService', () => {
  let service: InquiryMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InquiryMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
