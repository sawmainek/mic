import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('micinvestmentbyzone3', true)
})
export class MicInvestmentByZone3Service extends BaseService {
}