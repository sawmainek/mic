import { TestBed } from '@angular/core/testing';

import { MicInvestmentByZone3Service } from './mic-investment-by-zone3.service';

describe('MicInvestmentByZone3Service', () => {
  let service: MicInvestmentByZone3Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MicInvestmentByZone3Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
