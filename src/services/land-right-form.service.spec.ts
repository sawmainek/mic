import { TestBed } from '@angular/core/testing';

import { LandRightFormService } from './land-right-form.service';

describe('LandRightFormService', () => {
  let service: LandRightFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LandRightFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
