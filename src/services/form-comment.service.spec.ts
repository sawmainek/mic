import { TestBed } from '@angular/core/testing';

import { FormCommentService } from './form-comment.service';

describe('FormCommentService', () => {
  let service: FormCommentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormCommentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
