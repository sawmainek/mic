import { TestBed } from '@angular/core/testing';

import { EndorsementInvestmentByFormService } from './endorsement-investment-by-form.service';

describe('EndorsementInvestmentByFormService', () => {
  let service: EndorsementInvestmentByFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementInvestmentByFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
