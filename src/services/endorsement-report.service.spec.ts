import { TestBed } from '@angular/core/testing';

import { EndorsementReportService } from './endorsement-report.service';

describe('EndorsementReportService', () => {
  let service: EndorsementReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
