import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ISICService {

    constructor(
        private http: HttpClient
    ) { }

    uniqueArray(list: any[], alias: string = 'id') {
        let values = list || [];
        let unique: any = [];
        values.forEach(x => {
            if (x.id && unique.filter(y => y.id == x.id).length == 0) {
                unique.push(x);
            }
        });
        unique.sort((a, b) => (a.id > b.id) ? 1 : -1);
        return unique;
    }

    getISIC() : Observable<any[]> {
        return this.http.get<any[]>('assets/json/isic.json').pipe(
            map(result => {
                return result || [];
            })
        )
    }

    getInvestment(lang: string = 'en'): Observable<any[]> {
        return this.http.get<any[]>('assets/json/isic.json').pipe(
            map(result => {
                let invest = [];
                result.forEach(res => {
                    invest.push({
                        id: res.investment,
                        name: lang == 'mm' ? res.investment_mm : res.investment
                    })
                });
                return invest;
            })
        )
    }

    getSection(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/isic.json').pipe(
            map(result => {
                let section = [];
                result.forEach(res => {
                    section.push({
                        id: res.section,
                        name: res.section,
                        investment: res.investment
                    })
                });
                return section;
            })
        )
    }

    getDivison(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/isic.json').pipe(
            map(result => {
                let division = [];
                result.forEach(res => {
                    division.push({
                        id: res.division,
                        name: res.division,
                        section: res.section
                    })
                });
                return division;
            })
        )
    }

    getGroup(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/isic.json').pipe(
            map(result => {
                let group = [];
                result.forEach(res => {
                    group.push({
                        id: res.group,
                        name: res.group,
                        division: res.division
                    })
                });
                return group;
            })
        )
    }

    getClasses(lang: string = 'en'): Observable<any[]> {
        return this.http.get<any[]>('assets/json/isic.json').pipe(
            map(result => {
                let classes = [];
                result.forEach(res => {
                    if(lang == 'mm') {
                        let code = res.class.split('-')[0] || '';
                        code = code.replace('Class', 'အမျိုးအစား');
                        classes.push({
                            id: res.class,
                            name: `${code}- ${res.class_name_mm}`,
                            group: res.group
                        })
                    } else {
                        classes.push({
                            id: res.class,
                            name: res.class,
                            group: res.group
                        })
                    }
                });
                return classes;
            })
        )
    }

    getSector(lang: string = 'en'): Observable<any[]> {
        return this.http.get<any[]>('assets/json/isic.json').pipe(
            map(result => {
                let classes = [];
                result.forEach(res => {
                    classes.push({
                        id: res.sector,
                        name: res.sector,
                    })
                });
                return classes;
            })
        )
    }

    getSectors(lang: string = 'en'): Observable<any[]> {
        return this.http.get<any[]>('assets/json/isic.json').pipe(
            map(result => {
                let classes = [];
                result.forEach(res => {
                    classes.push({
                        id: res.class,
                        name: res.class,
                        sector: res.sector
                    })
                });
                return classes;
            })
        )
    }
}
