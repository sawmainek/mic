import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('micinvestmentbyzone2', true)
})
export class MicInvestmentByZone2Service extends BaseService {
}