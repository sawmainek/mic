import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory} from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory:()=>BaseServiceFactory('landright', true)
})
export class LandRightService extends BaseService {
  
}
