import { TestBed } from '@angular/core/testing';

import { LandRightPaymentService } from './land-right-payment.service';

describe('LandRightPaymentService', () => {
  let service: LandRightPaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LandRightPaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
