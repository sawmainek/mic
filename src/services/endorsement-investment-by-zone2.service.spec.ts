import { TestBed } from '@angular/core/testing';

import { EndorsementInvestmentByZone2Service } from './endorsement-investment-by-zone2.service';

describe('EndorsementInvestmentByZone2Service', () => {
  let service: EndorsementInvestmentByZone2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementInvestmentByZone2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
