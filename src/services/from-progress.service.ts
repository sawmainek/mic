import { Injectable } from '@angular/core';
import { BaseServiceFactory, BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('formprogress', true)
})
export class FormProgressService extends BaseService {


}
