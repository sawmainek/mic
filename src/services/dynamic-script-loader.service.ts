import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
interface Scripts {
    name: string;
    src: string;
}

export const ScriptStore: Scripts[] = [
    { name: 'jquery', src: '../../../assets/js/vendor/jquery-1.12.0.min.js' },
    { name: 'bootstrap', src: '../../../assets/js/bootstrap.min.js' },
    { name: 'plugins', src: '../../../assets/js/plugins.js' },
    { name: 'slick', src: '../../../assets/js/slick.min.js' },
    { name: 'carousel', src: '../../../assets/js/owl.carousel.min.js' },
    { name: 'waypoints', src: '../../../assets/js/waypoints.min.js' },
    { name: 'officer-bundle', src: '../../../assets/vendors/bundle.js' },
    { name: 'officer-main', src: '../../../assets/js/app.js' },

    { name: 'date-jquery', src: 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js' },
    { name: 'date-bootstrap', src: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js' },
    
];

declare var document: any;
declare var google: any;
declare var $: any;

@Injectable({
    providedIn: 'root'
})
export class DynamicScriptLoaderService {

    scripts: any = {};
    stopPriceSlider$: Subject<any[]> = new Subject<any[]>();
    constructor() {
        ScriptStore.forEach((script: any) => {
            this.scripts[script.name] = {
                loaded: false,
                src: script.src
            };
        });
    }

    load(...scripts: string[]) {
        const promises: any[] = [];
        scripts.forEach((script) => promises.push(this.loadScript(script)));
        return Promise.all(promises);
    }

    loadScript(name: string) {
        return new Promise((resolve, reject) => {
            if (this.scripts[name] && !this.scripts[name].loaded) {
                //load script
                let script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = this.scripts[name].src;
                if (script.readyState) {  //IE
                    script.onreadystatechange = () => {
                        if (script.readyState === "loaded" || script.readyState === "complete") {
                            script.onreadystatechange = null;
                            this.scripts[name].loaded = true;
                            resolve({ script: name, loaded: true, status: 'Loaded' });
                        }
                    };
                } else {  //Others
                    script.onload = () => {
                        this.scripts[name].loaded = true;
                        resolve({ script: name, loaded: true, status: 'Loaded' });
                    };
                }
                script.onerror = (error: any) => resolve({ script: name, loaded: false, status: 'Loaded' });
                document.getElementsByTagName('head')[0].appendChild(script);
            } else {
                resolve({ script: name, loaded: true, status: 'Already Loaded' });
            }
        });
    }

    homeScripts() {

        if ($('.slider__activation__wrap').length) {
            $('.slider__activation__wrap').owlCarousel({
                loop: true,
                margin: 0,
                nav: true,
                autoplay: false,
                navText: ['<i class="zmdi zmdi-chevron-left"></i>', '<i class="zmdi zmdi-chevron-right"></i>'],
                autoplayTimeout: 10000,
                items: 1,
                dots: false,
                lazyLoad: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    1920: {
                        items: 1
                    }
                }
            });
        }

        if ($('.slider__activation__02').length) {
            $('.slider__activation__02').owlCarousel({
                loop: true,
                margin: 0,
                nav: false,
                autoplay: false,
                autoplayTimeout: 10000,
                items: 1,
                dots: false,
                lazyLoad: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    1920: {
                        items: 1
                    }
                }
            });
        }
        $('.popular__product__wrap').owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            autoplay: false,
            navText: ['<i class="zmdi zmdi-chevron-left"></i>', '<i class="zmdi zmdi-chevron-right"></i>'],
            autoplayTimeout: 10000,
            items: 3,
            dots: false,
            lazyLoad: true,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                },
                800: {
                    items: 3
                },
                1024: {
                    items: 4
                },
                1200: {
                    items: 4
                },
                1400: {
                    items: 4
                },
                1920: {
                    items: 4
                }
            }
        });

        $('.htc__product__container').imagesLoaded(function () {

            // filter items on button click
            $('.product__menu').on('click', 'button', function () {
                var filterValue = $(this).attr('data-filter');
                $grid.isotope({ filter: filterValue });
            });
            // init Isotope
            var $grid = $('.product__list').isotope({
                itemSelector: '.single__pro',
                percentPosition: true,
                transitionDuration: '0.7s',
                layoutMode: 'fitRows',
                masonry: {
                    // use outer width of grid-sizer for columnWidth
                    columnWidth: 1,
                }
            });
            $grid.isotope({ filter: '.cat--1' });

        });
    }

    productScripts() {
        $("#slider-range").slider({
            range: true,
            min: 1000,
            max: 9999999,
            step: 5000,
            values: [50000, 9999999],
            slide:  (event, ui) => {
                $("#amount").val("MMK " + ui.values[0] + " - MMK" + ui.values[1]);
            },
            stop: (event, ui) => {
                this.stopPriceSlider$.next(ui.values);
            }
        });
        $("#amount").val("MMK " + $("#slider-range").slider("values", 0) +
            " - MMK " + $("#slider-range").slider("values", 1));
    }

    detailScripts() {
        $('.related__product__wrap').owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            autoplay: false,
            navText: ['<i class="zmdi zmdi-chevron-left"></i>', '<i class="zmdi zmdi-chevron-right"></i>'],
            autoplayTimeout: 10000,
            items: 3,
            dots: false,
            lazyLoad: true,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                },
                800: {
                    items: 3
                },
                1024: {
                    items: 4
                },
                1200: {
                    items: 4
                },
                1400: {
                    items: 4
                },
                1920: {
                    items: 4
                }
            }
        });
    }

    aboutScripts() {
        $('.testimonial__wrap').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            autoplay: false,
            navText: false,
            autoplayTimeout: 10000,
            items: 1,
            dots: false,
            lazyLoad: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                800: {
                    items: 1
                },
                1024: {
                    items: 1
                },
                1200: {
                    items: 1
                },
                1400: {
                    items: 1
                },
                1920: {
                    items: 1
                }
            }
        });
    }

    contactScripts() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 12,

            scrollwheel: false,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(23.7286, 90.3854), // New York

            // How you would like to style the map. 
            // This is where you would paste any style found on Snazzy Maps.
            styles: [


                {
                    "featureType": "administrative",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#888888"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "color": "#6e6e6e"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "administrative.country",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#6f6b6b"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "color": "#c5c5c5"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#cfcfcf"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.landcover",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.landcover",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        },
                        {
                            "color": "#909090"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.medical",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e5e5e5"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e5e5e5"
                        }
                    ]
                },
                {
                    "featureType": "poi.place_of_worship",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "poi.sports_complex",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f7f7f7"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "gamma": 7.18
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "gamma": 0.48
                        }
                    ]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#bcbcbc"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                }
            ]
        };

        // Get the HTML DOM element that will contain your map 
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('googleMap');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(23.7286, 90.3854),
            map: map,
            title: 'Tasfiu!',
            icon: 'images/icons/map.png',
            animation: google.maps.Animation.BOUNCE

        });
    }
}
