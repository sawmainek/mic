import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('endorsementinvoice', true)
})
export class EndorsementPaymentService extends BaseService {

}
