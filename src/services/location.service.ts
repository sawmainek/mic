import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LocationService {

    constructor(
        private http: HttpClient,
        private langService: TranslateService
    ) { }

    uniqueArray(list: any[], alias: string = 'id') {
        let values = list || [];
        let unique: any = [];
        values.forEach(x => {
            if (x.id && unique.filter(y => y.id == x.id).length == 0) {
                unique.push(x);
            }
        });
        unique.sort((a, b) => (a.name > b.name) ? 1 : -1);
        return unique;
    }

    getCountry(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/countries.json').pipe(
            map(result => {
                let location = [];
                result.forEach(res => {
                    location.push({
                        id: res.name,
                        name: res.name
                    })
                });
                return this.uniqueArray(location);
            })
        )
    }

    getCurrency(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/countries.json').pipe(
            map(result => {
                let location = [];
                result.forEach(res => {
                    location.push({
                        id: `${res.name} (${res.currency_code})`,
                        name: `${res.name} (${res.currency_code})`
                    })
                });
                return this.uniqueArray(location);
            })
        )
    }

    getZone(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/location.json').pipe(
            map(result => {
                let location = [];
                result.forEach(res => {
                    location.push({
                        id: res.zone,
                        name: res.zone
                    })
                });
                return this.uniqueArray(location);
            })
        )
    }

    getState(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/location.json').pipe(
            map(result => {
                let location = [];
                result.forEach(res => {
                    location.push({
                        id: res.state,
                        name: lang == 'mm' ? res.state_mm : res.state,
                        zone: res.zone,
                    })
                });
                // Don't add unique array becuase of zone
                return location;
            })
        )
    }

    getDistrict(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/location.json').pipe(
            map(result => {
                let location = [];
                result.forEach(res => {
                    location.push({
                        id: res.district,
                        name: lang == 'mm' ? res.district_mm : res.district,
                        state: res.state,
                    })
                });
                return this.uniqueArray(location);
            })
        )
    }

    getTownship(lang: string = 'en'): Observable<any[]> {
        if (window.location.href.indexOf('/mm') != -1) {
            lang = 'mm';
        }
        return this.http.get<any[]>('assets/json/location.json').pipe(
            map(result => {
                let location = [];
                result.forEach(res => {
                    location.push({
                        id: res.township,
                        name: lang == 'mm' ? res.township_mm : res.township,
                        district: res.district,
                    })
                });
                return this.uniqueArray(location);
            })
        )
    }

    getStateRegion(): Observable<any[]> {
        return this.http.get<any[]>('assets/json/countries+states.json').pipe(
            map(result => {
                let location = [];
                result.forEach(country => {
                    if(country.states) {
                        country.states.forEach(state => {
                            location.push({
                                id: state.name,
                                name: state.name,
                                country: country.name,
                            })
                        });
                    }
                });
                return this.uniqueArray(location);
            })
        )
    }
}
