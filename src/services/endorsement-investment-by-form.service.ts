import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('endorsementinvestmentbyform', true)
})
export class EndorsementInvestmentByFormService extends BaseService{
}
