import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('inquiry-reply', true)
})
export class InquiryReplyService extends BaseService {

}
