import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  host: string;
  isLogin: boolean = false;
  isLogin$: Subject<any> = new Subject<any>();
  changeLanguage$: Subject<string> = new Subject<string>();

  constructor(
    private http: HttpClient,
    private store: Store<AppState>,
  ) {
    this.host = environment.host;
  }

  getCurrentUser(): Observable<any> {
    return this.store
      .pipe(
        select(currentUser),
        map((result: any) => {
          return result;
        }));
  }

  getAccessToken(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const data = {
      'grant_type': 'client_credentials',
      'client_id': environment.client_id,
      'client_secret': environment.client_secret,
    };
    return this.http.post<any>(this.host + '/oauth/token', data, httpOptions);
  }

  logout() {
    localStorage.removeItem(environment.loginUserKey);
    localStorage.removeItem(environment.isVertifyKey);
    localStorage.removeItem(environment.tokenKey);
    window.location.href = '/';
  }
}
