import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('endorsementinvvaluebysector', true)
})
export class EndorsementInvestmentBySectorService extends BaseService {
}
