import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('micinvestmentvaluebysector', true)
})
export class MicInvestmentBySectorService extends BaseService {
}