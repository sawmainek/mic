import { TestBed } from '@angular/core/testing';

import { InquiryPaymentService } from './inquiry-payment.service';

describe('InquiryPaymentService', () => {
  let service: InquiryPaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InquiryPaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
