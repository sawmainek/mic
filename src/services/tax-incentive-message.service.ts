import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('tax-incentive-message', true)
})
export class TaxIncentiveMessageService extends BaseService {

}
