import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory} from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory:()=>BaseServiceFactory('landrightmm', true)
})
export class LandRightMMService extends BaseService {
  
}
