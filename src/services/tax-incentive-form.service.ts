import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('taxincentiveform', true)
})
export class TaxIncentiveFormService extends BaseService {

}