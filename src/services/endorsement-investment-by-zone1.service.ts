import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('endorsementinvestmentbyzone1', true)
})
export class EndorsementInvestmentByZone1Service extends BaseService {
}
