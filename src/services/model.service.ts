import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModelService {
  public model: any;

  constructor() { }

  setModel(model: any){
    this.model = model;
  }

  getModel(){
    return this.model;
  }
}
