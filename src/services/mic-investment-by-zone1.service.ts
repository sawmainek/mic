import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('micinvestmentbyzone1', true)
})
export class MicInvestmentByZone1Service extends BaseService {
}