import { TestBed } from '@angular/core/testing';

import { EndorsementInvestmentByZone3Service } from './endorsement-investment-by-zone3.service';

describe('EndorsementInvestmentByZone3Service', () => {
  let service: EndorsementInvestmentByZone3Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndorsementInvestmentByZone3Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
