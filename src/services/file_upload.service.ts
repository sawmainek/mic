import { Injectable} from '@angular/core';
import { BaseService, BaseServiceFactory} from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory:()=>BaseServiceFactory('fileupload', true)
})
export class FileUploadService extends BaseService {
}
