import { TestBed } from '@angular/core/testing';

import { MICReplyService } from './micreply.service';

describe('MICReplyService', () => {
  let service: MICReplyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MICReplyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
