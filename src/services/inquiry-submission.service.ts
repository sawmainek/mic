import { Injectable } from '@angular/core';
import { BaseServiceFactory, BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('submissioninquiry', true)
})
export class InquirySubmissionService extends BaseService {


}
