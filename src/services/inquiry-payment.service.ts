import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
    providedIn: 'root',
    useFactory: () => BaseServiceFactory('inquiryinvoice', true)
})
export class InquiryPaymentService extends BaseService {

}
