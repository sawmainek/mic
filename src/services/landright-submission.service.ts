import { Injectable } from '@angular/core';
import { BaseService, BaseServiceFactory } from './base.service';

@Injectable({
  providedIn: 'root',
  useFactory: () => BaseServiceFactory('submissionlandright', true)
})
export class LandrightSubmissionService extends BaseService {

}
