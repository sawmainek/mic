import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../login/user.service';
import { Store, select } from '@ngrx/store';
import { AppState, currentUser, Login } from 'src/app/core';
import { map, switchMap, tap, takeWhile, scan, first, take } from 'rxjs/operators';
import { TokenHash } from 'src/app/core/token-hash';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-verification',
    templateUrl: './verification.component.html',
    styleUrls: ['./verification.component.scss']
})
export class VerificationComponent implements OnInit {

    verifyForm: FormGroup;
    verifyToken: any;
    submitted = false;
    loading = false;
    errMsg = '';
    lang = 'en';

    constructor(
        public router: Router,
        public userService: UserService,
        public store: Store<AppState>,
        private translateService: TranslateService,
    ) {
        this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
        this.translateService.use(this.lang ? this.lang : 'en');
        this.store
            .pipe(
                first(),
                select(currentUser),
                map((result: any) => {
                    return result ? result : JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.loginUserKey), 'mic-dica'))
                }),
                switchMap(user => { return this.userService.getVerifyToken({ email: user.email, timeout: 60 }) }),
                tap((x: any) => this.verifyToken = x.token)
            ).subscribe();
    }

    ngOnInit(): void {
        this.verifyForm = new FormGroup({
            code: new FormControl('', [Validators.required, Validators.minLength(4)]),
        });
    }

    verify() {
        console.log('enter');
        this.submitted = true;
        if (this.verifyForm.invalid || !this.verifyToken) {
            return;
        }
        this.loading = true;
        this.userService.verifyCode({ token: this.verifyToken, code: this.verifyForm.controls['code'].value })
            .subscribe((user: any) => {
                this.userService.roles(user.personal_access_token).subscribe(security => {
                    this.loading = false;
                
                    const roles: any[] = security.roles || [];
                    if (roles.filter(x => x.slug == 'view-mic' || x.slug == 'view-pat' || x.slug == 'view-region' || x.slug == 'checking' || x.slug == 'approval').length > 0) {
                        user.security = security;
                        user.type = 'officer';
                        this.store.dispatch(new Login({ user_id: user.id, user: user, isVertify: true, loggedIn: true, security: security }));
                        this.router.navigateByUrl('/officer');
                    } else {
                        user.security = security;
                        user.type = 'investor';
                        this.store.dispatch(new Login({ user_id: user.id, user: user, isVertify: true, loggedIn: true, security: security }));
                        this.router.navigateByUrl('/pages');
                    }
                })
            }, error => {
                this.loading = false;
                this.showMessage('Invalid Code.');
                console.log(JSON.stringify(error.error.message));
            });
    }

    get fg() { return this.verifyForm.controls; }

    showMessage(msg) {
        this.errMsg = msg;
        setTimeout(() => {
            this.errMsg = '';
        }, 4000);
    }

}
