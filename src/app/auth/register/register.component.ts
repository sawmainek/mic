import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { RegisterService } from './register.service';
import { UserService } from '../login/user.service';
import { Store } from '@ngrx/store';
import { AppState, Login, Register } from 'src/app/core';
import { Router } from '@angular/router';
import { MustMatch } from 'src/app/_helper/must-match.validator';
import { TranslateService } from '@ngx-translate/core';
declare var jQuery: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  regForm: FormGroup;
  submitted = false;
  loading = false;
  hide = true;
  errMsg = '';
  lang = 'en';

  constructor(
    public fb: FormBuilder,
    private translateService: TranslateService,
    public userService: UserService,
    public store: Store<AppState>,
    public router: Router
  ) {
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
    this.regForm = this.fb.group({
      name: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password_confirmation: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      consent: [false]
    }, { validator: [MustMatch('password', 'password_confirmation')] });

  }

  get fg() { return this.regForm.controls; }

  checkRegisterForm() {
    this.submitted = true;
    if (this.regForm.invalid) {
      return;
    } else {
      this.register();
    }
  }

  register() {
    this.loading = true;
    this.userService.register(this.regForm.value)
      .subscribe((user) => {
        this.store.dispatch(new Register({ user: user }));
        this.doLogin(this.regForm.value.email, this.regForm.value.password);
      },
        error => {
          this.loading = false;
          console.log(JSON.stringify(error));
          this.showMessage(JSON.stringify(error.error.message));
        });
  }

  doLogin(email: string, password: string): void {
    this.userService.login({ email, password }).subscribe((user) => {
      this.loading = false;
      this.router.navigateByUrl('/auth/verify');
    },
      error => {
        this.loading = false;
        this.showMessage(JSON.stringify(error));
      });
  }

  showMessage(msg) {
    this.errMsg = msg;
    setTimeout(() => {
      this.errMsg = '';
    }, 4000);
  }

}
