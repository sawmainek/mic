import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConstantProvider } from 'src/app/provider/constant';
import { AppsConstantProvider } from 'src/app/provider/constant.app';
import { Observable } from 'rxjs/internal/Observable';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({ providedIn: "root" })
export class RegisterService {
    constructor(public http: HttpClient, public constant: ConstantProvider, public appConstant: AppsConstantProvider) {

    }

    registerUser(data: any): Observable<any> {
        return this.http.post<any>(environment.host+ '/api/register', data, { headers: this.constant.getHeader() })
            .pipe(catchError(this.handleError),
                map(x => x)
            );
    }
    
    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}