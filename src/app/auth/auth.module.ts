import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VerificationComponent } from './verification/verification.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { authReducer, AuthEffects } from '../core';
import { EffectsModule } from '@ngrx/effects';
import { AuthService } from 'src/services/auth.service';
import { AuthGuard } from '../core/auth/_guards/auth.guards';
import { RegisterSuccessComponent } from './register-success/register-success.component';
import { HttpLoaderFactory } from '../app-routing.module';
import { HttpClient } from '@angular/common/http';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PasswordResetOtpComponent } from './password-reset-otp/password-reset-otp.component';
import { NewPasswordComponent } from './new-password/new-password.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'register-success',
        component: RegisterSuccessComponent,
    },
    {
        path: 'verify',
        component: VerificationComponent,
    },
    {
        path: 'password-reset',
        component: PasswordResetComponent,
    },
    {
        path: 'password-reset-otp',
        component: PasswordResetOtpComponent,
    },
    {
        path: 'new-password',
        component: NewPasswordComponent,
    }


];


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
		HttpClientModule,
        TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
		}),
        StoreModule.forFeature('auth', authReducer),
        EffectsModule.forFeature([AuthEffects])
    ],
    providers: [

    ],
    exports: [LoginComponent],
    declarations: [
        LoginComponent,
        RegisterComponent,
        RegisterSuccessComponent,
        VerificationComponent,
        PasswordResetComponent,
        PasswordResetOtpComponent,
        NewPasswordComponent
    ]
})

export class AuthModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AuthModule,
            providers: [
                AuthService,
                TranslateService,
                //AuthGuard
            ]
        };
    }
}
