import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../login/user.service';
import { NavExtrasService } from 'src/app/_helper/nav-extras.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-password-reset-otp',
  templateUrl: './password-reset-otp.component.html',
  styleUrls: ['./password-reset-otp.component.scss']
})
export class PasswordResetOtpComponent implements OnInit {

  otpForm: FormGroup;
  otpSubmit = false;
  loading = false;
  errMsg = '';
  lang = 'en';

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private translateService: TranslateService,
    public navExtraService: NavExtrasService,
    private router: Router,
  ) 
  { 
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
    this.otpForm = new FormGroup({
      code: new FormControl('', [Validators.required, Validators.minLength(4)]),
    });
  }

  checkOtpForm() {
    this.otpSubmit = true;
    if (this.otpForm.invalid) {
      return;
    } else {
      this.resetPassword(this.otpForm.value.code);
    }
  }

  resetPassword(code) {
    const token = this.navExtraService.getExtras();
    if (token) {
      this.loading = true;
      this.userService.resetPasword({ token: token, code: code }).subscribe(result => {
        this.loading = false;
        this.navExtraService.setExtras(result);
        this.router.navigateByUrl('/auth/new-password');
      }, err => {
        this.loading = false;
        this.showMessage('Invalid Code.');
      });
    }
  }

  get ControlOtpForm() {
    return this.otpForm.controls;
  }

  showMessage(msg) {
    this.errMsg = msg;
    setTimeout(() => {
      this.errMsg = '';
    }, 4000);
  }

}
