import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register-success',
  templateUrl: './register-success.component.html',
  styleUrls: ['./register-success.component.scss']
})
export class RegisterSuccessComponent implements OnInit {

  lang = 'en';
  constructor(    
    private translateService: TranslateService,
  ) 
  { 
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
  }

}
