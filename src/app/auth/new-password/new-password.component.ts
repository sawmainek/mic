import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../login/user.service';
import { NavExtrasService } from 'src/app/_helper/nav-extras.service';
import { Router } from '@angular/router';
import { MustMatch } from 'src/app/_helper/must-match.validator';
import { Store } from '@ngrx/store';
import { AppState, Login, Register } from 'src/app/core';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-new-password',
    templateUrl: './new-password.component.html',
    styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnInit {

    passwordForm: FormGroup;
    passwordSubmit = false;
    loading = false;
    errMsg = '';
    user: any = {};
    hide = true;
    lang = 'en';

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserService,
        public navExtraService: NavExtrasService,
        private router: Router,
        private translateService: TranslateService,
        private store: Store<AppState>,
        private toastrService: ToastrService
    ) {
        this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
        this.translateService.use(this.lang ? this.lang : 'en');
    }

    ngOnInit(): void {
        this.user = this.navExtraService.getExtras();

        this.passwordForm = this.formBuilder.group({
            current_password: ['', [Validators.required]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            password_confirmation: ['', [Validators.required, Validators.minLength(6)]]
        }, {
            validator: MustMatch('password', 'password_confirmation')
        });

    }

    checkPasswordForm() {
        this.passwordSubmit = true;
        if (this.passwordForm.invalid) {
            return;
        } else {
            this.changeNewPassword();
        }
    }

    changeNewPassword() {
        if (this.user && this.user.id) {
            this.loading = true;
            this.userService.changePassword(this.passwordForm.value, this.user.id, this.user.personal_access_token).subscribe(result => {
                this.toastrService.success('Your password is successfully changed.');
                let user: any = result;
                this.userService.roles(user.personal_access_token).subscribe(security => {
                    this.loading = false;
                    const roles: any[] = security.roles || [];
                    if (roles.filter(x => x.slug == 'view-mic' || x.slug == 'view-pat' || x.slug == 'view-region' || x.slug == 'checking' || x.slug == 'approval').length > 0) {
                        user.security = security;
                        user.type = 'officer';
                        this.store.dispatch(new Login({ user_id: user.id, user: user, isVertify: true, loggedIn: true, security: security }));
                        this.router.navigateByUrl('/officer');
                    } else {
                        user.security = security;
                        user.type = 'investor';
                        this.store.dispatch(new Login({ user_id: user.id, user: user, isVertify: true, loggedIn: true, security: security }));
                        this.router.navigateByUrl('/pages');
                    }
                })
            },
                error => {
                    this.loading = false;
                    this.showMessage('Your current password is wrong. Please check your mail inbox.');
                });
        } else {
            this.loading = false;
            this.toastrService.error('Something Wrong.');
            this.router.navigateByUrl('/auth/password-reset');
        }
    }

    get ControlPasswordForm() {
        return this.passwordForm.controls;
    }

    showMessage(msg) {
        this.errMsg = msg;
        setTimeout(() => {
            this.errMsg = '';
        }, 5000);
    }

}
