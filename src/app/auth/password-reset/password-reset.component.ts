import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../login/user.service';
import { NavExtrasService } from 'src/app/_helper/nav-extras.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {

  forgotForm: FormGroup;
  forgotSubmit = false;
  loading = false;
  errMsg = '';
  lang = 'en';

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private translateService: TranslateService,
    private navExtraService: NavExtrasService,
    private router: Router,
  ) 
  { 
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
    this.forgotForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  get ControlForgot() {
    return this.forgotForm.controls;
  }

  checkMailForm() {
    this.forgotSubmit = true;
    if (this.forgotForm.invalid) {
      return;
    } else {
      this.checkUserMail(this.forgotForm.value.email);
    }
  }

  checkUserMail(mail) {
    this.loading = true;
    this.userService.getForgotPasswordToken({ email: mail, timeout: 60 }).subscribe((result: any) => {
      this.loading = false;
      this.navExtraService.setExtras(result.token);
      this.router.navigateByUrl('/auth/password-reset-otp');
    },
      error => {
        this.loading = false;
        this.showMessage('Mail not found.');
      });
  }

  showMessage(msg) {
    this.errMsg = msg;
    setTimeout(() => {
      this.errMsg = '';
    }, 4000);
  }

}
