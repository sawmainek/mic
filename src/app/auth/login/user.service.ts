import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenHash } from 'src/app/core/token-hash';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    host: string;
    token: any;

    constructor(
        private http: HttpClient
    ) {
        this.host = environment.host;
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
    }

    getUser(token): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            })
        };
        return this.http.get<any>(this.host + '/api/user', httpOptions);
    }

    // search(search: string): Observable<any[]> {
    //     this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
    //     const httpOptions = {
    //         headers: new HttpHeaders({
    //             'Content-Type': 'application/json',
    //             'Authorization': 'Bearer ' + this.token.access_token
    //         })
    //     };
    //     return this.http.get<any[]>(this.host + `/api/users?search=${search}:OR&sort=id&order=desc&page=1&rows=12`, httpOptions);
    // }

    register(data): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.post(this.host + '/api/register', data, httpOptions);
    }

    getVerifyToken(data) {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.post(this.host + '/api/verification-code/email', data, httpOptions);
    }

    edit(data: any, token): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                Accept: 'application/json',
                Authorization: 'Bearer ' + this.token.access_token
            })
        };
        return this.http.post(this.host + '/api/users', data, httpOptions);
    }

    login(data): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.post<any>(this.host + `/api/login`, data, httpOptions);
    }

    roles(token): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            })
        };
        return this.http.get<any>(this.host + `/api/user/role`, httpOptions);
    }

    forget(data): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.post<any>(this.host + `/api/forget`, data, httpOptions);
    }

    changeNewPassword(data: any): Observable<any> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.put(this.host + `/api/users/${data.id}`, { password: data.password }, httpOptions);
    }

    changePassword(data, id, token) {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            })
        };
        return this.http.post(this.host + `/api/change-password/${id}`, data, httpOptions);
    }


    getForgotUser(email: any): Observable<any[]> {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.get<any[]>(this.host + `/api/users?search=email:like:${email}`, httpOptions);
    }

    verifyCode(data) {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.post<any[]>(this.host + '/api/verify/code', data, httpOptions);
    }

    getForgotPasswordToken(data) {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.post(this.host + '/api/forget-password/email', data, httpOptions);
    }

    resetPasword(data) {
        this.token = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.tokenKey), 'mic-dica'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.token.access_token
            })
        };
        return this.http.post(this.host + '/api/reset-password', data, httpOptions);
    }

    editProfile(data, id, token): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                Authorization: 'Bearer ' + token
            })
        };
        return this.http.post(this.host + `/api/edit-profile/${id}`, data, httpOptions);
    }

}
