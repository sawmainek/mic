import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from './user.service';
import { ConstantProvider } from 'src/app/provider/constant';
import { AppsConstantProvider } from 'src/app/provider/constant.app';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState, Login, Register } from 'src/app/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    message = '';
    loading = false;
    redirect: string;
    submitted = false;
    lang = 'en';
    errMsg = '';
    hide = true;

    constructor(
        private userService: UserService,
        public constant: ConstantProvider,
        public appsConstant: AppsConstantProvider,
        public router: Router,
        private translateService: TranslateService,
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute,
    ) {
        this.redirect = this.activatedRoute.snapshot.queryParamMap.get('return');
        this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
        this.translateService.use(this.lang ? this.lang : 'en');
    }

    ngOnInit(): void {
        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
    }

    get login() {
        return this.loginForm.controls;
    }

    checkLoginForm() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        } else {
            this.doLogin();
        }
    }

    doLogin() {
        this.loading = true;
        this.userService.login(this.loginForm.value).subscribe((user) => {
            this.loading = false;
            this.store.dispatch(new Register({ user: user }));
            this.router.navigateByUrl('/auth/verify');
        },
            error => {
                this.loading = false;
                this.showMessage('Your username and password combination is incorrect.');
                console.log(JSON.stringify(error.error.message));
            });
    }

    showMessage(msg) {
        this.errMsg = msg;
        setTimeout(() => {
            this.errMsg = '';
        }, 4000);
    }

}
