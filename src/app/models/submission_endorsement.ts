export class SubmissionEndorsement {
    id: number;
    form_id: number;
    submitted_user_id: number;
    application_no: any;
    investment_value: number;
    investment_enterprise_name: any;
    investment_type: any;
    sector_of_investment: any;
    submitted_to: any;	
    status: any;
}