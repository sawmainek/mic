export class ExpectedInvestdt {
    cash_equivalent: CashEquivalent;
    start_cash_date: string;
    end_cash_date: string;
    machinery_equipment: Breakdown;
    machinery_total_kyat: number;
    machinery_total_usd: number;
    raw_materials: Breakdown;
    raw_total_kyat: number;
    raw_total_usd: number;
    intellectual_upload: DocumentType;
    evidence_ownership_upload: DocumentType[];
    supporting_upload: DocumentType[];
}

interface CashEquivalent {
    name: string;
    myan_origin_amount: number;
    foreign_origin_list: ForeignOrigin;
    foreign_origin_total: ForeignOrigin;

}

interface Breakdown {
    item: string;
    hs_code: number;
    unit: string;
    unit_price: number;
    quantity: number;
    total_price: number;
    currency: number;
    equ_kyat: number;
    equ_usd: number;
    country: string;
}

interface ForeignOrigin {
    amount: number;
    courrency_id: number;
}
