export class SubmissionMic {
    id: number;
    form_id: number;
    submitted_user_id: number;
    investment_value: number;
    application_no: any;
    investment_enterprise_name: any;
    investment_type: any;
    sector_of_investment: any;
    status: any;
}