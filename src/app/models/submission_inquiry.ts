export class SubmissionInquiry {
    id: number;
    form_id: number;
    submitted_user_id: number;
    application_no: any;
    investment_value: any;
    investor_name: any;
    country: any;
    submit_to: any;
    sector_of_investment: any;
    status: any;
}	