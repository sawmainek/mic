export class TaxIncentive {
    id: number;
    user_id: number;
    choose_language: string;
    authorized_representative: any;
    investment: any;
    relief_section: any;
    section_75_a: any;
    section_77_a: any;
    section_77_a_investor: any;
    section_77_b: any;
    section_77_c: any;
    section_77_d: any;
    section_77_d_investor: any;
    section_78_a: any;
    submission: any;
    is_draft: boolean;
    agreement: any;
    owner: any;
    endorsement_no: any;
}