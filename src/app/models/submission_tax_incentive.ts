export class SubmissionTaxIncentive {
    id: number;
    form_id: number;
    submitted_user_id: number;
    application_no: any;
    permit_endorsement_no: any;
    investment_enterprise_name: any;
    applied: any;
    status: any;
}	