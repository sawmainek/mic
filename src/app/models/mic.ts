import { ExpectedInvestment } from './expected_invest';
export class Mic {
    submission: any;
    id: number;
    user_id: number;
    choose_language: string;

    // Section A
    overview_is_established: string;
    invest_form: InvestForm;
    investor_list: InvestorList[] = [];

    // Section B
    company_info: CompanyInfo;
    share_holder: ShareHolder;
    significant_list: SignificantList;

    // Section C
    timeline: Timeline;
    expected_investment: ExpectedInvestment;
    expected_investmentdt: ExpectedInvestmentDetail;
    expected_by_zone: ExpectedByZone;
    expected_by_phase: ExpectedByPhase;
    proposed_invest_basic_info: BasicInfo;
    employment_plan: EmploymentPlan;

    // Section D
    wish_to_apply: string;
	owner: any;
	location: any;
	land : any;
	building: any;
	lease_agreement: any;
	use_consumption: any;

    // Section E
    other_attachments: DocumentType[];
    other_attachments_div_2: DocumentType[];
    other_attachments_div_3: DocumentType[];
    other_attachments_div_4: DocumentType[];

    // Section F
    other_investor_info: OtherInvestorInfo;

    // Permit Certificate
    permit: any;

    // MIC Undertaking
    undertaking: any;
}

interface OtherInvestorInfo {
    is_authorized: string;
    company_name: string;
    company_address: string;
    contact_full_name: string;
    contact_position: string;
    contact_phone: number;
    contact_email: string;
    contact_nrc_passport: string;
    contact_citizenship: string;
    document: DocumentType;
}

interface Location {
    location_list: LocationList[];
    is_agree: boolean;
    lessor: string;
    production: string;
    electricity_amount: number;
    supply_amount: number;
}

interface LocationList {
    a: A;
    b: B;
    c: C;
    particular_building: ParticularBuilding[];
    e: E;
    f: F;
}

interface F {
    product_value: number;
    electricity_amount: number;
    supply_amount: number;
}

interface E {
    agreement_document: DocumentType;
    lessee_name: string;
    lease_from: string;
    lease_to: string;
    rental_fees: number;
    fees_of_square: number;
    premium_fees: number;
    is_agree: string;
    lessor: number;
    is_payment: string;
    description: string;
    building_document: DocumentType;
    owner_type: string;
    name: string;
    nrc: string;
    address: string;
    email: string;
    phone: string;
    person_name: string;
    person_position: string;
    person_phone: number;
    person_email: string;
}

interface C {
    land_type: string;
    is_change: string;
    land_type_description: string;
    is_required: string;
    required_description: string;
    unit: string;
    area: number;
    description: string;
    layout_plan_document: DocumentType;
    map_document: DocumentType;
    change_documents: DocumentType[];
    required_document: DocumentType;
}

interface B {
    state: string;
    district: string;
    township: string;
    ward: string;
    location_address: string;
    zone: string;
    zone_description: string;
}

interface A {
    permit_from: string;
    permit_to: string;
    is_investor: boolean;
    owner_specification: string;
    owner_type: string;
    name: string;
    address: string;
    nrc: string;
    email: string;
    phone: number;
    register_no: string;
    person_name: string;
    person_position: string;
    person_phone: number;
    person_email: string;
    ownership_document: DocumentType;
    agreement_document: DocumentType;
}

interface ParticularBuilding {
    area: number;
    unit: number;
    detail: string;
    value: number;
    map_document: DocumentType;
    photo_document: DocumentType;
    other_documents: DocumentType[];
}

interface EmploymentPlan {
    years: Year[];
    total_myan: number;
    total_foreign: number;
    plan_upload: DocumentType;
}

interface Year {
    positions: YearList[];
    total_myan_qty: number;
    total_myan_salary: number;
    total_foreign_qty: number;
    total_foreign_salary: number;
    total_qty: number;
    total_salary: number;
}

interface YearList {
    position: string;
    classification_id: string;
    myan_qty: number;
    myan_salary: number;
    foreign_qty: number;
    foreign_salary: number;
    qty: number;
    salary: number;
}

interface BasicInfo {
    loan_repay: LoanRepay[];
    issued_share: IssuedShare[];
    other_source: OtherSource[];
    myanmar_capital_kyat: number;
    myanmar_capital_usd: number;
    foreign_capital_kyat: number;
    foreign_capital_usd: number;
    evidence_upload: DocumentType[];
}

interface LoanRepay {
    list: LoanRepayList;
    total_kyat: number;
    total_usd: number;
}

interface LoanRepayList {
    origin_id: string;
    source: string
    currency: number;
    value: number;
    equ_kyat: number;
    equ_usd: number;
}

interface OtherSourceList {
    type: string;
    origin_id: string;
    currency: number;
    value: number;
    equ_kyat: number;
    equ_usd: number;
}

interface IssuedShareList {
    origin_id: number;
    currency: number;
    value: number;
    equ_kyat: number;
    equ_usd: number;
}

interface OtherSource {
    list: OtherSourceList;
    total_kyat: number;
    total_usd: number;
}

interface IssuedShare {
    list: IssuedShareList;
    total_kyat: number;
    total_usd: number;
}

interface ExpectedByPhase {
    construction_phase: Phase[];
    commercial_phase: Phase[];
}

interface Phase {
    start_date: string;
    end_date: string;
    currency: number;
    expected_value: number;
}

interface ExpectedByZone {
    zone1: Zone[];
    zone2: Zone[];
    zone3: Zone[];
    zone1_percent_total: number;
    zone2_percent_total: number;
    zone3_percent_total: number;
    supporting_upload: DocumentType[];
}

interface Zone {
    state_id: string;
    district_id: string;
    township_id: string;
    pecent_expected: number;
}

interface ExpectedInvestmentDetail {
    cash_equivalent: CashEquivalent;
    start_cash_date: string;
    end_cash_date: string;
    machinery_equipment: Breakdown;
    machinery_total_kyat: number;
    machinery_total_usd: number;
    raw_materials: Breakdown;
    raw_total_kyat: number;
    raw_total_usd: number;
    intellectual_upload: DocumentType;
    evidence_ownership_upload: DocumentType[];
    supporting_upload: DocumentType[];
}

interface CashEquivalent {
    name: string;
    myan_origin_amount: number;
    foreign_origin: ForeignOrigin;

}

interface Breakdown {
    item: string;
    hs_code: number;
    unit: string;
    unit_price: number;
    quantity: number;
    total_price: number;
    currency: number;
    equ_kyat: number;
    equ_usd: number;
    country: string;
}

interface ForeignOrigin {
    amount: number;
    courrency_id: number;
}

interface Timeline {
    construction_start_date: string;
    construction_end_date: string;
    commercial_start_date: string;
    commercial_end_date: string;
    investment_start_date: string;
    investment_end_date: string;
    elaborate: string;
    document_upload: DocumentType[];
}

interface SignificantList {
    signi_person_list: SingiPersonList[];
    sub_company_list: SigniCompanyList[];
}

interface SingiPersonList {
    name: string;
    investor_type_id: string;
    country: number;
}

interface SigniCompanyList {
    name: string;
}

interface ShareHolderList {
    name: string;
    shareholder_type_id: string;
    country: number;
    share_percentage: number;
}

interface ShareHolder {
    sharholder_list: ShareHolderList[];
    myanmar_share_ratio: number;
    foreigner_share_ratio: number;
    gover_org_share_ration: number;
}

interface CompanyInfo {
    general_sector_id: number;
    investment_name: string;
    company_type_id: string;
    company_certificate_no: string;
    certificate_upload: DocumentType;
    address: string;
    phone_no: number;
    fax: number;
    email_address: string;
    website: string;

    general_business_sector: any;
    general_section_id: number;
    general_division_id: number;
    specific_section_id: number;
    specific_division_id: number;
    specific_group_id: number;
    specific_class_id: number;
    specific_prod_section_id: number;
    specific_prod_division_id: number;
    specific_prod_group_id: number;
    specific_prod_class_id: number;
    specific_prod_subclass_id: number;

    invest_description: string;
    estimated_domestic: number;
    estimated_export: number;
    invest_benefit: string;

    contact_name: string;
    contact_position: string;
    contact_phone: number;
    contact_email: string;
}

interface InvestorList {
    investor_type: number;
    country: number;
    individual: Individual;
    company: Company;
    government: Government;
}

interface Individual {
    full_name: string;
    citizenship: string;
    passport_or_nrc: string;
    address: string;
    phone_no: number;
    email: string;
    business_expertise: string;
}

interface Company {
    company_name: string;
    incorporate_country: string;
    company_type: string;
    address: string;
    phone_no: number;
    fax_no: number;
    email: string;
    website: string;
    short_description: string;
    parent_company_name: string;
    parent_company_address: string;
    contact_full_name: string;
    contact_position: string;
    contact_phone_no: number;
    contact_email: string;
}

interface Government {
    name: string;
    address: string;
    phone_no: number;
    fax: number;
    email: string;
    website: string;
    contact_full_name: string;
    contact_position: string;
    contact_phone_no: number;
    contact_email: string;
}

interface InvestForm {
    form_investment: string;
    myan_direct_share_percent: number;
    joint_direct_share_percent: number;
    joint_document: DocumentType;
    special_type: string;
    other_type: string;
    contract_document: DocumentType;
    ministry_document: DocumentType;
}

interface DocumentType {
    id: number;
    type: string;
    type_id: number;
    name: string;
    file: string;
    submit: string[];
    text: string[];
    error: string[];
}

