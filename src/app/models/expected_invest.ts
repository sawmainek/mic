export class ExpectedInvestment {
    exchange_usd: number;
    exchange_kyat: number;
    exchange_date: string;
    expected_list: ExpectedList[];
    total_expected_amount: number;
    total_expected_equ_kyat: number;
    total_expected_equ_usd: number;
    percent_expected_construction: number;
    percent_expected_commercial: number;
    explanation_doc: DocumentType;
    spent_percent_amount_kyat: number;
}

interface ExpectedList {
    name: string;
    currency: number;
    usd: number;
    kyat: number;
}
