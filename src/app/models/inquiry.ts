export interface Inquiry {
    id: number;
    user_id: number;
    apply_to: string;
    language: string;
    inquiry_to_state: string;
    citizen_share_ratio: number;
    foreigner_share_ratio: number;
    government_share_ratio: number;
    company_type: string;
    general_business_sector: string;
    check: boolean;
    draft: boolean;
    description: string;
    investment: Investment;
    plan_location: PlanLocation;
    cross_border: CrossBorder;
    relocation: Relocation;
    legal_right: LegalRight;
    conflict: Conflict;
    request_information: RequestInformation;
    investor: Investor;
    applicant_investor: ApplicantInvestor;
    impact: Impact;
    documents: Documents[];
    comment: string;
    business_sector: any;
    business_cpc: any;
    submission: any;
}

interface BusinessCPC {
    cpc_section_id: string;
    cpc_division_id: string;
    cpc_group_id: string;
    cpc_class_id: string[];
    cpc_sub_class_ids: string[];
}

export interface BusinessSector {
    apply_to: string;
    business_sector_id: string;
    isic_section_id: string;
    isic_division_id: string;
    isic_group_id: string;
    isic_class_ids: string[];
}

interface Documents {
    type: string;
    type_id: number;
    name: string;
    file: File;
}

interface File {
    id: number;
    name: string;
    type: string;
    size: number;
    url: string;
    submit: string;
    text: string;
}

interface Impact {
    is_adversely_impact: boolean;
    number_adversely_impact: string;
}

interface ApplicantInvestor {
    name: string;
    country: number;
    address: string;
    personal_id_no: string;
    email: string;
    phone: number;
    fax: number;
}

interface Investor {
    name: string;
    country: number;
    address: string;
    address_other: string;
    personal_id_no: number;
    email: string;
    phone: number;
    fax: number;
}

interface Investment {
    currency_code: number;
    unit_id: number;
    e_usd: number;
    e_kyat: number;
    area: number;
}

interface RequestInformation {
    option1: boolean;
    option2: boolean;
    option3: boolean;
    option4: boolean;
    option5: boolean;
    option6: boolean;
    other: string;
}

interface PlanLocation {
    state: number;
    district: number;
    township_id: number;
    owner_name: string;
    owner_type_id: string;
    lessor_name: string;
    lessor_owner_type_id: string;
    plan_from: string;
    plan_to: string;
    land_type_id: number;
}

interface CrossBorder {
    is_cross_border: boolean;
    elaborate: string;
}

interface Relocation {
    has_compensation: string; // Yes No
    is_relocation: string;
    number_relocation: number;
    is_more_than_100: string;
}

interface LegalRight {
    is_legal_right: boolean;
    number_legal_right: string;
}

interface Conflict {
    is_conflict: boolean;
    number_conflict: string;
}

