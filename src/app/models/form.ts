export class Form {
    id: number;
    type: string;
    type_id: number;
    user_id: number;
    data: any;
    data_mm: any;
    status: any;
    language: string;
}