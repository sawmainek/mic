export class Shareholder {
    sharholder_list: ShareHolderList[];
    myanmar_share_ratio: number;
    foreigner_share_ratio: number;
    gover_org_share_ration: number;
}

interface ShareHolderList {
    name: string;
    shareholder_type_id: string;
    country: number;
    share_percentage: number;
}
