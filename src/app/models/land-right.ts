export class LandRight {
    id: number;
    user_id: number;
    choose_language: string;

    authorized_person: AuthorizedPerson;
    investment: Investment;
    owner: Owner;
    submission: any;

    land_to_state: any;
    apply_to: any;
}

interface Owner {
    land_evidence_docs: DocumentType;
    contract_docs: DocumentType;
    land_grant_start_date: string;
    land_grant_end_date: string;
    owner_type_id: string;
    company: Company;
    government: Government;
    individual: Individual;
}

interface Individual {
    full_name: string;
    citizenship: string;
    passport_or_nrc: string;
    address: string;
    phone_no: number;
    email: string;
    business_expertise: string;
}

interface Company {
    company_name: string;
    incorporate_country: string;
    company_type: string;
    address: string;
    phone_no: number;
    fax_no: number;
    email: string;
    website: string;
    short_description: string;
    parent_company_name: string;
    parent_company_address: string;
    contact_full_name: string;
    contact_position: string;
    contact_phone_no: number;
    contact_email: string;
}

interface Government {
    name: string;
    address: string;
    phone_no: number;
    fax: number;
    email: string;
    website: string;
    contact_full_name: string;
    contact_position: string;
    contact_phone_no: number;
    contact_email: string;
}

interface Investment {
    endorsement_permit_no: number;
    endorsement_permit_date: string;
}

interface AuthorizedPerson {
    is_authorized_applicant: string;
    company_name: string;
    company_address: string;
    auth_contact_full_name: string;
    auth_contact_position: string;
    auth_contact_phone: string;
    auth_contact_email: string;
    auth_passport_or_nrc: string;
    auth_citizenship: string;
    official_legal_docs: DocumentType;
}

interface DocumentType {
    id: number;
    type: string;
    type_id: number;
    name: string;
    file: string;
    submit: string[];
    text: string[];
    error: string[];
}
