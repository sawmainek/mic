export class Endorsement {
    //New Insert
    is_authorized: any;

    id: number;
    user_id: number;

    // Form
    apply_to: string

    // State and Region
    state: string;

    // Choose Language
    choose_language: string;

    // Section A
    overview_is_established: string;
    invest_form: InvestForm;
    investor_list: InvestorList[] = [];

    // Section B
    company_info: CompanyInfo;
    share_holder: ShareHolder;

    // Section C
    timeline: Timeline;
    expected_investment: ExpectedInvestment;
    expected_by_zone: ExpectedByZone;
    expected_by_phase: ExpectedByPhase;
    expected_investmentdt: ExpectedInvestmentDetail;
    employment_plan: EmploymentPlan;
    proposed_basic_info: BasicInfo;

    // Section D
    wish_to_apply: string;
    owner: any;
    location: any;
    land: any;
    building: any;
    agreement: any;

    // Section E
    other_attachments: any;
    other_attachments_div_2: DocumentType[];
    other_attachments_div_3: DocumentType[];
    other_attachments_div_4: DocumentType[];

    // Certificate
    permit: any;

    // Undertaking
    undertaking: any;

}

interface Location {
    location_list: LocationList[];
    is_agree: boolean;
    lessor: string;
    production: string;
    electricity_amount: number;
    supply_amount: number;
}

interface LocationList {
    a: A;
    b: B;
    c: C;
    particular_building: ParticularBuilding[];
    e: E;
    f: F;
}

interface F {
    product_value: number;
    electricity_amount: number;
    supply_amount: number;
}

interface E {
    agreement_document: DocumentType;
    lessee_name: string;
    lease_from: string;
    lease_to: string;
    rental_fees: number;
    fees_of_square: number;
    premium_fees: number;
    is_agree: string;
    lessor: number;
    is_payment: string;
    description: string;
    building_document: DocumentType;
    owner_type: string;
    name: string;
    nrc: string;
    address: string;
    email: string;
    phone: string;
    person_name: string;
    person_position: string;
    person_phone: number;
    person_email: string;
}

interface C {
    land_type: string;
    is_change: string;
    land_type_description: string;
    is_required: string;
    required_description: string;
    unit: string;
    area: number;
    description: string;
    layout_plan_document: DocumentType;
    map_document: DocumentType;
    change_documents: DocumentType[];
    required_document: DocumentType;
}

interface B {
    state: string;
    district: string;
    township: string;
    ward: string;
    location_address: string;
    zone: string;
    zone_description: string;
}

interface A {
    permit_from: string;
    permit_to: string;
    is_investor: boolean;
    owner_specification: string;
    owner_type: string;
    name: string;
    address: string;
    nrc: string;
    email: string;
    phone: number;
    register_no: string;
    person_name: string;
    person_position: string;
    person_phone: number;
    person_email: string;
    ownership_document: DocumentType;
    agreement_document: DocumentType;
}

interface ParticularBuilding {
    area: number;
    unit: number;
    detail: string;
    value: number;
    map_document: DocumentType;
    photo_document: DocumentType;
    other_documents: DocumentType[];
}

interface EmploymentPlan {
    year: Year;
    total_myan: number;
    total_foreign: number;
    plan_upload: DocumentType;
}

interface Year {
    year_list: YearList[];
    total_myan_qty: number;
    total_myan_salary: number;
    total_foreign_qty: number;
    total_foreign_salary: number;
    total_qty: number;
    total_salary: number;
}

interface YearList {
    position: string;
    classification_id: string;
    myan_qty: number;
    myan_salary: number;
    foreign_qty: number;
    foreign_salary: number;
    qty: number;
    salary: number;
}

interface BasicInfo {
    loan_repay: LoanRepay[];
    issued_share: IssuedShare[];
    other_source: OtherSource[];
    myanmar_capital_kyat: number;
    myanmar_capital_usd: number;
    foreign_capital_kyat: number;
    foreign_capital_usd: number;
    evidence_upload: DocumentType[];
}

interface IssuedShare {
    list: IssuedShareList;
    total_kyat: number;
    total_usd: number;
}

interface LoanRepay {
    list: LoanRepayList;
    total_kyat: number;
    total_usd: number;
}

interface LoanRepayList {
    origin_id: string;
    source: string
    currency: number;
    value: number;
    equ_kyat: number;
    equ_usd: number;
}

interface OtherSourceList {
    type: string;
    origin_id: string;
    currency: number;
    value: number;
    equ_kyat: number;
    equ_usd: number;
}

interface IssuedShareList {
    origin_id: number;
    currency: number;
    value: number;
    equ_kyat: number;
    equ_usd: number;
}

interface OtherSource {
    list: OtherSourceList;
    total_kyat: number;
    total_usd: number;
}

interface ExpectedInvestmentDetail {
    cash_equivalent: CashEquivalent;
    start_cash_date: string;
    end_cash_date: string;
    machinery_equipment: ExpectedDetail[];
    machinery_total_kyat: number;
    machinery_total_usd: number;
    raw_materials: ExpectedDetail[];
    raw_total_kyat: number;
    raw_total_usd: number;
}

interface CashEquivalent {
    name: string;
    myan_origin_amount: number;
    foreign_origin: ForeignOrigin;

}

interface ForeignOrigin {
    amount: number;
    courrency_id: number;
}

interface ExpectedDetail {
    item: string;
    hs_code: number;
    unit: string;
    unit_price: number;
    quantity: number;
    total_price: number;
    currency: number;
    equ_kyat: number;
    equ_usd: number;
    country: string;
}

interface ExpectedByPhase {
    construction_phase: Phase[];
    commercial_phase: Phase[];
}

interface Phase {
    start_date: string;
    end_date: string;
    currency: number;
    expected_value: number;
}

interface ExpectedByZone {
    zone1: Zone[];
    zone2: Zone[];
    zone3: Zone[];
    zone1_percent_total: number;
    zone2_percent_total: number;
    zone3_percent_total: number;
    supporting_upload: DocumentType[];
}

interface Zone {
    state_id: string;
    district_id: string;
    township_id: string;
    pecent_expected: number;
}

interface ExpectedInvestment {
    exchange_usd: number;
    exchange_kyat: number;
    exchange_date: string;
    expected_list: ExpectedList[];
    total_expected_amount: number;
    total_expected_equ_kyat: number;
    total_expected_equ_usd: number;
    percent_expected_construction: number;
    percent_expected_commercial: number;
    explanation_upload: DocumentType;
    spent_percent_amount_kyat: number
}

interface ExpectedList {
    name: string;
    currency: number;
    usd: number;
    kyat: number;
}

interface Timeline {
    construction_start_date: string;
    construction_end_date: string;
}

interface ShareHolder {
    sharholder_list: ShareHolderList[];
    myanmar_share_ratio: number;
    foreigner_share_ratio: number;
    gover_org_share_ration: number;
}

interface ShareHolderList {
    name: string;
    shareholder_type_id: string;
    country: number;
    share_percentage: number;
}

export interface CompanyInfo {
    overview_is_established: string;
    apply_to: string;
    is_preview: string;
    investment_name: string;
    company_type_id: string;
    company_certificate_no: string;
    certificate_upload: DocumentType;
    address: string;
    phone_no: number;
    fax: number;
    email_address: string;
    website: string;

    specific_business_sector: any;
    general_section_id: string;
    general_division_id: string;
    general_sector_id: string;
    specific_section_id: string;
    specific_division_id: string;
    specific_group_id: string;
    specific_class_id: string;
    specific_prod_section_id: string;
    specific_prod_division_id: string;
    specific_prod_group_id: string;
    specific_prod_class_id: string;
    specific_prod_subclass_id: string;

    invest_description: string;
    estimated_domestic: number;
    estimated_export: number;
    invest_benefit: string;

    contact_name: string;
    contact_position: string;
    contact_phone: number;
    contact_email: string;
}

interface InvestorList {
    investor_type: number;
    country: number;
    individual: Individual;
    company: Company;
    government: Government;
}

interface InvestForm {
    form_investment: string;
    myan_direct_share_percent: number;
    joint_direct_share_percent: number;
    joint_document: DocumentType;
    special_type: string;
    other_type: string;
    contract_document: DocumentType;
    ministry_document: DocumentType;
}

interface OtherAttachDocs {

}

interface DocumentType {
    id: number;
    type: string;
    type_id: number;
    name: string;
    file: string;
    submit: string[];
    text: string[];
    error: string[];
}

interface Individual {
    full_name: string;
    citizenship: string;
    passport_or_nrc: string;
    address: string;
    phone_no: number;
    email: string;
    business_expertise: string;
}

interface Company {
    company_name: string;
    incorporate_country: string;
    company_type: string;
    address: string;
    phone_no: number;
    fax_no: number;
    email: string;
    website: string;
    short_description: string;
    parent_company_name: string;
    parent_company_address: string;
    contact_full_name: string;
    contact_position: string;
    contact_phone_no: number;
    contact_email: string;
}

interface Government {
    name: string;
    address: string;
    phone_no: number;
    fax: number;
    email: string;
    website: string;
    contact_full_name: string;
    contact_position: string;
    contact_phone_no: number;
    contact_email: string;
}
