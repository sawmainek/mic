import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-common-header',
  templateUrl: './common-header.component.html',
  styleUrls: ['./common-header.component.scss']
})
export class CommonHeaderComponent implements OnInit {

  @Input() page = 'home';
  lang = 'en';
  user: any;

  constructor(
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
    public authService: AuthService,
  ) {
    this.getCurrentUser();
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
    this.authService.changeLanguage$.subscribe(lang => {
      this.translateService.setDefaultLang(lang);
      this.cdr.detectChanges();
    });
    console.log("Route" + this.page);
  }

  getCurrentUser() {
    this.authService.getCurrentUser().subscribe(user => {
      this.user = user;
    }, error => {
      console.log(JSON.stringify(error.error.message));
    });
  }

  changeLanguage(lan) {
    this.lang = lan;
    localStorage.setItem('lan', lan);
    this.translateService.use(lan);
    this.authService.changeLanguage$.next(lan);
    this.cdr.detectChanges();
  }

}
