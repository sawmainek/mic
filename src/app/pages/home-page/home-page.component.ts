import { ToastrService } from 'ngx-toastr';
import { Loaded } from './../../core/form/_actions/form.actions';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AppsConstantProvider } from '../../provider/constant.app';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AppState, currentUser, Login, Logout } from 'src/app/core';
import { map, tap, delay } from 'rxjs/operators';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { InquiryService } from 'src/services/inquiry.service';
import { InquirySubmissionService } from 'src/services/inquiry-submission.service';
import { MicSubmissionService } from 'src/services/mic-submission.service';
import { EndorsementSubmissionService } from 'src/services/endorsement-submission.service';
import { TaxincentiveSubmissionService } from 'src/services/taxincentive-submission.service';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { forkJoin } from 'rxjs';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

    username: any;
    user: any;
    loading: boolean = false;
    language: any = 'en';

    constructor(
        private router: Router,
        private translateService: TranslateService,
        private cdr: ChangeDetectorRef,
        private store: Store<AppState>,
        private authService: AuthService,
        private micSubmissionService: MicSubmissionService,
        private endoSubmissionService: EndorsementSubmissionService
    ) {
        this.language = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.language ? this.language : 'en');
        this.translateService.use(this.language ? this.language : 'en');

        this.authService.changeLanguage$.subscribe(lang => {
            this.translateService.setDefaultLang(lang);
            this.language = lang;
            this.translateService.setDefaultLang(this.language ? this.language : 'en');
            this.translateService.use(this.language ? this.language : 'en');
            this.cdr.detectChanges();
        });

        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.language = event.lang;
            this.cdr.detectChanges();
        });
    }

    ngOnInit(): void {
        this.store.dispatch(new Loaded({ loaded: true }));
        this.store
            .pipe(
                select(currentUser),
                map((result: any) => {
                    return result;
                })).subscribe(user => {
                    if (user) {
                        this.user = user;
                        this.username = user.name;
                    }
                });
    }

    logout() {
        this.store.dispatch(new Logout());
        this.router.navigateByUrl('/auth/login');
    }

    openPage(link) {
        this.router.navigateByUrl('pages/' + link);
    }

    openInquiry() {
        this.router.navigateByUrl('pages/inquiry');
        // const params: any = {
        //     page: 1,
        //     search: `submitted_user_id:equal:${this.user.id}`,
        //     secure: true,
        // };
        // this.loading = true;
        // this.inquirySubmissionService.get(params).subscribe((results: any) => {
        //     this.loading = false;
        //     if (results.length > 0 && results[0].inquiry_id) {
        //         this.loading = false;
        //         this.router.navigate(['/pages/status-checker-detail', results[0].inquiry_id || null, 'inquiry']);
        //     } else {
        //         this.loading = false;
        //         this.router.navigateByUrl('pages/inquiry');
        //     }
        // });
    }

    openMIC() {
        this.router.navigateByUrl('pages/mic/choose-language');
        // Will Remove when Production
        /* const params: any = {
            page: 1,
            search: `submitted_user_id:equal:${this.user.id}`,
            secure: true,
        };
        this.loading = true;
        forkJoin([
            this.endoSubmissionService.get(params),
            this.micSubmissionService.get(params)
        ]).subscribe(result => {
            this.loading = false;
             // Will Remove when Production
            // Check for already submitted to Endorsement
            // if(result[0].length == 0) {
                if (result[1].length > 0) {
                    this.loading = false;
                    this.router.navigate(['/pages/status-checker-detail', result[1][0].id || null, 'mic']);
                } else {
                    this.loading = false;
                    this.router.navigateByUrl('pages/mic/choose-language');
                }
            // }else {
            //    this.toastr.error("You already submitted to Endorsement.");
            // }
        }) */
    }

    openEndo() {
        this.router.navigateByUrl('pages/endorsement/form');
         // Will Remove when Production
       /*  const params: any = {
            page: 1,
            search: `submitted_user_id:equal:${this.user.id}`,
            secure: true,
        };
        this.loading = true;
        forkJoin([
            this.micSubmissionService.get(params),
            this.endoSubmissionService.get(params)
        ]).subscribe(result => {
            this.loading = false;
             // Will Remove when Production
            // Check for already submitted to MIC Permit
            // if (result[0].length == 0) {
                if (result[1].length > 0) {
                    this.loading = false;
                    this.router.navigate(['/pages/status-checker-detail', result[1][0].id || null, 'endorsement']);
                } else {
                    this.loading = false;
                    this.router.navigateByUrl('pages/endorsement/form');
                }
            // } else {
            //      this.toastr.error("You already submitted to MIC Permit.");
            // }
        }); */
    }

    openTax() {
        this.router.navigateByUrl('pages/tax-incentive/choose-language');
        // const params: any = {
        //     page: 1,
        //     search: `submitted_user_id:equal:${this.user.id}`,
        //     secure: true,
        // };
        // this.loading = true;
        // this.taxSubmissionService.get(params).subscribe((results: any) => {
        //     if (results.length > 0 && results[0].tax_incentive_id != null) {
        //         this.loading = false;
        //         this.router.navigate(['/pages/status-checker-detail', results[0].tax_incentive_id || null, 'tax']);
        //     } else {
        //         this.loading = false;
        //         this.router.navigateByUrl('pages/tax-incentive/choose-language');
        //     }
        // });
    }

    openLand() {
        this.router.navigateByUrl('pages/land-right/landright-choose-investment');
        // const params: any = {
        //     page: 1,
        //     search: `submitted_user_id:equal:${this.user.id}`,
        //     secure: true,
        // };
        // this.loading = true;
        // this.landSubmissionService.get(params).subscribe((results: any) => {
        //     if (results.length > 0 && results[0].land_right_id != null) {
        //         this.loading = false;
        //         this.router.navigate(['/pages/status-checker-detail', results[0].land_right_id || null, 'landright']);
        //     } else {
        //         this.loading = false;
        //         this.router.navigateByUrl('pages/land-right/landright-choose-investment');
        //     }
        // });
    }

}
