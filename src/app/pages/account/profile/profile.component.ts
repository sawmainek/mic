import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { AppState, Login, Register, UpdateUser } from 'src/app/core';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/auth/login/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  menu: any = 'info';
  user: any = {};
  loading = false;
  profile = '';
  imageSrc = '';
  username = '';
  token = '';
  lang = 'en';
  imgFile: any;
  error: any = {};

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private store: Store<AppState>,
    private translateService: TranslateService,
    private toastr: ToastrService
  ) 
  {    
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.loading = false;
    this.authService.getCurrentUser().subscribe((result: any) => {
      this.token = result.personal_access_token;
      this.userService.getUser(result.personal_access_token).subscribe(user => {
        this.loading = false;
        this.user = user;
        this.username = this.user.name;
        this.profile = this.user.avatar;
        this.imageSrc = this.user.avatar ? `https://mic-dica.apitoolz.com/img/${this.user.avatar}` : null;
      }, err => {
        this.loading = false;
        console.log('err ' + JSON.stringify(err));
      });
    });
  }

  checkValidateForm() {
    let valid = true;
    this.error = {};

    if (!this.user.name || this.user.name === '') {
      this.error.name = 'Name field is required.';
      valid = false;
    }

    if (!this.user.phone || this.user.phone === '') {
      this.error.phone = 'Phone field is required.';
      valid = false;
    }

    return valid;
  }

  submitUserInfo() {
    if (this.checkValidateForm()) {
      const formData: any = new FormData();
      formData.append('name', this.user.name ? this.user.name : '');
      formData.append('phone', this.user.phone ? this.user.phone : '');
      formData.append('avatar', this.imgFile ? this.imgFile : '');
      console.log(' formdata ' + formData);
      this.saveUserProfile(formData);
    }
  }

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      //alert('invalid File format');
      this.toastr.error('invalid File format');
      return;
    }

    if (e.target.files.length > 0) {
      this.imgFile = e.target.files[0];
    }

    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    let reader = e.target;
    this.imageSrc = reader.result;
  }

  removeImage() {
    this.user.avatar = '';
    this.imageSrc = '';
  }

  saveUserProfile(data: any) {
    this.loading = true;
    this.userService.editProfile(data, this.user.id, this.token).subscribe(result => {
      this.loading = false;
      result.personal_access_token = this.token;
      this.store.dispatch(new UpdateUser({ user: result }));
      this.getCurrentUser();
      this.toastr.success('Successfully Updated.');
    }, err => {
      this.loading = false;
      this.toastr.error(JSON.stringify(err.error.message));
      console.log(JSON.stringify(err));
    });
  }

}
