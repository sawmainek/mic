import { AuthService } from 'src/services/auth.service';
import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/auth/login/user.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  @Input() menu = 'info';
  loading = false;
  profile = '';
  imageSrc = '';
  user: any;
  username = '';
  lang = 'en';
  error: any = {};
  hasAdminRole: any;

  constructor(
    private authService: AuthService,
    private translateService: TranslateService,
    private userService: UserService
  ) 
  {     
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.loading = false;
    this.authService.getCurrentUser().subscribe((result: any) => {
      this.userService.getUser(result.personal_access_token).subscribe(user => {
        this.loading = false;
        this.user = user;
        this.username = this.user.name;
        this.profile = this.user.avatar ? `https://mic-dica.apitoolz.com/img/${this.user.avatar}` : null;
        this.imageSrc = this.user.avatar;
      }, err => {
        this.loading = false;
        console.log('err ' + JSON.stringify(err));
      });
    });
  }

  logout() {
    this.authService.logout();
  }

}
