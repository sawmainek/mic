import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AppState, Login, Register, UpdateUser } from 'src/app/core';
import { Store } from '@ngrx/store';
import { UserService } from 'src/app/auth/login/user.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit {

  menu: any = 'mail';
  user: any;
  loading = false;
  hide = true;

  // password form
  passwordForm: FormGroup;
  passwordSubmit = false;
  showPasswordForm = true;

  // password form
  emailForm: FormGroup;
  emailSubmit = false;
  showEmailForm = false;

  // password form
  otpForm: FormGroup;
  otpSubmit = false;
  showOtpForm = false;
  lang = 'en';

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private translateService: TranslateService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private toastr: ToastrService,
  ) 
  {
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
    this.getCurrentUser();
    this.setFormControl();
  }

  getCurrentUser() {
    this.loading = false;
    this.authService.getCurrentUser().subscribe((result: any) => {
      this.loading = false;
      this.user = result;
    });
  }

  setFormControl() {
    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.required]],
    });

    this.emailForm = this.formBuilder.group({
      email: [this.user.email, [Validators.required, Validators.email]],
    });

    this.otpForm = this.formBuilder.group({
      code: ['', [Validators.required, Validators.minLength(4)]],
    });
  }

  checkPasswordForm() {
    this.passwordSubmit = true;
    if (this.passwordForm.invalid) {
      return;
    } else {
      this.doLogin();
    }
  }

  get ControlPasswordForm() {
    return this.passwordForm.controls;
  }

  doLogin() {
    this.loading = true;
    const data: any = {
      email: this.user.email,
      password: this.passwordForm.value.password
    };
    this.userService.login(data).subscribe((user) => {
      this.loading = false;
      this.showPasswordForm = false;
      this.showEmailForm = true;
      this.showOtpForm = false;
      this.passwordSubmit = false;
    },
      error => {
        this.loading = false;
        this.toastr.error('Your password is incorrect.');
      });
  }

  checkEmailForm() {
    this.emailSubmit = true;
    if (this.emailForm.invalid) {
      return;
    } else {
      this.changeEmail(); // this.getVerificationCode()
    }
  }

  get ControlEmailForm() {
    return this.emailForm.controls;
  }

  getVerificationCode() {
    this.loading = true;
    const data: any = {
      email: this.emailForm.value.email,
      timeout: 60
    };
    this.userService.getVerifyToken(data).subscribe((user) => {
      this.loading = false;
      this.showPasswordForm = false;
      this.showEmailForm = false;
      this.showOtpForm = true;
      this.emailSubmit = false;
    },
      error => {
        this.loading = false;
        this.toastr.error(JSON.stringify(error.error.message));
      });
  }


  checkOtpForm() {
    this.otpSubmit = true;
    if (this.otpForm.invalid) {
      return;
    } else {
      this.changeEmail();
    }
  }

  get ControlOtpForm() {
    return this.otpForm.controls;
  }

  changeEmail() {
    this.loading = true;
    const data: any = {
      id: this.user.id,
      email: this.emailForm.value.email,
    };
    this.userService.edit(data, this.user.personal_access_token).subscribe((result) => {
      this.loading = false;
      this.showPasswordForm = false;
      this.showEmailForm = false;
      this.showOtpForm = false;
      this.emailSubmit = false;
      this.clickCancel();
      result.personal_access_token = this.user.personal_access_token;
      this.store.dispatch(new UpdateUser({ user: result }));
      this.getCurrentUser();
      this.toastr.success('Successfully Updated.');
    },
      error => {
        this.loading = false;
        this.toastr.error(JSON.stringify(error.error.message));
      });
  }

  clickCancel() {
    this.showEmailForm = false;
    this.showPasswordForm = true;
    this.passwordForm.reset();
    this.passwordSubmit = false;
  }

}
