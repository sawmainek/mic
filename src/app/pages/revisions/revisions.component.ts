import { LandRightPreviewService } from './../land-right/land-right-preview/land-right-preview.service';
import { TaxIncentivePreviewService } from './../tax-incentive/tax-incentive-preview/tax-incentive-preview.service';
import { EndorsementPreviewService } from './../endorsement/endorsement-preview/endorsement-preview.service';
import { MICFormPreviewService } from './../mic-permit/mic-preview/mic-preview.service';
import { InquiryPreviewService } from './../inquiry/inquiry-preview/inquiry-preview.service';
import { NotifyService } from './../../../services/notify.service';
import { FormLogService } from './../../../services/form-log.service';
import { FormCommentService } from './../../../services/form-comment.service';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { InquirySubmissionService } from 'src/services/inquiry-submission.service';
import { Location, DatePipe } from '@angular/common';
import { MicSubmissionService } from 'src/services/mic-submission.service';
import { EndorsementSubmissionService } from 'src/services/endorsement-submission.service';
import { TaxincentiveSubmissionService } from 'src/services/taxincentive-submission.service';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { BaseService } from 'src/services/base.service';

declare var jQuery: any;
@Component({
    selector: 'app-revisions',
    templateUrl: './revisions.component.html',
    styleUrls: ['./revisions.component.scss']
})
export class RevisionsComponent implements OnInit {
    user: any = {};
    id: any;
    type: any;
    lang: any;
    submission: any = {};
    tab: any = 'revision';
    loading: boolean = false;
    host: string = environment.host;
    submissionService: BaseService;
    displayedColumns: any[] = [];
    datatable: any;
    revisions: any[] = [];

    constructor(
        private router: Router,
        private authService: AuthService,

        private inquirySubmissionService: InquirySubmissionService,
        private micSubmissionService: MicSubmissionService,
        private endorSubmissionService: EndorsementSubmissionService,
        private landRightSubmissionService: LandrightSubmissionService,
        private taxSubmissionService: TaxincentiveSubmissionService,

        private inquiryPreviewService: InquiryPreviewService,
        private micPreviewService: MICFormPreviewService,
        private endoPreviewService: EndorsementPreviewService,
        private taxInvPreviewService: TaxIncentivePreviewService,
        private landRightPreviewService: LandRightPreviewService,

        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
        private formCommentService: FormCommentService,
        private formLogService: FormLogService,
        private notifyService: NotifyService,
        public location: Location,
    ) {
        this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
        this.translateService.use(this.lang ? this.lang : 'en');
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.type = paramMap.get('type') || null;

            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;

                this.displayedColumns = [
                    {
                        field: 'created_at',
                        title: 'Date',
                        sortable: true,
                        template: (row) => {
                            const datePipe = new DatePipe('en');
                            return datePipe.transform(row.created_at, 'MMM dd, yyyy hh:mm a');
                        }
                    },
                    {
                        field: 'comment',
                        title: 'Message',
                        sortable: true,
                        template: (row) => {
                            return row.comment || '' ;
                        }
                    },
                    {
                        field: 'user',
                        title: 'Commented By',
                        sortable: true,
                        template: (row) => {
                            return row?.user?.name || '';
                        }
                    },
                    {
                        field: 'action',
                        title: 'Action',
                        sortable: false,
                        template: (row) => {
                            return `<button class="btn btn-outline-warning btn-message" data-url="${row.page}">Revise</button>
                                        <button class="btn btn-revise ${row.revised ? 'btn-outline-success' : 'btn-outline-danger'}" data-id="${row.id}" data-revised="${row.revised}">Mark as ${row.revised ? 'Unrevise' : 'Revise'}</button>`;
                        }
                    }
                ];

                if (this.user) {
                    if (this.type == 'inquiry') {
                        this.submissionService = this.inquirySubmissionService;
                        this.getInquiry();
                    }
                    if (this.type == 'mic') {
                        this.submissionService = this.micSubmissionService;
                        this.getMIC();
                    }
                    if (this.type == 'endorsement') {
                        this.submissionService = this.endorSubmissionService;
                        this.getEndorsement();
                    }
                    if (this.type == 'tax') {
                        this.submissionService = this.taxSubmissionService;
                        this.getTax();
                    }
                    if (this.type == 'landright') {
                        this.submissionService = this.landRightSubmissionService;
                        this.getLandRight();
                    }
                }
            }, err => {
                console.log(err);
            });

        });
    }

    ngOnInit(): void { }

    initDatatable(dataJson) {
        let options: any = {
            data: {
                type: 'local',
                source: dataJson,
                saveState: {
                    cookie: false
                },
                pageSize: 10
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            }
        };
        if (this.datatable) { jQuery('.kt-datatable-form-revision').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-form-revision').KTDatatable(options);
        console.log(this.datatable)
        jQuery('.kt-datatable-form-revision').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;
            jQuery('.btn-message').on('click', function () {
                setTimeout(() => {
                    const url = jQuery(this).data('url');
                    ctx.gotoResolve(url);
                }, 300);
            });
            jQuery('.btn-revise').on('click', function () {
                setTimeout(() => {
                    const id = jQuery(this).data('id');
                    const revised = jQuery(this).data('revised');
                    ctx.makeRevised(id, revised);
                }, 300);
            });
        });
    }
    

    getInquiry() {
        this.inquirySubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "Inquiry (Form 1)";
            // For Revision
            if (this.submission?.allow_revision) {
                this.formCommentService.get({
                    search: `type:equal:Inquiry|type_id:equal:${this.submission?.form?.id || 0}|resolved:equal:false`,
                    rows: 999
                }).subscribe(results => {
                    this.revisions = results;
                    this.initDatatable(this.revisions);
                })
            }
        })
    }

    getMIC() {
        this.micSubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "MIC Permit Application ";
            // For Revision
            if (this.submission?.allow_revision) {
                forkJoin([
                    this.formCommentService.get({
                        search: `type:equal:MIC Permit|type_id:equal:${this.submission?.form?.data?.id || 0}|resolved:equal:false`,
                        rows: 999
                    }),
                    this.formCommentService.get({
                        search: `type:equal:MIC Permit|type_id:equal:${this.submission?.form?.data_mm?.id || 0}|resolved:equal:false`,
                        rows: 999
                    })
                ]).subscribe(results => {
                    this.revisions = [...results[0], ...results[1]];
                    this.initDatatable(this.revisions);
                })
            }
        })
    }

    getEndorsement() {
        this.endorSubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "Endorsement (Form 4ab)";
            // For Revision
            if (this.submission?.allow_revision) {
                forkJoin([
                    this.formCommentService.get({
                        search: `type:equal:Endorsement|type_id:equal:${this.submission?.form?.data?.id || 0}|resolved:equal:false`,
                        rows: 999
                    }),
                    this.formCommentService.get({
                        search: `type:equal:Endorsement|type_id:equal:${this.submission?.form?.data_mm?.id || 0}|resolved:equal:false`,
                        rows: 999
                    })
                ]).subscribe(results => {
                    this.revisions = [...results[0], ...results[1]];
                    this.initDatatable(this.revisions);
                })
            }
        });
    }

    getTax() {
        this.taxSubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "Tax Incentive Application Authorization (Form 6)";
            // For Revision
            if (this.submission?.allow_revision) {
                forkJoin([
                    this.formCommentService.get({
                        search: `type:equal:Tax Incentive|type_id:equal:${this.submission?.form?.data?.id || 0}|resolved:equal:false`,
                        rows: 999
                    }),
                    this.formCommentService.get({
                        search: `type:equal:Tax Incentive|type_id:equal:${this.submission?.form?.data_mm?.id || 0}|resolved:equal:false`,
                        rows: 999
                    })
                ]).subscribe(results => {
                    this.revisions = [...results[0], ...results[1]];
                    this.initDatatable(this.revisions);
                })
            }
        })
    }

    getLandRight() {
        this.landRightSubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "Land Rights Authorization (Form 7ab)";
            // For Revision
            if (this.submission?.allow_revision) {
                forkJoin([
                    this.formCommentService.get({
                        search: `type:equal:Land Right|type_id:equal:${this.submission?.form?.data?.id || 0}|resolved:equal:false`,
                        rows: 999
                    }),
                    this.formCommentService.get({
                        search: `type:equal:Land Right|type_id:equal:${this.submission?.form?.data_mm?.id || 0}|resolved:equal:false`,
                        rows: 999
                    })
                ]).subscribe(results => {
                    this.revisions = [...results[0], ...results[1]];
                    this.initDatatable(this.revisions);
                })
            }
        })
    }

    openTab(tab) {
        this.tab = tab;
    }

    gotoResolve(page) {
        let paths: string[] = page.split('/');
        if (paths.length >= 3) {
            paths[2] = this.submission?.form?.id;
            this.router.navigateByUrl(`${paths.join('/')}?action=Edit`);
        }
    }

    makeRevised(id, revised) {
        this.formCommentService.update(id, { revised: revised ? false : true}).subscribe(result => {
            let revision = this.revisions.filter(x => x.id == id)[0];
            revision.revised = result.revised;
            this.initDatatable(this.revisions);
        })
    }

    previewForm() {
        if (this.type == 'inquiry') {
            this.inquiryPreviewService.toFormPDF(this.submission?.form, { readOnly: true, language: 'en' });
        } else {
            if (this.type == 'mic') {
                this.micPreviewService.toFormPDF(this.submission?.form?.data_mm, { readOnly: true, language: 'en' });
            }
            if (this.type == 'endorsement') {
                this.endoPreviewService.toFormPDF(this.submission?.form?.data_mm, { readOnly: true, language: 'en' });
            }
            if (this.type == 'tax') {
                this.taxInvPreviewService.toFormPDF(this.submission?.form?.data_mm, { readOnly: true, language: 'en' });
            }
            if (this.type == 'landright') {
                this.landRightPreviewService.toFormPDF(this.submission?.form?.data_mm, { readOnly: true, language: 'en' });
            }
        }
    }

    resubmit() {
        this.loading = true;
        let formModel: any = {
            user_id: this.user.id,
            type_id : this.submission.id,
            status : "Revision",
            sub_status: "Submission",
            data : this.submission.form,
        };

        if (this.type == 'inquiry') {
            formModel.type = "Inquiry ( Form 1 )";
            formModel.form_id = this.submission?.inquiry_id;
            formModel.url = `/pages/inquiry/success`;
            formModel.message = `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/inquiry/form/${this.submission.inquiry_id}">${this.submission?.application_no}</a> for new submission.`;
        }
        if (this.type == 'mic') {
            formModel.type = "MIC-Permit";
            formModel.form_id = this.submission?.mic_id;
            formModel.url = `/pages/mic/success`;
            formModel.message = `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/mic/choose-language/${this.submission.mic_id}">${this.submission?.application_no}</a> for new submission.`;
        }
        if (this.type == 'endorsement') {
            formModel.type = "Endorsement";
            formModel.form_id = this.submission?.endorsement_id;
            formModel.url = `/pages/endorsement/success`;
            formModel.message = `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/endorsement/form/${this.submission.endorsement_id}">${this.submission?.application_no}</a> for new submission.`;
        }
        if (this.type == 'tax') {
            formModel.type = "Tax Incentive";
            formModel.form_id = this.submission?.tax_incentive_id;
            formModel.url = `/pages/tax-incentive/success`;
            formModel.message = `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/tax-incentive/choose-language/${this.submission.tax_incentive_id}">${this.submission?.application_no}</a> for new submission.`;
        }
        if (this.type == 'landright') {
            formModel.type = "Land Right";
            formModel.form_id = this.submission?.land_right_id;
            formModel.url = `/pages/land-right/success`;
            formModel.message = `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/land-right/landright-choose-investment/${this.submission.land_right_id}">${this.submission?.application_no}</a> for new submission.`;
        }
        this.submissionService.update(this.submission.id, {
            'sub_status': 'New',
            'allow_revision': false
        }).subscribe(result => {
            this.loading = false;
            // Save to Form Data/Status Log Model;
            this.formLogService.create({
                type: formModel.type,
                type_id: formModel.type_id,
                status: formModel.status,
                sub_status: formModel.sub_status,
                submitted_user_id: formModel.user_id,
                reply_user_id: this.submission?.reply_user_id,
                incoming: true,
                extra: {
                    application_no: this.submission?.application_no,
                    form_id: formModel.form_id,
                    history_data: formModel.data
                }
            }).subscribe();
            if (this.submission?.reply_user_id) {
                this.notifyService.create({
                    user_id: this.submission?.reply_user_id,
                    subject: `${formModel.status} ${formModel.type}: # ${this.submission?.application_no} received.`,
                    message: formModel.message
                }).subscribe();
            }
            this.router.navigate([formModel.url, this.id]);
        })
        
    }

}
