import { EndorsementSectionDForm2 } from './../sectionD/endorsement-section-d-form2/endorsement-section-d-form2';
import { Injectable } from '@angular/core';
import { EndorsementSectionEForm4 } from './../sectionE/endorsement-section-e-form4/endorsement-section-e-form4';
import { EndorsementSectionEForm3 } from './../sectionE/endorsement-section-e-form3/endorsement-section-e-form3';
import { EndorsementSectionEForm2 } from './../sectionE/endorsement-section-e-form2/endorsement-section-e-form2';
import { EndorsementSectionDForm7 } from './../sectionD/endorsement-section-d-form7/endorsement-section-d-form7';
import { EndorsementSectionDForm6 } from './../sectionD/endorsement-section-d-form6/endorsement-section-d-form6';
import { EndorsementSectionDForm5 } from './../sectionD/endorsement-section-d-form5/endorsement-section-d-form5';
import { EndorsementSectionDForm4 } from './../sectionD/endorsement-section-d-form4/endorsement-section-d-form4';
import { EndorsementSectionDForm3 } from './../sectionD/endorsement-section-d-form3/endorsement-section-d-form3';
import { EndorsementSectionCForm9 } from './../sectionC/endorsement-section-c-form9/endorsement-section-c-form9';
import { EndorsementSectionCForm8 } from './../sectionC/endorsement-section-c-form8/endorsement-section-c-form8';
import { EndorsementSectionCForm7 } from './../sectionC/endorsement-section-c-form7/endorsement-section-c-form7';
import { EndorsementSectionCForm6 } from './../sectionC/endorsement-section-c-form6/endorsement-section-c-form6';
import { EndorsementSectionCForm5 } from './../sectionC/endorsement-section-c-form5/endorsement-section-c-form5';
import { EndorsementSectionCForm4 } from './../sectionC/endorsement-section-c-form4/endorsement-section-c-form4';
import { EndorsementSectionCForm3 } from './../sectionC/endorsement-section-c-form3/endorsement-section-c-form3';
import { EndorsementSectionCForm2 } from './../sectionC/endorsement-section-c-form2/endorsement-section-c-form2';
import { EndorsementSectionBForm3 } from './../sectionB/endorsement-section-b-form3/endorsement-section-b-form3';
import { EndorsementSectionBForm2 } from './../sectionB/endorsement-section-b-form2/endorsement-section-b-form2';
import { EndorsementSectionAForm4 } from './../sectionA/endorsement-section-a-form4/endorsement-section-a-form4';
import { EndorsementSectionAForm3 } from './../sectionA/endorsement-section-a-form3/endorsement-section-a-form3';
import { EndorsementSectionAForm2 } from './../sectionA/endorsement-section-a-form2/endorsement-section-a-form2';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { PrintPDFService } from './../../../custom-component/print.pdf.service';
import { CPCService } from './../../../../services/cpc.service';
import { ISICService } from './../../../../services/isic.service';
import { LocalService } from './../../../../services/local.service';
import { LocationService } from './../../../../services/location.service';
import { EndorsementSectionBForm4 } from '../sectionB/endorsement-section-b-form4/endorsement-section-b-form4';

@Injectable()
export class EndorsementPreviewService {
    forms: BaseForm<string | boolean>[] = [];
    constructor(
        public lotService: LocationService,
        public localService: LocalService,
        public isicService: ISICService,
        public cpcService: CPCService,
        public printPDFService: PrintPDFService
    ) {

    }

    toFormPDF(data: any, option: {
        readOnly?: boolean,
        language?: string,
        onSubmit$?: any
    }) {
        const sectionAForm4 = new EndorsementSectionAForm4(option.language, this.lotService, this.localService);
        const sectionBForm2 = new EndorsementSectionBForm2();
        const sectionBForm3 = new EndorsementSectionBForm3(option.language, this.lotService, this.localService, this.isicService, this.cpcService, 'Yes');
        const sectionBForm4 = new EndorsementSectionBForm4(option.language, this.lotService, this.localService);
        const sectionCForm2 = new EndorsementSectionCForm2(this.localService);
        const sectionCForm3 = new EndorsementSectionCForm3();
        const sectionCForm4 = new EndorsementSectionCForm4(option.language, this.lotService);
        const sectionCForm5 = new EndorsementSectionCForm5(this.lotService);
        const sectionCForm6 = new EndorsementSectionCForm6(this.lotService);
        const sectionCForm7 = new EndorsementSectionCForm7(this.lotService); //Need to copy from FORM 2
        const sectionCForm8 = new EndorsementSectionCForm8(option.language, this.lotService, this.localService);
        const sectionCForm9 = new EndorsementSectionCForm9();
        const sectionDForm2 = new EndorsementSectionDForm2();
        const sectionDForm3 = new EndorsementSectionDForm3(option.language, this.lotService, this.localService);
        const sectionDForm4 = new EndorsementSectionDForm4(this.lotService);
        const sectionDForm5 = new EndorsementSectionDForm5(this.localService);
        const sectionDForm6 = new EndorsementSectionDForm6(this.localService);
        const sectionDForm7 = new EndorsementSectionDForm7(this.lotService, this.localService);
        const sectionEForm2 = new EndorsementSectionEForm2();
        const sectionEForm3 = new EndorsementSectionEForm3();
        const sectionEForm4 = new EndorsementSectionEForm4(this.lotService);

        sectionBForm2.forms[0]['formGroup'][0].value = data?.company_info?.overview_is_established || "Yes";
        sectionBForm2.forms[0]['formGroup'][1].value = data?.apply_to || "Myanmar Investment Commision";

        let expected_investmentdt = data['expected_investmentdt'] || null;
        let cash_equivalents: any[] = [];
        if (expected_investmentdt) {
            cash_equivalents.push({
                currency: 'Myanmar',
                bank_balances: expected_investmentdt.cash_equivalent[0].myan_origin,
                cash_on_hand: expected_investmentdt.cash_equivalent[1].myan_origin,
                deposits: expected_investmentdt.cash_equivalent[2].myan_origin
            });
            expected_investmentdt.cash_equivalent[0].foreign_origin_list.forEach((origin, index) => {
                cash_equivalents.push({
                    currency: origin.currency,
                    bank_balances: expected_investmentdt.cash_equivalent[0]['foreign_origin_list'][index]['amount'],
                    cash_on_hand: expected_investmentdt.cash_equivalent[1]['foreign_origin_list'][index]['amount'],
                    deposits: expected_investmentdt.cash_equivalent[2]['foreign_origin_list'][index]['amount'],
                });
            });
        }
        data['expected_investmentdt']['cash_equivalents'] = cash_equivalents;

        data['timeline']['construction_date_no'] = data.timeline?.construction_date_no + ' ' + data.timeline?.construction_date_label;
        data['timeline']['construction_date_label'] = '';

        data['timeline']['commercial_date_no'] = data.timeline?.commercial_date_no + ' ' + data.timeline?.commercial_date_label;
        data['timeline']['commercial_date_label'] = '';

        data['timeline']['investment_start_date'] = data.timeline?.investment_start_date + ' Years';
        
        data.company_info.is_preview = 'Yes';
        if (data?.apply_to == 'Myanmar Investment Commision') {
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'business_sector_label')[0].label = "c. Specific sector of the proposed investment:";
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'business_cpc_label')[0].label = "d. Optional: please name the specific product(s)/service(s) of the investment:";
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'invest_description')[0].label = "e. Please give a description of the nature of the proposed activities of the investment, including details on the products/services and production and sales strategy of the investment business";
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'invest_benefit')[0].label = "f. Proposed investment’s supply chain and benefits to the other related businesses";
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'contact_label')[0].label = "g. Contact Person";
        }
        else {
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'business_sector_label')[0].label = "b. Specific sector of the proposed investment:";
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'business_cpc_label')[0].label = "c. Optional: please name the specific product(s)/service(s) of the investment:";
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'invest_description')[0].label = "d. Please give a description of the nature of the proposed activities of the investment, including details on the products/services and production and sales strategy of the investment business";
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'invest_benefit')[0].label = "e. Proposed investment’s supply chain and benefits to the other related businesses";
            sectionBForm3.forms[0].formGroup.filter(x => x.key == 'contact_label')[0].label = "f. Contact Person";
        }

        this.forms = [
            ...sectionAForm4.forms,
            ...sectionBForm2.forms,
            ...sectionBForm3.forms,
            ...sectionBForm4.forms,
            ...sectionCForm2.forms,
            ...sectionCForm3.forms,
            ...sectionCForm4.forms,
            ...sectionCForm5.forms,
            // ...sectionCForm6.forms, Removed from DICA
            ...sectionCForm7.forms,
            ...sectionCForm8.forms,
            ...sectionCForm9.forms,
            ...sectionDForm2.forms,
            ...sectionDForm3.forms,
            ...sectionDForm4.forms,
            ...sectionDForm5.forms,
            ...sectionDForm6.forms,
            ...sectionDForm7.forms,
            ...sectionEForm2.forms,
            ...sectionEForm3.forms,
            ...sectionEForm4.forms,
        ];
        this.printPDFService.toFormPDF(this.forms, data, 'Endorsement (Form 4ab)', option);

    }
}