import { FormCheckBox } from './../../../../custom-component/dynamic-forms/base/form-checkbox';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { FormTitle } from './../../../../custom-component/dynamic-forms/base/form-title';
import { ToastrService } from 'ngx-toastr';
import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators } from '@angular/forms';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementSectionE } from '../endorsement-section-e';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { LocationService } from 'src/services/location.service';

@Component({
    selector: 'app-endorsement-section-e-form4',
    templateUrl: './endorsement-section-e-form4.component.html',
    styleUrls: ['./endorsement-section-e-form4.component.scss']
})
export class EndorsementSectionEForm4Component extends EndorsementSectionE implements OnInit {
    menu: any = "sectionE";
    page = "endorsement/sectionE-form4/";
    @Input() id: any;
    mm: any;

    user: any = {};

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    endoForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '3. If an authorized representative fill in the application on behalf of the investor(s)?'
        }),
        new FormRadio({
            key: 'is_authorized',
            label: 'Yes',
            value: 'Yes',
            required: true,
            columns: 'col-md-4',
        }),
        new FormRadio({
            key: 'is_authorized',
            label: 'No ( The investor(s) filled in the application by themselves)',
            value: 'No',
            required: true,
            columns: 'col-md-8',
        }),
        new FormInput({
            label: 'a. Name of company, if applicable',
            key: 'company_name',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormTitle({
            label: 'b. Address of company, if applicable',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormSelect({
            key: 'company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            value: 'Myanmar',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormSelect({
            key: 'company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'company_address',
            label: 'Additional address details',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormSectionTitle({
            label: 'c. Contact person',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            label: 'Full Name',
            key: 'full_name',
            columns: 'col-md-6',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            label: 'Position',
            key: 'position',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            label: 'Phone number',
            key: 'phone_no',
            required: true,
            type: 'number',
            columns: 'col-md-6',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            label: 'E-mail address',
            key: 'email',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            label: 'Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
            key: 'passport_nrc',
            required: true,
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormSelect({
            key: 'citizenship',
            label: 'Citizenship',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormTitle({
            label: "Please upload an official letter of legal representation:",
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormFile({
            label: 'Name of Document',
            key: 'official_letter_docs',
            required: true,
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        })
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        public location: Location,
        private store: Store<AppState>,
        private toast: ToastrService,
        public formCtrService: FormControlService,
        private translateService: TranslateService,
        private lotService: LocationService
    ) {
        super(formCtrService);
        this.loading = true;
        this.formGroup = this.formCtrService.toFormGroup(this.endoForm);
        this.getEndorsementData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getEndorsementData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.is_authorized = this.endoModel?.is_authorized || form?.data?.is_authorized || {};
                        this.formGroup = this.formCtrService.toFormGroup(this.endoForm, this.endoModel?.is_authorized);

                        this.page = "endorsement/sectionE-form4/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));

        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtrService.lazyUpload(this.endoForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let data = { 'is_authorized': formValue };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        this.spinner = false;
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/endorsement-certificate', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/undertaking', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionE-form4', id, 'mm']);
                }
            }
        }
    }
}
