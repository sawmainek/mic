import { FormCheckBox } from './../../../../custom-component/dynamic-forms/base/form-checkbox';
import { FormTitle } from './../../../../custom-component/dynamic-forms/base/form-title';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocationService } from 'src/services/location.service';

export class EndorsementSectionEForm4{
    
    public forms: BaseForm<string>[] = []

    constructor(
        private lotService: LocationService
    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'is_authorized',
                formGroup: [
                    new FormSectionTitle({
                        label: '3. If an authorized representative fill in the application on behalf of the investor(s)?'
                    }),
                    new FormRadio({
                        key: 'is_authorized',
                        label: 'Yes',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-4',
                    }),
                    new FormRadio({
                        key: 'is_authorized',
                        label: 'No ( The investor(s) filled in the application by themselves)',
                        value: 'No',
                        required: true,
                        columns: 'col-md-8',
                    }),
                    new FormInput({
                        label: 'a. Name of company, if applicable',
                        key: 'company_name',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormTitle({
                        label: 'b. Address of company, if applicable',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'company_country',
                        label: 'Country',
                        options$: this.lotService.getCountry(),
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'company_state',
                        label: 'State / Region:',
                        options$: this.lotService.getState(),
                        required: true,
                        columns: 'col-md-4',
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'company_district',
                        label: 'District:',
                        options$: this.lotService.getDistrict(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'company_state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'company_township',
                        label: 'Township:',
                        options$: this.lotService.getTownship(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'company_district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormInput({
                        key: 'company_address',
                        label: 'Additional address details',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),  
                    new FormSectionTitle({
                        label: 'c. Contact person',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        label: 'Full Name',
                        key: 'full_name',
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        label: 'Position',
                        key: 'position',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        label: 'Phone number',
                        key: 'phone_no',
                        required: true,
                        type: 'number',
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        label: 'E-mail address',
                        key: 'email',
                        required: true,
                        validators: [Validators.email],
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        label: 'Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
                        key: 'passport_nrc',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'citizenship',
                        label: 'Citizenship',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormTitle({
                        label: "Please upload an official letter of legal representation:",
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormFile({
                        label: 'Name of Document',
                        key: 'official_letter_docs',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    })
                ]
            }),
            new BaseFormGroup({
                key: 'undertaking',
                formGroup: [
                    new FormTitle({
                        label: 'By the submitting this application:'
                    }),
                    new FormCheckBox({
                        key: 'is_true',
                        required: true,
                        label: 'I / We hereby declare that the above statements are true and correct to the best of my/our knowledge and belief.'
                    }),
                    new FormCheckBox({
                        key: 'is_understand',
                        required: true,
                        label: 'I /We fully understand that proposal may be denied or unnecessarily delayed if the applicant fails to provide required information to the Commission for issuance of the permit.'
                    }),
                    new FormCheckBox({
                        key: 'is_strictly_comply',
                        required: true,
                        label: 'I/We hereby declare to strictly comply with terms and conditions set out by the Myanmar Investment Commission.'
                    }),
                ]
            })
            
        ];
    }
}