import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionEForm4Component } from './endorsement-section-e-form4.component';

describe('EndorsementSectionEForm4Component', () => {
  let component: EndorsementSectionEForm4Component;
  let fixture: ComponentFixture<EndorsementSectionEForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionEForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionEForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
