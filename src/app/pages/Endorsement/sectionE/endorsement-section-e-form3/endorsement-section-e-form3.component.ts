import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EndorsementService } from 'src/services/endorsement.service';
import { NavExtrasService } from 'src/app/_helper/nav-extras.service';
declare var jQuery: any;
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { EndorsementSectionE } from '../endorsement-section-e';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-endorsement-section-e-form3',
    templateUrl: './endorsement-section-e-form3.component.html',
    styleUrls: ['./endorsement-section-e-form3.component.scss']
})
export class EndorsementSectionEForm3Component extends EndorsementSectionE implements OnInit {
    menu: any = "sectionE";
    page = "endorsement/sectionE-form3/";
    @Input() id: any;
    mm: any;

    user: any = {};

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    endoForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Please upload any other recommendations, approvals, licenses, permits and similar authorized documents (which have been received so far), which have not been uploaded yet and are required for the operation of the investment:'
        }),
        new BaseFormArray({
            key: 'other_attach_docs',
            formArray: [
                new FormFile({
                    key: 'document',
                    label: 'Name of document:',
                    required: true,
                    multiple: true,
                })
            ]
        }),
        new FormNote({
            label: 'According the Myanmar Investment Rule 69, documents which can only be obtained in the implementation process of the investment (after receiving the endorsement) can be submitted later.'
        }),
        new FormDummy({
            key: 'csr_plan_docs'
        }),
        new FormDummy({
            key: 'fire_prevention_plan_docs'
        }),
        new FormDummy({
            key: 'cmp_contract_docs'
        }),
        new FormDummy({
            key: 'service_plan_docs'
        }),
        new FormDummy({
            key: 'sale_plan_docs'
        }),
        new FormDummy({
            key: 'construction_materials_docs'
        }),
        new FormDummy({
            key: 'permit_president'
        }),
        new FormDummy({
            key: 'commitment_statement_docs'
        }),
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        public location: Location,
        private toast: ToastrService,
        private store: Store<AppState>,
        public formCtrService: FormControlService,
        private translateService: TranslateService
    ) {
        super(formCtrService);
        this.loading = true;

        this.formGroup = this.formCtrService.toFormGroup(this.endoForm);
        this.getEndorsementData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getEndorsementData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.other_attachments = this.endoModel?.other_attachments?.other_attach_docs.length > 0 ? this.endoModel.other_attachments : form?.data?.other_attachments;

                        // this.endoModel.other_attachments = this.endoModel?.other_attachments || form?.data?.other_attachments || {};
                        this.formGroup = this.formCtrService.toFormGroup(this.endoForm, this.endoModel?.other_attachments || [], form?.data?.other_attachments || []);

                        this.page = "endorsement/sectionE-form3/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtrService.lazyUpload(this.endoForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let data = { 'other_attachments': formValue };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionE-form4', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionE-form4', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionE-form3', id, 'mm']);
                }
            }
        }
    }
}
