import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class EndorsementSectionEForm3{
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'other_attachments',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Please upload any other recommendations, approvals, licenses, permits and similar authorized documents (which have been received so far), which have not been uploaded yet and are required for the operation of the investment:'
                    }),
                    new BaseFormArray({
                        key: 'other_attach_docs',
                        formArray: [
                            new FormFile({
                                key: 'document',
                                label: 'Name of document:',
                                required: true,
                                multiple: true,
                            })
                        ]
                    }),
                    new FormNote({
                        label: 'According the Myanmar Investment Rule 69, documents which can only be obtained in the implementation process of the investment (after receiving the endorsement) can be submitted later.'
                    }),
                    new FormDummy({
                        key: 'csr_plan_docs'
                    }),
                    new FormDummy({
                        key: 'fire_prevention_plan_docs'
                    }),
                    new FormDummy({
                        key: 'cmp_contract_docs'
                    }),
                    new FormDummy({
                        key: 'service_plan_docs'
                    }),
                    new FormDummy({
                        key: 'sale_plan_docs'
                    }),
                    new FormDummy({
                        key: 'construction_materials_docs'
                    }),
                    new FormDummy({
                        key: 'permit_president'
                    }),
                    new FormDummy({
                        key: 'commitment_statement_docs'
                    })
                ]
            })
            
        ];
    }
}