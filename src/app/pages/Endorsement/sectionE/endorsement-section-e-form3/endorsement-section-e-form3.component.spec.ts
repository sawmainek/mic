import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionEForm3Component } from './endorsement-section-e-form3.component';

describe('EndorsementSectionEForm3Component', () => {
  let component: EndorsementSectionEForm3Component;
  let fixture: ComponentFixture<EndorsementSectionEForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionEForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionEForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
