import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementProgress } from '../endorsement_progress';

export class EndorsementSectionE extends EndorsementProgress {
    static section = "Section E";
    static totalForm = 3;
    totalForm = 3;
    constructor(public formCtlService: FormControlService) {
        super();
    }

    saveFormProgress(form_id: number, id: number, page: string) {
        if (this.progressForms?.length > 0) {
            if (this.progressForms.filter(x => x.type == EndorsementProgress.type && x.section == EndorsementSectionE.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: EndorsementProgress.type,
                    type_id: id,
                    section: EndorsementSectionE.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }

        }
        else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: EndorsementSectionE.type,
                type_id: id,
                section: EndorsementSectionE.section,
                page: page,
                status: true,
                revise: false
            });
        }
    }
}