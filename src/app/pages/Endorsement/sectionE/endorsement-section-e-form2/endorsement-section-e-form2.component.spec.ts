import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionEForm2Component } from './endorsement-section-e-form2.component';

describe('EndorsementSectionEForm2Component', () => {
  let component: EndorsementSectionEForm2Component;
  let fixture: ComponentFixture<EndorsementSectionEForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionEForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionEForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
