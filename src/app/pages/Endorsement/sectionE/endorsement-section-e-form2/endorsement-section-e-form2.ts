import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class EndorsementSectionEForm2{
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'other_attachments',
                formGroup: [
                    new FormSectionTitle({
                        label: '1. Please upload the following documents:'
                    }),
                    // new FormTitle({
                    //     label: 'List of machinery and equipment (items should be listed by domestic/foreign origin; a 4-digit HS code; and whether they are reconditioned or brand new)'
                    // }),
                    // new FormFile({
                    //     key: 'machinery_equipment',
                    //     label: 'File Name',
                    //     required: true
                    // }),
                    // new FormTitle({
                    //     label: 'List of raw materials (items should be listed by domestic/foreign origin and a 4-digit HS code; and whether they are reconditioned or brand new)'
                    // }),
                    // new FormFile({
                    //     key: 'raw_materials',
                    //     label: 'File Name',
                    //     required: true
                    // }),
                    new FormTitle({
                        label: 'Corporate social responsibility (CSR) plan:'
                    }),
                    new FormFile({
                        key: 'csr_plan_docs',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Fire prevention plan:'
                    }),
                    new FormFile({
                        key: 'fire_prevention_plan_docs',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'CMP contract, if applicable:'
                    }),
                    new FormFile({
                        key: 'cmp_contract_docs',
                        label: 'File Name',
                    }),
                    new FormTitle({
                        label: 'Annual service plan (including estimation of annual service targets), if enterprise is a services business:'
                    }),
                    new FormFile({
                        key: 'service_plan_docs',
                        label: 'File Name',
                    }),
                    new FormTitle({
                        label: 'Annual production and sales (export and domestic sales) plan, if enterprise produces goods:'
                    }),
                    new FormFile({
                        key: 'sale_plan_docs',
                        label: 'File Name',
                    }),
                    new FormTitle({
                        label: "List of construction materials (items should be listed by domestic/foreign origin and a 4-digit HS code), if constructing in the sectors of Hotel & Tourism, Real Estate Development, Mining, Oil & Gas, Power or other services:"
                    }),
                    new FormFile({
                        key: 'construction_materials_docs',
                        label: 'File Name'
                    }),
                    // new FormTitle({
                    //     label: "If relevant, Permits of the President's Office / Union Government concerning the operation on the land of relevant Government Department or Ministry:"
                    // }),
                    // new FormFile({
                    //     key: 'permit_president',
                    //     label: 'File Name',
                    // }),
                    new FormTitle({
                        label: "Commitment statement on payment of income tax on staff salaries"
                    }),
                    new FormFile({
                        key: 'commitment_statement_docs',
                        label: 'File Name',
                        required: true
                    }),
                    new FormDummy({
                        key: 'other_attach_docs'
                    })
                ]
            })
            
        ];
    }
}