import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementSectionE } from '../endorsement-section-e';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';


@Component({
    selector: 'app-endorsement-section-e-form2',
    templateUrl: './endorsement-section-e-form2.component.html',
    styleUrls: ['./endorsement-section-e-form2.component.scss']
})
export class EndorsementSectionEForm2Component extends EndorsementSectionE implements OnInit {
    menu: any = "sectionE";
    page = "endorsement/sectionE-form2/";
    @Input() id: any;
    mm: any;

    user: any = {};

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;
    other_attach_docs: any = [];

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    endoForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '1. Please upload the following documents:'
        }),


        // new FormTitle({
        //     label: 'List of machinery and equipment (items should be listed by domestic/foreign origin; a 4-digit HS code; and whether they are reconditioned or brand new)'
        // }),
        // new FormFile({
        //     key: 'machinery_equipment',
        //     label: 'File Name',
        //     required: true
        // }),
        // new FormTitle({
        //     label: 'List of raw materials (items should be listed by domestic/foreign origin and a 4-digit HS code; and whether they are reconditioned or brand new)'
        // }),
        // new FormFile({
        //     key: 'raw_materials',
        //     label: 'File Name',
        //     required: true
        // }),
        new FormTitle({
            label: 'Corporate social responsibility (CSR) plan:'
        }),
        new FormFile({
            key: 'csr_plan_docs',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'Fire prevention plan:'
        }),
        new FormFile({
            key: 'fire_prevention_plan_docs',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'CMP contract, if applicable:'
        }),
        new FormFile({
            key: 'cmp_contract_docs',
            label: 'File Name',
        }),
        new FormTitle({
            label: 'Annual service plan (including estimation of annual service targets), if enterprise is a services business:'
        }),
        new FormFile({
            key: 'service_plan_docs',
            label: 'File Name',
        }),
        new FormTitle({
            label: 'Annual production and sales (export and domestic sales) plan, if enterprise produces goods:'
        }),
        new FormFile({
            key: 'sale_plan_docs',
            label: 'File Name',
        }),
        new FormTitle({
            label: "List of construction materials (items should be listed by domestic/foreign origin and a 4-digit HS code), if constructing in the sectors of Hotel & Tourism, Real Estate Development, Mining, Oil & Gas, Power or other services:"
        }),
        new FormFile({
            key: 'construction_materials_docs',
            label: 'File Name'
        }),
        // new FormTitle({
        //     label: "If relevant, Permits of the President's Office / Union Government concerning the operation on the land of relevant Government Department or Ministry:"
        // }),
        // new FormFile({
        //     key: 'permit_president',
        //     label: 'File Name',
        // }),
        new FormTitle({
            label: "Commitment statement on payment of income tax on staff salaries"
        }),
        new FormFile({
            key: 'commitment_statement_docs',
            label: 'File Name',
            required: true
        }),
        new FormDummy({
            key: 'other_attach_docs'
        })
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        public location: Location,
        private store: Store<AppState>,
        private toast: ToastrService,
        public formCtrService: FormControlService,
        private translateService: TranslateService,
    ) {
        super(formCtrService);
        this.loading = true;
        this.formGroup = this.formCtrService.toFormGroup(this.endoForm);
        this.getEndorsementData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getEndorsementData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.other_attachments = this.endoModel?.other_attachments || form?.data?.other_attachments || {};
                        this.other_attach_docs = this.endoModel.other_attachments?.other_attach_docs || [];
                        this.formGroup = this.formCtlService.toFormGroup(this.endoForm, this.endoModel.other_attachments);

                        this.page = "endorsement/sectionE-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtrService.lazyUpload(this.endoForm, this.formGroup, { type_id: this.id }, (formValue) => {
            formValue['other_attach_docs'] = this.other_attach_docs;
            let data = { 'other_attachments': formValue };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionE-form3', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionE-form3', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionE-form2', id, 'mm']);
                }
            }
        }
    }
}
