import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionEForm1Component } from './endorsement-section-e-form1.component';

describe('EndorsementSectionEForm1Component', () => {
  let component: EndorsementSectionEForm1Component;
  let fixture: ComponentFixture<EndorsementSectionEForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionEForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionEForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
