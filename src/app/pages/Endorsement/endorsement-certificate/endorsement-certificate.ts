import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementProgress } from '../endorsement_progress';

export class EndorsementCertificate extends EndorsementProgress {
    static section = "Certificate";
    static totalForm = 1;
    constructor(public formCtlService: FormControlService) {
        super();
    }

    saveFormProgress(form_id: number, id: number, page: string) {
        if (this.progressForms?.length > 0) {
            if (this.progressForms.filter(x => x.type == EndorsementProgress.type && x.section == EndorsementCertificate.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: EndorsementProgress.type,
                    type_id: id,
                    section: EndorsementCertificate.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }

        }
        else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: EndorsementCertificate.type,
                type_id: id,
                section: EndorsementCertificate.section,
                page: page,
                status: true,
                revise: false
            });
        }
    }
}