import { EndorsementCerticateForm } from './endorsement-certificate-form';
import { LocationService } from './../../../../services/location.service';
import { ISICService } from './../../../../services/isic.service';
import { ToastrService } from 'ngx-toastr';
import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementCertificate } from './endorsement-certificate';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { LocalService } from 'src/services/local.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-endorsement-certificate',
    templateUrl: './endorsement-certificate.component.html',
    styleUrls: ['./endorsement-certificate.component.scss']
})
export class EndorsementCertificateComponent extends EndorsementCertificate implements OnInit {
    @Input() id: any;
    menu = 'certificate';
    page = "endorsement/endorsement-certificate/";
    mm: string = 'en';

    user: any = {};
    business_sectors: any;

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    baseForm: EndorsementCerticateForm;

    isRevision: boolean = false;

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        public isicService: ISICService,
        private lotService: LocationService,
        private localService: LocalService,
        private translateService: TranslateService
    ) {
        super(formCtrService);
        this.loading = true;

        this.getEndorsementData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getEndorsementData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.translateService.use(this.mm || 'en');
            this.baseForm = new EndorsementCerticateForm(this.mm || 'en', this.isicService, this.lotService, this.localService);
            this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms);
            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form.data_mm || {} : form.data || {};

                        this.endoModel.permit = this.endoModel?.permit || {};
                        this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms, this.endoModel.permit);

                        this.page = `endorsement/endorsement-certificate/${this.endoModel.id}/mm`;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });


                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }


    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtrService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue) => {
            let data = { 'permit': formValue };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        })
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/undertaking', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/undertaking', id, 'mm']);
                }
                else {
                    this.router.navigate([page + '/endorsement/endorsement-certificate', id, 'mm']);
                }
            }
        }

    }

}
