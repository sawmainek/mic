import { FormDate } from './../../../custom-component/dynamic-forms/base/form.date';
import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { LocalService } from './../../../../services/local.service';
import { FormRadio } from './../../../custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitleCounter } from './../../../custom-component/dynamic-forms/base/form-section-title-counter';
import { LocationService } from 'src/services/location.service';
import { FormInput } from './../../../custom-component/dynamic-forms/base/form-input';
import { ISICService } from './../../../../services/isic.service';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
export class EndorsementCerticateForm {

    public forms: BaseForm<string>[] = []
    constructor(
        private lang: string,
        private isicService: ISICService,
        private lotService: LocationService,
        private localService: LocalService
    ) {
        let businessSector: BaseForm < any > [] =[
            new FormSelect({
                key: 'isic_section_id',
                label: 'Choose business sections ( ISIC Section )',
                options$: this.isicService.getSection(),
                required: true
            }),
            new FormSelect({
                key: 'isic_division_id',
                label: 'Choose business divisions ( ISIC Division )',
                options$: this.isicService.getDivison(),
                filter: {
                    parent: 'isic_section_id',
                    key: 'section'
                },
                required: true,
            }),
            new FormSelect({
                key: 'isic_group_id',
                label: 'Choose business groups ( ISIC Group )',
                options$: this.isicService.getGroup(),
                filter: {
                    parent: 'isic_division_id',
                    key: 'division'
                },
                required: true,
            }),
            new FormSelect({
                key: 'isic_class_ids',
                label: 'Choose business class(es)',
                required: true,
                multiple: true,
                options$: this.isicService.getClasses(this.lang),
                filter: {
                    parent: 'isic_group_id',
                    key: 'group'
                },
            })
        ];

        let permit_location: BaseForm < string > [] =[
            new FormInput({
                key: 'no',
                label: 'Location',
                columns: 'col-md-6',
                required: true,
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region',
                columns: 'col-md-6',
                options$: this.lotService.getState(this.lang),
                required: true
            }),
            new FormSelect({
                key: 'district',
                label: 'District',
                columns: 'col-md-6',
                options$: this.lotService.getDistrict(this.lang),
                required: true,
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township',
                columns: 'col-md-6',
                options$: this.lotService.getTownship(this.lang),
                required: true,
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
        ]

        let permit_investor: BaseForm < string > [] =[
            new FormSectionTitleCounter({
                label: 'Investor'
            }),
            new FormInput({
                key: 'name',
                label: 'a. Name ',
                required: true
            }),
            new FormTitle({
                label: 'b. Address'
            }),           
            new FormSelect({
                key: 'country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'investor_state',
                label: 'State / Region:',
                options$: this.lotService.getState(this.lang),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'investor_district',
                label: 'District:',
                options$: this.lotService.getDistrict(this.lang),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'investor_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'investor_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: this.lotService.getTownship(this.lang),
                required: true,
                filter: {
                    parent: 'investor_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'country',
                    value: ['Myanmar'],
                }
            }),              
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),       
        ]

        this.forms =[
            new FormSectionTitle({
                label: '1. Name of Enterprise (in English)'
            }),
            new FormInput({
                key: 'enterprise_name',
                label: 'Name of enterprise',
                required: true,
            }),
            new FormSectionTitle({
                label: '2. Form of Investment'
            }),
            new FormRadio({
                key: 'investment_type',
                required: true,
                value: 'Wholly Myanmar owned investment',
                label: 'Wholly Myanmar owned investment',
                tooltip: 'An enterprise owned wholly by Myanmar.',
                style: { 'margin-bottom': '5px' }
            }),
            new FormRadio({
                key: 'investment_type',
                required: true,
                value: 'Wholly Foreign owned investment',
                label: 'Wholly Foreign owned investment',
                tooltip: 'An enterprise owned wholly by foreign.',
                style: { 'margin-bottom': '5px' }
            }),
            new FormRadio({
                key: 'investment_type',
                required: true,
                value: 'Myanmar citizen investment',
                label: 'Myanmar citizen investment',
                tooltip: 'An enterprise by Myanmar citizen with up to 35% foreign shareholding.',
                style: { 'margin-bottom': '5px' }
            }),
            new FormRadio({
                key: 'investment_type',
                required: true,
                value: 'Joint Venture',
                label: 'Joint Venture',
                tooltip: 'An enterprise by Myanmar citizen and foreign with more than 35% foreign shareholding.'
            }),
            new FormSectionTitle({
                label: '3. Type of Company'
            }),
            new FormSelect({
                key: 'company_type',
                label: 'Type of company',
                options$: this.localService.getCompany(this.lang),
                required: true
            }),
            new FormSectionTitle({
                label: '4. Specific sector of investment:'
            }),
            new BaseFormGroup({
                key: 'business_sector',
                formGroup: businessSector
            }),
            new FormSectionTitle({
                label: '5. Location(s) of the investment project (in English)'
            }),

            new BaseFormArray({
                key: 'permit_locations',
                formArray: permit_location
            }),
            new FormSectionTitle({
                label: '6. Amount of Foreign Capital'
            }),
            new FormInput({
                key: 'usd_amount',
                label: 'Amount of foreign capital (Equivalent US$)',
                required: true,
                type: 'number',
            }),
            new FormSectionTitle({
                label: '7. Period during which Foreign Cash and Equivalents will be brought in'
            }),
            new FormDate({
                key: 'brought_start_date',
                label: 'From Date',
                columns: 'col-md-6',

            }),
            new FormDate({
                key: 'brought_end_date',
                label: 'To Date',
                columns: 'col-md-6',

            }),
            new FormSectionTitle({
                label: '8. (Expected) investment value'
            }),
            new FormInput({
                key: 'kyat_amount',
                label: '(Expected) investment value (Kyat)',
                required: true,
                type: 'number',
            }),
            new FormDate({
                key: 'construction_start_date',
                label: '9. Construction or preparatory period',
                placeholder: 'Start date',
                required: true,
                dateView: 'month'
            }),
            new FormInput({
                key: 'investment_start_date',
                label: '10. (Expected) Investment Period',
                type: 'number',
                required: true
            }),
            new FormSectionTitle({
                label: '11. Name(s) of investor(s) (in English):'
            }),
            new BaseFormArray({
                key: 'permit_investors',
                formArray: permit_investor
            }),
        ];
    }
}