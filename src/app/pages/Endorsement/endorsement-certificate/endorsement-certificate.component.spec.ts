import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementCertificateComponent } from './endorsement-certificate.component';

describe('EndorsementCertificateComponent', () => {
  let component: EndorsementCertificateComponent;
  let fixture: ComponentFixture<EndorsementCertificateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementCertificateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
