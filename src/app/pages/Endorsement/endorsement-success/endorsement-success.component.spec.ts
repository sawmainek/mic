import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSuccessComponent } from './endorsement-success.component';

describe('EndorsementSuccessComponent', () => {
  let component: EndorsementSuccessComponent;
  let fixture: ComponentFixture<EndorsementSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
