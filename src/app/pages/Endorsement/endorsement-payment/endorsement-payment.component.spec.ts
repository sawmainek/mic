import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementPaymentComponent } from './endorsement-payment.component';

describe('EndorsementPaymentComponent', () => {
  let component: EndorsementPaymentComponent;
  let fixture: ComponentFixture<EndorsementPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
