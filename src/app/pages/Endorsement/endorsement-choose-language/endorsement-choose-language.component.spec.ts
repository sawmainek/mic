import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementChooseLanguageComponent } from './endorsement-choose-language.component';

describe('EndorsementChooseLanguageComponent', () => {
  let component: EndorsementChooseLanguageComponent;
  let fixture: ComponentFixture<EndorsementChooseLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementChooseLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementChooseLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
