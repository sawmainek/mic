import { AppState } from './../../../core/reducers/index';
import { Store } from '@ngrx/store';
import { FormProgress } from './../../../core/form/_actions/form.actions';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Endorsement } from 'src/app/models/endorsement';
import { AuthService } from 'src/services/auth.service';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { EndorsementService } from 'src/services/endorsement.service';
import { FormProgressService } from 'src/services/form-progress.service';
import { EndorsementChooseLanguage } from './endorsement-choose-language';

@Component({
    selector: 'app-endorsement-choose-language',
    templateUrl: './endorsement-choose-language.component.html',
    styleUrls: ['./endorsement-choose-language.component.scss']
})
export class EndorsementChooseLanguageComponent extends EndorsementChooseLanguage implements OnInit {
    @Input() id: any;
    menu: any = "choose-language";
    page = "endorsement/endorsement-choose-language/";

    endoModel: Endorsement;
    endoMMModel: Endorsement;
    user: any = {};
    form: any;

    language: any = "myanmar";
    spinner: boolean = false;
    loading: boolean = false;

    constructor(
        private router: Router,
        private store: Store<AppState>,
        private authService: AuthService,
        private formService: EndorsementFormService, //For New Flow
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private formProgressService: FormProgressService,
    ) {
        super(formCtrService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') || null;
        });


    }

    ngOnInit(): void {
        this.authService.getCurrentUser().subscribe(result => {
            this.user = result;
            if (this.user) {
                this.getFormData();
            }
        });
    }

    getFormData() {
        if (this.id) {
            this.formService.show(this.id)
                .subscribe(result => {
                    this.form = result || {};
                    this.language = this.form?.language || 'Myanmar';
                    this.openLanguage(this.language)
                    this.endoModel = this.form?.data || null;
                    this.endoMMModel = this.form?.data_mm || null;

                    this.loading = false;
                })
        } else {
            this.formService.get({ search: `user_id:equal:${this.user.id}` })
                .subscribe(results => {
                    this.form = results[0] || {};
                    this.language = this.form?.language || 'Myanmar';
                    this.openLanguage(this.language);
                    this.endoModel = this.form?.data || null;
                    this.endoMMModel = this.form?.data_mm || null;

                    this.loading = false;
                })
        }
    }

    openLanguage(language) {
        if (window.location.href.indexOf('officer') == -1) {
            this.language = language;
        }
    }

    goToHome() {
        this.router.navigate(['/']);
    }

    getProgressData(form_id, model_id) {
        this.formProgressService.get({
            page: 1,
            rows: 100,
            search: `form_id:equal:${form_id || 0}|type:equal:Endorsement` // For New Flow this.id => this.micModel.id
        }).subscribe((x: any) => {
            this.getProgressForms(x || []);
            this.store.dispatch(new FormProgress({ progress: x }));
            this.saveFormProgress(form_id, model_id, this.page);
        });
    }

    onSubmit() {
        this.spinner = true;
        //For New Flow
        this.form.user_id = this.form.user_id || this.user.id;
        this.form.type = 'Endorsement';
        this.form.language = this.language;
        this.formService.create(this.form)
            .subscribe(result => {
                this.form = result;
                if (this.language == 'Myanmar') {
                    this.endoMMModel = {
                        ...this.endoMMModel || {},
                        ...{ user_id: this.form.user_id || this.user.id, choose_language: this.language, form_id: this.form.id, apply_to: this.form.apply_to, state: this.form.state }
                    } as Endorsement;

                    this.endoMMService.create(this.endoMMModel).subscribe(result => {
                        this.form.data_mm = result;

                        this.page = `endorsement/endorsement-choose-language/${result.id}/mm`;
                        this.getProgressData(this.form.id, result.id);

                        this.redirectLink(this.form.id);
                    })
                } else {

                    // For both English / Myanmar
                    this.endoModel = { ...this.endoModel || {}, ...{ user_id: this.form.user_id || this.user.id, choose_language: this.language, form_id: this.form.id, apply_to: this.form.apply_to, state: this.form.state } } as Endorsement;

                    this.endoMMModel = { ...this.endoMMModel || {}, ...{ user_id: this.form.user_id || this.user.id, choose_language: this.language, form_id: this.form.id, apply_to: this.form.apply_to, state: this.form.state } } as Endorsement;

                    forkJoin([
                        this.endoService.create(this.endoModel),
                        this.endoMMService.create(this.endoMMModel)
                    ])
                        .subscribe(results => {
                            this.page = `endorsement/endorsement-choose-language/${results[1].id}/mm`;
                            this.getProgressData(this.form.id, results[1].id);

                            this.redirectLink(this.form.id);
                        })
                }
            })
    }

    redirectLink(id: number) {
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form.language == 'Myanmar') {
                this.router.navigate(['/officer/endorsement/sectionA-form1', id, 'mm']);
            }
            else {
                this.router.navigate(['/officer/endorsement/sectionA-form1', id]);
            }

        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate(['/pages/endorsement/sectionA-form1', id, 'mm']);
            }
            else {
                this.router.navigate(['/pages/endorsement/sectionA-form1', id]);
            }
        }
    }


}
