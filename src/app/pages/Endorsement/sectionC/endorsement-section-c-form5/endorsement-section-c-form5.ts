import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { LocationService } from 'src/services/location.service';

export class EndorsementSectionCForm5 {

    public forms: BaseForm<string>[] = []

    constructor(
        private lotService: LocationService,
    ) {
        let phase: BaseForm < any > [] =[
            new FormSectionTitleCounter({
                label: 'Phase'
            }),
            new FormDate({
                key: 'start_date',
                label: 'From date',
                columns: 'col-md-6'
            }),
            new FormDate({
                key: 'end_date',
                label: 'To date',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'expected_value',
                label: '( Expected ) investment value',
                columns: 'col-md-6',
                type: 'number'
            }),
            new FormSelect({
                key: 'currency',
                label: 'Select currency',
                columns: 'col-md-6',
                options$: this.lotService.getCurrency()
            }),
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'expected_by_phase',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Expected investment value'
                    }),
                    new FormSectionTitle({
                        label: 'c. OPTIONAL: (Expected) investment value by phase'
                    }),
                    new FormTitle({
                        label: 'Phases in construction/preparatory period:'
                    }),
                    new BaseFormArray({
                        key: 'construction_phase',
                        formArray: phase,
                        defaultLength: 1
                    }),
                    new FormTitle({
                        label: 'Phases in commercial operation period:'
                    }),
                    new BaseFormArray({
                        key: 'commercial_phase',
                        formArray: phase,
                        defaultLength: 1
                    })
                ]
            })
            
        ];
    }
}