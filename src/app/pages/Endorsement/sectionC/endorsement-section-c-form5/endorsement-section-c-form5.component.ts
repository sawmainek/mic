import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Observable } from 'rxjs';
import { EndorsementSectionC } from '../endorsement-section-c';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { EndorsementSectionCForm5 } from './endorsement-section-c-form5';
import { cloneDeep } from 'lodash';


@Component({
    selector: 'app-endorsement-section-c-form5',
    templateUrl: './endorsement-section-c-form5.component.html',
    styleUrls: ['./endorsement-section-c-form5.component.scss']
})
export class EndorsementSectionCForm5Component extends EndorsementSectionC implements OnInit {
    @Input() id: any;
    page = "endorsement/sectionC-form5/";
    mm: any;

    user: any = {};

    currencies: any;

    endoModel: any;
    endoMMModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loading = false;
    spinner = false;
    is_draft = false;

    isRevision: boolean = false;

    /* phase: BaseForm<any>[] = [
        new FormSectionTitleCounter({
            label: 'Phase'
        }),
        new FormDate({
            key: 'start_date',
            label: 'From date',
            columns: 'col-md-6'
        }),
        new FormDate({
            key: 'end_date',
            label: 'To date',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'expected_value',
            label: '( Expected ) investment value',
            columns: 'col-md-6',
            type: 'number'
        }),
        new FormSelect({
            key: 'currency',
            label: 'Select currency',
            columns: 'col-md-6',
            options$: this.lotService.getCurrency()
        }),
    ]

    endoForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Expected investment value'
        }),
        new FormSectionTitle({
            label: 'c. OPTIONAL: (Expected) investment value by phase'
        }),
        new FormTitle({
            label: 'Phases in construction/preparatory period:'
        }),
        new BaseFormArray({
            key: 'construction_phase',
            formArray: this.phase,
            defaultLength: 1
        }),
        new FormTitle({
            label: 'Phases in commercial operation period:'
        }),
        new BaseFormArray({
            key: 'commercial_phase',
            formArray: this.phase,
            defaultLength: 1
        }),
    ] */

    baseForm: EndorsementSectionCForm5;
    constructor(
        private http: HttpClient,
        private router: Router,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        public location: Location,
        private lotService: LocationService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService

    ) {
        super(formCtlService);

    }

    ngOnInit(): void {
        this.loading = true;
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.baseForm = new EndorsementSectionCForm5(this.lotService)
            this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form.data_mm || {} : form.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.endoModel.expected_by_phase = this.endoModel?.expected_by_phase || {};
                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.endoModel, form?.data);

                        this.page = "endorsement/sectionC-form5/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.service.create({ ...this.endoModel, ...this.formGroup.value }, { secure: true })
            .subscribe(x => {
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                    this.redirectLink(this.form.id)
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionC-form7', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionC-form7', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionC-form5', id, 'mm']);
                }
            }
        }
    }
}
