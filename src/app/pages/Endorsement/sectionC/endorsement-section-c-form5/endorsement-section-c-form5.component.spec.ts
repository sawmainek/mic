import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm5Component } from './endorsement-section-c-form5.component';

describe('EndorsementSectionCForm5Component', () => {
  let component: EndorsementSectionCForm5Component;
  let fixture: ComponentFixture<EndorsementSectionCForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
