import { formProgress } from './../../../../core/form/_selectors/form.selectors';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/core';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementSectionC } from '../endorsement-section-c';
import { FormProgressService } from 'src/services/form-progress.service';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { CurrentForm } from 'src/app/core/form/_actions/form.actions';

@Component({
    selector: 'app-endorsement-section-c-form1',
    templateUrl: './endorsement-section-c-form1.component.html',
    styleUrls: ['./endorsement-section-c-form1.component.scss']
})
export class EndorsementSectionCForm1Component extends EndorsementSectionC implements OnInit {
    menu: any = "sectionC";
    @Input() id: any;
    mm: any;

    endoModel: Endorsement;
    endoMMModel: Endorsement;
    form: any;

    progressLists: any[] = [];
    pages: any[] = [];

    loaded: any = false;
    loading = false;

    constructor(
        private formService: EndorsementFormService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private router: Router,
        private formProgressService: FormProgressService,
        private translateService: TranslateService,
        private tostr: ToastrService
    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            if (this.id > 0) {
                // Get data from API
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.endoModel = this.form?.data || {};
                        this.endoMMModel = this.form?.data_mm || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.getFormProgress();

                        this.changeLanguage();
                        this.loading = false;

                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            `endorsement/sectionC-form2`, //0
            `endorsement/sectionC-form3`, //1
            `endorsement/sectionC-form4`, //2
            `endorsement/sectionC-form5`, //3 
            `endorsement/sectionC-form7`, //4
            `endorsement/sectionC-form8`, //5
            `endorsement/sectionC-form9`, //6
        ];
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:Endorsement` 
                    }).subscribe(result => {
                        this.progressLists = result;
                    });
                }
            }).unsubscribe();
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.endoModel.id}`).length;
            const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    openPage(index, link) {
        var complete = true;
        if (this.form.language == 'Myanmar') {
            if (index == 0 && !this.endoMMModel.company_info?.overview_is_established) {
                complete = false;
                this.tostr.error('Please complete Section-B, Question-2');
                return;
            }
        }
        else {
            if (index == 0 && (!this.endoModel.company_info?.overview_is_established || !this.endoMMModel.company_info?.overview_is_established)) {
                complete = false;
                this.tostr.error('Please complete Section-B, Question-2');
                return;
            }
        }

        if (complete) {
            if (window.location.href.indexOf('officer') !== -1) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate(['/officer/endorsement/' + link, this.id, 'mm']);
                } else {
                    this.router.navigate(['/officer/endorsement/' + link, this.id]);
                }

            } else {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate(['/pages/endorsement/' + link, this.id, 'mm']);
                } else {
                    this.router.navigate(['/pages/endorsement/' + link, this.id]);
                }
            }
        }
    }
}
