import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm1Component } from './endorsement-section-c-form1.component';

describe('EndorsementSectionCForm1Component', () => {
  let component: EndorsementSectionCForm1Component;
  let fixture: ComponentFixture<EndorsementSectionCForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
