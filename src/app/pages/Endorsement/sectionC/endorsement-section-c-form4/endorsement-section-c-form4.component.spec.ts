import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm4Component } from './endorsement-section-c-form4.component';

describe('EndorsementSectionCForm4Component', () => {
  let component: EndorsementSectionCForm4Component;
  let fixture: ComponentFixture<EndorsementSectionCForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
