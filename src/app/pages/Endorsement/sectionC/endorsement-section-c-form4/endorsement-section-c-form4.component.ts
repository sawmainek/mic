import { FormDummy } from './../../../../custom-component/dynamic-forms/base/form-dummy';
import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Endorsement } from 'src/app/models/endorsement';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { EndorsementSectionC } from '../endorsement-section-c';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { EndorsementSectionCForm4 } from './endorsement-section-c-form4';


@Component({
    selector: 'app-endorsement-section-c-form4',
    templateUrl: './endorsement-section-c-form4.component.html',
    styleUrls: ['./endorsement-section-c-form4.component.scss']
})
export class EndorsementSectionCForm4Component extends EndorsementSectionC implements OnInit {
    page = "endorsement/sectionC-form4/";
    @Input() id: any;
    mm: any = null;

    user: any = {};

    endoModel: any;
    formGroup: FormGroup;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    loading = false;

    totalZone1 = 0;
    totalZone2 = 0;
    totalZone3 = 0;

    isRevision: boolean = false;

    /* zone1: BaseForm<any>[] = [
        new FormDummy({
            key: 'zone',
            value: '1'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region',
            options$: this.lotService.getState(),
            filter: {
                parent: 'zone',
                key: 'zone'
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District',
            options$: this.lotService.getDistrict(),
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township',
            options$: this.lotService.getTownship(),
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'percent_expected',
            label: 'Percentage (%) of total expected investment value',
            validators: [Validators.min(0), Validators.max(100)],
            endfix: '%',
            value: "0",
            style: {
                'min-width': '200px'
            },
            type: 'number',
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateZone1Total(value, index, form, formGroup);
            }
        }),
    ]

    zone2: BaseForm<any>[] = [
        new FormDummy({
            key: 'zone',
            value: '2'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region',
            options$: this.lotService.getState(),
            filter: {
                parent: 'zone',
                key: 'zone'
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District',
            options$: this.lotService.getDistrict(),
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township',
            options$: this.lotService.getTownship(),
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'percent_expected',
            label: 'Percentage (%) of total expected investment value',
            validators: [Validators.min(0), Validators.max(100)],
            value: "0",
            endfix: '%',
            style: {
                'min-width': '200px'
            },
            type: 'number',
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateZone2Total(value, index, form, formGroup);
            }
        }),
    ]

    zone3: BaseForm<any>[] = [
        new FormDummy({
            key: 'zone',
            value: '3'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region',
            options$: this.lotService.getState(),
            filter: {
                parent: 'zone',
                key: 'zone'
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District',
            options$: this.lotService.getDistrict(),
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township',
            options$: this.lotService.getTownship(),
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'percent_expected',
            label: 'Percentage (%) of total expected investment value',
            validators: [Validators.min(0), Validators.max(100)],
            value: "0",
            endfix: '%',
            style: {
                'min-width': '200px'
            },
            type: 'number',
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateZone3Total(value, index, form, formGroup);
            }
        }),
    ]

    endoForm: BaseForm<any>[] = [
        new FormSectionTitle({
            label: '2. Expected investment value'
        }),
        new FormSectionTitle({
            label: 'b. Please indicate the expected investment value in each Zone'
        }),
        new FormNote({
            label: 'Locations by Zone were announced in Notification No. 10 / 2017 of the Myanmar Investment Commission.'
        }),
        new FormSectionTitle({
            label: 'Zone 1'
        }),
        new BaseFormGroup({
            key: 'zone_1',
            formGroup: [
                new BaseFormArray({
                    key: 'zone_1_list',
                    formArray: this.zone1,
                    useTable: true,
                    rowDeleteEvent: (index) => {
                        this.totalZone1 = this.totalZoneAddition(this.Zone1, 'pecent_expected');
                        this.formGroup.get('zone_1').get('zone1_total').setValue(this.totalZone1);
                    },
                    tableFooter: [
                        [
                            { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 },
                            {
                                cellFn: () => {
                                    return this.totalZone1;
                                }
                            },
                        ]
                    ]
                }),
                new FormHidden({ key: 'zone1_total' })
            ]
        }),
        new FormSectionTitle({
            label: 'Zone 2'
        }),
        new BaseFormGroup({
            key: 'zone_2',
            formGroup: [
                new BaseFormArray({
                    key: 'zone_2_list',
                    formArray: this.zone2,
                    useTable: true,
                    rowDeleteEvent: (index) => {
                        this.totalZone2 = this.totalZoneAddition(this.Zone2, 'pecent_expected');
                        this.formGroup.get('zone_2').get('zone2_total').setValue(this.totalZone2);
                    },
                    tableFooter: [
                        [
                            { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 },
                            {
                                cellFn: () => {
                                    return this.totalZone2;
                                }
                            },
                        ]
                    ]
                }),
                new FormHidden({ key: 'zone2_total' })
            ]
        }),
        new FormSectionTitle({
            label: 'Zone 3'
        }),
        new BaseFormGroup({
            key: 'zone_3',
            formGroup: [
                new BaseFormArray({
                    key: 'zone_3_list',
                    formArray: this.zone3,
                    useTable: true,
                    rowDeleteEvent: (index) => {
                        this.totalZone3 = this.totalZoneAddition(this.Zone3, 'pecent_expected');
                        this.formGroup.get('zone_3').get('zone3_total').setValue(this.totalZone3);
                    },
                    tableFooter: [
                        [
                            { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 },
                            {
                                cellFn: () => {
                                    return this.totalZone3;
                                }
                            },
                        ]
                    ]
                }),
                new FormHidden({ key: 'zone3_total' })
            ]
        }),
        new FormTitle({
            label: 'If necessary, upload supporting documents regarding expected investment value by zone location here:'
        }),
        new BaseFormArray({
            key: 'supporting_doc',
            formArray: [
                new FormFile({
                    key: 'document',
                    label: 'Name of document:',
                    multiple: true,
                })
            ],
            defaultLength: 1
        }),
    ] */

    baseForm: EndorsementSectionCForm4;
    constructor(
        private http: HttpClient,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private store: Store<AppState>,
        public formCtrService: FormControlService,
        private toast: ToastrService,
        private lotService: LocationService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
    ) {
        super(formCtrService);
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.baseForm = new EndorsementSectionCForm4(this.mm || 'en', this.lotService);
            this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms);
            this.baseForm.attachFormGroup(this.formGroup);

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.endoModel.expected_by_zone = this.endoModel?.expected_by_zone || [];
                        this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms, this.endoModel, form?.data);
                        this.baseForm.attachFormGroup(this.formGroup);

                        this.page = "endorsement/sectionC-form4/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });
                        this.baseForm.totalZone1 = this.baseForm.totalZoneAddition(this.baseForm.Zone1, 'pecent_expected');
                        this.baseForm.totalZone2 = this.baseForm.totalZoneAddition(this.baseForm.Zone2, 'pecent_expected');
                        this.baseForm.totalZone3 = this.baseForm.totalZoneAddition(this.baseForm.Zone3, 'pecent_expected');

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    // get Zone1() {
    //     return (this.formGroup.get('zone_1').get('zone_1_list') as FormArray).controls;
    // }

    // get Zone2() {
    //     return (this.formGroup.get('zone_2').get('zone_2_list') as FormArray).controls;
    // }

    // get Zone3() {
    //     return (this.formGroup.get('zone_3').get('zone_3_list') as FormArray).controls;
    // }

    // updateZone1Total(value, index, form, formGroup) {
    //     this.totalZone1 = 0;
    //     let formValue = this.Zone1[index].get(form.key).value;
    //     formValue = value;
    //     this.totalZone1 = this.totalZoneAddition(this.Zone1, form.key);
    //     this.formGroup.get('zone_1').get('zone1_total').setValue(this.totalZone1);
    // }

    // updateZone2Total(value, index, form, formGroup) {
    //     this.totalZone2 = 0;
    //     let formValue = this.Zone2[index].get(form.key).value;
    //     formValue = value;
    //     this.totalZone2 = this.totalZoneAddition(this.Zone2, form.key);
    //     this.formGroup.get('zone_2').get('zone2_total').setValue(this.totalZone2);
    // }

    // updateZone3Total(value, index, form, formGroup) {
    //     this.totalZone3 = 0;
    //     let formValue = this.Zone3[index].get(form.key).value;
    //     formValue = value;
    //     this.totalZone3 = this.totalZoneAddition(this.Zone3, form.key);
    //     this.formGroup.get('zone_3').get('zone3_total').setValue(this.totalZone3);
    // }

    totalZoneAddition(zone, key) {
        let total = 0;
        for (let i = 0; i < zone.length; i++) {
            total += +zone[i].get('percent_expected').value || 0;
        }
        return total;
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;

        this.formCtrService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue) => {
            // let data = { 'expected_by_zone': formValue };
            this.service.create({ ...this.endoModel, ...formValue }, { secure: true })
                .subscribe(x => {
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionC-form5', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionC-form5', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionC-form4', id, 'mm']);
                }
            }
        }

    }

}
