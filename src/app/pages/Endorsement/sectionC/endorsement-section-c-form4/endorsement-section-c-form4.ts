import { FormArray, FormGroup, Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocationService } from 'src/services/location.service';

export class EndorsementSectionCForm4 {

    public forms: BaseForm<string>[] = [];
    formGroup: FormGroup;
    totalZone1 = 0;
    totalZone2 = 0;
    totalZone3 = 0;

    constructor(
        private lang: string,
        private lotService: LocationService
    ) {
        
        let zone1: BaseForm < any > [] =[
            new FormDummy({
                key: 'zone',
                value: '1'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region',
                options$: this.lotService.getState(this.lang),
                filter: {
                    parent: 'zone',
                    key: 'zone'
                }
            }),
            new FormSelect({
                key: 'district',
                label: 'District',
                options$: this.lotService.getDistrict(this.lang),
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township',
                options$: this.lotService.getTownship(this.lang),
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
            new FormInput({
                key: 'percent_expected',
                label: 'Percentage (%) of total expected investment value',
                validators: [Validators.min(0), Validators.max(100)],
                endfix: '%',
                value: "0",
                style: {
                    'min-width': '200px'
                },
                type: 'number',
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateZone1Total(value, index, form, formGroup);
                }
            }),
        ];

        let zone2: BaseForm < any > [] =[
            new FormDummy({
                key: 'zone',
                value: '2'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region',
                options$: this.lotService.getState(this.lang),
                filter: {
                    parent: 'zone',
                    key: 'zone'
                }
            }),
            new FormSelect({
                key: 'district',
                label: 'District',
                options$: this.lotService.getDistrict(this.lang),
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township',
                options$: this.lotService.getTownship(this.lang),
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
            new FormInput({
                key: 'percent_expected',
                label: 'Percentage (%) of total expected investment value',
                validators: [Validators.min(0), Validators.max(100)],
                value: "0",
                endfix: '%',
                style: {
                    'min-width': '200px'
                },
                type: 'number',
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateZone2Total(value, index, form, formGroup);
                }
            }),
        ];

        let zone3: BaseForm < any > [] =[
            new FormDummy({
                key: 'zone',
                value: '3'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region',
                options$: this.lotService.getState(this.lang),
                filter: {
                    parent: 'zone',
                    key: 'zone'
                }
            }),
            new FormSelect({
                key: 'district',
                label: 'District',
                options$: this.lotService.getDistrict(this.lang),
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township',
                options$: this.lotService.getTownship(this.lang),
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
            new FormInput({
                key: 'percent_expected',
                label: 'Percentage (%) of total expected investment value',
                validators: [Validators.min(0), Validators.max(100)],
                value: "0",
                endfix: '%',
                style: {
                    'min-width': '200px'
                },
                type: 'number',
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateZone3Total(value, index, form, formGroup);
                }
            }),
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'expected_by_zone',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Expected investment value'
                    }),
                    new FormSectionTitle({
                        label: 'b. Please indicate the expected investment value in each Zone'
                    }),
                    new FormNote({
                        label: 'Locations by Zone were announced in Notification No. 10 / 2017 of the Myanmar Investment Commission.'
                    }),
                    new FormSectionTitle({
                        label: 'Zone 1'
                    }),
                    new BaseFormGroup({
                        key: 'zone_1',
                        formGroup: [
                            new BaseFormArray({
                                key: 'zone_1_list',
                                formArray: zone1,
                                useTable: true,
                                headerWidths: ['*', '*', '*', '40%'],
                                rowDeleteEvent: (index) => {
                                    this.totalZone1 = this.totalZoneAddition(this.Zone1, 'pecent_expected');
                                    this.formGroup.get('expected_by_zone').get('zone_1').get('zone1_total').setValue(this.totalZone1);
                                },
                                tablePDFFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 },
                                        {},
                                        {},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['percent_expected'] || 0)
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 },
                                        {
                                            cellFn: () => {
                                                return this.totalZone1;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormHidden({ key: 'zone1_total' })
                        ]
                    }),
                    new FormSectionTitle({
                        label: 'Zone 2'
                    }),
                    new BaseFormGroup({
                        key: 'zone_2',
                        formGroup: [
                            new BaseFormArray({
                                key: 'zone_2_list',
                                formArray: zone2,
                                useTable: true,
                                headerWidths: ['*', '*', '*', '40%'],
                                rowDeleteEvent: (index) => {
                                    this.totalZone2 = this.totalZoneAddition(this.Zone2, 'pecent_expected');
                                    this.formGroup.get('expected_by_zone').get('zone_2').get('zone2_total').setValue(this.totalZone2);
                                },
                                tablePDFFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 },
                                        {},
                                        {},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['percent_expected'] || 0)
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 },
                                        {
                                            cellFn: () => {
                                                return this.totalZone2;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormHidden({ key: 'zone2_total' })
                        ]
                    }),
                    new FormSectionTitle({
                        label: 'Zone 3'
                    }),
                    new BaseFormGroup({
                        key: 'zone_3',
                        formGroup: [
                            new BaseFormArray({
                                key: 'zone_3_list',
                                formArray: zone3,
                                useTable: true,
                                headerWidths: ['*', '*', '*', '40%'],
                                rowDeleteEvent: (index) => {
                                    this.totalZone3 = this.totalZoneAddition(this.Zone3, 'pecent_expected');
                                    this.formGroup.get('expected_by_zone').get('zone_3').get('zone3_total').setValue(this.totalZone3);
                                },
                                tablePDFFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 },
                                        { },
                                        { },
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['percent_expected'] || 0)
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 },
                                        {
                                            cellFn: () => {
                                                return this.totalZone3;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormHidden({ key: 'zone3_total' })
                        ]
                    }),
                    new FormTitle({
                        label: 'If necessary, upload supporting documents regarding expected investment value by zone location here:'
                    }),
                    new BaseFormArray({
                        key: 'supporting_doc',
                        formArray: [
                            new FormFile({
                                key: 'document',
                                label: 'Name of document:',
                                multiple: true,
                            })
                        ],
                        defaultLength: 1
                    }),
                ]
            })
        ];
    }

    get Zone1() {
        return (this.formGroup.get('expected_by_zone').get('zone_1').get('zone_1_list') as FormArray).controls;
    }

    get Zone2() {
        return (this.formGroup.get('expected_by_zone').get('zone_2').get('zone_2_list') as FormArray).controls;
    }

    get Zone3() {
        return (this.formGroup.get('expected_by_zone').get('zone_3').get('zone_3_list') as FormArray).controls;
    }

    updateZone1Total(value, index, form, formGroup) {
        this.totalZone1 = 0;
        let formValue = this.Zone1[index].get(form.key).value;
        formValue = value;
        this.totalZone1 = this.totalZoneAddition(this.Zone1, form.key);
        this.formGroup.get('expected_by_zone').get('zone_1').get('zone1_total').setValue(this.totalZone1);
    }

    updateZone2Total(value, index, form, formGroup) {
        this.totalZone2 = 0;
        let formValue = this.Zone2[index].get(form.key).value;
        formValue = value;
        this.totalZone2 = this.totalZoneAddition(this.Zone2, form.key);
        this.formGroup.get('expected_by_zone').get('zone_2').get('zone2_total').setValue(this.totalZone2);
    }

    updateZone3Total(value, index, form, formGroup) {
        this.totalZone3 = 0;
        let formValue = this.Zone3[index].get(form.key).value;
        formValue = value;
        this.totalZone3 = this.totalZoneAddition(this.Zone3, form.key);
        this.formGroup.get('expected_by_zone').get('zone_3').get('zone3_total').setValue(this.totalZone3);
    }

    totalZoneAddition(zone, key) {
        let total = 0;
        for (let i = 0; i < zone.length; i++) {
            total += +zone[i].get('percent_expected').value || 0;
        }
        return total;
    }

    attachFormGroup(formGroup: FormGroup) {
        this.formGroup = formGroup;
    }

}