import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
declare var jQuery: any;
import { Location } from '@angular/common';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Endorsement } from 'src/app/models/endorsement';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { ExpectedInvestment } from 'src/app/models/expected_invest';
import { HttpClient } from '@angular/common/http';
import { EndorsementSectionC } from '../endorsement-section-c';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';


@Component({
    selector: 'app-endorsement-section-c-form3',
    templateUrl: './endorsement-section-c-form3.component.html',
    styleUrls: ['./endorsement-section-c-form3.component.scss']
})
export class EndorsementSectionCForm3Component extends EndorsementSectionC implements OnInit {
    @Input() id: any;
    menu: any = "sectionC";
    page = "endorsement/sectionC-form3/";
    mm: any;

    user: any = {};
    is_officer: boolean = false;

    currencies: any;

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    endoExpectedInvestment: FormGroup;
    endoFormGroupFile: FormGroup;
    rowNumbers: any;

    submitted: boolean = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    endoForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Expected investment value'
        }),
        new FormSectionTitle({
            label: 'a . Expected investment value'
        })
    ]
    endoFormFile: BaseForm<string>[] = [
        new FormTitle({
            label: 'Please upload a detailed calculation for the expected investment value, including details on the method of valuation of the investment items:'
        }),
        new FormFile({
            key: 'explanation_doc',
            label: 'File Name',
            required: true
        })
    ]

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private ref: ChangeDetectorRef,
        private store: Store<AppState>,
        private toast: ToastrService,
        private lotService: LocationService,
        public location: Location,
        public formCtrService: FormControlService,
        private translateService: TranslateService,

    ) {
        super(formCtrService);
        this.loading = false;

        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;

        this.lotService.getCurrency()
            .subscribe(x => this.currencies = x);

        this.formGroup = this.formCtrService.toFormGroup(this.endoForm);
        this.endoFormGroupFile = this.formCtrService.toFormGroup(this.endoFormFile);
        this.endoExpectedInvestment = this.formBuilder.group({
            expected_list: new FormArray(
                [
                    this.formBuilder.group({
                        name: ['Cash and equivalents'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]

                    }),
                    this.formBuilder.group({
                        name: ['Other investment items (e.g. property, plant and equipment, intangible assets, etc.)'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]
                    }),
                ],
                [Validators.required]),
            spent_amount_kyat: ['0', [Validators.required]],
            spent_amount_usd: ['0', [Validators.required]],
            percent_construction: ['0', [Validators.required, Validators.min(0), Validators.max(100)]],
            percent_commercial: ['0', [Validators.required, Validators.min(0), Validators.max(100)]]
        });
        this.rowNumbers = ['i.', 'ii.', 'iii.', 'iv.', 'v.', 'vi.', 'vii.', 'viii.', 'ix.', 'x.', 'xi.', 'xii.', 'xiii.', 'xiv.', 'xv.', 'xvi.', 'xvii.', 'xviii.', 'xix.', 'xx.', 'xxi.', 'xxii.', 'xxiii.', 'xxiv.', 'xxv.', 'xxvi.', 'xxvii.', 'xxviii.', 'xxix.', 'xxx.'];

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        let expectedInvest = this.endoModel?.expected_investment || form?.data?.expected_investment || new ExpectedInvestment;
                        this.formGroup = this.formCtrService.toFormGroup(this.endoForm, expectedInvest);
                        this.endoFormGroupFile = this.formCtrService.toFormGroup(this.endoFormFile, expectedInvest);

                        this.page = "endorsement/sectionC-form3/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        if (expectedInvest.expected_list && expectedInvest?.expected_list.length > 0) {
                            this.expected_list.clear();
                            expectedInvest?.expected_list.forEach(x => {
                                this.addRow();
                                this.endoExpectedInvestment.patchValue(this.endoModel?.expected_investment || form?.data?.expected_investment);
                            });
                        }

                        this.changeLanguage();
                        this.loading = false;

                        this.endoExpectedInvestment.get('percent_construction').valueChanges.subscribe(val => {
                            this.calculatePercentage();
                        });

                    });
            }
        }).unsubscribe();
    }

    createmicFormGroup() {
        return this.formBuilder.group({
            name: ['', Validators.required],
            amount_kyat: ['', Validators.required],
            amount_usd: ['', Validators.required],
        })
    }

    addRow() {
        let fg = this.createmicFormGroup();
        this.expected_list.push(fg);
        this.ref.detectChanges();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    getPercentCommercial() {
        var percent_construction = Number(this.endoExpectedInvestment.get('percent_construction').value)
        var percent_commercial = 100 - percent_construction;
        this.endoExpectedInvestment.get('percent_commercial').setValue(percent_commercial);
        return percent_commercial;
    }

    getExpectedValue(index) {
        return (this.endoExpectedInvestment.get('expected_list') as FormArray).controls[index];
    }

    calculatePercentage() {
        var spent_amount = Number(this.endoExpectedInvestment.get('spent_amount').value | 0)
        var percetage = (Number(this.endoExpectedInvestment.get('percent_construction').value | 0) / 100) * spent_amount;
        this.endoExpectedInvestment.get('percent_commercial').setValue(spent_amount - percetage);
    }

    getTotalMillionAmoutUSD() {
        let amount: number = 0;
        for (let i = 0; i < this.expected_list.length; i++) {
            amount += Number(this.expected_list.controls[i].get('amount_usd').value);
        }
        this.endoExpectedInvestment.get('spent_amount_usd').setValue(amount);
        return amount;
    }

    getTotalMillionAmoutKyat() {
        let amount: number = 0;
        for (let i = 0; i < this.expected_list.length; i++) {
            amount += Number(this.expected_list.controls[i].get('amount_kyat').value);
        }
        this.endoExpectedInvestment.get('spent_amount_kyat').setValue(amount);
        return amount;
    }

    getCommercialPercent() {
        let amount_usd: number = 0;
        for (let i = 0; i < this.expected_list.length; i++) {
            amount_usd += Number(this.expected_list.controls[i].get('amount_usd').value);
        }
        this.endoExpectedInvestment.get('spent_amount_usd').setValue(amount_usd);
        return amount_usd;
    }

    onDraftSave() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid || this.endoExpectedInvestment.invalid || this.endoFormGroupFile.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;

        this.formCtrService.lazyUpload(this.endoFormFile, this.endoFormGroupFile, { type_id: this.id }, (formValue) => {
            let data = { 'expected_investment': { ...this.formGroup.value, ...this.endoExpectedInvestment.value, ...formValue } };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionC-form4', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionC-form4', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionC-form3', id, 'mm']);
                }
            }
        }
    }

    // saveFormData() {
    //     this.spinner = true;
    //     this.formCtrService.lazyUpload(this.endoFormFile, this.endoFormGroupFile, { type_id: this.id }, (formValue) => {
    //         let data = { 'expected_investment': { ...this.formGroup.value, ...this.endoExpectedInvestment.value, ...formValue } };
    //         this.service.create({ ...this.endoModel, ...data }, { secure: true })
    //             .subscribe(x => {
    //                 this.spinner = false;
    //                 if (!this.is_draft) {
    //                     this.router.navigate(['/pages/endorsement/sectionC-form4', x.id]);
    //                 }
    //             }, error => {
    //                 console.log(error);
    //             });
    //     });
    // }

    get expected_list(): FormArray {
        return this.endoExpectedInvestment.get('expected_list') as FormArray;
    }

    get validation() {
        return this.endoExpectedInvestment.controls;
    }

}
