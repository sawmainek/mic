import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { FormInput } from './../../../../custom-component/dynamic-forms/base/form-input';
import { BaseFormArray } from './../../../../custom-component/dynamic-forms/base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class EndorsementSectionCForm3 {
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {

        this.forms = [
            new BaseFormGroup({
                key: 'expected_investment',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Expected investment value'
                    }),
                    new FormSectionTitle({
                        label: 'a . Expected investment value'
                    }),
                    new BaseFormArray({
                        key: 'expected_list',
                        formArray: [
                            new FormInput({
                                key: 'name',
                            }),
                            new FormInput({
                                key: 'amount_kyat',
                            }),
                            new FormInput({
                                key: 'amount_usd',
                            })
                        ],
                        useTable: true,
                        tablePDFHeader: [
                            [
                                {},
                                { label: 'Total Amount in Myanmar Kyat', style: { 'alignment': 'center' } },
                                { label: 'Equivalent Amount in USD', style: { 'alignment': 'center' } }
                            ]
                        ],
                        tablePDFFooter: [
                            [
                                { label: '(Expected) investment value' },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.map(x => {
                                            total += Number(x['amount_kyat'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.map(x => {
                                            total += Number(x['amount_usd'] || 0);
                                        });
                                        return total;
                                    }
                                }
                            ]
                        ]
                    }),
                    new FormInput({
                        key: 'percent_construction',
                        label: 'Percentage (%) of (expected) investment value during the construction or preparatory period'
                    }),
                    new FormInput({
                        key: 'percent_commercial',
                        label: 'Percentage (%) of (expected) investment value during the commercial operation period'
                    }),
                    new FormTitle({
                        label: 'Please upload a detailed calculation for the expected investment value, including details on the method of valuation of the investment items:'
                    }),
                    new FormFile({
                        key: 'explanation_doc',
                        label: 'File Name',
                        required: true
                    })
                ]
            })
        ];
    }
}