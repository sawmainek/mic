import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm3Component } from './endorsement-section-c-form3.component';

describe('EndorsementSectionCForm3Component', () => {
  let component: EndorsementSectionCForm3Component;
  let fixture: ComponentFixture<EndorsementSectionCForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
