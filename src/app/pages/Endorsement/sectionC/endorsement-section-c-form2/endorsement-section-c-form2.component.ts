import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementService } from 'src/services/endorsement.service';
import { EndorsementSectionC } from '../endorsement-section-c';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
declare var jQuery: any;
import { cloneDeep } from 'lodash';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { EndorsementSectionCForm2 } from './endorsement-section-c-form2';
import { LocalService } from 'src/services/local.service';
import { Location } from '@angular/common';


@Component({
    selector: 'app-endorsement-section-c-form2',
    templateUrl: './endorsement-section-c-form2.component.html',
    styleUrls: ['./endorsement-section-c-form2.component.scss']
})
export class EndorsementSectionCForm2Component extends EndorsementSectionC implements OnInit {
    menu: any = "sectionB";
    @Input() id: any;
    page = "endorsement/sectionC-form2/";
    mm: any;

    user: any = {};

    endoModel: any;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    /* endoForm: BaseForm<any>[] = [
        new FormSectionTitle({
            label: '1. (Expected) timeline of investment'
        }),
        new FormDate({
            key: 'construction_start_date',
            label: 'Expected construction or preparatory period',
            placeholder: 'Start date',
            required: true,
            dateView: 'month'
        }),
        new FormDate({
            key: 'commercial_start_date',
            label: 'Expected commercial operation period',
            placeholder: 'Start date',
            required: true,
            dateView: 'month'
        }),
        new FormInput({
            key: 'investment_start_date',
            label: 'Expected investment period',
            type: 'number',
            required: true
        }),
        new FormTitle({
            label: 'Will you start the construction period right after obtaining the permit/endorsement?',
            required: true
        }),
        new FormRadio({
            key: 'start_after_obtain',
            label: 'Yes',
            value: 'Yes',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'start_after_obtain',
            label: 'No',
            value: 'No',
            required: true,
            columns: 'col-md-8'
        }),
        new FormInput({
            key: 'reason',
            label: 'If “No”, please describe the reasons or other necessary process you need to start for construction period?',
            required: true,
            criteriaValue: {
                key: 'start_after_obtain',
                value: 'No'
            }
        }),
        new FormTextArea({
            key: 'elaborate',
            label: 'If necessary, please elaborate:'
        }),
        new FormTitle({
            label: 'If necessary, upload supporting documents here:'
        }),
        new BaseFormArray({
            key: 'documents',
            formArray: [
                new FormFile({
                    key: 'supporting_doc',
                    label: 'Name of document:',
                    columns: 'col-md-9',
                    multiple: true,
                }),
            ],
            defaultLength: 1
        }),
        new FormNote({
            label: 'The Myanmar Investment Rules 107, 139 and 146 give guidance on the definitions of the construction/preparatory period and commencement of commercial operation.',
        })
    ] */

    baseForm: EndorsementSectionCForm2;
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public formCtlService: FormControlService,
        private translateService: TranslateService,
        private localService: LocalService,
        public location: Location,
    ) {
        super(formCtlService);
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.baseForm = new EndorsementSectionCForm2(this.localService);
            this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form.data_mm || {} : form.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));
                        this.endoModel.timeline = this.endoModel?.timeline || {};

                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.endoModel, form?.data);

                        this.page = "endorsement/sectionC-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    save() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;

        this.formCtlService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue: any) => {
            this.service.create({ ...this.endoModel, ...formValue }, { secure: true })
                .subscribe(x => {
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionC-form3', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionC-form3', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionC-form2', id, 'mm']);
                }
            }
        }
    }

    // saveFormData() {
    //     this.spinner = true;
    //     if ((this.formGroup.get('documents') as FormArray).controls[0].get('supporting_doc').value) {
    //         this.formCtlService.lazyUpload(this.endoForm, this.formGroup, { type_id: this.id }, (formValue) => {
    //             let data = { 'timeline': formValue };
    //             this.service.create({ ...this.endoModel, ...data }, { secure: true })
    //                 .subscribe(x => {
    //                     this.spinner = false;

    //                     if (!this.is_draft) {
    //                         this.router.navigate(['/pages/endorsement/sectionC-form3', x.id]);
    //                     }
    //                 }, error => {
    //                     console.log(error);
    //                 });
    //         });
    //     }
    //     else {
    //         let data = { 'timeline': this.formGroup.value };
    //         this.service.create({ ...this.endoModel, ...data }, { secure: true })
    //             .subscribe(x => {
    //                 this.spinner = false;

    //                 if (!this.is_draft) {
    //                     if (window.location.href.indexOf('officer') !== -1) {
    //                         this.router.navigate(['/officer/endorsement/sectionC-form3', x.id]);
    //                     } else {
    //                         this.router.navigate(['/pages/endorsement/sectionC-form3', x.id]);
    //                     }
    //                 }
    //             }, error => {
    //                 console.log(error);
    //             });
    //     }
    // }
}
