import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm2Component } from './endorsement-section-c-form2.component';

describe('EndorsementSectionCForm2Component', () => {
  let component: EndorsementSectionCForm2Component;
  let fixture: ComponentFixture<EndorsementSectionCForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
