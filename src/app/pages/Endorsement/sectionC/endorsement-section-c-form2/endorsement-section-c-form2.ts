import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { LocalService } from 'src/services/local.service';

export class EndorsementSectionCForm2 {

    public forms: BaseForm<string>[] = []

    constructor(
        private localService: LocalService,
    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'timeline',
                formGroup: [
                    new FormSectionTitle({
                        label: '1. (Expected) timeline of investment',
                        pageOrientation: 'portrait',
                        pageBreak: 'before'  
                    }),
                    new FormInput({
                        key: 'construction_date_no',
                        type: 'number',
                        required: true,
                        label: 'Expected construction or preparatory period',
                        columns: 'col-md-6 d-flex',
                    }),
                    new FormSelect({
                        key: 'construction_date_label',
                        required: true,
                        options$: this.localService.getDate(),
                        columns: 'col-md-3 d-flex',
                        value: 'Months'
                    }),
                    new FormInput({
                        key: 'commercial_date_no',
                        type: 'number',
                        required: true,
                        label: 'Expected commercial operation period/income tax exemption period',
                        columns: 'col-md-6 d-flex',
                    }),
                    new FormSelect({
                        key: 'commercial_date_label',
                        required: true,
                        options$: this.localService.getDate(),
                        columns: 'col-md-3 d-flex',
                        value: 'Months'
                    }),
                    new FormInput({
                        key: 'investment_start_date',
                        type: 'number',
                        endfix: 'Years',
                        label: 'Expected investment period',
                        required: true,
                    }),
                    new FormTitle({
                        label: 'Will you start the construction period right after obtaining the permit/endorsement?',
                        required: true
                    }),
                    new FormRadio({
                        key: 'start_after_obtain',
                        label: 'Yes',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'start_after_obtain',
                        label: 'No',
                        value: 'No',
                        required: true,
                        columns: 'col-md-8'
                    }),
                    new FormInput({
                        key: 'reason',
                        label: 'If “No”, please describe the reasons or other necessary process you need to start for construction period?',
                        required: true,
                        criteriaValue: {
                            key: 'start_after_obtain',
                            value: 'No'
                        }
                    }),
                    new FormTextArea({
                        key: 'elaborate',
                        label: 'If necessary, please elaborate:'
                    }),
                    new FormTitle({
                        label: 'If necessary, upload supporting documents here:'
                    }),
                    new BaseFormArray({
                        key: 'documents',
                        formArray: [
                            new FormFile({
                                key: 'supporting_doc',
                                label: 'Name of document:',
                                columns: 'col-md-9',
                                multiple: true,
                            }),
                        ],
                        defaultLength: 1
                    }),
                    new FormNote({
                        label: 'The Myanmar Investment Rules 107, 139 and 146 give guidance on the definitions of the construction/preparatory period and commencement of commercial operation.',
                    })
                ]
            })
        ];
    }
}