import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm6Component } from './endorsement-section-c-form6.component';

describe('EndorsementSectionCForm6Component', () => {
  let component: EndorsementSectionCForm6Component;
  let fixture: ComponentFixture<EndorsementSectionCForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
