import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
declare var jQuery: any;
import { Location } from '@angular/common';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { HttpClient } from '@angular/common/http';
import { EndorsementService } from 'src/services/endorsement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { ExpectedInvestdt } from 'src/app/models/expected-investdt';
import { Observable } from 'rxjs';
import { EndorsementSectionC } from '../endorsement-section-c';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { EndorsementSectionCForm6 } from './endorsement-section-c-form6';


@Component({
    selector: 'app-endorsement-section-c-form6',
    templateUrl: './endorsement-section-c-form6.component.html',
    styleUrls: ['./endorsement-section-c-form6.component.scss']
})
export class EndorsementSectionCForm6Component extends EndorsementSectionC implements OnInit {

    @Input() id: any;
    page = "endorsement/sectionC-form6/";
    mm: any;

    user: any = {};

    // cashFormGroup: FormGroup;
    endoModel: Endorsement;
    endoMMModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    loading = false;

    totalValue: any[] = [
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 }
    ];

    totalCash: any[] = [0, 0, 0];

    isRevision: boolean = false;

    /* machinery_breakdown: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'd. Please provide a detailed list of Machinery & equipment, (procured locally and imported) using the following format:'
        }),
        new BaseFormArray({
            key: 'breakdowns',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                    required: true,
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    required: true,
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                    required: true
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'machinery_breakdown');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'machinery_breakdown');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    required: true,
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCountry(),
                    required: true
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'machinery_breakdown');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'machinery_breakdown');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                    required: true
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('machinery_breakdown', 0);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Kyat Equivalent', style: { 'text-align': 'center' } },
                    { label: 'USD Equivalent', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    raw_breakdown: BaseForm<any>[] = [
        new FormTitle({
            'label': 'e. Please provide a detailed list of raw materials (procured locally and imported) using the following format:'
        }),
        new BaseFormArray({
            key: 'breakdowns',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                    required: true,
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    required: true,
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                    required: true
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'raw_breakdown');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'raw_breakdown');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    required: true,
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    required: true
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'raw_breakdown');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'raw_breakdown');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                    required: true
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('raw_breakdown', 1);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Kyat Equivalent', style: { 'text-align': 'center' } },
                    { label: 'USD Equivalent', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    endoForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'machinery_breakdown',
            formGroup: this.machinery_breakdown
        }),
        new BaseFormGroup({
            key: 'raw_breakdown',
            formGroup: this.raw_breakdown
        }),
        new FormDummy({
            key: 'cash_equivalent'
        }),
        new FormDummy({
            key: 'period_start_date'
        }),
        new FormDummy({
            key: 'period_end_date'
        })
    ]; */

    baseForm: EndorsementSectionCForm6;
    constructor(
        private ref: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private router: Router,
        public location: Location,
        private store: Store<AppState>,
        private lotService: LocationService,
        public formCtlService: FormControlService,
        private toast: ToastrService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService

    ) {
        super(formCtlService);
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.baseForm = new EndorsementSectionCForm6(this.lotService);
            this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            this.baseForm.attachFormGroup(this.formGroup);
            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        let expectedInvestdt = this.endoModel?.expected_investmentdt || form?.data?.expected_investmentdt || new ExpectedInvestdt;
                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, expectedInvestdt);
                        this.baseForm.attachFormGroup(this.formGroup);

                        this.baseForm.calculateTotalValue('machinery_breakdown', 0);
                        this.baseForm.calculateTotalValue('raw_breakdown', 1);

                        this.page = "endorsement/sectionC-form6/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }


    columnForm() {
        return this.formBuilder.group({
            amount: ['0', [Validators.required]],
            currency: ['', Validators.required],
        })
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    /* calculateTotalValue(control, index) {
        let formValue = this.formGroup.value;
        this.totalValue[index].total_kyat = 0;
        this.totalValue[index].total_usd = 0;

        formValue[control]['breakdowns'].map(x => {
            this.totalValue[index].total_kyat += parseFloat(x.kyat);
            this.totalValue[index].total_usd += parseFloat(x.usd);

            this.formGroup.get(control).get('total_kyat').setValue(this.totalValue[index].total_kyat);
            this.formGroup.get(control).get('total_usd').setValue(this.totalValue[index].total_usd);
        });
    }

    calculateEquivalent(value, index, form, formGroup, control) {
        let formValue = this.formGroup.value;
        formValue[control]['breakdowns'][index][form.key] = value;
        this.calculateTotalValue(control, control == 'machinery_breakdown' ? 0 : 1);
    }

    updateFormValue(value, index, form, formGroup, control) {
        let formValue = this.formGroup.value;
        formValue[control]['breakdowns'][index][form.key] = value;

        // Update Total Value
        let rowValue = formGroup.value;
        formGroup.get('total').setValue(Number(rowValue.quantity | 0) * Number(rowValue.price | 0));

        this.calculateTotalValue(control, control == 'machinery_breakdown' ? 0 : 1);
    } */

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    save() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            console.log(this.formGroup);
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    // saveFormData() {
    //   this.spinner = true;
    //   this.formCtlService.lazyUpload(this.endoForm, this.formGroup, { type_id: this.id }, (formValue) => {
    //     let data = { 'expected_investmentdt': { ...this.formGroup.value, ...formValue } };
    //     this.endoService.create({ ...this.endoModel, ...data }, { secure: true })
    //       .subscribe(x => {
    //         this.spinner = false;
    //         if (!this.is_draft) {
    //           if (window.location.href.indexOf('officer') !== -1) {
    //             this.router.navigate(['/officer/endorsement/sectionC-form7', x.id]);
    //           } else {
    //             this.router.navigate(['/pages/endorsement/sectionC-form7', x.id]);
    //           }
    //         }
    //       }, error => {
    //         console.log(error);
    //       });
    //   });
    // }

    saveFormData() {
        this.spinner = true;

        this.formCtlService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue) => {
            this.service.create({ ...this.endoModel, ...{ ...this.formGroup.value, ...formValue } }, { secure: true })
                .subscribe(x => {
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionC-form7', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionC-form7', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionC-form6', id, 'mm']);
                }
            }
        }
    }
}
