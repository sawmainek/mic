import { FormTitle } from './../../../../custom-component/dynamic-forms/base/form-title';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { FormGroup, Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { LocationService } from 'src/services/location.service';

export class EndorsementSectionCForm6 {

    public forms: BaseForm<string>[] = [];
    formGroup: FormGroup;
    
    totalValue: any[] = [
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 }
    ];

    constructor(
        private lotService: LocationService,
    ) {
        let machinery_breakdown: BaseForm < any > [] =[
            new FormSectionTitle({
                'label': 'd. Please provide a detailed list of Machinery & equipment, (procured locally and imported) using the following format:'
            }),
            new BaseFormArray({
                key: 'breakdowns',
                formArray: [
                    new FormSectionTitleCounter({
                        label: '',
                        style: { 'alignment': 'center' }
                    }),
                    new FormInput({
                        key: 'name',
                        required: true,
                    }),
                    new FormInput({
                        key: 'code',
                        type: 'number',
                        required: true,
                        validators: [Validators.minLength(4), Validators.maxLength(4)],
                    }),
                    new FormInput({
                        key: 'unit',
                        required: true,
                        // type: 'number',
                    }),
                    new FormInput({
                        key: 'price',
                        type: 'number',
                        required: true,
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'machinery_breakdown');
                        }
                    }),
                    new FormInput({
                        key: 'quantity',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'machinery_breakdown');
                        }
                    }),
                    new FormInput({
                        key: 'total',
                        type: 'number',
                        value: '0',
                        required: true,
                        readonly: true
                    }),
                    new FormSelect({
                        key: 'currency',
                        options$: this.lotService.getCountry(),
                        required: true,
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'machinery_breakdown');
                        }
                    }),
                    new FormInput({
                        key: 'usd',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'machinery_breakdown');
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: this.lotService.getCountry(), 
                        required: true,                       
                    }),
                ],
                useTable: true,
                headerRows: 2,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue('machinery_breakdown', 0);
                },
                tableHeader: [
                    [
                        { label: 'No.', style: { 'alignment': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                        { label: 'Item', style: { 'alignment': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'alignment': 'center' } },
                        { label: 'Unit', style: { 'alignment': 'center' } },
                        { label: 'Unit Price', style: { 'alignment': 'center' } },
                        { label: 'Quantity', style: { 'alignment': 'center' } },
                        { label: 'Total Value', style: { 'alignment': 'center' }, colSpan: 2 },
                        {},
                        { label: 'Kyat Equivalent', style: { 'alignment': 'center' } },
                        { label: 'USD Equivalent', style: { 'alignment': 'center' } },
                        { label: 'Country procured from', style: { 'alignment': 'center' } }
                    ],
                    [
                        {},
                        { label: '1', style: { 'alignment': 'center' } },
                        { label: '2', style: { 'alignment': 'center' } },
                        { label: '3', style: { 'alignment': 'center' } },
                        { label: '4', style: { 'alignment': 'center' } },
                        { label: '5', style: { 'alignment': 'center' } },
                        { label: '6', style: { 'alignment': 'center' } },
                        { label: '7', style: { 'alignment': 'center' } },
                        { label: '8', style: { 'alignment': 'center' } },
                        { label: '9', style: { 'alignment': 'center' } },
                        { label: '10', style: { 'alignment': 'center' } }
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Total', colSpan: 8 },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_kyat;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_usd;
                            }
                        },
                    ]
                ]
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
            new FormHidden({
                key: 'total_usd'
            }),
        ];

        let raw_breakdown: BaseForm < any > [] =[
            new FormTitle({
                'label': 'e. Please provide a detailed list of raw materials (procured locally and imported) using the following format:'
            }),
            new BaseFormArray({
                key: 'breakdowns',
                formArray: [
                    new FormSectionTitleCounter({
                        label: '',
                        style: { 'alignment': 'center' }
                    }),
                    new FormInput({
                        key: 'name',
                        required: true,
                    }),
                    new FormInput({
                        key: 'code',
                        type: 'number',
                        required: true,
                        validators: [Validators.minLength(4), Validators.maxLength(4)],
                    }),
                    new FormInput({
                        key: 'unit',
                        // type: 'number',
                        required: true,
                    }),
                    new FormInput({
                        key: 'price',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'raw_breakdown');
                        }
                    }),
                    new FormInput({
                        key: 'quantity',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'raw_breakdown');
                        }
                    }),
                    new FormInput({
                        key: 'total',
                        type: 'number',
                        value: '0',
                        required: true,
                        readonly: true
                    }),
                    new FormSelect({
                        key: 'currency',
                        required: true,
                        options$: this.lotService.getCurrency(),
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'raw_breakdown');
                        }
                    }),
                    new FormInput({
                        key: 'usd',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'raw_breakdown');
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: this.lotService.getCountry(),
                        required: true,
                    }),
                ],
                useTable: true,
                headerRows: 2,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue('raw_breakdown', 1);
                },
                tableHeader: [
                    [
                        { label: 'No.', style: { 'alignment': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                        { label: 'Item', style: { 'alignment': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'alignment': 'center' } },
                        { label: 'Unit', style: { 'alignment': 'center' } },
                        { label: 'Unit Price', style: { 'alignment': 'center' } },
                        { label: 'Quantity', style: { 'alignment': 'center' } },
                        { label: 'Total Value', style: { 'alignment': 'center' }, colSpan: 2 },
                        {},
                        { label: 'Kyat Equivalent', style: { 'alignment': 'center' } },
                        { label: 'USD Equivalent', style: { 'alignment': 'center' } },
                        { label: 'Country procured from', style: { 'alignment': 'center' } }
                    ],
                    [
                        {},
                        { label: '1', style: { 'alignment': 'center' } },
                        { label: '2', style: { 'alignment': 'center' } },
                        { label: '3', style: { 'alignment': 'center' } },
                        { label: '4', style: { 'alignment': 'center' } },
                        { label: '5', style: { 'alignment': 'center' } },
                        { label: '6', style: { 'alignment': 'center' } },
                        { label: '7', style: { 'alignment': 'center' } },
                        { label: '8', style: { 'alignment': 'center' } },
                        { label: '9', style: { 'alignment': 'center' } },
                        { label: '10', style: { 'alignment': 'center' } }
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Total', colSpan: 8 },
                        {
                            cellFn: () => {
                                return this.totalValue[1].total_kyat;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[1].total_usd;
                            }
                        },
                    ]
                ]
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
            new FormHidden({
                key: 'total_usd'
            }),
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'expected_investmentdt',
                formGroup: [
                    new FormTitle( {
                        label: '2. Expected investment value',
                        pageBreak: 'before',
                        pageOrientation: 'landscape'
                    }),
                    new BaseFormGroup({
                        key: 'machinery_breakdown',
                        formGroup: machinery_breakdown
                    }),
                    new BaseFormGroup({
                        key: 'raw_breakdown',
                        formGroup: raw_breakdown
                    }),
                    new FormDummy({
                        key: 'cash_equivalent'
                    }),
                    new FormDummy({
                        key: 'period_start_date'
                    }),
                    new FormDummy({
                        key: 'period_end_date'
                    })
                ]
            })
            
        ];
    }

    updateFormValue(value, index, form, formGroup, control) {
        let formValue = this.formGroup.value;
        formValue.expected_investmentdt[control]['breakdowns'][index][form.key] = value;

        // Update Total Value
        let rowValue = formGroup.value;
        formGroup.get('total').setValue(Number(rowValue.quantity | 0) * Number(rowValue.price | 0));

        this.calculateTotalValue(control, control == 'machinery_breakdown' ? 0 : 1);
    }

    calculateEquivalent(value, index, form, formGroup, control) {
        let formValue = this.formGroup.value;
        formValue.expected_investmentdt[control]['breakdowns'][index][form.key] = value;
        this.calculateTotalValue(control, control == 'machinery_breakdown' ? 0 : 1);
    }

    calculateTotalValue(control, index) {
        let formValue = this.formGroup.value;
        this.totalValue[index].total_kyat = 0;
        this.totalValue[index].total_usd = 0;

        formValue.expected_investmentdt[control]['breakdowns'].map(x => {
            this.totalValue[index].total_kyat += parseFloat(x.kyat);
            this.totalValue[index].total_usd += parseFloat(x.usd);

            this.formGroup.get('expected_investmentdt').get(control).get('total_kyat').setValue(this.totalValue[index].total_kyat);
            this.formGroup.get('expected_investmentdt').get(control).get('total_usd').setValue(this.totalValue[index].total_usd);
        });
    }

    attachFormGroup(formGroup: FormGroup) {
        this.formGroup = formGroup;
    }
}