import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm8Component } from './endorsement-section-c-form8.component';

describe('EndorsementSectionCForm8Component', () => {
  let component: EndorsementSectionCForm8Component;
  let fixture: ComponentFixture<EndorsementSectionCForm8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
