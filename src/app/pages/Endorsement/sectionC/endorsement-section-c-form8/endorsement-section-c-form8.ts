import { FormGroup } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

export class EndorsementSectionCForm8 {

    public forms: BaseForm<string>[] = [];
    formGroup: FormGroup;

    totalValue: any[] = [
        { 'kyat': 0, 'usd': 0 }, //loan_repay_total
        { 'kyat': 0, 'usd': 0 }, //issued_share_total
        { 'kyat': 0, 'usd': 0 }, //other_total
    ];

    constructor(
        private lang: string,
        private lotService: LocationService,
        private localService: LocalService,
    ) {
        let loanRepay: BaseForm < string > [] =[
            new FormSelect({
                key: 'origin_id',
                label: 'Origin',
                options$: this.localService.getOrigin(this.lang),
                style: {
                    'max-width:': '150px;'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateOrigin(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
                }
            }),
            new FormInput({
                key: 'source',
                label: 'Source (Name of company/ individual)',
                style: {
                    'min-width': '200px'
                }
            }),
            new FormSelect({
                key: 'currency',
                label: 'Currency',
                options$: this.lotService.getCurrency(),
            }),
            new FormInput({
                key: 'value',
                label: 'Value',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateTotal(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
                }
            }),
            new FormInput({
                key: 'equ_kyat',
                label: 'Equivalent Kyat',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.calculateEquivalent(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
                }
            }),
            new FormInput({
                key: 'equ_usd',
                label: 'Equivalent US$',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.calculateEquivalent(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
                }
            })
        ];

        let issuedShare: BaseForm < string > [] =[
            new FormSelect({
                key: 'origin_id',
                label: 'Origin',
                options$: this.localService.getOrigin(this.lang),
                style: {
                    'max-width:': '150px;'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateOrigin(value, index, form, formGroup, 'issued_share', 'issued_share_list');
                }
            }),
            new FormSelect({
                key: 'currency',
                label: 'Currency',
                options$: this.lotService.getCurrency(),
            }),
            new FormInput({
                key: 'value',
                label: 'Value',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateTotal(value, index, form, formGroup, 'issued_share', 'issued_share_list');
                }
            }),
            new FormInput({
                key: 'equ_kyat',
                label: '(Equivalent) Kyat',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.calculateEquivalent(value, index, form, formGroup, 'issued_share', 'issued_share_list');
                }
            }),
            new FormInput({
                key: 'equ_usd',
                label: '(Equivalent) US$',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.calculateEquivalent(value, index, form, formGroup, 'issued_share', 'issued_share_list');
                }
            })
        ];

        let otherSource: BaseForm < string > [] =[
            new FormInput({
                key: 'type',
                label: 'Type',
                style: {
                    'min-width': '200px'
                }
            }),
            new FormSelect({
                key: 'origin_id',
                label: 'Origin',
                options$: this.localService.getOrigin(this.lang),
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateOrigin(value, index, form, formGroup, 'other_source', 'other_source_list');
                }
            }),
            new FormSelect({
                key: 'currency',
                label: 'Currency',
                options$: this.lotService.getCurrency(),
            }),
            new FormInput({
                key: 'value',
                label: 'Value',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.updateTotal(value, index, form, formGroup, 'other_source', 'other_source_list');
                }
            }),
            new FormInput({
                key: 'equ_kyat',
                label: 'Equivalent Kyat',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.calculateEquivalent(value, index, form, formGroup, 'other_source', 'other_source_list');
                }
            }),
            new FormInput({
                key: 'equ_usd',
                label: 'Equivalent US$',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    this.calculateEquivalent(value, index, form, formGroup, 'other_source', 'other_source_list');
                }
            })
        ];

        let capital: BaseForm < string > [] =[
            new FormInput({
                key: 'kyat',
                label: 'Equivalent Kyat',
                columns: 'col-md-6',
                type: 'number',
                required: true,
                readonly: true,
            }),
            new FormInput({
                key: 'usd',
                label: 'Equivalent US$',
                columns: 'col-md-6',
                type: 'number',
                required: true,
                readonly: true,
            })
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'proposed_basic_info',
                formGroup: [
                    new FormSectionTitle({
                        label: '3.Particulars of financing of investment',
                        pageOrientation: 'portrait',
                        pageBreak: 'before'
                    }),
                    new FormTitle({
                        label: 'a. Loans repayable by investment business to finance investment'
                    }),
                    new BaseFormGroup({
                        key: 'loan_repay',
                        formGroup: [
                            new BaseFormArray({
                                key: 'loan_repay_list',
                                formArray: loanRepay,
                                useTable: true,
                                headerWidths: ['20%', '25%', 'auto', 'auto', '*', '*'],
                                tablePDFFooter: [
                                    [
                                        { label: 'Total', colSpan: 4 },
                                        {},{},{},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['equ_kyat'] || 0);
                                                });
                                                return total;
                                            }
                                        },
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['equ_usd'] || 0);
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Total', colSpan: 4 },
                                        {
                                            cellFn: () => {
                                                return this.totalValue[0].kyat;
                                            }
                                        },
                                        {
                                            cellFn: () => {
                                                return this.totalValue[0].usd;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormDummy({ key: 'total_equ_kyat' }),
                            new FormDummy({ key: 'total_equ_usd' })
                        ]
                    }),

                    new FormTitle({
                        label: 'b. Issued/paid up share capital to finance investment'
                    }),
                    new BaseFormGroup({
                        key: 'issued_share',
                        formGroup: [
                            new BaseFormArray({
                                key: 'issued_share_list',
                                formArray: issuedShare,
                                useTable: true,
                                headerWidths: ['25%','25%','*','*','*'],
                                tablePDFFooter: [
                                    [
                                        { label: 'Total', colSpan: 3 },
                                        {},{},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['equ_kyat'] || 0);
                                                });
                                                return total;
                                            }
                                        },
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['equ_usd'] || 0);
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Total', colSpan: 3 },
                                        {
                                            cellFn: () => {
                                                return this.totalValue[1].kyat;
                                            }
                                        },
                                        {
                                            cellFn: () => {
                                                return this.totalValue[1].usd;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormDummy({ key: 'total_equ_kyat' }),
                            new FormDummy({ key: 'total_equ_usd' })
                        ]
                    }),

                    new FormTitle({
                        label: 'c. Other sources to finance investment:'
                    }),
                    new BaseFormGroup({
                        key: 'other_source',
                        formGroup: [
                            new BaseFormArray({
                                key: 'other_source_list',
                                formArray: otherSource,
                                useTable: true,
                                headerWidths: ['20%', '25%', 'auto', 'auto', '*', '*'],
                                tablePDFFooter: [
                                    [
                                        { label: 'Total', colSpan: 4 },
                                        {},{},{},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['equ_kyat'] || 0);
                                                });
                                                return total;
                                            }
                                        },
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['equ_usd'] || 0);
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Total', colSpan: 4 },
                                        {
                                            cellFn: () => {
                                                return this.totalValue[2].kyat;
                                            }
                                        },
                                        {
                                            cellFn: () => {
                                                return this.totalValue[2].usd;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormDummy({ key: 'total_equ_kyat' }),
                            new FormDummy({ key: 'total_equ_usd' })
                        ]
                    }),
                    new FormTitle({
                        label: 'd. Total:',
                        pageBreak: 'before'
                    }),
                    new BaseFormGroup({
                        key: 'total',
                        formGroup: [
                            new FormTitle({
                                label: 'Myanmar capital',
                            }),
                            new BaseFormGroup({
                                key: 'myanmar_capital',
                                formGroup: capital
                            }),
                            new FormTitle({
                                label: 'Foreign capital',
                            }),
                            new BaseFormGroup({
                                key: 'foreign_capital',

                                formGroup: capital
                            })
                        ]
                    }),
                    new FormTitle({
                        label: 'Please upload evidence of the financing of the investment:'
                    }),
                    new BaseFormArray({
                        key: 'finance_evidence_doc',
                        formArray: [
                            new FormFile({
                                key: 'document',
                                label: 'Name of document:',
                                required: true,
                                multiple: true,
                            })
                        ]
                    })
                ]
            })
            
        ];
    }

    updateOrigin(value, index, form, formGroup, control, controlList) {
        let formValue = this.formGroup.value;
        formValue['proposed_basic_info'][control][controlList][index][form.key] = value;
        this.calculateCapitalValue();
    }

    calculateCapitalValue() {
        // Number(foreign_total_kyat | 0) + Number(x.equ_kyat | 0)
        var foreign_total_kyat = 0;
        var foreign_total_usd = 0;
        var myanmar_total_kyat = 0;
        var myanmar_total_usd = 0;
        this.formGroup.get('proposed_basic_info').get('loan_repay').get('loan_repay_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = parseFloat(foreign_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                foreign_total_usd = parseFloat(foreign_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = parseFloat(myanmar_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                myanmar_total_usd = parseFloat(myanmar_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
        });

        this.formGroup.get('proposed_basic_info').get('issued_share').get('issued_share_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = parseFloat(foreign_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                foreign_total_usd = parseFloat(foreign_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = parseFloat(myanmar_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                myanmar_total_usd = parseFloat(myanmar_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
        });

        this.formGroup.get('proposed_basic_info').get('other_source').get('other_source_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = parseFloat(foreign_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                foreign_total_usd = parseFloat(foreign_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = parseFloat(myanmar_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                myanmar_total_usd = parseFloat(myanmar_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
        });

        this.formGroup.get('proposed_basic_info').get('total').get('foreign_capital').get('kyat').setValue(foreign_total_kyat);
        this.formGroup.get('proposed_basic_info').get('total').get('foreign_capital').get('usd').setValue(foreign_total_usd);
        this.formGroup.get('proposed_basic_info').get('total').get('myanmar_capital').get('kyat').setValue(myanmar_total_kyat);
        this.formGroup.get('proposed_basic_info').get('total').get('myanmar_capital').get('usd').setValue(myanmar_total_usd);

    }

    updateTotal(value, index, form, formGroup, control, controlList) {
        let formValue = this.formGroup.value;
        formValue['proposed_basic_info'][control][controlList][index][form.key] = value;

        var i = 0;
        if (control == 'loan_repay') {
            i = 0;
        }
        else if (control == 'issued_share') {
            i = 1;
        }
        else if (control == 'other_source') {
            i = 2;
        }
        this.calculateTotalValue(control, controlList, i);
    }

    calculateTotalValue(control, controlList, index) {
        let formValue = this.formGroup.value;
        this.totalValue[index].kyat = 0;
        this.totalValue[index].usd = 0;

        formValue['proposed_basic_info'][control][controlList].map(x => {
            this.totalValue[index].kyat += parseFloat(x.equ_kyat || 0);
            this.totalValue[index].usd += parseFloat(x.equ_usd || 0);

            this.formGroup.get('proposed_basic_info').get(control).get('total_equ_kyat').setValue(this.totalValue[index].kyat);
            this.formGroup.get('proposed_basic_info').get(control).get('total_equ_usd').setValue(this.totalValue[index].usd);
        });
        this.calculateCapitalValue();
    }

    calculateEquivalent(value, index, form, formGroup, control, controlList) {
        let formValue = this.formGroup.value;
        formValue['proposed_basic_info'][control][controlList][index][form.key] = value;

        var i = 0;
        if (control == 'loan_repay') {
            i = 0;
        }
        else if (control == 'issued_share') {
            i = 1;
        }
        else if (control == 'other_source') {
            i = 2;
        }
        this.calculateTotalValue(control, controlList, i);
    }

    attachFormGroup(formGroup: FormGroup) {
        this.formGroup = formGroup;
    }
}