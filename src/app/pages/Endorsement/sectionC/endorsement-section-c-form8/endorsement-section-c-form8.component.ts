import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
declare var jQuery: any;
import { Location } from '@angular/common';
import { Endorsement } from 'src/app/models/endorsement';
import { FormGroup } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { Observable, of } from 'rxjs';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { HttpClient } from '@angular/common/http';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementService } from 'src/services/endorsement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { EndorsementSectionC } from '../endorsement-section-c';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { EndorsementSectionCForm8 } from './endorsement-section-c-form8';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-endorsement-section-c-form8',
    templateUrl: './endorsement-section-c-form8.component.html',
    styleUrls: ['./endorsement-section-c-form8.component.scss']
})
export class EndorsementSectionCForm8Component extends EndorsementSectionC implements OnInit {
    @Input() id: any;
    page = "endorsement/sectionC-form8/";
    mm: any;

    user: any = {};

    totalValue: any[] = [
        { 'kyat': 0, 'usd': 0 }, //loan_repay_total
        { 'kyat': 0, 'usd': 0 }, //issued_share_total
        { 'kyat': 0, 'usd': 0 }, //other_total
    ];

    endoModel: any;
    endoMMModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    /* loanRepay: BaseForm<string>[] = [
        new FormSelect({
            key: 'origin_id',
            label: 'Origin',
            options$: this.localService.getOrigin(),
            style: {
                'max-width:': '150px;'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateOrigin(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
            }
        }),
        new FormInput({
            key: 'source',
            label: 'Source (Name of company/ individual)',
            style: {
                'min-width': '200px'
            }
        }),
        new FormSelect({
            key: 'currency',
            label: 'Currency',
            options$: this.lotService.getCurrency(),
        }),
        new FormInput({
            key: 'value',
            label: 'Value',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateTotal(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
            }
        }),
        new FormInput({
            key: 'equ_kyat',
            label: 'Equivalent Kyat',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
            }
        }),
        new FormInput({
            key: 'equ_usd',
            label: 'Equivalent US$',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
            }
        })
    ];
    issuedShare: BaseForm<string>[] = [
        new FormSelect({
            key: 'origin_id',
            label: 'Origin',
            options$: this.localService.getOrigin(),
            style: {
                'max-width:': '150px;'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateOrigin(value, index, form, formGroup, 'issued_share', 'issued_share_list');
            }
        }),
        new FormSelect({
            key: 'currency',
            label: 'Currency',
            options$: this.lotService.getCurrency(),
        }),
        new FormInput({
            key: 'value',
            label: 'Value',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateTotal(value, index, form, formGroup, 'issued_share', 'issued_share_list');
            }
        }),
        new FormInput({
            key: 'equ_kyat',
            label: '(Equivalent) Kyat',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'issued_share', 'issued_share_list');
            }
        }),
        new FormInput({
            key: 'equ_usd',
            label: '(Equivalent) US$',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'issued_share', 'issued_share_list');
            }
        })
    ]
    otherSource: BaseForm<string>[] = [
        new FormInput({
            key: 'type',
            label: 'Type',
            style: {
                'min-width': '200px'
            }
        }),
        new FormSelect({
            key: 'origin_id',
            label: 'Origin',
            options$: this.localService.getOrigin(),
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateOrigin(value, index, form, formGroup, 'other_source', 'other_source_list');
            }
        }),
        new FormSelect({
            key: 'currency',
            label: 'Currency',
            options$: this.lotService.getCurrency(),
        }),
        new FormInput({
            key: 'value',
            label: 'Value',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateTotal(value, index, form, formGroup, 'other_source', 'other_source_list');
            }
        }),
        new FormInput({
            key: 'equ_kyat',
            label: 'Equivalent Kyat',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'other_source', 'other_source_list');
            }
        }),
        new FormInput({
            key: 'equ_usd',
            label: 'Equivalent US$',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'other_source', 'other_source_list');
            }
        })
    ]
    capital: BaseForm<string>[] = [
        new FormInput({
            key: 'kyat',
            label: 'Equivalent Kyat',
            columns: 'col-md-6',
            type: 'number',
            required: true,
            readonly: true,
        }),
        new FormInput({
            key: 'usd',
            label: 'Equivalent US$',
            columns: 'col-md-6',
            type: 'number',
            required: true,
            readonly: true,
        })
    ]

    endoForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '3.Particulars of financing of investment'
        }),
        new FormTitle({
            label: 'a. Loans repayable by investment business to finance investment'
        }),
        new BaseFormGroup({
            key: 'loan_repay',
            formGroup: [
                new BaseFormArray({
                    key: 'loan_repay_list',
                    formArray: this.loanRepay,
                    useTable: true,
                    tableFooter: [
                        [
                            { label: 'Total', colSpan: 4 },
                            {
                                cellFn: () => {
                                    return this.totalValue[0].kyat;
                                }
                            },
                            {
                                cellFn: () => {
                                    return this.totalValue[0].usd;
                                }
                            },
                        ]
                    ]
                }),
                new FormDummy({ key: 'total_equ_kyat' }),
                new FormDummy({ key: 'total_equ_usd' })
            ]
        }),

        new FormTitle({
            label: 'b. Issued/paid up share capital to finance investment'
        }),
        new BaseFormGroup({
            key: 'issued_share',
            formGroup: [
                new BaseFormArray({
                    key: 'issued_share_list',
                    formArray: this.issuedShare,
                    useTable: true,
                    tableFooter: [
                        [
                            { label: 'Total', colSpan: 3 },
                            {
                                cellFn: () => {
                                    return this.totalValue[1].kyat;
                                }
                            },
                            {
                                cellFn: () => {
                                    return this.totalValue[1].usd;
                                }
                            },
                        ]
                    ]
                }),
                new FormDummy({ key: 'total_equ_kyat' }),
                new FormDummy({ key: 'total_equ_usd' })
            ]
        }),

        new FormTitle({
            label: 'c. Other sources to finance investment:'
        }),
        new BaseFormGroup({
            key: 'other_source',
            formGroup: [
                new BaseFormArray({
                    key: 'other_source_list',
                    formArray: this.otherSource,
                    useTable: true,
                    tableFooter: [
                        [
                            { label: 'Total', colSpan: 4 },
                            {
                                cellFn: () => {
                                    return this.totalValue[2].kyat;
                                }
                            },
                            {
                                cellFn: () => {
                                    return this.totalValue[2].usd;
                                }
                            },
                        ]
                    ]
                }),
                new FormDummy({ key: 'total_equ_kyat' }),
                new FormDummy({ key: 'total_equ_usd' })
            ]
        }),
        new FormTitle({
            label: 'd. Total:'
        }),
        new BaseFormGroup({
            key: 'total',
            formGroup: [
                new FormTitle({
                    label: 'Myanmar capital',
                }),
                new BaseFormGroup({
                    key: 'myanmar_capital',
                    formGroup: this.capital
                }),
                new FormTitle({
                    label: 'Foreign capital',
                }),
                new BaseFormGroup({
                    key: 'foreign_capital',

                    formGroup: this.capital
                })
            ]
        }),
        new FormTitle({
            label: 'Please upload evidence of the financing of the investment:'
        }),
        new BaseFormArray({
            key: 'finance_evidence_doc',
            formArray: [
                new FormFile({
                    key: 'document',
                    label: 'Name of document:',
                    required: true,
                    multiple: true,
                })
            ]
        })
    ] */

    baseForm: EndorsementSectionCForm8;
    constructor(
        private http: HttpClient,
        private formCtrService: FormControlService,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private router: Router,
        private lotService: LocationService,
        private localService: LocalService,
        public activatedRoute: ActivatedRoute,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        super(formCtrService);
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.baseForm = new EndorsementSectionCForm8(this.mm || 'en', this.lotService, this.localService);
            this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms);
            this.baseForm.attachFormGroup(this.formGroup);

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow                        
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.endoModel.proposed_basic_info = this.endoModel?.proposed_basic_info || {};
                        this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms, this.endoModel, form?.data);
                        this.baseForm.attachFormGroup(this.formGroup);

                        this.baseForm.calculateTotalValue("loan_repay", "loan_repay_list", 0);
                        this.baseForm.calculateTotalValue("issued_share", "issued_share_list", 1);
                        this.baseForm.calculateTotalValue("other_source", "other_source_list", 2);

                        this.page = "endorsement/sectionC-form8/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    /* updateOrigin(value, index, form, formGroup, control, controlList) {
        let formValue = this.formGroup.value;
        formValue[control][controlList][index][form.key] = value;
        this.calculateCapitalValue();
    }

    updateTotal(value, index, form, formGroup, control, controlList) {
        let formValue = this.formGroup.value;
        formValue[control][controlList][index][form.key] = value;

        var i = 0;
        if (control == 'loan_repay') {
            i = 0;
        }
        else if (control == 'issued_share') {
            i = 1;
        }
        else if (control == 'other_source') {
            i = 2;
        }
        this.calculateTotalValue(control, controlList, i);
    }

    calculateTotalValue(control, controlList, index) {
        let formValue = this.formGroup.value;
        this.totalValue[index].kyat = 0;
        this.totalValue[index].usd = 0;

        formValue[control][controlList].map(x => {
            this.totalValue[index].kyat += parseFloat(x.equ_kyat);
            this.totalValue[index].usd += parseFloat(x.equ_usd);

            this.formGroup.get(control).get('total_equ_kyat').setValue(this.totalValue[index].kyat);
            this.formGroup.get(control).get('total_equ_usd').setValue(this.totalValue[index].usd);
        });
        this.calculateCapitalValue();
    }

    calculateCapitalValue() {
        // Number(foreign_total_kyat | 0) + Number(x.equ_kyat | 0)
        var foreign_total_kyat = 0;
        var foreign_total_usd = 0;
        var myanmar_total_kyat = 0;
        var myanmar_total_usd = 0;
        this.formGroup.get('loan_repay').get('loan_repay_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = parseFloat(foreign_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                foreign_total_usd = parseFloat(foreign_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = parseFloat(myanmar_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                myanmar_total_usd = parseFloat(myanmar_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
        });

        this.formGroup.get('issued_share').get('issued_share_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = parseFloat(foreign_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                foreign_total_usd = parseFloat(foreign_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = parseFloat(myanmar_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                myanmar_total_usd = parseFloat(myanmar_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
        });

        this.formGroup.get('other_source').get('other_source_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = parseFloat(foreign_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                foreign_total_usd = parseFloat(foreign_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = parseFloat(myanmar_total_kyat.toString()) + parseFloat(x.equ_kyat.toString());
                myanmar_total_usd = parseFloat(myanmar_total_usd.toString()) + parseFloat(x.equ_usd.toString());
            }
        });

        this.formGroup.get('total').get('foreign_capital').get('kyat').setValue(foreign_total_kyat);
        this.formGroup.get('total').get('foreign_capital').get('usd').setValue(foreign_total_usd);
        this.formGroup.get('total').get('myanmar_capital').get('kyat').setValue(myanmar_total_kyat);
        this.formGroup.get('total').get('myanmar_capital').get('usd').setValue(myanmar_total_usd);

    }

    calculateEquivalent(value, index, form, formGroup, control, controlList) {
        let formValue = this.formGroup.value;
        formValue[control][controlList][index][form.key] = value;

        var i = 0;
        if (control == 'loan_repay') {
            i = 0;
        }
        else if (control == 'issued_share') {
            i = 1;
        }
        else if (control == 'other_source') {
            i = 2;
        }
        this.calculateTotalValue(control, controlList, i);
    } */

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    // saveFormData() {
    //   this.spinner = true;
    //   this.formCtrService.lazyUpload(this.endoForm, this.formGroup, { type_id: this.id }, (formValue) => {
    //     let data = { 'proposed_basic_info': formValue };
    //     this.service.create({ ...this.endoModel, ...data }, { secure: true })
    //       .subscribe(x => {
    //         console.log(x);
    //         this.spinner = false;

    //         if (!this.is_draft) {
    //           if (window.location.href.indexOf('officer') !== -1) {
    //             this.router.navigate(['/officer/endorsement/sectionC-form9', x.id]);
    //           } else {
    //             this.router.navigate(['/pages/endorsement/sectionC-form9', x.id]);
    //           }
    //         }
    //       }, error => {
    //         console.log(error);
    //       });
    //   });

    // }

    saveFormData() {
        this.spinner = true;
        this.formCtrService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue) => {
            this.service.create({ ...this.endoModel, ...formValue }, { secure: true })
                .subscribe(x => {
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionC-form9', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionC-form9', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionC-form8', id, 'mm']);
                }
            }
        }
    }

}
