import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm7Component } from './endorsement-section-c-form7.component';

describe('EndorsementSectionCForm7Component', () => {
  let component: EndorsementSectionCForm7Component;
  let fixture: ComponentFixture<EndorsementSectionCForm7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
