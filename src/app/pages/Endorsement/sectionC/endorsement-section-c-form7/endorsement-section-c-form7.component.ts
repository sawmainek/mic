import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
declare var jQuery: any;
import { Location } from '@angular/common';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { EndorsementService } from 'src/services/endorsement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { ExpectedInvestdt } from 'src/app/models/expected-investdt';
import { Observable } from 'rxjs';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { EndorsementSectionC } from '../endorsement-section-c';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';

@Component({
    selector: 'app-endorsement-section-c-form7',
    templateUrl: './endorsement-section-c-form7.component.html',
    styleUrls: ['./endorsement-section-c-form7.component.scss']
})
export class EndorsementSectionCForm7Component extends EndorsementSectionC implements OnInit {

    @Input() id: any;
    page = "endorsement/sectionC-form7/";
    mm: any;

    user: any = {};
    is_officer: boolean = false;

    cashFormGroup: FormGroup;
    formGroup: FormGroup;
    endoModel: any;
    form: any;
    service: any;

    count: number = 0;
    currencies: any;

    isRevision: boolean = false;

    submitted: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    totalValue: any[] = [
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 }
    ];

    totalCash: any[] = [0, 0, 0];

    new_item: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Brand new items'
        }),
        new BaseFormArray({
            key: 'items',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                    required: true,
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    required: true,
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                    required: true
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    required: true,
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    required: true,
                    value: '102',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                    required: true
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('new_item', 0);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                    { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    recondition_item: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Reconditioned Items'
        }),
        new BaseFormArray({
            key: 'items',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    value: '102',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('recondition_item', 1);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                    { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    raw_material: BaseForm<any>[] = [
        new BaseFormArray({
            key: 'items',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    value: '102',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('raw_material', 2);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                    { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[2].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[2].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    endoForm: BaseForm<string>[] = [
        new FormDummy({
            key: 'machinery_breakdown'
        }),
        new FormDummy({
            key: 'raw_breakdown'
        }),
        new FormDummy({
            key: 'cash_equivalent'
        }),
        new FormInput({
            key: 'total_equivalent_usd',
            label: 'Total amount of Foreign cash to be brough in (Equivalent in USD)',
            // required: true,
        }),
        new FormTitle({
            label: 'Period during which Foreign Cash and Equivalents will be brought in USD'
        }),
        new FormDate({
            key: 'period_start_date',
            label: 'From date',
            // required: true,
            columns: 'col-md-6',
            dateView: 'month'
        }),
        new FormDate({
            key: 'period_end_date',
            label: 'To date',
            // required: true,
            columns: 'col-md-6',
            dateView: 'month'
        }),
        new FormTitle({
            label: 'e. Please provide a breakdown of “Machinery & equipment”, procured locally and imported, using the following format:'
        }),
        new BaseFormGroup({
            key: 'new_item',
            formGroup: this.new_item
        }),
        new BaseFormGroup({
            key: 'recondition_item',
            formGroup: this.recondition_item
        }),
        new FormTitle({
            label: 'f. Please provide a breakdown of “Raw materials”, procured locally and imported, using the following format:'
        }),
        new BaseFormGroup({
            key: 'raw_material',
            formGroup: this.raw_material
        }),
    ]

    constructor(
        private ref: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private router: Router,
        private store: Store<AppState>,
        public location: Location,
        private toast: ToastrService,
        private lotService: LocationService,
        public formCtrService: FormControlService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService

    ) {
        super(formCtrService);
        this.loading = true;

        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;

        this.lotService.getCurrency().subscribe(data => {
            this.currencies = data;
        });

        this.createCashFormGroup();
        this.formGroup = this.formCtrService.toFormGroup(this.endoForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow                        
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        let expectedInvestdt = this.endoModel?.expected_investmentdt || new ExpectedInvestdt;
                        expectedInvestdt = expectedInvestdt.cash_equivalent ? expectedInvestdt : form?.data?.expected_investmentdt

                        this.formGroup = this.formCtlService.toFormGroup(this.endoForm, expectedInvestdt);

                        this.page = "endorsement/sectionC-form7/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        if (expectedInvestdt?.cash_equivalent && expectedInvestdt?.cash_equivalent.length > 0) {

                            for (var i = 2; i < expectedInvestdt.cash_equivalent[0].foreign_origin_list?.length; i++) {
                                this.foreignList(0).push(this.columnForm());
                                this.foreignList(1).push(this.columnForm());
                                this.foreignList(2).push(this.columnForm());
                            }

                            expectedInvestdt.cash_equivalent.forEach((x, i) => {
                                this.cashFormGroup.get('cash_equivalent').patchValue(expectedInvestdt.cash_equivalent);
                            });

                            this.calculateKyat();
                            this.calculateForeign();
                        }

                        this.calculateTotalValue('new_item', 0);
                        this.calculateTotalValue('recondition_item', 1);
                        this.calculateTotalValue('raw_material', 2);

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();

    }

    calculateEquivalent(value, index, form, formGroup, control) {
        let formValue = this.formGroup.value;
        formValue[control]['items'][index][form.key] = value;

        this.calculateTotalValue(control, control == 'new_item' ? 0 : control == 'recondition_item' ? 1 : 2);
    }

    updateFormValue(value, index, form, formGroup, control) {
        console.log(this.formGroup)
        let formValue = this.formGroup.value;
        formValue[control]['items'][index][form.key] = value;

        // Update Total Value
        let rowValue = formGroup.value;
        formGroup.get('total').setValue(Number(rowValue.quantity | 0) * Number(rowValue.price | 0));

        this.calculateTotalValue(control, control == 'new_item' ? 0 : control == 'recondition_item' ? 1 : 2);
    }

    calculateTotalValue(control, index) {
        let formValue = this.formGroup.value;
        this.totalValue[index].total_kyat = 0;
        this.totalValue[index].total_usd = 0;

        formValue[control]['items'].map(x => {
            this.totalValue[index].total_kyat += parseFloat(x.kyat);
            this.totalValue[index].total_usd += parseFloat(x.usd);

            this.formGroup.get(control).get('total_kyat').setValue(this.totalValue[index].total_kyat);
            this.formGroup.get(control).get('total_usd').setValue(this.totalValue[index].total_usd);
        });
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }


    createCashFormGroup() {
        this.cashFormGroup = this.formBuilder.group({
            cash_equivalent: new FormArray(
                [
                    this.formBuilder.group({
                        name: ['Bank balances'],
                        myan_origin: ['0', [Validators.required, Validators.pattern('[0-9, +]+')]],
                        foreign_origin_list: new FormArray([this.columnForm(), this.columnForm()])
                    }),
                    this.formBuilder.group({
                        name: ['Cash on hand'],
                        myan_origin: ['0', [Validators.required, Validators.pattern('[0-9, +]+')]],
                        foreign_origin_list: new FormArray([this.columnForm(), this.columnForm()])
                    }),
                    this.formBuilder.group({
                        name: ['Short term deposits'],
                        myan_origin: ['0', [Validators.required, Validators.pattern('[0-9, +]+')]],
                        foreign_origin_list: new FormArray([this.columnForm(), this.columnForm()])
                    }),
                ],
                [Validators.required]),
        });
    }

    get cashList() {
        return (this.cashFormGroup.get('cash_equivalent') as FormArray).controls;
    }

    foreignList(index) {
        return (this.cashFormGroup.get('cash_equivalent') as FormArray).controls[index].get('foreign_origin_list') as FormArray;
    }

    columnForm() {
        return this.formBuilder.group({
            // amount: ['0', [Validators.required]],
            // currency: ['', Validators.required],
            amount: ['0'],
            currency: [''],
        })
    }

    addColumn() {
        this.foreignList(0).push(this.columnForm());
        this.foreignList(1).push(this.columnForm());
        this.foreignList(2).push(this.columnForm());

        this.totalCash.push(0);
        this.ref.detectChanges();
    }

    removeColumn(index) {
        this.foreignList(0).removeAt(index);
        this.foreignList(1).removeAt(index);
        this.foreignList(2).removeAt(index);

        this.totalCash.splice(index, 1);

        this.ref.detectChanges();
    }

    changeCurrency(index, value) {
        this.foreignList(0).controls[index].get('currency').setValue(value);
        this.foreignList(1).controls[index].get('currency').setValue(value);
        this.foreignList(2).controls[index].get('currency').setValue(value);
    }

    calculateKyat() {
        this.totalCash[0] = Number(this.cashList[0].get('myan_origin').value) +
            Number(this.cashList[1].get('myan_origin').value) +
            Number(this.cashList[2].get('myan_origin').value);
    }

    calculateForeign() {
        this.foreignList(0).controls.forEach((foreign, i) => {
            this.totalCash[i + 1] = Number(this.foreignList(0).controls[i].get('amount').value) +
                Number(this.foreignList(1).controls[i].get('amount').value) +
                Number(this.foreignList(2).controls[i].get('amount').value);
        });
        this.ref.detectChanges();
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    checkDates(group: FormGroup) {
        let start_date = group.get('start_date').value;
        let end_date = group.get('end_date').value;
        let control = group.get('end_date');

        if (control.errors && !control.errors.date) {
            return;
        }
        else {
            if (Date.parse(start_date) > Date.parse(end_date))
                control.setErrors({ date: true });
            else
                control.setErrors(null);
        }
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    save() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.cashFormGroup.invalid || this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        let data = { 'expected_investmentdt': { ...this.formGroup.value, ...this.cashFormGroup.value } };
        this.service.create({ ...this.endoModel, ...data }, { secure: true })
            .subscribe(x => {
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                    this.redirectLink(this.form.id)
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionC-form8', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionC-form8', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionC-form7', id, 'mm']);
                }
            }
        }
    }
}
