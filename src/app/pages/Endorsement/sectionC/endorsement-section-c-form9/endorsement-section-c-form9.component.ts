import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
declare var jQuery: any;
import { Location } from '@angular/common';
import { FormArray, FormGroup } from '@angular/forms';
import { Endorsement } from 'src/app/models/endorsement';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { Observable, of } from 'rxjs';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { HttpClient } from '@angular/common/http';
import { EndorsementService } from 'src/services/endorsement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { EndorsementSectionC } from '../endorsement-section-c';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { LocalService } from 'src/services/local.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';


@Component({
    selector: 'app-endorsement-section-c-form9',
    templateUrl: './endorsement-section-c-form9.component.html',
    styleUrls: ['./endorsement-section-c-form9.component.scss']
})
export class EndorsementSectionCForm9Component extends EndorsementSectionC implements OnInit {
    page = "endorsement/sectionC-form9/";
    @Input() id: any;
    mm: any;

    user: any = {};

    view: any;
    hide: any = true;

    endoModel: any;
    endoMMModel: any;
    formGroup: FormGroup;
    form: any;
    service: any;

    classification: any = ['Managers', 'Professionals', 'Technicians and associate professionals', 'Clerical support workers', 'Service and sales workers', 'Skilled agricultural, forestry and fishery workers', 'Craft and related trades workers', 'Plant and machine operators, and assemblers', 'Elementary occupations'];
    classification_mm: any = ['မန်နေဂျာများ/ အဆင့်မြင့်အရာရှိများ', 'သက်မွေးဝမ်းကျောင်းပညာရှင်များ', 'နည်းပညာနှင့်ဆက်စပ်သည့် သက်မွေးပညာရှင်များ', 'အထောက်အပံ့ပြုစာရေးလုပ်သားများ', 'ဝန်ဆောင်မှုနှင့်အရောင်းလုပ်သားများ', 'လယ်ယာသစ်တောနှင့် ငါးလုပ်ငန်းကျွမ်းကျင်လုပ်သားများ', 'လက်မှုပညာနှင့်ဆက်စပ်လုပ်ငန်းအလုပ်သမားများ', 'စက်ကရိယာနှင့်စက်ပစ္စည်းကိုင်တွယ်အသုံးပြုသူများနှင့် စုစည်းတပ်ဆင်သူများ', 'အခြေခံအလုပ်သမားများ'];

    total_max: any = {};
    total_year: any = [];

    averageSalary: any[] = []
    spinner = false;
    is_draft = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    endoForm: BaseForm<string>[] = [
        new FormSectionTitle({
            'label': '4. Planned employment:'
        }),
        new FormNote({
            label: 'Investors should classify employment according to the International Standard Classification of Occupations (ISCO), known as ISCO-08, by the International Labor Organization (ILO). According to the Myanmar Investment Law Art. 51, investors shall appoint only Myanmar citizens for works which does not require skill.'
        }),

        new BaseFormArray({
            key: 'emp_max',
            formArray: [
                new FormInput({
                    key: 'classification',
                    required: true,
                    readonly: true
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueMax(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueMax(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueMax(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueMax(value, index, form, formGroup);
                    }
                })
            ],
            useTable: true,
            disableAddRow: true,
            defaultLength: this.classification.length,
            tableHeader: [
                [
                    { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'text-align': 'center', 'vertical-align': 'middle', 'min-width': '400px' } },
                    { label: 'Maximum number of employment created over lifetime of investment business', colSpan: 4, style: { 'text-align': 'center' } }
                ],
                [
                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },
                ],
                [
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total employment and average weighted salary' },
                    {
                        cellFn: () => {
                            return this.total_max.mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_max.mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_max.fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_max.fr_citizens_salary_rate_total;
                        }
                    }
                ]
            ]
        }),
        new BaseFormArray({
            key: 'emp_year',
            formArray: [
                new FormInput({
                    key: 'classification',
                    required: true,
                    readonly: true,
                    style: { 'background': 'white', 'position': 'sticky', 'left': '0px', 'z-index': '999' }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_1',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 1);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_1',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 1);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_1',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 1);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_1',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 1);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_2',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 2);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_2',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 2);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_2',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 2);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_2',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 2);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_3',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 3);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_3',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 3);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_3',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 3);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_3',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 3);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_4',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 4);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_4',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 4);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_4',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 4);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_4',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 4);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_5',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 5);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_5',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 5);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_5',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 5);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_5',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 5);
                    }
                }),
            ],
            useTable: true,
            disableAddRow: true,
            defaultLength: this.classification.length,
            tableHeader: [
                [
                    { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'text-align': 'center', 'vertical-align': 'middle', 'min-width': '400px', 'background': 'white', 'position': 'sticky', 'left': '0px', 'z-index': '999' } },
                    { label: 'Newly Recruited Staff Year 1', colSpan: 4, style: { 'text-align': 'center' } },
                    { label: 'Newly Recruited Staff Year 2', colSpan: 4, style: { 'text-align': 'center' } },
                    { label: 'Newly Recruited Staff Year 3', colSpan: 4, style: { 'text-align': 'center' } },
                    { label: 'Newly Recruited Staff Year 4', colSpan: 4, style: { 'text-align': 'center' } },
                    { label: 'Newly Recruited Staff Year 5', colSpan: 4, style: { 'text-align': 'center' } },
                ],
                [
                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },

                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },

                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },

                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },

                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },
                ],
                [
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },

                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },

                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },

                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },

                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total employment and average weighted salary ', style: { 'background': 'white', 'position': 'sticky', 'left': '0px', 'z-index': '999' } },
                    {
                        cellFn: () => {
                            return this.total_year[0].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[0].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[0].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[0].fr_citizens_salary_rate_total;
                        }
                    },

                    {
                        cellFn: () => {
                            return this.total_year[1].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[1].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[1].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[1].fr_citizens_salary_rate_total;
                        }
                    },

                    {
                        cellFn: () => {
                            return this.total_year[2].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[2].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[2].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[2].fr_citizens_salary_rate_total;
                        }
                    },

                    {
                        cellFn: () => {
                            return this.total_year[3].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[3].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[3].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[3].fr_citizens_salary_rate_total;
                        }
                    },

                    {
                        cellFn: () => {
                            return this.total_year[4].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[4].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[4].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[4].fr_citizens_salary_rate_total;
                        }
                    }

                ]
            ]
        }),
        // new FormTitle({
        //     label: 'Average yearly employment over 10-year period:',
        // }),
        // new FormInput({
        //     key: 'myanmar_citizens_avg',
        //     label: 'Myanmar citizens:',
        //     required: true,
        //     columns: 'col-md-6',
        //     readonly: true
        // }),
        // new FormInput({
        //     key: 'foreign_nationals_avg',
        //     label: 'Foreign nationals:',
        //     required: true,
        //     columns: 'col-md-6',
        //     readonly: true
        // }),
        new FormTitle({
            label: 'Please upload plans on social security and welfare plan for employees, including career advancement and employee retirement arrangements:'
        }),
        new FormFile({
            key: 'plan_doc',
            label: 'File Name',
            required: true
        })
    ]

    constructor(
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,

    ) {
        super(formCtrService);
        this.loading = true;

        this.total_max.mm_citizens_qty_total = 0;
        this.total_max.mm_citizens_salary_rate_total = 0;
        this.total_max.fr_citizens_qty_total = 0;
        this.total_max.fr_citizens_salary_rate_total = 0;

        for (let i = 0; i < 5; i++) {
            this.total_year.push({
                mm_citizens_qty_total: 0,
                mm_citizens_salary_rate_total: 0,
                fr_citizens_qty_total: 0,
                fr_citizens_salary_rate_total: 0,
            })
        }

        this.formGroup = this.formCtrService.toFormGroup(this.endoForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.employment_plan = this.endoModel?.employment_plan || form?.data?.employment_plan || {};
                        this.formGroup = this.formCtrService.toFormGroup(this.endoForm, this.endoModel?.employment_plan || {}, form?.data?.employment_plan || {});

                        this.page = "endorsement/sectionC-form9/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', comments }));
                        });

                        if (this.mm == 'mm') {
                            this.classification_mm.forEach((x, i) => {
                                (this.formGroup.get('emp_max') as FormArray).controls[i].get('classification').setValue(x);
                                (this.formGroup.get('emp_year') as FormArray).controls[i].get('classification').setValue(x);
                            });
                        }
                        else {
                            this.classification.forEach((x, i) => {
                                (this.formGroup.get('emp_max') as FormArray).controls[i].get('classification').setValue(x);
                                (this.formGroup.get('emp_year') as FormArray).controls[i].get('classification').setValue(x);
                            });
                        }

                        this.calculateTotal();

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    calculateTotal() {
        this.total_max.mm_citizens_qty_total = 0;
        this.total_max.mm_citizens_salary_rate_total = 0;
        this.total_max.fr_citizens_qty_total = 0;
        this.total_max.fr_citizens_salary_rate_total = 0;

        this.endoModel.employment_plan.emp_max = this.endoModel.employment_plan?.emp_max || [];
        this.endoModel.employment_plan.emp_max.forEach((x, i) => {
            this.total_max.mm_citizens_qty_total += Number(x.myanmar_citizens_qty || 0);
            this.total_max.mm_citizens_salary_rate_total += Number(x.myanmar_citizens_salary_rate || 0);
            this.total_max.fr_citizens_qty_total += Number(x.foreign_nationals_qty || 0);
            this.total_max.fr_citizens_salary_rate_total += Number(x.foreign_nationals_salary_rate || 0);
        });

        for (var i = 0; i < 5; i++) {
            this.total_year[i].mm_citizens_qty_total = 0;
            this.total_year[i].mm_citizens_salary_rate_total = 0;
            this.total_year[i].fr_citizens_qty_total = 0;
            this.total_year[i].fr_citizens_salary_rate_total = 0;
        }

        this.endoModel.employment_plan.emp_year = this.endoModel.employment_plan?.emp_year || [];
        this.endoModel.employment_plan.emp_year.forEach(x => {
            for (var i = 0; i < 5; i++) {
                this.total_year[i].mm_citizens_qty_total += Number(x[`myanmar_citizens_qty_${i + 1}`] || 0);
                this.total_year[i].mm_citizens_salary_rate_total += Number(x[`myanmar_citizens_salary_rate_${i + 1}`] || 0);
                this.total_year[i].fr_citizens_qty_total += Number(x[`foreign_nationals_qty_${i + 1}`] || 0);
                this.total_year[i].fr_citizens_salary_rate_total += Number(x[`foreign_nationals_salary_rate_${i + 1}`] || 0);
            }
        });

    }

    updateFormValueMax(value, index, form: BaseForm<string>, formGroup: FormGroup) {
        let formValue = this.formGroup.value;
        formValue['emp_max'][index][form.key] = value;

        this.total_max.mm_citizens_qty_total = 0;
        this.total_max.mm_citizens_salary_rate_total = 0;
        this.total_max.fr_citizens_qty_total = 0;
        this.total_max.fr_citizens_salary_rate_total = 0;

        (this.formGroup.get('emp_max') as FormArray).controls.forEach((x, i) => {
            this.total_max.mm_citizens_qty_total += Number(x.get('myanmar_citizens_qty').value || 0);
            this.total_max.mm_citizens_salary_rate_total += Number(x.get('myanmar_citizens_salary_rate').value || 0);
            this.total_max.fr_citizens_qty_total += Number(x.get('foreign_nationals_qty').value || 0);
            this.total_max.fr_citizens_salary_rate_total += Number(x.get('foreign_nationals_salary_rate').value || 0);
        });
    }

    updateFormValueYear(value, index, form: BaseForm<string>, formGroup: FormGroup, year) {
        let formValue = this.formGroup.value;
        formValue[`emp_year`][index][form.key] = value;

        this.total_year[year - 1].mm_citizens_qty_total = 0;
        this.total_year[year - 1].mm_citizens_salary_rate_total = 0;
        this.total_year[year - 1].fr_citizens_qty_total = 0;
        this.total_year[year - 1].fr_citizens_salary_rate_total = 0;

        (this.formGroup.get(`emp_year`) as FormArray).controls.forEach((x, i) => {
            this.total_year[year - 1].mm_citizens_qty_total += Number(x.get(`myanmar_citizens_qty_${year}`).value || 0);
            this.total_year[year - 1].mm_citizens_salary_rate_total += Number(x.get(`myanmar_citizens_salary_rate_${year}`).value || 0);
            this.total_year[year - 1].fr_citizens_qty_total += Number(x.get(`foreign_nationals_qty_${year}`).value || 0);
            this.total_year[year - 1].fr_citizens_salary_rate_total += Number(x.get(`foreign_nationals_salary_rate_${year}`).value || 0);
        });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;

        this.formCtrService.lazyUpload(this.endoForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let data = { 'employment_plan': formValue };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionD-form1', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionD-form1', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionC-form9', id, 'mm']);
                }
            }
        }
    }
}
