import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionCForm9Component } from './endorsement-section-c-form9.component';

describe('EndorsementSectionCForm9Component', () => {
  let component: EndorsementSectionCForm9Component;
  let fixture: ComponentFixture<EndorsementSectionCForm9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionCForm9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionCForm9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
