import { formProgress, currentForm } from './../../../core/form/_selectors/form.selectors';
import { AppState } from './../../../core/reducers/index';
import { Store, select } from '@ngrx/store';
import { ReadyToSubmit, FormProgress } from './../../../core/form/_actions/form.actions';
import { EndorsementFormService } from './../../../../services/endorsement-form.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { FormProgressService } from 'src/services/form-progress.service';
import { EndorsementCertificate } from '../endorsement-certificate/endorsement-certificate';
import { EndorsementSectionA } from '../sectionA/endorsement-section-a';
import { EndorsementSectionB } from '../sectionB/endorsement-section-b';
import { EndorsementSectionC } from '../sectionC/endorsement-section-c';
import { EndorsementSectionD } from '../sectionD/endorsement-section-d';
import { EndorsementSectionE } from '../sectionE/endorsement-section-e';
import { EndorsementUndertaking } from '../endorsement-undertaking/endorsement-undertaking';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementChooseLanguage } from '../endorsement-choose-language/endorsement-choose-language';
import { Endorsement } from 'src/app/models/endorsement';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';

@Component({
    selector: 'app-endorsement-sidemenu-mini',
    templateUrl: './endorsement-sidemenu-mini.component.html',
    styleUrls: ['./endorsement-sidemenu-mini.component.scss']
})
export class EndorsementSidemenuMiniComponent implements OnInit {
    @Input() menu: any = "choose-language";
    @Input() progress: any = false;
    @Output() progressForms = new EventEmitter<any>();

    pagesA: any;
    pagesB: any;
    pagesC: any;
    pagesD: any;
    pagesE: any;

    user: any;
    is_officer: boolean = false;

    form: any = {};
    model: any = {};
    endoModel: Endorsement;
    endoMMModel: Endorsement;

    formProgress: any;
    completeSections = [];
    progressLists: any[] = [];

    id: any = 0;
    mm: any;

    constructor(
        private router: Router,
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute,
        private endoFormService: EndorsementFormService,
        private formProgressService: FormProgressService,
        public formCtlService: FormControlService
    ) {
        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            // Check for Officer
            if (this.id > 0) {
                // Get data from state
                this.store.pipe(select(currentForm))
                    .subscribe(data => {
                        if (data.form) {
                            this.form = data.form || {};
                            this.endoModel = this.form?.data || {};
                            this.endoMMModel = this.form?.data_mm || {};
                            this.model = this.form?.language == 'Myanmar' ? this.form?.data_mm || {} : this.form?.data || {};
                            this.createProgressList();
                            this.editTotalForms();
                        } else {
                            // Get data from API
                            this.endoFormService.show(this.id)
                                .subscribe((form: any) => {
                                    this.form = form || {};
                                    this.endoModel = this.form?.data || {};
                                    this.endoMMModel = this.form?.data_mm || {};
                                    this.model = this.form?.language == 'Myanmar' ? this.form?.data_mm || {} : this.form?.data || {};
                                    this.createProgressList();
                                    this.editTotalForms();
                                });
                        }
                    }).unsubscribe();
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    createProgressList() {
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                    this.getFormProgress();
                    this.editTotalForms();
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:Endorsement`
                    }).subscribe(results => {
                        this.progressLists = results;
                        this.getFormProgress();
                        this.editTotalForms();
                        this.store.dispatch(new FormProgress({ progress: this.progressLists }));
                    });
                }
            }).unsubscribe();
    }

    getFormProgress() {
        if (this.progressLists.length > 0) {
            this.formProgress = this.progressLists;
            this.progressForms.emit(this.formProgress);

            let sections = ["Choose Language", "Section A", "Section B", "Section C", "Section D", "Section E", "Certificate", "Undetaking"];
            this.completeSections = sections.map(x => this.isSectionComplete(x));

            //Enable submission button
            if (this.completeSections[0] && this.completeSections[1] && this.completeSections[2] && this.completeSections[3] && this.completeSections[4] && this.completeSections[5]) {
                setTimeout(() => {
                    this.store.dispatch(new ReadyToSubmit({ submission: 'Endorsement' }));
                }, 600);
            }
        }
        else {
            this.progressForms.emit([]);
        }
    }

    checkRevise(section) {
        const revise = this.progressLists.filter(x => x.section == section && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    editTotalForms() {
        if (this.form?.language == 'Myanmar') {
            EndorsementSectionA.totalForm = new EndorsementSectionA(this.formCtlService).totalForm;
            EndorsementSectionB.totalForm = new EndorsementSectionB(this.formCtlService).totalForm;
            EndorsementSectionC.totalForm = new EndorsementSectionC(this.formCtlService).totalForm;
            EndorsementSectionD.totalForm = new EndorsementSectionD(this.formCtlService).totalForm;
            EndorsementSectionE.totalForm = new EndorsementSectionE(this.formCtlService).totalForm;
            EndorsementUndertaking.totalForm = new EndorsementUndertaking(this.formCtlService).totalForm;
        }
        else {
            EndorsementSectionA.totalForm = new EndorsementSectionA(this.formCtlService).totalForm * 2;
            EndorsementSectionB.totalForm = new EndorsementSectionB(this.formCtlService).totalForm * 2;
            EndorsementSectionC.totalForm = new EndorsementSectionC(this.formCtlService).totalForm * 2;
            EndorsementSectionD.totalForm = new EndorsementSectionD(this.formCtlService).totalForm * 2;
            EndorsementSectionE.totalForm = new EndorsementSectionE(this.formCtlService).totalForm * 2;
            EndorsementUndertaking.totalForm = new EndorsementUndertaking(this.formCtlService).totalForm * 2;
        }
    }

    checkPageComplete(page) {
        const length = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel?.id || 0}/mm`).length;
        return length > 0 ? 1 : 0;
    }

    checkCompleteFormA() {
        this.pagesA = [
            `endorsement/sectionA-form4`
        ];

        var totalLength = 0;
        this.pagesA.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormB() {
        this.pagesB = [
            `endorsement/sectionB-form2`,
            `endorsement/sectionB-form3`,           
            `endorsement/sectionB-form4`,
        ];

        var totalLength = 0;
        this.pagesB.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormC() {
        this.pagesC = [
            `endorsement/sectionC-form2`, //0
            `endorsement/sectionC-form3`, //1
            `endorsement/sectionC-form4`, //2
            `endorsement/sectionC-form5`, //3 
            `endorsement/sectionC-form7`, //4
            `endorsement/sectionC-form8`, //5
            `endorsement/sectionC-form9`, //6
        ];

        var totalLength = 0;
        this.pagesC.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormD() {
        this.pagesD = [
            `endorsement/sectionD-form2`, //0
            `endorsement/sectionD-form3`, //1
            `endorsement/sectionD-form4`, //2
            `endorsement/sectionD-form5`, //3 
            `endorsement/sectionD-form6`, //4
            `endorsement/sectionD-form7`, //5
        ];

        var totalLength = 0;
        this.pagesD.forEach((page, index) => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.endoMMModel.id}/mm` : `${page}/${this.endoMMModel.id}/0/mm`)).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.endoModel.id}` : `${page}/${this.endoModel.id}/0`)).length;
                const lengthMM = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.endoMMModel.id}/mm` : `${page}/${this.endoMMModel.id}/0/mm`)).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormE() {
        this.pagesE = [
            `endorsement/sectionE-form2`, //0
            `endorsement/sectionE-form3`, //1
            `endorsement/sectionE-form4`, //2
        ];

        var totalLength = 0;
        this.pagesE.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteUndertaking() {
        this.pagesA = [
            `endorsement/undertaking`
        ];

        var totalLength = 0;
        this.pagesA.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.endoModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    isSectionComplete(menu) {
        let hasComplete = false;
        switch (menu) {
            case EndorsementChooseLanguage.section: {
                if (EndorsementChooseLanguage.totalForm <= this.checkPageComplete('endorsement/endorsement-choose-language')) {
                    hasComplete = true;
                }
                break;
            }
            case EndorsementSectionA.section: {
                if (EndorsementSectionA.totalForm <= this.checkCompleteFormA()) {
                    hasComplete = true;
                }
                break;
            }
            case EndorsementSectionB.section: {
                if (EndorsementSectionB.totalForm <= this.checkCompleteFormB()) {
                    hasComplete = true;
                }
                break;
            }
            case EndorsementSectionC.section: {
                if (EndorsementSectionC.totalForm <= this.checkCompleteFormC()) {
                    hasComplete = true;
                }
                break;
            }
            case EndorsementSectionD.section: {
                if (EndorsementSectionD.totalForm <= this.checkCompleteFormD()) {
                    hasComplete = true;
                }
                break;
            }
            case EndorsementSectionE.section: {
                if (EndorsementSectionE.totalForm <= this.checkCompleteFormE()) {
                    hasComplete = true;
                }
                break;
            }
            case EndorsementCertificate.section: {
                if (this.form?.language == 'Myanmar') {
                    if (EndorsementCertificate.totalForm <= this.checkPageComplete('endorsement/endorsement-certificate')) {
                        hasComplete = true;
                    }
                    break;
                }
                else {
                    hasComplete = true;
                }
                break;
            }
            case EndorsementUndertaking.section: {
                if (EndorsementUndertaking.totalForm <= this.checkCompleteUndertaking()) {
                    hasComplete = true;
                }
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
        return hasComplete;
    }

    openSection(index, link) {
        if(this.allowRevision) {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/' + link, this.form?.id, 'mm']);
            } else {
                this.router.navigate([page + '/endorsement/' + link, this.form?.id]);
            }
        }
    }

    get allowRevision(): boolean {
        if (window.location.href.indexOf('officer') == -1 && this.form?.submission) {
            return false;
        }
        return true;
    }

}
