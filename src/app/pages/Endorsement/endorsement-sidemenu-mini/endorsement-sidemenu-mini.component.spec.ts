import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSidemenuMiniComponent } from './endorsement-sidemenu-mini.component';

describe('EndorsementSidemenuMiniComponent', () => {
  let component: EndorsementSidemenuMiniComponent;
  let fixture: ComponentFixture<EndorsementSidemenuMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSidemenuMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSidemenuMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
