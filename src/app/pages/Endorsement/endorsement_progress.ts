import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';

export abstract class EndorsementProgress {
    static type = "Endorsement";
    private _progressForms: any;

    get progressForms(): any {
        return this._progressForms;
    }

    getProgressForms(data) {
        this._progressForms = data ? data : [];
    }

}