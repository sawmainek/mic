import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSideMenuComponent } from './endorsement-side-menu.component';

describe('EndorsementSideMenuComponent', () => {
  let component: EndorsementSideMenuComponent;
  let fixture: ComponentFixture<EndorsementSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
