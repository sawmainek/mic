import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EndorsementFormComponent } from './endorsement-form/endorsement-form.component';
import { EndorsementSectionAForm1Component } from './sectionA/endorsement-section-a-form1/endorsement-section-a-form1.component';
import { EndorsementSectionAForm2Component } from './sectionA/endorsement-section-a-form2/endorsement-section-a-form2.component';
import { EndorsementSectionBForm1Component } from './sectionB/endorsement-section-b-form1/endorsement-section-b-form1.component';
import { EndorsementSectionAForm3Component } from './sectionA/endorsement-section-a-form3/endorsement-section-a-form3.component';
import { EndorsementSectionBForm2Component } from './sectionB/endorsement-section-b-form2/endorsement-section-b-form2.component';
import { EndorsementSectionBForm3Component } from './sectionB/endorsement-section-b-form3/endorsement-section-b-form3.component';
import { EndorsementSectionCForm1Component } from './sectionC/endorsement-section-c-form1/endorsement-section-c-form1.component';
import { EndorsementSectionCForm2Component } from './sectionC/endorsement-section-c-form2/endorsement-section-c-form2.component';
import { EndorsementSectionCForm3Component } from './sectionC/endorsement-section-c-form3/endorsement-section-c-form3.component';
import { EndorsementSectionDForm1Component } from './sectionD/endorsement-section-d-form1/endorsement-section-d-form1.component';
import { EndorsementSectionDForm2Component } from './sectionD/endorsement-section-d-form2/endorsement-section-d-form2.component';
import { EndorsementSectionDForm3Component } from './sectionD/endorsement-section-d-form3/endorsement-section-d-form3.component';
import { EndorsementSectionDForm4Component } from './sectionD/endorsement-section-d-form4/endorsement-section-d-form4.component';
import { EndorsementSectionEForm1Component } from './sectionE/endorsement-section-e-form1/endorsement-section-e-form1.component';
import { EndorsementSectionEForm2Component } from './sectionE/endorsement-section-e-form2/endorsement-section-e-form2.component';
import { EndorsementSectionEForm3Component } from './sectionE/endorsement-section-e-form3/endorsement-section-e-form3.component';
import { EndorsementSectionEForm4Component } from './sectionE/endorsement-section-e-form4/endorsement-section-e-form4.component';
import { EndorsementTemplateForm5Component } from './endorsement-template/endorsement-template-form5/endorsement-template-form5.component';
import { EndorsementTemplateForm6Component } from './endorsement-template/endorsement-template-form6/endorsement-template-form6.component';
import { EndorsementSuccessComponent } from './endorsement-success/endorsement-success.component';
import { EndorsementSectionCForm4Component } from './sectionC/endorsement-section-c-form4/endorsement-section-c-form4.component';
import { EndorsementSectionCForm5Component } from './sectionC/endorsement-section-c-form5/endorsement-section-c-form5.component';
import { EndorsementSectionCForm6Component } from './sectionC/endorsement-section-c-form6/endorsement-section-c-form6.component';
import { EndorsementSectionCForm7Component } from './sectionC/endorsement-section-c-form7/endorsement-section-c-form7.component';
import { EndorsementSectionCForm8Component } from './sectionC/endorsement-section-c-form8/endorsement-section-c-form8.component';
import { EndorsementSectionCForm9Component } from './sectionC/endorsement-section-c-form9/endorsement-section-c-form9.component';
import { EndorsementUndertakingComponent } from './endorsement-undertaking/endorsement-undertaking.component';
import { EndorsementSectionAForm4Component } from './sectionA/endorsement-section-a-form4/endorsement-section-a-form4.component';
import { EndorsementSectionDForm5Component } from './sectionD/endorsement-section-d-form5/endorsement-section-d-form5.component';
import { EndorsementSectionDForm6Component } from './sectionD/endorsement-section-d-form6/endorsement-section-d-form6.component';
import { EndorsementSectionDForm7Component } from './sectionD/endorsement-section-d-form7/endorsement-section-d-form7.component';
import { EndorsementCertificateComponent } from './endorsement-certificate/endorsement-certificate.component';
import { EndorsementChooseLanguageComponent } from './endorsement-choose-language/endorsement-choose-language.component';
import { EndorsementStateAndRegionComponent } from './endorsement-state-and-region/endorsement-state-and-region.component';
import { EndorsementPaymentComponent } from './endorsement-payment/endorsement-payment.component';
import { EndorsementSectionBForm4Component } from './sectionB/endorsement-section-b-form4/endorsement-section-b-form4.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'form', pathMatch: 'full' },
            { path: 'form', component: EndorsementFormComponent },
            { path: 'form/:id', component: EndorsementFormComponent },

            { path: 'sectionA-form1/:id', component: EndorsementSectionAForm1Component },
            { path: 'sectionA-form1/:id/:mm', component: EndorsementSectionAForm1Component },

            { path: 'sectionA-form2/:id', component: EndorsementSectionAForm2Component },
            { path: 'sectionA-form2/:id/:mm', component: EndorsementSectionAForm2Component },

            { path: 'sectionA-form3/:id', component: EndorsementSectionAForm3Component },
            { path: 'sectionA-form3/:id/:mm', component: EndorsementSectionAForm3Component },

            { path: 'sectionA-form4/:id', component: EndorsementSectionAForm4Component },
            { path: 'sectionA-form4/:id/:mm', component: EndorsementSectionAForm4Component },

            { path: 'sectionB-form1/:id', component: EndorsementSectionBForm1Component },
            { path: 'sectionB-form1/:id/:mm', component: EndorsementSectionBForm1Component },

            { path: 'sectionB-form2/:id', component: EndorsementSectionBForm2Component },
            { path: 'sectionB-form2/:id/:mm', component: EndorsementSectionBForm2Component },

            { path: 'sectionB-form3/:id', component: EndorsementSectionBForm3Component },
            { path: 'sectionB-form3/:id/:mm', component: EndorsementSectionBForm3Component },

            { path: 'sectionB-form4/:id', component: EndorsementSectionBForm4Component },
            { path: 'sectionB-form4/:id/:mm', component: EndorsementSectionBForm4Component },

            { path: 'sectionC-form1/:id', component: EndorsementSectionCForm1Component },
            { path: 'sectionC-form1/:id/:mm', component: EndorsementSectionCForm1Component },

            { path: 'sectionC-form2/:id', component: EndorsementSectionCForm2Component },
            { path: 'sectionC-form2/:id/:mm', component: EndorsementSectionCForm2Component },

            { path: 'sectionC-form3/:id', component: EndorsementSectionCForm3Component },
            { path: 'sectionC-form3/:id/:mm', component: EndorsementSectionCForm3Component },

            { path: 'sectionC-form4/:id', component: EndorsementSectionCForm4Component },
            { path: 'sectionC-form4/:id/:mm', component: EndorsementSectionCForm4Component },

            { path: 'sectionC-form5/:id', component: EndorsementSectionCForm5Component },
            { path: 'sectionC-form5/:id/:mm', component: EndorsementSectionCForm5Component },

            { path: 'sectionC-form6/:id', component: EndorsementSectionCForm6Component },
            { path: 'sectionC-form6/:id/:mm', component: EndorsementSectionCForm6Component },

            { path: 'sectionC-form7/:id', component: EndorsementSectionCForm7Component },
            { path: 'sectionC-form7/:id/:mm', component: EndorsementSectionCForm7Component },

            { path: 'sectionC-form8/:id', component: EndorsementSectionCForm8Component },
            { path: 'sectionC-form8/:id/:mm', component: EndorsementSectionCForm8Component },

            { path: 'sectionC-form9/:id', component: EndorsementSectionCForm9Component },
            { path: 'sectionC-form9/:id/:mm', component: EndorsementSectionCForm9Component },

            { path: 'sectionD-form1/:id', component: EndorsementSectionDForm1Component },
            { path: 'sectionD-form1/:id/:mm', component: EndorsementSectionDForm1Component },

            { path: 'sectionD-form2/:id', component: EndorsementSectionDForm2Component },
            { path: 'sectionD-form2/:id/:mm', component: EndorsementSectionDForm2Component },

            { path: 'sectionD-form3/:id/:index', component: EndorsementSectionDForm3Component },
            { path: 'sectionD-form3/:id/:index/:mm', component: EndorsementSectionDForm3Component },

            { path: 'sectionD-form4/:id/:index', component: EndorsementSectionDForm4Component },
            { path: 'sectionD-form4/:id/:index/:mm', component: EndorsementSectionDForm4Component },

            { path: 'sectionD-form5/:id/:index', component: EndorsementSectionDForm5Component },
            { path: 'sectionD-form5/:id/:index/:mm', component: EndorsementSectionDForm5Component },

            { path: 'sectionD-form6/:id/:index', component: EndorsementSectionDForm6Component },
            { path: 'sectionD-form6/:id/:index/:mm', component: EndorsementSectionDForm6Component },

            { path: 'sectionD-form7/:id/:index', component: EndorsementSectionDForm7Component },
            { path: 'sectionD-form7/:id/:index/:mm', component: EndorsementSectionDForm7Component },

            { path: 'sectionE-form1/:id', component: EndorsementSectionEForm1Component },
            { path: 'sectionE-form1/:id/:mm', component: EndorsementSectionEForm1Component },

            { path: 'sectionE-form2/:id', component: EndorsementSectionEForm2Component },
            { path: 'sectionE-form2/:id/:mm', component: EndorsementSectionEForm2Component },

            { path: 'sectionE-form3/:id', component: EndorsementSectionEForm3Component },
            { path: 'sectionE-form3/:id/:mm', component: EndorsementSectionEForm3Component },

            { path: 'sectionE-form4/:id', component: EndorsementSectionEForm4Component },
            { path: 'sectionE-form4/:id/:mm', component: EndorsementSectionEForm4Component },

            { path: 'template-form5/:id', component: EndorsementTemplateForm5Component },
            { path: 'template-form5/:id/:mm', component: EndorsementTemplateForm5Component },

            { path: 'template-form6/:id', component: EndorsementTemplateForm6Component },
            { path: 'template-form6/:id/:mm', component: EndorsementTemplateForm6Component },

            { path: 'undertaking/:id', component: EndorsementUndertakingComponent },
            { path: 'undertaking/:id/:mm', component: EndorsementUndertakingComponent },

            { path: 'success/:id', component: EndorsementSuccessComponent },
            { path: 'success/:id/:mm', component: EndorsementSuccessComponent },
            
            // { path: 'endorsement-sectionA-form4/:id', component: EndorsementSectionAForm4Component },
            // { path: 'endorsement-sectionA-form4/:id/:mm', component: EndorsementSectionAForm4Component },

            // { path: 'endorsement-sectionA-form5/:id', component: EndorsementSectionAForm5Component },
            // { path: 'endorsement-sectionA-form5/:id/:mm', component: EndorsementSectionAForm5Component },

            // { path: 'endorsement-sectionD-form5/:id', component: EndorsementSectionDForm5Component },
            // { path: 'endorsement-sectionD-form5/:id/:mm', component: EndorsementSectionDForm5Component },

            // { path: 'endorsement-sectionD-form6/:id', component: EndorsementSectionDForm6Component },
            // { path: 'endorsement-sectionD-form6/:id/:mm', component: EndorsementSectionDForm6Component },

            // { path: 'endorsement-sectionD-form7/:id', component: EndorsementSectionDForm7Component },
            // { path: 'endorsement-sectionD-form7/:id/:mm', component: EndorsementSectionDForm7Component },

            { path: 'endorsement-certificate/:id', component: EndorsementCertificateComponent },
            { path: 'endorsement-certificate/:id/:mm', component: EndorsementCertificateComponent },

            { path: 'endorsement-choose-language/:id', component: EndorsementChooseLanguageComponent },
            { path: 'endorsement-choose-language/:id/:mm', component: EndorsementChooseLanguageComponent },

            { path: 'endorsement-state-and-region/:id', component: EndorsementStateAndRegionComponent },
            { path: 'endorsement-state-and-region/:id/:mm', component: EndorsementStateAndRegionComponent },

            { path: 'payment/:id', component: EndorsementPaymentComponent },
            { path: 'payment/:id/:mm', component: EndorsementPaymentComponent },
        ]
    },
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class EndorsementRoutingModule { }
