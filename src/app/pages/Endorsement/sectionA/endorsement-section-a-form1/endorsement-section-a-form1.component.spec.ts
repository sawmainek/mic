import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionAForm1Component } from './endorsement-section-a-form1.component';

describe('EndorsementSectionAForm1Component', () => {
  let component: EndorsementSectionAForm1Component;
  let fixture: ComponentFixture<EndorsementSectionAForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionAForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionAForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
