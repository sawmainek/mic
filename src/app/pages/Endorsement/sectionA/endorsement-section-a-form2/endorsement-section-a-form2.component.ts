import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { EndorsementSectionA } from '../endorsement-section-a';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { EndorsementSectionAForm2 } from './endorsement-section-a-form2';


@Component({
    selector: 'app-endorsement-section-a-form2',
    templateUrl: './endorsement-section-a-form2.component.html',
    styleUrls: ['./endorsement-section-a-form2.component.scss']
})
export class EndorsementSectionAForm2Component extends EndorsementSectionA implements OnInit {
    @Input() id: any;
    page = "endorsement/sectionA-form2/";
    mm: any;

    user: any = {};

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    baseForm: EndorsementSectionAForm2;

    isRevision: boolean = false;

    constructor(
        private router: Router,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService

    ) {
        super(formCtrService);
        this.loading = true;

        this.getEndorsementData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getEndorsementData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.baseForm = new EndorsementSectionAForm2();
            this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms);
            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms, this.endoModel, form?.data || {});

                        this.page = "endorsement/sectionA-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveEndorsementInfo();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;

        this.saveEndorsementInfo();
    }

    saveEndorsementInfo() {
        this.spinner = true;
        this.endoModel = { ...this.endoModel, ...this.formGroup.value };

        this.service.create(this.endoModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                    this.redirectLink(this.form.id)
                } else {
                    this.toast.success('Saved successfully.');
                }

            }, error => {
                console.log(error);
            });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionA-form3', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionA-form3', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionA-form2', id, 'mm']);
                }
            }
        }
    }
}
