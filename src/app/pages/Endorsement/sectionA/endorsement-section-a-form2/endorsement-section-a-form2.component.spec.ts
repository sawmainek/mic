import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionAForm2Component } from './endorsement-section-a-form2.component';

describe('EndorsementSectionAForm2Component', () => {
  let component: EndorsementSectionAForm2Component;
  let fixture: ComponentFixture<EndorsementSectionAForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionAForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionAForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
