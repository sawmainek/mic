import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';

export class EndorsementSectionAForm2 {
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new FormTitle({
                label: '1. Has the enterprise been legally established in Myanmar yet?',
                required: true
            }),
            new FormRadio({
                key: 'overview_is_established',
                label: 'Yes',
                value: 'Yes',
                required: true,
                columns: 'col-md-4'
            }),
            new FormRadio({
                key: 'overview_is_established',
                label: 'No',
                value: 'No',
                required: true,
                columns: 'col-md-8'
            }),
            new FormNote({
                label: 'If the investor has not yet legally established the enterprise, the natural or legal person responsible for establishing such enterprise may submit the proposal as an investor. The establishment as the enterprise under the law is a condition of being issued the permit and shall not alter any of the obligations of the investor under the Myanmar Investment Law.'
            })
        ];
    }
}