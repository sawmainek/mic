import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionAForm3Component } from './endorsement-section-a-form3.component';

describe('EndorsementSectionAForm3Component', () => {
  let component: EndorsementSectionAForm3Component;
  let fixture: ComponentFixture<EndorsementSectionAForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionAForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionAForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
