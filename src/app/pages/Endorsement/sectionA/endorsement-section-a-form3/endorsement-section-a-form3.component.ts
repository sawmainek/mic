import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementSectionA } from '../endorsement-section-a';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { AuthService } from 'src/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { EndorsementSectionAForm3 } from './endorsement-section-a-form3';


@Component({
    selector: 'app-endorsement-section-a-form3',
    templateUrl: './endorsement-section-a-form3.component.html',
    styleUrls: ['./endorsement-section-a-form3.component.scss']
})
export class EndorsementSectionAForm3Component extends EndorsementSectionA implements OnInit {
    @Input() id: any;
    page = "endorsement/sectionA-form3/";
    mm: any;

    user: any = {};

    endoModel: any;
    endoMMModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_next = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    /* forms: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'invest_form',
            formGroup: [
                new FormSectionTitle({
                    label: '2. Form of investment'
                }),
                new FormTitle({
                    label: 'a. Please select the investment form of the enterprise, which has been established or which will be established:'
                }),
                new FormRadio({
                    key: 'form_investment',
                    label: 'Wholly Myanmar owned investment',
                    tooltip: 'An enterprise owned wholly by Myanmar.',
                    value: 'Wholly Myanmar owned investment',
                    required: true,
                    style: { 'margin-bottom': '10px' }
                }),
                new FormRadio({
                    key: 'form_investment',
                    label: 'Wholly Foreign owned investment',
                    tooltip: 'An enterprise owned wholly by foreign.',
                    value: 'Wholly Foreign owned investment',
                    style: { 'margin-bottom': '10px' },
                    required: true
                }),
                new FormRadio({
                    key: 'form_investment',
                    label: 'Myanmar citizen investment (up to 35% foreign shareholding)',
                    tooltip: 'An enterprise by Myanmar citizen with up to 35% foreign shareholding.',
                    value: 'Myanmar citizen investment',
                    required: true,
                    style: { 'margin-bottom': '10px' }
                }),
                new FormInput({
                    key: 'myan_direct_share_percent',
                    label: 'Please specify the percentage of direct shareholding or interest proportion by Foreign citizens',
                    type: 'number',
                    required: true,
                    validators: [Validators.min(0), Validators.max(35)],
                    criteriaValue: {
                        key: 'form_investment',
                        value: 'Myanmar citizen investment'
                    }
                }),
                new FormRadio({
                    key: 'form_investment',
                    label: 'Joint Venture (more than 35% foreign shareholding)',
                    tooltip: 'An enterprise by Myanmar citizen and foreign with more than 35% foreign shareholding.',
                    value: 'Joint Venture',
                    required: true,
                    // style: { 'margin-bottom': '10px'}
                }),
                new FormInput({
                    key: 'joint_direct_share_percent',
                    label: 'Please specify the percentage of direct shareholding or interest proportion by Foreign citizens',
                    type: 'number',
                    required: true,
                    validators: [Validators.min(35), Validators.max(100)],
                    criteriaValue: {
                        key: 'form_investment',
                        value: 'Joint Venture'
                    }
                }),
                new FormTitle({
                    // label: 'Please upload the (draft) of Joint Venture agreement:',
                    label: 'If the proposed investment should be submitted from/through the respective government departments, please upload a recommendation letter of the respective ministry here:',
                    criteriaValue: {
                        key: 'form_investment',
                        value: 'Joint Venture'
                    }
                }),
                new FormFile({
                    key: 'joint_document',
                    label: 'File Name',
                    required: true,
                    acceptFiles: '.pdf',
                    criteriaValue: {
                        key: 'form_investment',
                        value: 'Joint Venture'
                    }
                }),
                new FormNote({
                    label: 'The Myanmar Investment Rule 44 described the situations which are required to submit the approval from/through the relevant government department/organization.'
                }),
                
                // new FormTextArea({
                //     key: 'special_type',
                //     label: 'b. Please specify if the investment includes a special type of contractual agreement (e.g. Build Operate Transfer (BOT), Profit Sharing Contract, Production Sharing Contract, etc.):',
                // }),
                // new FormTitle({
                //     label: 'Please upload the draft contract / agreement:'
                // }),
                // new BaseFormArray({
                //     key: 'contract_document',
                //     formArray: [
                //         new FormFile({
                //             key: 'contract_document_doc',
                //             label: 'Name of document:',
                //             multiple: true
                //         })
                //     ],
                // }),
                // new FormTextArea({
                //     key: 'other_type',
                //     label: 'endo-c. Please specify if the investment involves any other type of approval/agreement or partnership/contract with the government:',
                // }),
                // new FormTitle({
                //     label: 'endo-Please upload a copy of the document of the respective ministry/department here:'
                // }),
                // new BaseFormArray({
                //     key: 'ministry_document',
                //     formArray: [
                //         new FormFile({
                //             key: 'ministry_document_doc',
                //             label: 'Name of document:',
                //             multiple: true
                //         })
                //     ],
                // }),
                // new FormNote({
                //     label: 'If the proposed investment is related with the Myanmar Investment Rule 44, approval from the relevant government department/organization is required.'
                // })
            ]
        })
    ] */
    baseForm: EndorsementSectionAForm3;
    constructor(
        private router: Router,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService

    ) {
        super(formCtlService);
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.baseForm = new EndorsementSectionAForm3();
            this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        if (this.endoModel?.overview_is_established === 'No' || this.endoModel?.invest_form?.form_investment !== 'Wholly Myanmar owned investment') {
                            this.is_next = true;
                        }
                        else {
                            this.is_next = false;
                        }

                        this.next_label = !this.is_next && this.mm ? "NEXT SECTION" : "NEXT";

                        this.endoModel.invest_form = this.endoModel?.invest_form || {};
                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.endoModel, form.data || {});

                        this.page = "endorsement/sectionA-form3/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();

        this.formGroup.get('invest_form').get('form_investment').valueChanges.subscribe(val => {
            if (this.endoModel?.overview_is_established === 'No' || val !== 'Wholly Myanmar owned investment') {
                this.is_next = true;
            }
            else {
                this.is_next = false;
            }
            this.next_label = !this.is_next && this.mm ? "NEXT SECTION" : "NEXT";
        });
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveEndorsementInfo();
    }

    saveDraft() {
        this.is_draft = true;
        this.saveEndorsementInfo();
    }

    saveEndorsementInfo() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue) => {
            this.endoModel = { ...this.endoModel, ...formValue };
            this.service.create(this.endoModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        })
    }

    // For New Flow 
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        let route = "";
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar' || this.mm) {
                if (this.endoModel.overview_is_established === 'No' || this.endoModel.invest_form.form_investment !== 'Wholly Myanmar owned investment') {
                    route = "sectionA-form4";
                }
                else {
                    route = "sectionB-form1";
                }
            }
            else {
                route = "sectionA-form3";
            }

            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/' + route, id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/' + route, id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/' + route, id, 'mm']);
                }
            }
        }
    }
}
