import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

export class EndorsementSectionAForm4 {

    public forms: BaseForm<string>[] = []

    constructor(
        private lang: string,
        private lotService: LocationService,
        private localService: LocalService
    ) {
        let individual: BaseForm<string>[] = [
            new FormTitle({
                label: 'Details on the investor(s) responsible for establishing the enterprise:'
            }),
            new FormInput({
                key: 'full_name',
                label: 'a. Full Name',
                required: true,
            }),
            new FormSelect({
                key: 'citizenship',
                label: 'b. Citizenship ',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar',
            }),
            new FormInput({
                key: 'passport_or_nrc',
                label: 'c. Passport number (foreign nationals) or National Registration Card number (Myanmar citizens) ',
                required: true,
                columns: 'col-md-12'
            }),
            new FormTitle({
                label: 'Please upload a copy of passport (foreign nationals) or National Registration Card (Myanmar citizens):'
            }),
            new FormFile({
                key: 'copy_of_nrc_doc',
                label: 'File Name',
                required: true,
            }),
            new FormTitle({
                label: 'Please upload bank statements or other evidence of financial standing(in Myanmar language or in English):'
            }),
            new BaseFormArray({
                key: 'bank_statements',
                formArray: [
                    new FormFile({
                        key: 'bank_statement_doc',
                        label: 'Name of document:',
                        multiple: true,
                        required: true,
                    })
                ],
                defaultLength: 1
            }),
            new FormTitle({
                label: 'd. Address ',
            }),
            new FormSelect({
                key: 'individual_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'individual_state',
                label: 'State / Region:',
                options$: this.lotService.getState(this.lang),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_district',
                label: 'District:',
                options$: this.lotService.getDistrict(this.lang),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_township',
                label: 'Township:',
                options$: this.lotService.getTownship(this.lang),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'individual_address',
                label: 'Additional address details',
                required: true
            }),
            new FormInput({
                key: 'phone_no',
                label: 'e. Phone number ',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'f. E-mail address',
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
            new FormTextArea({
                key: 'business_expertise',
                label: 'g. Please describe your sectoral and regional business expertise',
                required: true,
            }),
        ];

        let company: BaseForm<string>[] = [
            new FormTitle({
                label: 'Details on the investor(s) responsible for establishing the enterprise:'
            }),
            new FormInput({
                key: 'company_name',
                label: 'a. Name ',
                required: true,
            }),
            new FormSelect({
                key: 'incorporate_country',
                label: 'b. Country of incorporation',
                options$: this.lotService.getCountry(),
                required: true,
            }),
            new FormInput({
                key: 'company_type',
                label: 'c. Type of company',
                required: true,
                columns: 'col-md-12'
            }),
            new FormInput({
                key: 'certificate_no',
                label: 'Company Registration Certificate Number',
                required: true,
            }),
            new FormTitle({
                label: 'Please upload a copy of the Certificate of Incorporation/company registration certificate:'
            }),
            new FormFile({
                key: 'company_certificate_doc',
                label: 'File Name',
                required: true,
            }),
            new FormTitle({
                label: 'Please upload the most recent annual audit report and other evidence of the financial conditions of the business (in Myanmar language or English):'
            }),
            new BaseFormArray({
                key: 'recent_annual_audit',
                formArray: [
                    new FormFile({
                        key: 'recent_annual_audit_doc',
                        label: 'Name of document:',
                        multiple: true,
                        required: true,
                    })
                ],
                defaultLength: 1
            }),
            new FormTitle({
                label: 'd. Address ',
            }),
            new FormSelect({
                key: 'company_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'company_state',
                label: 'State / Region:',
                options$: this.lotService.getState(this.lang),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_district',
                label: 'District:',
                options$: this.lotService.getDistrict(this.lang),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_township',
                label: 'Township:',
                options$: this.lotService.getTownship(this.lang),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'company_address',
                label: 'Additional address details',
                required: true
            }),
            new FormInput({
                key: 'phone_no',
                label: 'e. Phone number ',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'fax_no',
                label: 'f. Fax number',
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'g. E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'website',
                label: 'h. Website',
                columns: 'col-md-6'
            }),
            new FormTextArea({
                key: 'short_description',
                label: 'i. Short description of company',
                required: true
            }),
            new FormTitle({
                label: 'j. Parent / Holding company',
            }),
            new FormInput({
                key: 'parent_company_name',
                label: 'Name',
                required: true,
            }),
            new FormTitle({
                label: 'Address',
            }),
            new FormSelect({
                key: 'parent_company_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'parent_company_state',
                label: 'State / Region:',
                options$: this.lotService.getState(this.lang),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'parent_company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'parent_company_district',
                label: 'District:',
                options$: this.lotService.getDistrict(this.lang),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'parent_company_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'parent_company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'parent_company_township',
                label: 'Township:',
                options$: this.lotService.getTownship(this.lang),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'parent_company_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'parent_company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'parent_company_address',
                label: 'Additional address details',
                required: true
            }),
            new FormTitle({
                label: 'k. Contact person'
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                type: 'number',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let investor: BaseForm<string>[] = [
            new FormSectionTitleCounter({
                label: 'Investor'
            }),
            new FormSelect({
                key: 'investor_type',
                options$: this.localService.getType1(this.lang),
                label: 'Type',
                required: true
            }),
            new BaseFormGroup({
                key: 'individual',
                formGroup: individual,
                criteriaValue: {
                    key: 'investor_type',
                    value: 'Individual'
                }
            }),
            new BaseFormGroup({
                key: 'company',
                formGroup: company,
                criteriaValue: {
                    key: 'investor_type',
                    value: 'Company'
                }
            })
        ]

        this.forms = [
            new FormSectionTitle({
                label: '1. Please give an overview of the investor(s) responsible for establishing the enterprise:'
            }),
            new BaseFormArray({
                key: 'investor_list',
                label: 'Add Investor',
                formArray: investor
            })
        ];
    }
}