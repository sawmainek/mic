import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionAForm4Component } from './endorsement-section-a-form4.component';

describe('EndorsementSectionAForm4Component', () => {
  let component: EndorsementSectionAForm4Component;
  let fixture: ComponentFixture<EndorsementSectionAForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionAForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionAForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
