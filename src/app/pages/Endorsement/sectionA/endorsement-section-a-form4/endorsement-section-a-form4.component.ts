import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
declare var jQuery: any;
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { EndorsementService } from 'src/services/endorsement.service';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormGroup, Validators } from '@angular/forms';
import { Endorsement } from 'src/app/models/endorsement';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { Observable } from 'rxjs';
import { EndorsementSectionA } from '../endorsement-section-a';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { AuthService } from 'src/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { LocalService } from 'src/services/local.service';
import { EndorsementSectionAForm4 } from './endorsement-section-a-form4';

@Component({
    selector: 'app-endorsement-section-a-form4',
    templateUrl: './endorsement-section-a-form4.component.html',
    styleUrls: ['./endorsement-section-a-form4.component.scss']
})
export class EndorsementSectionAForm4Component extends EndorsementSectionA implements OnInit {
    @Input() id: any;
    page = "endorsement/sectionA-form4/";
    mm: any;

    user: any = {};

    types: any = [];

    endoModel: Endorsement;
    endoMMModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    /* individual: BaseForm<string>[] = [
        new FormInput({
            key: 'full_name',
            label: 'a. Full Name',
            required: true,
        }),
        new FormInput({
            key: 'citizenship',
            label: 'b. Country of Incorporation or citizenship',
            required: true,
        }),
        new FormInput({
            key: 'passport_or_nrc',
            label: 'c. Passport number (foreign nationals) or National Registration Card number (Myanmar citizens) ',
            required: true,
            columns: 'col-md-12'
        }),
        new FormTitle({
            label: 'Please upload a copy of passport (foreign nationals) or National Registration Card (Myanmar citizens):'
        }),
        new FormFile({
            key: 'copy_of_nrc_doc',
            label: 'File Name',
            required: true,
        }),
        new FormTitle({
            label: 'Please upload bank statements or other evidence of financial standing(in Myanmar language or in English):'
        }),
        new BaseFormArray({
            key: 'bank_statements',
            formArray: [
                new FormFile({
                    key: 'bank_statement_doc',
                    label: 'Name of document:',
                    multiple: true,
                    required: true,
                })
            ],
            defaultLength: 1
        }),
        new FormInput({
            key: 'address',
            label: 'd. Address ',
            required: true
        }),
        new FormSelect({
            key: 'individual_state',
            label: 'e. State / Region',
            columns: 'col-md-6',
            options$: this.lotService.getStateRegion(),
            required: true,
            filter: {
                parent: 'individual_country',
                key: 'country'
            }
        }),
        new FormSelect({
            key: 'individual_country',
            label: 'f. Country',
            options$: this.lotService.getCountry(),
            columns: 'col-md-6',
            required: true,
            value: 'Myanmar'
        }),
        new FormInput({
            key: 'phone_no',
            label: 'g. Phone number ',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'h. E-mail address',
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
        new FormTextArea({
            key: 'business_expertise',
            label: 'i. Please describe your sectoral and regional business expertise',
            required: true,
        }),
    ]
    company: BaseForm<string>[] = [
        new FormInput({
            key: 'company_name',
            label: 'a. Name ',
            required: true,
        }),
        new FormSelect({
            key: 'incorporate_country',
            label: 'b. Country of incorporation',
            options$: this.lotService.getCountry(),
            required: true,
        }),
        new FormTitle({
            label: 'Please upload a copy of the Certificate of Incorporation/company registration certificate:'
        }),
        new FormFile({
            key: 'company_certificate_doc',
            label: 'File Name',
            required: true,
        }),
        new FormTitle({
            label: 'Please upload the most recent annual audit report and other evidence of the financial conditions of the business (in Myanmar language or English):'
        }),
        new BaseFormArray({
            key: 'recent_annual_audit',
            formArray: [
                new FormFile({
                    key: 'recent_annual_audit_doc',
                    label: 'Name of document:',
                    multiple: true,
                    required: true,
                })
            ],
            defaultLength: 1
        }),
        new FormInput({
            key: 'company_type',
            label: 'c. Type of company',
            required: true,
            columns: 'col-md-12'
        }),
        new FormInput({
            key: 'address',
            label: 'd. Address ',
            required: true,
        }),
        new FormSelect({
            key: 'company_state',
            label: 'e. State / Region',
            columns: 'col-md-6',
            options$: this.lotService.getStateRegion(),
            required: true,
            filter: {
                parent: 'company_country',
                key: 'country'
            }
        }),
        new FormSelect({
            key: 'company_country',
            label: 'f. Country',
            options$: this.lotService.getCountry(),
            columns: 'col-md-6',
            required: true,
            value: 'Myanmar'
        }),
        new FormInput({
            key: 'phone_no',
            label: 'g. Phone number ',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'fax_no',
            label: 'h. Fax number',
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'i. E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'website',
            label: 'j. Website',
            columns: 'col-md-6'
        }),
        new FormTextArea({
            key: 'short_description',
            label: 'k. Short description of company',
            required: true
        }),
        new FormTitle({
            label: 'l. Parent / Holding company',
        }),
        new FormInput({
            key: 'parent_company_name',
            label: 'Name',
            required: true,
        }),
        new FormInput({
            key: 'parent_company_address',
            label: 'Address',
            required: true
        }),
        new FormSelect({
            key: 'parent_company_state',
            label: 'State / Region',
            columns: 'col-md-6',
            options$: this.lotService.getStateRegion(),
            required: true,
            filter: {
                parent: 'parent_company_country',
                key: 'country'
            }
        }),
        new FormSelect({
            key: 'parent_company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            columns: 'col-md-6',
            required: true,
            value: 'Myanmar'
        }),
        new FormTitle({
            label: 'm. Contact person'
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_phone_no',
            label: 'Phone number',
            type: 'number',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ]
    government: BaseForm<string>[] = [
        new FormInput({
            key: 'name',
            label: 'a. Name of government department / organization',
            required: true
        }),
        new FormInput({
            key: 'address',
            label: 'b. Address',
            required: true
        }),
        new FormSelect({
            key: 'government_state',
            label: 'c. State / Region',
            columns: 'col-md-6',
            options$: this.lotService.getStateRegion(),
            required: true,
            filter: {
                parent: 'government_country',
                key: 'country'
            }
        }),
        new FormSelect({
            key: 'government_country',
            label: 'd. Country',
            options$: this.lotService.getCountry(),
            columns: 'col-md-6',
            required: true,
            value: 'Myanmar'
        }),
        new FormInput({
            key: 'phone_no',
            label: 'e. Phone number ',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'fax',
            label: 'f. Fax number',
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'g. E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'website',
            label: 'h. Website',
            columns: 'col-md-6'
        }),
        new FormTitle({
            label: 'i. Contact person',
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full Name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_phone_no',
            label: 'Phone number',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ]
    investor: BaseForm<string>[] = [
        new FormSectionTitleCounter({
            label: 'Investor'
        }),
        new FormSelect({
            key: 'investor_type',
            options$: this.localService.getType(),
            label: 'Type',
            required: true
        }),
        // new FormSelect({
        //     key: 'country',
        //     label: 'Citizenship or country of incorporation/ registration/ origin',
        //     tooltip: 'Citizenship or country of incorporation/ registration/ origin',
        //     options$: this.lotService.getCountry(),
        //     required: true,
        //     criteriaValue: {
        //         key: 'investor_type',
        //         value: 'Individual'
        //     }
        // }),
        new BaseFormGroup({
            key: 'individual',
            formGroup: this.individual,
            criteriaValue: {
                key: 'investor_type',
                value: 'Individual'
            }
        }),
        new BaseFormGroup({
            key: 'company',
            formGroup: this.company,
            criteriaValue: {
                key: 'investor_type',
                value: 'Company'
            }
        }),
        new BaseFormGroup({
            key: 'government',
            formGroup: this.government,
            criteriaValue: {
                key: 'investor_type',
                value: 'Government department/organization'
            }
        })
    ]
    endoForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '3. Details of the investor(s) responsible for establishing the enterprise:'
        }),

        new BaseFormArray({
            key: 'investor_list',
            formArray: this.investor
        })
    ] */
    baseForm: EndorsementSectionAForm4;
    constructor(
        private router: Router,
        private lotService: LocationService,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private localService: LocalService
    ) {
        super(formCtlService);
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.baseForm = new EndorsementSectionAForm4(this.mm || 'en', this.lotService, this.localService);
            this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.endoModel.investor_list = this.endoModel?.investor_list || [];
                        // this.formGroup = this.formCtlService.toFormGroup(this.endoForm, this.endoModel?.investor_list || [], form?.data?.investor_list || []);
                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.endoModel, form.data || {});

                        this.page = "endorsement/sectionA-form4/";
                        this.page += this.mm ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        var lan = this.mm || 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue) => {
            this.endoModel = { ...this.endoModel, ...formValue };
            this.service.create(this.endoModel, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }

                }, error => {
                    console.log(error);
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionB-form1', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionB-form1', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionA-form4', id, 'mm']);
                }
            }
        }


    }
}
