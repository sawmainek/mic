import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementProgress } from '../endorsement_progress';

export class EndorsementSectionA extends EndorsementProgress {
    static section = "Section A";
    static totalForm = 1;
    totalForm = 1;
    constructor(public formCtlService: FormControlService) {
        super();
    }

    saveFormProgress(form_id: number, id: number, page: string) {
        if (this.progressForms?.length > 0) {
            if (this.progressForms.filter(x => x.type == EndorsementProgress.type && x.section == EndorsementSectionA.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: EndorsementProgress.type,
                    type_id: id,
                    section: EndorsementSectionA.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }

        }
        else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: EndorsementSectionA.type,
                type_id: id,
                section: EndorsementSectionA.section,
                page: page,
                status: true,
                revise: false
            });
        }
    }
}