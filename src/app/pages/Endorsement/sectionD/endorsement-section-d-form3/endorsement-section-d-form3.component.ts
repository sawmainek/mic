import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Location } from '@angular/common';
import { EndorsementSectionD } from '../endorsement-section-d';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { cloneDeep } from 'lodash';
import { LocationService } from 'src/services/location.service';


@Component({
    selector: 'app-endorsement-section-d-form3',
    templateUrl: './endorsement-section-d-form3.component.html',
    styleUrls: ['./endorsement-section-d-form3.component.scss']
})
export class EndorsementSectionDForm3Component extends EndorsementSectionD implements OnInit {
    @Input() id: any;
    menu: any = "sectionD";
    page = "endorsement/sectionD-form3/";
    mm: any;

    user: any = {};

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    index: any = 0;
    previous_owner: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    endorsementForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location ' + (this.index + 1),
            style: { 'font-size': '18px' }
        }),
        new FormTitle({
            label: 'a. Owner of land and/or building(s)'
        }),
        new FormTitle({
            label: 'Please upload evidence of land ownership/ land use rights (unless location is in an Industrial Zone):'
        }),
        new FormFile({
            key: 'ownership_document',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'If the current owner has not changed the name of the former owner on the ownership evidence, please upload contract/agreement (e.g. special power, general power, etc.) between former owner and current owner:'
        }),
        new FormFile({
            key: 'agreement_document',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'i. Period of land grant validity / initial period permitted to use the land'
        }),
        new FormDate({
            key: 'permit_from',
            label: 'From date',
            columns: 'col-md-6',
        }),
        new FormDate({
            key: 'permit_to',
            label: 'To date',
            columns: 'col-md-6',
        }),
        new FormSelect({
            key: 'owner_type',
            label: 'ii. Type of owner',
            options$: this.localService.getType(),
            required: true
        }),
        new FormInput({
            key: 'individual_name',
            label: 'iii. Full Name',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new FormInput({
            key: 'nrc',
            label: 'iv. National Registration Card No.',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new FormTitle({
            label: 'v. Address',
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new FormSelect({
            key: 'individual_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar',
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new FormSelect({
            key: 'individual_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'individual_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'individual_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'individual_township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'individual_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'individual_address',
            label: 'Additional address details',
            required: true,
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new FormInput({
            key: 'phone',
            label: "vi. Phone number",
            required: true,
            type: 'number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new FormInput({
            key: 'email',
            label: 'vii. E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new FormInput({
            key: 'company_name',
            label: 'iii. Name of company',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: 'Company'
            }
        }),
        new FormInput({
            key: 'register_no',
            label: 'iv. Company Registration No.',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: 'Company'
            }
        }),
        new FormTitle({
            label: 'v. Address',
            criteriaValue: {
                key: 'owner_type',
                value: 'Company'
            }
        }),
        new FormSelect({
            key: 'company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar',
            criteriaValue: {
                key: 'owner_type',
                value: 'Company'
            }
        }),
        new FormSelect({
            key: 'company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'company_address',
            label: 'Additional address details',
            required: true,
            criteriaValue: {
                key: 'owner_type',
                value: 'Company'
            }
        }),
        new FormInput({
            key: 'government_name',
            label: 'iii. Name of government department / organization',
            required: true,
            criteriaValue: {
                key: 'owner_type',
                value: 'Government department/organization'
            }
        }),
        new FormTitle({
            label: 'iv. Address',
            criteriaValue: {
                key: 'owner_type',
                value: 'Government department/organization'
            }
        }),
        new FormSelect({
            key: 'government_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar',
            criteriaValue: {
                key: 'owner_type',
                value: 'Government department/organization'
            }
        }),
        new FormSelect({
            key: 'government_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'government_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'government_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'government_township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'government_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'government_address',
            label: 'Additional address details',
            required: true,
            criteriaValue: {
                key: 'owner_type',
                value: 'Government department/organization'
            }
        }),
        new FormTitle({
            label: 'vi. Contact person',
            criteriaValue: {
                key: 'owner_type',
                value: 'Company'
            }
        }),
        new FormTitle({
            label: 'v. Contact person',
            criteriaValue: {
                key: 'owner_type',
                value: 'Government department/organization'
            }
        }),
        new FormInput({
            key: 'person_name',
            label: 'Full Name',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: ['Government department/organization', 'Company']
            }
        }),
        new FormInput({
            key: 'person_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: ['Government department/organization', 'Company']
            }
        }),
        new FormInput({
            key: 'person_phone',
            label: 'Phone number',
            required: true,
            type: 'number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: ['Government department/organization', 'Company']
            }
        }),
        new FormInput({
            key: 'person_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6',
            criteriaValue: {
                key: 'owner_type',
                value: ['Government department/organization', 'Company']
            }
        })
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private localService: LocalService,
        private lotService: LocationService

    ) {
        super(formCtrService);
    }

    ngOnInit(): void {
        this.formGroup = this.formCtrService.toFormGroup(this.endorsementForm);
        this.loading = true;
        this.getEndorsementData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    getEndorsementData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.index = +paramMap.get('index');
            this.endorsementForm[2].label = 'Location ' + (this.index + 1);

            // For New Flow
            this.service = this.mm && this.mm == 'mm' ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm && this.mm == 'mm' ? form.data_mm || {} : form.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.owner = this.endoModel?.owner || form?.data?.owner || [];
                        this.endoModel.owner[this.index] = this.endoModel.owner[this.index] || form?.data?.owner && form?.data?.owner[this.index] || {};

                        this.formGroup = this.formCtlService.toFormGroup(this.endorsementForm, this.endoModel?.owner[this.index], form?.data?.owner && form?.data?.owner[this.index] || {});

                        this.page = "endorsement/sectionD-form3/";
                        this.page += this.mm && this.mm == 'mm' ? (this.endoModel.id + '/' + this.index + '/mm') : (this.endoModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var owner_type = this.endorsementForm.filter(x => x.key == "owner_type")[0];
        owner_type.options$ = this.localService.getType(lan);

        this.authService.changeLanguage$.subscribe(x => {
            owner_type.options$ = this.localService.getType(x);
        })
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    checkDates(group: FormGroup) {
        let permit_from = group.get('permit_from').value;
        let permit_to = group.get('permit_to').value;
        let permit_control = group.get('permit_to');
        if (permit_control.errors && !permit_control.errors.date) {
            return;
        }
        else {
            if (Date.parse(permit_from) > Date.parse(permit_to))
                permit_control.setErrors({ date: true });
            else
                permit_control.setErrors(null);
        }
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;

        this.formCtrService.lazyUpload(this.endorsementForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let owner = this.endoModel.owner ? this.endoModel.owner.map(x => Object.assign({}, x)) || [] : [];
            owner[this.index] = formValue;

            let data = { 'owner': owner };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionD-form4', id, this.index, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/endorsement/sectionD-form4', id, this.index]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionD-form3', id, this.index, 'mm']);
                }
            }
        }        
    }
}
