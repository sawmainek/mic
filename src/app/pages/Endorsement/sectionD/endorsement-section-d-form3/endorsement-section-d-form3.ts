import { BaseFormArray } from './../../../../custom-component/dynamic-forms/base/form-array';
import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

export class EndorsementSectionDForm3{
    
    public forms: BaseForm<string>[] = [];
    index: any = 0;

    constructor(
        private lang: string,
        private lotService: LocationService,
        private localService: LocalService,
    ) {
        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new FormSectionTitle({
                        label: '2. Location of investment and lease agreements'
                    }),
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location ' + (this.index + 1),
                        style: { 'font-size': '18px' }
                    }),
                    new FormTitle({
                        label: 'a. Owner of land and/or building(s)'
                    }),
                    new FormTitle({
                        label: 'Please upload evidence of land ownership/ land use rights (unless location is in an Industrial Zone):'
                    }),
                    new FormFile({
                        key: 'ownership_document',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'If the current owner has not changed the name of the former owner on the ownership evidence, please upload contract/agreement (e.g. special power, general power, etc.) between former owner and current owner:'
                    }),
                    new FormFile({
                        key: 'agreement_document',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'i. Period of land grant validity / initial period permitted to use the land'
                    }),
                    new FormDate({
                        key: 'permit_from',
                        label: 'From date',
                        columns: 'col-md-6',
                    }),
                    new FormDate({
                        key: 'permit_to',
                        label: 'To date',
                        columns: 'col-md-6',
                    }),
                    new FormSelect({
                        key: 'owner_type',
                        label: 'ii. Type of owner',
                        options$: this.localService.getType(),
                        required: true
                    }),
                    new FormInput({
                        key: 'individual_name',
                        label: 'iii. Full Name',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),
                    new FormInput({
                        key: 'nrc',
                        label: 'iv. National Registration Card No.',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),
                    new FormTitle({
                        label: 'v. Address',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),
                    new FormSelect({
                        key: 'individual_country',
                        label: 'Country',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),
                    new FormSelect({
                        key: 'individual_state',
                        label: 'State / Region:',
                        options$: this.lotService.getState(),
                        required: true,
                        columns: 'col-md-4',
                        criteriaValue: {
                            key: 'individual_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'individual_district',
                        label: 'District:',
                        options$: this.lotService.getDistrict(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'individual_state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'individual_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'individual_township',
                        label: 'Township:',
                        options$: this.lotService.getTownship(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'individual_district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'individual_country',
                            value: ['Myanmar'],
                        }
                    }),        
                    new FormInput({
                        key: 'individual_address',
                        label: 'Additional address details',
                        required: true,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),  
                    new FormInput({
                        key: 'phone',
                        label: "vi. Phone number",
                        required: true,
                        type: 'number',
                        validators: [Validators.minLength(6)],
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),
                    new FormInput({
                        key: 'email',
                        label: 'vii. E-mail address',
                        required: true,
                        validators: [Validators.email],
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),
                    new FormInput({
                        key: 'company_name',
                        label: 'iii. Name of company',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Company'
                        }
                    }),
                    new FormInput({
                        key: 'register_no',
                        label: 'iv. Company Registration No.',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Company'
                        }
                    }),
                    new FormTitle({
                        label: 'v. Address',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Company'
                        }
                    }),
                    new FormSelect({
                        key: 'company_country',
                        label: 'Country',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Company'
                        }
                    }),
                    new FormSelect({
                        key: 'company_state',
                        label: 'State / Region:',
                        options$: this.lotService.getState(),
                        required: true,
                        columns: 'col-md-4',
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'company_district',
                        label: 'District:',
                        options$: this.lotService.getDistrict(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'company_state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'company_township',
                        label: 'Township:',
                        options$: this.lotService.getTownship(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'company_district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormInput({
                        key: 'company_address',
                        label: 'Additional address details',
                        required: true,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Company'
                        }
                    }),  
                    new FormInput({
                        key: 'government_name',
                        label: 'iii. Name of government department / organization',
                        required: true,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Government department/organization'
                        }
                    }),
                    new FormTitle({
                        label: 'iv. Address',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Government department/organization'
                        }
                    }),
                    new FormSelect({
                        key: 'government_country',
                        label: 'Country',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Government department/organization'
                        }
                    }),
                    new FormSelect({
                        key: 'government_state',
                        label: 'State / Region:',
                        options$: this.lotService.getState(),
                        required: true,
                        columns: 'col-md-4',
                        criteriaValue: {
                            key: 'government_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'government_district',
                        label: 'District:',
                        options$: this.lotService.getDistrict(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'government_state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'government_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'government_township',
                        label: 'Township:',
                        options$: this.lotService.getTownship(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'government_district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'government_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormInput({
                        key: 'government_address',
                        label: 'Additional address details',
                        required: true,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Government department/organization'
                        }
                    }),  
                    new FormTitle({
                        label: 'vi. Contact person',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Company'
                        }
                    }),
                    new FormTitle({
                        label: 'v. Contact person',
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Government department/organization'
                        }
                    }),
                    new FormInput({
                        key: 'person_name',
                        label: 'Full Name',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: ['Government department/organization', 'Company']
                        }
                    }),
                    new FormInput({
                        key: 'person_position',
                        label: 'Position',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: ['Government department/organization', 'Company']
                        }
                    }),
                    new FormInput({
                        key: 'person_phone',
                        label: 'Phone number',
                        required: true,
                        type: 'number',
                        validators: [Validators.minLength(6)],
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: ['Government department/organization', 'Company']
                        }
                    }),
                    new FormInput({
                        key: 'person_email',
                        label: 'E-mail address',
                        required: true,
                        validators: [Validators.email],
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'owner_type',
                            value: ['Government department/organization', 'Company']
                        }
                    })
                ]
            })
            
        ];
    }

}