import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionDForm3Component } from './endorsement-section-d-form3.component';

describe('EndorsementSectionDForm3Component', () => {
  let component: EndorsementSectionDForm3Component;
  let fixture: ComponentFixture<EndorsementSectionDForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionDForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionDForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
