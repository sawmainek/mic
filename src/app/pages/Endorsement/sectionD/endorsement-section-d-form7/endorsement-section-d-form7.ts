import { BaseFormArray } from './../../../../custom-component/dynamic-forms/base/form-array';
import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

export class EndorsementSectionDForm7{
    
    public forms: BaseForm<string>[] = []

    constructor(
        private lotService: LocationService,
        private localService: LocalService,
    ) {
        let individual: BaseForm<any>[] = [
            new FormInput({
                key: 'individual_name',
                label: '(2) Full name',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'nrc',
                label: '(3) National Registration Card No.',
                columns: 'col-md-6'
            }),
            new FormTitle({
                label: '(4) Address',
            }),
            new FormSelect({
                key: 'individual_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'individual_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),        
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormInput({
                key: 'phone',
                label: '(5) Phone number',
                validators: [Validators.minLength(6)],
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: '(6) E-mail address',
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];
    
        let company: BaseForm<any>[] = [
            new FormInput({
                key: 'company_name',
                label: '(2) Name of company',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'card_no',
                label: '(3) Company Registration No.',
                columns: 'col-md-6'
            }),
            new FormTitle({
                label: '(4) Address',
            }),
            new FormSelect({
                key: 'company_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'company_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormTitle({
                label: '(5) Contact person'
            }),
            new FormInput({
                key: 'person_name',
                label: 'Full name',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'person_position',
                label: 'Position',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'person_phone',
                label: 'Phone number',
                validators: [Validators.minLength(6)],
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'person_email',
                label: 'E-mail address',
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];
    
        let government: BaseForm<any>[] = [
            new FormInput({
                key: 'government_name',
                label: '(2) Name of government department / organization',
            }),
            new FormTitle({
                label: '(3) Address',
            }),
            new FormSelect({
                key: 'government_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'government_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'government_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'government_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'government_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'government_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormTitle({
                label: '(4) Contact Person'
            }),
            new FormInput({
                key: 'person_name',
                label: 'Full name',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'person_position',
                label: 'Position',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'person_phone',
                label: 'Phone number',
                validators: [Validators.minLength(6)],
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'person_email',
                label: 'E-mail address',
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];
        
        this.forms = [
            new BaseFormArray({
                key: 'agreement',
                formArray: [
                    new FormSectionTitle({
                        label: '2. Location of investment and lease agreements'
                    }),
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location 1',
                        style: { 'font-size': '18px' }
                    }),
                    new FormTitle({
                        label: 'e. Particulars on lease agreement'
                    }),
                    new FormNote({
                        label: 'If the investor is investing in her/his own land, please leave Question 2e blank and go to next.'
                    }),
                    new FormTitle({
                        label: 'Please upload the draft of the lease agreement:'
                    }),
                    new FormFile({
                        key: 'agreement_document',
                        label: 'File Name',
                    }),
                    new FormNote({
                        label: 'Registered final copy should be submitted to DICA once received or after receiving the permit.'
                    }),
                    new FormInput({
                        key: 'lessee_name',
                        label: 'i. Name of lessee',
                    }),
                    new FormTitle({
                        label: 'ii. Proposed lease period'
                    }),
                    new FormDate({
                        key: 'lease_from',
                        label: 'From Date',
                        columns: 'col-md-6',
                    }),
                    new FormDate({
                        key: 'lease_to',
                        label: 'To Date',
                        columns: 'col-md-6',
                    }),
                    new FormSectionTitle({
                        label: 'iii. Specify fees(s)'
                    }),
                    new FormInput({
                        key: 'rental_fees',
                        label: 'aa. Annual rental fees of land and/or buildings',
                        type: 'number',
                    }),
                    new FormInput({
                        key: 'fees_of_square',
                        label: 'bb. Rental fees of 1 square meter per year of land and/or buildings',
                        type: 'number',
                    }),
                    new FormInput({
                        key: 'premium_fees',
                        label: 'cc. Land Use Premium fees (rate per acre):',
                        type: 'number',
                    }),
                    new FormNote({
                        label: 'If the land and/or buildings belong to a Government Department/Organization, the LUP shall be paid in cash (Kyat) by the lessee.'
                    }),
                    new FormTitle({
                        label: 'dd. Is the payment or part of the payment in form of capital?'
                    }),
                    new FormRadio({
                        key: 'is_payment',
                        label: 'Yes',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'is_payment',
                        label: 'No',
                        value: 'No',
                        required: true,
                        columns: 'col-md-8'
                    }),
                    new FormInput({
                        key: 'description',
                        label: 'Describe specifically to choosing yes',
                        criteriaValue: {
                            key: 'is_payment',
                            value: 'Yes'
                        }
                    }),
                    new FormSectionTitle({
                        label: 'iv. In case of sublease (i.e. if the owner is not the lessor of the land and/or buildings):',
                    }),
                    new FormSectionTitle({
                        label: 'aa. Particulars on the lessor :'
                    }),
                    new FormTitle({
                        label: 'Please upload evidence of the lessor’s right to use land and/or buildings:'
                    }),
                    new FormFile({
                        key: 'building_document',
                        label: 'File Name',
                    }),
                    new FormNote({
                        label: 'General power/ special power including the stamp from contract registration office should be uploaded. If subleasing from another investor, upload their Land Rights Authorization.',
                    }),
                    new FormSelect({
                        key: 'owner_type',
                        label: '(1) Type of lessor',
                        options$: this.localService.getType(),
                    }),
                    new BaseFormGroup({
                        key: 'individual',
                        formGroup: individual,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),
                    new BaseFormGroup({
                        key: 'company',
                        formGroup: company,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Company'
                        }
                    }),
                    new BaseFormGroup({
                        key: 'government',
                        formGroup: government,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Government department/organization'
                        }
                    }),
                    new FormTitle({
                        label: 'bb. Does the owner agree to the lease agreement?'
                    }),
                    new FormRadio({
                        key: 'is_agree',
                        label: 'Yes',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'is_agree',
                        label: 'No',
                        value: 'No',
                        required: true,
                        columns: 'col-md-8'
                    }),
                    new FormTitle({
                        label: 'cc. If subleasing state-owned land or buildings, please select which of the following apply to the lessor:',
                        // criteriaValue: {
                        //     key: 'is_agree',
                        //     value: 'No'
                        // }
                    }),
                    new FormRadio({
                        key: 'lessor',
                        label: 'A person who has previously obtained the right to use the state-owned and or buildings from the government department and government organization in accordance with the laws of the Union.',
                        value: '1',
                        // criteriaValue: {
                        //     key: 'is_agree',
                        //     value: 'No'
                        // }
                    }),
                    new FormRadio({
                        key: 'lessor',
                        label: 'A person authorized to sub-lease or sub-license the state-owned land or building in accordance with the approval of the government department and government organization.',
                        value: '2',
                        // criteriaValue: {
                        //     key: 'is_agree',
                        //     value: 'No'
                        // }
                    }),
                    new FormRadio({
                        key: 'lessor',
                        label: 'Not applicable',
                        value: '3',
                        // criteriaValue: {
                        //     key: 'is_agree',
                        //     value: 'No'
                        // }
                    }),
                    new FormSectionTitle({
                        label: 'All the data for location 1 is completed now. If you have another location, click the button below:'
                    }),
                ]
            })
            
        ];
    }
}