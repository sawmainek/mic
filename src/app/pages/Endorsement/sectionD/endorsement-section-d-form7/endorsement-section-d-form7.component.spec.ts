import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionDForm7Component } from './endorsement-section-d-form7.component';

describe('EndorsementSectionDForm7Component', () => {
  let component: EndorsementSectionDForm7Component;
  let fixture: ComponentFixture<EndorsementSectionDForm7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionDForm7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionDForm7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
