import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { EndorsementSectionD } from '../endorsement-section-d';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';


@Component({
    selector: 'app-endorsement-section-d-form4',
    templateUrl: './endorsement-section-d-form4.component.html',
    styleUrls: ['./endorsement-section-d-form4.component.scss']
})

export class EndorsementSectionDForm4Component extends EndorsementSectionD implements OnInit {
    menu: any = "sectionD";
    page = "endorsement/sectionD-form4/";
    @Input() id: any;
    mm: any = null;

    user: any = {};

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    radio: any;
    index: any = 0;
    previous_locations: any = [];

    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    endorsementForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location 1',
            style: { 'font-size': '18px' }
        }),
        new FormTitle({
            label: 'b. Location'
        }),
        new FormTitle({
            label: 'i. Address'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-6'
        }),
        new FormSelect({
            key: 'district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-6',
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-6',
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'ward',
            label: 'Ward:',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details:',
            required: true,
        }),
        new FormTitle({
            label: 'ii. Please indicate if the location is in a relevant business zone:'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Not in a business zone',
            value: 'Not in a business zone',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Industrial zone',
            value: 'Industrial',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Hotel zone',
            value: 'Hotel',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Trade zone',
            value: 'Trade',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Other',
            value: 'Other',
            required: true,
            columns: 'col-md-8'
        }),
        new FormInput({
            key: 'zone_description',
            label: 'Describe chosen zone specifically',
            required: true,
            columns: 'col-md-12',
            criteriaValue: {
                key: 'zone',
                value: ['Industrial', 'Hotel', 'Trade', 'Other']
            }
        })
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private lotService: LocationService,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private ref: ChangeDetectorRef,
        private toast: ToastrService,
        private store: Store<AppState>,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
    ) {
        super(formCtrService);
        this.loading = true;
        this.formGroup = this.formCtrService.toFormGroup(this.endorsementForm);
        this.getEndorsementData();
        this.getCurrentUser();
    }

    ngOnInit(): void { }

    getEndorsementData() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.index = +paramMap.get('index');
            this.endorsementForm[2].label = 'Location ' + (this.index + 1);

            // For New Flow
            this.service = this.mm && this.mm == 'mm' ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm && this.mm == 'mm' ? form.data_mm || {} : form.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.location = this.endoModel?.location || form?.data?.location || [];
                        this.endoModel.location[this.index] = this.endoModel?.location[this.index] || form?.data?.location && form?.data?.location[this.index] || {};

                        this.formGroup = this.formCtlService.toFormGroup(this.endorsementForm, this.endoModel?.location[this.index], form?.data?.location && form?.data?.location[this.index] || {});

                        this.page = "endorsement/sectionD-form4/";
                        this.page += this.mm && this.mm == 'mm' ? (this.endoModel.id + '/' + this.index + '/mm') : (this.endoModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var state = this.endorsementForm.filter(x => x.key == "state")[0];
        state.options$ = this.lotService.getState(lan);

        var district = this.endorsementForm.filter(x => x.key == "district")[0];
        district.options$ = this.lotService.getDistrict(lan);

        var township = this.endorsementForm.filter(x => x.key == "township")[0];
        township.options$ = this.lotService.getTownship(lan);

        this.authService.changeLanguage$.subscribe(x => {
            state.options$ = this.lotService.getState(x);
            district.options$ = this.lotService.getDistrict(x);
            township.options$ = this.lotService.getTownship(x);
        })
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        let location = this.endoModel.location ? this.endoModel.location.map(x => Object.assign({}, x)) || [] : [];
        location[this.index] = this.formGroup.value;

        let data = { 'location': location };
        this.service.create({ ...this.endoModel, ...data }, { secure: true })
            .subscribe(x => {
                this.spinner = false;

                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                    this.redirectLink(this.form.id)
                } else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionD-form5', id, this.index, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/endorsement/sectionD-form5', id, this.index]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionD-form4', id, this.index, 'mm']);
                }
            }
        }
    }
}
