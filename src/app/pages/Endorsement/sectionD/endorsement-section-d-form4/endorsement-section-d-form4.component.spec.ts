import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionDForm4Component } from './endorsement-section-d-form4.component';

describe('EndorsementSectionDForm4Component', () => {
  let component: EndorsementSectionDForm4Component;
  let fixture: ComponentFixture<EndorsementSectionDForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionDForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionDForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
