import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionDForm1Component } from './endorsement-section-d-form1.component';

describe('EndorsementSectionDForm1Component', () => {
  let component: EndorsementSectionDForm1Component;
  let fixture: ComponentFixture<EndorsementSectionDForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionDForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionDForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
