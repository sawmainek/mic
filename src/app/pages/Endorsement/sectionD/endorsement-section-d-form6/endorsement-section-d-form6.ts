import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';

export class EndorsementSectionDForm6 {

    public forms: BaseForm<string>[] = []

    constructor(
        private localService: LocalService
    ) {
        let building: BaseForm < any > [] =[
            new FormSectionTitleCounter({
                label: "Building"
            }),
            new FormInput({
                key: 'area',
                label: 'Area',
                required: true,
                type: 'number',
                columns: 'col-md-4',
            }),
            new FormSelect({
                key: 'unit',
                label: 'Unit of measurement',
                required: true,
                options$: this.localService.getUnit(),
                columns: 'col-md-4',
            }),
            new FormInput({
                key: 'value_building',
                label: 'Value of Building in MMK',
                type: 'number',
                required: true,
                columns: 'col-md-4',
            }),
            new FormInput({
                key: 'detail_regarding',
                label: 'Details regarding the construction (is construction complete? If not, who is responsible for construction?)',
                required: true,
            }),
            new BaseFormGroup({
                key: 'building_support_docs',
                formGroup: [
                    new FormFile({
                        label: 'Please upload a building map/drawing/concept here:',
                        key: 'building_docs',
                        required: true,
                    }),
                    new FormTitle({
                        label: 'If building is complete, submit a photo:'
                    }),
                    new FormFile({
                        label: 'File Name',
                        key: 'building_photo'
                    }),
                    new FormTitle({
                        label: 'Other supporting documents',
                    }),
                    new BaseFormArray({
                        key: 'other_docs',
                        formArray: [
                            new FormFile({
                                key: 'document',
                                label: 'Name of document:',
                                multiple: true,
                            }),
                        ],
                    }),
                    new FormTitle({
                        label: ' '
                    })
                ]
            })
        ];

        this.forms = [
            new BaseFormArray({
                key: 'building',
                formArray: [
                    new FormSectionTitle({
                        label: '2.Location of investment and lease agreements'
                    }),
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location 1',
                        style: { 'font-size': '18px' }
                    }),
                    new FormTitle({
                        label: 'd. Particulars on buildings used/leased'
                    }),
                    new BaseFormArray({
                        key: 'building',
                        addMoreText: 'Add more buildings',
                        formArray: building,
                    })
                ]
            })
            
        ];
    }
}