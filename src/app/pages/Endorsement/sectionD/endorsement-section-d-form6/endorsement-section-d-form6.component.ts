import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { EndorsementSectionD } from '../endorsement-section-d';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { LocalService } from 'src/services/local.service';
import { cloneDeep } from 'lodash';


@Component({
    selector: 'app-endorsement-section-d-form6',
    templateUrl: './endorsement-section-d-form6.component.html',
    styleUrls: ['./endorsement-section-d-form6.component.scss']
})
export class EndorsementSectionDForm6Component extends EndorsementSectionD implements OnInit {
    @Input() id: any;
    menu: any = "sectionD";
    page = "endorsement/sectionD-form6/";
    mm: any;

    user: any = {};

    edit: any = false;

    index: any = 0;
    previous_building: any;

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    building: BaseForm<any>[] = [
        new FormSectionTitleCounter({
            label: "Building"
        }),
        new FormInput({
            key: 'area',
            label: 'Area',
            required: true,
            type: 'number',
            columns: 'col-md-4',
        }),
        new FormSelect({
            key: 'unit',
            label: 'Unit of measurement',
            required: true,
            options$: this.localService.getUnit(),
            columns: 'col-md-4',
        }),
        new FormInput({
            key: 'value_building',
            label: 'Value of Building in MMK',
            type: 'number',
            required: true,
            columns: 'col-md-4',
        }),
        new FormInput({
            key: 'detail_regarding',
            label: 'Details regarding the construction (is construction complete? If not, who is responsible for construction?)',
            required: true,
        }),
        new BaseFormGroup({
            key: 'building_support_docs',
            formGroup: [
                new FormFile({
                    label: 'Please upload a building map/drawing/concept here:',
                    key: 'building_docs',
                    required: true,
                }),
                new FormTitle({
                    label: 'If building is complete, submit a photo:'
                }),
                new FormFile({
                    label: 'File Name',
                    key: 'building_photo'
                }),
                new FormTitle({
                    label: 'Other supporting documents',
                }),
                new BaseFormArray({
                    key: 'other_docs',
                    formArray: [
                        new FormFile({
                            key: 'document',
                            label: 'Name of document:',
                            multiple: true,
                        }),
                    ],
                }),
                new FormTitle({
                    label: ' '
                })
            ]
        })
    ];

    endorsementForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2.Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location 1',
            style: { 'font-size': '18px' }
        }),
        new FormTitle({
            label: 'd. Particulars on buildings used/leased'
        }),
        new BaseFormArray({
            key: 'building',
            addMoreText: 'Add more buildings',
            formArray: this.building,
        }),
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private localService: LocalService
    ) {
        super(formCtrService);
        this.loading = true;
        this.formGroup = this.formCtrService.toFormGroup(this.endorsementForm);
        this.getEndorsementData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getEndorsementData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.index = +paramMap.get('index');
            this.endorsementForm[2].label = 'Location ' + (this.index + 1);

            // For New Flow
            this.service = this.mm && this.mm == 'mm' ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.building = this.endoModel?.building || form?.data?.building || [];
                        this.endoModel.building[this.index] = this.endoModel?.building[this.index] || form?.data?.building && form?.data?.building[this.index] || {};

                        this.formGroup = this.formCtrService.toFormGroup(this.endorsementForm, this.endoModel.building[this.index], form?.data?.building && form?.data?.building[this.index] || {});

                        this.page = "endorsement/sectionD-form6/";
                        this.page += this.mm && this.mm == 'mm' ? (this.endoModel.id + '/' + this.index + '/mm') : (this.endoModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var unit = this.building.filter(x => x.key == "unit")[0];
        unit.options$ = this.localService.getUnit(lan);

        this.authService.changeLanguage$.subscribe(x => {
            unit.options$ = this.localService.getUnit(x);
        })
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }


    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtrService.lazyUpload(this.endorsementForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let building = this.endoModel.building ? this.endoModel.building.map(x => Object.assign({}, x)) || [] : [];
            building[this.index] = formValue;

            let data = { 'building': building };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionD-form7', id, this.index, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/endorsement/sectionD-form7', id, this.index]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionD-form6', id, this.index, 'mm']);
                }
            }
        }
    }
}
