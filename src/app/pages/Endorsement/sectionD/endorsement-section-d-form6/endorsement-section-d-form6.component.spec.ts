import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionDForm6Component } from './endorsement-section-d-form6.component';

describe('EndorsementSectionDForm6Component', () => {
  let component: EndorsementSectionDForm6Component;
  let fixture: ComponentFixture<EndorsementSectionDForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionDForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionDForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
