import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { EndorsementSectionD } from '../endorsement-section-d';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { cloneDeep } from 'lodash';


@Component({
    selector: 'app-endorsement-section-d-form5',
    templateUrl: './endorsement-section-d-form5.component.html',
    styleUrls: ['./endorsement-section-d-form5.component.scss']
})
export class EndorsementSectionDForm5Component extends EndorsementSectionD implements OnInit {
    @Input() id: any;
    page = "endorsement/sectionD-form5/";
    mm: any;

    user: any = {};

    land_url = 'assets/json/land_type.json';
    menu: any = "sectionD";
    correct: any = false;

    index: any = 0;
    previous_land: any;

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    endorsementForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location 1',
            style: { 'font-size': '18px' }
        }),
        new FormTitle({
            label: 'c. Particulars on land used/leased'
        }),
        new FormTitle({
            label: 'Please upload a location map/layout plan here:'
        }),
        new FormFile({
            key: 'layout_plan_document',
            label: 'File Name',
            required: true
        }),
        new FormInput({
            key: 'area',
            label: 'i. Area size',
            required: true,
            columns: 'col-md-6 d-flex',
            type: 'number',
        }),
        new FormSelect({
            key: 'unit',
            label: 'Choose unit of measurement for land area',
            options$: this.localService.getUnit(),
            required: true,
            columns: 'col-md-6 d-flex',
        }),
        new FormTextArea({
            key: 'description',
            label: 'ii. In case the land is rented by multiple lessees, describe whether there is clear division of the land (e.g. fences, separate entry/exit, etc.)',
        }),
        new FormTitle({
            label: 'Optional: upload a map/photo here:'
        }),
        new FormFile({
            key: 'map_document',
            label: 'File Name'
        }),
        new FormSelect({
            key: 'land_type',
            label: 'iii. Type of land',
            options$: this.localService.getLandType(),
            required: true
        }),
        new FormTitle({
            label: 'iv. Is it necessary to change the land type?'
        }),
        new FormRadio({
            key: 'is_change',
            label: 'Yes',
            value: 'Yes',
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'is_change',
            label: 'No',
            value: 'No',
            columns: 'col-md-8'
        }),
        new FormTextArea({
            key: 'land_type_description',
            label: 'Please specify the desired land type and the status of the process to obtain permission to change the land type:',
            required: true,
            criteriaValue: {
                key: 'is_change',
                value: 'Yes'
            }
        }),
        new FormTitle({
            label: 'If necessary, please upload supporting documents (e.g. recommendations or permission obtained on change of land use):',
            criteriaValue: {
                key: 'is_change',
                value: 'Yes'
            }
        }),
        new BaseFormArray({
            key: 'change_documents',
            formArray: [
                new FormFile({
                    key: 'document',
                    label: 'Name of document:'
                }),
            ],
            defaultLength: 1,
            criteriaValue: {
                key: 'is_change',
                value: 'Yes'
            }
        }),
        new FormTitle({
            label: 'v. Is the investor required to significantly alter the topography or elevation of the proposed land?',
        }),
        new FormRadio({
            key: 'is_required',
            label: 'Yes',
            value: 'Yes',
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'is_required',
            label: 'No',
            value: 'No',
            columns: 'col-md-8'
        }),
        new FormTextArea({
            key: 'required_description',
            label: 'Describe specifically to choosing yes',
            required: true,
            criteriaValue: {
                key: 'is_required',
                value: 'Yes'
            }
        }),
        new FormTitle({
            label: 'Please upload supporting documents here:',
            criteriaValue: {
                key: 'is_required',
                value: 'Yes'
            }
        }),
        new FormFile({
            key: 'required_document',
            label: 'Name of document:',
            criteriaValue: {
                key: 'is_required',
                value: 'Yes'
            }
        }),
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private localService: LocalService
    ) {
        super(formCtrService);
        this.loading = true;
        this.formGroup = this.formCtrService.toFormGroup(this.endorsementForm);
        this.getEndorsementData();
        this.getCurrentUser();
    }

    ngOnInit(): void { }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getEndorsementData() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.index = +paramMap.get('index');
            this.endorsementForm[2].label = 'Location ' + (this.index + 1);

            // For New Flow
            this.service = this.mm && this.mm == 'mm' ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm && this.mm == 'mm' ? form.data_mm || {} : form.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.land = this.endoModel?.land || form?.data?.land || [];
                        this.endoModel.land[this.index] = this.endoModel?.land[this.index] || form?.data?.land && form?.data?.land[this.index] || {};

                        this.formGroup = this.formCtrService.toFormGroup(this.endorsementForm, this.endoModel.land[this.index], form?.data?.land && form?.data?.land[this.index] || {});

                        this.page = "endorsement/sectionD-form5/";
                        this.page += this.mm && this.mm == 'mm' ? (this.endoModel.id + '/' + this.index + '/mm') : (this.endoModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var unit = this.endorsementForm.filter(x => x.key == "unit")[0];
        unit.options$ = this.localService.getUnit(lan);

        var land_type = this.endorsementForm.filter(x => x.key == "land_type")[0];
        land_type.options$ = this.localService.getLandType(lan);

        this.authService.changeLanguage$.subscribe(x => {
            unit.options$ = this.localService.getUnit(x);
            land_type.options$ = this.localService.getLandType(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtrService.lazyUpload(this.endorsementForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let land = this.endoModel.land ? this.endoModel.land.map(x => Object.assign({}, x)) || [] : [];
            land[this.index] = formValue;

            let data = { 'land': land };
            this.service.create({ ...this.endoModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionD-form6', id, this.index, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/endorsement/sectionD-form6', id, this.index]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionD-form5', id, this.index, 'mm']);
                }
            }
        }
    }
}
