import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionDForm5Component } from './endorsement-section-d-form5.component';

describe('EndorsementSectionDForm5Component', () => {
  let component: EndorsementSectionDForm5Component;
  let fixture: ComponentFixture<EndorsementSectionDForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionDForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionDForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
