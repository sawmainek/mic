import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionDForm2Component } from './endorsement-section-d-form2.component';

describe('EndorsementSectionDForm2Component', () => {
  let component: EndorsementSectionDForm2Component;
  let fixture: ComponentFixture<EndorsementSectionDForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionDForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionDForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
