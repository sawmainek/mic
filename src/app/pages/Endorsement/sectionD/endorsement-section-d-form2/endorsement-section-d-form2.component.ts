import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { Endorsement } from 'src/app/models/endorsement';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Location } from '@angular/common';
import { EndorsementSectionD } from '../endorsement-section-d';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { EndorsementSectionDForm2 } from './endorsement-section-d-form2';


@Component({
    selector: 'app-endorsement-section-d-form2',
    templateUrl: './endorsement-section-d-form2.component.html',
    styleUrls: ['./endorsement-section-d-form2.component.scss']
})
export class EndorsementSectionDForm2Component extends EndorsementSectionD implements OnInit {
    menu: any = "sectionD";
    @Input() id: any;
    page = "endorsement/sectionD-form2/";
    mm: any;

    user: any = {};

    endoModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    /* endorsementForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '1. Do you wish to apply for permission to lease or use land according to the Myanmar Investment Rules 116 together with this proposal?'
        }),
        new FormRadio({
            key: 'wish_to_apply',
            label: 'Yes',
            value: 'Yes',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'wish_to_apply',
            label: 'No',
            value: 'No',
            required: true,
            columns: 'col-md-8'
        }),
        new FormNote({
            label: 'An Investor who obtains a Permit or an Endorsement under the Myanmar Investment Law has the right to obtain a long-term lease of land or building from the private owned or from the relevant government departments, governmental organizations managed by the Government, or owned by the State in accordance with the stipulations in order to do investment. Myanmar citizen investors may invest in their own land or building in accordance with relevant laws. The following investors shall not be required to obtain the land use right from the Commission through submission in connection with Section 50 of the Myanmar Investment Law: (a) Myanmar citizen investors; or (b)an investor who has been granted under the laws to make investment in a company that retains its status as a Myanmar company (according to Myanmar Companies Law) after dealing with foreign investors. If the investor proposes to sublease land or buildings from a person who has previously obtained the land use right for the land and buildings, the investor shall not be required to apply for the separate land use right. However, the investor must notify the commission below.'
        })
    ] */

    baseForm: EndorsementSectionDForm2;
    constructor(
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private ref: ChangeDetectorRef,
        public formCtrService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
    ) {
        super(formCtrService);
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.baseForm = new EndorsementSectionDForm2();
            this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms);
            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.endoModel || {}, form.data || {});

                        this.page = "endorsement/sectionD-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }


    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    // saveFormData() {
    //   this.endorsementModel = { ...this.endorsementModel, ...this.formGroup.value };

    //   this.endorsementService.create(this.endorsementModel)
    //     .subscribe(x => {
    //       this.spinner = false;

    //       if (!this.is_draft) {
    //         if (window.location.href.indexOf('officer') !== -1) {
    //           this.router.navigate(['/officer/endorsement/sectionD-form3', x.id, 0]);
    //         } else {
    //           this.router.navigate(['/pages/endorsement/sectionD-form3', x.id, 0]);
    //         }
    //       }
    //     }, error => {
    //       console.log(error);
    //     });
    // }

    saveFormData() {
        this.spinner = true;

        let data = { ...this.endoModel, ...this.formGroup.value };
        this.service.create({ ...this.endoModel, ...data }, { secure: true })
            .subscribe(x => {
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                    this.redirectLink(this.form.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionD-form3', id, 0, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionD-form3', id, 0]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionD-form2', id, 'mm']);
                }
            }
        }
    }
}
