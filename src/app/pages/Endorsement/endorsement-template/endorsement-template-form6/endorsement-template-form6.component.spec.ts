import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementTemplateForm6Component } from './endorsement-template-form6.component';

describe('EndorsementTemplateForm6Component', () => {
  let component: EndorsementTemplateForm6Component;
  let fixture: ComponentFixture<EndorsementTemplateForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementTemplateForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementTemplateForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
