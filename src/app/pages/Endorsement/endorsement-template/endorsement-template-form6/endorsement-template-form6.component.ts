import { LocationService } from './../../../../../services/location.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormBuilder, FormArray, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
declare var jQuery: any;

@Component({
    selector: 'app-endorsement-template-form6',
    templateUrl: './endorsement-template-form6.component.html',
    styleUrls: ['./endorsement-template-form6.component.scss']
})
export class EndorsementTemplateForm6Component implements OnInit {

    countries: any;
    menu: any = "template";
    correct: any = false;
    id: number = undefined;
    model: any = {};
    endorsementForm: FormGroup;
    submitted = false;
    loaded: boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: EndorsementService,
        private lotService: LocationService,
        private ref: ChangeDetectorRef
    ) {
        this.lotService.getCountry().subscribe(x => this.countries = x);
        this.endorsementForm = this.formBuilder.group({
            rows: new FormArray(
                [this.createFormGroup(null)],
                [Validators.required])
        });

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id');
            if (this.id) {
                this.service.show(this.id)
                    .subscribe(x => {
                        this.model = x;
                        if (this.model.tem_investor) {
                            this.rows.clear();
                            this.ref.detectChanges();

                            this.model.tem_investor.rows.forEach(data => {
                                let fg = this.createFormGroup(data);
                                this.rows.push(fg);
                                this.loaded = true;
                                this.ref.detectChanges();
                            });
                        }
                        this.loaded = true;
                        this.ref.detectChanges();
                    });
            }
            else {
                this.loaded = true;
                this.ref.detectChanges();
            }
        });
    }

    ngOnInit(): void {

    }

    createFormGroup(data) {
        return this.formBuilder.group({
            name: [data?.name, Validators.required],
            address: [data?.address, Validators.required],
            country: [data?.country, Validators.required]
        })
    }


    get rows(): FormArray {
        return this.endorsementForm.get('rows') as FormArray;
    }

    openMenu(menu) {
        this.menu = menu;
    }

    addRow() {
        this.submitted = false;
        let fg = this.createFormGroup(null);
        this.rows.push(fg);
        this.ref.detectChanges();
    }

    removeRow(index) {
        this.rows.removeAt(index);
        this.ref.detectChanges();
    }

    onSubmit() {
        this.submitted = true;
        if (this.endorsementForm.invalid) {
            return;
        } else {
            this.saveEndorsementInfo();
        }
    }

    saveEndorsementInfo() {
        let tem_investor = { tem_investor: this.endorsementForm.value };
        this.service.create({ ...this.model, ...tem_investor })
            .subscribe(x => {
                this.router.navigate(['/pages/endorsement/undertaking-form1', x.id, 'mm']);
            });
    }
}