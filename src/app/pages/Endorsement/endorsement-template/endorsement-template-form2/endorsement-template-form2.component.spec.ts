import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementTemplateForm2Component } from './endorsement-template-form2.component';

describe('EndorsementTemplateForm2Component', () => {
  let component: EndorsementTemplateForm2Component;
  let fixture: ComponentFixture<EndorsementTemplateForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementTemplateForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementTemplateForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
