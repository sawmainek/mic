import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementTemplateForm5Component } from './endorsement-template-form5.component';

describe('EndorsementTemplateForm5Component', () => {
  let component: EndorsementTemplateForm5Component;
  let fixture: ComponentFixture<EndorsementTemplateForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementTemplateForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementTemplateForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
