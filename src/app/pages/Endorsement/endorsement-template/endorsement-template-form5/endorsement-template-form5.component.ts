import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { EndorsementService } from 'src/services/endorsement.service';
declare var jQuery: any;

@Component({
  selector: 'app-endorsement-template-form5',
  templateUrl: './endorsement-template-form5.component.html',
  styleUrls: ['./endorsement-template-form5.component.scss']
})
export class EndorsementTemplateForm5Component implements OnInit {
  menu: any = "template";
  correct: any = false;
  id: number = undefined;

  user: any = {};

  model: any = {};
  endorsementForm: FormGroup;

  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private service: EndorsementService,
    private store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    this.getCurrentUser();
    this.endorsementForm = this.formBuilder.group({
      construction_from: ['', Validators.required],
      construction_to: ['', Validators.required]
    }, { validator: this.checkDates });

    this.activatedRoute.paramMap.subscribe(paramMap => {
      this.id = +paramMap.get('id');
      if (this.id) {
        this.service.show(this.id)
          .subscribe(x => {
            this.model = x;
            if (this.model.tem_period) {
              this.endorsementForm.get("construction_from").setValue(this.model.tem_period.construction_from);
              this.endorsementForm.get("construction_to").setValue(this.model.tem_period.construction_to);
            }
          });
      }
    });
  }

  getCurrentUser() {
    this.store.pipe(select(currentUser))
      .subscribe(user => {
        if (user) {
          this.user = user;
        }
      });
  }

  checkDates(group: FormGroup) {
    let construction_from = group.get('construction_from').value;
    let construction_to = group.get('construction_to').value;
    let construction_control = group.get('construction_to');
    if (construction_control.errors && !construction_control.errors.date) {
      return;
    }
    else {
      if (Date.parse(construction_from) > Date.parse(construction_to))
        construction_control.setErrors({ date: true });
      else
        construction_control.setErrors(null);
    }
  }

  get endorsement() {
    return this.endorsementForm.controls;
  }

  getDate(control, date) {
    this.endorsementForm.controls[control].setValue(date);
  }

  openMenu(menu) {
    this.menu = menu;
  }

  onSubmit() {
    this.submitted = true;
    if (this.endorsementForm.invalid) {
      return;
    } else {
      this.saveEndorsementInfo();
    }
  }

  saveEndorsementInfo() {
    let tem_period = { tem_period: this.endorsementForm.value };
    this.service.create({ ...this.model, ...tem_period })
      .subscribe(x => {
        this.router.navigate(['/pages/endorsement/template-form6', x.id]);
      });
  }

}
