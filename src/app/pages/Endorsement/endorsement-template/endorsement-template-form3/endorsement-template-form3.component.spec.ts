import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementTemplateForm3Component } from './endorsement-template-form3.component';

describe('EndorsementTemplateForm3Component', () => {
  let component: EndorsementTemplateForm3Component;
  let fixture: ComponentFixture<EndorsementTemplateForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementTemplateForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementTemplateForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
