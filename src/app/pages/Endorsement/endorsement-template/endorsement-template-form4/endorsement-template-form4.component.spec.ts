import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementTemplateForm4Component } from './endorsement-template-form4.component';

describe('EndorsementTemplateForm4Component', () => {
  let component: EndorsementTemplateForm4Component;
  let fixture: ComponentFixture<EndorsementTemplateForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementTemplateForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementTemplateForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
