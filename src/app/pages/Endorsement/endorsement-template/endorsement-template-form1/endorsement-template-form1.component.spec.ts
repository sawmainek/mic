import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementTemplateForm1Component } from './endorsement-template-form1.component';

describe('EndorsementTemplateForm1Component', () => {
  let component: EndorsementTemplateForm1Component;
  let fixture: ComponentFixture<EndorsementTemplateForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementTemplateForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementTemplateForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
