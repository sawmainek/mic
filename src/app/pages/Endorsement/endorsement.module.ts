import { EndorsementChooseLanguageComponent } from './endorsement-choose-language/endorsement-choose-language.component';
import { EndorsementCertificateComponent } from './endorsement-certificate/endorsement-certificate.component';
import { EndorsementSectionDForm7Component } from './sectionD/endorsement-section-d-form7/endorsement-section-d-form7.component';
import { EndorsementSectionDForm6Component } from './sectionD/endorsement-section-d-form6/endorsement-section-d-form6.component';
import { EndorsementSectionDForm5Component } from './sectionD/endorsement-section-d-form5/endorsement-section-d-form5.component';
import { EndorsementUndertakingComponent } from './endorsement-undertaking/endorsement-undertaking.component';
import { EndorsementSectionCForm9Component } from './sectionC/endorsement-section-c-form9/endorsement-section-c-form9.component';
import { EndorsementSectionCForm8Component } from './sectionC/endorsement-section-c-form8/endorsement-section-c-form8.component';
import { EndorsementSectionCForm7Component } from './sectionC/endorsement-section-c-form7/endorsement-section-c-form7.component';
import { EndorsementSectionCForm6Component } from './sectionC/endorsement-section-c-form6/endorsement-section-c-form6.component';
import { EndorsementSectionCForm5Component } from './sectionC/endorsement-section-c-form5/endorsement-section-c-form5.component';
import { EndorsementSectionCForm4Component } from './sectionC/endorsement-section-c-form4/endorsement-section-c-form4.component';
import { EndorsementSectionAForm4Component } from './sectionA/endorsement-section-a-form4/endorsement-section-a-form4.component';
import { EndorsementSectionBForm3Component } from './sectionB/endorsement-section-b-form3/endorsement-section-b-form3.component';
import { EndorsementSectionBForm2Component } from './sectionB/endorsement-section-b-form2/endorsement-section-b-form2.component';
import { EndorsementSectionBForm1Component } from './sectionB/endorsement-section-b-form1/endorsement-section-b-form1.component';
import { EndorsementSuccessComponent } from './endorsement-success/endorsement-success.component';
import { EndorsementTemplateForm6Component } from './endorsement-template/endorsement-template-form6/endorsement-template-form6.component';
import { EndorsementTemplateForm5Component } from './endorsement-template/endorsement-template-form5/endorsement-template-form5.component';
import { EndorsementSectionEForm4Component } from './sectionE/endorsement-section-e-form4/endorsement-section-e-form4.component';
import { EndorsementSectionEForm3Component } from './sectionE/endorsement-section-e-form3/endorsement-section-e-form3.component';
import { EndorsementSectionEForm2Component } from './sectionE/endorsement-section-e-form2/endorsement-section-e-form2.component';
import { EndorsementSectionEForm1Component } from './sectionE/endorsement-section-e-form1/endorsement-section-e-form1.component';
import { EndorsementSectionDForm4Component } from './sectionD/endorsement-section-d-form4/endorsement-section-d-form4.component';
import { EndorsementSectionDForm3Component } from './sectionD/endorsement-section-d-form3/endorsement-section-d-form3.component';
import { EndorsementSectionDForm2Component } from './sectionD/endorsement-section-d-form2/endorsement-section-d-form2.component';
import { EndorsementSectionDForm1Component } from './sectionD/endorsement-section-d-form1/endorsement-section-d-form1.component';
import { EndorsementSectionCForm3Component } from './sectionC/endorsement-section-c-form3/endorsement-section-c-form3.component';
import { EndorsementSectionCForm2Component } from './sectionC/endorsement-section-c-form2/endorsement-section-c-form2.component';
import { EndorsementSectionCForm1Component } from './sectionC/endorsement-section-c-form1/endorsement-section-c-form1.component';
import { EndorsementSectionAForm3Component } from './sectionA/endorsement-section-a-form3/endorsement-section-a-form3.component';
import { EndorsementSectionAForm2Component } from './sectionA/endorsement-section-a-form2/endorsement-section-a-form2.component';
import { EndorsementSectionAForm1Component } from './sectionA/endorsement-section-a-form1/endorsement-section-a-form1.component';
import { EndorsementFormComponent } from './endorsement-form/endorsement-form.component';
import { CustomComponentModule } from './../../custom-component/custom-component.module';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { EndorsementRoutingModule } from './endorsement-routing.module';
import { EndorsementSideMenuComponent } from './endorsement-side-menu/endorsement-side-menu.component';
import { EndorsementStateAndRegionComponent } from './endorsement-state-and-region/endorsement-state-and-region.component';
import { EndorsementSidemenuMiniComponent } from './endorsement-sidemenu-mini/endorsement-sidemenu-mini.component';
import { EndorsementPaymentComponent } from './endorsement-payment/endorsement-payment.component';
import { EndorsementSectionBForm4Component } from './sectionB/endorsement-section-b-form4/endorsement-section-b-form4.component';

@NgModule({
    declarations: [
        EndorsementFormComponent,
        EndorsementSectionAForm1Component,
        EndorsementSectionAForm2Component,
        EndorsementSectionAForm3Component,
        EndorsementSectionCForm1Component,
        EndorsementSectionCForm2Component,
        EndorsementSectionCForm3Component,
        EndorsementSectionDForm1Component,
        EndorsementSectionDForm2Component,
        EndorsementSectionDForm3Component,
        EndorsementSectionDForm4Component,
        EndorsementSectionEForm1Component,
        EndorsementSectionEForm2Component,
        EndorsementSectionEForm3Component,
        EndorsementSectionEForm4Component,
        EndorsementTemplateForm5Component,
        EndorsementTemplateForm6Component,
        EndorsementSuccessComponent,
        EndorsementSectionBForm1Component,
        EndorsementSectionBForm2Component,
        EndorsementSectionBForm3Component,
        EndorsementSectionAForm4Component,
        EndorsementSectionCForm4Component,
        EndorsementSectionCForm5Component,
        EndorsementSectionCForm6Component,
        EndorsementSectionCForm7Component,
        EndorsementSectionCForm8Component,
        EndorsementSectionCForm9Component,
        EndorsementUndertakingComponent,
        EndorsementSectionDForm5Component,
        EndorsementSectionDForm6Component,
        EndorsementSectionDForm7Component,
        EndorsementCertificateComponent,
        EndorsementChooseLanguageComponent,
        EndorsementSideMenuComponent,
        EndorsementStateAndRegionComponent,
        EndorsementSidemenuMiniComponent,
        EndorsementPaymentComponent,
        EndorsementSectionBForm4Component,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        EndorsementRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // closeButton: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ],
    exports: [
        EndorsementFormComponent,
        EndorsementSectionAForm1Component,
        EndorsementSectionAForm2Component,
        EndorsementSectionAForm3Component,
        EndorsementSectionCForm1Component,
        EndorsementSectionCForm2Component,
        EndorsementSectionCForm3Component,
        EndorsementSectionDForm1Component,
        EndorsementSectionDForm2Component,
        EndorsementSectionDForm3Component,
        EndorsementSectionDForm4Component,
        EndorsementSectionEForm1Component,
        EndorsementSectionEForm2Component,
        EndorsementSectionEForm3Component,
        EndorsementSectionEForm4Component,
        EndorsementTemplateForm5Component,
        EndorsementTemplateForm6Component,
        EndorsementSuccessComponent,
        EndorsementSectionBForm1Component,
        EndorsementSectionBForm2Component,
        EndorsementSectionBForm3Component,
        EndorsementSectionBForm4Component,
        EndorsementSectionAForm4Component,
        EndorsementSectionCForm4Component,
        EndorsementSectionCForm5Component,
        EndorsementSectionCForm6Component,
        EndorsementSectionCForm7Component,
        EndorsementSectionCForm8Component,
        EndorsementSectionCForm9Component,
        EndorsementUndertakingComponent,
        EndorsementSectionDForm5Component,
        EndorsementSectionDForm6Component,
        EndorsementSectionDForm7Component,
        EndorsementCertificateComponent,
        EndorsementChooseLanguageComponent,
        EndorsementSideMenuComponent,
        EndorsementStateAndRegionComponent,
        EndorsementSidemenuMiniComponent,
    ], providers: [
        TranslateService,
    ],
})
export class EndorsementModule { }
