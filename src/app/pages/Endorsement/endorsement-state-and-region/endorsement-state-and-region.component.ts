import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { EndorsementService } from 'src/services/endorsement.service';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/core';
import { Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-endorsement-state-and-region',
    templateUrl: './endorsement-state-and-region.component.html',
    styleUrls: ['./endorsement-state-and-region.component.scss']
})
export class EndorsementStateAndRegionComponent implements OnInit {

    endoForm: FormGroup;
    endoModel: Endorsement;
    endoMMModel: Endorsement;
    form: any;

    states: any;
    id: any;

    submitted = false;
    spinner = false;
    loading = false;

    is_officer: boolean = false;

    constructor(
        public location: Location,
        private formBuilder: FormBuilder,
        private router: Router,
        private http: HttpClient,
        private activatedRoute: ActivatedRoute,
        private toast: ToastrService,
        private formService: EndorsementFormService,
        private lotService: LocationService,
        private store: Store<AppState>,
        private authService: AuthService
    ) {
        this.loading = true;

        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;

        this.endoForm = this.formBuilder.group({
            state: ['', [Validators.required]]
        });

        this.changeCboData();

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe(result => {
                        this.form = result;
                        this.endoForm.patchValue(this.form || {});
                        this.loading = false;
                    })
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    changeCboData() {
        let lan = localStorage.getItem('lan') || 'en';
        
        this.lotService.getState(lan).subscribe(x => this.states = x);

        this.authService.changeLanguage$.subscribe(x => {
            this.lotService.getState(x).subscribe(x => this.states = x);
        })
    }


    onSubmit() {
        if (this.endoForm.invalid) {
            this.toast.error('Please complete all required field(s).');
            this.spinner = false;
            return;
        }
        this.spinner = true;
        //For New Flow
        this.formService.create({ ...this.form, ...this.endoForm.value })
            .subscribe(result => {
                this.form = result;
                this.redirectLink(this.form.id);
            })
    }

    redirectLink(id: number) {
        if (window.location.href.indexOf('officer') !== -1) {
            this.router.navigate(['/officer/endorsement/endorsement-choose-language', id]);
        } else {
            this.router.navigate(['/pages/endorsement/endorsement-choose-language', id]);
        }
    }

    // onSubmit() {
    //   this.spinner = true;
    //   this.submitted = true;
    //   this.store.dispatch(new Submit({ submit: true }));
    //   if (this.endoForm.invalid) {
    //     this.toast.error('Please complete all required field(s).');
    //     this.spinner = false;
    //     return;
    //   }
    //   else {
    //     this.service.create({ ...this.endoModel, ...this.endoForm.value }, { secure: true })
    //       .subscribe(x => {
    //         this.spinner = false;
    //         if (window.location.href.indexOf('officer') !== -1){
    //           this.router.navigate(['/officer/endorsement/endorsement-choose-language', x.id]);
    //         }else{
    //           this.router.navigate(['/pages/endorsement/endorsement-choose-language', x.id]);
    //         }
    //       });
    //   }
    // }

}
