import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementStateAndRegionComponent } from './endorsement-state-and-region.component';

describe('EndorsementStateAndRegionComponent', () => {
  let component: EndorsementStateAndRegionComponent;
  let fixture: ComponentFixture<EndorsementStateAndRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementStateAndRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementStateAndRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
