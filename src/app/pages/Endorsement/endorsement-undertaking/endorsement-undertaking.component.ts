import { ISICService } from 'src/services/isic.service';
import { NotifyService } from './../../../../services/notify.service';
import { FormLogService } from './../../../../services/form-log.service';
import { EndorsementPreviewService } from './../endorsement-preview/endorsement-preview.service';
import { readyToSubmit } from './../../../core/form/_selectors/form.selectors';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormCheckBox } from 'src/app/custom-component/dynamic-forms/base/form-checkbox';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { EndorsementUndertaking } from './endorsement-undertaking';
import { BaseFormData } from 'src/app/core/form';
import { AppState } from 'src/app/core';
import { Store, select } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { FormService } from 'src/services/form.service';
import { EndorsementSubmissionService } from 'src/services/endorsement-submission.service';
import { Form } from 'src/app/models/form';
import { SubmissionEndorsement } from 'src/app/models/submission_endorsement';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';

declare var jQuery: any;
@Component({
    selector: 'app-endorsement-undertaking',
    templateUrl: './endorsement-undertaking.component.html',
    styleUrls: ['./endorsement-undertaking.component.scss']
})
export class EndorsementUndertakingComponent extends EndorsementUndertaking implements OnInit {
    @Input() id: any;
    menu: any = "undertaking";
    page = "endorsement/undertaking/";
    mm: any;

    user: any = {};
    is_officer: boolean = false;

    formModel: Form;
    endoSubmissionModel: SubmissionEndorsement;
    service: any;

    isicSectors: any[] = [];
    endoModel: any;
    formGroup: FormGroup;
    form: any;

    readyToSubmitNow: boolean = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    isPreview = false;
    loading = false;

    endorsementForm: BaseForm<any>[] = [
        new FormCheckBox({
            key: 'is_true',
            required: true,
            label: 'I / We hereby declare that the above statements are true and correct to the best of my/our knowledge and belief.'
        }),
        new FormCheckBox({
            key: 'is_understand',
            required: true,
            label: 'I /We fully understand that proposal may be denied or unnecessarily delayed if the applicant fails to provide required information to the Commission for issuance of the permit.'
        }),
        new FormCheckBox({
            key: 'is_strictly_comply',
            required: true,
            label: 'I/We hereby declare to strictly comply with terms and conditions set out by the Myanmar Investment Commission.'
        }),
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private endoFormService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private formService: FormService,
        private formLogService: FormLogService,
        private endoSubmissionService: EndorsementSubmissionService,
        private translateService: TranslateService,
        private isicService: ISICService,
        private notifyService: NotifyService,
        private endoPreviewService: EndorsementPreviewService,
        public location: Location,
    ) {
        super(formCtrService);

        this.loading = true;
        this.store.pipe(select(currentUser)).subscribe(result => {
            this.user = result;
        });

        this.store.pipe(select(readyToSubmit)).subscribe(submission => {
            if (submission == 'Endorsement') {
                this.readyToSubmitNow = true;
            }
        })

        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;
        this.formGroup = this.formCtrService.toFormGroup(this.endorsementForm);

    }

    ngOnInit(): void { 
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.isicService.getSectors().subscribe(results => {
                this.isicSectors = results;
            })

            // For New Flow
            if (this.id) {
                this.endoFormService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.service = this.mm ? this.endoMMService : this.endoService;

                        this.formGroup.patchValue(this.endoModel?.undertaking || {});

                        this.page = "endorsement/undertaking/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.endoSubmissionModel = this.form.submission || {};

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    openMenu(menu) {
        this.menu = menu;
    }

    onReply() {
        jQuery('.nav-tabs a[href="#reply"]').tab('show');
    }

    saveDraft() {
        this.is_draft = true;
        this.spinner = true;
        const undertaking = { undertaking: this.formGroup.value };
        this.endoMMService.create({ ...this.endoModel, ...undertaking }).subscribe(results => {
            this.spinner = false;
        });
    }

    onPreview() {
        this.isPreview = true;
        this.spinner = true;
        const undertaking = { undertaking: this.formGroup.value };
        this.endoMMService.create({ ...this.endoModel, ...undertaking }).subscribe(result => {
            this.spinner = false;
            this.isPreview = false;
            this.endoPreviewService.toFormPDF(result, {
                readOnly: true,
                onSubmit$: () => {

                }
            })
        });
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);

        if (this.mm == 'mm') {
            this.saveFormData();
        }
        else {
            this.saveEndorsementInfo();
        }
    }

    saveEndorsementInfo() {
        this.spinner = true;
        const undertaking = { undertaking: this.formGroup.value };

        this.service.create({ ...this.endoModel, ...undertaking }, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
                this.router.navigate([page+'/endorsement/undertaking', this.id, 'mm']);

            }, error => {
                console.log(error);
            });
    }

    saveFormData() {

        if (!this.readyToSubmitNow) {
            this.toast.error("Please complete all section.");
            return false;
        }
        
        this.spinner = true;
        let data = {
            'endorsement_id': this.id,
            'submitted_user_id': this.user.id,
            'status': this.endoSubmissionModel?.status || 'New',
            'sub_status': 'New',
            'allow_revision': false
        };

        data['application_no'] = this.endoSubmissionModel?.application_no || `${Math.floor(Math.random() * 999999)}`;
        data['investment_value'] = this.endoModel?.expected_investment?.spent_amount_kyat || 0;
        data['investment_value_kyat'] = this.endoModel?.expected_investment?.spent_amount_kyat || 0;
        data['investment_value_usd'] = this.endoModel?.expected_investment?.spent_amount_usd || 0;
        data['investment_enterprise_name'] = this.endoModel?.company_info?.investment_name || '';
        data['investment_type'] = this.endoModel?.invest_form?.form_investment || '';
        data['sector_of_investment'] = this.endoModel?.company_info?.general_sector_id;
        data['submit_to'] = this.form?.apply_to;

        let classes = this.isicSectors.filter(x => {
            let types: any[] = this.endoModel.company_info?.specific_business_sector?.isic_class_ids || [];
            return types.includes(x.name) ? true : false;
        })[0] || {};
        data['sector'] = classes.sector || '';

        // Todo for permission
        if (this.form?.apply_to == 'Myanmar Investment Commision') {
            if (this.endoModel?.company_info?.general_sector_id && this.endoModel?.company_info?.general_sector_id.indexOf('Investment Division 1') !== -1) {
                data['submit_to_role'] = 'division-1';
            }
            if (this.endoModel?.company_info?.general_sector_id && this.endoModel?.company_info?.general_sector_id.indexOf('Investment Division 2') !== -1) {
                data['submit_to_role'] = 'division-2';
            }
            if (this.endoModel?.company_info?.general_sector_id && this.endoModel?.company_info?.general_sector_id.indexOf('Investment Division 3') !== -1) {
                data['submit_to_role'] = 'division-3';
            }
            if (this.endoModel?.company_info?.general_sector_id && this.endoModel?.company_info?.general_sector_id.indexOf('Investment Division 4') !== -1) {
                data['submit_to_role'] = 'division-4';
            }
        } else {
            data['submit_to_role'] = `${this.endoModel.state.toLowerCase()}-branch`;
        }

        this.endoSubmissionModel = { ...this.endoSubmissionModel, ...data };
        this.endoSubmissionService.create(this.endoSubmissionModel).subscribe(submission => {
            this.formModel = new Form;
            this.formModel.user_id = this.user.id;
            this.formModel.type = "Endorsement";
            this.formModel.type_id = submission.id;
            this.formModel.status = this.endoSubmissionModel.id ? 'Resubmit' : 'New';
            this.formModel.data = this.form.data;
            this.formModel.data_mm = { ...this.endoModel, ...{ submission: this.formGroup.value } };
            // Save to Form Data/Status Log Model;
            forkJoin([
                this.endoMMService.create({ ...this.endoModel, ...{ submission: this.formGroup.value } }),
                this.endoFormService.create({ ...this.form, ...{ status: 'Submitted' } }),
                this.formService.create(this.formModel)
            ]).subscribe(results => {
                this.spinner = false;
                this.formLogService.create({
                    type: this.formModel.type,
                    type_id: this.formModel.type_id,
                    status: this.endoSubmissionModel.id ? 'Revision' : 'New',
                    sub_status: 'Submission',
                    submitted_user_id: this.formModel.user_id,
                    reply_user_id: submission?.reply_user_id,
                    incoming: true,
                    extra: {
                        application_no: submission?.application_no,
                        form_id: submission?.endorsement_id,
                        history_data: { ...this.form, ...{ data_mm: this.formModel.data_mm } }
                    }
                }).subscribe();
                if (submission?.reply_user_id) {
                    this.notifyService.create({
                        user_id: submission?.reply_user_id,
                        subject: `${this.formModel.status} Endorsement: # ${submission?.application_no} received.`,
                        message: `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/endorsement/form/${submission.endorsement_id}">${submission?.application_no}</a> for new submission.`
                    }).subscribe();
                }
                this.router.navigate(['/pages/endorsement/success', this.id]);
            });
            
        });

    }

}
