import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementUndertakingComponent } from './endorsement-undertaking.component';

describe('EndorsementUndertakingComponent', () => {
  let component: EndorsementUndertakingComponent;
  let fixture: ComponentFixture<EndorsementUndertakingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementUndertakingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementUndertakingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
