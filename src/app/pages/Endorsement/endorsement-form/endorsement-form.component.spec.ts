import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementFormComponent } from './endorsement-form.component';

describe('EndorsementFormComponent', () => {
  let component: EndorsementFormComponent;
  let fixture: ComponentFixture<EndorsementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
