import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-endorsement-form',
    templateUrl: './endorsement-form.component.html',
    styleUrls: ['./endorsement-form.component.scss']
})
export class EndorsementFormComponent implements OnInit {
    @Input() id: any;

    endoForm: FormGroup;
    endoModel: Endorsement;
    user: any = {};
    form: any = {};

    loading: boolean = false;
    loaded: boolean = false;
    submitted = false;
    spinner: boolean = false;
    isDraft: boolean = false;

    constructor(
        private router: Router,
        private toast: ToastrService,
        private formService: EndorsementFormService,
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private activatedRoute: ActivatedRoute
    ) {
        this.loading = true;
        this.endoForm = this.formBuilder.group({
            apply_to: ["Myanmar Investment Commision", Validators.required]
        });
        this.authService.getCurrentUser().subscribe(result => {
            this.user = result;
        });
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            if (this.id > 0) {
                this.formService.show(this.id)
                    .subscribe(result => {
                        this.form = result;
                        this.endoForm.patchValue(this.form || {});
                        this.loading = false;
                    })
            } else {
                this.loading = false;
            }
        }).unsubscribe();
        
    }
    
    openChoose(choose) {
        if (window.location.href.indexOf('officer') == -1) {
            this.endoForm.get('apply_to').setValue(choose);
        }
    }

    onSubmit() {
        this.spinner = true;
        //For New Flow
        this.form.user_id = this.form.user_id || this.user.id;
        this.form.apply_to = this.endoForm.get('apply_to').value;
        this.form.state = this.endoForm.get('apply_to').value === 'States and Regions Investment Committees' ? this.form.state : null;
        this.form.status = this.form.status || 'Draft';
        
        this.formService.create(this.form)
            .subscribe(result => {
                this.form = result;
                this.redirectLink(this.form.id);
            })
    }

    redirectLink(id: number) {
        if (id == null) {
            this.toast.error("Please login with investor account.");
            return;
        }
        if (this.endoForm.get('apply_to').value === 'States and Regions Investment Committees') {
            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['/officer/endorsement/endorsement-state-and-region', id]);
            } else {
                this.router.navigate(['/pages/endorsement/endorsement-state-and-region', id]);
            }

        } else {
            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['/officer/endorsement/endorsement-choose-language', id]);
            } else {
                this.router.navigate(['/pages/endorsement/endorsement-choose-language', id]);
            }

        }
    }
}
