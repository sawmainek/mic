import { formProgress } from './../../../../core/form/_selectors/form.selectors';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementSectionB } from '../endorsement-section-b';
import { FormProgressService } from 'src/services/form-progress.service';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { CurrentForm } from 'src/app/core/form/_actions/form.actions';

@Component({
    selector: 'app-endorsement-section-b-form1',
    templateUrl: './endorsement-section-b-form1.component.html',
    styleUrls: ['./endorsement-section-b-form1.component.scss']
})
export class EndorsementSectionBForm1Component extends EndorsementSectionB implements OnInit {
    menu: any = "sectionB";
    @Input() id: any;
    mm: any;

    endoModel: Endorsement;
    endoMMModel: Endorsement;
    form: any = {};

    progressLists: any[] = [];
    pages: any[] = [];

    loaded: any = false;
    loading = false;

    constructor(
        private formService: EndorsementFormService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private store: Store<AppState>,
        private tostr: ToastrService,
        private router: Router,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            if (this.id > 0) {
                // Get data from API
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.endoModel = this.form?.data || {};
                        this.endoMMModel = this.form?.data_mm || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.getFormProgress();

                        this.changeLanguage();
                        this.loading = false;

                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            `endorsement/sectionB-form2`,
            `endorsement/sectionB-form3`,
            `endorsement/sectionB-form4`,
        ];
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:Endorsement`
                    }).subscribe(result => {
                        this.progressLists = result;
                    });
                }
            }).unsubscribe();
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.endoModel.id}`).length;
            const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.endoMMModel.id}/mm`).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    openPage(index, link) {
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form.language == 'Myanmar') {
                this.router.navigate(['/officer/endorsement/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/officer/endorsement/' + link, this.id]);
            }

        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate(['/pages/endorsement/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/pages/endorsement/' + link, this.id]);
            }
        }
    }
}
