import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionBForm1Component } from './endorsement-section-b-form1.component';

describe('EndorsementSectionBForm1Component', () => {
  let component: EndorsementSectionBForm1Component;
  let fixture: ComponentFixture<EndorsementSectionBForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionBForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionBForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
