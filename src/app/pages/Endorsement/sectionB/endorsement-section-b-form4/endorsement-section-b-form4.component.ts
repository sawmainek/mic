import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { Observable, of } from 'rxjs';
import { EndorsementSectionB } from '../endorsement-section-b';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { cloneDeep } from 'lodash';
import { EndorsementSectionBForm4 } from './endorsement-section-b-form4';

@Component({
    selector: 'app-endorsement-section-b-form4',
    templateUrl: './endorsement-section-b-form4.component.html',
    styleUrls: ['./endorsement-section-b-form4.component.scss']
})
export class EndorsementSectionBForm4Component extends EndorsementSectionB implements OnInit {
    @Input() id: any;
    menu: any = "sectionB";
    page = "endorsement/sectionB-form4/";
    mm: any;

    user: any = {};

    endoModel: any;
    endoMMModel: any;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    baseForm: EndorsementSectionBForm4;
    constructor(
        private http: HttpClient,
        private router: Router,
        private lotService: LocationService,
        private localService: LocalService,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;

        this.getEndorsementData();
        this.getCurrentUser();
    }

    ngOnInit(): void { }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getEndorsementData() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.baseForm = new EndorsementSectionBForm4(this.mm || 'en', this.lotService, this.localService)
            this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.endoModel.share_holder = this.endoModel?.share_holder || form?.data?.share_holder || {};
                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.endoModel);

                        this.page = "endorsement/sectionB-form4/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        else {
            const shares = (Number(this.formGroup.value.share_holder.myanmar_share_ratio) || 0) + (Number(this.formGroup.value.share_holder.foreigner_share_ratio) || 0);
            if (Number(shares) === 100) {
                this.is_draft = false;
                this.saveFormData();
            } else {
                this.toast.error('Your total share ratio must be 100%.');
            }
        }
    }

    saveFormData() {
        this.spinner = true;
        //For New Flow ths.micService => this.service
        this.service.create({ ...this.endoModel, ...this.formGroup.value })
            .subscribe(x => {
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                    this.redirectLink(this.form.id)
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionC-form1', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionC-form1', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionB-form4', id, 'mm']);
                }
            }
        }
    }
}