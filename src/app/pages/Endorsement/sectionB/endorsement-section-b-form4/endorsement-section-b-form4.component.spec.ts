import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionBForm4Component } from './endorsement-section-b-form4.component';

describe('EndorsementSectionBForm4Component', () => {
  let component: EndorsementSectionBForm4Component;
  let fixture: ComponentFixture<EndorsementSectionBForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionBForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionBForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
