import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionBForm3Component } from './endorsement-section-b-form3.component';

describe('EndorsementSectionBForm3Component', () => {
  let component: EndorsementSectionBForm3Component;
  let fixture: ComponentFixture<EndorsementSectionBForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionBForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionBForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
