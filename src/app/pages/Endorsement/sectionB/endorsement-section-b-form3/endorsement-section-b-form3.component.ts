import { CompanyInfo } from './../../../../models/endorsement';
import { CPCService } from './../../../../../services/cpc.service';
import { ISICService } from './../../../../../services/isic.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { Observable, of, Subject } from 'rxjs';
import { EndorsementSectionB } from '../endorsement-section-b';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { LocationService } from 'src/services/location.service';
import { LocalService } from 'src/services/local.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { EndorsementSectionBForm3 } from './endorsement-section-b-form3';


@Component({
    selector: 'app-endorsement-section-b-form3',
    templateUrl: './endorsement-section-b-form3.component.html',
    styleUrls: ['./endorsement-section-b-form3.component.scss']
})
export class EndorsementSectionBForm3Component extends EndorsementSectionB implements OnInit {
    @Input() id: any;
    menu: any = "sectionB";
    page = "endorsement/sectionB-form3/";
    mm: any;

    user: any = {};

    endoModel: Endorsement;
    endoMMModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    generalSector$: Subject<any> = new Subject<any>();
    baseForm: EndorsementSectionBForm3;

    isRevision: boolean = false;

    constructor(
        private http: HttpClient,
        private router: Router,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private isicService: ISICService,
        private cpcService: CPCService,
        public location: Location,
        private lotService: LocationService,
        private localService: LocalService,
        private translateService: TranslateService,
        private authService: AuthService,
    ) {
        super(formCtrService);
        this.loading = true;
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.baseForm = new EndorsementSectionBForm3(this.mm || 'en', this.lotService, this.localService, this.isicService, this.cpcService, 'No');
            this.formGroup = this.formCtrService.toFormGroup(this.baseForm.forms);

            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form.data_mm || {} : form.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        // this.baseForm.forms[0].formGroup[0].value = this.form?.apply_to || "";
                        // this.baseForm.forms[0].value = this.form?.apply_to == 'Myanmar Investment Commision' ? this.endoModel?.company_info?.general_sector_id : null;
                        this.endoModel.company_info = this.endoModel?.company_info || form?.data?.company_info || {} as CompanyInfo;
                        this.endoModel.company_info.specific_business_sector = this.endoModel.company_info.specific_business_sector || {};

                        // Make sure for new dummy value;
                        this.endoModel.company_info.apply_to = this.form?.apply_to || "";

                        this.endoModel.company_info.specific_business_sector.general_sector_id = this.form?.apply_to == 'Myanmar Investment Commision' ? this.endoModel.company_info.general_sector_id : null;

                        this.changeFormLabel();

                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.endoModel, form.data);

                        this.generalSector$.subscribe(value => {
                            this.formGroup.get('company_info')
                                .get('specific_business_sector')
                                .get('general_sector_id').setValue(value);
                        })

                        this.page = "endorsement/sectionB-form3/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeFormLabel() {
        if (this.baseForm.forms[0].formGroup[0].value == 'Myanmar Investment Commision') {
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'business_sector_label')[0].label = "c. Specific sector of the proposed investment:";
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'business_cpc_label')[0].label = "d. Optional: please name the specific product(s)/service(s) of the investment:";
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'invest_description')[0].label = "e. Please give a description of the nature of the proposed activities of the investment, including details on the products/services and production and sales strategy of the investment business";
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'invest_benefit')[0].label = "f. Proposed investment’s supply chain and benefits to the other related businesses";
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'contact_label')[0].label = "g. Contact Person";
        }
        else {
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'business_sector_label')[0].label = "b. Specific sector of the proposed investment:";
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'business_cpc_label')[0].label = "c. Optional: please name the specific product(s)/service(s) of the investment:";
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'invest_description')[0].label = "d. Please give a description of the nature of the proposed activities of the investment, including details on the products/services and production and sales strategy of the investment business";
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'invest_benefit')[0].label = "e. Proposed investment’s supply chain and benefits to the other related businesses";
            this.baseForm.forms[0].formGroup.filter(x => x.key == 'contact_label')[0].label = "f. Contact Person";
        }
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }


    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }

        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue: any) => {
            this.endoModel = { ...this.endoModel, ...formValue };

            this.service.create(this.endoModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionB-form4', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionB-form4', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionB-form3', id, 'mm']);
                }
            }
        }
    }
}
