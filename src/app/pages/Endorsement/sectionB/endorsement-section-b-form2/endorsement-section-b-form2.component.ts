import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EndorsementService } from 'src/services/endorsement.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Endorsement } from 'src/app/models/endorsement';
import { EndorsementSectionB } from '../endorsement-section-b';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { AuthService } from 'src/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { EndorsementSectionBForm2 } from './endorsement-section-b-form2';


@Component({
    selector: 'app-endorsement-section-b-form2',
    templateUrl: './endorsement-section-b-form2.component.html',
    styleUrls: ['./endorsement-section-b-form2.component.scss']
})
export class EndorsementSectionBForm2Component extends EndorsementSectionB implements OnInit {
    @Input() id: any;
    page = "endorsement/sectionB-form2/";
    mm: any;

    user: any = {};

    endoModel: any;
    endoMMModel: Endorsement;
    formGroup: FormGroup;
    form: any;
    service: any;

    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;
    next_label = "NEXT SECTION";
    baseForm: EndorsementSectionBForm2;

    isRevision: boolean = false;

    constructor(
        private router: Router,
        private formService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.baseForm = new EndorsementSectionBForm2();
            this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            // For New Flow
            this.service = this.mm ? this.endoMMService : this.endoService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.endoModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.next_label = "NEXT";

                        this.endoModel.invest_form = this.endoModel?.invest_form || {};
                        this.formGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.endoModel, form.data || {});

                        this.page = "endorsement/sectionB-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.endoModel.id + '/mm' : this.endoModel.id;

                        this.formCtlService.getComments$('Endorsement', this.endoModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.endoModel?.id, type: 'Endorsement', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
        this.next_label = "NEXT";
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveEndorsementInfo();
    }

    saveDraft() {
        this.is_draft = true;
        this.saveEndorsementInfo();
    }

    saveEndorsementInfo() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.baseForm.forms, this.formGroup, { type_id: this.id }, (formValue) => {
            this.endoModel = { ...this.endoModel, ...formValue };
            this.service.create(this.endoModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.endoModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        })
    }

    // For New Flow 
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/endorsement/sectionB-form3', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/endorsement/sectionB-form3', id]);
                }
                else {
                    this.router.navigate([page + '/endorsement/sectionB-form2', id, 'mm']);
                }
            }
        }

    }
}
