import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class EndorsementSectionBForm2 {
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'invest_form',
                formGroup: [
                    new FormSectionTitle({
                        label: '1. Form of Investment'
                    }),
                    new FormTitle({
                        label: 'Please select the investment form of the enterprise, which has been established or which will be established:'
                    }),
                    new FormRadio({
                        key: 'form_investment',
                        label: 'Wholly Myanmar owned investment',
                        tooltip: 'An enterprise owned wholly by Myanmar.',
                        value: 'Wholly Myanmar owned investment',
                        required: true,
                        style: { 'margin-bottom': '10px' }
                    }),
                    new FormRadio({
                        key: 'form_investment',
                        label: 'Wholly Foreign owned investment',
                        tooltip: 'An enterprise owned wholly by foreign.',
                        value: 'Wholly Foreign owned investment',
                        style: { 'margin-bottom': '10px' },
                        required: true
                    }),
                    new FormRadio({
                        key: 'form_investment',
                        label: 'Myanmar citizen investment (up to 35% foreign shareholding)',
                        tooltip: 'An enterprise by Myanmar citizen with up to 35% foreign shareholding.',
                        value: 'Myanmar citizen investment',
                        required: true,
                        style: { 'margin-bottom': '10px' }
                    }),
                    new FormInput({
                        key: 'myan_direct_share_percent',
                        label: 'Please specify the percentage of direct shareholding or interest proportion by Foreign citizens',
                        type: 'number',
                        required: true,
                        validators: [Validators.min(0), Validators.max(35)],
                        criteriaValue: {
                            key: 'form_investment',
                            value: 'Myanmar citizen investment'
                        }
                    }),
                    new FormRadio({
                        key: 'form_investment',
                        label: 'Joint Venture (more than 35% foreign shareholding)',
                        tooltip: 'An enterprise by Myanmar citizen and foreign with more than 35% foreign shareholding.',
                        value: 'Joint Venture',
                        required: true,
                        // style: { 'margin-bottom': '10px'}
                    }),
                    new FormInput({
                        key: 'joint_direct_share_percent',
                        label: 'Please specify the percentage of direct shareholding or interest proportion by Foreign citizens',
                        type: 'number',
                        required: true,
                        validators: [Validators.min(35), Validators.max(100)],
                        criteriaValue: {
                            key: 'form_investment',
                            value: 'Joint Venture'
                        }
                    }),
                    new FormTitle({
                        label: 'Please upload the (draft) of Joint Venture agreement:',
                        criteriaValue: {
                            key: 'form_investment',
                            value: 'Joint Venture'
                        }
                    }),
                    new FormFile({
                        key: 'joint_document',
                        label: 'File Name',
                        required: true,
                        criteriaValue: {
                            key: 'form_investment',
                            value: 'Joint Venture'
                        }
                    }),
                ]
            })

        ];
    }
}