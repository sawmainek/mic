import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementSectionBForm2Component } from './endorsement-section-b-form2.component';

describe('EndorsementSectionBForm2Component', () => {
  let component: EndorsementSectionBForm2Component;
  let fixture: ComponentFixture<EndorsementSectionBForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementSectionBForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementSectionBForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
