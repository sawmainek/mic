export abstract class MICPermit {
    static type = "MIC-Permit";
    private _progressForms: any;

    get progressForms(): any {
        return this._progressForms;
    }

    getProgressForms(data) {
        this._progressForms = data ? data : [];
    }

}