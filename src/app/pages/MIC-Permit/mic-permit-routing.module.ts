import { MicSuccessComponent } from './mic-success/mic-success.component';
import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MicSectionAForm2Component } from './sectionA/mic-section-a-form2/mic-section-a-form2.component';
import { MicSectionAForm3Component } from './sectionA/mic-section-a-form3/mic-section-a-form3.component';
import { MicSectionAForm4Component } from './sectionA/mic-section-a-form4/mic-section-a-form4.component';
import { MicSectionAForm1Component } from './sectionA/mic-section-a-form1/mic-section-a-form1.component';
import { MicSectionBForm1Component } from './sectionB/mic-section-b-form1/mic-section-b-form1.component';
import { MicSectionBForm2Component } from './sectionB/mic-section-b-form2/mic-section-b-form2.component';
import { MicSectionBForm3Component } from './sectionB/mic-section-b-form3/mic-section-b-form3.component';
import { MicSectionBForm4Component } from './sectionB/mic-section-b-form4/mic-section-b-form4.component';
import { MicSectionCForm1Component } from './sectionC/mic-section-c-form1/mic-section-c-form1.component';
import { MicSectionCForm2Component } from './sectionC/mic-section-c-form2/mic-section-c-form2.component';
import { MicSectionCForm3Component } from './sectionC/mic-section-c-form3/mic-section-c-form3.component';
import { MicSectionCForm4Component } from './sectionC/mic-section-c-form4/mic-section-c-form4.component';
import { MicSectionCForm5Component } from './sectionC/mic-section-c-form5/mic-section-c-form5.component';
import { MicSectionCForm6Component } from './sectionC/mic-section-c-form6/mic-section-c-form6.component';
import { MicSectionCForm7Component } from './sectionC/mic-section-c-form7/mic-section-c-form7.component';
import { MicSectionCForm8Component } from './sectionC/mic-section-c-form8/mic-section-c-form8.component';
import { MicSectionDForm1Component } from './sectionD/mic-section-d-form1/mic-section-d-form1.component';
import { MicSectionDForm2Component } from './sectionD/mic-section-d-form2/mic-section-d-form2.component';
import { MicSectionDForm3Component } from './sectionD/mic-section-d-form3/mic-section-d-form3.component';
import { MicSectionDForm4Component } from './sectionD/mic-section-d-form4/mic-section-d-form4.component';
import { MicSectionDForm5Component } from './sectionD/mic-section-d-form5/mic-section-d-form5.component';
import { MicSectionDForm6Component } from './sectionD/mic-section-d-form6/mic-section-d-form6.component';
import { MicSectionDForm7Component } from './sectionD/mic-section-d-form7/mic-section-d-form7.component';
import { MicSectionDForm8Component } from './sectionD/mic-section-d-form8/mic-section-d-form8.component';
import { MicSectionEForm1Component } from './sectionE/mic-section-e-form1/mic-section-e-form1.component';
import { MicSectionEForm2Component } from './sectionE/mic-section-e-form2/mic-section-e-form2.component';
import { MicSectionFForm1Component } from './sectionF/mic-section-f-form1/mic-section-f-form1.component';
import { MicSectionFForm2Component } from './sectionF/mic-section-f-form2/mic-section-f-form2.component';
import { MicSectionEForm5Component } from './sectionE/mic-section-e-form5/mic-section-e-form5.component';
import { MicSectionEForm3Component } from './sectionE/mic-section-e-form3/mic-section-e-form3.component';
import { MicSectionEForm4Component } from './sectionE/mic-section-e-form4/mic-section-e-form4.component';
import { MicUndertakingComponent } from './mic-undertaking/mic-undertaking.component';
import { PermitCertificateComponent } from './permit-certificate/permit-certificate.component';
import { MicChooseLanguageComponent } from './mic-choose-language/mic-choose-language.component';
import { MicPaymentComponent } from './mic-payment/mic-payment.component';
import { MicSectionBForm5Component } from './sectionB/mic-section-b-form5/mic-section-b-form5.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'choose-language', pathMatch: 'full' },
            { path: 'sectionA-form1/:id', component: MicSectionAForm1Component },
            { path: 'sectionA-form1/:id/:mm', component: MicSectionAForm1Component },

            { path: 'sectionA-form2/:id', component: MicSectionAForm2Component },
            { path: 'sectionA-form2/:id/:mm', component: MicSectionAForm2Component },

            { path: 'sectionA-form3/:id', component: MicSectionAForm3Component },
            { path: 'sectionA-form3/:id/:mm', component: MicSectionAForm3Component },

            { path: 'sectionA-form4/:id', component: MicSectionAForm4Component },
            { path: 'sectionA-form4/:id/:mm', component: MicSectionAForm4Component },

            { path: 'sectionB-form1/:id', component: MicSectionBForm1Component },
            { path: 'sectionB-form1/:id/:mm', component: MicSectionBForm1Component },

            { path: 'sectionB-form2/:id', component: MicSectionBForm2Component },
            { path: 'sectionB-form2/:id/:mm', component: MicSectionBForm2Component },

            { path: 'sectionB-form3/:id', component: MicSectionBForm3Component },
            { path: 'sectionB-form3/:id/:mm', component: MicSectionBForm3Component },

            { path: 'sectionB-form4/:id', component: MicSectionBForm4Component },
            { path: 'sectionB-form4/:id/:mm', component: MicSectionBForm4Component },

            { path: 'sectionB-form5/:id', component: MicSectionBForm5Component },
            { path: 'sectionB-form5/:id/:mm', component: MicSectionBForm5Component },

            { path: 'sectionC-form1/:id', component: MicSectionCForm1Component },
            { path: 'sectionC-form1/:id/:mm', component: MicSectionCForm1Component },
            { path: 'sectionC-form2/:id', component: MicSectionCForm2Component },
            { path: 'sectionC-form3/:id', component: MicSectionCForm3Component },
            { path: 'sectionC-form4/:id', component: MicSectionCForm4Component },
            { path: 'sectionC-form5/:id', component: MicSectionCForm5Component },
            { path: 'sectionC-form6/:id', component: MicSectionCForm6Component },
            { path: 'sectionC-form7/:id', component: MicSectionCForm7Component },
            { path: 'sectionC-form8/:id', component: MicSectionCForm8Component },
            { path: 'sectionD-form1/:id', component: MicSectionDForm1Component },
            { path: 'sectionD-form2/:id', component: MicSectionDForm2Component },
            { path: 'sectionD-form3/:id/:index', component: MicSectionDForm3Component },
            { path: 'sectionD-form4/:id/:index', component: MicSectionDForm4Component },
            { path: 'sectionD-form5/:id/:index', component: MicSectionDForm5Component },
            { path: 'sectionD-form6/:id/:index', component: MicSectionDForm6Component },
            { path: 'sectionD-form7/:id/:index', component: MicSectionDForm7Component },
            { path: 'sectionD-form8/:id/:index', component: MicSectionDForm8Component },
            { path: 'sectionE-form1/:id', component: MicSectionEForm1Component },
            { path: 'sectionE-form2/:id', component: MicSectionEForm2Component },
            { path: 'sectionE-form3/:id', component: MicSectionEForm3Component },
            { path: 'sectionE-form4/:id', component: MicSectionEForm4Component },
            { path: 'sectionE-form5/:id', component: MicSectionEForm5Component },
            { path: 'sectionF-form1/:id', component: MicSectionFForm1Component },
            { path: 'sectionF-form2/:id', component: MicSectionFForm2Component },
            { path: 'permit-certificate/:id', component: PermitCertificateComponent },
            { path: 'undertaking/:id', component: MicUndertakingComponent },
            { path: 'success/:id', component: MicSuccessComponent },
            { path: 'choose-language', component: MicChooseLanguageComponent },
            { path: 'choose-language/:id', component: MicChooseLanguageComponent },
            { path: 'payment/:id', component: MicPaymentComponent },

            { path: 'sectionC-form2/:id/:mm', component: MicSectionCForm2Component },
            { path: 'sectionC-form3/:id/:mm', component: MicSectionCForm3Component },
            { path: 'sectionC-form4/:id/:mm', component: MicSectionCForm4Component },
            { path: 'sectionC-form5/:id/:mm', component: MicSectionCForm5Component },
            { path: 'sectionC-form6/:id/:mm', component: MicSectionCForm6Component },
            { path: 'sectionC-form7/:id/:mm', component: MicSectionCForm7Component },
            { path: 'sectionC-form8/:id/:mm', component: MicSectionCForm8Component },
            { path: 'sectionD-form1/:id/:mm', component: MicSectionDForm1Component },
            { path: 'sectionD-form2/:id/:mm', component: MicSectionDForm2Component },
            { path: 'sectionD-form3/:id/:index/:mm', component: MicSectionDForm3Component },
            { path: 'sectionD-form4/:id/:index/:mm', component: MicSectionDForm4Component },
            { path: 'sectionD-form5/:id/:index/:mm', component: MicSectionDForm5Component },
            { path: 'sectionD-form6/:id/:index/:mm', component: MicSectionDForm6Component },
            { path: 'sectionD-form7/:id/:index/:mm', component: MicSectionDForm7Component },
            { path: 'sectionD-form8/:id/:index/:mm', component: MicSectionDForm8Component },
            { path: 'sectionE-form1/:id/:mm', component: MicSectionEForm1Component },
            { path: 'sectionE-form2/:id/:mm', component: MicSectionEForm2Component },
            { path: 'sectionE-form3/:id/:mm', component: MicSectionEForm3Component },
            { path: 'sectionE-form4/:id/:mm', component: MicSectionEForm4Component },
            { path: 'sectionE-form5/:id/:mm', component: MicSectionEForm5Component },
            { path: 'sectionF-form1/:id/:mm', component: MicSectionFForm1Component },
            { path: 'sectionF-form2/:id/:mm', component: MicSectionFForm2Component },
            { path: 'permit-certificate/:id/:mm', component: PermitCertificateComponent },
            { path: 'undertaking/:id/:mm', component: MicUndertakingComponent },
            { path: 'success/:id/:mm', component: MicSuccessComponent },
            { path: 'choose-language/:mm', component: MicChooseLanguageComponent },
            { path: 'choose-language/:id/:mm', component: MicChooseLanguageComponent },
            { path: 'payment/:id/:mm', component: MicPaymentComponent },
        ]
    },
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class MicPermitRoutingModule { }
