import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermitCertificateComponent } from './permit-certificate.component';

describe('PermitCertificateComponent', () => {
  let component: PermitCertificateComponent;
  let fixture: ComponentFixture<PermitCertificateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermitCertificateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermitCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
