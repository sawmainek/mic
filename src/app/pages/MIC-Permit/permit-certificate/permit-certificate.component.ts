import { cloneDeep } from 'lodash';
import { LocationService } from './../../../../services/location.service';
import { ISICService } from './../../../../services/isic.service';
import { ToastrService } from 'ngx-toastr';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { BaseFormData, Submit, CurrentForm } from './../../../core/form/_actions/form.actions';
import { FormSectionTitleCounter } from './../../../custom-component/dynamic-forms/base/form-section-title-counter';
import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { MICPermitCertificate } from './mic-permit-certificate';
import { Endorsement } from 'src/app/models/endorsement';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { currentUser } from 'src/app/core';
import { LocalService } from 'src/services/local.service';
import { AuthService } from 'src/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-permit-certificate',
    templateUrl: './permit-certificate.component.html',
    styleUrls: ['./permit-certificate.component.scss']
})
export class PermitCertificateComponent extends MICPermitCertificate implements OnInit {
    menu = 'certificate';
    page = "mic/certificate/";
    @Input() id: any;
    mm: any = null;

    user: any = {};

    business_sectors: any;

    micModel: Endorsement;
    micFormGroup: FormGroup;
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = true;

    isRevision: boolean = false;

    businessSector: BaseForm<any>[] = [
        new FormSelect({
            key: 'isic_section_id',
            label: 'Choose business sections ( ISIC Section )',
            options$: this.isicService.getSection(),
            required: true
        }),
        new FormSelect({
            key: 'isic_division_id',
            label: 'Choose business divisions ( ISIC Division )',
            options$: this.isicService.getDivison(),
            filter: {
                parent: 'isic_section_id',
                key: 'section'
            },
            required: true,
        }),
        new FormSelect({
            key: 'isic_group_id',
            label: 'Choose business groups ( ISIC Group )',
            options$: this.isicService.getGroup(),
            filter: {
                parent: 'isic_division_id',
                key: 'division'
            },
            required: true,
        }),
        new FormSelect({
            key: 'isic_class_ids',
            label: 'Choose business class(es)',
            required: true,
            multiple: true,
            options$: this.isicService.getClasses(),
            filter: {
                parent: 'isic_group_id',
                key: 'group'
            },
        })
    ];

    permit_location: BaseForm<string>[] = [
        new FormInput({
            key: 'no',
            label: 'Location',
            columns: 'col-md-6',
            required: true,
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-6'
        }),
        new FormSelect({
            key: 'district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-6',
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-6',
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
    ]

    permit_investor: BaseForm<string>[] = [
        new FormSectionTitleCounter({
            label: 'Investor'
        }),
        new FormInput({
            key: 'name',
            label: 'a.  Name',
            columns: 'col-md-6',
            required: true
        }),
        new FormTitle({
            label: 'b. Address'
        }),
        new FormSelect({
            key: 'country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'state',
                key: 'state'
            },
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'district',
                key: 'district'
            },
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
    ]

    micForm: BaseForm<string>[] = [
        // new FormTitle({
        //     label: '1. Name of Enterprise (in English)'
        // }),
        new FormInput({
            key: 'enterprise_name',
            label: '1. Name of enterprise',
            required: true,
        }),
        new FormTitle({
            label: '2. Form of Investment'
        }),
        new FormRadio({
            key: 'investment_type',
            required: true,
            value: 'Wholly Myanmar owned investment',
            label: 'Wholly Myanmar owned investment',
            tooltip: 'An enterprise owned wholly by Myanmar.',
            style: { 'margin-bottom': '5px' }
        }),
        new FormRadio({
            key: 'investment_type',
            required: true,
            value: 'Wholly Foreign owned investment',
            label: 'Wholly Foreign owned investment',
            tooltip: 'An enterprise owned wholly by foreign.',
            style: { 'margin-bottom': '5px' }
        }),
        new FormRadio({
            key: 'investment_type',
            required: true,
            value: 'Myanmar citizen investment',
            label: 'Myanmar citizen investment',
            tooltip: 'An enterprise by Myanmar citizen with up to 35% foreign shareholding.',
            style: { 'margin-bottom': '5px' }
        }),
        new FormRadio({
            key: 'investment_type',
            required: true,
            value: 'Joint Venture',
            label: 'Joint Venture',
            tooltip: 'An enterprise by Myanmar citizen and foreign with more than 35% foreign shareholding.'
        }),
        // new FormTitle({
        //     label: '3. Type of Company'
        // }),
        new FormSelect({
            key: 'company_type',
            label: '3. Type of company',
            options$: this.localService.getCompany(),
            required: true
        }),
        new FormSectionTitle({
            label: '4. Specific sector of investment:'
        }),
        new BaseFormGroup({
            key: 'business_sector',
            formGroup: this.businessSector
        }),
        new FormSectionTitle({
            label: '5. Location(s) of the investment project (in English)'
        }),

        new BaseFormArray({
            key: 'permit_locations',
            formArray: this.permit_location
        }),
        // new FormTitle({
        //     label: '6. Amount of Foreign Capital'
        // }),
        new FormInput({
            key: 'usd_amount',
            label: '6. Amount of Foreign Capital (Equivalent US$)',
            required: true,
            type: 'number',
        }),
        new FormSectionTitle({
            label: '7. Period during which Foreign Cash and Equivalents will be brought in'
        }),
        new FormDate({
            key: 'brought_start_date',
            label: 'From Date',
            columns: 'col-md-6',
        }),
        new FormDate({
            key: 'brought_end_date',
            label: 'To Date',
            columns: 'col-md-6',
        }),
        // new FormTitle({
        //     label: '8. (Expected) investment value'
        // }),
        new FormInput({
            key: 'kyat_amount',
            label: '8. (Expected) investment value (Kyat)',
            required: true,
            type: 'number',
        }),
        new FormSectionTitle({
            label: '9. Construction or preparatory period'
        }),
        new FormDate({
            key: 'construction_start_date',
            label: 'From Date',
            columns: 'col-md-6',
            required: true
        }),
        new FormDate({
            key: 'construction_end_date',
            label: 'To Date',
            columns: 'col-md-6',
            required: true
        }),
        new FormSectionTitle({
            label: '10. (Expected) Investment Period'
        }),
        new FormDate({
            key: 'investment_start_date',
            label: 'From Date',
            columns: 'col-md-6',
            required: true
        }),
        new FormDate({
            key: 'investment_end_date',
            label: 'To Date',
            columns: 'col-md-6',
            required: true
        }),
        new FormSectionTitle({
            label: '11. Name(s) of investor(s) (in English):'
        }),
        new BaseFormArray({
            key: 'permit_investors',
            formArray: this.permit_investor
        }),
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formCtrService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private toast: ToastrService,
        private store: Store<AppState>,
        private isicService: ISICService,
        private lotService: LocationService,
        public location: Location,
        private localService: LocalService,
        private authService: AuthService,
        private translateService: TranslateService,
    ) {
        super(formCtrService);
        this.loading = true;
        this.micFormGroup = this.formCtrService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

    }

    ngOnInit(): void { }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            // For New Flow
            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.micModel = form?.data_mm || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micFormGroup = this.formCtrService.toFormGroup(this.micForm, this.micModel.permit || {});

                        this.formCtrService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var isic_class_ids = this.businessSector.filter(x => x.key == "isic_class_ids")[0];
        isic_class_ids.options$ = this.isicService.getClasses(lan);

        var state = this.permit_location.filter(x => x.key == "state")[0];
        state.options$ = this.lotService.getState(lan);

        var district = this.permit_location.filter(x => x.key == "district")[0];
        district.options$ = this.lotService.getDistrict(lan);

        var township = this.permit_location.filter(x => x.key == "township")[0];
        township.options$ = this.lotService.getTownship(lan);

        var company_type = this.micForm.filter(x => x.key == "company_type")[0];
        company_type.options$ = this.localService.getCompany(lan);

        this.authService.changeLanguage$.subscribe(x => {
            isic_class_ids.options$ = this.isicService.getClasses(x);
            state.options$ = this.lotService.getState(x);
            district.options$ = this.lotService.getDistrict(x);
            township.options$ = this.lotService.getTownship(x);
            company_type.options$ = this.localService.getCompany(x);
        })
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;

        this.page = `mic/certificate/${this.micModel.id}/mm`;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;

        let data = { 'permit': this.micFormGroup.value };
        this.service.create({ ...this.micModel, ...data }, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                    this.redirectLink(this.form?.id)
                } else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        //For choose Myanmar Language




        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            this.spinner = false;
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/undertaking', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/mic/undertaking', id]);
                }
                else {
                    this.router.navigate([page + '/mic/permit-certificate', id, 'mm']);
                }
            }
        }
    }
}
