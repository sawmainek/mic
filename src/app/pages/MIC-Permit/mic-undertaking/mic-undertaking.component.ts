import { ISICService } from 'src/services/isic.service';
import { NotifyService } from './../../../../services/notify.service';
import { FormLogService } from './../../../../services/form-log.service';
import { MICFormPreviewService } from './../mic-preview/mic-preview.service';
import { readyToSubmit } from './../../../core/form/_selectors/form.selectors';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormCheckBox } from 'src/app/custom-component/dynamic-forms/base/form-checkbox';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermitUndertaking } from './mic-permit-undertaking';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { Form } from 'src/app/models/form';
import { SubmissionMic } from 'src/app/models/submission_mic';
import { FormService } from 'src/services/form.service';
import { MicSubmissionService } from 'src/services/mic-submission.service';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { MicService } from 'src/services/mic.service';

declare var jQuery: any;
@Component({
    selector: 'app-mic-undertaking',
    templateUrl: './mic-undertaking.component.html',
    styleUrls: ['./mic-undertaking.component.scss']
})
export class MicUndertakingComponent extends MICPermitUndertaking implements OnInit {
    menu: any = "undertaking";
    page = "mic/undertaking/";
    @Input() id: any;
    mm: any;

    user: any = {};

    formModel: Form;
    isicSectors: any[] =[];
    micSubmissionModel: SubmissionMic;
    service: any;

    micModel: any;
    formGroup: FormGroup;
    form: any;

    spinner = false;
    is_draft = false;
    isPreview = false;
    loading = true;

    readyToSubmitNow: boolean = false;

    micForm: BaseForm<any>[] = [
        new FormCheckBox({
            key: 'is_true',
            required: true,
            label: 'I / We hereby declare that the above statements are true and correct to the best of my/our knowledge and belief.'
        }),
        new FormCheckBox({
            key: 'is_understand',
            required: true,
            label: 'I /We fully understand that proposal may be denied or unnecessarily delayed if the applicant fails to provide required information to the Commission for issuance of the permit.'
        }),
        new FormCheckBox({
            key: 'is_strictly_comply',
            required: true,
            label: 'I/We hereby declare to strictly comply with terms and conditions set out by the Myanmar Investment Commission.'
        }),
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        formCtlService: FormControlService,
        private micFormService: MICFormService,
        private micMMService: MICMMService,
        private micService: MicService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private isicService: ISICService,
        private formService: FormService,
        private notifyService: NotifyService,
        private formLogService: FormLogService,
        private micPreviewService: MICFormPreviewService,
        private submissionService: MicSubmissionService,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.formGroup = this.formCtlService.toFormGroup(this.micForm);
        this.store.pipe(select(currentUser)).subscribe(result => {
            this.user = result;
        });

        this.store.pipe(select(readyToSubmit)).subscribe(submission => {
            if(submission == 'MIC Permit') {
                this.readyToSubmitNow = true;
            }
        })
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;
            this.isicService.getSectors().subscribe(results => {
                this.isicSectors = results;
            })

            if (this.id > 0) {
                this.micFormService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.formGroup.patchValue(this.micModel?.submission || {});

                        this.page = "mic/undertaking/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.micSubmissionModel = this.form?.submission || {};

                        this.changeLanguage();
                        this.loading = false;
                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
        
     }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    openMenu(menu) {
        this.menu = menu;
    }

    onReply() {
        jQuery('.nav-tabs a[href="#reply"]').tab('show');
    }

    saveDraft() {
        this.is_draft = true;
        this.spinner = true;
        const submission = { submission: this.formGroup.value };
        this.micMMService.create({ ...this.micModel, ...submission }).subscribe(data => {
            this.spinner = false;
            this.is_draft = false;
            this.form.data_mm = data;
        });
    }

    onPreview() {
        this.isPreview = true;
        this.spinner = true;
        const submission = { submission: this.formGroup.value };
        this.micMMService.create({ ...this.micModel, ...submission }).subscribe(result => {
            this.spinner = false;
            this.isPreview = false;
            this.micPreviewService.toFormPDF(result, {
                readOnly: true,
                onSubmit$: () => {

                }
            })
        });
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
        if (this.mm == 'mm') {
            this.saveFormData();
        }
        else {
            this.saveMIC();
        }
    }

    saveMIC() {
        this.spinner = true;
        const submission = { submission: this.formGroup.value };
        this.service.create({ ...this.micModel, ...submission }).subscribe(results => {
            this.spinner = false;
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            this.router.navigate([page +'/mic/undertaking', this.id, 'mm']);
        });
    }

    saveFormData() {

        if(!this.readyToSubmitNow) {
            this.toast.error("Please complete all section.");
            return false;
        }

        this.spinner = true;
        let data = {
            'mic_id': this.id,
            'submitted_user_id': this.user.id,
            'status': this.micSubmissionModel?.status || 'New',
            'sub_status': 'New',
            'allow_revision': false
        };

        data['application_no'] = this.micSubmissionModel?.application_no || `${Math.floor(Math.random() * 999999)}`;
        data['investment_value'] = this.micModel?.expected_investment?.spent_amount_kyat || 0;
        data['investment_value_kyat'] = this.micModel?.expected_investment?.spent_amount_kyat || 0;
        data['investment_value_usd'] = this.micModel?.expected_investment?.spent_amount_usd || 0;
        data['investment_enterprise_name'] = this.micModel?.company_info?.investment_name || '';
        data['investment_type'] = this.micModel?.invest_form?.form_investment || '';
        data['sector_of_investment'] = this.micModel.company_info?.general_business_sector?.general_sector_id;

        let classes = this.isicSectors.filter(x => {
            let types: any[] = this.micModel.company_info?.general_business_sector?.isic_class_ids || [];
            return types.includes(x.name) ? true : false;
        })[0] || {};
        data['sector'] = classes.sector || '';

        // Todo for permission
        if (data['sector_of_investment'] && data['sector_of_investment'].indexOf('Investment Division 1') !== -1) {
            data['submit_to_role'] = 'division-1';
        }
        if (data['sector_of_investment'] && data['sector_of_investment'].indexOf('Investment Division 2') !== -1) {
            data['submit_to_role'] = 'division-2';
        }
        if (data['sector_of_investment'] && data['sector_of_investment'].indexOf('Investment Division 3') !== -1) {
            data['submit_to_role'] = 'division-3';
        }
        if (data['sector_of_investment'] && data['sector_of_investment'].indexOf('Investment Division 4') !== -1) {
            data['submit_to_role'] = 'division-4';
        }
        this.micSubmissionModel = { ...this.micSubmissionModel, ...data };
        this.submissionService.create(this.micSubmissionModel).subscribe(submission => {
            this.formModel = new Form;
            this.formModel.user_id = this.user.id;
            this.formModel.type = "MIC-Permit";
            this.formModel.type_id = submission.id;
            this.formModel.status = this.micSubmissionModel.id ? 'Resubmit' : 'New';
            this.formModel.data = this.form.data;
            this.formModel.data_mm = { ...this.micModel, ...{ submission: this.formGroup.value } };
            // Save to Form Data/Status Log Model;
            forkJoin([
                this.micMMService.create({ ...this.micModel, ...{ submission: this.formGroup.value } }),
                this.micFormService.create({ ...this.form, ...{ status: 'Submitted'}}),
                this.formService.create(this.formModel)
            ]).subscribe(results => {
                this.spinner = false;
                this.formLogService.create({
                    type: this.formModel.type,
                    type_id: this.formModel.type_id,
                    status: this.micSubmissionModel.id ? 'Revision' : 'New',
                    sub_status: 'Submission',
                    submitted_user_id: this.formModel.user_id,
                    reply_user_id: submission?.reply_user_id,
                    incoming: true,
                    extra: {
                        application_no: submission?.application_no,
                        form_id: submission?.mic_id,
                        history_data: {...this.form, ...{ data_mm: this.formModel.data_mm }}
                    }
                }).subscribe();
                if (submission?.reply_user_id) {
                    this.notifyService.create({
                        user_id: submission?.reply_user_id,
                        subject: `${this.formModel.status} MIC-Permit: # ${submission?.application_no} received.`,
                        message: `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/mic/choose-language/${submission.mic_id}">${submission?.application_no}</a> for new submission.`
                    }).subscribe();
                }
                this.router.navigate([`/pages/mic/success`, this.id]);
            });
            
        });

    }
}
