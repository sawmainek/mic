import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermit } from '../mic-permit';

export abstract class MICPermitUndertaking extends MICPermit {
    static section = "Undertaking";
    static totalForm = 1;
    totalForm = 1;

    constructor(public formCtlService: FormControlService) {
        super();
    }

    saveFormProgress(form_id: number, id: number, page: string) {
        if (this.progressForms?.length > 0) {
            if (this.progressForms.filter(x => x.type == MICPermit.type && x.section == MICPermitUndertaking.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: MICPermit.type,
                    type_id: id,
                    section: MICPermitUndertaking.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }

        }
        else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: MICPermitUndertaking.type,
                type_id: id,
                section: MICPermitUndertaking.section,
                page: page,
                status: true,
                revise: false
            });
        }
    }
}