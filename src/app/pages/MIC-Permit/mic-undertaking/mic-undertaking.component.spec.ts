import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicUndertakingComponent } from './mic-undertaking.component';

describe('MicUndertakingComponent', () => {
  let component: MicUndertakingComponent;
  let fixture: ComponentFixture<MicUndertakingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicUndertakingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicUndertakingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
