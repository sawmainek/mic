import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSidemenuMiniComponent } from './mic-sidemenu-mini.component';

describe('MicSidemenuMiniComponent', () => {
  let component: MicSidemenuMiniComponent;
  let fixture: ComponentFixture<MicSidemenuMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSidemenuMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSidemenuMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
