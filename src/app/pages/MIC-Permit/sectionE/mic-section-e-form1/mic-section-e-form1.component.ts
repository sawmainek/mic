import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MicService } from 'src/services/mic.service';
import { MICPermitSectionE } from '../mic-permit-section-e';
import { MICFormService } from 'src/services/micform.service';
import { Mic } from 'src/app/models/mic';
import { FormProgressService } from 'src/services/form-progress.service';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-e-form1',
    templateUrl: './mic-section-e-form1.component.html',
    styleUrls: ['./mic-section-e-form1.component.scss']
})
export class MicSectionEForm1Component extends MICPermitSectionE implements OnInit {
    menu: any = "sectionE";
    correct: any = false;
    @Input() id: any;
    mm: any;

    micModel: Mic;
    micMMModel: Mic;
    form: any; //For New Flow

    progressLists: any[] = [];
    pages: any[] = [];

    next_documents: string = "";
    next_documents_index: boolean = false;
    sector: string;

    loading = false;

    constructor(
        private tostr: ToastrService,
        private formService: MICFormService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private router: Router,
        private store: Store<AppState>,
        private translateService: TranslateService,
    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            if (this.id > 0) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.form?.data || {}
                        this.micMMModel = this.form?.data_mm || {}; //First priority
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.sector = this.micMMModel?.company_info?.general_business_sector?.general_sector_id || '';
                        if (this.sector.startsWith('Investment Division 2')) {
                            this.next_documents = 'sectionE-form3';
                        }
                        if (this.sector.startsWith('Investment Division 3')) {
                            this.next_documents = 'sectionE-form4';
                        }
                        if (this.sector.startsWith('Investment Division 4')) {
                            this.next_documents = 'sectionE-form5';
                        }
                        this.getFormProgress();

                        this.changeLanguage();
                        this.loading = false;

                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            `mic/sectionE-form2`,
            `mic/sectionE-form3`,
            `mic/sectionE-form4`,
            `mic/sectionE-form5`
        ];
        forkJoin([
            this.formProgressService.get({
                rows: 999,
                search: `type_id:equal:${this.micModel?.id || 0}|type:equal:MIC-Permit` // For New Flow this.id => this.micModel.id
            }),
            this.formProgressService.get({
                rows: 999,
                search: `type_id:equal:${this.micMMModel?.id || 0}|type:equal:MIC-Permit` // For New Flow this.id => this.micMMModel.id
            })
        ]).subscribe((x: any[]) => {
            this.progressLists = [...x[0], ...x[1]];
            this.loading = false;
        });
    }

    checkComplete(index) {
        const page = this.pages[index] || null;

        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
            const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    openPage(index, link) {
        var complete = true;
        if (this.form.language == 'Myanmar') {
            if (index == 0 && !this.micMMModel.company_info) {
                complete = false;
                this.tostr.error('Please complete Section-B, Question-1');
                return;
            }
        }
        else {
            if (index == 0 && (!this.micModel.company_info || !this.micMMModel.company_info)) {
                complete = false;
                this.tostr.error('Please complete Section-B, Question-1');
                return;
            }
        }

        if (complete) {
            if (window.location.href.indexOf('officer') !== -1) {
                if (this.form?.language == 'Myanmar') {
                    this.router.navigate(['/officer/mic/' + link, this.id, 'mm']);
                } else {
                    this.router.navigate(['/officer/mic/' + link, this.id]);
                }
            } else {
                if (this.form?.language == 'Myanmar') {
                    this.router.navigate(['/pages/mic/' + link, this.id, 'mm']);
                } else {
                    this.router.navigate(['/pages/mic/' + link, this.id]);
                }
            }
        }
    }

}
