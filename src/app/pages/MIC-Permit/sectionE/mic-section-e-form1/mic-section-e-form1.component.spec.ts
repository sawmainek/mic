import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionEForm1Component } from './mic-section-e-form1.component';

describe('MicSectionEForm1Component', () => {
  let component: MicSectionEForm1Component;
  let fixture: ComponentFixture<MicSectionEForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionEForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionEForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
