import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionEForm2Component } from './mic-section-e-form2.component';

describe('MicSectionEForm2Component', () => {
  let component: MicSectionEForm2Component;
  let fixture: ComponentFixture<MicSectionEForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionEForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionEForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
