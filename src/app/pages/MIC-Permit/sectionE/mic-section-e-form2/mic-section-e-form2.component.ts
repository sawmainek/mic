import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { MICPermitSectionE } from '../mic-permit-section-e';
import { AppState, currentUser } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-e-form2',
    templateUrl: './mic-section-e-form2.component.html',
    styleUrls: ['./mic-section-e-form2.component.scss']
})
export class MicSectionEForm2Component extends MICPermitSectionE implements OnInit {
    menu: any = "sectionE";
    correct: any = false;
    @Input() id: any;
    page = "mic/sectionE-form2/";

    user: any = {};

    micModel: any = {};
    micFormGroup: FormGroup;
    mm: any;
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    next_url = "";
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '1. Please upload the following documents:'
        }),
        // new FormTitle({
        //     label: 'List of machinery and equipment (items should be listed by domestic/foreign origin; a 4-digit HS code; and whether they are reconditioned or brand new)'
        // }),
        // new FormFile({
        //     key: 'machinery_equipment',
        //     label: 'File Name',
        //     required: true
        // }),
        // new FormTitle({
        //     label: 'List of raw materials (items should be listed by domestic/foreign origin and a 4-digit HS code; and whether they are reconditioned or brand new)'
        // }),
        // new FormFile({
        //     key: 'raw_materials',
        //     label: 'File Name',
        //     required: true
        // }),
        new FormTitle({
            label: 'Corporate social responsibility (CSR) plan:'
        }),
        new FormFile({
            key: 'csr_plan',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'Fire prevention plan:'
        }),
        new FormFile({
            key: 'fire_prevention_plan',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'Annual service plan (including estimation of annual service targets), if investment allocated to Investment Division 3:'
        }),
        new FormFile({
            key: 'service_plan',
            label: 'File Name',
        }),
        new FormTitle({
            label: 'Annual production and sales (export and domestic sales) plan, if investment allocated to Investment Division 1, 2 and 4:'
        }),
        new FormFile({
            key: 'sale_plan',
            label: 'File Name',
        }),
        new FormTitle({
            label: "If relevant, Permits of the President's Office / Union Government concerning the operation on the land of relevant Government Department or Ministry:"
        }),
        new FormFile({
            key: 'permit',
            label: 'File Name'
        }),
        new FormTitle({
            label: "Commitment statement on payment of income tax on staff salaries:"
        }),
        new FormFile({
            key: 'salary',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: "Mission Statement:"
        }),
        new FormFile({
            key: 'mission',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: "Company overview presentation:"
        }),
        new FormFile({
            key: 'overview',
            label: 'File Name',
            required: true
        }),
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

    }

    ngOnInit(): void { }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.other_attachments = this.micModel?.other_attachments || {};
                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel.other_attachments, form?.data?.other_attachments || {});

                        this.page = "mic/sectionE-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        if (this.micModel.company_info) {
                            this.next_label = "NEXT";
                            const sector = this.micModel?.company_info?.general_business_sector?.general_sector_id || "";
                            if (sector.startsWith('Investment Division 2')) {
                                this.next_url = '/mic/sectionE-form3';
                            }
                            else if (sector.startsWith('Investment Division 3')) {
                                this.next_url = '/mic/sectionE-form4';
                            }
                            else if (sector.startsWith('Investment Division 4')) {
                                this.next_url = '/mic/sectionE-form5';
                            } else {
                                this.next_label = "NEXT SECTION";
                                this.next_url = '/mic/sectionF-form1';
                            }
                        }

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'other_attachments': formValue };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + this.next_url, id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + this.next_url, id]);
                } else {
                    this.next_label = "NEXT";
                    this.router.navigate([page + '/mic/sectionE-form2', id, 'mm']);
                }
            }
        }

    }
}
