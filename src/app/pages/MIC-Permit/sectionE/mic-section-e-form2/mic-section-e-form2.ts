import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class MicSectionEForm2 {
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'other_attachments',
                formGroup: [
                    new FormSectionTitle({
                        label: '1. Please upload the following documents:'
                    }),

                    // new FormTitle({
                    //     label: 'List of machinery and equipment (items should be listed by domestic/foreign origin; a 4-digit HS code; and whether they are reconditioned or brand new)'
                    // }),
                    // new FormFile({
                    //     key: 'machinery_equipment',
                    //     label: 'File Name',
                    //     required: true
                    // }),
                    // new FormTitle({
                    //     label: 'List of raw materials (items should be listed by domestic/foreign origin and a 4-digit HS code; and whether they are reconditioned or brand new)'
                    // }),
                    // new FormFile({
                    //     key: 'raw_materials',
                    //     label: 'File Name',
                    //     required: true
                    // }),

                    new FormTitle({
                        label: 'Corporate social responsibility (CSR) plan:'
                    }),
                    new FormFile({
                        key: 'csr_plan',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Fire prevention plan:'
                    }),
                    new FormFile({
                        key: 'fire_prevention_plan',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Annual service plan (including estimation of annual service targets), if investment allocated to Investment Division 3:'
                    }),
                    new FormFile({
                        key: 'service_plan',
                        label: 'File Name',
                    }),
                    new FormTitle({
                        label: 'Annual production and sales (export and domestic sales) plan, if investment allocated to Investment Division 1, 2 and 4:'
                    }),
                    new FormFile({
                        key: 'sale_plan',
                        label: 'File Name',
                    }),
                    new FormTitle({
                        label: "If relevant, Permits of the President's Office / Union Government concerning the operation on the land of relevant Government Department or Ministry:"
                    }),
                    new FormFile({
                        key: 'permit',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: "Commitment statement on payment of income tax on staff salaries:"
                    }),
                    new FormFile({
                        key: 'salary',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: "Mission Statement:"
                    }),
                    new FormFile({
                        key: 'mission',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: "Company overview presentation:"
                    }),
                    new FormFile({
                        key: 'overview',
                        label: 'File Name',
                        required: true
                    }),
                ]
            })
        ];
    }
}