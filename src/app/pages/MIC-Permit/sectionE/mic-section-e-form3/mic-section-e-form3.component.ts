import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICPermitSectionE } from '../mic-permit-section-e';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-e-form3',
    templateUrl: './mic-section-e-form3.component.html',
    styleUrls: ['./mic-section-e-form3.component.scss']
})
export class MicSectionEForm3Component extends MICPermitSectionE implements OnInit {
    page = "mic/sectionE-form3/";
    @Input() id: any;
    menu: any = "sectionE";
    correct: any = false;
    mm: any;

    user: any = {};

    form: any;
    service: any;
    micModel: any = {};
    micFormGroup: FormGroup;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [

        new FormSectionTitle({
            label: '2. Please upload the following documents, as the investment falls under Investment Division 2 (Manufacturing except for food and wood processing industry):'
        }),
        new FormSectionTitle({
            label: 'All investments in Investment Division 2:'
        }),
        new FormTitle({
            label: 'CMP contract, if applicable:'
        }),
        new FormFile({
            key: 'cmp_contract',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Localization plan (plan on how to localize production):'
        }),
        new FormFile({
            key: 'localization_plan',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'Description of production process:'
        }),
        new FormFile({
            key: 'production_process',
            label: 'File Name',
            required: true
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Textiles sector:'
        }),
        new FormTitle({
            label: 'Wastewater treatment plan (Solid waste/ Liquid waste):'
        }),
        new FormFile({
            key: 'textiles_wastewater_treatment_plan',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Automobiles sector:'
        }),
        new FormTitle({
            label: 'Inspection certificate of the parent company:'
        }),
        new FormFile({
            key: 'inspection_certificate',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Completely Knocked Down (CKD) or Semi Knocked Down (SKD) assembler certificate / letter of authorization from the parent company:'
        }),
        new FormFile({
            key: 'ckd_skd_assembler_certificate',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Brand Authorization Letter by the parent company, allowing the company to produce the product:'
        }),
        new FormFile({
            key: 'brand_authorization_letter',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Raw materials list in 10 different categories of components to be included in the new SKD-2 system in compliance with MIC Notification No 85/2017 (Announcement of SKD-2 System for the Production of Various Vehicles):'
        }),
        new FormFile({
            key: 'raw_materials_list',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Assembly Line Plan, including layout plans and machine list for each vehicle assembly line:'
        }),
        new FormFile({
            key: 'assembly_line_plan',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Information on brand and model of the vehicles to be manufactured:'
        }),
        new FormFile({
            key: 'brand_model_manufactured',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Medical sector:'
        }),
        new FormTitle({
            label: 'Technical Collaboration and License Agreement with the parent company:'
        }),
        new FormFile({
            key: 'collaboration_license_agreement',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Information on brand of drug to be manufactured'
        }),
        new FormFile({
            key: 'information_brand_drug',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Cement sector:'
        }),
        new FormTitle({
            label: 'Permit from the Ministry of Mines for the production of heavy metals (limestone raw materials):'
        }),
        new FormFile({
            key: 'permit_for_production',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Information on the brands and manufacturing technology used to produce the product:'
        }),
        new FormFile({
            key: 'brands_manufacturing_technology',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Pulp sector:'
        }),
        new FormTitle({
            label: 'Plan on obtaining raw materials for pulp production:'
        }),
        new FormFile({
            key: 'pulp_obtaining_raw_materials',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Wastewater treatment plan (Solid waste/ Liquid waste):'
        }),
        new FormFile({
            key: 'pulp_wastewater_treatment_plan',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Detailed information on wastewater treatment system and technology (Aerobic / Anaerobic):'
        }),
        new FormFile({
            key: 'pulp_wastewater_treatment_system',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Manufacturing of plastics capsules using plastic cutters:'
        }),
        new FormTitle({
            label: 'Plan on obtaining raw materials domestically, annual raw material requirements, use of raw materials, production ratio and countries materials will be imported from:'
        }),
        new FormFile({
            key: 'plastic_raw_materials',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Complete overview of the technical/production methods used in the manufacture of plastic capsules, the machinery used, and details on products being produced:'
        }),
        new FormFile({
            key: 'complete_technical_overview',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Wastewater treatment plan (detailed description of the purification method for the waste that will be released from the operation), including details on the equipment which will be used:'
        }),
        new FormFile({
            key: 'plastic_wastewater_treatment_plan',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Detailed factory layout plan, including machinery which will be installed:'
        }),
        new FormFile({
            key: 'factory_layout_plan',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Company profile(s) of the equipment manufacturer(s) from whom equipment will be purchased from for the investment:'
        }),
        new FormFile({
            key: 'company_profile',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Company profile of parent company:'
        }),
        new FormFile({
            key: 'parent_company_profile',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Plan on how to reduce environmental impact and health risks for workers:'
        }),
        new FormFile({
            key: 'reduce_environmental_plan',
            label: 'File Name'
        }),


    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService


    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

    }

    ngOnInit(): void { }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";
            this.service = this.mm ? this.micMMService : this.micService;
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.other_attachments_div_2 = this.micModel?.other_attachments_div_2 || {};
                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel.other_attachments_div_2, form?.data?.other_attachments_div_2 || {});

                        this.page = "mic/sectionE-form3/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'other_attachments_div_2': formValue };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });

    }

    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionF-form1', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionF-form1', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionE-form3', id, 'mm']);
                }
            }
        }

    }
}
