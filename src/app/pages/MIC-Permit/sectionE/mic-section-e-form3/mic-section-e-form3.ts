import { FormDummy } from './../../../../custom-component/dynamic-forms/base/form-dummy';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class MicSectionEForm3 {
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new FormDummy({
                key: 'general_sector_id',
                value: ''
            }),
            new BaseFormGroup({
                key: 'other_attachments_div_2',
                criteriaValue: {
                    value: 'Investment Division 2 - Manufacturing (except for food and wood processing industry)',
                    key: 'general_sector_id'
                },
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Please upload the following documents, as the investment falls under Investment Division 2 (Manufacturing except for food and wood processing industry):'
                    }),
                    new FormSectionTitle({
                        label: 'All investments in Investment Division 2:'
                    }),
                    new FormTitle({
                        label: 'CMP contract, if applicable:'
                    }),
                    new FormFile({
                        key: 'cmp_contract',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Localization plan (plan on how to localize production):'
                    }),
                    new FormFile({
                        key: 'localization_plan',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Description of production process:'
                    }),
                    new FormFile({
                        key: 'production_process',
                        label: 'File Name',
                        required: true
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Textiles sector:'
                    }),
                    new FormTitle({
                        label: 'Wastewater treatment plan (Solid waste/ Liquid waste):'
                    }),
                    new FormFile({
                        key: 'textiles_wastewater_treatment_plan',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Automobiles sector:'
                    }),
                    new FormTitle({
                        label: 'Inspection certificate of the parent company:'
                    }),
                    new FormFile({
                        key: 'inspection_certificate',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Completely Knocked Down (CKD) or Semi Knocked Down (SKD) assembler certificate / letter of authorization from the parent company:'
                    }),
                    new FormFile({
                        key: 'ckd_skd_assembler_certificate',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Brand Authorization Letter by the parent company, allowing the company to produce the product:'
                    }),
                    new FormFile({
                        key: 'brand_authorization_letter',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Raw materials list in 10 different categories of components to be included in the new SKD-2 system in compliance with MIC Notification No 85/2017 (Announcement of SKD-2 System for the Production of Various Vehicles):'
                    }),
                    new FormFile({
                        key: 'raw_materials_list',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Assembly Line Plan, including layout plans and machine list for each vehicle assembly line:'
                    }),
                    new FormFile({
                        key: 'assembly_line_plan',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Information on brand and model of the vehicles to be manufactured:'
                    }),
                    new FormFile({
                        key: 'brand_model_manufactured',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Medical sector:'
                    }),
                    new FormTitle({
                        label: 'Technical Collaboration and License Agreement with the parent company:'
                    }),
                    new FormFile({
                        key: 'collaboration_license_agreement',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Information on brand of drug to be manufactured'
                    }),
                    new FormFile({
                        key: 'information_brand_drug',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Cement sector:'
                    }),
                    new FormTitle({
                        label: 'Permit from the Ministry of Mines for the production of heavy metals (limestone raw materials):'
                    }),
                    new FormFile({
                        key: 'permit_for_production',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Information on the brands and manufacturing technology used to produce the product:'
                    }),
                    new FormFile({
                        key: 'brands_manufacturing_technology',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Pulp sector:'
                    }),
                    new FormTitle({
                        label: 'Plan on obtaining raw materials for pulp production:'
                    }),
                    new FormFile({
                        key: 'pulp_obtaining_raw_materials',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Wastewater treatment plan (Solid waste/ Liquid waste):'
                    }),
                    new FormFile({
                        key: 'pulp_wastewater_treatment_plan',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Detailed information on wastewater treatment system and technology (Aerobic / Anaerobic):'
                    }),
                    new FormFile({
                        key: 'pulp_wastewater_treatment_system',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Manufacturing of plastics capsules using plastic cutters:'
                    }),
                    new FormTitle({
                        label: 'Plan on obtaining raw materials domestically, annual raw material requirements, use of raw materials, production ratio and countries materials will be imported from:'
                    }),
                    new FormFile({
                        key: 'plastic_raw_materials',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Complete overview of the technical/production methods used in the manufacture of plastic capsules, the machinery used, and details on products being produced:'
                    }),
                    new FormFile({
                        key: 'complete_technical_overview',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Wastewater treatment plan (detailed description of the purification method for the waste that will be released from the operation), including details on the equipment which will be used:'
                    }),
                    new FormFile({
                        key: 'plastic_wastewater_treatment_plan',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Detailed factory layout plan, including machinery which will be installed:'
                    }),
                    new FormFile({
                        key: 'factory_layout_plan',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Company profile(s) of the equipment manufacturer(s) from whom equipment will be purchased from for the investment:'
                    }),
                    new FormFile({
                        key: 'company_profile',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Company profile of parent company:'
                    }),
                    new FormFile({
                        key: 'parent_company_profile',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Plan on how to reduce environmental impact and health risks for workers:'
                    }),
                    new FormFile({
                        key: 'reduce_environmental_plan',
                        label: 'File Name'
                    })
                ]
            })
            
        ];
    }
}