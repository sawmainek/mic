import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionEForm3Component } from './mic-section-e-form3.component';

describe('MicSectionEForm3Component', () => {
  let component: MicSectionEForm3Component;
  let fixture: ComponentFixture<MicSectionEForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionEForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionEForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
