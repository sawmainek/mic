import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermitSectionE } from '../mic-permit-section-e';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { AuthService } from 'src/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-mic-section-e-form5',
    templateUrl: './mic-section-e-form5.component.html',
    styleUrls: ['./mic-section-e-form5.component.scss']
})
export class MicSectionEForm5Component extends MICPermitSectionE implements OnInit {
    menu: any = "sectionE";
    correct: any = false;
    page = "mic/sectionE-form5/";
    @Input() id: any;

    user: any = {};

    micModel: any = {};
    micFormGroup: FormGroup;
    mm: any;
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Please upload the following documents, if the investment falls under Investment Division 4 (Extractive Industries, Power and Other Services (Health, Education, Logistics, etc.)):'
        }),
        new FormSectionTitle({
            label: 'All investments in Investment Division 4:'
        }),
        new FormTitle({
            label: 'Environmental Impact Mitigation Action Plan:'
        }),
        new FormFile({
            key: 'mitigation_action_plan',
            label: 'File Name',
            required: true
        }),
        new FormSectionTitle({
            label: 'Only if investment operates Electricity Service:'
        }),
        new FormTitle({
            label: 'Power purchase agreement with the Village or Township Electrification Committee, which is authorized by the Region or State to procure electricity for the Region or State, except in connection with the national grid line, except for the distribution of electricity:'
        }),
        new FormFile({
            key: 'electricity_document',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Power and Energy related services sector (Offshore supply base distribution):'
        }),
        new FormTitle({
            label: 'Detailed design drawing and layout plan prepared in accordance with the standards of the Ministry of Electricity and Energy:'
        }),
        new FormFile({
            key: 'electricity_and_energy_document',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates Mineral Production:'
        }),
        new FormTitle({
            label: 'Permit (copy) granted by the Ministry of Natural Resources and Environmental Conservation:'
        }),
        new FormFile({
            key: 'mineral_production_permit',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Proposal letter to the Commission by the relevant ministry:'
        }),
        new FormFile({
            key: 'mineral_production_proposal',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'A plan for dealing with landfill waste generated in the operation:'
        }),
        new FormFile({
            key: 'mineral_production_dealing',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Plan to address landmine closure:'
        }),
        new FormFile({
            key: 'mineral_production_landmine_closure',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates Education Services:'
        }),
        new FormTitle({
            label: 'Detailed description of type of school/training institute to be established, including: curriculum (national or international accreditation bodies); Certification types provided to graduates; Description on the administration of the school; Evidence of education and experience of the persons/organization responsible for teaching:'
        }),
        new FormFile({
            key: 'education_description',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Acknowledgment that there are enough teachers and administrators:'
        }),
        new FormFile({
            key: 'education_acknowledgment',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Agreement letter with signatures of at least 10 neighbors that agree to the construction of the school buildings and opening school near their homes:'
        }),
        new FormFile({
            key: 'education_agreement',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Approval letter from the township/district administrator that there are no objections from the neighbors for constructing/opening the facility:'
        }),
        new FormFile({
            key: 'education_approval',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Evidence of a safe design of school premises, classroom environment, and labs:'
        }),
        new FormFile({
            key: 'education_evidence',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Recommendation of the respective ministry according to the type of school to be opened:'
        }),
        new FormFile({
            key: 'education_recommend',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'A pledge to abide by existing education laws and to comply with future education laws, according to the National Education Law:'
        }),
        new FormFile({
            key: 'education_law',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Commitment statement to forbid alcohol and drug usage on school premises:'
        }),
        new FormFile({
            key: 'education_commitment_school',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Commitment statement on safety for teachers and students:'
        }),
        new FormFile({
            key: 'education_commitment_teachers_students',
            label: 'File Name'
        }),
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

    }

    ngOnInit(): void { }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";
            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.other_attachments_div_4 = this.micModel?.other_attachments_div_4 || {}
                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel.other_attachments_div_4, form?.data?.other_attachments_div_4 || {});

                        this.page = "mic/sectionE-form5/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'other_attachments_div_4': formValue };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
            this.service.create(this.micModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionF-form1', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionF-form1', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionE-form5', id, 'mm']);
                }
            }
        }

    }


}
