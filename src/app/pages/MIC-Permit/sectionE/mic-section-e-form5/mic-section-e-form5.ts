import { FormDummy } from './../../../../custom-component/dynamic-forms/base/form-dummy';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class MicSectionEForm5 {
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new FormDummy({
                key: 'general_sector_id',
                value: ''
            }),
            new BaseFormGroup({
                key: 'other_attachments_div_4',
                criteriaValue: {
                    value: 'Investment Division 4 - Extractive Industries, Power and Other Services (Health, Education, Logistics, etc.)',
                    key: 'general_sector_id'
                },
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Please upload the following documents, if the investment falls under Investment Division 4 (Extractive Industries, Power and Other Services (Health, Education, Logistics, etc.)):'
                    }),
                    new FormSectionTitle({
                        label: 'All investments in Investment Division 4:'
                    }),
                    new FormTitle({
                        label: 'Environmental Impact Mitigation Action Plan:'
                    }),
                    new FormFile({
                        key: 'mitigation_action_plan',
                        label: 'File Name',
                        required: true
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates Electricity Service:'
                    }),
                    new FormTitle({
                        label: 'Power purchase agreement with the Village or Township Electrification Committee, which is authorized by the Region or State to procure electricity for the Region or State, except in connection with the national grid line, except for the distribution of electricity:'
                    }),
                    new FormFile({
                        key: 'electricity_document',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Power and Energy related services sector (Offshore supply base distribution):'
                    }),
                    new FormTitle({
                        label: 'Detailed design drawing and layout plan prepared in accordance with the standards of the Ministry of Electricity and Energy:'
                    }),
                    new FormFile({
                        key: 'electricity_and_energy_document',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates Mineral Production:'
                    }),
                    new FormTitle({
                        label: 'Permit (copy) granted by the Ministry of Natural Resources and Environmental Conservation:'
                    }),
                    new FormFile({
                        key: 'mineral_production_permit',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Proposal letter to the Commission by the relevant ministry:'
                    }),
                    new FormFile({
                        key: 'mineral_production_proposal',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'A plan for dealing with landfill waste generated in the operation:'
                    }),
                    new FormFile({
                        key: 'mineral_production_dealing',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Plan to address landmine closure:'
                    }),
                    new FormFile({
                        key: 'mineral_production_landmine_closure',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates Education Services:'
                    }),
                    new FormTitle({
                        label: 'Detailed description of type of school/training institute to be established, including: curriculum (national or international accreditation bodies); Certification types provided to graduates; Description on the administration of the school; Evidence of education and experience of the persons/organization responsible for teaching:'
                    }),
                    new FormFile({
                        key: 'education_description',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Acknowledgment that there are enough teachers and administrators:'
                    }),
                    new FormFile({
                        key: 'education_acknowledgment',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Agreement letter with signatures of at least 10 neighbors that agree to the construction of the school buildings and opening school near their homes:'
                    }),
                    new FormFile({
                        key: 'education_agreement',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Approval letter from the township/district administrator that there are no objections from the neighbors for constructing/opening the facility:'
                    }),
                    new FormFile({
                        key: 'education_approval',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Evidence of a safe design of school premises, classroom environment, and labs:'
                    }),
                    new FormFile({
                        key: 'education_evidence',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Recommendation of the respective ministry according to the type of school to be opened:'
                    }),
                    new FormFile({
                        key: 'education_recommend',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'A pledge to abide by existing education laws and to comply with future education laws, according to the National Education Law:'
                    }),
                    new FormFile({
                        key: 'education_law',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Commitment statement to forbid alcohol and drug usage on school premises:'
                    }),
                    new FormFile({
                        key: 'education_commitment_school',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Commitment statement on safety for teachers and students:'
                    }),
                    new FormFile({
                        key: 'education_commitment_teachers_students',
                        label: 'File Name'
                    }),
                ]
            })
            
        ];
    }
}