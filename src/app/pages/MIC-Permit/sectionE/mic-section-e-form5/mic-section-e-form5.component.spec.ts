import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionEForm5Component } from './mic-section-e-form5.component';

describe('MicSectionEForm5Component', () => {
  let component: MicSectionEForm5Component;
  let fixture: ComponentFixture<MicSectionEForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionEForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionEForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
