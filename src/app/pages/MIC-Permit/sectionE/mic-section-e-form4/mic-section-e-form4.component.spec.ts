import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionEForm4Component } from './mic-section-e-form4.component';

describe('MicSectionEForm4Component', () => {
  let component: MicSectionEForm4Component;
  let fixture: ComponentFixture<MicSectionEForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionEForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionEForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
