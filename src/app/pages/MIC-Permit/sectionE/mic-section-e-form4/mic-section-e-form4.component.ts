import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermitSectionE } from '../mic-permit-section-e';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-e-form4',
    templateUrl: './mic-section-e-form4.component.html',
    styleUrls: ['./mic-section-e-form4.component.scss']
})
export class MicSectionEForm4Component extends MICPermitSectionE implements OnInit {
    menu: any = "sectionE";
    correct: any = false;
    page = "mic/sectionE-form4/";
    @Input() id: any;

    user: any = {};

    micModel: any = {};
    micFormGroup: FormGroup;
    mm: any;
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Please upload the following documents, as the investment falls under Investment Division 3 (Hotel & Tourism, Real Estate Development, Transport & Telecommunication, Construction, Establishment of Industrial Estate):'
        }),
        new FormSectionTitle({
            label: 'All investments in Investment Division 3:'
        }),
        new FormTitle({
            label: 'Wastewater treatment and disposal plan:'
        }),
        new FormFile({
            key: 'wastewater_treatment_disposal_plan',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'Company profile of parent company (if applicable), including financial performance evidence:'
        }),
        new FormFile({
            key: 'parent_company_profile',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Business Plan:'
        }),
        new FormFile({
            key: 'business_plan',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'Director list (if the enterprise has been established):'
        }),
        new FormFile({
            key: 'director_list',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Establishment of Industrial Estate:'
        }),
        new FormTitle({
            label: 'Environmental Impact Assessments:'
        }),
        new FormFile({
            key: 'environmental_impact_assessments',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Summary of operation procedures:'
        }),
        new FormFile({
            key: 'operation_procedures_summary',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Map of Industrial Zone, including information on location, area size, land, roads, bridges, water, electricity (master plan):'
        }),
        new FormFile({
            key: 'industrial_zone_map',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Estimation of annual water consumption/requirements for factories/businesses and worker residential areas in industrial zone and plan for obtaining water supply:'
        }),
        new FormFile({
            key: 'estimation_annual_wate_water',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Estimation of annual electricity consumption/requirements (KW/HP) for factories/businesses and worker residential areas in industrial zone and plan for obtaining electricity supply:'
        }),
        new FormFile({
            key: 'estimation_annual_wate_electricity',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Reports based on preliminary survey (feasibility study or scoping report):'
        }),
        new FormFile({
            key: 'preliminary_survey_report',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Description of the type and number of businesses in the industrial zone:'
        }),
        new FormFile({
            key: 'businesses_types',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates in Real Estate Development:'
        }),
        new FormTitle({
            label: 'Environmental Impact Mitigation Action Plan:'
        }),
        new FormFile({
            key: 'red_env_impact_mitigation_plan',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates an Airline, Airport service or Port:'
        }),
        new FormTitle({
            label: 'Environmental Impact Mitigation Action Plan:'
        }),
        new FormFile({
            key: 'aasp_env_impact_mitigation_plan',
            label: 'File Name'
        }),
        new FormTitle({
            label: "Copy of Union Aviation Administration's permit, if the investment is an airline:"
        }),
        new FormFile({
            key: 'union_administration_permit_copy',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates a Hotel Service:'
        }),
        new FormTitle({
            label: 'Statement of commitment to adhere to the instructions of the respective departments for the preservation of national culture:'
        }),
        new FormFile({
            key: 'respective_dept_commitment_statement',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Statement of commitment to adhere to Heritage Conservation and related requirements, if heritage buildings are being used/leased:'
        }),
        new FormFile({
            key: 'heritage_commitment_statement',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Waste disposal plan:'
        }),
        new FormFile({
            key: 'waste_disposal_plan',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates a Communication Service:'
        }),
        new FormTitle({
            label: 'Environmental Impact Mitigation Action Plan:'
        }),
        new FormFile({
            key: 'communication_env_impact_mitigation_plan',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Relevant telecommunication license issued by the Ministry of Transport and Communications:'
        }),
        new FormFile({
            key: 'relevant_telecommunication_license',
            label: 'File Name'
        }),
        new FormSectionTitle({
            label: 'Only if investment operates Road constructions with a BOT system:'
        }),
        new FormTitle({
            label: 'Environmental Impact Mitigation Action Plan:'
        }),
        new FormFile({
            key: 'bot_env_impact_mitigation_plan',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Photo of roads and bridges, if construction complete:'
        }),
        new FormFile({
            key: 'roads_bridges_photo',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'Road and bridge designs and concepts, if construction incomplete:'
        }),
        new FormFile({
            key: 'roads_bridges_design',
            label: 'File Name'
        }),
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,

    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

    }

    ngOnInit(): void { }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";

            this.service = this.mm ? this.micMMService : this.micService;
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.other_attachments_div_3 = this.micModel?.other_attachments_div_3 || {}
                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel.other_attachments_div_3, form?.data?.other_attachments_div_3 || {});

                        this.page = "mic/sectionE-form4/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'other_attachments_div_3': formValue };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionF-form1', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionF-form1', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionE-form4', id, 'mm']);
                }
            }
        }
    }
}
