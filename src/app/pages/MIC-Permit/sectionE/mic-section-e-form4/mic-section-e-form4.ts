import { FormDummy } from './../../../../custom-component/dynamic-forms/base/form-dummy';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class MicSectionEForm4 {
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new FormDummy({
                key: 'general_sector_id',
                value: ''
            }),
            new BaseFormGroup({
                key: 'other_attachments_div_3',
                criteriaValue: {
                    value: 'Investment Division 3 - Hotel & Tourism, Real Estate Development, Transport & Telecommunication, Construction, Establishment of Industrial Estate',
                    key: 'general_sector_id'
                },
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Please upload the following documents, as the investment falls under Investment Division 3 (Hotel & Tourism, Real Estate Development, Transport & Telecommunication, Construction, Establishment of Industrial Estate):'
                    }),
                    new FormSectionTitle({
                        label: 'All investments in Investment Division 3:'
                    }),
                    new FormTitle({
                        label: 'Wastewater treatment and disposal plan:'
                    }),
                    new FormFile({
                        key: 'wastewater_treatment_disposal_plan',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Company profile of parent company (if applicable), including financial performance evidence:'
                    }),
                    new FormFile({
                        key: 'parent_company_profile',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Business Plan:'
                    }),
                    new FormFile({
                        key: 'business_plan',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Director list (if the enterprise has been established):'
                    }),
                    new FormFile({
                        key: 'director_list',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Establishment of Industrial Estate:'
                    }),
                    new FormTitle({
                        label: 'Environmental Impact Assessments:'
                    }),
                    new FormFile({
                        key: 'environmental_impact_assessments',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Summary of operation procedures:'
                    }),
                    new FormFile({
                        key: 'operation_procedures_summary',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Map of Industrial Zone, including information on location, area size, land, roads, bridges, water, electricity (master plan):'
                    }),
                    new FormFile({
                        key: 'industrial_zone_map',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Estimation of annual water consumption/requirements for factories/businesses and worker residential areas in industrial zone and plan for obtaining water supply:'
                    }),
                    new FormFile({
                        key: 'estimation_annual_wate_water',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Estimation of annual electricity consumption/requirements (KW/HP) for factories/businesses and worker residential areas in industrial zone and plan for obtaining electricity supply:'
                    }),
                    new FormFile({
                        key: 'estimation_annual_wate_electricity',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Reports based on preliminary survey (feasibility study or scoping report):'
                    }),
                    new FormFile({
                        key: 'preliminary_survey_report',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Description of the type and number of businesses in the industrial zone:'
                    }),
                    new FormFile({
                        key: 'businesses_types',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates in Real Estate Development:'
                    }),
                    new FormTitle({
                        label: 'Environmental Impact Mitigation Action Plan:'
                    }),
                    new FormFile({
                        key: 'red_env_impact_mitigation_plan',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates an Airline, Airport service or Port:'
                    }),
                    new FormTitle({
                        label: 'Environmental Impact Mitigation Action Plan:'
                    }),
                    new FormFile({
                        key: 'aasp_env_impact_mitigation_plan',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: "Copy of Union Aviation Administration's permit, if the investment is an airline:"
                    }),
                    new FormFile({
                        key: 'union_administration_permit_copy',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates a Hotel Service:'
                    }),
                    new FormTitle({
                        label: 'Statement of commitment to adhere to the instructions of the respective departments for the preservation of national culture:'
                    }),
                    new FormFile({
                        key: 'respective_dept_commitment_statement',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Statement of commitment to adhere to Heritage Conservation and related requirements, if heritage buildings are being used/leased:'
                    }),
                    new FormFile({
                        key: 'heritage_commitment_statement',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Waste disposal plan:'
                    }),
                    new FormFile({
                        key: 'waste_disposal_plan',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates a Communication Service:'
                    }),
                    new FormTitle({
                        label: 'Environmental Impact Mitigation Action Plan:'
                    }),
                    new FormFile({
                        key: 'communication_env_impact_mitigation_plan',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Relevant telecommunication license issued by the Ministry of Transport and Communications:'
                    }),
                    new FormFile({
                        key: 'relevant_telecommunication_license',
                        label: 'File Name'
                    }),
                    new FormSectionTitle({
                        label: 'Only if investment operates Road constructions with a BOT system:'
                    }),
                    new FormTitle({
                        label: 'Environmental Impact Mitigation Action Plan:'
                    }),
                    new FormFile({
                        key: 'bot_env_impact_mitigation_plan',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Photo of roads and bridges, if construction complete:'
                    }),
                    new FormFile({
                        key: 'roads_bridges_photo',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'Road and bridge designs and concepts, if construction incomplete:'
                    }),
                    new FormFile({
                        key: 'roads_bridges_design',
                        label: 'File Name'
                    }),
                ]
            })
            
        ];
    }
}