import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicPaymentComponent } from './mic-payment.component';

describe('MicPaymentComponent', () => {
  let component: MicPaymentComponent;
  let fixture: ComponentFixture<MicPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
