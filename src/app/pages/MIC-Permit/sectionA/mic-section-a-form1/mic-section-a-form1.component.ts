import { formProgress } from './../../../../core/form/_selectors/form.selectors';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { forkJoin } from 'rxjs';
import { MICFormService } from './../../../../../services/micform.service';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Mic } from 'src/app/models/mic';
import { ActivatedRoute, Router } from '@angular/router';
import { MICPermitSectionA } from '../mic-permit-section-a';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormData, currentForm } from 'src/app/core/form';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/core';
import { FormProgressService } from 'src/services/form-progress.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-a-form1',
    templateUrl: './mic-section-a-form1.component.html',
    styleUrls: ['./mic-section-a-form1.component.scss']
})
export class MicSectionAForm1Component extends MICPermitSectionA implements OnInit {
    menu: any = "sectionA";
    @Input() id: any;
    mm: any;

    model: any = {};
    micModel: Mic;
    micMMModel: Mic;
    form: any; //For New Flow

    progressLists: any[] = [];
    pages: any[] = [];

    loading = false;

    constructor(
        private formService: MICFormService, //For New Flow
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private router: Router,
        private tostr: ToastrService,
        private store: Store<AppState>,
        private translateService: TranslateService,
    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            if (this.id > 0) {
                // Get data from API
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.form?.data || {};
                        this.micMMModel = this.form?.data_mm || {};
                        this.model = (this.form?.language == 'Myanmar') ? form?.data_mm || {} : form?.data || {};
                        this.getFormProgress();

                        this.changeLanguage();
                        this.loading = false;
                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            `mic/sectionA-form4`
        ];
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:MIC-Permit` // For New Flow this.id => this.micModel.id
                    }).subscribe(result => {
                        this.progressLists = result;
                    });
                }
            }).unsubscribe();
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
            const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    openPage(index, link) {
        this.store.dispatch(new CurrentForm({ form: this.form }));
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/officer/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/officer/' + link, this.id]);
            }
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/pages/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/pages/' + link, this.id]);
            }
        }
    }
}
