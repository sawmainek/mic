import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionAForm1Component } from './mic-section-a-form1.component';

describe('MicSectionAForm1Component', () => {
  let component: MicSectionAForm1Component;
  let fixture: ComponentFixture<MicSectionAForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionAForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionAForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
