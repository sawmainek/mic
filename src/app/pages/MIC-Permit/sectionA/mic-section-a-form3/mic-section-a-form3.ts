import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { Validators } from '@angular/forms';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';

export class MICSectionAForm3 {
    public forms: BaseForm<string>[] = [];

    constructor(

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'invest_form',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Form of investment'
                    }),
                    new FormTitle({
                        label: 'a. Please select the investment form of the enterprise, which has been established or which will be established:'
                    }),
                    new FormRadio({
                        key: 'form_investment',
                        label: 'Wholly Myanmar owned investment',
                        tooltip: 'An enterprise owned wholly by Myanmar.',
                        value: 'Wholly Myanmar owned investment',
                        required: true,
                        style: { 'margin-bottom': '10px' }
                    }),
                    new FormRadio({
                        key: 'form_investment',
                        label: 'Wholly Foreign owned investment',
                        tooltip: 'An enterprise owned wholly by foreign.',
                        value: 'Wholly Foreign owned investment',
                        required: true,
                        style: { 'margin-bottom': '10px' }
                    }),
                    new FormRadio({
                        key: 'form_investment',
                        label: 'Myanmar citizen investment (up to 35% foreign shareholding)',
                        tooltip: 'An enterprise by Myanmar citizen with up to 35% foreign shareholding.',
                        value: 'Myanmar citizen investment',
                        required: true,
                        style: { 'margin-bottom': '10px' }
                    }),
                    new FormInput({
                        key: 'myan_direct_share_percent',
                        label: 'Please specify the percentage of direct shareholding or interest proportion by Foreign citizens',
                        type: 'number',
                        required: true,
                        validators: [Validators.min(0), Validators.max(35)],
                        criteriaValue: {
                            key: 'form_investment',
                            value: 'Myanmar citizen investment'
                        }
                    }),
                    new FormRadio({
                        key: 'form_investment',
                        label: 'Joint Venture (more than 35% foreign shareholding)',
                        tooltip: 'An enterprise by Myanmar citizen and foreign with more than 35% foreign shareholding.',
                        value: 'Joint Venture',
                        required: true
                    }),
                    new FormInput({
                        key: 'joint_direct_share_percent',
                        label: 'Please specify the percentage of direct shareholding or interest proportion by Foreign citizens',
                        type: 'number',
                        required: true,
                        validators: [Validators.min(35), Validators.max(100)],
                        criteriaValue: {
                            key: 'form_investment',
                            value: 'Joint Venture'
                        }
                    }),
                    new FormTitle({
                        label: 'Please upload the (draft) of Joint Venture agreement:',
                        criteriaValue: {
                            key: 'form_investment',
                            value: 'Joint Venture'
                        }
                    }),
                    new FormFile({
                        key: 'joint_document',
                        label: 'File Name',
                        required: true,
                        criteriaValue: {
                            key: 'form_investment',
                            value: 'Joint Venture'
                        }
                    }),
                    new FormTextArea({
                        key: 'special_type',
                        label: 'b. Please specify if the investment includes a special type of contractual agreement (e.g. Build Operate Transfer (BOT), Profit Sharing Contract, Production Sharing Contract, etc.):',
                    }),
                    new FormTitle({
                        label: 'Please upload the draft contract / agreement:'
                    }),
                    new BaseFormArray({
                        key: 'contract_document',
                        formArray: [
                            new FormFile({
                                key: 'contract_document_doc',
                                label: 'Name of document:',
                                multiple: true
                            })
                        ],
                    }),
                    new FormTextArea({
                        key: 'other_type',
                        label: 'mic-c. Please specify if the investment involves any other type of approval/agreement or partnership/contract with the government:',
                    }),
                    new FormTitle({
                        label: 'Please upload a copy of the document of the respective ministry/department here:'
                    }),
                    new BaseFormArray({
                        key: 'ministry_document',
                        formArray: [
                            new FormFile({
                                key: 'ministry_document_doc',
                                label: 'Name of document:',
                                multiple: true
                            })
                        ],
                    }),
                    new FormNote({
                        label: 'If the proposed investment is related with the Myanmar Investment Rule 44, approval from the relevant government department/organization is required.'
                    })
                ]
            })
        ]
    }
}