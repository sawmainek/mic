import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionAForm3Component } from './mic-section-a-form3.component';

describe('MicSectionAForm3Component', () => {
  let component: MicSectionAForm3Component;
  let fixture: ComponentFixture<MicSectionAForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionAForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionAForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
