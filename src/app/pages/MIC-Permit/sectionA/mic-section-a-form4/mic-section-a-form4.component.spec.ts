import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionAForm4Component } from './mic-section-a-form4.component';

describe('MicSectionAForm4Component', () => {
    let component: MicSectionAForm4Component;
    let fixture: ComponentFixture<MicSectionAForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [MicSectionAForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(MicSectionAForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
