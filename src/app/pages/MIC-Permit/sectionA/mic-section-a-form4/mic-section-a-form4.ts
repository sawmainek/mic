import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { LocalService } from 'src/services/local.service';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { Validators } from '@angular/forms';
import { LocationService } from 'src/services/location.service';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';

export class MicSectionAForm4 {
    public individual: BaseForm<string>[] = [];
    public company: BaseForm<string>[] = [];
    public government: BaseForm<string>[] = [];
    public investor: BaseForm<string>[] = [];
    public forms: BaseForm<string>[] = [];
    constructor(
        lotService: LocationService,
        localService: LocalService
    ) {

        this.individual = [
            new FormTitle({
                label: 'Details on the investor(s) responsible for establishing the enterprise:'
            }),
            new FormInput({
                key: 'full_name',
                label: 'a.  Full name',
                required: true,
            }),
            new FormSelect({
                key: 'citizenship',
                label: 'b. Citizenship ',
                options$: lotService.getCountry(),
                required: true,
                value: 'Myanmar',
            }),
            new FormInput({
                key: 'passport_or_nrc',
                label: 'c.  Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
                required: true,
            }),
            new FormTitle({
                label: 'Please upload a copy of passport (foreign nationals) or National Registration Card (Myanmar citizens):'
            }),
            new FormFile({
                key: 'copy_of_nrc_doc',
                label: 'File Name',
                required: true,
            }),
            new FormTitle({
                label: 'Please upload bank statements or other evidence of financial standing(in Myanmar language or in English):'
            }),
            new BaseFormArray({
                key: 'bank_statements',
                formArray: [
                    new FormFile({
                        key: 'bank_statement_doc',
                        label: 'Name of document:',
                        multiple: true,
                        required: true,
                    })
                ],
            }),
            new FormTitle({
                label: 'd. Address '
            }),
            new FormSelect({
                key: 'individual_country',
                label: 'Country',
                options$: lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'individual_state',
                label: 'State / Region:',
                options$: lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_district',
                label: 'District:',
                options$: lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'individual_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),
            new FormInput({
                key: 'phone_no',
                label: 'e. Phone number ',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'f. E-mail address',
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
            new FormTextArea({
                key: 'business_expertise',
                label: 'g. Please describe your sectoral and regional business expertise',
                required: true,
            }),
        ]

        this.company =[
            new FormTitle({
                label: 'Details on the investor(s) responsible for establishing the enterprise:'
            }),
            new FormInput({
                key: 'company_name',
                label: 'a.  Name',
                required: true,
            }),
            new FormSelect({
                key: 'incorporate_country',
                label: 'b. Country of incorporation',
                options$: lotService.getCountry(),
                required: true,
            }),
            new FormInput({
                key: 'company_type',
                label: 'c. Type of company',
                required: true,
            }),
            new FormInput({
                key: 'certificate_no',
                label: 'Company Registration Certificate Number',
                required: true,
            }),
            new FormTitle({
                label: 'Please upload a copy of the Certificate of Incorporation/company registration certificate:'
            }),
            new FormFile({
                key: 'company_certificate_doc',
                label: 'File Name',
                required: true,
                columns: 'col-md-12',
            }),
            new FormTitle({
                label: 'Please upload the most recent annual audit report and other evidence of the financial conditions of the business (in Myanmar language or English):'
            }),
            new BaseFormArray({
                key: 'recent_annual_audit',
                formArray: [
                    new FormFile({
                        key: 'recent_annual_audit_doc',
                        label: 'Name of document:',
                        multiple: true,
                        required: true,
                    })
                ],
            }),
            new FormTitle({
                label: 'd. Address '
            }),
            new FormSelect({
                key: 'company_country',
                label: 'Country',
                options$: lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'company_state',
                label: 'State / Region:',
                options$: lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_district',
                label: 'District:',
                options$: lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'company_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),
            new FormInput({
                key: 'phone_no',
                label: 'e. Phone number ',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'fax_no',
                label: 'f. Fax number',
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'g. E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'website',
                label: 'h. Website',
                columns: 'col-md-6'
            }),
            new FormTextArea({
                key: 'short_description',
                label: 'i. Short description of company',
                required: true
            }),
            new FormTitle({
                label: 'j. Parent / Holding company'
            }),
            new FormInput({
                key: 'parent_company_name',
                label: 'Name',
                required: true,
            }),
            new FormTitle({
                label: 'Address'
            }),
            new FormSelect({
                key: 'parent_company_country',
                label: 'Country',
                options$: lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'parent_company_state',
                label: 'State / Region:',
                options$: lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'parent_company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'parent_company_district',
                label: 'District:',
                options$: lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'parent_company_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'parent_company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'parent_company_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'parent_company_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'parent_company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'parent_company_address',
                label: 'Additional address details',
                required: true
            }),
            new FormTitle({
                label: 'k. Contact person',
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                type: 'number',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ]
        
        this.government =[
            new FormTitle({
                label: 'Details on the investor(s) responsible for establishing the enterprise:'
            }),
            new FormInput({
                key: 'name',
                label: 'a. Name of government department / organization',
                required: true
            }),
            new FormTitle({
                label: 'b. Address',
            }),
            new FormSelect({
                key: 'government_country',
                label: 'Country',
                options$: lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'government_state',
                label: 'State / Region:',
                options$: lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'government_district',
                label: 'District:',
                options$: lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'government_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'government_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'government_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),
            new FormInput({
                key: 'phone_no',
                label: 'c. Phone number',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'fax',
                label: 'd. Fax number',
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'e. E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'website',
                label: 'f. Website',
                columns: 'col-md-6'
            }),
            new FormTitle({
                label: 'g. Contact Person',
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ]

        this.investor =[
            new FormSectionTitleCounter({
                label: 'Investor'
            }),
            new FormSelect({
                key: 'investor_type',
                options$: localService.getType(),
                label: 'Type',
                required: true
            }),
            // new FormSelect({
            //     key: 'country',
            //     label: 'Citizenship or country of incorporation/ registration/ origin',
            //     options$: lotService.getCountry(),
            //     required: true
            // }),
            new BaseFormGroup({
                key: 'individual',
                formGroup: this.individual,
                criteriaValue: {
                    key: 'investor_type',
                    value: 'Individual'
                }
            }),
            new BaseFormGroup({
                key: 'company',
                formGroup: this.company,
                criteriaValue: {
                    key: 'investor_type',
                    value: 'Company'
                }
            }),
            new BaseFormGroup({
                key: 'government',
                formGroup: this.government,
                criteriaValue: {
                    key: 'investor_type',
                    value: 'Government department/organization'
                }
            })
        ]

        this.forms = [
            new FormSectionTitle({
                label: '1. Please give an overview of the investor(s) responsible for establishing the enterprise:'
            }),
    
            new BaseFormArray({
                key: 'investor_list',
                label: 'Add Investor',
                formArray: this.investor
            })
        ]
    }
}
