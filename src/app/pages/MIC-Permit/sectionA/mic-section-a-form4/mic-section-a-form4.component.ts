import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { FormSectionTitleCounter } from '../../../../custom-component/dynamic-forms/base/form-section-title-counter';
import { FormControlService } from '../../../../custom-component/dynamic-forms/form-control.service';
import { FormFile } from '../../../../custom-component/dynamic-forms/base/form.file';
import { FormTitle } from '../../../../custom-component/dynamic-forms/base/form-title';
import { FormTextArea } from '../../../../custom-component/dynamic-forms/base/form-textarea';
import { BaseFormGroup } from '../../../../custom-component/dynamic-forms/base/form-group';
import { FormSelect } from '../../../../custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseFormArray } from '../../../../custom-component/dynamic-forms/base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Mic } from 'src/app/models/mic';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { MICPermitSectionA } from '../mic-permit-section-a';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { LocalService } from 'src/services/local.service';
import { cloneDeep } from "lodash";

@Component({
    selector: 'app-mic-section-a-form4',
    templateUrl: './mic-section-a-form4.component.html',
    styleUrls: ['./mic-section-a-form4.component.scss']
})
export class MicSectionAForm4Component extends MICPermitSectionA implements OnInit {
    @Input() id: any;
    page = "mic/sectionA-form4/";
    mm: any;

    user: any = {};

    types: any = [];

    micFormGroup: FormGroup;
    micModel: Mic;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    individual: BaseForm<string>[] = [
        new FormTitle({
            label: 'Details on the investor(s) responsible for establishing the enterprise:'
        }),
        new FormInput({
            key: 'full_name',
            label: 'a.  Full name',
            required: true,
        }),
        new FormSelect({
            key: 'citizenship',
            label: 'b. Citizenship ',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar',
        }),
        new FormInput({
            key: 'passport_or_nrc',
            label: 'c.  Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
            required: true,
        }),
        new FormTitle({
            label: 'Please upload a copy of passport (foreign nationals) or National Registration Card (Myanmar citizens):'
        }),
        new FormFile({
            key: 'copy_of_nrc_doc',
            label: 'File Name',
            required: true,
        }),
        new FormTitle({
            label: 'Please upload bank statements or other evidence of financial standing(in Myanmar language or in English):'
        }),
        new BaseFormArray({
            key: 'bank_statements',
            formArray: [
                new FormFile({
                    key: 'bank_statement_doc',
                    label: 'Name of document:',
                    multiple: true,
                    required: true,
                })
            ],
        }),
        new FormTitle({
            label: 'd. Address '
        }),
        new FormSelect({
            key: 'individual_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'individual_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'individual_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'individual_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'individual_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'individual_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormInput({
            key: 'phone_no',
            label: 'e. Phone number ',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'f. E-mail address',
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
        new FormTextArea({
            key: 'business_expertise',
            label: 'g. Please describe your sectoral and regional business expertise',
            required: true,
        }),
    ]
    company: BaseForm<string>[] = [
        new FormTitle({
            label: 'Details on the investor(s) responsible for establishing the enterprise:'
        }),
        new FormInput({
            key: 'company_name',
            label: 'a.  Name',
            required: true,
        }),
        new FormSelect({
            key: 'incorporate_country',
            label: 'b. Country of incorporation',
            options$: this.lotService.getCountry(),
            required: true,
        }),
        new FormInput({
            key: 'company_type',
            label: 'c. Type of company',
            required: true,
        }),
        new FormInput({
            key: 'certificate_no',
            label: 'Company Registration Certificate Number',
            required: true,
        }),
        new FormTitle({
            label: 'Please upload a copy of the Certificate of Incorporation/company registration certificate:'
        }),
        new FormFile({
            key: 'company_certificate_doc',
            label: 'File Name',
            required: true,
            columns: 'col-md-12',
        }),
        new FormTitle({
            label: 'Please upload the most recent annual audit report and other evidence of the financial conditions of the business (in Myanmar language or English):'
        }),
        new BaseFormArray({
            key: 'recent_annual_audit',
            formArray: [
                new FormFile({
                    key: 'recent_annual_audit_doc',
                    label: 'Name of document:',
                    multiple: true,
                    required: true,
                })
            ],
        }),
        new FormTitle({
            label: 'd. Address '
        }),
        new FormSelect({
            key: 'company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'company_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormInput({
            key: 'phone_no',
            label: 'e. Phone number ',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'fax_no',
            label: 'f. Fax number',
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'g. E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'website',
            label: 'h. Website',
            columns: 'col-md-6'
        }),
        new FormTextArea({
            key: 'short_description',
            label: 'i. Short description of company',
            required: true
        }),
        new FormTitle({
            label: 'j. Parent / Holding company'
        }),
        new FormInput({
            key: 'parent_company_name',
            label: 'Name',
            required: true,
        }),
        new FormTitle({
            label: 'Address'
        }),
        new FormSelect({
            key: 'parent_company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'parent_company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'parent_company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'parent_company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'parent_company_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'parent_company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'parent_company_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'parent_company_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'parent_company_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'parent_company_address',
            label: 'Additional address details',
            required: true
        }),
        new FormTitle({
            label: 'k. Contact person',
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full Name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_phone_no',
            label: 'Phone number',
            type: 'number',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ]
    government: BaseForm<string>[] = [
        new FormTitle({
            label: 'Details on the investor(s) responsible for establishing the enterprise:'
        }),
        new FormInput({
            key: 'name',
            label: 'a. Name of government department / organization',
            required: true
        }),
        new FormTitle({
            label: 'b. Address',
        }),
        new FormSelect({
            key: 'government_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'government_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'government_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'government_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'government_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'government_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormInput({
            key: 'phone_no',
            label: 'c. Phone number',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'fax',
            label: 'd. Fax number',
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'e. E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'website',
            label: 'f. Website',
            columns: 'col-md-6'
        }),
        new FormTitle({
            label: 'g. Contact Person',
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_phone_no',
            label: 'Phone number',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ]
    investor: BaseForm<string>[] = [
        new FormSectionTitleCounter({
            label: 'Investor'
        }),
        new FormSelect({
            key: 'investor_type',
            options$: this.localService.getType(),
            label: 'Type',
            required: true
        }),
        // new FormSelect({
        //     key: 'country',
        //     label: 'Citizenship or country of incorporation/ registration/ origin',
        //     options$: this.lotService.getCountry(),
        //     required: true
        // }),
        new BaseFormGroup({
            key: 'individual',
            formGroup: this.individual,
            criteriaValue: {
                key: 'investor_type',
                value: 'Individual'
            }
        }),
        new BaseFormGroup({
            key: 'company',
            formGroup: this.company,
            criteriaValue: {
                key: 'investor_type',
                value: 'Company'
            }
        }),
        new BaseFormGroup({
            key: 'government',
            formGroup: this.government,
            criteriaValue: {
                key: 'investor_type',
                value: 'Government department/organization'
            }
        })
    ]
    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '1. Please give an overview of the investor(s) responsible for establishing the enterprise:'
        }),
        new BaseFormArray({
            key: 'investor_list',
            label: 'Add Investor',
            formArray: this.investor
        })
    ]

    constructor(
        private activatedRoute: ActivatedRoute,
        private lotService: LocationService,
        private http: HttpClient,
        private router: Router,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private localService: LocalService
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));
                        this.createFormData(form);
                    });
            }
        }).unsubscribe();
    }

    createFormData(form: any) {
        this.next_label = this.mm ? "NEXT SECTION" : "NEXT";
        this.form = form || {};
        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

        this.micModel.investor_list = this.micModel?.investor_list || form?.data?.investor_list;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel || {}, form?.data);

        this.page = "mic/sectionA-form4/";
        this.page += this.mm ? this.micModel.id + '/mm' : this.micModel.id;

        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
        });

        this.changeLanguage();
        this.loading = false;
    }

    changeLanguage() {
        var lan = this.mm || 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    getCountries(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.is_save = false;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.is_save = true;
        // this.saveFormProgress(this.micModel.id, this.page);
        this.saveFormData();
    }

    saveFormData() {
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            this.micModel = { ...this.micModel, ...formValue };

            this.service.create(this.micModel, { secure: true })
                .subscribe(x => {
                    if (this.mm == 'mm') {
                        this.form.data_mm = x;
                    } else {
                        this.form.data = x;
                    }
                    this.store.dispatch(new CurrentForm({ form: this.form }));

                    this.spinner = false;
                    this.is_save = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionB-form1', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionB-form1', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionA-form4', id, 'mm']);
                }
            }
        }

    }

}
