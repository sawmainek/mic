import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { currentForm, comments } from './../../../../core/form/_selectors/form.selectors';
import { MICFormService } from './../../../../../services/micform.service';
import { ToastrService } from 'ngx-toastr';
import { FormControlService } from './../../../../custom-component/dynamic-forms/form-control.service';
import { FormNote } from '../../../../custom-component/dynamic-forms/base/form-note';
import { FormRadio } from './../../../../custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from './../../../../custom-component/dynamic-forms/base/form-title';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { AuthService } from 'src/services/auth.service';
import { Location } from '@angular/common';
import { Mic } from 'src/app/models/mic';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { MICPermitSectionA } from '../mic-permit-section-a';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-mic-section-a-form2',
    templateUrl: './mic-section-a-form2.component.html',
    styleUrls: ['./mic-section-a-form2.component.scss']
})
export class MicSectionAForm2Component extends MICPermitSectionA implements OnInit {
    menu: any = "sectionA";
    @Input() id: any;
    micModel: Mic;
    page = "mic/sectionA-form2/";
    mm: any;

    user: any = {};

    form: any;
    service: any;
    formGroup: FormGroup;

    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    forms: BaseForm<string>[] = [
        new FormTitle({
            label: '1. Has the enterprise been legally established in Myanmar yet?',
            required: true
        }),
        new FormRadio({
            key: 'overview_is_established',
            label: 'Yes, established',
            value: 'Yes',
            required: true,
            columns: 'col-md-6'
        }),
        new FormRadio({
            key: 'overview_is_established',
            label: 'No, established',
            value: 'No',
            required: true,
            columns: 'col-md-6'
        }),
        new FormNote({
            label: 'If the investor has not yet legally established the enterprise, the natural or legal person responsible for establishing such enterprise may submit the proposal as an investor. The establishment as the enterprise under the law is a condition of being issued the permit and shall not alter any of the obligations of the investor under the Myanmar Investment Law.'
        })
    ]

    constructor(
        private router: Router,
        public location: Location,
        public formCtrService: FormControlService,
        private formService: MICFormService, // For New Flow
        private micService: MicService,// For New Flow
        private micMMService: MICMMService,// For New Flow
        private store: Store<AppState>,
        private toast: ToastrService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
    ) {
        super(formCtrService);
        this.loading = true;
        this.formGroup = this.formCtrService.toFormGroup(this.forms);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            // For New Flow
            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));
                        this.createFormData(form);
                    });
            }
        }).unsubscribe();
    }

    createFormData(form: any) {
        // For New Flow
        this.form = form || {};
        this.micModel = this.mm ? form.data_mm || {} : form.data || {};

        this.formGroup = this.formCtrService.toFormGroup(this.forms, this.micModel || {}, form.data || {});

        this.page = "mic/sectionA-form2/";
        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
        });


        this.changeLanguage();
        this.loading = false;
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    openMenu(menu) {
        this.menu = menu;
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        //For New Flow ths.micService => this.service
        this.service.create({ ...this.micModel, ...this.formGroup.value })
            .subscribe(x => {
                if (this.mm == 'mm') {
                    this.form.data_mm = x;
                } else {
                    this.form.data = x;
                }
                this.store.dispatch(new CurrentForm({ form: this.form }));
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form.id, this.micModel.id, this.page);
                    this.redirectLink(this.form.id)
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }

    // For New Flow
    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            //For choose Myanmar Language
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionA-form3', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionA-form3', id]);
                }
                else {
                    this.router.navigate([page + '/mic/sectionA-form2', id, 'mm']);
                }
            }
        }
    }

}
