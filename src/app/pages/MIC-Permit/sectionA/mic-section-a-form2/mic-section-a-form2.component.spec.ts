import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionAForm2Component } from './mic-section-a-form2.component';

describe('MicSectionAForm2Component', () => {
  let component: MicSectionAForm2Component;
  let fixture: ComponentFixture<MicSectionAForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionAForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionAForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
