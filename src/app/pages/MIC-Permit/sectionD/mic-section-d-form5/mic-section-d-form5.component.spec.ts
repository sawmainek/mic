import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionDForm5Component } from './mic-section-d-form5.component';

describe('MicSectionDForm5Component', () => {
  let component: MicSectionDForm5Component;
  let fixture: ComponentFixture<MicSectionDForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionDForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionDForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
