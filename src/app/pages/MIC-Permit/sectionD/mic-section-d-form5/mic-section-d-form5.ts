import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';

export class MicSectionDForm5 {
    
    public forms: BaseForm<string>[] = []

    constructor(
        private localService: LocalService,
    ) {
        this.forms = [
            new BaseFormArray({
                key: 'land',
                formArray: [
                    new FormSectionTitle({
                        label: '2. Location of investment and lease agreements'
                    }),
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location 1',
                        style: { 'font-size': '18px' }
                    }),
                    new FormSectionTitle({
                        label: 'c. Particulars on land used/leased'
                    }),
                    new FormTitle({
                        label: 'Please upload a location map/layout plan here:'
                    }),
                    new FormFile({
                        key: 'layout_plan_document',
                        label: 'File Name',
                        required: true
                    }),
                    new FormInput({
                        key: 'area',
                        label: 'i. Area size',
                        required: true,
                        columns: 'col-md-6 d-flex'
                    }),
                    new FormSelect({
                        key: 'unit',
                        label: 'Choose unit of measurement for land area',
                        options$: this.localService.getUnit(),
                        required: true,
                        columns: 'col-md-6 d-flex',
                    }),
                    new FormTextArea({
                        key: 'description',
                        label: 'ii. In case the land is rented by multiple lessees, describe whether there is clear division of the land (e.g. fences, separate entry/exit, etc.)',
                    }),
                    new FormTitle({
                        label: 'Optional: upload a map/photo here:'
                    }),
                    new FormFile({
                        key: 'map_document',
                        label: 'File Name'
                    }),
                    new FormSelect({
                        key: 'land_type',
                        label: 'iii. Type of land',
                        options$: this.localService.getLandType(),
                        required: true
                    }),
                    new FormTitle({
                        label: 'iv. Is it necessary to change the land type?'
                    }),
                    new FormRadio({
                        key: 'is_change',
                        label: 'Yes',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'is_change',
                        label: 'No',
                        value: 'No',
                        required: true,
                        columns: 'col-md-8'
                    }),
                    new FormTextArea({
                        key: 'land_type_description',
                        label: 'Please specify the desired land type and the status of the process to obtain permission to change the land type:',
                        required: true,
                        criteriaValue: {
                            key: 'is_change',
                            value: 'Yes'
                        }
                    }),
                    new FormTitle({
                        label: 'If necessary, please upload supporting documents (e.g. recommendations or permission obtained on change of land use):',
                        pageBreak: 'before'
                        // criteriaValue: {
                        //   key: 'is_change',
                        //   value: 'Yes'
                        // }
                    }),
                    new BaseFormArray({
                        key: 'change_documents',
                        formArray: [
                            new FormFile({
                                key: 'document',
                                label: 'Name of document:',
                                multiple: true,
                            }),
                        ],
                        defaultLength: 1,
                        // criteriaValue: {
                        //   key: 'is_change',
                        //   value: 'Yes'
                        // }
                    }),
                    new FormTitle({
                        label: 'v. Is the investor required to significantly alter the topography or elevation of the proposed land?',
                    }),
                    new FormRadio({
                        key: 'is_required',
                        label: 'Yes',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'is_required',
                        label: 'No',
                        value: 'No',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormTextArea({
                        key: 'required_description',
                        label: 'Describe specifically to choosing yes',
                        required: true,
                        criteriaValue: {
                            key: 'is_required',
                            value: 'Yes'
                        }
                    }),
                    new FormTitle({
                        label: 'Please upload supporting documents here:',
                        criteriaValue: {
                            key: 'is_required',
                            value: 'Yes'
                        }
                    }),
                    new FormFile({
                        key: 'required_document',
                        label: 'Name of document:',
                        criteriaValue: {
                            key: 'is_required',
                            value: 'Yes'
                        }
                    }),
                ]
            })
            
        ];
    }
}