import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionDForm8Component } from './mic-section-d-form8.component';

describe('MicSectionDForm8Component', () => {
  let component: MicSectionDForm8Component;
  let fixture: ComponentFixture<MicSectionDForm8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionDForm8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionDForm8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
