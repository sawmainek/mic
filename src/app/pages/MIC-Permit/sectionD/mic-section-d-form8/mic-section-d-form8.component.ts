import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermitSectionD } from '../mic-permit-section-d';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { BaseFormData } from 'src/app/core/form';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-d-form8',
    templateUrl: './mic-section-d-form8.component.html',
    styleUrls: ['./mic-section-d-form8.component.scss']
})
export class MicSectionDForm8Component extends MICPermitSectionD implements OnInit {
    page = "mic/sectionD-form8/";
    menu: any = "sectionD";
    correct: any = false;
    index: any = 0;
    @Input() id: any;

    user: any = {};
    is_officer: boolean = true;

    previous_use_consumption: any;
    micModel: any = {};
    micFormGroup: FormGroup;
    mm: any;
    form: any;
    service: any;

    submitted = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    is_more = false;
    loaded: boolean = false;
    is_last: boolean = true;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location 1',
            style: { 'font-size': '18px' }
        }),
        new FormSectionTitle({
            label: 'f. Particulars on land/building use and consumption'
        }),
        new FormInput({
            key: 'product_value',
            label: 'Expected annual value of products/services to be produced',
            required: true,
        }),
        new FormInput({
            key: 'electricity_amount',
            label: 'Expected annual requirement of electricity',
            required: true,
        }),
        new FormInput({
            key: 'supply_amount',
            label: 'Expected annual requirement of water supply',
            required: true,
        }),
        new FormSectionTitle({
            label: 'All the data for location 1 is completed now. If you have another location, click the button below:'
        }),
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        public location: Location,
        private toast: ToastrService,
        private store: Store<AppState>,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;

        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;
    }

    ngOnInit(): void {
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm && this.mm == 'mm' ? this.micMMService : this.micService;

            this.index = +paramMap.get('index');
            this.micForm[2].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm && this.mm == 'mm' ? form.data_mm || {} : form.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.use_consumption = this.micModel?.use_consumption || form?.data?.use_consumption || [];
                        this.micModel.use_consumption[this.index] = this.micModel.use_consumption[this.index] || form?.data?.use_consumption && form?.data?.use_consumption[this.index] || {};

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel.use_consumption[this.index] || {}, form?.data?.use_consumption && form?.data?.use_consumption[this.index] || {});

                        this.page = "mic/sectionD-form8/";
                        this.page += this.mm && this.mm == 'mm' ? (this.micModel.id + '/' + this.index + '/mm') : (this.micModel.id + '/' + this.index);

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        if ((this.index < this.micModel?.use_consumption?.length - 1) || (this.index < this.micModel?.owner?.length - 1)) {
                            this.is_last = false;
                        }
                        else {
                            this.is_last = true;
                        }

                        this.next_label = this.is_last && this.mm && this.mm == 'mm' ? "NEXT SECTION" : "NEXT";

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    moreLocation() {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_more = true;
        this.is_draft = false;

        let use_consumption = this.micModel.use_consumption ? this.micModel.use_consumption.map(x => Object.assign({}, x)) || [] : [];
        use_consumption[this.index] = this.micFormGroup.value;

        let data = { 'use_consumption': use_consumption };
        this.service.create({ ...this.micModel, ...data })
            .subscribe(x => {
                this.spinner = false;
                this.is_more = false;

                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                    if (this.form?.language == 'Myanmar') {
                        this.router.navigate([page + '/mic/sectionD-form3', this.form?.id, (this.index + 1), 'mm']);
                    } else {
                        if (this.mm && this.mm == 'mm') {
                            this.router.navigate([page + '/mic/sectionD-form3', this.form?.id, (this.index + 1)]);
                        } else {
                            this.router.navigate([page + '/mic/sectionD-form8', this.form?.id, this.index, 'mm']);
                        }
                    }
                } else {
                    this.toast.success('Saved successfully.');
                }
                // this.router.navigate(['/pages/mic/sectionD-form3', x.id, this.micModel.use_consumption.length]);
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.is_more = false;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            alert('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.is_more = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        let use_consumption = this.micModel.use_consumption ? this.micModel.use_consumption.map(x => Object.assign({}, x)) || [] : [];
        use_consumption[this.index] = this.micFormGroup.value;

        let data = { 'use_consumption': use_consumption };
        this.service.create({ ...this.micModel, ...data }, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                    this.redirectLink(this.form?.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            const route = this.is_last ? '/mic/sectionE-form1/' + id : '/mic/sectionD-form3/' + id + '/' + (this.index + 1);

            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + route, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + route]);
                } else {
                    this.router.navigate([page + '/mic/sectionD-form8', id, this.index, 'mm']);
                }
            }
        }
    }
}
