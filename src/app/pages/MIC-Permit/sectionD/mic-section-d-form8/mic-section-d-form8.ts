import { BaseFormArray } from './../../../../custom-component/dynamic-forms/base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';

export class MicSectionDForm8 {
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new BaseFormArray({
                key: 'use_consumption',
                formArray: [
                    new FormSectionTitle({
                        label: '2. Location of investment and lease agreements'
                    }),
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location 1',
                        style: { 'font-size': '18px' }
                    }),
                    new FormSectionTitle({
                        label: 'f. Particulars on land/building use and consumption'
                    }),
                    new FormInput({
                        key: 'product_value',
                        label: 'Expected annual value of products/services to be produced',
                        required: true,
                    }),
                    new FormInput({
                        key: 'electricity_amount',
                        label: 'Expected annual requirement of electricity',
                        required: true,
                    }),
                    new FormInput({
                        key: 'supply_amount',
                        label: 'Expected annual requirement of water supply',
                        required: true,
                    }),
                    new FormSectionTitle({
                        label: 'All the data for location 1 is completed now. If you have another location, click the button below:'
                    })
                ]
            })
            
        ];
    }
}