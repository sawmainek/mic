import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionDForm6Component } from './mic-section-d-form6.component';

describe('MicSectionDForm6Component', () => {
  let component: MicSectionDForm6Component;
  let fixture: ComponentFixture<MicSectionDForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionDForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionDForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
