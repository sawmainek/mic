import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { MICPermitSectionD } from '../mic-permit-section-d';
import { AppState, currentUser } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { LocalService } from 'src/services/local.service';

@Component({
    selector: 'app-mic-section-d-form6',
    templateUrl: './mic-section-d-form6.component.html',
    styleUrls: ['./mic-section-d-form6.component.scss']
})
export class MicSectionDForm6Component extends MICPermitSectionD implements OnInit {
    menu: any = "sectionD";
    correct: any = false;
    edit: any = false;
    @Input() id: any;
    page = "mic/sectionD-form6/";

    user: any = {};

    mm: any;
    form: any;
    service: any;
    index: any = 0;
    previous_building: any;
    micModel: any = {};
    micFormGroup: FormGroup;

    submitted = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    building: BaseForm<any>[] = [
        new FormSectionTitleCounter({
            label: "Building"
        }),
        new FormInput({
            key: 'area',
            label: 'Area',
            required: true,
            type: 'number',
            columns: 'col-md-6',
        }),
        new FormSelect({
            key: 'unit',
            label: 'Unit of measurement',
            required: true,
            options$: this.localService.getUnit(),
            columns: 'col-md-6',
        }),
        new FormInput({
            key: 'detail_regarding',
            label: 'Details regarding the construction (is construction complete? If not, who is responsible for construction?)',
            required: true,
        }),
        new FormInput({
            key: 'value_building',
            label: 'Value of Building in MMK',
            type: 'number',
            required: true,
            columns: 'col-md-12',
        }),
        new BaseFormGroup({
            key: 'building_support_docs',
            formGroup: [
                new FormFile({
                    label: 'Please upload a building map/drawing/concept here:',
                    key: 'building_docs',
                    required: true,
                }),
                new FormTitle({
                    label: 'If building is complete, submit a photo:'
                }),
                new FormFile({
                    label: 'File Name',
                    key: 'building_photo'
                }),
                new FormTitle({
                    label: 'Other supporting documents',
                }),
                new BaseFormArray({
                    key: 'other_docs',
                    formArray: [
                        new FormFile({
                            key: 'document',
                            label: 'Name of document:',
                            multiple: true,
                        }),
                    ],
                }),
                new FormTitle({
                    label: ' '
                })
            ]
        })
    ];

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2.Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location 1',
            style: { 'font-size': '18px' }
        }),
        new FormSectionTitle({
            label: 'd. Particulars on buildings used/leased'
        }),
        new BaseFormArray({
            key: 'building',
            addMoreText: 'Add more buildings',
            formArray: this.building,
        }),
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        public location: Location,
        private toast: ToastrService,
        private store: Store<AppState>,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private localService: LocalService,

    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm && this.mm == 'mm' ? this.micMMService : this.micService;

            this.index = +paramMap.get('index');
            this.micForm[2].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.building = this.micModel?.building || form?.data?.building || [];
                        this.micModel.building[this.index] = this.micModel.building[this.index] || form?.data?.building && form?.data?.building[this.index] || {};

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel.building[this.index] || {}, form?.data?.building && form?.data?.building[this.index] || {});

                        this.page = "mic/sectionD-form6/";
                        this.page += this.mm && this.mm == 'mm' ? (this.micModel.id + '/' + this.index + '/mm') : (this.micModel.id + '/' + this.index);

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var unit = this.building.filter(x => x.key == "unit")[0];
        unit.options$ = this.localService.getUnit(lan);

        this.authService.changeLanguage$.subscribe(x => {
            unit.options$ = this.localService.getUnit(x);
        })
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let building = this.micModel.building ? this.micModel.building.map(x => Object.assign({}, x)) || [] : [];
            building[this.index] = formValue;
            this.service.create({ ...this.micModel, ...{ 'building': building } }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionD-form7', id, this.index, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/mic/sectionD-form7', id, this.index]);
                } else {
                    this.router.navigate([page + '/mic/sectionD-form6', id, this.index, 'mm']);
                }
            }
        }

    }
}
