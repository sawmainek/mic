import { BaseFormArray } from './../../../../custom-component/dynamic-forms/base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { LocationService } from 'src/services/location.service';

export class MicSectionDForm4 {
    
    public forms: BaseForm<string>[] = []

    constructor(
        private lotService: LocationService,
    ) {
        this.forms = [
            new BaseFormArray({
                key: 'location',
                formArray: [
                    new FormSectionTitle({
                        label: '2. Location of investment and lease agreements'
                    }),
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location 1',
                        style: {'font-size': '18px'}
                    }),
                    new FormSectionTitle({
                        label: 'b. Location'
                    }),
                    new FormTitle({
                        label: 'i. Address',
                    }),
                    new FormSelect({
                        key: 'country',
                        label: 'Country',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar'
                    }),
                    new FormSelect({
                        key: 'state',
                        label: 'State / Region:',
                        options$: this.lotService.getState(),
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'district',
                        label: 'District:',
                        options$: this.lotService.getDistrict(),
                        required: true,
                        columns: 'col-md-6',
                        filter: {
                            parent: 'state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'township',
                        label: 'Township:',
                        options$: this.lotService.getTownship(),
                        required: true,
                        columns: 'col-md-6',
                        filter: {
                            parent: 'district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormInput({
                        key: 'ward',
                        label: 'Ward:',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormTextArea({
                        key: 'address',
                        label: 'Additional address details:',
                        required: true,
                    }),
                    new FormTitle({
                        label: 'ii. Please indicate if the location is in a relevant business zone:'
                    }),
                    new FormRadio({
                        key: 'zone',
                        label: 'Not in a business zone',
                        value: 'Not in a business zone',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'zone',
                        label: 'Industrial zone',
                        value: 'Industrial',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'zone',
                        label: 'Hotel zone',
                        value: 'Hotel',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'zone',
                        label: 'Trade zone',
                        value: 'Trade',
                        required: true,
                        columns: 'col-md-4'
                    }),
                    new FormRadio({
                        key: 'zone',
                        label: 'Other',
                        value: 'Other',
                        required: true,
                        columns: 'col-md-8'
                    }),
                    new FormInput({
                        key: 'zone_description',
                        label: 'Describe chosen zone specifically',
                        required: true,
                        columns: 'col-md-12',
                        criteriaValue: {
                            key: 'zone',
                            value: ['Industrial', 'Hotel', 'Trade', 'Other']
                        }
                    })
                ]
            })
        ];
    }
}