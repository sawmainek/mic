import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { MICPermitSectionD } from '../mic-permit-section-d';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-d-form4',
    templateUrl: './mic-section-d-form4.component.html',
    styleUrls: ['./mic-section-d-form4.component.scss']
})
export class MicSectionDForm4Component extends MICPermitSectionD implements OnInit {
    page = "mic/sectionD-form4/";
    menu: any = "sectionD";
    index: any = 0;
    @Input() id: any;
    mm: any = null;

    correct: any = false;
    radio: any;

    user: any = {};

    previous_locations: any = [];
    micModel: any = {};
    micFormGroup: FormGroup;
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location 1',
            style: { 'font-size': '18px' }
        }),
        new FormSectionTitle({
            label: 'b. Location'
        }),
        new FormTitle({
            label: 'i. Address',
        }),
        new FormSelect({
            key: 'country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-6',
            filter: {
                parent: 'state',
                key: 'state'
            },
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-6',
            filter: {
                parent: 'district',
                key: 'district'
            },
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'ward',
            label: 'Ward:',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormTextArea({
            key: 'address',
            label: 'Additional address details:',
            required: true,
        }),
        new FormTitle({
            label: 'ii. Please indicate if the location is in a relevant business zone:'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Not in a business zone',
            value: 'Not in a business zone',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Industrial zone',
            value: 'Industrial',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Hotel zone',
            value: 'Hotel',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Trade zone',
            value: 'Trade',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'zone',
            label: 'Other',
            value: 'Other',
            required: true,
            columns: 'col-md-8'
        }),
        new FormInput({
            key: 'zone_description',
            label: 'Describe chosen zone specifically',
            required: true,
            columns: 'col-md-12',
            criteriaValue: {
                key: 'zone',
                value: ['Industrial', 'Hotel', 'Trade', 'Other']
            }
        })
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private lotService: LocationService,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm && this.mm == 'mm' ? this.micMMService : this.micService;

            this.index = +paramMap.get('index');
            this.micForm[2].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.location = this.micModel?.location || form?.data?.location || [];
                        this.micModel.location[this.index] = this.micModel.location[this.index] || form?.data?.location && form?.data?.location[this.index] || {};

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel?.location[this.index] || {}, form?.data?.location && form?.data?.location[this.index] || {});

                        this.page = "mic/sectionD-form4/";
                        this.page += this.mm && this.mm == 'mm' ? (this.micModel.id + '/' + this.index + '/mm') : (this.micModel.id + '/' + this.index);

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var state = this.micForm.filter(x => x.key == "state")[0];
        state.options$ = this.lotService.getState(lan);

        var district = this.micForm.filter(x => x.key == "district")[0];
        district.options$ = this.lotService.getDistrict(lan);

        var township = this.micForm.filter(x => x.key == "township")[0];
        township.options$ = this.lotService.getTownship(lan);

        this.authService.changeLanguage$.subscribe(x => {
            state.options$ = this.lotService.getState(x);
            district.options$ = this.lotService.getDistrict(x);
            township.options$ = this.lotService.getTownship(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        let location = this.micModel.location ? this.micModel.location.map(x => Object.assign({}, x)) || [] : [];
        location[this.index] = this.micFormGroup.value;
        this.service.create({ ...this.micModel, ...{ 'location': location } }, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                    this.redirectLink(this.form?.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionD-form5', id, this.index, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/mic/sectionD-form5', id, this.index]);
                } else {
                    this.router.navigate([page + '/mic/sectionD-form4', id, this.index, 'mm']);
                }
            }
        }
    }
}
