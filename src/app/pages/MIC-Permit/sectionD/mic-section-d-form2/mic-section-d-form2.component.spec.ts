import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionDForm2Component } from './mic-section-d-form2.component';

describe('MicSectionDForm2Component', () => {
  let component: MicSectionDForm2Component;
  let fixture: ComponentFixture<MicSectionDForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionDForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionDForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
