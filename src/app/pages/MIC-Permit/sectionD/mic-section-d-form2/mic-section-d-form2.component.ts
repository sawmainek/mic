import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Mic } from 'src/app/models/mic';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Location } from '@angular/common';
import { MICPermitSectionD } from '../mic-permit-section-d';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { AuthService } from 'src/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-mic-section-d-form2',
    templateUrl: './mic-section-d-form2.component.html',
    styleUrls: ['./mic-section-d-form2.component.scss']
})
export class MicSectionDForm2Component extends MICPermitSectionD implements OnInit {
    menu: any = "sectionD";
    @Input() id: any;
    page = "mic/sectionD-form2/";

    correct: any = false;

    user: any = {};

    micModel: Mic;
    micFormGroup: FormGroup;
    mm: any;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    submitted = false;
    loading = false;

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormTitle({
            label: '1. Do you wish to apply for permission to lease or use land according to the Myanmar Investment Rules 116 together with this proposal?',
            required: true
        }),
        new FormRadio({
            key: 'wish_to_apply',
            label: 'Yes',
            value: 'Yes',
            required: true,
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'wish_to_apply',
            label: 'No',
            value: 'No',
            required: true,
            columns: 'col-md-4'
        }),
        new FormNote({
            label: 'An Investor who obtains a Permit or an Endorsement under the Myanmar Investment Law has the right to obtain a long-term lease of land or building from the private owned or from the relevant government departments, governmental organizations managed by the Government, or owned by the State in accordance with the stipulations in order to do investment. Myanmar citizen investors may invest in their own land or building in accordance with relevant laws. The following investors shall not be required to obtain the land use right from the Commission through submission in connection with Section 50 of the Myanmar Investment Law: (a) Myanmar citizen investors; or (b) an investor who has been granted under the laws to make investment in a company that retains its status as a Myanmar company (according to Myanmar Companies Law) after dealing with foreign investors. If the investor proposes to sublease land or buildings from a person who has previously obtained the land use right for the land and buildings, the investor shall not be required to apply for the separate land use right. However, the investor must notify the commission.'
        })
    ]

    constructor(
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private ref: ChangeDetectorRef,
        private store: Store<AppState>,
        private toast: ToastrService,
        public formCtlService: FormControlService,
        public location: Location,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel, form.data || {});

                        this.page = "mic/sectionD-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
            else {
                this.ref.detectChanges();
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.micModel = { ...this.micModel, ...this.micFormGroup.value };

        this.service.create(this.micModel)
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                    this.redirectLink(this.form?.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionD-form3', id, 0, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionD-form3', id, 0]);
                } else {
                    this.router.navigate([page + '/mic/sectionD-form2', id, 'mm']);
                }
            }
        }
    }

}
