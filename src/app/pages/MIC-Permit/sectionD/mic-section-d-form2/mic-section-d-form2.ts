import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';

export class MicSectionDForm2 {

    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new FormTitle({
                label: '1. Do you wish to apply for permission to lease or use land according to the Myanmar Investment Rules 116 together with this proposal?',
                required: true
            }),
            new FormRadio({
                key: 'wish_to_apply',
                label: 'Yes',
                value: 'Yes',
                required: true,
                columns: 'col-md-4'
            }),
            new FormRadio({
                key: 'wish_to_apply',
                label: 'No',
                value: 'No',
                required: true,
                columns: 'col-md-4'
            }),
            new FormNote({
                label: 'An Investor who obtains a Permit or an Endorsement under the Myanmar Investment Law has the right to obtain a long-term lease of land or building from the private owned or from the relevant government departments, governmental organizations managed by the Government, or owned by the State in accordance with the stipulations in order to do investment. Myanmar citizen investors may invest in their own land or building in accordance with relevant laws. The following investors shall not be required to obtain the land use right from the Commission through submission in connection with Section 50 of the Myanmar Investment Law: (a) Myanmar citizen investors; or (b) an investor who has been granted under the laws to make investment in a company that retains its status as a Myanmar company (according to Myanmar Companies Law) after dealing with foreign investors. If the investor proposes to sublease land or buildings from a person who has previously obtained the land use right for the land and buildings, the investor shall not be required to apply for the separate land use right. However, the investor must notify the commission.'
            })
        ];
    }
}