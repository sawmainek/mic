import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionDForm7Component } from './mic-section-d-form7.component';

describe('MicSectionDForm7Component', () => {
  let component: MicSectionDForm7Component;
  let fixture: ComponentFixture<MicSectionDForm7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionDForm7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionDForm7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
