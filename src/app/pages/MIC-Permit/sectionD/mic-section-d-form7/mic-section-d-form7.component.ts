import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { cloneDeep } from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators } from '@angular/forms';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { MICPermitSectionD } from '../mic-permit-section-d';
import { AppState, currentUser } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

@Component({
    selector: 'app-mic-section-d-form7',
    templateUrl: './mic-section-d-form7.component.html',
    styleUrls: ['./mic-section-d-form7.component.scss']
})
export class MicSectionDForm7Component extends MICPermitSectionD implements OnInit {
    menu: any = "sectionD";
    correct: any = false;
    page = "mic/sectionD-form7/";
    @Input() id: any;

    user: any = {};

    mm: any;
    form: any;
    service: any;
    index: any = 0;
    previous_lease_agreement: any;
    micModel: any = {};
    micFormGroup: FormGroup;

    submitted = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    individual: BaseForm<any>[] = [
        new FormInput({
            key: 'individual_name',
            label: '(2) Full name',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'nrc',
            label: '(3) National Registration Card No.',
            columns: 'col-md-6'
        }),
        new FormTitle({
            label: '(4) Address',
        }),
        new FormSelect({
            key: 'individual_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'individual_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'individual_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'individual_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'individual_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'individual_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details'
        }),
        new FormInput({
            key: 'phone',
            label: '(5) Phone number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: '(6) E-mail address',
            validators: [Validators.email],
            columns: 'col-md-6'
        }),

    ];

    company: BaseForm<any>[] = [
        new FormInput({
            key: 'company_name',
            label: '(2) Name of company',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'company_reg_no',
            label: '(3) Company Registration No.',
            columns: 'col-md-6'
        }),
        new FormTitle({
            label: '(4) Address',
        }),
        new FormSelect({
            key: 'company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'company_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormTitle({
            label: '(5) Contact person',
        }),
        new FormInput({
            key: 'person_name',
            label: 'Full name',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'person_position',
            label: 'Position',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'person_phone',
            label: 'Phone number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'person_email',
            label: 'E-mail address',
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ];

    government: BaseForm<any>[] = [
        new FormInput({
            key: 'government_name',
            label: '(2) Name of government department / organization',
        }),
        new FormTitle({
            label: '(3) Address',
        }),
        new FormSelect({
            key: 'government_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'government_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'government_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'government_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'government_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'government_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormTitle({
            label: '(4) Contact Person'
        }),
        new FormInput({
            key: 'person_name',
            label: 'Full Name',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'person_position',
            label: 'Position',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'person_phone',
            label: 'Phone number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'person_email',
            label: 'E-mail address',
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ];

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location 1',
            style: { 'font-size': '18px' }
        }),
        new FormSectionTitle({
            label: 'e. Particulars on lease agreement'
        }),
        new FormNote({
            label: 'If the investor is investing in her/his own land, please leave Question 2e blank.'
        }),
        new FormTitle({
            label: 'Please upload the draft of the lease agreement:'
        }),
        new FormFile({
            key: 'agreement_document',
            label: 'File Name',
        }),
        new FormNote({
            label: 'Registered final copy should be submitted to DICA once received or after receiving the permit.'
        }),
        new FormInput({
            key: 'lessee_name',
            label: 'i. Name of lessee:',
        }),
        new FormTitle({
            label: 'ii. Proposed lease period'
        }),
        new FormDate({
            key: 'lease_from',
            label: 'From Date',
            columns: 'col-md-6',
        }),
        new FormDate({
            key: 'lease_to',
            label: 'To Date',
            columns: 'col-md-6',
        }),
        new FormSectionTitle({
            label: 'iii. Specify fees(s)'
        }),
        new FormInput({
            key: 'rental_fees',
            label: 'aa. Annual rental fees of land and/or buildings',
        }),
        new FormInput({
            key: 'fees_of_square',
            label: 'bb. Rental fees of 1 square meter per year of land and/or buildings',
        }),
        new FormInput({
            key: 'premium_fees',
            label: 'cc. Land Use Premium fees (rate per acre):',
        }),
        new FormNote({
            label: 'If the land and/or buildings belong to a Government Department/Organization, the LUP shall be paid in cash (Kyat) by the lessee.'
        }),
        new FormTitle({
            label: 'dd. Is the payment or part of the payment in form of capital?'
        }),
        new FormRadio({
            key: 'is_payment',
            label: 'Yes',
            value: 'Yes',
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'is_payment',
            label: 'No',
            value: 'No',
            columns: 'col-md-4'
        }),
        new FormInput({
            key: 'description',
            label: 'Describe specifically to choosing yes',
            criteriaValue: {
                key: 'is_payment',
                value: 'Yes'
            }
        }),
        new FormTitle({
            label: 'In case of sublease (i.e. if the owner is not the lessor of the land and/or buildings):',
        }),
        new FormTitle({
            label: 'aa. Particulars on the lessor :'
        }),
        new FormTitle({
            label: 'Please upload evidence of the lessor’s right to use land and/or buildings:'
        }),
        new FormFile({
            key: 'building_document',
            label: 'File Name',
        }),
        new FormNote({
            label: 'General power/ special power including the stamp from contract registration office should be uploaded. If subleasing from another investor, upload their Land Rights Authorization.',

        }),
        new FormSelect({
            key: 'owner_type',
            label: '(1) Type of lessor',
            options$: this.localService.getType(),
        }),
        new BaseFormGroup({
            key: 'individual',
            formGroup: this.individual,
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new BaseFormGroup({
            key: 'company',
            formGroup: this.company,
            criteriaValue: {
                key: 'owner_type',
                value: 'Company'
            }
        }),
        new BaseFormGroup({
            key: 'government',
            formGroup: this.government,
            criteriaValue: {
                key: 'owner_type',
                value: 'Government department/organization'
            }
        }),
        new FormTitle({
            label: 'bb. Does the owner agree to the lease agreement?'
        }),
        new FormRadio({
            key: 'is_agree',
            label: 'Yes',
            value: 'Yes',
            columns: 'col-md-4'
        }),
        new FormRadio({
            key: 'is_agree',
            label: 'No',
            value: 'No',
            columns: 'col-md-4'
        }),
        new FormTitle({
            label: 'cc. If subleasing state-owned land or buildings, please select which of the following apply to the lessor:',
            // criteriaValue: {
            //   key: 'is_agree',
            //   value: 'Yes'
            // }
        }),
        new FormRadio({
            key: 'lessor',
            label: 'A person who has previously obtained the right to use the state-owned and or buildings from the government department and government organization in accordance with the laws of the Union.',
            value: '1',
            // criteriaValue: {
            //   key: 'is_agree',
            //   value: 'Yes'
            // }
        }),
        new FormRadio({
            key: 'lessor',
            label: 'A person authorized to sub-lease or sub-license the state-owned land or building in accordance with the approval of the government department and government organization.',
            value: '2',
            // criteriaValue: {
            //   key: 'is_agree',
            //   value: 'Yes'
            // }
        }),
        new FormRadio({
            key: 'lessor',
            label: 'Not applicable',
            value: '3',
            // criteriaValue: {
            //   key: 'is_agree',
            //   value: 'Yes'
            // }
        }),
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private localService: LocalService,
        private lotService: LocationService
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm && this.mm == 'mm' ? this.micMMService : this.micService;

            this.index = +paramMap.get('index');
            this.micForm[2].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.lease_agreement = this.micModel?.lease_agreement || form?.data?.lease_agreement || [];
                        this.micModel.lease_agreement[this.index] = this.micModel.lease_agreement[this.index] || form?.data?.lease_agreement && form?.data?.lease_agreement[this.index] || {};

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel.lease_agreement[this.index] || {}, form?.data?.lease_agreement && form?.data?.lease_agreement[this.index] || {});

                        this.page = "mic/sectionD-form7/";
                        this.page += this.mm && this.mm == 'mm' ? (this.micModel.id + '/' + this.index + '/mm') : (this.micModel.id + '/' + this.index);

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var owner_type = this.micForm.filter(x => x.key == "owner_type")[0];
        owner_type.options$ = this.localService.getType(lan);

        this.authService.changeLanguage$.subscribe(x => {
            owner_type.options$ = this.localService.getType(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    checkDates(group: FormGroup) {
        const planFrom = group.controls.plan_location.get('plan_from').value;
        const planTo = group.controls.plan_location.get('plan_to').value;
        const planControl = group.controls.plan_location.get('plan_to');
        if (planControl.errors && !planControl.errors.date) {
            return;
        }
        else {
            if (Date.parse(planFrom) > Date.parse(planTo)) { planControl.setErrors({ date: true }); }
            else { planControl.setErrors(null); }
        }
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let lease_agreement = this.micModel.lease_agreement ? this.micModel.lease_agreement.map(x => Object.assign({}, x)) || [] : [];
            lease_agreement[this.index] = formValue;

            let data = { 'lease_agreement': lease_agreement };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionD-form8', id, this.index, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/mic/sectionD-form8', id, this.index]);
                } else {
                    this.router.navigate([page + '/mic/sectionD-form7', id, this.index, 'mm']);
                }
            }
        }
    }
}
