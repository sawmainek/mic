import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionDForm3Component } from './mic-section-d-form3.component';

describe('MicSectionDForm3Component', () => {
  let component: MicSectionDForm3Component;
  let fixture: ComponentFixture<MicSectionDForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionDForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionDForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
