import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { FileUploadService } from 'src/services/file_upload.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Location } from '@angular/common';
import { MICPermitSectionD } from '../mic-permit-section-d';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

@Component({
    selector: 'app-mic-section-d-form3',
    templateUrl: './mic-section-d-form3.component.html',
    styleUrls: ['./mic-section-d-form3.component.scss']
})
export class MicSectionDForm3Component extends MICPermitSectionD implements OnInit {
    menu: any = "sectionD";
    page = "mic/sectionD-form3/";
    index: any = 0;
    @Input() id: any;

    user: any = {};

    correct: any = false;

    mm: any;
    form: any;
    service: any;
    previous_owner: any;
    micModel: any = {};
    micFormGroup: FormGroup;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    individual: BaseForm<string>[] = [
        new FormInput({
            key: 'full_name',
            label: 'iii. Full name',
            required: true,
        }),
        new FormInput({
            key: 'passport_or_nrc',
            label: 'iv. National Registration Card No.',
            required: true,
        }),
        new FormTitle({
            label: 'v. Address'
        }),
        new FormSelect({
            key: 'individual_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4'
        }),
        new FormSelect({
            key: 'individual_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'individual_state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'individual_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'individual_district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormInput({
            key: 'phone_no',
            label: 'vi. Phone number',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'vii. E-mail address',
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ]
    company: BaseForm<string>[] = [
        new FormInput({
            key: 'company_name',
            label: 'iii. Name of company',
            required: true,
        }),
        new FormInput({
            key: 'company_reg_no',
            label: 'iv. Company Registration No.',
            required: true,
        }),
        new FormTitle({
            label: 'v. Address'
        }),
        new FormSelect({
            key: 'company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4'
        }),
        new FormSelect({
            key: 'company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'company_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'company_district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormTitle({
            label: 'vi. Contact person',
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full Name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_phone_no',
            label: 'Phone number',
            type: 'number',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ]
    government: BaseForm<string>[] = [
        new FormInput({
            key: 'name',
            label: 'iii. Name of government department/organization',
            required: true
        }),
        new FormTitle({
            label: 'iv. Address'
        }),
        new FormSelect({
            key: 'government_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4'
        }),
        new FormSelect({
            key: 'government_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'government_state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'government_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'government_district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormTitle({
            label: 'v. Contact Person',
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_phone_no',
            label: 'Phone number',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ]

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Location of investment and lease agreements'
        }),
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location ' + (this.index + 1),
            style: { 'font-size': '18px' }
        }),
        new FormSectionTitle({
            label: 'a.Owner of land and/or building(s)'
        }),
        new FormTitle({
            label: 'Please upload evidence of land ownership/ land use rights (unless location is in an Industrial Zone):'
        }),
        new FormFile({
            key: 'ownership_document',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'If the current owner has not changed the name of the former owner on the ownership evidence, please upload contract/agreement (e.g. special power, general power, etc.) between former owner and current owner:'
        }),
        new FormFile({
            key: 'agreement_document',
            label: 'File Name'
        }),
        new FormTitle({
            label: 'i. Period of land grant validity / initial period permitted to use the land'
        }),
        new FormDate({
            key: 'permit_from',
            label: 'From Date',
            columns: 'col-md-6',
            required: true
        }),
        new FormDate({
            key: 'permit_to',
            label: 'To Date',
            columns: 'col-md-6',
            required: true
        }),
        new FormSelect({
            key: 'owner_type',
            label: 'ii. Type of owner',
            options$: this.localService.getType(),
            required: true
        }),
        new BaseFormGroup({
            key: 'individual',
            formGroup: this.individual,
            criteriaValue: {
                key: 'owner_type',
                value: 'Individual'
            }
        }),
        new BaseFormGroup({
            key: 'company',
            formGroup: this.company,
            criteriaValue: {
                key: 'owner_type',
                value: 'Company'
            }
        }),
        new BaseFormGroup({
            key: 'government',
            formGroup: this.government,
            criteriaValue: {
                key: 'owner_type',
                value: 'Government department/organization'
            }
        })
        // new FormInput({
        //   key: 'individual_name',
        //   label: 'Full name',
        //   required: true,
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Individual'
        //   }
        // }),
        // new FormInput({
        //   key: 'nrc',
        //   label: 'National Registration Card No.',
        //   required: true,
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Individual'
        //   }
        // }),
        // new FormTextArea({
        //   key: 'individual_address',
        //   label: 'Address',
        //   required: true,
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Individual'
        //   }
        // }),
        // new FormInput({
        //   key: 'email',
        //   label: 'Email',
        //   required: true,
        //   validators: [Validators.email],
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Individual'
        //   }
        // }),
        // new FormInput({
        //   key: 'phone',
        //   label: "Phone",
        //   required: true,
        //   validators: [Validators.minLength(6)],
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Individual'
        //   }
        // }),
        // new FormInput({
        //   key: 'company_name',
        //   label: 'Name of company',
        //   required: true,
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Company'
        //   }
        // }),
        // new FormInput({
        //   key: 'register_no',
        //   label: 'Company registration number',
        //   required: true,
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Company'
        //   }
        // }),
        // new FormTextArea({
        //   key: 'company_address',
        //   label: 'Address',
        //   required: true,
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Company'
        //   }
        // }),
        // new FormInput({
        //   key: 'government_name',
        //   label: 'Name of government department / organization',
        //   required: true,
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Government'
        //   }
        // }),
        // new FormTextArea({
        //   key: 'government_address',
        //   label: 'Address',
        //   required: true,
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: 'Government'
        //   }
        // }),
        // new FormTitle({
        //   label: 'Contact Person Information',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: ['Government', 'Company']
        //   }
        // }),
        // new FormInput({
        //   key: 'person_name',
        //   label: 'Full name',
        //   required: true,
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: ['Government', 'Company']
        //   }
        // }),
        // new FormInput({
        //   key: 'person_position',
        //   label: 'Position',
        //   required: true,
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: ['Government', 'Company']
        //   }
        // }),
        // new FormInput({
        //   key: 'person_phone',
        //   label: 'Phone',
        //   required: true,
        //   validators: [Validators.minLength(6)],
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: ['Government', 'Company']
        //   }
        // }),
        // new FormInput({
        //   key: 'person_email',
        //   label: 'Email',
        //   required: true,
        //   validators: [Validators.email],
        //   columns: 'col-md-6',
        //   criteriaValue: {
        //     key: 'owner_type',
        //     value: ['Government', 'Company']
        //   }
        // })
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private fileUploadService: FileUploadService,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private localService: LocalService,
        private lotService: LocationService
    ) {
        super(formCtlService);
        this.loading = true;
    }

    ngOnInit(): void {
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

    }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.index = +paramMap.get('index');
            this.service = this.mm && this.mm == 'mm' ? this.micMMService : this.micService;

            this.micForm[2].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};

                        this.micModel.owner = this.micModel.owner || form?.data?.owner || [];
                        this.micModel.owner[this.index] = this.micModel.owner[this.index] || form?.data?.owner && form?.data?.owner[this.index] || {};

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel?.owner[this.index] || {}, form?.data?.owner && form?.data?.owner[this.index] || {});

                        this.page = "mic/sectionD-form3/";
                        this.page += this.mm && this.mm == 'mm' ? (this.micModel.id + '/' + this.index + '/mm') : (this.micModel.id + '/' + this.index);

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }


    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var owner_type = this.micForm.filter(x => x.key == "owner_type")[0];
        owner_type.options$ = this.localService.getType(lan);

        this.authService.changeLanguage$.subscribe(x => {
            owner_type.options$ = this.localService.getType(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    checkDates(group: FormGroup) {
        let permit_from = group.get('permit_from').value;
        let permit_to = group.get('permit_to').value;
        let permit_control = group.get('permit_to');
        if (permit_control.errors && !permit_control.errors.date) {
            return;
        }
        else {
            if (Date.parse(permit_from) > Date.parse(permit_to))
                permit_control.setErrors({ date: true });
            else
                permit_control.setErrors(null);
        }
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let owner = this.micModel.owner ? this.micModel.owner.map(x => Object.assign({}, x)) || [] : [];
            owner[this.index] = formValue;

            let data = { 'owner': owner };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionD-form4', id, this.index, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/mic/sectionD-form4', id, this.index]);
                } else {
                    this.router.navigate([page + '/mic/sectionD-form3', id, this.index, 'mm']);
                }
            }
        }
    }
}
