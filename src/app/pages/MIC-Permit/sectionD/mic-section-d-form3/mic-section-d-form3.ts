import { BaseFormArray } from './../../../../custom-component/dynamic-forms/base/form-array';
import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

export class MicSectionDForm3 {
    
    public forms: BaseForm<string>[] = []
    index: any = 0;

    constructor(
        private lotService: LocationService,
        private localService: LocalService,
    ) {
        let individual: BaseForm<string>[] = [
            new FormInput({
                key: 'full_name',
                label: 'iii. Full name',
                required: true,
            }),
            new FormInput({
                key: 'passport_or_nrc',
                label: 'iv. National Registration Card No.',
                required: true,
            }),
            new FormTitle({
                label: 'v. Address'
            }),
            new FormSelect({
                key: 'individual_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
            }),
            new FormSelect({
                key: 'individual_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_state',
                    key: 'state'
                },
            }),
            new FormSelect({
                key: 'individual_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: this.lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'individual_district',
                    key: 'district'
                },
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormInput({
                key: 'phone_no',
                label: 'vi. Phone number',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'vii. E-mail address', 
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let company: BaseForm<string>[] = [
            new FormInput({
                key: 'company_name',
                label: 'iii. Name of company',
                required: true,
            }),
            new FormInput({
                key: 'company_reg_no',
                label: 'iv. Company Registration No.',
                required: true,
            }),
            new FormTitle({
                label: 'v. Address'
            }),
            new FormSelect({
                key: 'company_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
            }),
            new FormSelect({
                key: 'company_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_state',
                    key: 'state'
                },
            }),
            new FormSelect({
                key: 'company_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: this.lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'company_district',
                    key: 'district'
                },
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormTitle({
                label: 'vi. Contact person',
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                type: 'number',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let government: BaseForm<string>[] = [
            new FormInput({
                key: 'name',
                label: 'iii. Name of government department/organization',
                required: true
            }),
            new FormTitle({
                label: 'iv. Address'
            }),
            new FormSelect({
                key: 'government_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
            }),
            new FormSelect({
                key: 'government_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'government_state',
                    key: 'state'
                },
            }),
            new FormSelect({
                key: 'government_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: this.lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'government_district',
                    key: 'district'
                },
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormTitle({
                label: 'v. Contact Person',
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new FormSectionTitle({
                        label: '2. Location of investment and lease agreements'
                    }),
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location ' + (this.index + 1),
                        style: {'font-size': '18px'}
                    }),
                    new FormSectionTitle({
                        label: 'a.Owner of land and/or building(s)'
                    }),
                    new FormTitle({
                        label: 'Please upload evidence of land ownership/ land use rights (unless location is in an Industrial Zone):'
                    }),
                    new FormFile({
                        key: 'ownership_document',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'If the current owner has not changed the name of the former owner on the ownership evidence, please upload contract/agreement (e.g. special power, general power, etc.) between former owner and current owner:'
                    }),
                    new FormFile({
                        key: 'agreement_document',
                        label: 'File Name'
                    }),
                    new FormTitle({
                        label: 'i. Period of land grant validity / initial period permitted to use the land'
                    }),
                    new FormDate({
                        key: 'permit_from',
                        label: 'From Date',
                        columns: 'col-md-6',
                        required: true
                    }),
                    new FormDate({
                        key: 'permit_to',
                        label: 'To Date',
                        columns: 'col-md-6',
                        required: true
                    }),
                    new FormSelect({
                        key: 'owner_type',
                        label: 'ii. Type of owner',
                        options$: this.localService.getType(),
                        required: true
                    }),
                    new BaseFormGroup({
                        key: 'individual',
                        formGroup: individual,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Individual'
                        }
                    }),
                    new BaseFormGroup({
                        key: 'company',
                        formGroup: company,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Company'
                        }
                    }),
                    new BaseFormGroup({
                        key: 'government',
                        formGroup: government,
                        criteriaValue: {
                            key: 'owner_type',
                            value: 'Government department/organization'
                        }
                    })
                ]
            })
        ];
    }
}