import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionDForm1Component } from './mic-section-d-form1.component';

describe('MicSectionDForm1Component', () => {
  let component: MicSectionDForm1Component;
  let fixture: ComponentFixture<MicSectionDForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionDForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionDForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
