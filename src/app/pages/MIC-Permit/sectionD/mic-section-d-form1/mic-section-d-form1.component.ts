import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../../../../core/reducers/index';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermitSectionD } from '../mic-permit-section-d';
import { MICFormService } from 'src/services/micform.service';
import { Mic } from 'src/app/models/mic';
import { FormProgressService } from 'src/services/form-progress.service';
import { forkJoin } from 'rxjs';
@Component({
    selector: 'app-mic-section-d-form1',
    templateUrl: './mic-section-d-form1.component.html',
    styleUrls: ['./mic-section-d-form1.component.scss']
})
export class MicSectionDForm1Component extends MICPermitSectionD implements OnInit {
    menu: any = "sectionD";
    q2_correct: any = false;
    @Input() id: any;
    mm: any;

    // model: any = {};
    micModel: Mic;
    micMMModel: Mic;
    form: any; //For New Flow

    progressLists: any[] = [];
    pages: any[] = [];

    loading = false;

    constructor(
        private formService: MICFormService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private translateService: TranslateService,
        private store: Store<AppState>,
        private router: Router,

    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            if (this.id > 0) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {

                        this.form = form || {};
                        this.micModel = this.form?.data || {};
                        this.micMMModel = this.form?.data_mm || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));
                        this.getFormProgress();
                        this.changeLanguage();
                        this.loading = true;
                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    ngOnInit(): void { }

    getFormProgress() {
        this.pages = [
            `mic/sectionD-form2`,
            `mic/sectionD-form3`,
            `mic/sectionD-form4`,
            `mic/sectionD-form5`,
            `mic/sectionD-form6`,
            `mic/sectionD-form7`,
            `mic/sectionD-form8`
        ];
        forkJoin([
            this.formProgressService.get({
                rows: 999,
                search: `type_id:equal:${this.micModel?.id || 0}|type:equal:MIC-Permit`,
                secure: true
            }),
            this.formProgressService.get({
                rows: 999,
                search: `type_id:equal:${this.micMMModel?.id || 0}|type:equal:MIC-Permit`,
                secure: true
            })
        ]).subscribe((x: any[]) => {
            this.progressLists = [...x[0], ...x[1]];
            this.loading = false;
        });
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.micMMModel.id}/mm` : `${page}/${this.micMMModel.id}/0/mm`)).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.micModel.id}` : `${page}/${this.micModel.id}/0`)).length;
            const lengthMM = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.micMMModel.id}/mm` : `${page}/${this.micMMModel.id}/0/mm`)).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    openPage(link, has_index) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        if (has_index) {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/mic/' + link, this.id, 0, 'mm']);
            } else {
                this.router.navigate([page + '/mic/' + link, this.id, 0]);
            }
        }
        else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/mic/' + link, this.id, 'mm']);
            } else {
                this.router.navigate([page + '/mic/' + link, this.id]);
            }
        }
    }

}
