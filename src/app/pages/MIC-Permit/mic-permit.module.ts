import { ErrorTailorModule } from '@ngneat/error-tailor';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomComponentModule } from './../../custom-component/custom-component.module';
import { MicPermitRoutingModule } from './mic-permit-routing.module';
import { GlobalModule } from './../global.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MicSuccessComponent } from './mic-success/mic-success.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MicSectionAForm2Component } from './sectionA/mic-section-a-form2/mic-section-a-form2.component';
import { MicSectionAForm3Component } from './sectionA/mic-section-a-form3/mic-section-a-form3.component';
import { MicSectionAForm4Component } from './sectionA/mic-section-a-form4/mic-section-a-form4.component';
import { MicSectionAForm1Component } from './sectionA/mic-section-a-form1/mic-section-a-form1.component';
import { MicSectionBForm1Component } from './sectionB/mic-section-b-form1/mic-section-b-form1.component';
import { MicSectionBForm2Component } from './sectionB/mic-section-b-form2/mic-section-b-form2.component';
import { MicSectionBForm3Component } from './sectionB/mic-section-b-form3/mic-section-b-form3.component';
import { MicSectionBForm4Component } from './sectionB/mic-section-b-form4/mic-section-b-form4.component';
import { MicSectionCForm1Component } from './sectionC/mic-section-c-form1/mic-section-c-form1.component';
import { MicSectionCForm2Component } from './sectionC/mic-section-c-form2/mic-section-c-form2.component';
import { MicSectionCForm3Component } from './sectionC/mic-section-c-form3/mic-section-c-form3.component';
import { MicSectionCForm4Component } from './sectionC/mic-section-c-form4/mic-section-c-form4.component';
import { MicSectionCForm5Component } from './sectionC/mic-section-c-form5/mic-section-c-form5.component';
import { MicSectionCForm6Component } from './sectionC/mic-section-c-form6/mic-section-c-form6.component';
import { MicSectionCForm7Component } from './sectionC/mic-section-c-form7/mic-section-c-form7.component';
import { MicSectionCForm8Component } from './sectionC/mic-section-c-form8/mic-section-c-form8.component';
import { MicSectionDForm1Component } from './sectionD/mic-section-d-form1/mic-section-d-form1.component';
import { MicSectionDForm2Component } from './sectionD/mic-section-d-form2/mic-section-d-form2.component';
import { MicSectionDForm3Component } from './sectionD/mic-section-d-form3/mic-section-d-form3.component';
import { MicSectionDForm4Component } from './sectionD/mic-section-d-form4/mic-section-d-form4.component';
import { MicSectionDForm5Component } from './sectionD/mic-section-d-form5/mic-section-d-form5.component';
import { MicSectionDForm6Component } from './sectionD/mic-section-d-form6/mic-section-d-form6.component';
import { MicSectionDForm7Component } from './sectionD/mic-section-d-form7/mic-section-d-form7.component';
import { MicSectionDForm8Component } from './sectionD/mic-section-d-form8/mic-section-d-form8.component';
import { MicSectionEForm1Component } from './sectionE/mic-section-e-form1/mic-section-e-form1.component';
import { MicSectionEForm2Component } from './sectionE/mic-section-e-form2/mic-section-e-form2.component';
import { MicSectionFForm1Component } from './sectionF/mic-section-f-form1/mic-section-f-form1.component';
import { MicSectionFForm2Component } from './sectionF/mic-section-f-form2/mic-section-f-form2.component';
import { MicSectionEForm5Component } from './sectionE/mic-section-e-form5/mic-section-e-form5.component';
import { MicSectionEForm3Component } from './sectionE/mic-section-e-form3/mic-section-e-form3.component';
import { MicSectionEForm4Component } from './sectionE/mic-section-e-form4/mic-section-e-form4.component';
import { MicUndertakingComponent } from './mic-undertaking/mic-undertaking.component';
import { PermitCertificateComponent } from './permit-certificate/permit-certificate.component';
import { MicChooseLanguageComponent } from './mic-choose-language/mic-choose-language.component';
import { MicSidemenuComponent } from './mic-sidemenu/mic-sidemenu.component';
import { MicSidemenuMiniComponent } from './mic-sidemenu-mini/mic-sidemenu-mini.component';
import { MicPaymentComponent } from './mic-payment/mic-payment.component';
import { MicSectionBForm5Component } from './sectionB/mic-section-b-form5/mic-section-b-form5.component';

@NgModule({
  declarations: [
        MicSectionAForm2Component,
        MicSectionAForm3Component,
        MicSectionAForm4Component,
        MicSectionAForm1Component,
        MicSectionBForm1Component,
        MicSectionBForm2Component,
        MicSectionBForm3Component,
        MicSectionBForm4Component,
        MicSectionBForm5Component,
        MicSectionCForm1Component,
        MicSectionCForm2Component,
        MicSectionCForm3Component,
        MicSectionCForm4Component,
        MicSectionCForm5Component,
        MicSectionCForm6Component,
        MicSectionCForm7Component,
        MicSectionCForm8Component,
        MicSectionDForm1Component,
        MicSectionDForm2Component,
        MicSectionDForm3Component,
        MicSectionDForm4Component,
        MicSectionDForm5Component,
        MicSectionDForm6Component,
        MicSectionDForm7Component,
        MicSectionDForm8Component,
        MicSectionEForm1Component,
        MicSectionEForm2Component,
        MicSectionFForm1Component,
        MicSectionFForm2Component,
        MicSuccessComponent,
        MicSectionEForm3Component,
        MicSectionEForm4Component,
        MicUndertakingComponent,
        MicSectionEForm5Component,
        PermitCertificateComponent,
        MicChooseLanguageComponent,
        MicSidemenuComponent,
        MicSidemenuMiniComponent,
        MicPaymentComponent,
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        GlobalModule,
        MicPermitRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // closeButton: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ],
    exports: [
        MicSectionAForm2Component,
        MicSectionAForm3Component,
        MicSectionAForm4Component,
        MicSectionAForm1Component,
        MicSectionBForm1Component,
        MicSectionBForm2Component,
        MicSectionBForm3Component,
        MicSectionBForm4Component,
        MicSectionBForm5Component,
        MicSectionCForm1Component,
        MicSectionCForm2Component,
        MicSectionCForm3Component,
        MicSectionCForm4Component,
        MicSectionCForm5Component,
        MicSectionCForm6Component,
        MicSectionCForm7Component,
        MicSectionCForm8Component,
        MicSectionDForm1Component,
        MicSectionDForm2Component,
        MicSectionDForm3Component,
        MicSectionDForm4Component,
        MicSectionDForm5Component,
        MicSectionDForm6Component,
        MicSectionDForm7Component,
        MicSectionDForm8Component,
        MicSectionEForm1Component,
        MicSectionEForm2Component,
        MicSectionFForm1Component,
        MicSectionFForm2Component,
        MicSuccessComponent,
        MicSectionEForm3Component,
        MicSectionEForm4Component,
        MicUndertakingComponent,
        MicSectionEForm5Component,
        PermitCertificateComponent,
        MicChooseLanguageComponent,
        MicSidemenuComponent,
        MicSidemenuMiniComponent,
        MicPaymentComponent
    ], providers: [
        TranslateService,
    ],
})
export class MicPermitModule { }
