import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSidemenuComponent } from './mic-sidemenu.component';

describe('MicSidemenuComponent', () => {
  let component: MicSidemenuComponent;
  let fixture: ComponentFixture<MicSidemenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSidemenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSidemenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
