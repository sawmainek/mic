import { FormProgress } from './../../../core/form/_actions/form.actions';
import { formProgress, currentForm } from './../../../core/form/_selectors/form.selectors';
import { Store, select } from '@ngrx/store';
import { MICFormService } from './../../../../services/micform.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { FormProgressService } from 'src/services/form-progress.service';
import { MICChooseLanguage } from '../mic-choose-language/mic-choose-language';
import { MICPermitUndertaking } from '../mic-undertaking/mic-permit-undertaking';
import { MICPermitCertificate } from '../permit-certificate/mic-permit-certificate';
import { MICPermitSectionA } from '../sectionA/mic-permit-section-a';
import { MICPermitSectionB } from '../sectionB/mic-permit-section-b';
import { MICPermitSectionC } from '../sectionC/mic-permit-section-c';
import { MICPermitSectionD } from '../sectionD/mic-permit-section-d';
import { MICPermitSectionE } from '../sectionE/mic-permit-section-e';
import { MICPermitSectionF } from '../sectionF/mic-permit-section-f';
import { Mic } from 'src/app/models/mic';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { AppState } from 'src/app/core';
import { ReadyToSubmit } from 'src/app/core/form/_actions/form.actions';

const isAllTrue = (currentValue) => currentValue == true;

@Component({
    selector: 'app-mic-sidemenu',
    templateUrl: './mic-sidemenu.component.html',
    styleUrls: ['./mic-sidemenu.component.scss']
})
export class MicSidemenuComponent implements OnInit {
    @Input() menu: any = "choose-language";
    @Input() progress: any = false;
    @Input() form: any;
    @Output() progressForms = new EventEmitter<any>();

    pagesA: any;
    pagesB: any;
    pagesC: any;
    pagesD: any;
    pagesE: any;
    pagesF: any;

    user: any;
    is_officer: boolean = false;

    model: any = {};
    micModel: Mic;
    micMMModel: Mic;

    formProgress: any;
    completeSections = [];
    progressLists: any[] = [];


    // For New Flow
    id: any = 0;
    mm: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private micFormService: MICFormService,
        private formProgressService: FormProgressService,
        private store: Store<AppState>,
        public formCtlService: FormControlService,
    ) {
        
    }

    ngOnInit(): void { 
        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
     
            if (this.id > 0) {
                // Get data from state
                this.store.pipe(select(currentForm))
                    .subscribe(data => {
                        if (data.form) {
                            this.form = Object.assign({}, data.form);
                            this.micModel = this.form?.data || {};
                            this.micMMModel = this.form?.data_mm || {};
                            this.model = this.form?.language == 'Myanmar' ? this.form?.data_mm || {} : this.form?.data || {};
                            this.createProgressList();
                        } else {
                            // Get data from API
                            this.micFormService.show(this.id)
                                .subscribe((form: any) => {
                                    this.form = form || {};
                                    this.micModel = this.form?.data || {};
                                    this.micMMModel = this.form?.data_mm || {};
                                    this.model = this.form?.language == 'Myanmar' ? this.form?.data_mm || {} : this.form?.data || {};
                                    this.createProgressList();
                                });
                        }
                    }).unsubscribe();
            }
        }).unsubscribe();
    }

    createProgressList() {
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                    this.getFormProgress();
                    this.editTotalForms();
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:MIC-Permit`
                    }).subscribe(results => {
                        this.progressLists = results;
                        this.getFormProgress();
                        this.editTotalForms();
                        this.store.dispatch(new FormProgress({progress: this.progressLists}));
                    });
                }
            }).unsubscribe();
    }

    getFormProgress() {
        if (this.progressLists.length > 0) {
            this.formProgress = this.progressLists;
            this.progressForms.emit(this.formProgress);

            let sections = ["Choose Language", "Section A", "Section B", "Section C", "Section D", "Section E", "Section F", "Certificate", "Undertaking"];
            this.completeSections = sections.map(x => this.isSectionComplete(x));

            //Enable submission button
            if (this.completeSections[0] && this.completeSections[1] && this.completeSections[2] && this.completeSections[3] && this.completeSections[4] && this.completeSections[5] && this.completeSections[6]) {
                setTimeout(() => {
                    this.store.dispatch(new ReadyToSubmit({ submission: 'MIC Permit' }));
                }, 600);
            }

        }
        else {
            this.progressForms.emit([]);
        }
    }

    checkRevise(section) {
        const revise = this.progressLists.filter(x => x.section == section && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    editTotalForms() {
        // To Get Section E Forms Total
        var totalE = 1;
        var sector: string = this.model?.company_info?.general_business_sector?.general_sector_id || null;
        if (sector) {
            if (!sector.startsWith('Investment Division 1')) {
                totalE = 2;
            }
            else {
                totalE = 1;
            }
        }

        if (this.form?.language == 'Myanmar') {
            MICPermitSectionA.totalForm = new MICPermitSectionA(this.formCtlService).totalForm;
            MICPermitSectionB.totalForm = new MICPermitSectionB(this.formCtlService).totalForm;
            MICPermitSectionC.totalForm = new MICPermitSectionC(this.formCtlService).totalForm;
            MICPermitSectionD.totalForm = new MICPermitSectionD(this.formCtlService).totalForm;
            MICPermitSectionE.totalForm = totalE;
            MICPermitSectionF.totalForm = new MICPermitSectionF(this.formCtlService).totalForm;
            MICPermitCertificate.totalForm = 1;
            MICPermitUndertaking.totalForm = 1;
        }
        else {
            MICPermitSectionA.totalForm = new MICPermitSectionA(this.formCtlService).totalForm * 2;
            MICPermitSectionB.totalForm = new MICPermitSectionB(this.formCtlService).totalForm * 2;
            MICPermitSectionC.totalForm = new MICPermitSectionC(this.formCtlService).totalForm * 2;
            MICPermitSectionD.totalForm = new MICPermitSectionD(this.formCtlService).totalForm * 2;
            MICPermitSectionE.totalForm = totalE * 2;
            MICPermitSectionF.totalForm = new MICPermitSectionF(this.formCtlService).totalForm * 2;
            MICPermitCertificate.totalForm = 2;
            MICPermitCertificate.totalForm = 0;
        }
    }

    checkPageComplete(page) {
        const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel?.id || 0}` || x.page == `${page}/${this.micMMModel?.id || 0}/mm`).length;
        return length > 0 ? 1 : 0;
    }

    checkCompleteFormA() {
        this.pagesA = [
            `mic/sectionA-form4`,
        ];
        var totalLength = 0;
        this.pagesA.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormB() {
        this.pagesB = [
            `mic/sectionB-form2`,
            `mic/sectionB-form3`,
            `mic/sectionB-form4`,            
            `mic/sectionB-form5`
        ];

        var totalLength = 0;
        this.pagesB.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormC() {
        this.pagesC = [
            `mic/sectionC-form2`,
            `mic/sectionC-form3`,
            `mic/sectionC-form4`,
            `mic/sectionC-form5`,
            `mic/sectionC-form6`,
            `mic/sectionC-form7`,
            `mic/sectionC-form8`
        ];

        var totalLength = 0;
        this.pagesC.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormD() {
        this.pagesD = [
            `mic/sectionD-form2`,
            `mic/sectionD-form3`,
            `mic/sectionD-form4`,
            `mic/sectionD-form5`,
            `mic/sectionD-form6`,
            `mic/sectionD-form7`,
            `mic/sectionD-form8`
        ];

        var totalLength = 0;
        this.pagesD.forEach((page, index) => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.micMMModel.id}/mm` : `${page}/${this.micMMModel.id}/0/mm`)).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.micModel.id}` : `${page}/${this.micModel.id}/0`)).length;
                const lengthMM = this.progressLists.filter(x => x.page == (index == 0 ? `${page}/${this.micMMModel.id}/mm` : `${page}/${this.micMMModel.id}/0/mm`)).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormE() {
        this.pagesE = [
            `mic/sectionE-form2`
        ];

        // Don't remove general sector id
        const sector = this.micMMModel?.company_info?.general_business_sector?.general_sector_id || '';
        if (sector.startsWith('Investment Division 2')) {
            this.pagesE.push('mic/sectionE-form3');
        }
        if (sector.startsWith('Investment Division 3')) {
            this.pagesE.push('mic/sectionE-form4');
        }
        if (sector.startsWith('Investment Division 4')) {
            this.pagesE.push('mic/sectionE-form5');
        }
        
        var totalLength = 0;
        this.pagesE.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteFormF() {
        this.pagesF = [
            `mic/sectionF-form2`
        ];

        var totalLength = 0;
        this.pagesF.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteUndertaking() {
        const pages = [
            `mic/undertaking`
        ];

        var totalLength = 0;
        pages.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    isSectionComplete(menu) {
        let hasComplete = false;

        switch (menu) {
            case MICChooseLanguage.section: {
                if (MICChooseLanguage.totalForm <= this.checkPageComplete('mic/choose-language')) {
                    hasComplete = true;
                }
                break;
            }
            case MICPermitSectionA.section: {
                if (MICPermitSectionA.totalForm <= this.checkCompleteFormA()) {
                    hasComplete = true;
                }
                break;
            }
            case MICPermitSectionB.section: {
                if (MICPermitSectionB.totalForm <= this.checkCompleteFormB()) {
                    hasComplete = true;
                }
                break;
            }
            case MICPermitSectionC.section: {
                if (MICPermitSectionC.totalForm <= this.checkCompleteFormC()) {
                    hasComplete = true;
                }
                break;
            }
            case MICPermitSectionD.section: {
                if (MICPermitSectionD.totalForm <= this.checkCompleteFormD()) {
                    hasComplete = true;
                }
                break;
            }
            case MICPermitSectionE.section: {
                if (MICPermitSectionE.totalForm <= this.checkCompleteFormE()) {
                    hasComplete = true;
                }
                break;
            }
            case MICPermitSectionF.section: {
                if (MICPermitSectionF.totalForm <= this.checkCompleteFormF()) {
                    hasComplete = true;
                }
                break;
            }
            case MICPermitCertificate.section: {
                if (this.form?.language == 'Myanmar') {
                    if (MICPermitCertificate.totalForm <= this.checkPageComplete('mic/certificate')) {
                        hasComplete = true;
                    }
                }
                else {
                    hasComplete = true;
                }

                break;
            }
            case MICPermitUndertaking.section: {
                if (MICPermitUndertaking.totalForm <= this.checkCompleteUndertaking()) {
                    hasComplete = true;
                }
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
        return hasComplete;
    }

    openSection(index, link) {
        if (this.allowRevision) {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/' + link, this.form?.id || 0, 'mm']);
            } else {
                this.router.navigate([page + '/mic/' + link, this.form?.id || 0]);
            }
        }
    }

    get allowRevision(): boolean {
        if (window.location.href.indexOf('officer') == -1 && this.form?.submission) {
            return false;
        }
        return true;
    }
}
