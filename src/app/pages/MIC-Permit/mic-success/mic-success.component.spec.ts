import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSuccessComponent } from './mic-success.component';

describe('MicSuccessComponent', () => {
  let component: MicSuccessComponent;
  let fixture: ComponentFixture<MicSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
