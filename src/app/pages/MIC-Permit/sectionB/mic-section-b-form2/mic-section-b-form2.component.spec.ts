import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionBForm2Component } from './mic-section-b-form2.component';

describe('MicSectionBForm2Component', () => {
  let component: MicSectionBForm2Component;
  let fixture: ComponentFixture<MicSectionBForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionBForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionBForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
