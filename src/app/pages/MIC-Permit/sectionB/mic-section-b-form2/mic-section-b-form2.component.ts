import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { ToastrService } from 'ngx-toastr';
import { FormControlService } from './../../../../custom-component/dynamic-forms/form-control.service';
import { FormNote } from '../../../../custom-component/dynamic-forms/base/form-note';
import { FormTextArea } from './../../../../custom-component/dynamic-forms/base/form-textarea';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { FormFile } from './../../../../custom-component/dynamic-forms/base/form.file';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from './../../../../custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from './../../../../custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { MICPermitSectionB } from '../mic-permit-section-b';
import { BaseFormData, currentForm } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICMMService } from 'src/services/micmm.service';
import { MICFormService } from 'src/services/micform.service';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from "lodash";

@Component({
    selector: 'app-mic-section-b-form2',
    templateUrl: './mic-section-b-form2.component.html',
    styleUrls: ['./mic-section-b-form2.component.scss']
})
export class MicSectionBForm2Component extends MICPermitSectionB implements OnInit {
    @Input() id: any;
    page = "mic/sectionB-form2/";
    mm: any;

    formGroup: FormGroup;
    micModel: any;
    user: any = {};
    model: any = {};
    form: any;
    service: any;

    loading = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    forms: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'invest_form',
            formGroup: [
                new FormSectionTitle({
                    label: '1. Form of Investment'
                }),
                new FormTitle({
                    label: 'a. Please select the investment form of the enterprise, which has been established or which will be established:'
                }),
                new FormRadio({
                    key: 'form_investment',
                    label: 'Wholly Myanmar owned investment',
                    tooltip: 'An enterprise owned wholly by Myanmar.',
                    value: 'Wholly Myanmar owned investment',
                    required: true,
                    style: { 'margin-bottom': '10px' }
                }),
                new FormRadio({
                    key: 'form_investment',
                    label: 'Wholly Foreign owned investment',
                    tooltip: 'An enterprise owned wholly by foreign.',
                    value: 'Wholly Foreign owned investment',
                    required: true,
                    style: { 'margin-bottom': '10px' }
                }),
                new FormRadio({
                    key: 'form_investment',
                    label: 'Myanmar citizen investment (up to 35% foreign shareholding)',
                    tooltip: 'An enterprise by Myanmar citizen with up to 35% foreign shareholding.',
                    value: 'Myanmar citizen investment',
                    required: true,
                    style: { 'margin-bottom': '10px' }
                }),
                new FormInput({
                    key: 'myan_direct_share_percent',
                    label: 'Please specify the percentage of direct shareholding or interest proportion by Foreign citizens',
                    type: 'number',
                    required: true,
                    validators: [Validators.min(0), Validators.max(35)],
                    criteriaValue: {
                        key: 'form_investment',
                        value: 'Myanmar citizen investment'
                    }
                }),
                new FormRadio({
                    key: 'form_investment',
                    label: 'Joint Venture (more than 35% foreign shareholding)',
                    tooltip: 'An enterprise by Myanmar citizen and foreign with more than 35% foreign shareholding.',
                    value: 'Joint Venture',
                    required: true
                }),
                new FormInput({
                    key: 'joint_direct_share_percent',
                    label: 'Please specify the percentage of direct shareholding or interest proportion by Foreign citizens',
                    type: 'number',
                    required: true,
                    validators: [Validators.min(35), Validators.max(100)],
                    criteriaValue: {
                        key: 'form_investment',
                        value: 'Joint Venture'
                    }
                }),
                new FormTitle({
                    label: 'Please upload the (draft) of Joint Venture agreement:',
                    criteriaValue: {
                        key: 'form_investment',
                        value: 'Joint Venture'
                    }
                }),
                new FormFile({
                    key: 'joint_document',
                    label: 'File Name',
                    required: true,
                    criteriaValue: {
                        key: 'form_investment',
                        value: 'Joint Venture'
                    }
                }),
                new FormTextArea({
                    key: 'special_type',
                    label: 'b. Please specify if the investment includes a special type of contractual agreement (e.g. Build Operate Transfer (BOT), Profit Sharing Contract, Production Sharing Contract, etc.):',
                }),
                new FormTitle({
                    label: 'Please upload the draft contract / agreement:'
                }),
                new FormFile({
                    key: 'contract_document',
                    label: 'File Name',
                    required: true
                }),
                new FormTextArea({
                    key: 'other_type',
                    label: 'mic-c. Please specify if the investment involves any other type of approval/agreement or partnership/contract with the government:',
                }),
                new FormTitle({
                    label: 'If the proposed investment should be submitted from/through the respective government departments, please upload a recommendation letter of the respective ministry here:'
                }),
                new FormFile({
                    key: 'ministry_document',
                    label: 'File Name',
                    required: true
                }),
                new FormNote({
                    label: 'The Myanmar Investment Rule 44 described the situations which are required to submit the approval from/through the relevant government department/organization.'
                })
            ]
        })
    ]

    constructor(
        private router: Router,
        private micService: MicService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private formService: MICFormService,
        private micMMService: MICMMService,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.forms);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {

            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((result: any) => {
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(result) }));
                        this.createFormData(result);
                    });
            }
        }).unsubscribe();
    }

    createFormData(form: any) {
        this.form = form || {};
        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};

        this.micModel.invest_form = this.micModel?.invest_form || {};
        this.formGroup = this.formCtlService.toFormGroup(this.forms, this.micModel || {}, form?.data || {});

        this.page = "mic/sectionB-form2/";
        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
        });

        this.changeLanguage();
        this.loading = false;
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.is_save = false;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.is_save = true;
        this.saveFormData();
    }

    saveFormData() {
        this.formCtlService.lazyUpload(this.forms, this.formGroup, { type_id: this.id }, (formValue) => {
            this.micModel = { ...this.micModel, ...formValue };
            this.service.create(this.micModel, { secure: true })
                .subscribe(x => {
                    if (this.mm == 'mm') {
                        this.form.data_mm = x;
                    } else {
                        this.form.data = x;
                    }
                    this.store.dispatch(new CurrentForm({ form: this.form }));

                    this.spinner = false;
                    this.is_save = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        })
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionB-form3', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionB-form3', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionB-form2', id, 'mm']);
                }
            }
        }
    }
}
