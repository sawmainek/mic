import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionBForm5Component } from './mic-section-b-form5.component';

describe('MicSectionBForm5Component', () => {
  let component: MicSectionBForm5Component;
  let fixture: ComponentFixture<MicSectionBForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionBForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionBForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
