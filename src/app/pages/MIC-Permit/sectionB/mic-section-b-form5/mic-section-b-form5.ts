import { LocationService } from 'src/services/location.service';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';

export class MicSectionBForm5 {

    public forms: BaseForm<string>[] = []

    constructor(
        lotService: LocationService
    ) {
        const signi_person_list: BaseForm < any > [] =[
            new FormInput({
                key: 'name',
                label: 'Name',
                required: true,
                columns: 'col-md-4 d-flex',
            }),
            new FormInput({
                key: 'interest',
                label: 'Explain the interest in the proposed investment',
                required: true,
                columns: 'col-md-4 d-flex',
            }),
            new FormSelect({
                key: 'country',
                label: 'Citizenship',
                tooltip: 'Citizenship or country of incorporation / registration / origin',
                options$: lotService.getCountry(),
                required: true,
                columns: 'col-md-4 d-flex',
            })
        ]

        const sub_company_list: BaseForm < any > [] =[
            new FormInput({
                key: 'name',
                label: 'Name of Company',
            }),
        ]

        this.forms =[
            new BaseFormGroup({
                key: 'significant_list',
                formGroup: [
                    new FormSectionTitle({                    
                        label: '4. Significant direct or indirect interest in the proposed investment.',
                        pageOrientation: 'portrait',
                        pageBreak: 'before'   
                    }),
                    new FormTitle({
                        label: '(a) Please describe all persons – other than the shareholders listed above – with a significant direct or indirect interest in the proposed investment:'
                    }),
                    new FormNote({
                        label: "The list should include natural or legal persons that: Hold indirectly (e.g. through subsidiaries) at least 25% of shares of the investment. Hold at least 25% of voting power over the investment. Hold power of appointment and dismissal over board of directors / management of investment. Are entitled to possess more than 10% of the profit distribution"
                    }),
                    new BaseFormArray({
                        key: 'signi_person_list',
                        formArray: signi_person_list,
                        defaultLength: 1,
                    }),
                    new FormTitle({
                        label: '(b) If there are subsidiaries of the investors directly involved in carrying out the proposed investment, please list the names of those companies:'
                    }),
                    new BaseFormArray({
                        key: 'sub_company_list',
                        formArray: sub_company_list,
                        defaultLength: 1,
                    }),
                ]
            })
        ]
    }
}