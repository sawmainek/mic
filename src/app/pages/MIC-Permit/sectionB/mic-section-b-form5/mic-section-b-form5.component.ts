import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { cloneDeep } from 'lodash';
import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { MICPermitSectionB } from '../mic-permit-section-b';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';

@Component({
    selector: 'app-mic-section-b-form5',
    templateUrl: './mic-section-b-form5.component.html',
    styleUrls: ['./mic-section-b-form5.component.scss']
})
export class MicSectionBForm5Component extends MICPermitSectionB implements OnInit {
    @Input() id: any;
    page = "mic/sectionB-form5/";
    mm: any;

    user: any = {};

    micModel: any = {};
    micFormGroup: FormGroup;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    loading = true;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    signi_person_list: BaseForm<any>[] = [
        new FormInput({
            key: 'name',
            label: 'Name',
            required: true,
            columns: 'col-md-4 d-flex',
        }),
        new FormInput({
            key: 'interest',
            label: 'Explain the interest in the proposed investment',
            required: true,
            columns: 'col-md-4 d-flex',
        }),
        new FormSelect({
            key: 'country',
            label: 'Citizenship',
            tooltip: 'Citizenship or country of incorporation / registration / origin',
            options$: this.lotService.getCountry(),
            required: true,
            columns: 'col-md-4 d-flex',
        })
    ]

    sub_company_list: BaseForm<any>[] = [
        new FormInput({
            key: 'name',
            label: 'Name of Company',
        }),
    ]

    micForm: BaseForm<any>[] = [
        new BaseFormGroup({
            key: 'significant_list',
            formGroup: [
                new FormSectionTitle({
                    label: '4. Significant direct or indirect interest in the proposed investment.'
                }),
                new FormTitle({
                    label: '(a) Please describe all persons – other than the shareholders listed above – with a significant direct or indirect interest in the proposed investment:'
                }),
                new FormNote({
                    label: "The list should include natural or legal persons that: Hold indirectly (e.g. through subsidiaries) at least 25% of shares of the investment. Hold at least 25% of voting power over the investment. Hold power of appointment and dismissal over board of directors / management of investment. Are entitled to possess more than 10% of the profit distribution"
                }),
                new BaseFormArray({
                    key: 'signi_person_list',
                    formArray: this.signi_person_list,
                    defaultLength: 1,
                }),
                new FormTitle({
                    label: '(b) If there are subsidiaries of the investors directly involved in carrying out the proposed investment, please list the names of those companies:'
                }),
                new BaseFormArray({
                    key: 'sub_company_list',
                    formArray: this.sub_company_list,
                    defaultLength: 1,
                }),
            ]
        })
    ]


    constructor(
        private router: Router,
        public location: Location,
        private lotService: LocationService,
        public formCtlService: FormControlService,
        private toast: ToastrService,
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private localService: LocalService,
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;
            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.micModel.significant_list = this.micModel?.significant_list || {};
                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel, form.data || {});

                        this.page = "mic/sectionB-form5/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveMic();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveMic();
    }

    saveMic() {
        this.spinner = true;
        this.service.create({ ...this.micModel, ...this.micFormGroup.value }, { secure: true })
            .subscribe(x => {
                this.spinner = false;

                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                    this.redirectLink(this.form?.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            });

    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionC-form1', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionC-form1', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionB-form5', id, 'mm']);
                }
            }
        }


    }
}
