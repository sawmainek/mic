import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionBForm1Component } from './mic-section-b-form1.component';

describe('MicSectionBForm1Component', () => {
  let component: MicSectionBForm1Component;
  let fixture: ComponentFixture<MicSectionBForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionBForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionBForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
