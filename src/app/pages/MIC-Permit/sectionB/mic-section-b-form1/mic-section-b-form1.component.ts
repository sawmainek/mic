import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { formProgress } from './../../../../core/form/_selectors/form.selectors';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Mic } from 'src/app/models/mic';
import { MicService } from 'src/services/mic.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MICPermitSectionB } from '../mic-permit-section-b';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormData } from 'src/app/core/form';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/core';
import { MICFormService } from 'src/services/micform.service';
import { forkJoin } from 'rxjs';
import { FormProgressService } from 'src/services/form-progress.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-b-form1',
    templateUrl: './mic-section-b-form1.component.html',
    styleUrls: ['./mic-section-b-form1.component.scss']
})
export class MicSectionBForm1Component extends MICPermitSectionB implements OnInit {
    menu: any = "sectionB";
    @Input() id: any;
    mm: any;

    // model: any = {};
    micModel: Mic;
    micMMModel: Mic;
    form: any; //For New Flow

    progressLists: any[] = [];
    pages: any[] = [];

    loading = false;

    constructor(
        private formService: MICFormService,
        public formCtlService: FormControlService,
        private activatedRoute: ActivatedRoute,
        private formProgressService: FormProgressService,
        private router: Router,
        private tostr: ToastrService,
        private store: Store<AppState>,
        private translateService: TranslateService,
    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            if (this.id > 0) {
                // Get data from API
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.store.dispatch(new CurrentForm({ form }));
                        this.createFormData(form);
                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    createFormData(form: any) {
        this.form = form || {};
        this.micModel = this.form?.data || {};
        this.micMMModel = this.form?.data_mm || {};
        this.getFormProgress();

        this.changeLanguage();
        this.loading = false;
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            `mic/sectionB-form2`,
            `mic/sectionB-form3`,
            `mic/sectionB-form4`,
            `mic/sectionB-form5`
        ];
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:MIC-Permit` // For New Flow this.id => this.micModel.id
                    }).subscribe(result => {
                        this.progressLists = result;
                    });
                }
            }).unsubscribe();
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
            const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    openPage(index, link) {
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/officer/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/officer/' + link, this.id]);
            }
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/pages/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/pages/' + link, this.id]);
            }
        }
    }
}
