import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { Validators } from '@angular/forms';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';


export class MicSectionBForm4 {

    public forms: BaseForm<string>[] = []

    constructor(
        lotService: LocationService,
        localService: LocalService
    ) {
        const micShareHolder: BaseForm<string>[] = [
            new FormSectionTitle({
                label: '3. List of shareholders of investment enterprise',
                pageOrientation: 'landscape', 
                pageBreak: 'before'
            }),
            new BaseFormArray({
                key: 'sharholder_list',
                useTable: true,
                tableHeader: [
                    [
                        { label: 'Name of shareholder' },
                        { label: 'Position in investment' },                    
                        { label: 'N.R.C No. / Passport No. / Company Registration No.' },
                        { label: 'Citizenship or country of incorporation / registration / origin' },
                        { label: 'Address' },
                        { label: 'Share percentage' },
                        { label: 'Issued share capital (equivalent in MMK)' },
                        { label: 'Issued share capital (equivalent in USD)' },
                    ],
                ],
                formArray: [
                    new FormInput({
                        key: 'name',
                        required: true,
                        style: {
                            'min-width': '200px'
                        },
                    }),
                    new FormInput({
                        key: 'position',
                        required: true,
                        style: {
                            'min-width': '200px'
                        },
                    }),
                    new FormInput({
                        key: 'nrc',
                        required: true,
                        style: {
                            'min-width': '200px'
                        },
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: lotService.getCountry(),
                        required: true,
                        style: {
                            'min-width': '200px'
                        },
                    }),
                    new FormInput({
                        key: 'address',
                        required: true,
                        style: {
                            'min-width': '200px'
                        },
                    }),
                    new FormInput({
                        key: 'share_percentage',
                        validators: [Validators.min(0), Validators.max(100)],
                        required: true,
                        type: 'number',
                        style: {
                            'min-width': '200px'
                        },
                    }),
                    new FormInput({
                        key: 'share_capital_MMK',
                        required: true,
                        type: 'number',
                        style: {
                            'min-width': '200px'
                        },
                    }),
                    new FormInput({
                        key: 'share_capital_USD',
                        required: true,
                        type: 'number',
                        style: {
                            'min-width': '200px'
                        },
                    }),
                ]
            }),
            new FormInput({
                key: 'myanmar_share_ratio',
                label: 'Myanmar Citizen share ratio:',
                endfix: '%',
                columns: 'col-md-4 d-flex',
                required: true,
                type: 'number',
                value: "0"
            }),
            new FormInput({
                key: 'foreigner_share_ratio',
                label: 'Foreign share ratio:',
                endfix: '%',
                columns: 'col-md-4 d-flex',
                required: true,
                type: 'number',
                value: "0"
            }),
            new FormInput({
                key: 'gover_org_share_ration',
                label: 'Government Department/Organization share ratio:',
                tooltip: 'Government department/organization share ratio',
                columns: 'col-md-4 d-flex',
                endfix: '%',
                required: true,
                type: 'number',
                value: "0"
            }),

        ]

        this.forms = [
            new BaseFormGroup({
                key: 'share_holder',
                formGroup: micShareHolder
            })
        ]
    }
}