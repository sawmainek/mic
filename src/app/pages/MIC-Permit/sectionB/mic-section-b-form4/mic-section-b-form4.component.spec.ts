import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionBForm4Component } from './mic-section-b-form4.component';

describe('MicSectionBForm4Component', () => {
  let component: MicSectionBForm4Component;
  let fixture: ComponentFixture<MicSectionBForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionBForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionBForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
