import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { MICPermitSectionB } from '../mic-permit-section-b';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';

@Component({
    selector: 'app-mic-section-b-form4',
    templateUrl: './mic-section-b-form4.component.html',
    styleUrls: ['./mic-section-b-form4.component.scss']
})
export class MicSectionBForm4Component extends MICPermitSectionB implements OnInit {
    @Input() id: any;
    page = "mic/sectionB-form4/";
    mm: any;

    user: any = {};

    micFormGroup: FormGroup;
    micModel: any = {};
    form: any;
    service: any;

    loading = true;
    spinner = false;
    is_draft = false;

    isRevision: boolean = false;

    micShareHolder: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '3. List of shareholders of investment enterprise'
        }),
        new BaseFormArray({
            key: 'sharholder_list',
            useTable: true,
            tableHeader: [
                [
                    { label: 'No.' },
                    { label: 'Name of shareholder' },
                    { label: 'Position in investment' },
                    { label: 'N.R.C No. / Passport No. / Company Registration No.' },
                    { label: 'Citizenship or country of incorporation / registration / origin' },
                    { label: 'Address' },
                    { label: 'Share percentage' },
                    { label: 'Issued share capital (equivalent in MMK)' },
                    { label: 'Issued share capital (equivalent in USD)' },
                ],
            ],
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: {
                        'min-width': '50px',
                        'vertical-align': 'middle',
                        'padding': '6px 6px'
                    },
                }),
                new FormInput({
                    key: 'name',
                    required: true,
                    style: {
                        'min-width': '200px'
                    },
                }),
                new FormInput({
                    key: 'position',
                    required: true,
                    style: {
                        'min-width': '200px'
                    },
                }),
                new FormInput({
                    key: 'nrc',
                    required: true,
                    style: {
                        'min-width': '200px'
                    },
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                    required: true,
                    style: {
                        'min-width': '200px'
                    },
                }),
                new FormInput({
                    key: 'address',
                    required: true,
                    style: {
                        'min-width': '200px'
                    },
                }),
                new FormInput({
                    key: 'share_percentage',
                    validators: [Validators.min(0), Validators.max(100)],
                    required: true,
                    type: 'number',
                    style: {
                        'min-width': '200px'
                    },
                }),
                new FormInput({
                    key: 'share_capital_MMK',
                    required: true,
                    type: 'number',
                    style: {
                        'min-width': '200px'
                    },
                }),
                new FormInput({
                    key: 'share_capital_USD',
                    required: true,
                    type: 'number',
                    style: {
                        'min-width': '200px'
                    },
                }),
            ]
        }),
        new FormInput({
            key: 'myanmar_share_ratio',
            label: 'Myanmar Citizen share ratio:',
            endfix: '%',
            columns: 'col-md-4 d-flex',
            required: true,
            type: 'number',
            value: "0"
        }),
        new FormInput({
            key: 'foreigner_share_ratio',
            label: 'Foreign share ratio:',
            endfix: '%',
            columns: 'col-md-4 d-flex',
            required: true,
            type: 'number',
            value: "0"
        }),
        new FormInput({
            key: 'gover_org_share_ration',
            label: 'Government Department/Organization share ratio:',
            tooltip: 'Government department/organization share ratio',
            columns: 'col-md-4 d-flex',
            endfix: '%',
            required: true,
            type: 'number',
            value: "0"
        }),

    ]

    micForm: BaseForm<any>[] = [
        new BaseFormGroup({
            key: 'share_holder',
            formGroup: this.micShareHolder
        })
    ]

    constructor(
        private router: Router,
        private lotService: LocationService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        public formCtlService: FormControlService,
        public location: Location,
        private toast: ToastrService,
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private localService: LocalService,
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel, form.data || {});

                        this.page = "mic/sectionB-form4/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveMic();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        } else {
            const shares = (Number(this.micFormGroup.value.share_holder.myanmar_share_ratio) || 0) + (Number(this.micFormGroup.value.share_holder.foreigner_share_ratio) || 0) + (Number(this.micFormGroup.value.share_holder.gover_org_share_ration) || 0);
            if (Number(shares) === 100) {
                this.spinner = true;
                this.is_draft = false;
                this.saveMic();
            } else {
                alert('Your total share ratio must be 100%.');
            }
        }
    }

    saveMic() {
        this.micModel = { ...this.micModel, ...this.micFormGroup.value };
        this.service.create(this.micModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                    this.redirectLink(this.form?.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionB-form5', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionB-form5', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionB-form4', id, 'mm']);
                }
            }
        }
    }

}
