import { LocationService } from 'src/services/location.service';
import { LocalService } from 'src/services/local.service';
import { CPCService } from './../../../../../services/cpc.service';
import { ISICService } from 'src/services/isic.service';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { Validators } from '@angular/forms';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { Subject } from 'rxjs';

export class MicSectionBForm3 {

    public forms: BaseForm<string>[] = [];
    generalSector$: Subject<any> = new Subject<any>();

    constructor(
        lotService: LocationService,
        localService: LocalService,
        isicService: ISICService,
        cpcService: CPCService,
    ) {
        let businessSector: BaseForm < any > [] =[
            new FormDummy({
                key: 'general_sector_id',
            }), // 0
            new FormSelect({
                key: 'isic_section_id',
                label: 'Choose business sections ( ISIC Sections )',
                options$: isicService.getSection(),
                multiple: true,
                required: true,
                filter: {
                    parent: 'general_sector_id',
                    key: 'investment'
                },
            }), // 1
            new FormSelect({
                key: 'isic_division_id',
                label: 'Choose business divisions ( ISIC Divisions )',
                options$: isicService.getDivison(),
                filter: {
                    parent: 'isic_section_id',
                    key: 'section'
                },
                multiple: true,
                required: true,
            }), // 2
            new FormSelect({
                key: 'isic_group_id',
                label: 'Choose business groups ( ISIC Groups )',
                options$: isicService.getGroup(),
                filter: {
                    parent: 'isic_division_id',
                    key: 'division'
                },
                multiple: true,
                required: true,
            }), // 3
            new FormSelect({
                key: 'isic_class_ids',
                label: 'Choose business class(es) ( ISIC Classes )',
                required: true,
                multiple: true,
                options$: isicService.getClasses(),
                filter: {
                    parent: 'isic_group_id',
                    key: 'group'
                },
            }) // 4
        ]

        let businessCPC: BaseForm < any > [] =[
            new FormSelect({
                key: 'cpc_section_id',
                label: 'Choose business section ( CPC Section )',
                options$: cpcService.getSection(),
                multiple: true,
            }), // 0
            new FormSelect({
                key: 'cpc_division_id',
                label: 'Choose business division ( CPC Division )',
                options$: cpcService.getDivison(),
                multiple: true,
                filter: {
                    parent: 'cpc_section_id',
                    key: 'section'
                }
            }), // 1
            new FormSelect({
                key: 'cpc_group_id',
                label: 'Choose business group ( CPC Group )',
                options$: cpcService.getGroup(),
                multiple: true,
                filter: {
                    parent: 'cpc_division_id',
                    key: 'division'
                }
            }), // 2
            new FormSelect({
                key: 'cpc_class_id',
                label: 'Choose business class(es) ( CPC Classes )',
                options$: cpcService.getClasses(),
                multiple: true,
                filter: {
                    parent: 'cpc_group_id',
                    key: 'group'
                }
            }), // 3
            new FormSelect({
                key: 'cpc_sub_class_ids',
                label: 'Choose business subclass(es) ( CPC Subclasses )',
                options$: cpcService.getSubclasses(),
                multiple: true,
                filter: {
                    parent: 'cpc_class_id',
                    key: 'class'
                }
            }) // 4
        ]

        this.forms = [
            new BaseFormGroup({
                key: 'company_info',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Basic information about the investment'
                    }), 
                    new FormNote({
                        label: 'Enterprise means: (i) Any legal entity, including company, trust, partnership, sole proprietorship, business association or similar organizations established or registered to do businesses in accordance with the applicable laws. (ii) A branch office of such legal entity established in accordance with the applicable laws. If the investor has not yet legally established the enterprise, the natural or legal person responsible for establishing such enterprise may submit the proposal as an investor. The establishment as the enterprise under the law is a condition of being issued the permit and shall not alter any of the obligations of the investor under the Myanmar Investment Law. New companies can be registered under the Myanmar Companies Law using the Myanmar Companies Online (MyCO) system (https://www.myco.dica.gov.mm/).'
                    }),
                    new FormTitle({
                        label: 'a. Will or did the investor(s) establish a new legal entity (e.g. company) for the proposed investment?'
                    }),
                    new FormRadio({
                        key: 'overview_is_established',
                        label: 'No, established',
                        value: 'No',
                        required: true,
                        columns: 'col-md-6'
                    }),
                    new FormRadio({
                        key: 'overview_is_established',
                        label: 'Yes, established',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-6'
                    }),                
                    new FormTitle({
                        label: "If yes, please give details on new legal entity.",
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes'],
                        }
                    }),
                    new FormInput({
                        key: 'investment_name',
                        label: 'i. Name',
                        required: true,
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes'],
                        }
                    }), 
                    new FormSelect({
                        key: 'company_type',
                        label: 'ii. Type of company, if legal entity is company',
                        options$: localService.getCompany(),
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes'],
                        }
                    }), 
                    new FormInput({
                        key: 'legal_type',
                        label: 'If not company, type of legal entity and relevant law under which it is / will be registered',
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }),
                    new FormInput({
                        key: 'company_certificate_no',
                        label: 'Registration certificate number, if applicable',
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }), 
                    new FormTitle({
                        label: 'Please upload a copy of the registration certificate, if applicable',
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }), 
                    new FormFile({
                        key: 'company_certificate_doc',
                        label: 'File name',
                        columns: 'col-md-9',
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }), 
                    new FormTitle({
                        label: 'iii. Address ',
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }), 
                    new FormSelect({
                        key: 'country',
                        label: 'Country',
                        options$: lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }),    
                    new FormSelect({
                        key: 'state',
                        label: 'State / Region:',
                        options$: lotService.getState(),
                        required: true,
                        columns: 'col-md-4',
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }), 
                    new FormSelect({
                        key: 'district',
                        label: 'District:',
                        options$: lotService.getDistrict(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }), 
                    new FormSelect({
                        key: 'township',
                        label: 'Township:',
                        columns: 'col-md-4',
                        options$: lotService.getTownship(),
                        required: true,
                        filter: {
                            parent: 'district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }), 
                    new FormInput({
                        key: 'address',
                        label: 'Additional address details',
                        required: true,
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }),  
                    new FormInput({
                        key: 'phone_no',
                        label: 'iv. Phone number',
                        columns: 'col-md-6',
                        type: 'number',
                        required: true,
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }),
                    new FormInput({
                        key: 'email_address',
                        label: 'v. E-mail address',
                        required: true,
                        columns: 'col-md-6',
                        validators: [Validators.email],
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }),               
                    new FormInput({
                        key: 'website',
                        label: 'vi. Website',
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }), 
                    new FormSelect({
                        key: 'general_sector_id',
                        label: 'b. General business sector of the proposed investment',
                        required: true,
                        options$: isicService.getInvestment(),
                        valueChanges$: this.generalSector$,
                    }), 
                    new FormTitle({
                        label: 'c. Specific sector of the proposed investment:'
                    }), 
                    new BaseFormGroup({
                        key: 'general_business_sector',
                        formGroup: businessSector,
                    }), 
                    new FormNote({
                        label: 'The specific sectors are classified according to International Standard Industrial Classification (ISIC) Rev. 4.'
                    }), 
                    new FormInput({
                        key: 'detail_sector',
                        label: 'Please provide further details on the sector of the investment, if necessary',
                    }),
                    new FormTitle({
                        label: 'd. Optional: please name the specific product(s)/service(s) of the investment:'
                    }), 
                    new BaseFormGroup({
                        key: 'business_cpc',
                        formGroup: businessCPC
                    }), 
                    new FormNote({
                        label: 'Products/services are classified according to Central Product Classification (CPC) Version 2.1.'
                    }), 
                    new FormTextArea({
                        key: 'invest_description',
                        label: 'e. Please give a description of the nature of the proposed activities of the investment, including details on the products/services and production and sales strategy of the investment business',
                        required: true,
                    }), 
                    new FormInput({
                        key: 'estimated_domestic',
                        label: 'Estimated domestic market sales / Total sales',
                        required: true,
                        columns: 'col-md-6',
                        type: 'number',
                        endfix: '%',
                        validators: [Validators.min(0), Validators.max(100)],
                    }), 
                    new FormInput({
                        key: 'estimated_export',
                        label: 'Estimated export market sales / Total sales',
                        required: true,
                        columns: 'col-md-6',
                        type: 'number',
                        endfix: '%',
                        validators: [Validators.min(0), Validators.max(100)],
                    }), 
                    new FormTextArea({
                        key: 'invest_benefit',
                        label: 'f. Proposed investment’s supply chain and benefits to the other related businesses',
                        required: true
                    }),
                    new FormTitle({
                        label: 'g. Contact Person'
                    }), 
                    new FormInput({
                        key: 'contact_name',
                        label: 'Full name',
                        columns: 'col-md-6'
                    }), 
                    new FormInput({
                        key: 'contact_position',
                        label: 'Position',
                        columns: 'col-md-6',
                        required: true
                    }), 
                    new FormInput({
                        key: 'contact_phone',
                        label: 'Phone number',
                        columns: 'col-md-6',
                        required: true,
                        type: 'number'
                    }), 
                    new FormInput({
                        key: 'contact_email',
                        label: 'E-mail address',
                        columns: 'col-md-6',
                        required: true,
                        validators: [Validators.email]
                    }), 
                    new FormTitle({
                        label: 'Please upload evidence of the financial conditions of the business (e.g. the most recent bank statement or the most recent audit report) in Myanmar language or English:',
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }), 
                    new BaseFormArray({
                        key: 'evidence_documents',
                        formArray: [
                            new FormFile({
                                key: 'evidence_doc',
                                label: 'Name of document:',
                                required: true,
                                multiple: true
                            })
                        ],
                        criteriaValue: {
                            key: 'overview_is_established',
                            value: ['Yes']
                        }
                    }), 
                ]
            }),
        ]
    }
}