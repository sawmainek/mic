import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionBForm3Component } from './mic-section-b-form3.component';

describe('MicSectionBForm3Component', () => {
  let component: MicSectionBForm3Component;
  let fixture: ComponentFixture<MicSectionBForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionBForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionBForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
