import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { CPCService } from './../../../../../services/cpc.service';
import { ISICService } from './../../../../../services/isic.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { MICPermitSectionB } from '../mic-permit-section-b';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { Observable, of, Subject } from 'rxjs';
import { LocationService } from 'src/services/location.service';
import { LocalService } from 'src/services/local.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';

@Component({
    selector: 'app-mic-section-b-form3',
    templateUrl: './mic-section-b-form3.component.html',
    styleUrls: ['./mic-section-b-form3.component.scss']
})
export class MicSectionBForm3Component extends MICPermitSectionB implements OnInit {
    page = "mic/sectionB-form3/";
    @Input() id: any;
    mm: any;

    user: any = {};

    business_sectors: any;
    business_cpc: any;
    sections: any;

    micModel: any = {};
    micFormGroup: FormGroup;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    generalSector$: Subject<any> = new Subject<any>();

    isRevision: boolean = false;

    // generalBusinessSector: BaseForm<any>[] = [
    //     new FormSelect({
    //         key: 'general_section_id',
    //         label: 'Choose business sections ( ISIC Sections )',
    //         options$: this.isicService.getSection(),
    //         required: true,
    //     }),
    //     new FormSelect({
    //         key: 'general_division_id',
    //         label: 'Choose business divisions ( ISIC Divisions )',
    //         options$: this.isicService.getDivison(),
    //         filter: {
    //             parent: 'general_section_id',
    //             key: 'section'
    //         },
    //         required: true,
    //         multiple: true,
    //     }),
    // ]

    businessSector: BaseForm<any>[] = [
        new FormDummy({
            key: 'general_sector_id',
        }), // 0
        new FormSelect({
            key: 'isic_section_id',
            label: 'Choose business sections ( ISIC Sections )',
            options$: this.isicService.getSection(),
            multiple: true,
            required: true,
            filter: {
                parent: 'general_sector_id',
                key: 'investment'
            },
        }), // 1
        new FormSelect({
            key: 'isic_division_id',
            label: 'Choose business divisions ( ISIC Divisions )',
            options$: this.isicService.getDivison(),
            filter: {
                parent: 'isic_section_id',
                key: 'section'
            },
            multiple: true,
            required: true,
        }), // 2
        new FormSelect({
            key: 'isic_group_id',
            label: 'Choose business groups ( ISIC Groups )',
            options$: this.isicService.getGroup(),
            filter: {
                parent: 'isic_division_id',
                key: 'division'
            },
            multiple: true,
            required: true,
        }), // 3
        new FormSelect({
            key: 'isic_class_ids',
            label: 'Choose business class(es) ( ISIC Classes )',
            required: true,
            multiple: true,
            options$: this.isicService.getClasses(),
            filter: {
                parent: 'isic_group_id',
                key: 'group'
            },
        }) // 4
    ]

    businessCPC: BaseForm<any>[] = [
        new FormSelect({
            key: 'cpc_section_id',
            label: 'Choose business section ( CPC Section )',
            options$: this.cpcService.getSection(),
            multiple: true,
        }), // 0
        new FormSelect({
            key: 'cpc_division_id',
            label: 'Choose business division ( CPC Division )',
            options$: this.cpcService.getDivison(),
            multiple: true,
            filter: {
                parent: 'cpc_section_id',
                key: 'section'
            }
        }), // 1
        new FormSelect({
            key: 'cpc_group_id',
            label: 'Choose business group ( CPC Group )',
            options$: this.cpcService.getGroup(),
            multiple: true,
            filter: {
                parent: 'cpc_division_id',
                key: 'division'
            }
        }), // 2
        new FormSelect({
            key: 'cpc_class_id',
            label: 'Choose business class(es) ( CPC Classes )',
            options$: this.cpcService.getClasses(),
            multiple: true,
            filter: {
                parent: 'cpc_group_id',
                key: 'group'
            }
        }), // 3
        new FormSelect({
            key: 'cpc_sub_class_ids',
            label: 'Choose business subclass(es) ( CPC Subclasses )',
            options$: this.cpcService.getSubclasses(),
            multiple: true,
            filter: {
                parent: 'cpc_class_id',
                key: 'class'
            }
        }) // 4
    ]

    micForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'company_info',
            formGroup: [
                new FormTitle({
                    label: 'a. Will or did the investor(s) establish a new legal entity (e.g. company) for the proposed investment?'
                }),
                new FormRadio({
                    key: 'overview_is_established',
                    label: 'No, established',
                    value: 'No',
                    required: true,
                    columns: 'col-md-6'
                }),
                new FormRadio({
                    key: 'overview_is_established',
                    label: 'Yes, established',
                    value: 'Yes',
                    required: true,
                    columns: 'col-md-6'
                }),
                new FormTitle({
                    label: "If yes, please give details on new legal entity.",
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes'],
                    }
                }),
                new FormInput({
                    key: 'investment_name',
                    label: 'i. Name',
                    required: true,
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes'],
                    }
                }),
                new FormSelect({
                    key: 'company_type',
                    label: 'ii. Type of company, if legal entity is company',
                    options$: this.localService.getCompany(),
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes'],
                    }
                }),
                new FormInput({
                    key: 'legal_type',
                    label: 'If not company, type of legal entity and relevant law under which it is / will be registered',
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormInput({
                    key: 'company_certificate_no',
                    label: 'Registration certificate number, if applicable',
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormTitle({
                    label: 'Please upload a copy of the registration certificate, if applicable',
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormFile({
                    key: 'company_certificate_doc',
                    label: 'File name',
                    columns: 'col-md-9',
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormTitle({
                    label: 'iii. Address ',
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormSelect({
                    key: 'country',
                    label: 'Country',
                    options$: this.lotService.getCountry(),
                    required: true,
                    value: 'Myanmar',
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormSelect({
                    key: 'state',
                    label: 'State / Region:',
                    options$: this.lotService.getState(),
                    required: true,
                    columns: 'col-md-4',
                    criteriaValue: {
                        key: 'country',
                        value: ['Myanmar'],
                    }
                }),
                new FormSelect({
                    key: 'district',
                    label: 'District:',
                    options$: this.lotService.getDistrict(),
                    required: true,
                    columns: 'col-md-4',
                    filter: {
                        parent: 'state',
                        key: 'state'
                    },
                    criteriaValue: {
                        key: 'country',
                        value: ['Myanmar'],
                    }
                }),
                new FormSelect({
                    key: 'township',
                    label: 'Township:',
                    columns: 'col-md-4',
                    options$: this.lotService.getTownship(),
                    required: true,
                    filter: {
                        parent: 'district',
                        key: 'district'
                    },
                    criteriaValue: {
                        key: 'country',
                        value: ['Myanmar'],
                    }
                }),
                new FormInput({
                    key: 'address',
                    label: 'Additional address details',
                    required: true,
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormInput({
                    key: 'phone_no',
                    label: 'iv. Phone number',
                    columns: 'col-md-6',
                    type: 'number',
                    required: true,
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormInput({
                    key: 'email_address',
                    label: 'v. E-mail address',
                    required: true,
                    columns: 'col-md-6',
                    validators: [Validators.email],
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormInput({
                    key: 'website',
                    label: 'vi. Website',
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new FormSelect({
                    key: 'general_sector_id',
                    label: 'b. General business sector of the proposed investment',
                    required: true,
                    options$: this.isicService.getInvestment(),
                    valueChanges$: this.generalSector$,
                }),
                new FormTitle({
                    label: 'c. Specific sector of the proposed investment:'
                }),
                new BaseFormGroup({
                    key: 'general_business_sector',
                    formGroup: this.businessSector,
                }),
                new FormNote({
                    label: 'The specific sectors are classified according to International Standard Industrial Classification (ISIC) Rev. 4.'
                }),
                new FormInput({
                    key: 'detail_sector',
                    label: 'Please provide further details on the sector of the investment, if necessary',
                }),
                new FormTitle({
                    label: 'd. Optional: please name the specific product(s)/service(s) of the investment:'
                }),
                new BaseFormGroup({
                    key: 'business_cpc',
                    formGroup: this.businessCPC
                }),
                new FormNote({
                    label: 'Products/services are classified according to Central Product Classification (CPC) Version 2.1.'
                }),
                new FormTextArea({
                    key: 'invest_description',
                    label: 'e. Please give a description of the nature of the proposed activities of the investment, including details on the products/services and production and sales strategy of the investment business',
                    required: true,
                }),
                new FormInput({
                    key: 'estimated_domestic',
                    label: 'Estimated domestic market sales / Total sales',
                    required: true,
                    columns: 'col-md-6',
                    type: 'number',
                    endfix: '%',
                    validators: [Validators.min(0), Validators.max(100)],
                }),
                new FormInput({
                    key: 'estimated_export',
                    label: 'Estimated export market sales / Total sales',
                    required: true,
                    columns: 'col-md-6',
                    type: 'number',
                    endfix: '%',
                    validators: [Validators.min(0), Validators.max(100)],
                }),
                new FormTextArea({
                    key: 'invest_benefit',
                    label: 'f. Proposed investment’s supply chain and benefits to the other related businesses',
                    required: true
                }),
                new FormTitle({
                    label: 'g. Contact Person'
                }),
                new FormInput({
                    key: 'contact_name',
                    label: 'Full name',
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'contact_position',
                    label: 'Position',
                    columns: 'col-md-6',
                    required: true
                }),
                new FormInput({
                    key: 'contact_phone',
                    label: 'Phone number',
                    columns: 'col-md-6',
                    required: true,
                    type: 'number'
                }),
                new FormInput({
                    key: 'contact_email',
                    label: 'E-mail address',
                    columns: 'col-md-6',
                    required: true,
                    validators: [Validators.email]
                }),
                new FormTitle({
                    label: 'Please upload evidence of the financial conditions of the business (e.g. the most recent bank statement or the most recent audit report) in Myanmar language or English:',
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
                new BaseFormArray({
                    key: 'evidence_documents',
                    formArray: [
                        new FormFile({
                            key: 'evidence_doc',
                            label: 'Name of document:',
                            required: true,
                            multiple: true
                        })
                    ],
                    criteriaValue: {
                        key: 'overview_is_established',
                        value: ['Yes']
                    }
                }),
            ]
        }),
    ]

    constructor(
        private http: HttpClient,
        private router: Router,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        public location: Location,
        private toast: ToastrService,
        private store: Store<AppState>,
        private isicService: ISICService,
        private cpcService: CPCService,
        private localService: LocalService,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private lotService: LocationService
    ) {
        super(formCtlService);
    }

    ngOnInit(): void {
        this.loading = true;
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;
            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.micModel.company_info = this.micModel.company_info || form?.data?.company_info || {};

                        this.micModel.company_info.general_business_sector = this.micModel.company_info.general_business_sector || {};
                        this.micModel.company_info.general_business_sector.general_sector_id = this.micModel.company_info.general_sector_id;

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel, form.data || {});

                        this.generalSector$.subscribe(value => {
                            this.micFormGroup.get('company_info')
                                .get('general_business_sector')
                                .get('general_sector_id').setValue(value);
                        })

                        this.page = "mic/sectionB-form3/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();

    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var company_type = this.micForm[0].formGroup.filter(x => x.key == "company_type")[0];
        company_type.options$ = this.localService.getCompany(lan);

        var general_sector_id = this.micForm[0].formGroup.filter(x => x.key == "general_sector_id")[0];
        general_sector_id.options$ = this.isicService.getInvestment(lan);

        var isic_class_ids = this.businessSector.filter(x => x.key == "isic_class_ids")[0];
        isic_class_ids.options$ = this.isicService.getClasses(lan);

        this.authService.changeLanguage$.subscribe(x => {
            company_type.options$ = this.localService.getCompany(x);
            general_sector_id.options$ = this.isicService.getInvestment(x);
            isic_class_ids.options$ = this.isicService.getClasses(x);
        })
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.is_save = false;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.is_save = true;

        this.saveFormData();
    }

    saveFormData() {
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {

            this.micModel = { ...this.micModel, ...formValue };
            this.service.create(this.micModel)
                .subscribe(x => {
                    this.spinner = false;
                    this.is_save = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionB-form4', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionB-form4', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionB-form3', id, 'mm']);
                }
            }
        }

    }
}
