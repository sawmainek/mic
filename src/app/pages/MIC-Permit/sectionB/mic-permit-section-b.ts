import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermit } from '../mic-permit';

export class MICPermitSectionB extends MICPermit {
    static section = "Section B";
    static totalForm = 4;
    totalForm = 4;
    constructor(public formCtlService: FormControlService) {
        super();
    }
    saveFormProgress(form_id: number, id: number, page: string) {
        if (this.progressForms?.length > 0) {
            if (this.progressForms.filter(x => x.type == MICPermit.type && x.section == MICPermitSectionB.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: MICPermit.type,
                    type_id: id,
                    section: MICPermitSectionB.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }
        } else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: MICPermit.type,
                type_id: id,
                section: MICPermitSectionB.section,
                page: page,
                status: true,
                revise: false
            });
        }
        
    }
}