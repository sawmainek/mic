import { BaseForm } from '../../../custom-component/dynamic-forms/base/base-form';
import { PrintPDFService } from '../../../custom-component/print.pdf.service';
import { MicSectionFForm2 } from '../sectionF/mic-section-f-form2/mic-section-f-form2';
import { MicSectionEForm5 } from '../sectionE/mic-section-e-form5/mic-section-e-form5';
import { MicSectionEForm4 } from '../sectionE/mic-section-e-form4/mic-section-e-form4';
import { MicSectionEForm3 } from '../sectionE/mic-section-e-form3/mic-section-e-form3';
import { MicSectionEForm2 } from '../sectionE/mic-section-e-form2/mic-section-e-form2';
import { MicSectionDForm8 } from '../sectionD/mic-section-d-form8/mic-section-d-form8';
import { MicSectionDForm7 } from '../sectionD/mic-section-d-form7/mic-section-d-form7';
import { MicSectionDForm6 } from '../sectionD/mic-section-d-form6/mic-section-d-form6';
import { MicSectionDForm5 } from '../sectionD/mic-section-d-form5/mic-section-d-form5';
import { MicSectionDForm4 } from '../sectionD/mic-section-d-form4/mic-section-d-form4';
import { MicSectionDForm3 } from '../sectionD/mic-section-d-form3/mic-section-d-form3';
import { MicSectionDForm2 } from '../sectionD/mic-section-d-form2/mic-section-d-form2';
import { MicSectionCForm8 } from '../sectionC/mic-section-c-form8/mic-section-c-form8';
import { MicSectionCForm7 } from '../sectionC/mic-section-c-form7/mic-section-c-form7';
import { MicSectionCForm6 } from '../sectionC/mic-section-c-form6/mic-section-c-form6';
import { MicSectionCForm5 } from '../sectionC/mic-section-c-form5/mic-section-c-form5';
import { MicSectionCForm4 } from '../sectionC/mic-section-c-form4/mic-section-c-form4';
import { MicSectionCForm3 } from '../sectionC/mic-section-c-form3/mic-section-c-form3';
import { MicSectionCForm2 } from '../sectionC/mic-section-c-form2/mic-section-c-form2';
import { ISICService } from 'src/services/isic.service';
import { CPCService } from '../../../../services/cpc.service';
import { MicSectionBForm4 } from '../sectionB/mic-section-b-form4/mic-section-b-form4';
import { MicSectionBForm3 } from '../sectionB/mic-section-b-form3/mic-section-b-form3';
import { MicSectionBForm2 } from '../sectionB/mic-section-b-form2/mic-section-b-form2';
import { Injectable } from '@angular/core';
import { MicSectionAForm4 } from '../sectionA/mic-section-a-form4/mic-section-a-form4';
import { MICSectionAForm3 } from '../sectionA/mic-section-a-form3/mic-section-a-form3';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';
import { MicSectionAForm2 } from '../sectionA/mic-section-a-form2/mic-section-a-form2';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { MicSectionBForm5 } from '../sectionB/mic-section-b-form5/mic-section-b-form5';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable()
export class MICFormPreviewService {

    forms: BaseForm<string>[] = [];
    constructor(
        public lotService: LocationService,
        public localService: LocalService,
        public isicService: ISICService,
        public cpcService: CPCService,
        public printPDFService: PrintPDFService
    ) {
        
    }


    toFormPDF(data: any, option: {
        readOnly?: boolean,
        language?: string,
        onSubmit$?: any
    }) {
        const sectionAForm4 = new MicSectionAForm4(this.lotService, this.localService);
        const sectionBForm2 = new MicSectionBForm2();
        const sectionBForm3 = new MicSectionBForm3(this.lotService, this.localService, this.isicService, this.cpcService);
        const sectionBForm4 = new MicSectionBForm4(this.lotService, this.localService);
        const sectionBForm5 = new MicSectionBForm5(this.lotService);
        const sectionCForm2 = new MicSectionCForm2(this.localService);
        const sectionCForm3 = new MicSectionCForm3();
        const sectionCForm4 = new MicSectionCForm4(this.lotService);
        const sectionCForm5 = new MicSectionCForm5(this.lotService);
        const sectionCForm6 = new MicSectionCForm6();
        const sectionCForm7 = new MicSectionCForm7(this.lotService, this.localService);
        const sectionCForm8 = new MicSectionCForm8();
        const sectionDForm2 = new MicSectionDForm2();
        const sectionDForm3 = new MicSectionDForm3(this.lotService, this.localService);
        const sectionDForm4 = new MicSectionDForm4(this.lotService);
        const sectionDForm5 = new MicSectionDForm5(this.localService);
        const sectionDForm6 = new MicSectionDForm6(this.localService);
        const sectionDForm7 = new MicSectionDForm7(this.lotService, this.localService);
        const sectionDForm8 = new MicSectionDForm8();
        const sectionEForm2 = new MicSectionEForm2();
        const sectionEForm3 = new MicSectionEForm3();
        const sectionEForm4 = new MicSectionEForm4();
        const sectionEForm5 = new MicSectionEForm5();
        const sectionFForm2 = new MicSectionFForm2(this.lotService);

        sectionEForm3.forms[0].value = data.company_info?.general_business_sector?.general_sector_id || "";
        sectionEForm4.forms[0].value = data.company_info?.general_business_sector?.general_sector_id || "";
        sectionEForm5.forms[0].value = data.company_info?.general_business_sector?.general_sector_id || "";

        let expected_investmentdt = data['expected_investmentdt'] || null;
        let cash_equivalents: any[] = [];
        if (expected_investmentdt) {
            cash_equivalents.push({
                currency: 'Myanmar',
                bank_balances: expected_investmentdt.cash_equivalent[0].myan_origin,
                cash_on_hand: expected_investmentdt.cash_equivalent[1].myan_origin,
                deposits: expected_investmentdt.cash_equivalent[2].myan_origin
            });
            expected_investmentdt.cash_equivalent[0].foreign_origin_list.forEach((origin, index) => {
                cash_equivalents.push({
                    currency: origin.currency,
                    bank_balances: expected_investmentdt.cash_equivalent[0]['foreign_origin_list'][index]['amount'],
                    cash_on_hand: expected_investmentdt.cash_equivalent[1]['foreign_origin_list'][index]['amount'],
                    deposits: expected_investmentdt.cash_equivalent[2]['foreign_origin_list'][index]['amount'],
                });
            });
        }
        data['expected_investmentdt'] = data['expected_investmentdt'] || {};
        data['expected_investmentdt']['cash_equivalents'] = cash_equivalents;

        data['timeline'] = data['timeline'] || {};

        data['timeline']['construction_date_no'] = (data.timeline?.construction_date_no || '')+' '+(data.timeline?.construction_date_label || '');

        data['timeline']['commercial_date_no'] = (data.timeline?.commercial_date_no || '')+' '+(data.timeline?.commercial_date_label || '');

        data['timeline']['investment_start_date'] = (data.timeline?.investment_start_date || '')+' Years';

        this.forms = [
            ...sectionAForm4.forms,
            ...sectionBForm2.forms,
            ...sectionBForm3.forms,
            ...sectionBForm4.forms,
            ...sectionBForm5.forms,
            ...sectionCForm2.forms,
            ...sectionCForm3.forms,
            ...sectionCForm4.forms,
            ...sectionCForm5.forms,
            ...sectionCForm6.forms,
            ...sectionCForm7.forms,
            ...sectionCForm8.forms,
            ...sectionDForm2.forms,
            ...sectionDForm3.forms,
            ...sectionDForm4.forms,
            ...sectionDForm5.forms,
            ...sectionDForm6.forms,
            ...sectionDForm7.forms,
            ...sectionDForm8.forms,
            ...sectionEForm2.forms,
            ...sectionEForm3.forms,
            ...sectionEForm4.forms,
            ...sectionEForm5.forms,
            ...sectionFForm2.forms,
        ];
        this.printPDFService.toFormPDF(this.forms, data, 'MIC Permit (Form 2)', option);
    }
}
