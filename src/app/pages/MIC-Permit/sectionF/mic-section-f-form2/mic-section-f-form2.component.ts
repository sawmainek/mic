import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Location } from '@angular/common';
import { MICPermitSectionF } from '../mic-permit-section-f';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { LocationService } from 'src/services/location.service';

@Component({
    selector: 'app-mic-section-f-form2',
    templateUrl: './mic-section-f-form2.component.html',
    styleUrls: ['./mic-section-f-form2.component.scss']
})
export class MicSectionFForm2Component extends MICPermitSectionF implements OnInit {
    menu: any = "sectionF";
    correct: any = false;
    page = "mic/sectionF-form2/";
    @Input() id: any;
    mm: any;

    user: any = {};

    micModel: any = {};
    micFormGroup: FormGroup;
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '1. Did an authorized representative fill in the application on behalf of the investor(s)?'
        }),
        new FormRadio({
            key: 'is_authorized',
            required: true,
            columns: 'col-md-4',
            value: 'Yes',
            label: 'Yes'
        }),
        new FormRadio({
            key: 'is_authorized',
            required: true,
            columns: 'col-md-8',
            value: 'No',
            label: 'No, the investor(s) filled in the application by themselves'
        }),
        new FormInput({
            key: 'company_name',
            label: 'a. Name of company, if applicable',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormTitle({
            label: 'b. Address of company, if applicable',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormSelect({
            key: 'company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormSelect({
            key: 'company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'company_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'company_address',
            label: 'Additional address details',
            required: true,
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormSectionTitle({
            label: 'c. Contact person',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full Name',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            key: 'contact_phone',
            label: 'Phone number',
            required: true,
            type: 'number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormInput({
            key: 'contact_nrc_passport',
            label: 'Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
            required: true,
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormSelect({
            key: 'contact_citizenship',
            label: 'Citizenship',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormTitle({
            label: 'Please upload official letter of legal representation:',
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
        new FormFile({
            key: 'document',
            label: 'File Name',
            required: true,
            criteriaValue: {
                key: 'is_authorized',
                value: 'Yes'
            }
        }),
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private lotService: LocationService
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
        this.getMicData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getMicData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";
            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel.other_investor_info || {}, form?.data?.other_investor_info || {});

                        this.page = "mic/sectionF-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'other_investor_info': formValue };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }

                });
        })
    }

    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

            if (this.micModel?.choose_language == 'Myanmar') {
                const route = '/mic/permit-certificate';

                if (this.form?.language == 'Myanmar') {
                    this.router.navigate([page + route, id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + route, id, 'mm']);
                    } else {
                        this.router.navigate([page + '/mic/sectionF-form2', id, 'mm']);
                    }
                }
            }
            else {
                const route = '/mic/undertaking';

                if (this.form?.language == 'Myanmar') {
                    this.router.navigate([page + route, id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + route, id]);
                    } else {
                        this.router.navigate([page + '/mic/sectionF-form2', id, 'mm']);
                    }
                }
            }
        }
    }
}
