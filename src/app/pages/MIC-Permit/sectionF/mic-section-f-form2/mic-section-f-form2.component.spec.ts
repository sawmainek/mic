import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionFForm2Component } from './mic-section-f-form2.component';

describe('MicSectionFForm2Component', () => {
  let component: MicSectionFForm2Component;
  let fixture: ComponentFixture<MicSectionFForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionFForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionFForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
