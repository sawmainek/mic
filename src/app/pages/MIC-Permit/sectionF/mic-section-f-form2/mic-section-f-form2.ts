import { FormCheckBox } from './../../../../custom-component/dynamic-forms/base/form-checkbox';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocationService } from 'src/services/location.service';

export class MicSectionFForm2 {
    
    public forms: BaseForm<string>[] = []

    constructor(
        private lotService: LocationService

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'other_investor_info',
                formGroup: [
                    new FormSectionTitle({
                        label: '1. Did an authorized representative fill in the application on behalf of the investor(s)?'
                    }),
                    new FormRadio({
                        key: 'is_authorized',
                        required: true,
                        columns: 'col-md-4',
                        value: 'Yes',
                        label: 'Yes'
                    }),
                    new FormRadio({
                        key: 'is_authorized',
                        required: true,
                        columns: 'col-md-8',
                        value: 'No',
                        label: 'No, the investor(s) filled in the application by themselves'
                    }),
                    new FormInput({
                        key: 'company_name',
                        label: 'a. Name of company, if applicable',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormTitle({
                        label: 'b. Address of company, if applicable',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'company_country',
                        label: 'Country',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'company_state',
                        label: 'State / Region:',
                        options$: this.lotService.getState(),
                        required: true,
                        columns: 'col-md-4',
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'company_district',
                        label: 'District:',
                        options$: this.lotService.getDistrict(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'company_state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'company_township',
                        label: 'Township:',
                        columns: 'col-md-4',
                        options$: this.lotService.getTownship(),
                        required: true,
                        filter: {
                            parent: 'company_district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormInput({
                        key: 'company_address',
                        label: 'Additional address details',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),  
                    new FormSectionTitle({
                        label: 'c. Contact person',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'contact_full_name',
                        label: 'Full Name',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'contact_position',
                        label: 'Position',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'contact_phone',
                        label: 'Phone number',
                        required: true,
                        type: 'number',
                        validators: [Validators.minLength(6)],
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'contact_email',
                        label: 'E-mail address',
                        required: true,
                        validators: [Validators.email],
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'contact_nrc_passport',
                        label: 'Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'contact_citizenship',
                        label: 'Citizenship',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'contact_citizenship',
                        label: 'Citizenship',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar'
                    }),
                    new FormTitle({
                        label: 'Please upload official letter of legal representation:',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormFile({
                        key: 'document',
                        label: 'File Name',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                ]
            }),
            new BaseFormGroup({
                key: 'submission',
                formGroup: [
                    new FormTitle({
                        label: 'By the submitting this application:'
                    }),
                    new FormCheckBox({
                        key: 'is_true',
                        required: true,
                        label: 'I / We hereby declare that the above statements are true and correct to the best of my/our knowledge and belief.'
                    }),
                    new FormCheckBox({
                        key: 'is_understand',
                        required: true,
                        label: 'I /We fully understand that proposal may be denied or unnecessarily delayed if the applicant fails to provide required information to the Commission for issuance of the permit.'
                    }),
                    new FormCheckBox({
                        key: 'is_strictly_comply',
                        required: true,
                        label: 'I/We hereby declare to strictly comply with terms and conditions set out by the Myanmar Investment Commission.'
                    }),
                ]
            })
            
        ];
    }
}