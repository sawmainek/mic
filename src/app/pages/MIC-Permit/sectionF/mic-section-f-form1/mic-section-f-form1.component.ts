import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../../../../core/reducers/index';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Mic } from 'src/app/models/mic';
import { FormProgressService } from 'src/services/form-progress.service';
import { MICFormService } from 'src/services/micform.service';
import { MICPermitSectionF } from '../mic-permit-section-f';

@Component({
    selector: 'app-mic-section-f-form1',
    templateUrl: './mic-section-f-form1.component.html',
    styleUrls: ['./mic-section-f-form1.component.scss']
})
export class MicSectionFForm1Component extends MICPermitSectionF implements OnInit {
    menu: any = "sectionF";
    correct: any = false;
    @Input() id: any;
    mm: any;


    // model: any = {};
    micModel: Mic;
    micMMModel: Mic;
    form: any; //For New Flow

    progressLists: any[] = [];
    pages: any[] = [];

    loading = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private formService: MICFormService,
        private router: Router,
        private store: Store<AppState>,
        private translateService: TranslateService,
    ) {
        super(formCtlService);
        this.loading = true;
        this.setTableData();
    }

    ngOnInit(): void { }

    setTableData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            if (this.id > 0) {
                this.formService.show(this.id)
                    .subscribe(x => {
                        this.form = x || {};
                        this.micModel = this.form?.data || {};
                        this.micMMModel = this.form?.data_mm || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(x) }));

                        this.getFormProgress();

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
            else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            `mic/sectionF-form2`
        ];
        forkJoin([
            this.formProgressService.get({
                rows: 999,
                search: `type_id:equal:${this.micModel?.id || 0}|type:equal:MIC-Permit`,
                secure: true
            }),
            this.formProgressService.get({
                rows: 999,
                search: `type_id:equal:${this.micMMModel?.id || 0}|type:equal:MIC-Permit`,
                secure: true
            })
        ]).subscribe((x: any[]) => {
            this.progressLists = [...x[0], ...x[1]];
            this.loading = false;
        });
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
            const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    openPage(link) {
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/officer/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/officer/' + link, this.id]);
            }
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/pages/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/pages/' + link, this.id]);
            }
        }
    }
}