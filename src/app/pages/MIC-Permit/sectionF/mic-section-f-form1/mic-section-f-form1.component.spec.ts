import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionFForm1Component } from './mic-section-f-form1.component';

describe('MicSectionFForm1Component', () => {
  let component: MicSectionFForm1Component;
  let fixture: ComponentFixture<MicSectionFForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionFForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionFForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
