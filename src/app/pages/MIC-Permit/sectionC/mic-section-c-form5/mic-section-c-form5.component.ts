import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { LocationService } from './../../../../../services/location.service';
import { FormDummy } from './../../../../custom-component/dynamic-forms/base/form-dummy';
import { ToastrService } from 'ngx-toastr';

import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { Mic } from 'src/app/models/mic';
import { MICPermitSectionC } from '../mic-permit-section-c';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-c-form5',
    templateUrl: './mic-section-c-form5.component.html',
    styleUrls: ['./mic-section-c-form5.component.scss']
})
export class MicSectionCForm5Component extends MICPermitSectionC implements OnInit {
    page = "mic/sectionC-form5/";
    @Input() id: any;
    menu: any = "sectionC";

    user: any = {};

    micModel: Mic;
    micFormGroup: FormGroup;
    mm: any = null;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    loading = false;

    totalZone1 = 0;
    totalZone2 = 0;
    totalZone3 = 0;

    isRevision: boolean = false;

    zone1: BaseForm<any>[] = [
        new FormDummy({
            key: 'zone',
            value: '1'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region',
            options$: this.lotService.getState(),
            filter: {
                parent: 'zone',
                key: 'zone'
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District',
            options$: this.lotService.getDistrict(),
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township',
            options$: this.lotService.getTownship(),
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'pecent_expected',
            label: 'Percentage (%) of total expected investment value',
            validators: [Validators.min(0), Validators.max(100)],
            endfix: '%',
            style: {
                'min-width': '200px'
            },
            type: 'number',
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateZone1Total(value, index, form, formGroup);
            }
        }),
    ]

    zone2: BaseForm<any>[] = [
        new FormDummy({
            key: 'zone',
            value: '2'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region',
            options$: this.lotService.getState(),
            filter: {
                parent: 'zone',
                key: 'zone'
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District',
            options$: this.lotService.getDistrict(),
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township',
            options$: this.lotService.getTownship(),
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'pecent_expected',
            label: 'Percentage (%) of total expected investment value',
            validators: [Validators.min(0), Validators.max(100)],
            endfix: '%',
            style: {
                'min-width': '200px'
            },
            type: 'number',
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateZone2Total(value, index, form, formGroup);
            }
        }),
    ]

    zone3: BaseForm<any>[] = [
        new FormDummy({
            key: 'zone',
            value: '3'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region',
            options$: this.lotService.getState(),
            filter: {
                parent: 'zone',
                key: 'zone'
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District',
            options$: this.lotService.getDistrict(),
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township',
            options$: this.lotService.getTownship(),
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
        new FormInput({
            key: 'pecent_expected',
            label: 'Percentage (%) of total expected investment value',
            validators: [Validators.min(0), Validators.max(100)],
            endfix: '%',
            style: {
                'min-width': '200px'
            },
            type: 'number',
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateZone3Total(value, index, form, formGroup);
            }
        }),
    ]

    micForm: BaseForm<any>[] = [
        new FormSectionTitle({
            label: '2. Expected investment value'
        }),
        new FormSectionTitle({
            label: 'c. Expected investment value by zone location'
        }),
        new FormNote({
            label: 'Locations by Zone were announced in Notification No. 10 / 2017 of the Myanmar Investment Commission.'
        }),
        new FormTitle({
            label: 'Zone 1'
        }),
        new BaseFormGroup({
            key: 'zone_1',
            formGroup: [
                new BaseFormArray({
                    key: 'zone_1_list',
                    formArray: this.zone1,
                    useTable: true,
                    rowDeleteEvent: (index) => {
                        this.totalZone1 = this.totalZoneAddition(this.Zone1, 'pecent_expected');
                        this.micFormGroup.get('zone_1').get('zone1_total').setValue(this.totalZone1);
                    },
                    tableFooter: [
                        [
                            { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 },
                            {
                                cellFn: () => {
                                    return this.totalZone1;
                                }
                            },
                        ]
                    ]
                }),
                new FormHidden({ key: 'zone1_total' })
            ]
        }),
        new FormSectionTitle({
            label: 'Zone 2'
        }),
        new BaseFormGroup({
            key: 'zone_2',
            formGroup: [
                new BaseFormArray({
                    key: 'zone_2_list',
                    formArray: this.zone2,
                    useTable: true,
                    rowDeleteEvent: (index) => {
                        this.totalZone2 = this.totalZoneAddition(this.Zone2, 'pecent_expected');
                        this.micFormGroup.get('zone_2').get('zone2_total').setValue(this.totalZone2);
                    },
                    tableFooter: [
                        [
                            { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 },
                            {
                                cellFn: () => {
                                    return this.totalZone2;
                                }
                            },
                        ]
                    ]
                }),
                new FormHidden({ key: 'zone2_total' })
            ]
        }),
        new FormSectionTitle({
            label: 'Zone 3'
        }),
        new BaseFormGroup({
            key: 'zone_3',
            formGroup: [
                new BaseFormArray({
                    key: 'zone_3_list',
                    formArray: this.zone3,
                    useTable: true,
                    rowDeleteEvent: (index) => {
                        this.totalZone3 = this.totalZoneAddition(this.Zone3, 'pecent_expected');
                        this.micFormGroup.get('zone_3').get('zone3_total').setValue(this.totalZone3);
                    },
                    tableFooter: [
                        [
                            { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 },
                            {
                                cellFn: () => {
                                    return this.totalZone3;
                                }
                            },
                        ]
                    ]
                }),
                new FormHidden({ key: 'zone3_total' })
            ]
        }),
        new FormTitle({
            label: 'If necessary, upload supporting documents regarding expected investment value by zone location here:'
        }),
        new BaseFormArray({
            key: 'supporting_doc',
            formArray: [
                new FormFile({
                    key: 'document',
                    label: 'Name of document:',
                    multiple: true,
                })
            ],
        }),
    ]

    constructor(
        private http: HttpClient,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        public formCtlService: FormControlService,
        private toast: ToastrService,
        private lotService: LocationService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel?.expected_by_zone || {}, form?.data?.expected_by_zone || {});

                        this.totalZone1 = this.totalZoneAddition(this.Zone1, 'pecent_expected');
                        this.totalZone2 = this.totalZoneAddition(this.Zone2, 'pecent_expected');
                        this.totalZone3 = this.totalZoneAddition(this.Zone3, 'pecent_expected');

                        this.page = "mic/sectionC-form5/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var state1 = this.zone1.filter(x => x.key == "state")[0];
        state1.options$ = this.lotService.getState(lan);

        var district1 = this.zone1.filter(x => x.key == "district")[0];
        district1.options$ = this.lotService.getDistrict(lan);

        var township1 = this.zone1.filter(x => x.key == "township")[0];
        township1.options$ = this.lotService.getTownship(lan);

        var state2 = this.zone2.filter(x => x.key == "state")[0];
        state2.options$ = this.lotService.getState(lan);

        var district2 = this.zone2.filter(x => x.key == "district")[0];
        district2.options$ = this.lotService.getDistrict(lan);

        var township2 = this.zone2.filter(x => x.key == "township")[0];
        township2.options$ = this.lotService.getTownship(lan);

        var state3 = this.zone3.filter(x => x.key == "state")[0];
        state3.options$ = this.lotService.getState(lan);

        var district3 = this.zone3.filter(x => x.key == "district")[0];
        district3.options$ = this.lotService.getDistrict(lan);

        var township3 = this.zone3.filter(x => x.key == "township")[0];
        township3.options$ = this.lotService.getTownship(lan);

        this.authService.changeLanguage$.subscribe(x => {
            state1.options$ = this.lotService.getState(x);
            district1.options$ = this.lotService.getDistrict(x);
            township1.options$ = this.lotService.getTownship(x);
            state2.options$ = this.lotService.getState(x);
            district2.options$ = this.lotService.getDistrict(x);
            township2.options$ = this.lotService.getTownship(x);
            state3.options$ = this.lotService.getState(x);
            district3.options$ = this.lotService.getDistrict(x);
            township3.options$ = this.lotService.getTownship(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    get Zone1() {
        return (this.micFormGroup.get('zone_1').get('zone_1_list') as FormArray).controls;
    }

    get Zone2() {
        return (this.micFormGroup.get('zone_2').get('zone_2_list') as FormArray).controls;
    }

    get Zone3() {
        return (this.micFormGroup.get('zone_3').get('zone_3_list') as FormArray).controls;
    }

    updateZone1Total(value, index, form, formGroup) {
        this.totalZone1 = 0;
        let formValue = this.Zone1[index].get(form.key).value;
        formValue = value;
        this.totalZone1 = this.totalZoneAddition(this.Zone1, form.key);
        this.micFormGroup.get('zone_1').get('zone1_total').setValue(this.totalZone1);
    }

    updateZone2Total(value, index, form, formGroup) {
        this.totalZone2 = 0;
        let formValue = this.Zone2[index].get(form.key).value;
        formValue = value;
        this.totalZone2 = this.totalZoneAddition(this.Zone2, form.key);
        this.micFormGroup.get('zone_2').get('zone2_total').setValue(this.totalZone2);
    }

    updateZone3Total(value, index, form, formGroup) {
        this.totalZone3 = 0;
        let formValue = this.Zone3[index].get(form.key).value;
        formValue = value;
        this.totalZone3 = this.totalZoneAddition(this.Zone3, form.key);
        this.micFormGroup.get('zone_3').get('zone3_total').setValue(this.totalZone3);
    }

    totalZoneAddition(zone, key) {
        let total = 0;
        for (let i = 0; i < zone.length; i++) {
            total += +zone[i].get(key).value;
        }
        return total;
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    onDraftSave() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'expected_by_zone': formValue };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionC-form6', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionC-form6', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionC-form5', id, 'mm']);
                }
            }
        }
    }

}
