import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionCForm5Component } from './mic-section-c-form5.component';

describe('MicSectionCForm5Component', () => {
  let component: MicSectionCForm5Component;
  let fixture: ComponentFixture<MicSectionCForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionCForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionCForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
