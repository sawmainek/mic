import { Validators } from '@angular/forms';
import { LocationService } from 'src/services/location.service';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';

export class MicSectionCForm5 {

    public forms: BaseForm<string>[] = []

    constructor(
        lotService: LocationService
    ) {
        const zone1: BaseForm < any > [] =[
            new FormDummy({
                key: 'zone',
                value: '1'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region',
                options$: lotService.getState(),
                filter: {
                    parent: 'zone',
                    key: 'zone'
                }
            }),
            new FormSelect({
                key: 'district',
                label: 'District',
                options$: lotService.getDistrict(),
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township',
                options$: lotService.getTownship(),
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
            new FormInput({
                key: 'pecent_expected',
                label: 'Percentage (%) of total expected investment value',
                validators: [Validators.min(0), Validators.max(100)],
                endfix: '%',
                style: {
                    'min-width': '200px'
                },
                type: 'number',
                valueChangeEvent: (value, index, form, formGroup) => {
                    // updateZone1Total(value, index, form, formGroup);
                }
            }),
        ]

        const zone2: BaseForm < any > [] =[
            new FormDummy({
                key: 'zone',
                value: '2'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region',
                options$: lotService.getState(),
                filter: {
                    parent: 'zone',
                    key: 'zone'
                }
            }),
            new FormSelect({
                key: 'district',
                label: 'District',
                options$: lotService.getDistrict(),
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township',
                options$: lotService.getTownship(),
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
            new FormInput({
                key: 'pecent_expected',
                label: 'Percentage (%) of total expected investment value',
                validators: [Validators.min(0), Validators.max(100)],
                endfix: '%',
                style: {
                    'min-width': '200px'
                },
                type: 'number',
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.updateZone2Total(value, index, form, formGroup);
                }
            }),
        ]

        const zone3: BaseForm< any > [] =[
            new FormDummy({
                key: 'zone',
                value: '3'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region',
                options$: lotService.getState(),
                filter: {
                    parent: 'zone',
                    key: 'zone'
                }
            }),
            new FormSelect({
                key: 'district',
                label: 'District',
                options$: lotService.getDistrict(),
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township',
                options$: lotService.getTownship(),
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
            new FormInput({
                key: 'pecent_expected',
                label: 'Percentage (%) of total expected investment value',
                validators: [Validators.min(0), Validators.max(100)],
                endfix: '%',
                style: {
                    'min-width': '200px'
                },
                type: 'number',
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.updateZone3Total(value, index, form, formGroup);
                }
            }),
        ]

        this.forms =[
            new BaseFormGroup({
                key: 'expected_by_zone',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Expected investment value',
                        pageBreak: 'before'
                    }),
                    new FormSectionTitle({
                        label: 'c. Expected investment value by zone location'
                    }),
                    new FormNote({
                        label: 'Locations by Zone were announced in Notification No. 10 / 2017 of the Myanmar Investment Commission.'
                    }),
                    new FormTitle({
                        label: 'Zone 1'
                    }),
                    new BaseFormGroup({
                        key: 'zone_1',
                        formGroup: [
                            new BaseFormArray({
                                key: 'zone_1_list',
                                formArray: zone1,
                                useTable: true,
                                headerWidths: ['*', '*', '*', '40%'],
                                rowDeleteEvent: (index) => {
                                    // this.totalZone1 = this.totalZoneAddition(this.Zone1, 'pecent_expected');
                                    // this.micFormGroup.get('zone_1').get('zone1_total').setValue(this.totalZone1);
                                },
                                tablePDFFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 },
                                        {},
                                        {},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['percent_expected'] || 0)
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 },
                                        {
                                            cellFn: () => {
                                                // return this.totalZone1;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormHidden({ key: 'zone1_total' })
                        ]
                    }),
                    new FormSectionTitle({
                        label: 'Zone 2'
                    }),
                    new BaseFormGroup({
                        key: 'zone_2',
                        formGroup: [
                            new BaseFormArray({
                                key: 'zone_2_list',
                                formArray: zone2,
                                headerWidths: ['*', '*', '*', '40%'],
                                useTable: true,
                                rowDeleteEvent: (index) => {
                                    // this.totalZone2 = this.totalZoneAddition(this.Zone2, 'pecent_expected');
                                    // this.micFormGroup.get('zone_2').get('zone2_total').setValue(this.totalZone2);
                                },
                                tablePDFFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 },
                                        {},
                                        {},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['percent_expected'] || 0)
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 },
                                        {
                                            cellFn: () => {
                                                // return this.totalZone2;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormHidden({ key: 'zone2_total' })
                        ]
                    }),
                    new FormSectionTitle({
                        label: 'Zone 3'
                    }),
                    new BaseFormGroup({
                        key: 'zone_3',
                        formGroup: [
                            new BaseFormArray({
                                key: 'zone_3_list',
                                formArray: zone3,
                                useTable: true,
                                headerWidths: ['*', '*', '*', '40%'],
                                rowDeleteEvent: (index) => {
                                    // this.totalZone3 = this.totalZoneAddition(this.Zone3, 'pecent_expected');
                                    // this.micFormGroup.get('zone_3').get('zone3_total').setValue(this.totalZone3);
                                },
                                tablePDFFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 },
                                        {},
                                        {},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.forEach(x => {
                                                    total += Number(x['percent_expected'] || 0)
                                                });
                                                return total;
                                            }
                                        },
                                    ]
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 },
                                        {
                                            cellFn: () => {
                                                // return this.totalZone3;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormHidden({ key: 'zone3_total' })
                        ]
                    }),
                    new FormTitle({
                        label: 'If necessary, upload supporting documents regarding expected investment value by zone location here:'
                    }),
                    new BaseFormArray({
                        key: 'supporting_doc',
                        formArray: [
                            new FormFile({
                                key: 'document',
                                label: 'Name of document:',
                                multiple: true,
                            })
                        ],
                    }),
                ]
            })
            
        ]
    }
}