import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';

export class MicSectionCForm6 {

    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        const phase: BaseForm < any > [] =[
            new FormSectionTitleCounter({
                label: 'Phase'
            }),
            new FormDate({
                key: 'start_date',
                label: 'Period From Date',
                columns: 'col-md-6',
            }),
            new FormDate({
                key: 'end_date',
                label: 'Period To Date',
                columns: 'col-md-6',
            }),
            new FormInput({
                key: 'expected_value',
                label: '(Expected) investment value in Myanmar Kyat',
                columns: 'col-md-12',
                type: 'number',
            })

        ]

        this.forms =[
            new BaseFormGroup({
                key: 'expected_by_phase',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Expected investment value',
                        pageBreak: 'before'
                    }),
                    new FormSectionTitle({
                        label: 'd. OPTIONAL: (Expected) investment value by phase'
                    }),
                    new FormTitle({
                        label: 'Phases in construction/preparatory period:'
                    }),
                    new BaseFormArray({
                        key: 'construction_phase',
                        formArray: phase,
                        defaultLength: 1
                    }),
                    new FormTitle({
                        label: 'Phases in commercial operation period:'
                    }),
                    new BaseFormArray({
                        key: 'commercial_phase',
                        formArray: phase,
                        defaultLength: 1
                    }),
                ]
            })
        ]
    }
}