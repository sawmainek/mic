import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { FormSectionTitleCounter } from './../../../../custom-component/dynamic-forms/base/form-section-title-counter';
import { Observable } from 'rxjs';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICPermitSectionC } from '../mic-permit-section-c';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-c-form6',
    templateUrl: './mic-section-c-form6.component.html',
    styleUrls: ['./mic-section-c-form6.component.scss']
})
export class MicSectionCForm6Component extends MICPermitSectionC implements OnInit {
    page = "mic/sectionC-form6/";
    @Input() id: any;

    user: any = {};

    currencies: any;

    micModel: any = {};
    micFormGroup: FormGroup;
    mm: any;
    form: any;
    service: any;

    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    phase: BaseForm<any>[] = [
        new FormSectionTitleCounter({
            label: 'Phase'
        }),
        new FormDate({
            key: 'start_date',
            label: 'Period From Date',
            columns: 'col-md-6',
        }),
        new FormDate({
            key: 'end_date',
            label: 'Period To Date',
            columns: 'col-md-6',
        }),
        new FormInput({
            key: 'expected_value',
            label: '(Expected) investment value in Myanmar Kyat',
            columns: 'col-md-12',
            type: 'number',
        })

    ]

    micForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'expected_by_phase',
            formGroup: [
                new FormSectionTitle({
                    label: '2. Expected investment value'
                }),
                new FormSectionTitle({
                    label: 'd. OPTIONAL: (Expected) investment value by phase'
                }),
                new FormTitle({
                    label: 'Phases in construction/preparatory period:'
                }),
                new BaseFormArray({
                    key: 'construction_phase',
                    formArray: this.phase,
                    defaultLength: 1
                }),
                new FormTitle({
                    label: 'Phases in commercial operation period:'
                }),
                new BaseFormArray({
                    key: 'commercial_phase',
                    formArray: this.phase,
                    defaultLength: 1
                }),
            ]
        })
    ]

    constructor(
        private http: HttpClient,
        private router: Router,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        public location: Location,
        private store: Store<AppState>,
        private toast: ToastrService,
        public formCtlService: FormControlService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel, form.data || {});

                        this.page = "mic/sectionC-form6/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveMic();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveMic();
    }

    saveMic() {
        this.spinner = true;
        this.service.create({ ...this.micModel, ...this.micFormGroup.value }, { secure: true })
            .subscribe(x => {
                this.spinner = false;

                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                    this.redirectLink(this.form?.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionC-form7', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionC-form7', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionC-form6', id, 'mm']);
                }
            }
        }

    }

}
