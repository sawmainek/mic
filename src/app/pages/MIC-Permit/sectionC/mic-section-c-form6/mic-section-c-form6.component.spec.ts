import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionCForm6Component } from './mic-section-c-form6.component';

describe('MicSectionCForm6Component', () => {
  let component: MicSectionCForm6Component;
  let fixture: ComponentFixture<MicSectionCForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionCForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionCForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
