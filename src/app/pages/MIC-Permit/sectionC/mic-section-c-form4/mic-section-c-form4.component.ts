import { cloneDeep } from 'lodash';
import { LocationService } from './../../../../../services/location.service';
import { Submit, CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { ExpectedInvestdt } from 'src/app/models/expected-investdt';
import { MICPermitSectionC } from '../mic-permit-section-c';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';

@Component({
    selector: 'app-mic-section-c-form4',
    templateUrl: './mic-section-c-form4.component.html',
    styleUrls: ['./mic-section-c-form4.component.scss']
})
export class MicSectionCForm4Component extends MICPermitSectionC implements OnInit {
    @Input() id: any;
    page = "mic/sectionC-form4/";

    user: any = {};
    is_officer: boolean = false;

    cashFormGroup: FormGroup;
    micFormGroup: FormGroup;
    count: number = 0;
    micModel: any;
    mm: any;
    form: any;
    service: any;

    currencies: any;

    submitted: boolean = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    totalValue: any[] = [
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 }
    ];

    totalCash: any[] = [0, 0, 0];

    new_item: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Brand new items'
        }),
        new BaseFormArray({
            key: 'items',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                    required: true,
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    required: true,
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                    required: true
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    required: true,
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    required: true,
                    value: '102',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                    required: true
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('new_item', 0);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                    { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    recondition_item: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Reconditioned Items'
        }),
        new BaseFormArray({
            key: 'items',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    value: '102',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('recondition_item', 1);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                    { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    raw_material: BaseForm<any>[] = [
        new BaseFormArray({
            key: 'items',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    value: '102',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'raw_material');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('raw_material', 2);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                    { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[2].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[2].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    micForm: BaseForm<string>[] = [
        new FormInput({
            key: 'total_equivalent_usd',
            label: 'Total amount of Foreign cash to be brough in (Equivalent in USD)',
            required: true,
        }),
        new FormTitle({
            label: 'Period during which Foreign Cash and Equivalents will be brought in USD'
        }),
        new FormDate({
            key: 'start_date',
            label: 'From Date',
            columns: 'col-md-6',
            required: true,
            dateView: 'month'
        }),
        new FormDate({
            key: 'end_date',
            label: 'To Date',
            columns: 'col-md-6',
            required: true,
            dateView: 'month'
        }),
        new FormTitle({
            label: 'Please provide a breakdown of “Machinery & equipment”, procured locally and imported, using the following format:'
        }),
        new BaseFormGroup({
            key: 'new_item',
            formGroup: this.new_item
        }),
        new BaseFormGroup({
            key: 'recondition_item',
            formGroup: this.recondition_item
        }),
        new FormTitle({
            label: 'Please provide a breakdown of “Raw materials”, procured locally and imported, using the following format:'
        }),
        new BaseFormGroup({
            key: 'raw_material',
            formGroup: this.raw_material
        }),
        new FormTitle({
            label: 'If applicable, please upload a detailed breakdown of “Intellectual property rights, including technical knowhow, patent, industrial designs, and trademarks”:'
        }),
        new BaseFormArray({
            key: 'detailed_breakdown_docs',
            formArray: [
                new FormFile({
                    key: 'detailed_breakdown_docs_document',
                    label: 'Name of document:',
                    multiple: true,
                }),
            ],
            defaultLength: 1,
        }),
        new FormTitle({
            label: 'If applicable, please upload evidence of ownership of “Intellectual property rights, including technical knowhow, patent, industrial designs, and trademarks”:'
        }),
        new BaseFormArray({
            key: 'ownership_docs',
            formArray: [
                new FormFile({
                    key: 'ownership_document',
                    label: 'Name of document:',
                    multiple: true,
                }),
            ],
            defaultLength: 1,
        }),
        new FormTitle({
            label: 'If applicable, please upload any other supporting documents regarding the investment value:'
        }),
        new BaseFormArray({
            key: 'supporting_docs',
            formArray: [
                new FormFile({
                    key: 'supporting_document',
                    label: 'Name of document:',
                    multiple: true,
                }),
            ],
            defaultLength: 1,
        }),
    ];

    constructor(
        private ref: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private router: Router,
        private store: Store<AppState>,
        public location: Location,
        private lotService: LocationService,
        private toast: ToastrService,
        public formCtlService: FormControlService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService

    ) {
        super(formCtlService);
        this.loading = true;

        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;

        this.lotService.getCurrency().subscribe(data => {
            this.currencies = data;
            console.log(this.currencies)
        });

        this.createCashFormGroup();
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id, { secure: true }).subscribe((form: any) => {
                    this.form = form || {};
                    this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                    this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                    let expectedInvestdt = this.micModel?.expected_investmentdt || form?.data?.expected_investmentdt || new ExpectedInvestdt;
                    this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, expectedInvestdt);

                    this.page = "mic/sectionC-form4/";
                    this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                    this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                        this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                    });

                    if (expectedInvestdt?.cash_equivalent && expectedInvestdt?.cash_equivalent.length > 0) {

                        for (var i = 2; i < expectedInvestdt.cash_equivalent[0].foreign_origin_list?.length; i++) {
                            this.foreignList(0).push(this.columnForm());
                            this.foreignList(1).push(this.columnForm());
                            this.foreignList(2).push(this.columnForm());
                        }

                        expectedInvestdt.cash_equivalent.forEach((x, i) => {
                            this.cashFormGroup.get('cash_equivalent').patchValue(expectedInvestdt.cash_equivalent);
                        });
                        this.calculateForeign();
                        this.calculateKyat();
                    }

                    this.calculateTotalValue('new_item', 0);
                    this.calculateTotalValue('recondition_item', 1);
                    this.calculateTotalValue('raw_material', 2);

                    this.changeLanguage();
                    this.loading = false;
                })
            }
        }).unsubscribe();
    }

    calculateEquivalent(value, index, form, formGroup, control) {
        let formValue = this.micFormGroup.value;
        formValue[control]['items'][index][form.key] = value;

        this.calculateTotalValue(control, control == 'new_item' ? 0 : control == 'recondition_item' ? 1 : 2);
    }

    updateFormValue(value, index, form, formGroup, control) {
        console.log(this.micFormGroup)
        let formValue = this.micFormGroup.value;
        formValue[control]['items'][index][form.key] = value;

        // Update Total Value
        let rowValue = formGroup.value;
        formGroup.get('total').setValue(Number(rowValue.quantity | 0) * Number(rowValue.price | 0));

        this.calculateTotalValue(control, control == 'new_item' ? 0 : control == 'recondition_item' ? 1 : 2);
    }

    calculateTotalValue(control, index) {
        let formValue = this.micFormGroup.value;
        this.totalValue[index].total_kyat = 0;
        this.totalValue[index].total_usd = 0;

        formValue[control]['items'].map(x => {
            this.totalValue[index].total_kyat += parseFloat(x.kyat);
            this.totalValue[index].total_usd += parseFloat(x.usd);

            this.micFormGroup.get(control).get('total_kyat').setValue(this.totalValue[index].total_kyat);
            this.micFormGroup.get(control).get('total_usd').setValue(this.totalValue[index].total_usd);
        });
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    createCashFormGroup() {
        this.cashFormGroup = this.formBuilder.group({
            cash_equivalent: new FormArray(
                [
                    this.formBuilder.group({
                        name: ['Bank balances'],
                        myan_origin: ['0', [Validators.pattern('[0-9, +]+')]],
                        foreign_origin_list: new FormArray([this.columnForm(), this.columnForm()])
                    }),
                    this.formBuilder.group({
                        name: ['Cash on hand'],
                        myan_origin: ['0', [Validators.pattern('[0-9, +]+')]],
                        foreign_origin_list: new FormArray([this.columnForm(), this.columnForm()])
                    }),
                    this.formBuilder.group({
                        name: ['Short term deposits'],
                        myan_origin: ['0', [Validators.pattern('[0-9, +]+')]],
                        foreign_origin_list: new FormArray([this.columnForm(), this.columnForm()])
                    }),
                ],
                [Validators.required]),
        });
    }

    get cashList() {
        return (this.cashFormGroup.get('cash_equivalent') as FormArray).controls;
    }

    foreignList(index) {
        return (this.cashFormGroup.get('cash_equivalent') as FormArray).controls[index].get('foreign_origin_list') as FormArray;
    }

    columnForm() {
        return this.formBuilder.group({
            amount: ['0', [Validators.pattern('[0-9, +]+')]],
            currency: [''],
        })
    }

    addColumn() {
        this.foreignList(0).push(this.columnForm());
        this.foreignList(1).push(this.columnForm());
        this.foreignList(2).push(this.columnForm());

        this.totalCash.push(0);
        this.ref.detectChanges();
    }

    removeColumn(index) {
        this.foreignList(0).removeAt(index);
        this.foreignList(1).removeAt(index);
        this.foreignList(2).removeAt(index);

        this.totalCash.splice(index, 1);

        this.ref.detectChanges();
    }

    calculateKyat() {
        this.totalCash[0] = Number(this.cashList[0].get('myan_origin').value) +
            Number(this.cashList[1].get('myan_origin').value) +
            Number(this.cashList[2].get('myan_origin').value);
        this.ref.detectChanges();
    }

    calculateForeign() {
        this.foreignList(0).controls.forEach((foreign, i) => {
            this.totalCash[i + 1] = Number(this.foreignList(0).controls[i].get('amount').value) +
                Number(this.foreignList(1).controls[i].get('amount').value) +
                Number(this.foreignList(2).controls[i].get('amount').value);
        });
        this.ref.detectChanges();
    }

    changeCurrency(index, value) {
        this.foreignList(0).controls[index].get('currency').setValue(value);
        this.foreignList(1).controls[index].get('currency').setValue(value);
        this.foreignList(2).controls[index].get('currency').setValue(value);
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    checkDates(group: FormGroup) {
        let start_date = group.get('start_date').value;
        let end_date = group.get('end_date').value;
        let control = group.get('end_date');

        if (control.errors && !control.errors.date) {
            return;
        }
        else {
            if (Date.parse(start_date) > Date.parse(end_date))
                control.setErrors({ date: true });
            else
                control.setErrors(null);
        }
    }

    saveDraft() {
        this.is_draft = true;
        this.saveMicData();
    }

    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid || this.cashFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveMicData();
    }

    saveMicData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'expected_investmentdt': { ...this.micFormGroup.value, ...this.cashFormGroup.value, ...formValue } };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }

                }, error => {
                    console.log(error);
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionC-form5', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionC-form5', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionC-form4', id, 'mm']);
                }
            }
        }
    }

}
