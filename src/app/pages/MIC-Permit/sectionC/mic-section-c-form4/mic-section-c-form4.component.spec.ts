import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionCForm4Component } from './mic-section-c-form4.component';

describe('MicSectionCForm4Component', () => {
  let component: MicSectionCForm4Component;
  let fixture: ComponentFixture<MicSectionCForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionCForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionCForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
