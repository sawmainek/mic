import { FormInput } from './../../../../custom-component/dynamic-forms/base/form-input';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormGroup, Validators } from '@angular/forms';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { LocationService } from 'src/services/location.service';

export class MicSectionCForm4 {

    public forms: BaseForm<string>[] = [];
    formGroup: FormGroup;
    totalValue: any[] = [
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 }
    ];

    constructor(
        private lotService: LocationService,
    ) {

        let new_item: BaseForm<any>[] = [
            new FormSectionTitle({
                'label': 'Brand new items'
            }),
            new BaseFormArray({
                key: 'items',
                formArray: [
                    new FormInput({
                        key: 'name',
                        required: true,
                    }),
                    new FormInput({
                        key: 'code',
                        type: 'number',
                        required: true,
                        validators: [Validators.minLength(4), Validators.maxLength(4)],
                    }),
                    new FormInput({
                        key: 'unit',
                        // type: 'number',
                        required: true
                    }),
                    new FormInput({
                        key: 'price',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormInput({
                        key: 'quantity',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormInput({
                        key: 'total',
                        type: 'number',
                        value: '0',
                        required: true,
                        readonly: true
                    }),
                    new FormSelect({
                        key: 'currency',
                        options$: this.lotService.getCurrency(),
                        required: true,
                        value: '102',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormInput({
                        key: 'usd',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: this.lotService.getCountry(),
                        required: true
                    }),
                ],
                useTable: true,
                headerRows: 2,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue('new_item', 0);
                },
                tablePDFHeader: [
                    [
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        {},
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Total', colSpan: 7 }, {}, {}, {}, {}, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                })
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['usd'] || 0);
                                })
                                return total;
                            }
                        },
                        {}
                    ]
                ],
                tableHeader: [
                    [
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Total', colSpan: 7 },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_kyat;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_usd;
                            }
                        },
                    ]
                ]
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
            new FormHidden({
                key: 'total_usd'
            }),
        ];

        let recondition_item: BaseForm<any>[] = [
            new FormSectionTitle({
                'label': 'Reconditioned Items',                        
                pageBreak: 'before'
            }),
            new BaseFormArray({
                key: 'items',
                formArray: [
                    new FormInput({
                        key: 'name',
                    }),
                    new FormInput({
                        key: 'code',
                        type: 'number',
                        validators: [Validators.minLength(4), Validators.maxLength(4)],
                    }),
                    new FormInput({
                        key: 'unit',
                        // type: 'number',
                    }),
                    new FormInput({
                        key: 'price',
                        type: 'number',
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormInput({
                        key: 'quantity',
                        type: 'number',
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormInput({
                        key: 'total',
                        type: 'number',
                        value: '0',
                        readonly: true
                    }),
                    new FormSelect({
                        key: 'currency',
                        options$: this.lotService.getCurrency(),
                        value: '102',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormInput({
                        key: 'usd',
                        type: 'number',
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: this.lotService.getCountry(),
                    }),
                ],
                useTable: true,
                headerRows: 2,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue('recondition_item', 1);
                },
                tablePDFHeader: [
                    [
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        {},
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Total', colSpan: 7 }, {}, {}, {}, {}, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                })
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['usd'] || 0);
                                })
                                return total;
                            }
                        },
                        {}
                    ]
                ],
                tableHeader: [
                    [
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Total', colSpan: 7 },
                        {
                            cellFn: () => {
                                return this.totalValue[1].total_kyat;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[1].total_usd;
                            }
                        },
                    ]
                ]
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
            new FormHidden({
                key: 'total_usd'
            }),
        ];

        let raw_material: BaseForm<any>[] = [
            new BaseFormArray({
                key: 'items',
                formArray: [
                    new FormInput({
                        key: 'name',
                        required: true,
                    }),
                    new FormInput({
                        key: 'code',
                        type: 'number',
                        required: true,
                        validators: [Validators.minLength(4), Validators.maxLength(4)],
                    }),
                    new FormInput({
                        key: 'unit',
                        // type: 'number',
                        required: true
                    }),
                    new FormInput({
                        key: 'price',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'raw_material');
                        }
                    }),
                    new FormInput({
                        key: 'quantity',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'raw_material');
                        }
                    }),
                    new FormInput({
                        key: 'total',
                        type: 'number',
                        value: '0',
                        required: true,
                        readonly: true
                    }),
                    new FormSelect({
                        key: 'currency',
                        options$: this.lotService.getCurrency(),
                        required: true,
                        value: '102',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'raw_material');
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'raw_material');
                        }
                    }),
                    new FormInput({
                        key: 'usd',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'raw_material');
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: this.lotService.getCountry(),
                        required: true
                    }),
                ],
                useTable: true,
                headerRows: 2,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue('raw_material', 2);
                },
                tablePDFHeader: [
                    [
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        {},
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Total', colSpan: 7 }, {}, {}, {}, {}, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                })
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['usd'] || 0);
                                })
                                return total;
                            }
                        },
                        {}
                    ]
                ],
                tableHeader: [
                    [
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Total', colSpan: 7 },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_kyat;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_usd;
                            }
                        },
                    ]
                ]
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
            new FormHidden({
                key: 'total_usd'
            }),
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'expected_investmentdt',
                formGroup: [
                    new FormTitle({
                        label: '2. Expected investment value',
                    }),
                    new FormTitle({
                        label: 'b. Expected investment value - details',
                    }),
                    new BaseFormArray({
                        key: 'cash_equivalents',
                        formArray: [
                            new FormInput({
                                key: 'currency',
                            }),
                            new FormInput({
                                key: 'bank_balances',
                            }),
                            new FormInput({
                                key: 'cash_on_hand',
                            }),
                            new FormInput({
                                key: 'deposits',
                            }),
                            new FormInput({
                                callbackFn: (data) => {
                                    let total = Number(data['bank_balances'] || 0) + Number(data['cash_on_hand'] || 0) + Number(data['deposits'] || 0);
                                    return total;
                                },
                            })
                        ],
                        useTable: true,
                        tablePDFHeader: [
                            [
                                { label: 'Cash and equivalents' },
                                { label: 'Bank balances' },
                                { label: 'Cash on hand' },
                                { label: 'Short term deposits' },
                                { label: 'Total' },
                            ]
                        ]
                    }),
                    new FormInput({
                        key: 'total_equivalent_usd',
                        label: 'Total amount of Foreign cash to be brough in (Equivalent in USD)',
                        required: true,
                    }),
                    new FormTitle({
                        label: 'Period during which Foreign Cash and Equivalents will be brought in USD'
                    }),
                    new FormDate({
                        key: 'start_date',
                        label: 'From Date',
                        columns: 'col-md-6',
                        required: true,
                        dateView: 'month'
                    }),
                    new FormDate({
                        key: 'end_date',
                        label: 'To Date',
                        columns: 'col-md-6',
                        required: true,
                        dateView: 'month'
                    }),
                    new FormTitle({
                        label: 'Please provide a breakdown of “Machinery & equipment”, procured locally and imported, using the following format:',
                        pageOrientation: 'landscape',
                        pageBreak: 'before'
                    }),
                    new BaseFormGroup({
                        key: 'new_item',
                        formGroup: new_item
                    }),
                    new BaseFormGroup({
                        key: 'recondition_item',
                        formGroup: recondition_item,
                    }),
                    new FormTitle({
                        label: 'Please provide a breakdown of “Raw materials”, procured locally and imported, using the following format:',
                        pageBreak: 'before'
                    }),
                    new BaseFormGroup({
                        key: 'raw_material',
                        formGroup: raw_material
                    }),
                    new FormTitle({
                        label: 'If applicable, please upload a detailed breakdown of “Intellectual property rights, including technical knowhow, patent, industrial designs, and trademarks”:',
                        pageOrientation: 'portrait',
                        pageBreak: 'before'
                    }),
                    new BaseFormArray({
                        key: 'detailed_breakdown_docs',
                        formArray: [
                            new FormFile({
                                key: 'detailed_breakdown_docs_document',
                                label: 'Name of document:',
                                multiple: true,
                            }),
                        ],
                        defaultLength: 1,
                    }),
                    new FormTitle({
                        label: 'If applicable, please upload evidence of ownership of “Intellectual property rights, including technical knowhow, patent, industrial designs, and trademarks”:'
                    }),
                    new BaseFormArray({
                        key: 'ownership_docs',
                        formArray: [
                            new FormFile({
                                key: 'ownership_document',
                                label: 'Name of document:',
                                multiple: true,
                            }),
                        ],
                        defaultLength: 1,
                    }),
                    new FormTitle({
                        label: 'If applicable, please upload any other supporting documents regarding the investment value:'
                    }),
                    new BaseFormArray({
                        key: 'supporting_docs',
                        formArray: [
                            new FormFile({
                                key: 'supporting_document',
                                label: 'Name of document:',
                                multiple: true,
                            }),
                        ],
                        defaultLength: 1,
                    })
                ]
            })

        ];

    }

    updateFormValue(value, index, form, formGroup, control) {
        console.log(this.formGroup)
        let formValue = this.formGroup.value;
        formValue[control]['items'][index][form.key] = value;

        // Update Total Value
        let rowValue = formGroup.value;
        formGroup.get('total').setValue(Number(rowValue.quantity | 0) * Number(rowValue.price | 0));

        this.calculateTotalValue(control, control == 'new_item' ? 0 : control == 'recondition_item' ? 1 : 2);
    }

    calculateTotalValue(control, index) {
        let formValue = this.formGroup.value;
        this.totalValue[index].total_kyat = 0;
        this.totalValue[index].total_usd = 0;

        formValue[control]['items'].map(x => {
            this.totalValue[index].total_kyat += parseFloat(x.kyat);
            this.totalValue[index].total_usd += parseFloat(x.usd);

            this.formGroup.get(control).get('total_kyat').setValue(this.totalValue[index].total_kyat);
            this.formGroup.get(control).get('total_usd').setValue(this.totalValue[index].total_usd);
        });
    }

    calculateEquivalent(value, index, form, formGroup, control) {
        let formValue = this.formGroup.value;
        formValue[control]['items'][index][form.key] = value;

        this.calculateTotalValue(control, control == 'new_item' ? 0 : control == 'recondition_item' ? 1 : 2);
    }
}