import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionCForm7Component } from './mic-section-c-form7.component';

describe('MicSectionCForm7Component', () => {
  let component: MicSectionCForm7Component;
  let fixture: ComponentFixture<MicSectionCForm7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionCForm7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionCForm7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
