import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { MicService } from 'src/services/mic.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { Mic } from 'src/app/models/mic';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermitSectionC } from '../mic-permit-section-c';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';

@Component({
    selector: 'app-mic-section-c-form7',
    templateUrl: './mic-section-c-form7.component.html',
    styleUrls: ['./mic-section-c-form7.component.scss']
})
export class MicSectionCForm7Component extends MICPermitSectionC implements OnInit {
    menu: any = "sectionC";
    page = "mic/sectionC-form7/";
    @Input() id: any;

    user: any = {};

    micModel: Mic;
    micFormGroup: FormGroup;
    formGroup;
    mm: any;
    form: any;
    service: any;

    totalValue: any[] = [
        { 'kyat': 0, 'usd': 0 }, //loan_repay_total
        { 'kyat': 0, 'usd': 0 }, //issued_share_total
        { 'kyat': 0, 'usd': 0 }, //other_total
    ];

    isSubmit = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    loanRepay: BaseForm<string>[] = [
        new FormSelect({
            key: 'origin_id',
            options$: this.localService.getOrigin(),
            style: {
                'max-width:': '150px;'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateOrigin(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
            }
        }),
        new FormInput({
            key: 'source',
            style: {
                'min-width': '200px'
            }
        }),
        new FormInput({
            key: 'value',
            type: 'number',
            value: '0',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateTotal(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
            }
        }),
        new FormSelect({
            key: 'currency',
            options$: this.lotService.getCurrency(),
            placeholder: 'Select currency',
        }),
        new FormInput({
            key: 'equ_kyat',
            type: 'number',
            value: '0',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
            }
        }),
        new FormInput({
            key: 'equ_usd',
            type: 'number',
            value: '0',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
            }
        }),
    ];
    issuedShare: BaseForm<string>[] = [
        new FormSelect({
            key: 'origin_id',
            options$: this.localService.getOrigin(),
            style: {
                'max-width:': '150px;'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateOrigin(value, index, form, formGroup, 'issued_share', 'issued_share_list');
            }
        }),
        new FormInput({
            key: 'value',
            value: '0',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateTotal(value, index, form, formGroup, 'issued_share', 'issued_share_list');
            }
        }),
        new FormSelect({
            key: 'currency',
            placeholder: 'Select currency',
            options$: this.lotService.getCurrency(),
        }),
        new FormInput({
            key: 'equ_kyat',
            type: 'number',
            value: '0',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'issued_share', 'issued_share_list');
            }
        }),
        new FormInput({
            key: 'equ_usd',
            type: 'number',
            value: '0',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'issued_share', 'issued_share_list');
            }
        })
    ]
    otherSource: BaseForm<string>[] = [
        new FormInput({
            key: 'type',
            style: {
                'min-width': '200px'
            }
        }),
        new FormSelect({
            key: 'origin_id',
            options$: this.localService.getOrigin(),
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateOrigin(value, index, form, formGroup, 'other_source', 'other_source_list');
            }
        }),
        new FormInput({
            key: 'value',
            value: '0',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.updateTotal(value, index, form, formGroup, 'other_source', 'other_source_list');
            }
        }),
        new FormSelect({
            key: 'currency',
            placeholder: 'Select currency',
            options$: this.lotService.getCurrency(),
        }),
        new FormInput({
            key: 'equ_kyat',
            value: '0',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'other_source', 'other_source_list');
            }
        }),
        new FormInput({
            key: 'equ_usd',
            value: '0',
            type: 'number',
            style: {
                'min-width': '150px'
            },
            valueChangeEvent: (value, index, form, formGroup) => {
                this.calculateEquivalent(value, index, form, formGroup, 'other_source', 'other_source_list');
            }
        })
    ]
    capital: BaseForm<string>[] = [
        new FormInput({
            key: 'kyat',
            label: 'Equivalent Kyat',
            columns: 'col-md-6',
            type: 'number',
            value: '0',
            required: true,
            readonly: true,
        }),
        new FormInput({
            key: 'usd',
            label: 'Equivalent US$',
            columns: 'col-md-6',
            type: 'number',
            value: '0',
            required: true,
            readonly: true,
        })
    ]
    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '3. Particulars of financing of investment'
        }),
        new FormTitle({
            label: 'a. Loans repayable by investment business to finance investment'
        }),
        new BaseFormGroup({
            key: 'loan_repay',
            formGroup: [
                new BaseFormArray({
                    key: 'loan_repay_list',
                    formArray: this.loanRepay,
                    useTable: true,
                    tableHeader: [
                        [
                            { label: 'Origin', rowSpan: 2 },
                            { label: 'Source (Name of company/individual)', rowSpan: 2, style: { 'min-width': '200px' } },
                            { label: 'Value', colSpan: 2, style: { 'text-align': 'center' } },
                            { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                            { label: 'Equivalent US$', style: { 'text-align': 'center' } },
                        ],
                    ],
                    tableFooter: [
                        [
                            { label: 'Total', colSpan: 4, style: { 'text-align': 'center' } },
                            {
                                cellFn: () => {
                                    return this.totalValue[0].kyat;
                                }
                            },
                            {
                                cellFn: () => {
                                    return this.totalValue[0].usd;
                                }
                            },
                        ]
                    ]
                }),
                new FormDummy({ key: 'total_equ_kyat' }),
                new FormDummy({ key: 'total_equ_usd' })
            ]
        }),
        new FormTitle({
            label: 'b. Issued/paid up share capital to finance investment'
        }),
        new BaseFormGroup({
            key: 'issued_share',
            formGroup: [
                new BaseFormArray({
                    key: 'issued_share_list',
                    formArray: this.issuedShare,
                    useTable: true,
                    tableHeader: [
                        [
                            { label: 'Origin', rowSpan: 2 },
                            { label: 'Value', colSpan: 2, style: { 'text-align': 'center' } },
                            { label: '(Equivalent) Kyat', style: { 'text-align': 'center' } },
                            { label: '(Equivalent) US$', style: { 'text-align': 'center' } },
                        ],
                    ],
                    tableFooter: [
                        [
                            { label: 'Total', colSpan: 3, style: { 'text-align': 'center' } },
                            {
                                cellFn: () => {
                                    return this.totalValue[1].kyat;
                                }
                            },
                            {
                                cellFn: () => {
                                    return this.totalValue[1].usd;
                                }
                            },
                        ]
                    ]
                }),
                new FormDummy({ key: 'total_equ_kyat' }),
                new FormDummy({ key: 'total_equ_usd' })
            ]
        }),
        new FormTitle({
            label: 'c. Other sources to finance investment:'
        }),
        new BaseFormGroup({
            key: 'other_source',
            formGroup: [
                new BaseFormArray({
                    key: 'other_source_list',
                    formArray: this.otherSource,
                    useTable: true,
                    tableHeader: [
                        [
                            { label: 'Type', rowSpan: 2 },
                            { label: 'Origin', rowSpan: 2, style: { 'min-width': '200px' } },
                            { label: 'Value', colSpan: 2, style: { 'text-align': 'center' } },
                            { label: '(Equivalent) Kyat', style: { 'text-align': 'center' } },
                            { label: '(Equivalent) US$', style: { 'text-align': 'center' } },
                        ],
                    ],
                    tableFooter: [
                        [
                            { label: 'Total', colSpan: 4, style: { 'text-align': 'center' } },
                            {
                                cellFn: () => {
                                    return this.totalValue[2].kyat;
                                }
                            },
                            {
                                cellFn: () => {
                                    return this.totalValue[2].usd;
                                }
                            },
                        ]
                    ]
                }),
                new FormDummy({ key: 'total_equ_kyat' }),
                new FormDummy({ key: 'total_equ_usd' })
            ]
        }),
        new FormTitle({
            label: 'd. Total:'
        }),
        new BaseFormGroup({
            key: 'total',
            formGroup: [
                new FormTitle({
                    label: 'Myanmar capital',
                }),
                new BaseFormGroup({
                    key: 'myanmar_capital',
                    formGroup: this.capital
                }),
                new FormTitle({
                    label: 'Foreign capital',
                }),
                new BaseFormGroup({
                    key: 'foreign_capital',

                    formGroup: this.capital
                })
            ]
        }),
        new FormTitle({
            label: 'Please upload evidence of the financing of the investment:'
        }),
        new BaseFormArray({
            key: 'finance_evidence_doc',
            formArray: [
                new FormFile({
                    key: 'document',
                    label: 'Name of document:',
                    required: true,
                    multiple: true,
                })
            ]
        })
    ]

    constructor(
        private http: HttpClient,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private store: Store<AppState>,
        private router: Router,
        private toast: ToastrService,
        private lotService: LocationService,
        private activatedRoute: ActivatedRoute,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private localService: LocalService,

    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel?.proposed_invest_basic_info || {}, form?.data?.proposed_invest_basic_info || {});

                        this.page = "mic/sectionC-form7/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.calculateTotalValue("loan_repay", "loan_repay_list", 0);
                        this.calculateTotalValue("issued_share", "issued_share_list", 1);
                        this.calculateTotalValue("other_source", "other_source_list", 2);

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var origin_id_1 = this.loanRepay.filter(x => x.key == "origin_id")[0];
        origin_id_1.options$ = this.localService.getOrigin(lan);

        var origin_id_2 = this.issuedShare.filter(x => x.key == "origin_id")[0];
        origin_id_2.options$ = this.localService.getOrigin(lan);

        var origin_id_3 = this.otherSource.filter(x => x.key == "origin_id")[0];
        origin_id_3.options$ = this.localService.getOrigin(lan);

        this.authService.changeLanguage$.subscribe(x => {
            origin_id_1.options$ = this.localService.getOrigin(x);
            origin_id_2.options$ = this.localService.getOrigin(x);
            origin_id_3.options$ = this.localService.getOrigin(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    updateOrigin(value, index, form, formGroup, control, controlList) {
        let formValue = this.micFormGroup.value;
        formValue[control][controlList][index][form.key] = value;
        this.calculateCapitalValue();
    }

    updateTotal(value, index, form, formGroup, control, controlList) {
        let formValue = this.micFormGroup.value;
        formValue[control][controlList][index][form.key] = value;

        var i = 0;
        if (control == 'loan_repay') {
            i = 0;
        }
        else if (control == 'issued_share') {
            i = 1;
        }
        else if (control == 'other_source') {
            i = 2;
        }
        this.calculateTotalValue(control, controlList, i);
    }

    calculateTotalValue(control, controlList, index) {
        let formValue = this.micFormGroup.value;
        this.totalValue[index].kyat = 0;
        this.totalValue[index].usd = 0;

        formValue[control][controlList].map(x => {
            this.totalValue[index].kyat += parseFloat(x.equ_kyat);
            this.totalValue[index].usd += parseFloat(x.equ_usd);

            this.micFormGroup.get(control).get('total_equ_kyat').setValue(this.totalValue[index].kyat);
            this.micFormGroup.get(control).get('total_equ_usd').setValue(this.totalValue[index].usd);
        });
        this.calculateCapitalValue();
    }

    calculateCapitalValue() {
        // Number(foreign_total_kyat | 0) + Number(x.equ_kyat | 0)
        var foreign_total_kyat = 0;
        var foreign_total_usd = 0;
        var myanmar_total_kyat = 0;
        var myanmar_total_usd = 0;
        this.micFormGroup.get('loan_repay').get('loan_repay_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = Number(foreign_total_kyat | 0) + Number(x.equ_kyat | 0);
                foreign_total_usd = Number(foreign_total_usd | 0) + Number(x.equ_usd | 0);
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = Number(myanmar_total_kyat | 0) + Number(x.equ_kyat | 0);
                myanmar_total_usd = Number(myanmar_total_usd | 0) + Number(x.equ_usd | 0);
            }
        });

        this.micFormGroup.get('issued_share').get('issued_share_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = Number(foreign_total_kyat | 0) + Number(x.equ_kyat | 0);
                foreign_total_usd = Number(foreign_total_usd | 0) + Number(x.equ_usd | 0);
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = Number(myanmar_total_kyat | 0) + Number(x.equ_kyat | 0);
                myanmar_total_usd = Number(myanmar_total_usd | 0) + Number(x.equ_usd | 0);
            }
        });

        this.micFormGroup.get('other_source').get('other_source_list').value.forEach(x => {
            if (x.origin_id === 'Foreign') {
                foreign_total_kyat = Number(foreign_total_kyat | 0) + Number(x.equ_kyat | 0);
                foreign_total_usd = Number(foreign_total_usd | 0) + Number(x.equ_usd | 0);
            }
            if (x.origin_id === 'Myanmar') {
                myanmar_total_kyat = Number(myanmar_total_kyat | 0) + Number(x.equ_kyat | 0);
                myanmar_total_usd = Number(myanmar_total_usd | 0) + Number(x.equ_usd | 0);
            }
        });

        this.micFormGroup.get('total').get('foreign_capital').get('kyat').setValue(foreign_total_kyat | 0);
        this.micFormGroup.get('total').get('foreign_capital').get('usd').setValue(foreign_total_usd | 0);
        this.micFormGroup.get('total').get('myanmar_capital').get('kyat').setValue(myanmar_total_kyat | 0);
        this.micFormGroup.get('total').get('myanmar_capital').get('usd').setValue(myanmar_total_usd | 0);

    }

    calculateEquivalent(value, index, form, formGroup, control, controlList) {
        let formValue = this.micFormGroup.value;
        formValue[control][controlList][index][form.key] = value;

        var i = 0;
        if (control == 'loan_repay') {
            i = 0;
        }
        else if (control == 'issued_share') {
            i = 1;
        }
        else if (control == 'other_source') {
            i = 2;
        }
        this.calculateTotalValue(control, controlList, i);
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'proposed_invest_basic_info': formValue };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });

    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionC-form8', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionC-form8', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionC-form7', id, 'mm']);
                }
            }
        }

    }
}
