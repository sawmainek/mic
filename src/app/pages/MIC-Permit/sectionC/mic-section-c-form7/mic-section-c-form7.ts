import { LocationService } from 'src/services/location.service';
import { LocalService } from 'src/services/local.service';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';

export class MicSectionCForm7 {

    public forms: BaseForm<string>[] = []

    constructor(
        lotService: LocationService,
        localService: LocalService
    ) {
        const loanRepay: BaseForm < string > [] =[
            new FormSelect({
                key: 'origin_id',
                options$: localService.getOrigin(),
                style: {
                    'max-width:': '150px;'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.updateOrigin(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
                }
            }),
            new FormInput({
                key: 'source',
                style: {
                    'min-width': '200px'
                }
            }),
            new FormInput({
                key: 'value',
                type: 'number',
                value: '0',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.updateTotal(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
                }
            }),
            new FormSelect({
                key: 'currency',
                options$: lotService.getCurrency(),
                placeholder: 'Select currency',
            }),
            new FormInput({
                key: 'equ_kyat',
                type: 'number',
                value: '0',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.calculateEquivalent(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
                }
            }),
            new FormInput({
                key: 'equ_usd',
                type: 'number',
                value: '0',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.calculateEquivalent(value, index, form, formGroup, 'loan_repay', 'loan_repay_list');
                }
            }),
        ];
        const issuedShare: BaseForm < string > [] =[
            new FormSelect({
                key: 'origin_id',
                options$: localService.getOrigin(),
                style: {
                    'max-width:': '150px;'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.updateOrigin(value, index, form, formGroup, 'issued_share', 'issued_share_list');
                }
            }),
            new FormInput({
                key: 'value',
                value: '0',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.updateTotal(value, index, form, formGroup, 'issued_share', 'issued_share_list');
                }
            }),
            new FormSelect({
                key: 'currency',
                placeholder: 'Select currency',
                options$: lotService.getCurrency(),
            }),
            new FormInput({
                key: 'equ_kyat',
                type: 'number',
                value: '0',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.calculateEquivalent(value, index, form, formGroup, 'issued_share', 'issued_share_list');
                }
            }),
            new FormInput({
                key: 'equ_usd',
                type: 'number',
                value: '0',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.calculateEquivalent(value, index, form, formGroup, 'issued_share', 'issued_share_list');
                }
            })
        ]
        const otherSource: BaseForm < string > [] =[
            new FormInput({
                key: 'type',
                style: {
                    'min-width': '200px'
                }
            }),
            new FormSelect({
                key: 'origin_id',
                options$: localService.getOrigin(),
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.updateOrigin(value, index, form, formGroup, 'other_source', 'other_source_list');
                }
            }),
            new FormInput({
                key: 'value',
                value: '0',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.updateTotal(value, index, form, formGroup, 'other_source', 'other_source_list');
                }
            }),
            new FormSelect({
                key: 'currency',
                placeholder: 'Select currency',
                options$: lotService.getCurrency(),
            }),
            new FormInput({
                key: 'equ_kyat',
                value: '0',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.calculateEquivalent(value, index, form, formGroup, 'other_source', 'other_source_list');
                }
            }),
            new FormInput({
                key: 'equ_usd',
                value: '0',
                type: 'number',
                style: {
                    'min-width': '150px'
                },
                valueChangeEvent: (value, index, form, formGroup) => {
                    // this.calculateEquivalent(value, index, form, formGroup, 'other_source', 'other_source_list');
                }
            })
        ]
        const capital: BaseForm < string > [] =[
            new FormInput({
                key: 'kyat',
                label: 'Equivalent Kyat',
                columns: 'col-md-6',
                type: 'number',
                value: '0',
                required: true,
                readonly: true,
            }),
            new FormInput({
                key: 'usd',
                label: 'Equivalent US$',
                columns: 'col-md-6',
                type: 'number',
                value: '0',
                required: true,
                readonly: true,
            })
        ]
        this.forms = [
            new BaseFormGroup({
                key: 'proposed_invest_basic_info',
                formGroup: [
                    new FormSectionTitle({
                        label: '3. Particulars of financing of investment',
                        pageBreak: 'before'
                    }),
                    new FormTitle({
                        label: 'a. Loans repayable by investment business to finance investment'
                    }),
                    new BaseFormGroup({
                        key: 'loan_repay',
                        formGroup: [
                            new BaseFormArray({
                                key: 'loan_repay_list',
                                formArray: loanRepay,
                                useTable: true,
                                headerRows: 1,
                                headerWidths: ['20%', '20%', '15%', '15%', '15%','15%'],
                                tablePDFHeader: [
                                    [
                                        { label: 'Origin' },
                                        { label: 'Source (Name of company/individual)', style: { 'min-width': '200px' } },
                                        { label: 'Value', colSpan: 2, style: { 'alignment': 'center' } },
                                        { },
                                        { label: 'Equivalent Kyat', style: { 'alignment': 'center' } },
                                        { label: 'Equivalent US$', style: { 'alignment': 'center' } },
                                    ]
                                ],
                                tablePDFFooter: [
                                    [
                                        { label: 'Total', colSpan: 4, style: { 'text-align': 'center' } },
                                        {}, {}, {},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.map(x => {
                                                    total += Number(x['equ_kyat']) || 0;
                                                })
                                                return total;
                                            }
                                        },
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.map(x => {
                                                    total += Number(x['equ_usd']) || 0;
                                                })
                                                return total;
                                            }
                                        }
                                    ]
                                ],
                                tableHeader: [
                                    [
                                        { label: 'Origin', rowSpan: 2 },
                                        { label: 'Source (Name of company/individual)', rowSpan: 2, style: { 'min-width': '200px' } },
                                        { label: 'Value', colSpan: 2, style: { 'text-align': 'center' } },
                                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                                        { label: 'Equivalent US$', style: { 'text-align': 'center' } },
                                    ],
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Total', colSpan: 4, style: { 'text-align': 'center' } },
                                        {
                                            cellFn: () => {
                                                // return this.totalValue[0].kyat;
                                            }
                                        },
                                        {
                                            cellFn: () => {
                                                // return this.totalValue[0].usd;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormDummy({ key: 'total_equ_kyat' }),
                            new FormDummy({ key: 'total_equ_usd' })
                        ]
                    }),
                    new FormTitle({
                        label: 'b. Issued/paid up share capital to finance investment'
                    }),
                    new BaseFormGroup({
                        key: 'issued_share',
                        formGroup: [
                            new BaseFormArray({
                                key: 'issued_share_list',
                                formArray: issuedShare,
                                useTable: true,
                                headerRows: 1,
                                headerWidths: ['20%', '20%', '20%', '20%', '20%'],
                                tablePDFHeader: [
                                    [
                                        { label: 'Origin' },
                                        { label: 'Value', colSpan: 2, style: { 'alignment': 'center' } },
                                        { },
                                        { label: '(Equivalent) Kyat', style: { 'alignment': 'center' } },
                                        { label: '(Equivalent) US$', style: { 'alignment': 'center' } },
                                    ]
                                ],
                                tablePDFFooter: [
                                    [
                                        { label: 'Total', colSpan: 3, style: { 'text-align': 'center' } },
                                        {}, {},
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.map(x => {
                                                    total += Number(x['equ_kyat']) || 0;
                                                })
                                                return total;
                                            }
                                        },
                                        {
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.map(x => {
                                                    total += Number(x['equ_usd']) || 0;
                                                })
                                                return total;
                                            }
                                        }
                                    ]
                                ],
                                tableHeader: [
                                    [
                                        { label: 'Origin', rowSpan: 2 },
                                        { label: 'Value', colSpan: 2, style: { 'alignment': 'center' } },
                                        { label: '(Equivalent) Kyat', style: { 'alignment': 'center' } },
                                        { label: '(Equivalent) US$', style: { 'alignment': 'center' } },
                                    ],
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Total', colSpan: 3, style: { 'text-align': 'center' } },
                                        {
                                            cellFn: () => {
                                                // return this.totalValue[1].kyat;
                                            }
                                        },
                                        {
                                            cellFn: () => {
                                                // return this.totalValue[1].usd;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormDummy({ key: 'total_equ_kyat' }),
                            new FormDummy({ key: 'total_equ_usd' })
                        ]
                    }),
                    new FormTitle({
                        label: 'c. Other sources to finance investment:'
                    }),
                    new BaseFormGroup({
                        key: 'other_source',
                        formGroup: [
                            new BaseFormArray({
                                key: 'other_source_list',
                                formArray: otherSource,
                                useTable: true,
                                headerRows: 1,
                                headerWidths: ['20%', '20%', '15%', '15%', '15%', '15%'],
                                tablePDFHeader: [
                                    [
                                        { label: 'Type' },
                                        { label: 'Origin', style: { 'width': '200px' } },
                                        { label: 'Value', colSpan: 2, style: { 'alignment': 'center' } },
                                        { },
                                        { label: '(Equivalent) Kyat', style: { 'alignment': 'center' } },
                                        { label: '(Equivalent) US$', style: { 'alignment': 'center' } },
                                    ]
                                ],
                                tablePDFFooter: [
                                    [
                                        { label: 'Total', colSpan: 4, style: { 'text-align': 'center' } },
                                        { }, { }, { },
                                        { 
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.map(x => {
                                                    total += Number(x['equ_kyat']) || 0;
                                                })
                                                return total;
                                            } 
                                        },
                                        { 
                                            cellFn: (data: any[]) => {
                                                let total = 0;
                                                data.map(x => {
                                                    total += Number(x['equ_usd']) || 0;
                                                })
                                                return total;
                                            } 
                                        }
                                    ]
                                ],
                                tableHeader: [
                                    [
                                        { label: 'Type', rowSpan: 2 },
                                        { label: 'Origin', rowSpan: 2, style: { 'width': '200px' } },
                                        { label: 'Value', colSpan: 2, style: { 'alignment': 'center' } },
                                        { label: '(Equivalent) Kyat', style: { 'alignment': 'center' } },
                                        { label: '(Equivalent) US$', style: { 'alignment': 'center' } },
                                    ],
                                ],
                                tableFooter: [
                                    [
                                        { label: 'Total', colSpan: 4, style: { 'text-align': 'center' } },
                                        {
                                            cellFn: () => {
                                                // return this.totalValue[2].kyat;
                                            }
                                        },
                                        {
                                            cellFn: () => {
                                                // return this.totalValue[2].usd;
                                            }
                                        },
                                    ]
                                ]
                            }),
                            new FormDummy({ key: 'total_equ_kyat' }),
                            new FormDummy({ key: 'total_equ_usd' })
                        ]
                    }),
                    new FormTitle({
                        label: 'd. Total:',
                        pageBreak: 'before'
                    }),
                    new BaseFormGroup({
                        key: 'total',
                        formGroup: [
                            new FormTitle({
                                label: 'Myanmar capital',
                            }),
                            new BaseFormGroup({
                                key: 'myanmar_capital',
                                formGroup: capital
                            }),
                            new FormTitle({
                                label: 'Foreign capital',
                            }),
                            new BaseFormGroup({
                                key: 'foreign_capital',
                                formGroup: capital
                            })
                        ]
                    }),
                    new FormTitle({
                        label: 'Please upload evidence of the financing of the investment:'
                    }),
                    new BaseFormArray({
                        key: 'finance_evidence_doc',
                        formArray: [
                            new FormFile({
                                key: 'document',
                                label: 'Name of document:',
                                required: true,
                                multiple: true,
                            })
                        ]
                    })
                ]
            })
            
        ]
    }
}