import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { FormHidden } from './../../../../custom-component/dynamic-forms/base/form-hidden';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MicService } from 'src/services/mic.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Mic } from 'src/app/models/mic';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICPermitSectionC } from '../mic-permit-section-c';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { LocalService } from 'src/services/local.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-c-form8',
    templateUrl: './mic-section-c-form8.component.html',
    styleUrls: ['./mic-section-c-form8.component.scss']
})
export class MicSectionCForm8Component extends MICPermitSectionC implements OnInit {
    @Input() id: any;
    menu: any = "sectionC";
    page = "mic/sectionC-form8/";

    user: any = {};

    view: any;
    hide: any = true;

    micFormGroup: FormGroup;
    micModel: any;
    mm: any;
    form: any;
    service: any;

    classification: any = ['Managers', 'Professionals', 'Technicians and associate professionals', 'Clerical support workers', 'Service and sales workers', 'Skilled agricultural, forestry and fishery workers', 'Craft and related trades workers', 'Plant and machine operators, and assemblers', 'Elementary occupations'];
    classification_mm: any = ['မန်နေဂျာများ/ အဆင့်မြင့်အရာရှိများ', 'သက်မွေးဝမ်းကျောင်းပညာရှင်များ', 'နည်းပညာနှင့်ဆက်စပ်သည့် သက်မွေးပညာရှင်များ', 'အထောက်အပံ့ပြုစာရေးလုပ်သားများ', 'ဝန်ဆောင်မှုနှင့်အရောင်းလုပ်သားများ', 'လယ်ယာသစ်တောနှင့် ငါးလုပ်ငန်းကျွမ်းကျင်လုပ်သားများ', 'လက်မှုပညာနှင့်ဆက်စပ်လုပ်ငန်းအလုပ်သမားများ', 'စက်ကရိယာနှင့်စက်ပစ္စည်းကိုင်တွယ်အသုံးပြုသူများနှင့် စုစည်းတပ်ဆင်သူများ', 'အခြေခံအလုပ်သမားများ'];
    total_max: any = {};
    total_year: any = [];

    spinner = false;
    is_draft = false;
    loading = false;
    next_label = "NEXT SECTION";

    averageSalary: any[] = [];

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            'label': '4. Planned employment:'
        }),
        new FormNote({
            label: 'Investors should classify employment according to the International Standard Classification of Occupations (ISCO), known as ISCO-08, by the International Labor Organization (ILO). According to the Myanmar Investment Law Art. 51, investors shall appoint only Myanmar citizens for works which does not require skill.'
        }),
        new BaseFormArray({
            key: 'emp_max',
            formArray: [
                new FormInput({
                    key: 'classification',
                    required: true,
                    readonly: true
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueMax(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueMax(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueMax(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueMax(value, index, form, formGroup);
                    }
                })
            ],
            useTable: true,
            disableAddRow: true,
            defaultLength: this.classification.length,
            tableHeader: [
                [
                    { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'text-align': 'center', 'vertical-align': 'middle', 'min-width': '400px' } },
                    { label: 'Maximum number of employment created over lifetime of investment business', colSpan: 4, style: { 'text-align': 'center' } }
                ],
                [
                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },
                ],
                [
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total employment and average weighted salary' },
                    {
                        cellFn: () => {
                            return this.total_max.mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_max.mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_max.fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_max.fr_citizens_salary_rate_total;
                        }
                    }
                ]
            ]
        }),
        new BaseFormArray({
            key: 'emp_year',
            formArray: [
                new FormInput({
                    key: 'classification',
                    required: true,
                    readonly: true,
                    style: { 'background': 'white', 'position': 'sticky', 'left': '0px', 'z-index': '999' }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_1',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 1);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_1',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 1);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_1',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 1);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_1',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 1);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_2',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 2);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_2',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 2);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_2',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 2);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_2',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 2);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_3',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 3);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_3',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 3);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_3',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 3);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_3',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 3);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_4',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 4);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_4',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 4);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_4',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 4);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_4',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 4);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_qty_5',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 5);
                    }
                }),
                new FormInput({
                    key: 'myanmar_citizens_salary_rate_5',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 5);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_qty_5',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 5);
                    }
                }),
                new FormInput({
                    key: 'foreign_nationals_salary_rate_5',
                    type: 'number',
                    required: true,
                    value: "0",
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueYear(value, index, form, formGroup, 5);
                    }
                }),
            ],
            useTable: true,
            disableAddRow: true,
            defaultLength: this.classification.length,
            tableHeader: [
                [
                    { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'text-align': 'center', 'vertical-align': 'middle', 'min-width': '400px', 'position': 'sticky', 'left': '0px', 'background': 'white', 'z-index': '9999' } },
                    { label: 'Newly Recruited Staff Year 1', colSpan: 4, style: { 'text-align': 'center' } },
                    { label: 'Newly Recruited Staff Year 2', colSpan: 4, style: { 'text-align': 'center' } },
                    { label: 'Newly Recruited Staff Year 3', colSpan: 4, style: { 'text-align': 'center' } },
                    { label: 'Newly Recruited Staff Year 4', colSpan: 4, style: { 'text-align': 'center' } },
                    { label: 'Newly Recruited Staff Year 5', colSpan: 4, style: { 'text-align': 'center' } },
                ],
                [
                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },

                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },

                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },

                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },

                    { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                    { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },
                ],
                [
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },

                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },

                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },

                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },

                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                    { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                    { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total employment and average weighted salary ', style: { 'background': 'white', 'position': 'sticky', 'left': '0px', 'z-index': '999' } },
                    {
                        cellFn: () => {
                            return this.total_year[0].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[0].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[0].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[0].fr_citizens_salary_rate_total;
                        }
                    },

                    {
                        cellFn: () => {
                            return this.total_year[1].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[1].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[1].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[1].fr_citizens_salary_rate_total;
                        }
                    },

                    {
                        cellFn: () => {
                            return this.total_year[2].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[2].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[2].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[2].fr_citizens_salary_rate_total;
                        }
                    },

                    {
                        cellFn: () => {
                            return this.total_year[3].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[3].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[3].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[3].fr_citizens_salary_rate_total;
                        }
                    },

                    {
                        cellFn: () => {
                            return this.total_year[4].mm_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[4].mm_citizens_salary_rate_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[4].fr_citizens_qty_total;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.total_year[4].fr_citizens_salary_rate_total;
                        }
                    }

                ]
            ]
        }),
        // new FormTitle({
        //     label: 'Average yearly employment over 5-year period:',
        // }),
        // new FormInput({
        //     key: 'myanmar_citizens_avg',
        //     label: 'Myanmar citizens:',
        //     required: true,
        //     columns: 'col-md-6',
        //     readonly: true
        // }),
        // new FormInput({
        //     key: 'foreign_nationals_avg',
        //     label: 'Foreign nationals:',
        //     required: true,
        //     columns: 'col-md-6',
        //     readonly: true
        // }),
        new FormTitle({
            label: 'Please upload plans on social security and welfare plan for employees, including career advancement and employee retirement arrangements:'
        }),
        new FormFile({
            key: 'plan_doc',
            label: 'File Name',
            required: true
        })
    ]

    constructor(
        private http: HttpClient,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private localService: LocalService,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,

    ) {
        super(formCtlService);
        this.loading = true;

        this.total_max.mm_citizens_qty_total = 0;
        this.total_max.mm_citizens_salary_rate_total = 0;
        this.total_max.fr_citizens_qty_total = 0;
        this.total_max.fr_citizens_salary_rate_total = 0;

        for (let i = 0; i < 5; i++) {
            this.total_year.push({
                mm_citizens_qty_total: 0,
                mm_citizens_salary_rate_total: 0,
                fr_citizens_qty_total: 0,
                fr_citizens_salary_rate_total: 0,
            })
        }

        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm ? this.micMMService : this.micService;

            this.next_label = this.mm ? "NEXT SECTION" : "NEXT";

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micModel.employment_plan = this.micModel?.employment_plan || form?.data?.employment_plan || {};
                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel?.employment_plan || {}, form?.data?.employment_plan || {});

                        console.log(this.micModel);

                        this.page = "mic/sectionC-form8/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        if (this.mm == 'mm') {
                            this.classification_mm.forEach((x, i) => {
                                (this.micFormGroup.get('emp_max') as FormArray).controls[i].get('classification').setValue(x);
                                (this.micFormGroup.get('emp_year') as FormArray).controls[i].get('classification').setValue(x);
                            });
                        }
                        else {
                            this.classification.forEach((x, i) => {
                                (this.micFormGroup.get('emp_max') as FormArray).controls[i].get('classification').setValue(x);
                                (this.micFormGroup.get('emp_year') as FormArray).controls[i].get('classification').setValue(x);
                            });
                        }



                        this.calculateTotal();
                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    calculateTotal() {
        this.total_max.mm_citizens_qty_total = 0;
        this.total_max.mm_citizens_salary_rate_total = 0;
        this.total_max.fr_citizens_qty_total = 0;
        this.total_max.fr_citizens_salary_rate_total = 0;

        this.micModel.employment_plan = this.micModel.employment_plan || {};
        this.micModel.employment_plan.emp_max = this.micModel?.employment_plan?.emp_max || [];
        this.micModel.employment_plan.emp_max.forEach((x, i) => {
            this.total_max.mm_citizens_qty_total += Number(x.myanmar_citizens_qty || 0);
            this.total_max.mm_citizens_salary_rate_total += Number(x.myanmar_citizens_salary_rate || 0);
            this.total_max.fr_citizens_qty_total += Number(x.foreign_nationals_qty || 0);
            this.total_max.fr_citizens_salary_rate_total += Number(x.foreign_nationals_salary_rate || 0);
        });

        for (var i = 0; i < 5; i++) {
            this.total_year[i].mm_citizens_qty_total = 0;
            this.total_year[i].mm_citizens_salary_rate_total = 0;
            this.total_year[i].fr_citizens_qty_total = 0;
            this.total_year[i].fr_citizens_salary_rate_total = 0;
        }

        this.micModel.employment_plan.emp_year = this.micModel.employment_plan?.emp_year || [];
        this.micModel.employment_plan.emp_year.forEach(x => {
            for (var i = 0; i < 5; i++) {
                this.total_year[i].mm_citizens_qty_total += Number(x[`myanmar_citizens_qty_${i + 1}`] || 0);
                this.total_year[i].mm_citizens_salary_rate_total += Number(x[`myanmar_citizens_salary_rate_${i + 1}`] || 0);
                this.total_year[i].fr_citizens_qty_total += Number(x[`foreign_nationals_qty_${i + 1}`] || 0);
                this.total_year[i].fr_citizens_salary_rate_total += Number(x[`foreign_nationals_salary_rate_${i + 1}`] || 0);
            }
        });

    }

    updateFormValueMax(value, index, form: BaseForm<string>, formGroup: FormGroup) {
        let formValue = this.micFormGroup.value;
        formValue['emp_max'][index][form.key] = value;

        this.total_max.mm_citizens_qty_total = 0;
        this.total_max.mm_citizens_salary_rate_total = 0;
        this.total_max.fr_citizens_qty_total = 0;
        this.total_max.fr_citizens_salary_rate_total = 0;

        (this.micFormGroup.get('emp_max') as FormArray).controls.forEach((x, i) => {
            this.total_max.mm_citizens_qty_total += Number(x.get('myanmar_citizens_qty').value || 0);
            this.total_max.mm_citizens_salary_rate_total += Number(x.get('myanmar_citizens_salary_rate').value || 0);
            this.total_max.fr_citizens_qty_total += Number(x.get('foreign_nationals_qty').value || 0);
            this.total_max.fr_citizens_salary_rate_total += Number(x.get('foreign_nationals_salary_rate').value || 0);
        });
    }

    updateFormValueYear(value, index, form: BaseForm<string>, formGroup: FormGroup, year) {
        let formValue = this.micFormGroup.value;
        formValue[`emp_year`][index][form.key] = value;

        this.total_year[year - 1].mm_citizens_qty_total = 0;
        this.total_year[year - 1].mm_citizens_salary_rate_total = 0;
        this.total_year[year - 1].fr_citizens_qty_total = 0;
        this.total_year[year - 1].fr_citizens_salary_rate_total = 0;

        (this.micFormGroup.get(`emp_year`) as FormArray).controls.forEach((x, i) => {
            this.total_year[year - 1].mm_citizens_qty_total += Number(x.get(`myanmar_citizens_qty_${year}`).value || 0);
            this.total_year[year - 1].mm_citizens_salary_rate_total += Number(x.get(`myanmar_citizens_salary_rate_${year}`).value || 0);
            this.total_year[year - 1].fr_citizens_qty_total += Number(x.get(`foreign_nationals_qty_${year}`).value || 0);
            this.total_year[year - 1].fr_citizens_salary_rate_total += Number(x.get(`foreign_nationals_salary_rate_${year}`).value || 0);
        });
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {
            let data = { 'employment_plan': formValue };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });

    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionD-form1', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionD-form1', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionC-form8', id, 'mm']);
                }
            }
        }


    }

}
