import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionCForm8Component } from './mic-section-c-form8.component';

describe('MicSectionCForm8Component', () => {
  let component: MicSectionCForm8Component;
  let fixture: ComponentFixture<MicSectionCForm8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionCForm8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionCForm8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
