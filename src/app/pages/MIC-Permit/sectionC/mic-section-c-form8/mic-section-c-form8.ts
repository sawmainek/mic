import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';

export class MicSectionCForm8 {

    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms =[
            new BaseFormGroup({
                key: 'employment_plan',
                formGroup: [
                    new FormSectionTitle({
                        'label': '4. Planned employment:',
                        'pageBreak': 'before'
                    }),
                    new FormNote({
                        label: 'Investors should classify employment according to the International Standard Classification of Occupations (ISCO), known as ISCO-08, by the International Labor Organization (ILO). According to the Myanmar Investment Law Art. 51, investors shall appoint only Myanmar citizens for works which does not require skill.'
                    }),
                    new BaseFormArray({
                        key: 'emp_max',
                        formArray: [
                            new FormInput({
                                key: 'classification',
                                required: true,
                                readonly: true
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_qty',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueMax(value, index, form, formGroup);
                                }
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_salary_rate',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueMax(value, index, form, formGroup);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_qty',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueMax(value, index, form, formGroup);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_salary_rate',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueMax(value, index, form, formGroup);
                                }
                            })
                        ],
                        useTable: true,
                        disableAddRow: true,
                        headerRows: 3,
                        tablePDFHeader: [
                            [
                                { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3 },
                                { label: 'Maximum number of employment created over lifetime of investment business', colSpan: 4, style: { 'alignment': 'center' } },
                                {},
                                {},
                                {}
                            ],
                            [
                                {},
                                { label: 'Myanmar citizens', colSpan: 2, style: { 'alignment': 'center' } },
                                {},
                                { label: 'Foreign nationals', colSpan: 2, style: { 'alignment': 'center' } },
                                {}
                            ],
                            [
                                {},
                                { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                                { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                            ]
                        ],
                        tablePDFFooter: [
                            [
                                { label: 'Total employment and average weighted salary' },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_qty'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_salary_rate'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_qty'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_salary_rate'] || 0);
                                        });
                                        return total;
                                    }
                                },
                            ]
                        ],
                        tableHeader: [
                            [
                                { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'text-align': 'center', 'vertical-align': 'middle', 'min-width': '400px' } },
                                { label: 'Maximum number of employment created over lifetime of investment business', colSpan: 4, style: { 'text-align': 'center' } }
                            ],
                            [
                                { label: 'Myanmar citizens', colSpan: 2, style: { 'text-align': 'center' } },
                                { label: 'Foreign nationals', colSpan: 2, style: { 'text-align': 'center' } },
                            ],
                            [
                                { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                                { label: 'Qty (pax)', style: { 'text-align': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'text-align': 'center' } },
                            ]
                        ],
                        tableFooter: [
                            [
                                { label: 'Total employment and average salary' },
                                {
                                    cellFn: () => {
                                        // return this.total_max.mm_citizens_qty_total;
                                    }
                                },
                                {
                                    cellFn: () => {
                                        // return this.total_max.mm_citizens_salary_rate_total;
                                    }
                                },
                                {
                                    cellFn: () => {
                                        // return this.total_max.fr_citizens_qty_total;
                                    }
                                },
                                {
                                    cellFn: () => {
                                        // return this.total_max.fr_citizens_salary_rate_total;
                                    }
                                }
                            ]
                        ]
                    }),
                    // Year 1
                    new FormTitle({
                        label: 'Newly Recruited Staff Year 1',
                        pageBreak: 'before'
                    }),
                    new BaseFormArray({
                        key: 'emp_year',
                        formArray: [
                            new FormInput({
                                key: 'classification',
                                required: true,
                                readonly: true
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_qty_1',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_salary_rate_1',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_qty_1',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_salary_rate_1',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            })
                        ],
                        useTable: true,
                        disableAddRow: true,
                        headerRows: 3,
                        tablePDFHeader: [
                            [
                                { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'alignment': 'center' } },
                                { label: 'Newly Recruited Staff Year 1', colSpan: 4, style: { 'alignment': 'center' } },
                                {},{},{}
                            ],
                            [
                                {},
                                { label: 'Myanmar citizens', colSpan: 2, style: { 'alignment': 'center' } },
                                {},
                                { label: 'Foreign nationals', colSpan: 2, style: { 'alignment': 'center' } },
                                {}
                            ],
                            [
                                {},
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } },
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } }
                            ]
                        ],
                        tablePDFFooter: [
                            [
                                { label: 'Total employment and average weighted salary ' },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_qty_1'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_salary_rate_1'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_qty_1'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_salary_rate_1'] || 0);
                                        });
                                        return total;
                                    }
                                }
                            ]
                        ]
                    }),
                    // Year 2
                    new FormTitle({
                        label: 'Newly Recruited Staff Year 2',
                        pageBreak: 'before'
                    }),
                    new BaseFormArray({
                        key: 'emp_year',
                        formArray: [
                            new FormInput({
                                key: 'classification',
                                required: true,
                                readonly: true
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_qty_2',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_salary_rate_2',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_qty_2',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_salary_rate_2',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            })
                        ],
                        useTable: true,
                        disableAddRow: true,
                        headerRows: 3,
                        tablePDFHeader: [
                            [
                                { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'alignment': 'center' } },
                                { label: 'Newly Recruited Staff Year 2', colSpan: 4, style: { 'alignment': 'center' } },
                                {}, {}, {}
                            ],
                            [
                                {},
                                { label: 'Myanmar citizens', colSpan: 2, style: { 'alignment': 'center' } },
                                {},
                                { label: 'Foreign nationals', colSpan: 2, style: { 'alignment': 'center' } },
                                {}
                            ],
                            [
                                {},
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } },
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } }
                            ]
                        ],
                        tablePDFFooter: [
                            [
                                { label: 'Total employment and average weighted salary ' },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_qty_2'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_salary_rate_2'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_qty_2'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_salary_rate_2'] || 0);
                                        });
                                        return total;
                                    }
                                }
                            ]
                        ]
                    }),
                    // Year 3
                    new FormTitle({
                        label: 'Newly Recruited Staff Year 3',
                        pageBreak: 'before'
                    }),
                    new BaseFormArray({
                        key: 'emp_year',
                        formArray: [
                            new FormInput({
                                key: 'classification',
                                required: true,
                                readonly: true
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_qty_3',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_salary_rate_3',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_qty_3',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_salary_rate_3',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            })
                        ],
                        useTable: true,
                        disableAddRow: true,
                        headerRows: 3,
                        tablePDFHeader: [
                            [
                                { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'alignment': 'center' } },
                                { label: 'Newly Recruited Staff Year 3', colSpan: 4, style: { 'alignment': 'center' } },
                                {}, {}, {}
                            ],
                            [
                                {},
                                { label: 'Myanmar citizens', colSpan: 2, style: { 'alignment': 'center' } },
                                {},
                                { label: 'Foreign nationals', colSpan: 2, style: { 'alignment': 'center' } },
                                {}
                            ],
                            [
                                {},
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } },
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } }
                            ]
                        ],
                        tablePDFFooter: [
                            [
                                { label: 'Total employment and average weighted salary ' },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_qty_3'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_salary_rate_3'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_qty_3'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_salary_rate_3'] || 0);
                                        });
                                        return total;
                                    }
                                }
                            ]
                        ]
                    }),
                    // Year 4
                    new FormTitle({
                        label: 'Newly Recruited Staff Year 4',
                        pageBreak: 'before'
                    }),
                    new BaseFormArray({
                        key: 'emp_year',
                        formArray: [
                            new FormInput({
                                key: 'classification',
                                required: true,
                                readonly: true
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_qty_4',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_salary_rate_4',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_qty_4',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_salary_rate_4',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            })
                        ],
                        useTable: true,
                        disableAddRow: true,
                        headerRows: 3,
                        tablePDFHeader: [
                            [
                                { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'alignment': 'center' } },
                                { label: 'Newly Recruited Staff Year 4', colSpan: 4, style: { 'alignment': 'center' } },
                                {}, {}, {}
                            ],
                            [
                                {},
                                { label: 'Myanmar citizens', colSpan: 2, style: { 'alignment': 'center' } },
                                {},
                                { label: 'Foreign nationals', colSpan: 2, style: { 'alignment': 'center' } },
                                {}
                            ],
                            [
                                {},
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } },
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } }
                            ]
                        ],
                        tablePDFFooter: [
                            [
                                { label: 'Total employment and average weighted salary ' },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_qty_4'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_salary_rate_4'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_qty_4'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_salary_rate_4'] || 0);
                                        });
                                        return total;
                                    }
                                }
                            ]
                        ]
                    }),
                    // Year 5
                    new FormTitle({
                        label: 'Newly Recruited Staff Year 5',
                        pageBreak: 'before'
                    }),
                    new BaseFormArray({
                        key: 'emp_year',
                        formArray: [
                            new FormInput({
                                key: 'classification',
                                required: true,
                                readonly: true
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_qty_5',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'myanmar_citizens_salary_rate_5',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_qty_5',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            }),
                            new FormInput({
                                key: 'foreign_nationals_salary_rate_5',
                                type: 'number',
                                required: true,
                                value: "0",
                                valueChangeEvent: (value, index, form, formGroup) => {
                                    // this.updateFormValueYear(value, index, form, formGroup, 1);
                                }
                            })
                        ],
                        useTable: true,
                        disableAddRow: true,
                        headerRows: 3,
                        tablePDFHeader: [
                            [
                                { label: 'Employment Classification (according to ISCO-08)', rowSpan: 3, style: { 'alignment': 'center' } },
                                { label: 'Newly Recruited Staff Year 5', colSpan: 4, style: { 'alignment': 'center' } },
                                {}, {}, {}
                            ],
                            [
                                {},
                                { label: 'Myanmar citizens', colSpan: 2, style: { 'alignment': 'center' } },
                                {},
                                { label: 'Foreign nationals', colSpan: 2, style: { 'alignment': 'center' } },
                                {}
                            ],
                            [
                                {},
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } },
                                { label: 'Qty (pax)', style: { 'alignment': 'center' } },
                                { label: 'Average Rate of Salary (Kyat)', style: { 'alignment': 'center' } }
                            ]
                        ],
                        tablePDFFooter: [
                            [
                                { label: 'Total employment and average weighted salary ' },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_qty_5'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['myanmar_citizens_salary_rate_5'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_qty_5'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.forEach(x => {
                                            total += Number(x['foreign_nationals_salary_rate_5'] || 0);
                                        });
                                        return total;
                                    }
                                }
                            ]
                        ]
                    }),
                    // new FormTitle({
                    //     label: 'Average yearly employment over 5-year period:',
                    // }),
                    // new FormInput({
                    //     key: 'myanmar_citizens_avg',
                    //     label: 'Myanmar citizens:',
                    //     required: true,
                    //     columns: 'col-md-6',
                    //     readonly: true
                    // }),
                    // new FormInput({
                    //     key: 'foreign_nationals_avg',
                    //     label: 'Foreign nationals:',
                    //     required: true,
                    //     columns: 'col-md-6',
                    //     readonly: true
                    // }),
                    new FormTitle({
                        label: 'Please upload plans on social security and welfare plan for employees, including career advancement and employee retirement arrangements:',
                        pageBreak: 'before'
                    }),
                    new FormFile({
                        key: 'plan_doc',
                        label: 'File Name',
                        required: true
                    })
                ]
            })
            
        ]
    }
}