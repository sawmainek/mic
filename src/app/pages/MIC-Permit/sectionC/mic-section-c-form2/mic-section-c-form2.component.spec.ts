import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionCForm2Component } from './mic-section-c-form2.component';

describe('MicSectionCForm2Component', () => {
  let component: MicSectionCForm2Component;
  let fixture: ComponentFixture<MicSectionCForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionCForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionCForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
