import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FileUploadService } from 'src/services/file_upload.service';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { MICPermitSectionC } from '../mic-permit-section-c';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { FormGroup } from '@angular/forms';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { LocalService } from 'src/services/local.service';

@Component({
    selector: 'app-mic-section-c-form2',
    templateUrl: './mic-section-c-form2.component.html',
    styleUrls: ['./mic-section-c-form2.component.scss']
})
export class MicSectionCForm2Component extends MICPermitSectionC implements OnInit {
    @Input() id: any;
    page = "mic/sectionC-form2/";
    mm: any;

    user: any = {};

    micFormGroup: FormGroup;
    micModel: any;
    form: any;
    service: any;

    submit = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    micForm: BaseForm<any>[] = [
        new BaseFormGroup({
            key: 'timeline',
            formGroup: [
                new FormSectionTitle({
                    label: '1. (Expected) timeline of investment'
                }),
                new FormInput({
                    key: 'construction_date_no',
                    type: 'number',
                    required: true,
                    label: 'Expected construction or preparatory period',
                    columns: 'col-md-6 d-flex',
                }),
                new FormSelect({
                    key: 'construction_date_label',
                    required: true,
                    options$: this.localService.getDate(),
                    columns: 'col-md-3 d-flex',
                    value: 'Months'
                }),
                new FormInput({
                    key: 'commercial_date_no',
                    type: 'number',
                    required: true,
                    label: 'Expected commercial operation period/income tax exemption period',
                    columns: 'col-md-6 d-flex',
                }),
                new FormSelect({
                    key: 'commercial_date_label',
                    required: true,
                    options$: this.localService.getDate(),
                    columns: 'col-md-3 d-flex',
                    value: 'Months'
                }),
                new FormInput({
                    key: 'investment_start_date',
                    type: 'number',
                    endfix: 'Years',
                    label: 'Expected investment period',
                    required: true,
                }),
                new FormTitle({
                    label: 'Will you start the construction period right after obtaining the permit/endorsement?',
                    required: true
                }),
                new FormRadio({
                    key: 'start_after_obtain',
                    label: 'Yes',
                    value: 'Yes',
                    required: true,
                    columns: 'col-md-4'
                }),
                new FormRadio({
                    key: 'start_after_obtain',
                    label: 'No',
                    value: 'No',
                    required: true,
                    columns: 'col-md-8'
                }),
                new FormInput({
                    key: 'reason',
                    label: 'If “No”, please describe the reasons or other necessary process you need to start for construction period?',
                    required: true,
                    criteriaValue: {
                        key: 'start_after_obtain',
                        value: 'No'
                    }
                }),
                new FormTextArea({
                    key: 'elaborate',
                    label: 'If necessary, please elaborate:'
                }),
                new FormTitle({
                    label: 'If necessary, upload supporting documents here:'
                }),
                new BaseFormArray({
                    key: 'documents',
                    formArray: [
                        new FormFile({
                            key: 'supporting_doc',
                            label: 'Name of document:',
                            columns: 'col-md-9',
                            multiple: true,
                        }),
                    ],
                    defaultLength: 1
                }),
                new FormNote({
                    label: 'The Myanmar Investment Rules 107, 139 and 146 give guidance on the definitions of the construction/preparatory period and commencement of commercial operation.',
                })
            ]
        })
    ]

    constructor(
        private router: Router,
        private micService: MicService,
        private activatedRoute: ActivatedRoute,
        public location: Location,
        private store: Store<AppState>,
        private toast: ToastrService,
        public formCtlService: FormControlService,
        private formService: MICFormService,
        private micMMService: MICMMService,
        private translateService: TranslateService,
        private localService: LocalService,
    ) {
        super(formCtlService);
        this.loading = true;
        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm == 'mm' ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, this.micModel, form.data || {});

                        this.page = "mic/sectionC-form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveMic();
    }

    onSubmit() {
        this.submit = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;

        this.saveMic();
    }

    saveMic() {
        this.formCtlService.lazyUpload(this.micForm, this.micFormGroup, { type_id: this.id }, (formValue) => {

            this.service.create({ ...this.micModel, ...formValue }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionC-form3', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionC-form3', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionC-form2', id, 'mm']);
                }
            }
        }
    }
}
