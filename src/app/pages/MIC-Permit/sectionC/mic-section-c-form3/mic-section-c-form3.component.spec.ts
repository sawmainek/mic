import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionCForm3Component } from './mic-section-c-form3.component';

describe('MicSectionCForm3Component', () => {
  let component: MicSectionCForm3Component;
  let fixture: ComponentFixture<MicSectionCForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionCForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionCForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
