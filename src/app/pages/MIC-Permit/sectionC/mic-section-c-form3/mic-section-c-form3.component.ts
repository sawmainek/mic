import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { cloneDeep } from 'lodash';
import { LocationService } from './../../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { MicService } from 'src/services/mic.service';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormBuilder, FormArray, Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { Mic } from 'src/app/models/mic';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { ExpectedInvestment } from 'src/app/models/expected_invest';
import { MICPermitSectionC } from '../mic-permit-section-c';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { MICFormService } from 'src/services/micform.service';
import { MICMMService } from 'src/services/micmm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-c-form3',
    templateUrl: './mic-section-c-form3.component.html',
    styleUrls: ['./mic-section-c-form3.component.scss']
})
export class MicSectionCForm3Component extends MICPermitSectionC implements OnInit {
    menu: any = "sectionC";
    page = "mic/sectionC-form3/";
    @Input() id: any;
    mm: any;

    user: any = {};
    is_officer: boolean = false;

    currencies: any;

    micFormGroup: FormGroup;
    micExpectedInvestment: FormGroup;
    micFormGroupFile: FormGroup;
    micModel: Mic;
    form: any;
    service: any;
    rowNumbers: any;

    submitted: boolean = false;
    isSubmit = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    micForm: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '2. Expected investment value'
        }),
        new FormSectionTitle({
            label: 'a . Expected investment value'
        }),
        new FormTitle({
            label: 'Please insert exchange rate of'
        }),
        new FormInput({
            key: 'exchange_usd',
            value: '0',
            label: '1 USD in Kyat:',
            type: 'number',
            required: true,
            columns: 'col-md-6 d-flex'
        }),
        new FormDate({
            key: 'exchange_date',
            label: 'Date of exchange rate:',
            required: true,
            columns: 'col-md-6 d-flex'
        })
    ]
    micFormFile: BaseForm<string>[] = [
        new FormTitle({
            label: 'Please upload an explanation of the method of valuation of the investment items:'
        }),
        new FormFile({
            key: 'explanation_doc',
            label: 'File Name',
            required: true
        })
    ];

    constructor(
        private http: HttpClient,
        private ref: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private lotService: LocationService,
        private toast: ToastrService,
        public location: Location,
        private formService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;

        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;

        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm);

        this.lotService.getCurrency().subscribe(data => {
            this.currencies = data;
        });

        this.micFormGroupFile = this.formCtlService.toFormGroup(this.micFormFile);
        this.micExpectedInvestment = this.formBuilder.group({
            // currency_type_id: ['', Validators.required], // Set Default Myanmar Currency
            expected_list: new FormArray(
                [
                    this.formBuilder.group({
                        name: ['Cash and equivalents'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]

                    }),
                    this.formBuilder.group({
                        name: ['Furniture & fixtures'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]
                    }),
                    this.formBuilder.group({
                        name: ['Machinery & equipment'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]
                    }),
                    this.formBuilder.group({
                        name: ['Land and Building'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]
                    }),
                    this.formBuilder.group({
                        name: ['Raw materials, including construction materials'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]
                    }),
                    this.formBuilder.group({
                        name: ['Vehicles'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]
                    }),
                    this.formBuilder.group({
                        name: ['Intellectual property rights under any laws, including technical knowhow, patent, industrial designs, and trademarks'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]
                    }),
                    this.formBuilder.group({
                        name: ['Other items'],
                        amount_kyat: ['0', Validators.required],
                        amount_usd: ['0', Validators.required]
                    }),
                ],
                [Validators.required]),
            spent_amount: ['0', [Validators.required]],
            spent_amount_usd: ['0'],
            spent_amount_kyat: ['0'],
            percent_construction: ['0', [Validators.required, Validators.min(0), Validators.max(100)]],
            percent_commercial: ['0', [Validators.required, Validators.min(0), Validators.max(100)]],
        });
        this.rowNumbers = ['i.', 'ii.', 'iii.', 'iv.', 'v.', 'vi.', 'vii.', 'viii.', 'ix.', 'x.', 'xi.', 'xii.', 'xiii.', 'xiv.', 'xv.', 'xvi.', 'xvii.', 'xviii.', 'xix.', 'xx.', 'xxi.', 'xxii.', 'xxiii.', 'xxiv.', 'xxv.', 'xxvi.', 'xxvii.', 'xxviii.', 'xxix.', 'xxx.'];

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.micMMService : this.micService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        let expectedInvest = this.micModel?.expected_investment || form?.data?.expected_investment || new ExpectedInvestment;

                        this.micFormGroup = this.formCtlService.toFormGroup(this.micForm, expectedInvest);
                        this.micFormGroupFile = this.formCtlService.toFormGroup(this.micFormFile, expectedInvest);

                        this.page = "mic/sectionC-form3/";
                        this.page += this.mm && this.mm == 'mm' ? this.micModel.id + '/mm' : this.micModel.id;

                        this.formCtlService.getComments$('MIC Permit', this.micModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.micModel?.id, type: 'MIC Permit', page: this.page, comments }));
                        });

                        if (expectedInvest?.expected_list && expectedInvest.expected_list.length > 0) {
                            this.expected_list.clear();
                            expectedInvest?.expected_list?.forEach(x => {
                                this.addRow();
                                this.micExpectedInvestment.patchValue(this.micModel?.expected_investment || form?.data?.expected_investment);
                            });
                        }

                        this.micFormGroup.get('exchange_usd').valueChanges.subscribe(val => {
                            this.change1USDKyat(val);
                        });

                        this.micExpectedInvestment.get('percent_construction').valueChanges.subscribe(val => {
                            this.calculatePercentage();
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getExpectedValue(index) {
        return (this.micExpectedInvestment.get('expected_list') as FormArray).controls[index];
    }

    calculatePercentage() {
        var spent_amount = Number(this.micExpectedInvestment.get('spent_amount').value)
        var percetage = (Number(this.micExpectedInvestment.get('percent_construction').value) / 100) * spent_amount;
        this.micExpectedInvestment.get('percent_commercial').setValue(spent_amount - percetage);
    }

    changeRowKyat(index) {
        var amount = Number(this.getExpectedValue(index).get('amount_kyat').value || 0);
        var exchange_usd = Number(this.micFormGroup.get('exchange_usd').value || 0);
        var usd = 0;

        if (amount > 0 && exchange_usd > 0) {
            usd = Math.round(amount / exchange_usd);
        }
        else {
            usd = 0;
        }

        this.getExpectedValue(index).get('amount_usd').setValue(usd);
    }

    change1USDKyat(value) {

        // this.micExpectedInvestment.get('percent_construction').setValue(0);
        // this.micExpectedInvestment.get('percent_commercial').setValue(0);
        (this.micExpectedInvestment.get('expected_list') as FormArray).controls.forEach((element, i) => {
            this.changeRowKyat(i);
        });
    }

    getTotalMillionAmoutUSD() {
        let amount_usd: number = 0;
        for (let i = 0; i < this.expected_list.length; i++) {
            amount_usd += Number(this.expected_list.controls[i].get('amount_usd').value);
        }
        this.micExpectedInvestment.get('spent_amount_usd').setValue(amount_usd);
        return amount_usd;
    }

    getPercentCommercial() {
        var percent_construction = Number(this.micExpectedInvestment.get('percent_construction').value)
        var percent_commercial = 100 - percent_construction;
        this.micExpectedInvestment.get('percent_commercial').setValue(percent_commercial);
        return percent_commercial;
    }

    getTotalMillionAmoutKyat() {
        let amount_kyat: number = 0;
        for (let i = 0; i < this.expected_list.length; i++) {
            amount_kyat += Number(this.expected_list.controls[i].get('amount_kyat').value);
        }
        this.micExpectedInvestment.get('spent_amount_kyat').setValue(amount_kyat.toFixed(4));
        return amount_kyat;
    }

    getPercentConstruction() {
        return this.micExpectedInvestment.get('percent_construction').value;
    }

    createmicFormGroup() {
        return this.formBuilder.group({
            name: ['', Validators.required],
            amount_kyat: ['0', Validators.required],
            amount_usd: ['0', Validators.required]
        })
    }

    addRow() {
        let fg = this.createmicFormGroup();
        this.expected_list.push(fg);
        this.ref.detectChanges();
    }

    removeRow(index) {
        this.expected_list.removeAt(index);
        this.ref.detectChanges();
    }

    onDraftSave() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.submitted = true;
        this.isSubmit = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.micFormGroup.invalid || this.micExpectedInvestment.invalid || this.micFormGroupFile.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.micFormFile, this.micFormGroupFile, { type_id: this.id }, (formValue) => {
            let data = { 'expected_investment': { ...this.micFormGroup.value, ...this.micExpectedInvestment.value, ...formValue } };
            this.service.create({ ...this.micModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.micModel.id, this.page);
                        this.redirectLink(this.form?.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                }, error => {
                    console.log(error);
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/mic/sectionC-form4', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/mic/sectionC-form4', id]);
                } else {
                    this.router.navigate([page + '/mic/sectionC-form3', id, 'mm']);
                }
            }
        }
    }

    get expected_list(): FormArray {
        return this.micExpectedInvestment.get('expected_list') as FormArray;
    }

    get validation() {
        return this.micExpectedInvestment.controls;
    }
}
