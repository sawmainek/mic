import { FormInput } from './../../../../custom-component/dynamic-forms/base/form-input';
import { BaseFormArray } from './../../../../custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from './../../../../custom-component/dynamic-forms/base/form-group';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class MicSectionCForm3 {

    public forms: BaseForm<string>[] = []

    constructor(

    ) {

        this.forms = [
            new BaseFormGroup({
                key: 'expected_investment',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. Expected investment value',
                    }),
                    new FormSectionTitle({
                        label: 'a . Expected investment value'
                    }),
                    new FormTitle({
                        label: 'Please insert exchange rate of'
                    }),
                    new FormInput({
                        key: 'exchange_usd',
                        value: '0',
                        label: '1 USD in Kyat:',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6 d-flex'
                    }),
                    new FormDate({
                        key: 'exchange_date',
                        label: 'Date of exchange rate:',
                        required: true,
                        columns: 'col-md-6 d-flex'
                    }),
                    new BaseFormArray({
                        key: 'expected_list',
                        formArray: [
                            new FormInput({
                                key: 'name',
                            }),
                            new FormInput({
                                key: 'amount_kyat',
                            }),
                            new FormInput({
                                key: 'amount_usd',
                            })
                        ],
                        useTable: true,
                        tablePDFHeader: [
                            [
                                {},
                                { label: 'Total Amount in Myanmar Kyat', style: {'alignment': 'center'} },
                                { label: 'Equivalent Amount in USD', style: {'alignment': 'center'} }
                            ]
                        ],
                        tablePDFFooter: [
                            [
                                { label: '(Expected) investment value' },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.map(x => {
                                            total += Number(x['amount_kyat'] || 0);
                                        });
                                        return total;
                                    }
                                },
                                {
                                    cellFn: (data: any[]) => {
                                        let total = 0;
                                        data.map(x => {
                                            total += Number(x['amount_usd'] || 0);
                                        });
                                        return total;
                                    }
                                }
                            ]
                        ]
                    }),
                    new FormInput({
                        key: 'percent_construction',
                        label: 'Percentage (%) of (expected) investment value during the construction or preparatory period'
                    }),
                    new FormInput({
                        key: 'percent_commercial',
                        label: 'Percentage (%) of (expected) investment value during the commercial operation period'
                    }),
                    new FormTitle({
                        label: 'Please upload an explanation of the method of valuation of the investment items:',
                        pageBreak: 'before'
                    }),
                    new FormFile({
                        key: 'explanation_doc',
                        label: 'File Name',
                        required: true
                    })

                ]
            }),
            
        ]
    }
}