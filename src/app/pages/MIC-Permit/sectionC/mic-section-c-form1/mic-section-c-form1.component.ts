import { cloneDeep } from 'lodash';
import { CurrentForm } from './../../../../core/form/_actions/form.actions';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Mic } from 'src/app/models/mic';
import { MicService } from 'src/services/mic.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MICPermitSectionC } from '../mic-permit-section-c';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormData } from 'src/app/core/form';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core';
import { MICFormService } from 'src/services/micform.service';
import { FormProgressService } from 'src/services/form-progress.service';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-mic-section-c-form1',
    templateUrl: './mic-section-c-form1.component.html',
    styleUrls: ['./mic-section-c-form1.component.scss']
})
export class MicSectionCForm1Component extends MICPermitSectionC implements OnInit {
    menu: any = "sectionC";
    @Input() id: any;
    mm: any;

    // model: any = {};
    micModel: Mic;
    micMMModel: Mic;
    form: any; //For New Flow

    progressLists: any[] = [];
    pages: any[] = [];
    loading = false;

    constructor(
        private formService: MICFormService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private router: Router,
        private store: Store<AppState>,
        private translateService: TranslateService,
    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            if (this.id > 0) {
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.micModel = this.form?.data || {};
                        this.micMMModel = this.form?.data_mm || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.getFormProgress();

                        this.changeLanguage();
                        this.loading = false;
                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            `mic/sectionC-form2`,
            `mic/sectionC-form3`,
            `mic/sectionC-form4`,
            `mic/sectionC-form5`,
            `mic/sectionC-form6`,
            `mic/sectionC-form7`,
            `mic/sectionC-form8`
        ];
        forkJoin([
            this.formProgressService.get({
                rows: 999,
                search: `type_id:equal:${this.micModel?.id || 0}|type:equal:MIC-Permit` // For New Flow this.id => this.micModel.id
            }),
            this.formProgressService.get({
                rows: 999,
                search: `type_id:equal:${this.micMMModel?.id || 0}|type:equal:MIC-Permit` // For New Flow this.id => this.micMMModel.id
            })
        ]).subscribe((x: any[]) => {
            this.progressLists = [...x[0], ...x[1]];
            this.loading = false;
        });
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.micModel.id}`).length;
            const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.micMMModel.id}/mm`).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    openPage(link) {
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/officer/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/officer/' + link, this.id]);
            }
        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/pages/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/pages/' + link, this.id]);
            }
        }
    }
}
