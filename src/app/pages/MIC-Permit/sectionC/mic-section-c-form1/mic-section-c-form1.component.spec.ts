import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicSectionCForm1Component } from './mic-section-c-form1.component';

describe('MicSectionCForm1Component', () => {
  let component: MicSectionCForm1Component;
  let fixture: ComponentFixture<MicSectionCForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicSectionCForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicSectionCForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
