import { FormProgress } from './../../../core/form/_actions/form.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../../../core/reducers/index';
import { TranslateService } from '@ngx-translate/core';
import { MICFormService } from './../../../../services/micform.service';
import { FormService } from './../../../../services/form.service';
import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { Mic } from 'src/app/models/mic';
import { MicService } from 'src/services/mic.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MICChooseLanguage } from './mic-choose-language';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { MICMMService } from 'src/services/micmm.service';
import { forkJoin } from 'rxjs';
import { FormProgressService } from 'src/services/form-progress.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-mic-choose-language',
    templateUrl: './mic-choose-language.component.html',
    styleUrls: ['./mic-choose-language.component.scss']
})
export class MicChooseLanguageComponent extends MICChooseLanguage implements OnInit {
    @Input() id: any;
    menu: any = "choose-language";
    page = `mic/choose-language`;
    language: any = "English";

    user: any = {};
    micModel: Mic;
    micMMModel: Mic;
    form: any = {};

    loading = false;
    spinner: boolean = false;
    isDraft: boolean = false;

    constructor(
        private router: Router,
        private authService: AuthService,
        private formService: MICFormService, //For New Flow
        private micService: MicService,
        private store: Store<AppState>,
        private micMMService: MICMMService,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private translateService: TranslateService,
        private activatedRoute: ActivatedRoute,
        private toast: ToastrService
    ) {
        super(formCtlService);
        this.language = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.language ? this.language : 'en');
        this.translateService.use(this.language ? this.language : 'en');
    }

    ngOnInit(): void {
        this.authService.getCurrentUser().subscribe(result => {
            this.user = result || null;
        });
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            if (this.id > 0) {
                this.formService.show(this.id)
                    .subscribe(result => {
                        this.form = result || {};
                        this.language = this.form?.language || 'English'
                        this.openLanguage(this.language || 'English');
                        this.micModel = this.form?.data || {};
                        this.micMMModel = this.form?.data_mm || {};

                        this.loading = false;
                    });
            } else {
                this.loading = false;
                this.language = 'English';
                this.openLanguage(this.language || 'English');
            }
        }).unsubscribe();

        
    }

    openLanguage(language) {
        if (window.location.href.indexOf('officer') == -1) {
            this.language = language;
        }
    }

    goToHome() {
        this.router.navigate(['/']);
    }

    getProgressData(form_id, model_id) {
        this.formProgressService.get({
            page: 1,
            rows: 999,
            search: `form_id:equal:${form_id || 0}|type:equal:MIC-Permit`
        }).subscribe((result: any) => {
            this.getProgressForms(result || []);
            this.store.dispatch(new FormProgress({progress: result}));
            this.saveFormProgress(form_id, model_id, this.page);
        });
    }

    onSubmit() {
        this.spinner = true;
        //For New Flow
        this.form.type = 'MIC Permit';
        this.form.user_id = this.form?.user_id || this.user.id;
        this.form.language = this.language;
        this.form.status = this.form.status || 'Draft';
        
        this.formService.create(this.form)
            .subscribe(result => {
                this.form = result;
                if (this.language == 'Myanmar') {
                    this.micMMModel = { ...this.micMMModel || {}, ...{ user_id: this.form?.user_id || this.user.id, choose_language: this.language, form_id: this.form?.id } } as Mic;
                    this.micMMService.create(this.micMMModel).subscribe(result => {
                        this.form.data_mm = result;

                        this.page = `mic/choose-language/${result.id}/mm`;
                        this.getProgressData(this.form.id, result.id);

                        this.redirectLink(this.form?.id);
                    })
                } else {
                    // For both English / Myanmar
                    this.micModel = { ...this.micModel || {}, ...{ user_id: this.form?.user_id || this.user.id, choose_language: this.language, form_id: this.form?.id } } as Mic;
                    this.micMMModel = { ...this.micMMModel || {}, ...{ user_id: this.form?.user_id || this.user.id, choose_language: this.language, form_id: this.form?.id } } as Mic;
                    forkJoin([
                        this.micService.create(this.micModel),
                        this.micMMService.create(this.micMMModel)
                    ])
                        .subscribe(results => {
                            this.page = `mic/choose-language/${results[1].id}`;
                            this.getProgressData(this.form.id, results[1].id);

                            this.redirectLink(this.form?.id);
                        })
                }
            })
    }

    redirectLink(id: number) {
        if(id == null) {
            this.toast.error("Please login with investor account.");
            return;
        }
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/officer/mic/sectionA-form1', id || this.form?.id, 'mm']);
            }
            else {
                this.router.navigate(['/officer/mic/sectionA-form1', id]);
            }

        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/pages/mic/sectionA-form1', id, 'mm']);
            }
            else {
                this.router.navigate(['/pages/mic/sectionA-form1', id]);
            }
        }
    }

}
