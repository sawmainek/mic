import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicChooseLanguageComponent } from './mic-choose-language.component';

describe('MicChooseLanguageComponent', () => {
  let component: MicChooseLanguageComponent;
  let fixture: ComponentFixture<MicChooseLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicChooseLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicChooseLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
