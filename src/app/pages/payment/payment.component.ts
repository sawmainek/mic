import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Form } from 'src/app/models/form';
import { SubmissionLandRight } from 'src/app/models/submission_land_right';
import { AuthService } from 'src/services/auth.service';
import { FormService } from 'src/services/form.service';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { LandRightService } from 'src/services/land_right.service';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
    id: any = undefined;
    land_rightModel: any = {};

    user: any = {};
    formModel: Form;
    landRightSubmissionModel: SubmissionLandRight;

    spinner = false;
    is_mpu = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private service: LandRightService,
        private authService: AuthService,
        private formService: FormService,
        private landRightSubmissionService: LandrightSubmissionService,
        private router: Router,
    ) {
        this.getLandRightData();
        this.getFormSubmissionData();
    }

    ngOnInit(): void { }

    getLandRightData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id');
            if (this.land_rightModel) {
                this.service.show(this.id)
                    .subscribe(x => {
                        this.land_rightModel = x;
                    });
            }
        });
    }

    getFormSubmissionData() {
        this.authService.getCurrentUser().subscribe(result => {
            this.user = result;
            if (this.user) {
                let params: any = {
                    page: 1,
                    search: `user_id:equal:${this.user.id}|type:equal:Land Right`,
                    secure: true,
                };

                // Get Form
                this.formService.get(params).subscribe((results: any) => {
                    if (results.length > 0) {
                        this.formModel = results[0] || null;
                    } else {
                        this.formModel = new Form;
                        this.formModel.user_id = this.formModel.user_id || this.user.id;
                        this.formModel.type = "Land Right";
                        this.formModel.status = 0;
                    }
                });

                params = {
                    page: 1,
                    search: `submitted_user_id:equal:${this.user.id}`,
                    secure: true,
                };
                // Get Land Right Submission
                this.landRightSubmissionService.get(params).subscribe((results: any) => {
                    if (results.length > 0) {
                        this.landRightSubmissionModel = results[0] || {};
                    } else {
                        this.landRightSubmissionModel = new SubmissionLandRight;
                        this.landRightSubmissionModel.submitted_user_id = this.landRightSubmissionModel.submitted_user_id || this.user.id;
                        this.landRightSubmissionModel.application_no = "111222";
                        this.landRightSubmissionModel.status = 0;
                    }
                });
            }
        }, err => {
            console.log(err);
        });
    }

    saveCredit() {
        this.is_mpu = false;
        this.saveFormData();
    }

    saveMPU() {
        this.is_mpu = true;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        let data = {};

        // Save Form Table
        data = { 'data': this.land_rightModel };
        this.formModel = { ...this.formModel, ...data };
        this.formService.create(this.formModel).subscribe(y => {

            // Save Submission Table
            data = { 'form_id': y.id };
            this.landRightSubmissionModel = { ...this.landRightSubmissionModel, ...data };
            this.landRightSubmissionService.create(this.landRightSubmissionModel).subscribe(z => {
                console.log(this.land_rightModel)
                // application_no
                // permit_endorsement_no
                // investment_enterprise_name
                // submitted_to
                this.spinner = false;
                this.is_mpu = false;
                this.router.navigate(['/pages/land-right/success', this.id]);
            });

        });
    }

}
