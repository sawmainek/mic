import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

export class LandRightForm4 {

    public forms: BaseForm<string>[] = [];
    index = 0;

    constructor(
        private lotService: LocationService,
        private localService: LocalService,
    ) {
        let individual: BaseForm < string > [] =[
            new FormInput({
                key: 'full_name',
                label: 'ii. Full Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'passport_or_nrc',
                label: 'iii. National Registration Card No.',
                required: true,
                columns: 'col-md-6'
            }),
            new FormTitle({
                label: 'iv. Address'
            }),
            new FormSelect({
                key: 'individual_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'individual_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormInput({
                key: 'phone_no',
                label: 'v. Phone number',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'vi. E-mail Address',
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let company: BaseForm < string > [] =[
            new FormInput({
                key: 'company_name',
                label: 'ii. Name of company',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'company_registration_no',
                label: 'iii. Company Registration No.',
                required: true,
                columns: 'col-md-6'
            }),
            new FormTitle({
                label: 'iv. Address'
            }),
            new FormSelect({
                key: 'company_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'company_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormTitle({
                label: 'v. Contact Person'
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                type: 'number',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail Address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let government: BaseForm < string > [] =[
            new FormInput({
                key: 'name',
                label: 'ii. Name of government department / organization',
                required: true
            }),
            new FormTitle({
                label: 'iii. Address'
            }),
            new FormSelect({
                key: 'government_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'government_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'government_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'government_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'government_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'government_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormTitle({
                label: 'iv. Contact person',
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail Address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let lessor_land: BaseForm < string > [] =[
            new FormSectionTitle({
                label: '4. In case of sublease (i.e. if the owner is not the lessor of the land and/or buildings):'
            }),
            new FormSectionTitle({
                label: 'a. Particulars on the lessor:'
            }),
            new FormTitle({
                label: 'Please upload evidence of the lessor’s right to use land and/or buildings:',
            }),
            new FormFile({
                label: 'File name:',
                key: 'lesson_land_doc'
            }),
            new FormNote({
                label: 'General power/ special power including the stamp from contract registration office should be uploaded. If subleasing from another investor, upload their Land Rights Authorization.'
            }),
            new FormSelect({
                key: 'lessor_type_id',
                label: 'i. Type of lessor',
                options$: this.localService.getType(),
                placeholder: 'Select lessor type'
            }),
            new BaseFormGroup({
                key: 'individual',
                formGroup: individual,
                criteriaValue: {
                    key: 'lessor_type_id',
                    value: 'Individual'
                }
            }),
            new BaseFormGroup({
                key: 'company',
                formGroup: company,
                criteriaValue: {
                    key: 'lessor_type_id',
                    value: 'Company'
                }
            }),
            new BaseFormGroup({
                key: 'government',
                formGroup: government,
                criteriaValue: {
                    key: 'lessor_type_id',
                    value: 'Government department/organization'
                }
            })
        ];

        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location ' + (this.index + 1),
                        style: { 'font-size': '18px' }
                    }),
                    new BaseFormGroup({
                        key: 'lessor_land',
                        formGroup: lessor_land,
                    })
                ]
            })
            
        ];
    }
}