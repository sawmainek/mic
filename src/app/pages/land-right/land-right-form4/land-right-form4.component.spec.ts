import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm4Component } from './land-right-form4.component';

describe('LandRightForm4Component', () => {
  let component: LandRightForm4Component;
  let fixture: ComponentFixture<LandRightForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
