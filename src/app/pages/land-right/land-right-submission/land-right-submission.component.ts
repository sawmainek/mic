import { NotifyService } from './../../../../services/notify.service';
import { FormLogService } from './../../../../services/form-log.service';
import { LandRightPreviewService } from './../land-right-preview/land-right-preview.service';
import { readyToSubmit } from './../../../core/form/_selectors/form.selectors';
import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Input } from '@angular/core';
import { forkJoin } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LandRightService } from 'src/services/land_right.service';
import { Location } from '@angular/common';
import { LandRightSubmission } from '../land-right-submission';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { LandRight } from 'src/app/models/land-right';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormCheckBox } from 'src/app/custom-component/dynamic-forms/base/form-checkbox';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { Form } from 'src/app/models/form';
import { SubmissionLandRight } from 'src/app/models/submission_land_right';
import { FormService } from 'src/services/form.service';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { CurrentForm } from 'src/app/core/form/_actions/form.actions';

declare var jQuery: any;
@Component({
    selector: 'app-land-right-submission',
    templateUrl: './land-right-submission.component.html',
    styleUrls: ['./land-right-submission.component.scss']
})
export class LandRightSubmissionComponent extends LandRightSubmission implements OnInit {
    menu: any = "Submission";
    page = "land-right/submission/";
    @Input() id: any;
    mm: any;

    states: any = [];

    landModel: LandRight;
    formGroup: FormGroup;
    form: any;

    user: any = {};

    formModel: Form;
    landRightSubmissionModel: SubmissionLandRight;
    service: any;

    readyToSubmitNow: boolean = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    isPreview = false;
    loading = true;

    landRightForm: BaseForm<any>[] = [
        new FormCheckBox({
            key: 'is_true',
            required: true,
            label: 'I / We hereby declare that the above statements are true and correct to the best of my/our knowledge and belief.'
        }),
        new FormCheckBox({
            key: 'is_understand',
            required: true,
            label: 'I /We fully understand that proposal may be denied or unnecessarily delayed if the applicant fails to provide required information to the Commission for issuance of the permit.'
        }),
        new FormCheckBox({
            key: 'is_strictly_comply',
            required: true,
            label: 'I/We hereby declare to strictly comply with terms and conditions set out by the Myanmar Investment Commission.'
        }),
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private landFormService: LandRightFormService,
        private landMMService: LandRightMMService,
        private landService: LandRightService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private formService: FormService,
        private lotService: LocationService,
        private landRightSubmissionService: LandrightSubmissionService,
        public formCtrService: FormControlService,
        private formLogService: FormLogService,
        public location: Location,
        private notifyService: NotifyService,
        private translateService: TranslateService,
        private landRightPreviewService: LandRightPreviewService
    ) {
        super(formCtrService);
        this.loading = true;
    }

    ngOnInit(): void {
        this.store.pipe(select(currentUser)).subscribe(result => {
            this.user = result;
        });
        this.store.pipe(select(readyToSubmit)).subscribe(submission => {
            if (submission == 'Land Right') {
                this.readyToSubmitNow = true;
            }
        })
        this.formGroup = this.formCtrService.toFormGroup(this.landRightForm);
        this.lotService.getState().subscribe(x => this.states = x);
        this.getLandRightData();
    }

    getLandRightData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.landMMService : this.landService;

            if (this.id) {
                this.landFormService.show(this.id)
                    .subscribe((form: any) => {
                        // For New Flow
                        this.form = form || {};
                        this.landModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.formGroup.patchValue(this.landModel?.submission || {});

                        this.page = "land-right/submission/";
                        this.page += this.mm && this.mm == 'mm' ? this.landModel.id + '/mm' : this.landModel.id;

                        this.formCtlService.getComments$('Land Right', this.landModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.landModel?.id, type: 'Land Right', page: this.page, comments }));
                        });

                        this.landRightSubmissionModel = this.form.submission || {};

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    openMenu(menu) {
        this.menu = menu;
    }

    onReply() {
        jQuery('.nav-tabs a[href="#reply"]').tab('show');
    }

    saveDraft() {
        this.is_draft = true;
        this.spinner = true;
        const submission = { submission: this.formGroup.value };
        this.landMMService.create({ ...this.landModel, ...submission }).subscribe(results => {
            this.spinner = false;
        });
    }

    onPreview() {
        this.isPreview = true;
        this.spinner = true;
        const submission = { submission: this.formGroup.value };
        this.landMMService.create({ ...this.landModel, ...submission }).subscribe(result => {
            this.spinner = false;
            this.isPreview = false;
            this.landRightPreviewService.toFormPDF(result, {
                readOnly: true,
                onSubmit$: () => {

                }
            })
        });
    }

    onSubmit() {
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }

        this.is_draft = false;
        this.saveFormProgress(this.form?.id, this.landModel.id, this.page);

        if (this.mm) {
            this.saveFormData();
        }
        else {
            this.saveLandRightInfo();
        }
    }

    saveLandRightInfo() {
        const submission = { submission: this.formGroup.value };
        this.service.create({ ...this.landModel, ...submission }).subscribe(results => {
            this.spinner = false;
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            this.router.navigate([page+'/land-right/submission', this.id, 'mm']);
        });
    }

    saveFormData() {

        if (!this.readyToSubmitNow) {
            this.toast.error("Please complete all section.");
            return false;
        }
        
        this.spinner = true;
        let data = {
            'land_right_id': this.id,
            'submitted_user_id': this.user.id,
            'status': this.landRightSubmissionModel?.status || 'New',
            'sub_status': 'New',
            'allow_revision': false
        };
        data['application_no'] = this.landRightSubmissionModel.application_no || `${Math.floor(Math.random() * 999999)}`;

        data['permit_endorsement_no'] = this.landModel?.investment?.endorsement_permit_no;
        data['investment_enterprise_name'] = this.user?.name;
        data['state'] = this.landModel?.land_to_state;
        data['submit_to'] = this.landModel?.apply_to;

        // Todo for permission
        if (this.form.apply_to == 'Myanmar Investment Commision') {
            data['submit_to'] = this.form.apply_to;
            data['submit_to_role'] = 'mic';
        } else {
            data['submit_to_role'] = `${this.form.state.toLowerCase()}-branch`;
        }

        this.landRightSubmissionModel = { ...this.landRightSubmissionModel, ...data };
        this.landRightSubmissionService.create(this.landRightSubmissionModel).subscribe(submission => {
            this.formModel = new Form;
            this.formModel.user_id = this.user.id;
            this.formModel.type = "Land Right";
            this.formModel.type_id = submission.id;
            this.formModel.status = this.landRightSubmissionModel.id ? 'Resubmit' : 'New';
            this.formModel.data = this.form.data;
            this.formModel.data_mm = { ...this.landModel, ...{ submission: this.formGroup.value } };
            // Save to Form Data/Status Log Model;
            forkJoin([
                this.landMMService.create({ ...this.landModel, ...{ submission: this.formGroup.value } }),
                this.landFormService.create({ ...this.form, ...{ status: 'Submitted' } }),
                this.formService.create(this.formModel)
            ]).subscribe(results => {
                this.spinner = false;
                this.formLogService.create({
                    type: this.formModel.type,
                    type_id: this.formModel.type_id,
                    status: this.landRightSubmissionModel.id ? 'Revision' : 'New',
                    sub_status: 'Submission',
                    submitted_user_id: this.formModel.user_id,
                    reply_user_id: submission?.reply_user_id,
                    incoming: true,
                    extra: {
                        application_no: submission?.application_no,
                        form_id: submission?.land_right_id,
                        history_data: { ...this.form, ...{ data_mm: this.formModel.data_mm } }
                    }
                }).subscribe();
                if (submission?.reply_user_id) {
                    this.notifyService.create({
                        user_id: submission?.reply_user_id,
                        subject: `${this.formModel.status} Land Right: # ${submission?.application_no} received.`,
                        message: `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/land-right/landright-choose-investment/${submission.land_right_id}">${submission?.application_no}</a> for new submission.`
                    }).subscribe();
                }
                this.router.navigate(['/pages/land-right/success', this.id]);
            });
            
        });
    }

}
