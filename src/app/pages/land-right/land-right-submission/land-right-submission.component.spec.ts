import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightSubmissionComponent } from './land-right-submission.component';

describe('LandRightSubmissionComponent', () => {
  let component: LandRightSubmissionComponent;
  let fixture: ComponentFixture<LandRightSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
