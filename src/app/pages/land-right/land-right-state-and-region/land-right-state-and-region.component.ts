import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LandRightService } from 'src/services/land_right.service';
import { Observable } from 'rxjs';
import { LandRight } from 'src/app/models/land-right';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { AppState } from 'src/app/core';
import { Store } from '@ngrx/store';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-land-right-state-and-region',
    templateUrl: './land-right-state-and-region.component.html',
    styleUrls: ['./land-right-state-and-region.component.scss']
})
export class LandRightStateAndRegionComponent implements OnInit {

    states: any;

    landRightForm: FormGroup;
    landRightModel: any = {};

    id: any;
    submitted = false;
    spinner = false;
    loading = true

    is_officer: boolean = false;

    constructor(
        public location: Location,
        private formBuilder: FormBuilder,
        private router: Router,
        private lotService: LocationService,
        private activatedRoute: ActivatedRoute,
        private toast: ToastrService,
        private formService: LandRightFormService,
        private store: Store<AppState>,
        private authService: AuthService,
    ) {
        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;
        this.loading = true;

        this.landRightForm = this.formBuilder.group({
            state: ['', [Validators.required]]
        });

        this.changeCboData();

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            if (this.id) {
                this.formService.show(this.id)
                    .subscribe(result => {
                        this.landRightModel = result;
                        this.landRightForm.patchValue(this.landRightModel || {}); //Make sure to match with form group

                        this.loading = false;
                    })
            }
        }).unsubscribe();

    }

    ngOnInit(): void { }

    changeCboData() {
        let lan = localStorage.getItem('lan') || 'en';

        this.lotService.getState(lan).subscribe(x => this.states = x);

        this.authService.changeLanguage$.subscribe(x => {
            this.lotService.getState(x).subscribe(x => this.states = x);
        })
    }

    onSubmit() {
        if (this.landRightForm.invalid) {
            this.toast.error('Please complete all required field(s).');
            this.spinner = false;
            return;
        }
        this.spinner = true;
        //For New Flow
        this.formService.create({ ...this.landRightModel, ...this.landRightForm.value })
            .subscribe(result => {
                this.landRightModel = result;
                this.redirectLink(this.landRightModel.id);
            })
    }

    redirectLink(id: number) {
        if (window.location.href.indexOf('officer') !== -1) {
            this.router.navigate(['/officer/land-right/landright-choose-language', id]);
        } else {
            this.router.navigate(['/pages/land-right/landright-choose-language', id]);
        }
    }

}
