import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightStateAndRegionComponent } from './land-right-state-and-region.component';

describe('LandRightStateAndRegionComponent', () => {
  let component: LandRightStateAndRegionComponent;
  let fixture: ComponentFixture<LandRightStateAndRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightStateAndRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightStateAndRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
