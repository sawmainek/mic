import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm5Component } from './land-right-form5.component';

describe('LandRightForm5Component', () => {
  let component: LandRightForm5Component;
  let fixture: ComponentFixture<LandRightForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
