import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';

export class LandRightForm5{
    
    public forms: BaseForm<string>[] = [];
    index = 0;

    constructor(

    ) {
        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location ' + (this.index + 1),
                        style: { 'font-size': '18px' }
                    }),
                    new BaseFormGroup({
                        key: 'owner_agree_option',
                        formGroup: [
                            new FormSectionTitle({
                                label: '4. In case of sublease (i.e. if the owner is not the lessor of the land and/or buildings):'
                            }),
                            new FormTitle({
                                label: 'b. Does the owner agree to the lease agreement?',
                            }),
                            new FormRadio({
                                label: 'Yes',
                                key: 'is_owner_agree_lease',
                                value: 'Yes',
                                columns: 'col-md-4'
                            }),
                            new FormRadio({
                                label: 'No',
                                key: 'is_owner_agree_lease',
                                value: 'No',
                                columns: 'col-md-8'
                            }),
                            new FormTitle({
                                label: 'c. If subleasing state-owned land or buildings, please select which of the following apply to the lessor:',
                            }),
                            new FormRadio({
                                label: 'A person who has previously obtained the right to use the state-owned and or buildings from the government department and government organization in accordance with the laws of the Union.',
                                key: 'apply_to_lessor_option',
                                value: 'A person who has previously obtained the right to use the state-owned and or buildings from the government department and government organization in accordance with the laws of the Union',
                            }),
                            new FormRadio({
                                label: 'A person authorized to sub-lease or sub-license the state-owned land or building in accordance with the approval of the government department and government organization.',
                                key: 'apply_to_lessor_option',
                                value: 'A person authorized to sub-lease or sub-license the state-owned land or building in accordance with the approval of the government department and government organization.',
                            }),
                            new FormRadio({
                                label: 'Not applicable',
                                key: 'apply_to_lessor_option',
                                value: 'Not applicable'
                            }),
                        ]
                    }),
                ]
            })
            
        ];
    }
}