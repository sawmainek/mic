import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm1Component } from './land-right-form1.component';

describe('LandRightForm1Component', () => {
  let component: LandRightForm1Component;
  let fixture: ComponentFixture<LandRightForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
