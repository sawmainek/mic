import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LandRightService } from 'src/services/land_right.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { LandRight } from 'src/app/models/land-right';
import { Location } from '@angular/common';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { LandRightFormData } from '../land-right-formdata';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { LocationService } from 'src/services/location.service';


@Component({
    selector: 'app-land-right-form1',
    templateUrl: './land-right-form1.component.html',
    styleUrls: ['./land-right-form1.component.scss']
})
export class LandRightForm1Component extends LandRightFormData implements OnInit {
    menu: any = "formData";
    page = "land-right/form1/";
    modelId: any;
    @Input() id: any;

    user: any = {};

    landRightModel: any = {};
    formGroup: FormGroup;
    submitted = false;
    loaded = false;

    spinner = false;
    is_draft = false;
    loading = false;

    mm: any;
    form: any;
    service: any;

    isRevision: boolean = false;

    landRightForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'authorized_person',
            formGroup: [
                new FormTitle({
                    label: '1. Is an authorized representative filling in the application on behalf of the investor(s)?'
                }),
                new FormRadio({
                    key: 'is_authorized_applicant',
                    label: 'Yes',
                    value: 'Yes',
                    required: true,
                    columns: 'col-md-4 d-block',
                }),
                new FormRadio({
                    key: 'is_authorized_applicant',
                    label: 'No, investor(s) are filling in the application by themselves',
                    value: 'No',
                    required: true,
                    columns: 'col-md-8 d-block',
                }),
                new FormInput({
                    key: 'company_name',
                    label: 'a. Name of company, if applicable',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormTitle({
                    label: 'b. Address of company, if applicable',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormSelect({
                    key: 'company_country',
                    label: 'Country',
                    options$: this.lotService.getCountry(),
                    value: 'Myanmar',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormSelect({
                    key: 'company_state',
                    label: 'State / Region:',
                    options$: this.lotService.getState(),
                    required: true,
                    columns: 'col-md-4',
                    criteriaValue: {
                        key: 'company_country',
                        value: ['Myanmar'],
                    }
                }),
                new FormSelect({
                    key: 'company_district',
                    label: 'District:',
                    options$: this.lotService.getDistrict(),
                    required: true,
                    columns: 'col-md-4',
                    filter: {
                        parent: 'company_state',
                        key: 'state'
                    },
                    criteriaValue: {
                        key: 'company_country',
                        value: ['Myanmar'],
                    }
                }),
                new FormSelect({
                    key: 'company_township',
                    label: 'Township:',
                    options$: this.lotService.getTownship(),
                    required: true,
                    columns: 'col-md-4',
                    filter: {
                        parent: 'company_district',
                        key: 'district'
                    },
                    criteriaValue: {
                        key: 'company_country',
                        value: ['Myanmar'],
                    }
                }),
                new FormInput({
                    key: 'company_address',
                    label: 'Additional address details',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormSectionTitle({
                    label: 'c. Contact person',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'auth_contact_full_name',
                    label: 'Full Name',
                    required: true,
                    columns: 'col-md-6',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes',
                    }
                }),
                new FormInput({
                    key: 'auth_contact_position',
                    label: 'Position',
                    required: true,
                    columns: 'col-md-6',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'auth_contact_phone',
                    label: 'Phone number',
                    required: true,
                    columns: 'col-md-6',
                    type: 'number',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'auth_contact_email',
                    label: 'E-mail address',
                    required: true,
                    validators: [Validators.email],
                    columns: 'col-md-6',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'auth_passport_or_nrc',
                    label: 'Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
                    required: true,
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
                new FormSelect({
                    key: 'auth_citizenship',
                    label: 'Citizenship',
                    options$: this.lotService.getCountry(),
                    required: true,
                    value: 'Myanmar',
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    },
                }),
                new FormFile({
                    key: 'official_legal_docs',
                    label: 'Please upload an official letter of legal representation:',
                    required: true,
                    criteriaValue: {
                        key: 'is_authorized_applicant',
                        value: 'Yes'
                    }
                }),
            ]
        })
    ];
    // store: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private landService: LandRightService,
        private formService: LandRightFormService,
        private landMMService: LandRightMMService,
        private translateService: TranslateService,
        private lotService: LocationService
    ) {
        super(formCtlService);
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm ? this.landMMService : this.landService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.landRightModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm, this.landRightModel, form?.data || {});

                        this.page = "land-right/form1/";
                        this.page += this.mm && this.mm == 'mm' ? this.landRightModel.id + '/mm' : this.landRightModel.id;

                        this.formCtlService.getComments$('Land Right', this.landRightModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.landRightModel?.id, type: 'Land Right', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        } else {
            this.spinner = true;
            this.is_draft = false;
            this.saveLandRightInfo();
        }
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveLandRightInfo();
    }

    saveLandRightInfo() {
        this.formCtlService.lazyUpload(this.landRightForm, this.formGroup, { type_id: this.id }, (formValue) => {
            this.landRightModel = { ...this.landRightModel, ...formValue };

            this.service.create(this.landRightModel, { secure: true })
                .subscribe(x => {

                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.landRightModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/form2', id, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/land-right/form2', id]);
                } else {
                    this.router.navigate([page + '/land-right/form1', id, 'mm']);
                }
            }
        }
    }
}
