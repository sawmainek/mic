import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocationService } from 'src/services/location.service';

export class LandRightForm1{
    
    public forms: BaseForm<string>[] = []

    constructor(
        private lotService: LocationService
    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'authorized_person',
                formGroup: [
                    new FormTitle({
                        label: '1. Is an authorized representative filling in the application on behalf of the investor(s)?'
                    }),
                    new FormRadio({
                        key: 'is_authorized_applicant',
                        label: 'Yes',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-4 d-block',
                    }),
                    new FormRadio({
                        key: 'is_authorized_applicant',
                        label: 'No, investor(s) are filling in the application by themselves',
                        value: 'No',
                        required: true,
                        columns: 'col-md-8 d-block',
                    }),
                    new FormInput({
                        key: 'company_name',
                        label: 'a. Name of company, if applicable',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                    new FormTitle({
                        label: 'b. Address of company, if applicable',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'company_country',
                        label: 'Country',
                        options$: this.lotService.getCountry(),
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'company_state',
                        label: 'State / Region:',
                        options$: this.lotService.getState(),
                        required: true,
                        columns: 'col-md-4',
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'company_district',
                        label: 'District:',
                        options$: this.lotService.getDistrict(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'company_state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'company_township',
                        label: 'Township:',
                        options$: this.lotService.getTownship(),
                        required: true,
                        columns: 'col-md-4',
                        filter: {
                            parent: 'company_district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'company_country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormInput({
                        key: 'company_address',
                        label: 'Additional address details',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),  
                    new FormSectionTitle({
                        label: 'c. Contact person',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'auth_contact_full_name',
                        label: 'Full Name',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes',
                        }
                    }),
                    new FormInput({
                        key: 'auth_contact_position',
                        label: 'Position',
                        required: true,
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'auth_contact_phone',
                        label: 'Phone number',
                        required: true,
                        columns: 'col-md-6',
                        type: 'number',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'auth_contact_email',
                        label: 'E-mail address',
                        required: true,
                        validators: [Validators.email],
                        columns: 'col-md-6',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'auth_passport_or_nrc',
                        label: 'Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'auth_citizenship',
                        label: 'Citizenship',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        },
                    }),
                    new FormFile({
                        key: 'official_legal_docs',
                        label: 'Please upload an official letter of legal representation:',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized_applicant',
                            value: 'Yes'
                        }
                    }),
                ]
            })
        ];
    }
}