import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { LandRightProgress } from './land-right-progress';

export class LandRightSubmission extends LandRightProgress{
    static section="Submission";
    static totalForm=1;
    totalForm=1;

    constructor(public formCtlService: FormControlService){
       super();
    }    

    saveFormProgress(form_id: number, id:number,page:string){
        if (this.progressForms?.length > 0) {
            if (this.progressForms.filter(x => x.type == LandRightProgress.type && x.section == LandRightSubmission.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: LandRightProgress.type,
                    type_id: id,
                    section: LandRightSubmission.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }

        }
        else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: LandRightSubmission.type,
                type_id: id,
                section: LandRightSubmission.section,
                page: page,
                status: true,
                revise: false
            });
        }
    }
}