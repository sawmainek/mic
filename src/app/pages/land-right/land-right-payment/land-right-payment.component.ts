import { ActivatedRoute } from '@angular/router';
import { LandRightPaymentService } from './../../../../services/land-right-payment.service';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { from, of } from 'rxjs';
import { concatMap, delay, finalize, mergeMap, tap } from 'rxjs/operators';

@Component({
    selector: 'app-land-right-payment',
    templateUrl: './land-right-payment.component.html',
    styleUrls: ['./land-right-payment.component.scss']
})
export class LandRightPaymentComponent implements OnInit {
    id: any;
    invoice: any;
    user: any = {};
    submission: any = {};
    total: number = 0;
    credit: boolean = false;
    mpu: boolean = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private landRightSubmissionService: LandrightSubmissionService,
        private landRightPaymentService: LandRightPaymentService,
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                    this.getPayment();
                }
            });
    }

    getPayment() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            if (this.id) {
                this.landRightPaymentService.show(this.id).subscribe(invoice => {
                    this.invoice = invoice;
                    this.total = Number(invoice.amount);
                })
            }
        }).unsubscribe();
    }

    get checkPaymentNeed() {
        return this.invoice.status == 'Unpaid' ? true : false;
    }

    saveCredit() {
        this.credit = true;
        this.landRightPaymentService.update(this.id, { status: 'Paid' }).subscribe(res => {
            this.credit = false;
            window.location.href = environment.host + "/payments/mpu?request_id=" + this.invoice.id + "&user_id=" + this.user.id + "&amount=" + this.total + "&success_redirect_url=" + window.location.hostname + "&fail_redirect_url=" + window.location.hostname
        });
    }

    saveMPU() {
        this.mpu = true;
        this.landRightPaymentService.update(this.id, { status: 'Paid' }).subscribe(res => {
            this.mpu = false;
            window.location.href = environment.host + "/payments/mpu?request_id=" + this.invoice.id + "&user_id=" + this.user.id + "&amount=" + this.total + "&success_redirect_url=" + window.location.hostname + "&fail_redirect_url=" + window.location.hostname
        });

    }

}

