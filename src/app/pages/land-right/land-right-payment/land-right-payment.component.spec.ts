import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightPaymentComponent } from './land-right-payment.component';

describe('LandRightPaymentComponent', () => {
  let component: LandRightPaymentComponent;
  let fixture: ComponentFixture<LandRightPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
