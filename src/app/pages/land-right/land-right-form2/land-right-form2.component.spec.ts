import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm2Component } from './land-right-form2.component';

describe('LandRightForm2Component', () => {
  let component: LandRightForm2Component;
  let fixture: ComponentFixture<LandRightForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
