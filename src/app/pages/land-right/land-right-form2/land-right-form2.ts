import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';

export class LandRightForm2{
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'investment',
                formGroup: [
                    new FormSectionTitle({
                        label: '2. For which investment is the land authorization application being submitted?'
                    }),
                    new FormInput({
                        key: 'endorsement_permit_no',
                        label: 'Permit or endorsement number:',
                        required: true,
                    }),
                    new FormDate({
                        key: 'endorsement_permit_date',
                        label: 'Date of receiving permit or endorsement:',
                        required: true,
                    })
                ]
            }),
        ];
    }
}