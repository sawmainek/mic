import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit, Pipe } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LandRightService } from 'src/services/land_right.service';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { Location } from '@angular/common';
import { LandRightFormData } from '../land-right-formdata';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-land-right-form2',
    templateUrl: './land-right-form2.component.html',
    styleUrls: ['./land-right-form2.component.scss']
})
export class LandRightForm2Component extends LandRightFormData implements OnInit {
    menu: any = "formData";
    page = "land-right/form2/";
    modelId: any;
    mm: any;
    @Input() id: any;

    user: any = {};

    landRightModel: any = {};
    formGroup: FormGroup;
    form: any;
    service: any;

    submitted = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    landRightForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'investment',
            formGroup: [
                new FormSectionTitle({
                    label: '2. For which investment is the land authorization application being submitted?'
                }),
                new FormInput({
                    key: 'endorsement_permit_no',
                    label: 'Permit or endorsement number:',
                    required: true,
                }),
                new FormDate({
                    key: 'endorsement_permit_date',
                    label: 'Date of receiving permit or endorsement:',
                    required: true,
                })
            ]
        }),
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private landService: LandRightService,
        private formService: LandRightFormService,
        private landMMService: LandRightMMService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        super(formCtlService);
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm ? this.landMMService : this.landService;
            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.landRightModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm, this.landRightModel, form?.data || {});

                        this.page = "land-right/form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.landRightModel.id + '/mm' : this.landRightModel.id;

                        this.formCtlService.getComments$('Land Right', this.landRightModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.landRightModel?.id, type: 'Land Right', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        } else {
            this.spinner = true;
            this.is_draft = false;
            this.saveLandRightInfo();
        }
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveLandRightInfo();
    }

    saveLandRightInfo() {
        this.landRightModel = { ...this.landRightModel, ...this.formGroup.value };

        this.service.create(this.landRightModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.landRightModel.id, this.page);
                    this.redirectLink(this.form.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/form3', id, 0, 'mm']);
            } else {
                if (this.mm) {
                    this.router.navigate([page + '/land-right/form3', id, 0]);
                } else {
                    this.router.navigate([page + '/land-right/form2', id, 'mm']);
                }
            }
        }
    }
}
