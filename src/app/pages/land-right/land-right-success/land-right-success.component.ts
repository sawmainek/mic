import { TranslateService } from '@ngx-translate/core';
import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-land-right-success',
    templateUrl: './land-right-success.component.html',
    styleUrls: ['./land-right-success.component.scss']
})
export class LandRightSuccessComponent implements OnInit {

    id: any;

    language: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private locationStrategy: LocationStrategy,
        private translateService: TranslateService
    ) {
        this.language = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.language ? this.language : 'en');
        this.translateService.use(this.language ? this.language : 'en');
        history.pushState(null, null, location.href);
        this.locationStrategy.onPopState(() => {
            history.pushState(null, null, location.href);
        });
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id');
        });
    }

    ngOnInit(): void { }

}
