import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightSuccessComponent } from './land-right-success.component';

describe('LandRightSuccessComponent', () => {
  let component: LandRightSuccessComponent;
  let fixture: ComponentFixture<LandRightSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
