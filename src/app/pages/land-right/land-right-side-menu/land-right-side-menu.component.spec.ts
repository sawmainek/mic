import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightSideMenuComponent } from './land-right-side-menu.component';

describe('LandRightSideMenuComponent', () => {
  let component: LandRightSideMenuComponent;
  let fixture: ComponentFixture<LandRightSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
