import { currentForm, formProgress } from './../../../core/form/_selectors/form.selectors';
import { Store, select } from '@ngrx/store';
import { AppState } from './../../../core/reducers/index';
import { ReadyToSubmit, FormProgress } from './../../../core/form/_actions/form.actions';
import { LandRightFormService } from './../../../../services/land-right-form.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { FormProgressService } from 'src/services/form-progress.service';
import { LandRightFormData } from '../land-right-formdata';
import { LandRightSubmission } from '../land-right-submission';
import { LandRightLanguage } from '../landright-choose-language/landright-choose-language';
import { LandRight } from 'src/app/models/land-right';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';

@Component({
    selector: 'app-land-right-side-menu',
    templateUrl: './land-right-side-menu.component.html',
    styleUrls: ['./land-right-side-menu.component.scss']
})
export class LandRightSideMenuComponent implements OnInit {
    @Input() menu: any = "choose-language";
    @Input() progress: any = false;
    @Output() progressForms = new EventEmitter<any>();

    pages = [
        'land-right/form1',
        'land-right/form2',
        'land-right/form3',
        'land-right/form4',
        'land-right/form5',
        'land-right/form6',
        'land-right/form7',
        'land-right/form8',
        'land-right/form9',
    ];

    user: any;
    is_officer: boolean = false;

    form: any = {};
    model: any = {};
    landRightModel: LandRight;
    landRightMMModel: LandRight;
    landSubmissionModel: any = {};

    formProgress: any;
    completeSections = [];
    progressLists: any[] = [];

    id: any = 0;
    mm: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private landRightFormService: LandRightFormService,
        private formProgressService: FormProgressService,
        public formCtlService: FormControlService,
        private store: Store<AppState>
    ) {
        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            
            if (this.id > 0) {
                // Get data from state
                this.store.pipe(select(currentForm))
                    .subscribe(data => {
                        if (data.form) {
                            this.form = data.form || {};
                            this.landRightModel = this.form?.data || {};
                            this.landRightMMModel = this.form?.data_mm || {};
                            this.model = this.form?.language == 'Myanmar' ? this.form?.data_mm || {} : this.form?.data || {};
                            this.landSubmissionModel = this.form.submission || {};
                            this.createProgressList();
                            this.editTotalForms();
                        } else {
                            // Get data from API
                            this.landRightFormService.show(this.id)
                                .subscribe((form: any) => {
                                    this.form = form || {};
                                    this.landRightModel = this.form?.data || {};
                                    this.landRightMMModel = this.form?.data_mm || {};
                                    this.model = this.form?.language == 'Myanmar' ? this.form?.data_mm || {} : this.form?.data || {};
                                    this.landSubmissionModel = this.form.submission || {};
                                    this.createProgressList();
                                    this.editTotalForms();
                                });
                        }
                    }).unsubscribe();
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    createProgressList() {
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                    this.getFormProgress();
                    this.editTotalForms();
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:Land Right`
                    }).subscribe(results => {
                        this.progressLists = results;
                        this.getFormProgress();
                        this.editTotalForms();
                        this.store.dispatch(new FormProgress({ progress: this.progressLists }));
                    });
                }
            }).unsubscribe();
    }

    getFormProgress() {
        if (this.progressLists.length > 0) {
            this.formProgress = this.progressLists;
            this.progressForms.emit(this.formProgress);
            let sections = ["Choose Language", "Form Data", "Submission"];
            this.completeSections = sections.map(x => this.isSectionComplete(x));

            //Enable submission button
            if (this.completeSections[0] && this.completeSections[1]) {
                setTimeout(() => {
                    this.store.dispatch(new ReadyToSubmit({ submission: 'Land Right' }));
                }, 600);
            }
        }
        else {
            this.progressForms.emit([]);
        }
    }

    checkRevise(section) {
        const revise = this.progressLists.filter(x => x.section == section && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    editTotalForms() {
        if (this.form?.language == 'Myanmar') {
            LandRightFormData.totalForm = new LandRightFormData(this.formCtlService).totalForm;
            LandRightSubmission.totalForm = new LandRightSubmission(this.formCtlService).totalForm;
        }
        else {
            LandRightFormData.totalForm = new LandRightFormData(this.formCtlService).totalForm * 2;
            LandRightSubmission.totalForm = new LandRightSubmission(this.formCtlService).totalForm * 2;
        }
    }

    checkPageComplete(page) {
        const length = this.progressLists.filter(x => x.page == `${page}/${this.landRightMMModel?.id || 0}/mm`).length;
        return length > 0 ? 1 : 0;
    }

    checkCompleteFormData() {
        this.pages = [
            'land-right/form1',
            'land-right/form2',
            'land-right/form3',
            'land-right/form4',
            'land-right/form5',
            'land-right/form6',
            'land-right/form7',
            'land-right/form8',
            'land-right/form9',
        ];

        var totalLength = 0;
        this.pages.forEach((page, index) => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == (index < 2 ? `${page}/${this.landRightMMModel.id}/mm` : `${page}/${this.landRightMMModel.id}/0/mm`)).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == (index < 2 ? `${page}/${this.landRightModel.id}` : `${page}/${this.landRightModel.id}/0`)).length;
                const lengthMM = this.progressLists.filter(x => x.page == (index < 2 ? `${page}/${this.landRightMMModel.id}/mm` : `${page}/${this.landRightMMModel.id}/0/mm`)).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteSubmission() {
        this.pages = [
            'land-right/submission'
        ];

        var totalLength = 0;
        this.pages.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.landRightMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.landRightModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.landRightMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    isSectionComplete(menu) {
        let hasComplete = false;
        switch (menu) {
            case LandRightLanguage.section: {
                if (LandRightLanguage.totalForm <= this.checkPageComplete('land-right/choose-language')) {
                    hasComplete = true;
                }
                break;
            }
            case LandRightFormData.section: {
                if (LandRightFormData.totalForm <= this.checkCompleteFormData()) {
                    hasComplete = true;
                }
                break;
            }
            case LandRightSubmission.section: {
                if (LandRightSubmission.totalForm <= this.checkCompleteSubmission()) {
                    hasComplete = true;
                }
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
        return hasComplete;
    }

    openSection(index, link) {
        // For New Flow
        if(this.allowRevision) {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/' + link, this.form?.id, 'mm']);
            } else {
                this.router.navigate([page + '/land-right/' + link, this.form?.id]);
            }
        }
    }

    get allowRevision(): boolean {
        if (window.location.href.indexOf('officer') == -1 && this.form?.submission) {
            return false;
        }
        return true;
    }

}
