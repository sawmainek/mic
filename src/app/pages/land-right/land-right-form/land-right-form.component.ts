import { formProgress } from './../../../core/form/_selectors/form.selectors';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { AppState } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { LandRight } from 'src/app/models/land-right';
import { AuthService } from 'src/services/auth.service';
import { FormProgressService } from 'src/services/form-progress.service';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightService } from 'src/services/land_right.service';
import { LandRightFormData } from '../land-right-formdata';

@Component({
    selector: 'app-land-right-form',
    templateUrl: './land-right-form.component.html',
    styleUrls: ['./land-right-form.component.scss']
})
export class LandRightFormComponent extends LandRightFormData implements OnInit {
    menu: any = "formData";
    correct: any = false;
    @Input() id: any;
    mm: any;

    user: any = {};
    landRightModel: LandRight
    landRightMMModel: LandRight
    form: any;

    progressLists: any[] = [];
    pages: any[] = [];

    loaded: boolean = false;
    loading = false;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: LandRightFormService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private formProgressService: FormProgressService,
        private translateService: TranslateService,
    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            if (this.id > 0) {
                // Get data from API
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.landRightModel = this.form?.data || {};
                        this.landRightMMModel = this.form?.data_mm || {};
                        this.getFormProgress();

                        this.changeLanguage();
                        this.loading = false;

                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    ngOnInit() { }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            'land-right/form1',
            'land-right/form2',
            'land-right/form3',
            'land-right/form4',
            'land-right/form5',
            'land-right/form6',
            'land-right/form7',
            'land-right/form8',
            'land-right/form9',
        ];
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:Land Right`
                    }).subscribe(result => {
                        this.progressLists = result;
                    });
                }
            }).unsubscribe();
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == (index < 2 ? `${page}/${this.landRightMMModel.id}/mm` : `${page}/${this.landRightMMModel.id}/0/mm`)).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == (index < 2 ? `${page}/${this.landRightModel.id}` : `${page}/${this.landRightModel.id}/0`)).length;
            const lengthMM = this.progressLists.filter(x => x.page == (index < 2 ? `${page}/${this.landRightMMModel.id}/mm` : `${page}/${this.landRightMMModel.id}/0/mm`)).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    openPage(index, link, has_index) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        if (has_index) {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/' + link, this.id, 0, 'mm']);
            } else {
                this.router.navigate([page + '/land-right/' + link, this.id, 0]);
            }
        }
        else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/' + link, this.id, 'mm']);
            } else {
                this.router.navigate([page + '/land-right/' + link, this.id]);
            }
        }
    }
}