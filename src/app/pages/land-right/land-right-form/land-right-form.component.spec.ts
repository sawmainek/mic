import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightFormComponent } from './land-right-form.component';

describe('LandRightFormComponent', () => {
  let component: LandRightFormComponent;
  let fixture: ComponentFixture<LandRightFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
