import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';

export class LandRightForm8{
    
    public forms: BaseForm<string>[] = [];
    index = 0;

    constructor(
        private localService: LocalService,
    ) {
        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location ' + (this.index + 1),
                        style: { 'font-size': '18px' }
                    }),
                    new BaseFormGroup({
                        key: 'land',
                        formGroup: [
                            new FormSectionTitle({
                                label: '7. Particulars on land used/leased'
                            }),
                            new FormFile({
                                label: 'Please upload a location map/layout plan here:',
                                key: 'location_plan_docs',
                                required: true,
                            }),
                            new FormInput({
                                label: 'a. Area size:',
                                key: 'land_map_area',
                                required: true,
                                type: 'number',
                                columns: 'col-md-6'
                            }),
                            new FormSelect({
                                label: 'Select unit of measurement',
                                key: 'land_map_unit',
                                required: true,
                                options$: this.localService.getUnit(),
                                columns: 'col-md-6'
                            }),
                            new FormTextArea({
                                label: 'b. In case the land is rented by multiple lessees, describe whether there is clear division of the land (e.g. fences, separate entry/exit, etc.)',
                                key: 'clear_division_desc',
                                required: true,
                            }),
                            new FormFile({
                                label: 'Optional: upload a map/photo here:',
                                key: 'map_docs',
                            }),
                            new FormSelect({
                                label: 'c. Type of land',
                                key: 'land_type',
                                required: true,
                                options$: this.localService.getLandType(),
                                placeholder: 'Select land type',
                            }),
                            new FormTitle({
                                label: 'd. Is it necessary to change the land type?',
                            }),
                            new FormRadio({
                                label: 'Yes',
                                key: 'is_change_land_type',
                                value: 'Yes',
                                columns: 'col-md-4'
                            }),
                            new FormRadio({
                                label: 'No',
                                key: 'is_change_land_type',
                                value: 'No',
                                columns: 'col-md-8'
                            }),
                            new FormTextArea({
                                label: 'Please specify the desired land type and the status of the process to obtain permission to change the land type:',
                                key: 'change_land_type_reason',
                                required: true,
                                criteriaValue: {
                                    key: 'is_change_land_type',
                                    value: 'Yes'
                                }
                            }),
                            new FormTitle({
                                label: 'If necessary, please upload supporting documents (e.g. recommendations or permission obtained on change of land use) here:'
                            }),
                            new BaseFormArray({
                                key: 'land_use_supporting_doc',
                                formArray: [
                                    new FormFile({
                                        key: 'document',
                                        label: 'Name of document:',
                                        multiple: true,
                                    })
                                ],
                            }),
                            new FormTitle({
                                label: 'e. Is the investor required to significantly alter the topography or elevation of the proposed land?'
                            }),
                            new FormRadio({
                                label: 'Yes',
                                key: 'is_alter_topography',
                                value: 'Yes',
                                columns: 'col-md-4',
                            }),
                            new FormRadio({
                                label: 'No',
                                key: 'is_alter_topography',
                                value: 'No',
                                columns: 'col-md-8',
                            }),
                            new FormTextArea({
                                label: 'Describe specifically to choosing yes',
                                key: 'topography_description',
                                required: true,
                                criteriaValue: {
                                    key: 'is_alter_topography',
                                    value: 'Yes'
                                }
                            }),
                            new FormTitle({
                                label: 'Please upload supporting documents here:',
                            }),
                            new FormFile({
                                label: 'Name of document:',
                                key: 'supporting_docs',
                                required: true,
                            }),
                        ]
                    }),
                ]
            })
            
        ];
    }
}