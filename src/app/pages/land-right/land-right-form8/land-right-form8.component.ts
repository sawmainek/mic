import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LandRightService } from 'src/services/land_right.service';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { Location } from '@angular/common';
import { LandRightFormData } from '../land-right-formdata';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-land-right-form8',
    templateUrl: './land-right-form8.component.html',
    styleUrls: ['./land-right-form8.component.scss']
})
export class LandRightForm8Component extends LandRightFormData implements OnInit {

    menu: any = "formData";
    page = "land-right/form8/";
    modelId: any;
    mm: any;
    user: any = {};

    form: any;
    service: any;
    index = 0;
    @Input() id: any;
    landRightModel: any = {};
    formGroup: FormGroup;
    submitted = false;

    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    landRightForm: BaseForm<string>[] = [
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location ' + (this.index + 1),
            style: { 'font-size': '18px' }
        }),
        new BaseFormGroup({
            key: 'land',
            formGroup: [
                new FormSectionTitle({
                    label: '7. Particulars on land used/leased'
                }),
                new FormFile({
                    label: 'Please upload a location map/layout plan here:',
                    key: 'location_plan_docs',
                    required: true,
                }),
                new FormInput({
                    label: 'a. Area size:',
                    key: 'land_map_area',
                    required: true,
                    type: 'number',
                    columns: 'col-md-6'
                }),
                new FormSelect({
                    label: 'Select unit of measurement',
                    key: 'land_map_unit',
                    required: true,
                    options$: this.localService.getUnit(),
                    columns: 'col-md-6'
                }),
                new FormTextArea({
                    label: 'b. In case the land is rented by multiple lessees, describe whether there is clear division of the land (e.g. fences, separate entry/exit, etc.)',
                    key: 'clear_division_desc',
                    required: true,
                }),
                new FormFile({
                    label: 'Optional: upload a map/photo here:',
                    key: 'map_docs',
                }),
                new FormSelect({
                    label: 'c. Type of land',
                    key: 'land_type',
                    required: true,
                    options$: this.localService.getLandType(),
                    placeholder: 'Select land type',
                }),
                new FormTitle({
                    label: 'd. Is it necessary to change the land type?',
                }),
                new FormRadio({
                    label: 'Yes',
                    key: 'is_change_land_type',
                    value: 'Yes',
                    columns: 'col-md-4'
                }),
                new FormRadio({
                    label: 'No',
                    key: 'is_change_land_type',
                    value: 'No',
                    columns: 'col-md-8'
                }),
                new FormTextArea({
                    label: 'Please specify the desired land type and the status of the process to obtain permission to change the land type:',
                    key: 'change_land_type_reason',
                    required: true,
                    criteriaValue: {
                        key: 'is_change_land_type',
                        value: 'Yes'
                    }
                }),
                new FormTitle({
                    label: 'If necessary, please upload supporting documents (e.g. recommendations or permission obtained on change of land use) here:'
                }),
                new BaseFormArray({
                    key: 'land_use_supporting_doc',
                    formArray: [
                        new FormFile({
                            key: 'document',
                            label: 'Name of document:',
                            multiple: true,
                        })
                    ],
                }),
                new FormTitle({
                    label: 'e. Is the investor required to significantly alter the topography or elevation of the proposed land?'
                }),
                new FormRadio({
                    label: 'Yes',
                    key: 'is_alter_topography',
                    value: 'Yes',
                    columns: 'col-md-4',
                }),
                new FormRadio({
                    label: 'No',
                    key: 'is_alter_topography',
                    value: 'No',
                    columns: 'col-md-8',
                }),
                new FormTextArea({
                    label: 'Describe specifically to choosing yes',
                    key: 'topography_description',
                    required: true,
                    criteriaValue: {
                        key: 'is_alter_topography',
                        value: 'Yes'
                    }
                }),
                new FormTitle({
                    label: 'Please upload supporting documents here:',
                }),
                new FormFile({
                    label: 'Name of document:',
                    key: 'supporting_docs',
                    required: true,
                }),
            ]
        }),
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private landService: LandRightService,
        private formService: LandRightFormService,
        private landMMService: LandRightMMService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private localService: LocalService,
    ) {
        super(formCtlService);
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm && this.mm == 'mm' ? this.landMMService : this.landService;
            this.index = +paramMap.get('index');
            this.landRightForm[1].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.landRightModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.landRightModel.owner = this.landRightModel.owner ? this.landRightModel?.owner || [] : form?.data?.owner || [];

                        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm, this.landRightModel.owner[this.index] || {}, form?.data?.owner && form?.data?.owner[this.index] || {});

                        this.page = "land-right/form8/";
                        this.page += this.mm && this.mm == 'mm' ? (this.landRightModel.id + '/' + this.index + '/mm') : (this.landRightModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Land Right', this.landRightModel.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.landRightModel?.id, type: 'Land Right', comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var land_map_unit = this.landRightForm[2].formGroup.filter(x => x.key == "land_map_unit")[0];
        land_map_unit.options$ = this.localService.getUnit(lan);

        var land_type = this.landRightForm[2].formGroup.filter(x => x.key == "land_type")[0];
        land_type.options$ = this.localService.getLandType(lan);

        this.authService.changeLanguage$.subscribe(x => {
            land_map_unit.options$ = this.localService.getUnit(x);
            land_map_unit.options$ = this.localService.getUnit(x);
        })
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveLandRightInfo();
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveLandRightInfo();
    }

    saveLandRightInfo() {
        this.formCtlService.lazyUpload(this.landRightForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let owner = this.landRightModel.owner ? this.landRightModel.owner.map(x => Object.assign({}, x)) || [] : [];
            owner[this.index] = { ...owner[this.index], ...formValue };

            let data = { 'owner': owner };
            this.service.create({ ...this.landRightModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.landRightModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/form9', id, this.index, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/land-right/form9', id, this.index]);
                } else {
                    this.router.navigate([page + '/land-right/form8', id, this.index, 'mm']);
                }
            }
        }

    }
}
