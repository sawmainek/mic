import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm8Component } from './land-right-form8.component';

describe('LandRightForm8Component', () => {
  let component: LandRightForm8Component;
  let fixture: ComponentFixture<LandRightForm8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
