import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
declare var jQuery: any;
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { LandRightService } from 'src/services/land_right.service';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { Location } from '@angular/common';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { LandRightFormData } from '../land-right-formdata';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { LocalService } from 'src/services/local.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-land-right-form9',
    templateUrl: './land-right-form9.component.html',
    styleUrls: ['./land-right-form9.component.scss']
})
export class LandRightForm9Component extends LandRightFormData implements OnInit {

    menu: any = "formData";
    page = "land-right/form9/";
    modelId: any;
    @Input() id: any;
    index = 0;
    mm: any;

    user: any = {};
    is_officer: boolean = false;

    landRightModel: any = {};
    formGroup: FormGroup;
    form: any;
    service: any;

    lastIndex = false;
    spinner = false;
    is_draft = false;
    is_more = false;
    loading = true;
    next_label = "NEXT SECTION";

    isRevision: boolean = false;

    building: BaseForm<any>[] = [
        new FormSectionTitleCounter({
            label: 'Building'
        }),
        new FormInput({
            key: 'area',
            label: 'Area',
            required: true,
            type: 'number',
            columns: 'col-md-4',
        }),
        new FormSelect({
            key: 'unit',
            label: 'Unit of measurement',
            required: true,
            options$: this.localService.getUnit(),
            columns: 'col-md-4',
        }),
        new FormInput({
            key: 'value_building',
            label: 'Value of Building in MMK',
            required: true,
            columns: 'col-md-4',
        }),
        new FormInput({
            key: 'detail_regarding',
            label: 'Details regarding the construction (is construction complete? If not, who is responsible for construction?)',
            required: true,
        }),
        new BaseFormGroup({
            label: 'Building map/drawing/concept and other supporting documents',
            key: 'building_support_docs',
            formGroup: [
                new FormSectionTitle({
                    label: 'Building map/drawing/concept and other supporting documents'
                }),
                new FormTitle({
                    label: 'Please upload a building map/drawing/concept here:'
                }),
                new FormFile({
                    label: 'Name of document:',
                    key: 'building_docs',
                    required: true,
                }),
                new FormTitle({
                    label: 'If building is complete, submit a photo:'
                }),
                new FormFile({
                    label: 'Name of document:',
                    key: 'building_photo',
                }),
                new BaseFormArray({
                    key: 'support_docs',
                    formArray: [
                        new FormFile({
                            key: 'document',
                            label: 'Name of document:',
                            multiple: true,
                            required: true
                        })
                    ],
                }),
            ]
        })
    ];

    landRightForm: BaseForm<string>[] = [
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location ' + (this.index + 1),
            style: { 'font-size': '18px' }
        }),
        new FormSectionTitle({
            label: '8. Particulars on buildings used/leased'
        }),
        new BaseFormArray({
            key: 'building',
            addMoreText: 'Add more buildings',
            formArray: this.building
        }),
        new FormNote({
            label: 'Please fill in one row per building.'
        }),
        new FormSectionTitle({
            label: 'All the data for location 1 is completed now. If you have another location, click the button below: ',
        })
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private landService: LandRightService,
        private formService: LandRightFormService,
        private landMMService: LandRightMMService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private localService: LocalService
    ) {
        super(formCtlService);
        this.loading = true;

        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;

        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm && this.mm == 'mm' ? this.landMMService : this.landService;
            this.index = +paramMap.get('index');
            this.landRightForm[1].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.landRightModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.landRightModel.owner = this.landRightModel.owner ? this.landRightModel?.owner || [] : form?.data?.owner || [];

                        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm, this.landRightModel?.owner[this.index] || {}, form?.data?.owner && form?.data?.owner[this.index] || {});

                        this.page = "land-right/form9/";
                        this.page += this.mm && this.mm == 'mm' ? (this.landRightModel.id + '/' + this.index + '/mm') : (this.landRightModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Land Right', this.landRightModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.landRightModel?.id, type: 'Land Right', page: this.page, comments }));
                        });
                        this.lastIndex = this.landRightModel.owner.length - 1 === this.index ? true : false;

                        this.next_label = this.lastIndex && this.mm && this.mm == 'mm' ? "NEXT SECTION" : "NEXT";

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var unit = this.building.filter(x => x.key == "unit")[0];
        unit.options$ = this.localService.getUnit(lan);

        this.authService.changeLanguage$.subscribe(x => {
            unit.options$ = this.localService.getUnit(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    moreLocation() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_more = true;
        this.is_draft = false;

        this.formCtlService.lazyUpload(this.landRightForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let owner = this.landRightModel.owner ? this.landRightModel.owner.map(x => Object.assign({}, x)) || [] : [];
            owner[this.index] = { ...owner[this.index], ...formValue };

            let data = { 'owner': owner };
            this.service.create({ ...this.landRightModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    this.saveFormProgress(this.form?.id, this.landRightModel.id, this.page);

                    const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

                    if (this.form.language == 'Myanmar') {
                        this.router.navigate([page + '/land-right/form3', this.form.id, (this.index + 1), 'mm']);
                    } else {
                        if (this.mm && this.mm == 'mm') {
                            this.router.navigate([page + '/land-right/form3', this.form.id, (this.index + 1)]);
                        } else {
                            this.router.navigate([page + '/land-right/form9/', this.form.id, this.index, 'mm']);
                        }
                    }

                    //   if (window.location.href.indexOf('officer') !== -1) {
                    //     this.router.navigate(['/officer/land-right/form3', this.id, this.landRightModel.owner.length]);
                    //   } else {
                    //     this.router.navigate(['/pages/land-right/form3', this.id, this.landRightModel.owner.length]);
                    //   }
                });
        });
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.is_more = false;
        this.saveFormProgress(this.form?.id, this.landRightModel.id, this.page);
        this.saveLandRightInfo();
    }

    saveDraft() {
        this.is_draft = true;
        this.is_more = false;
        this.saveLandRightInfo();
    }

    saveLandRightInfo() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.landRightForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let owner = this.landRightModel.owner ? this.landRightModel.owner.map(x => Object.assign({}, x)) || [] : [];
            owner[this.index] = { ...owner[this.index], ...formValue };

            let data = { 'owner': owner };
            this.service.create({ ...this.landRightModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
        const route = this.lastIndex ? '/land-right/submission/' + id : '/land-right/form3/' + id + '/' + (this.index + 1);

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + route, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + route]);
                } else {
                    this.router.navigate([page + '/land-right/form9/', id, this.index, 'mm']);
                }
            }
        }
    }
}
