import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm9Component } from './land-right-form9.component';

describe('LandRightForm9Component', () => {
  let component: LandRightForm9Component;
  let fixture: ComponentFixture<LandRightForm9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
