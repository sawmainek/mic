import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { FormCheckBox } from './../../../custom-component/dynamic-forms/base/form-checkbox';
import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';

export class LandRightForm9 {

    public forms: BaseForm<string>[] = [];
    index = 0;

    constructor(
        private localService: LocalService
    ) {
        let building: BaseForm<any>[] = [
            new FormSectionTitleCounter({
                label: 'Building'
            }),
            new FormInput({
                key: 'area',
                label: 'Area',
                required: true,
                type: 'number',
                columns: 'col-md-4',
            }),
            new FormSelect({
                key: 'unit',
                label: 'Unit of measurement',
                required: true,
                options$: this.localService.getUnit(),
                columns: 'col-md-4',
            }),
            new FormInput({
                key: 'value_building',
                label: 'Value of Building in MMK',
                required: true,
                columns: 'col-md-4',
            }),
            new FormInput({
                key: 'detail_regarding',
                label: 'Details regarding the construction (is construction complete? If not, who is responsible for construction?)',
                required: true,
            }),
            new BaseFormGroup({
                label: 'Building map/drawing/concept and other supporting documents',
                key: 'building_support_docs',
                formGroup: [
                    new FormSectionTitle({
                        label: 'Building map/drawing/concept and other supporting documents'
                    }),
                    new FormTitle({
                        label: 'Please upload a building map/drawing/concept here:'
                    }),
                    new FormFile({
                        label: 'Name of document:',
                        key: 'building_docs',
                        required: true,
                    }),
                    new FormTitle({
                        label: 'If building is complete, submit a photo:'
                    }),
                    new FormFile({
                        label: 'Name of document:',
                        key: 'building_photo',
                    }),
                    new BaseFormArray({
                        key: 'support_docs',
                        formArray: [
                            new FormFile({
                                key: 'document',
                                label: 'Name of document:',
                                multiple: true,
                                required: true
                            })
                        ],
                    }),
                ]
            })
        ];

        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location ' + (this.index + 1),
                        style: { 'font-size': '18px' }
                    }),
                    new FormSectionTitle({
                        label: '8. Particulars on buildings used/leased'
                    }),
                    new BaseFormArray({
                        key: 'building',
                        addMoreText: 'Add more buildings',
                        formArray: building
                    }),
                    new FormNote({
                        label: 'Please fill in one row per building.'
                    }),
                    new FormSectionTitle({
                        label: 'All the data for location 1 is completed now. If you have another location, click the button below: ',
                    })
                ]
            }),
            new BaseFormGroup({
                key: 'submission',
                formGroup: [
                    new FormCheckBox({
                        key: 'is_true',
                        required: true,
                        label: 'I / We hereby declare that the above statements are true and correct to the best of my/our knowledge and belief.'
                    }),
                    new FormCheckBox({
                        key: 'is_understand',
                        required: true,
                        label: 'I /We fully understand that proposal may be denied or unnecessarily delayed if the applicant fails to provide required information to the Commission for issuance of the permit.'
                    }),
                    new FormCheckBox({
                        key: 'is_strictly_comply',
                        required: true,
                        label: 'I/We hereby declare to strictly comply with terms and conditions set out by the Myanmar Investment Commission.'
                    }),
                ]
            })
            
        ];
    }
}