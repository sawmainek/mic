import { LandRightForm9 } from './../land-right-form9/land-right-form9';
import { LandRightForm8 } from './../land-right-form8/land-right-form8';
import { LandRightForm7 } from './../land-right-form7/land-right-form7';
import { LandRightForm6 } from './../land-right-form6/land-right-form6';
import { LandRightForm5 } from './../land-right-form5/land-right-form5';
import { LandRightForm4 } from './../land-right-form4/land-right-form4';
import { LandRightForm3 } from './../land-right-form3/land-right-form3';
import { LandRightForm2 } from './../land-right-form2/land-right-form2';
import { LandRightForm1 } from './../land-right-form1/land-right-form1';
import { LocationService } from './../../../../services/location.service';
import { PrintPDFService } from './../../../custom-component/print.pdf.service';
import { CPCService } from './../../../../services/cpc.service';
import { ISICService } from './../../../../services/isic.service';
import { LocalService } from './../../../../services/local.service';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { Injectable } from '@angular/core';
@Injectable()
export class LandRightPreviewService {
    forms: BaseForm<string | boolean>[] = [];
    constructor(
        public lotService: LocationService,
        public localService: LocalService,
        public isicService: ISICService,
        public cpcService: CPCService,
        public printPDFService: PrintPDFService
    ) {
        
    }

    toFormPDF(data: any, option: {
        readOnly?: boolean,
        language?: string,
        onSubmit$?: any
    }) {
        const landRightForm1 = new LandRightForm1(this.lotService);
        const landRightForm2 = new LandRightForm2();
        const landRightForm3 = new LandRightForm3(this.lotService, this.localService);
        const landRightForm4 = new LandRightForm4(this.lotService, this.localService);
        const landRightForm5 = new LandRightForm5();
        const landRightForm6 = new LandRightForm6();
        const landRightForm7 = new LandRightForm7(this.lotService);
        const landRightForm8 = new LandRightForm8(this.localService);
        const landRightForm9 = new LandRightForm9(this.localService);

        this.forms = [
            ...landRightForm1.forms,
            ...landRightForm2.forms,
            ...landRightForm3.forms,
            ...landRightForm4.forms,
            ...landRightForm5.forms,
            ...landRightForm6.forms,
            ...landRightForm7.forms,
            ...landRightForm8.forms,
            ...landRightForm9.forms,
        ];
        this.printPDFService.toFormPDF(this.forms, data, 'Land Rights Authorization (Form 7ab)', option);
    }
}