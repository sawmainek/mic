import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandRightFormComponent } from './land-right-form/land-right-form.component';
import { LandRightForm1Component } from './land-right-form1/land-right-form1.component';
import { LandRightForm2Component } from './land-right-form2/land-right-form2.component';
import { LandRightForm3Component } from './land-right-form3/land-right-form3.component';
import { LandRightForm4Component } from './land-right-form4/land-right-form4.component';
import { LandRightForm5Component } from './land-right-form5/land-right-form5.component';
import { LandRightForm6Component } from './land-right-form6/land-right-form6.component';
import { LandRightForm7Component } from './land-right-form7/land-right-form7.component';
import { LandRightForm8Component } from './land-right-form8/land-right-form8.component';
import { LandRightForm9Component } from './land-right-form9/land-right-form9.component';
import { LandRightSubmissionComponent } from './land-right-submission/land-right-submission.component';
import { LandRightSuccessComponent } from './land-right-success/land-right-success.component';
import { LandrightChooseLanguageComponent } from './landright-choose-language/landright-choose-language.component';
import { LandRightStateAndRegionComponent } from './land-right-state-and-region/land-right-state-and-region.component';
import { LandRightChooseInvestmentComponent } from './land-right-choose-investment/land-right-choose-investment.component';
import { LandRightPaymentComponent } from './land-right-payment/land-right-payment.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'landright-choose-investment', pathMatch: 'full' },
            { path: 'landright-choose-investment', component: LandRightChooseInvestmentComponent },
            { path: 'landright-choose-investment/:id', component: LandRightChooseInvestmentComponent },
            { path: 'landright-state-and-region/:id', component: LandRightStateAndRegionComponent },
            { path: 'landright-choose-language/:id', component: LandrightChooseLanguageComponent },
            { path: 'landright-choose-language/:id/:mm', component: LandrightChooseLanguageComponent },
            { path: 'form/:id', component: LandRightFormComponent },
            { path: 'form1/:id', component: LandRightForm1Component },
            { path: 'form2/:id', component: LandRightForm2Component },
            { path: 'form3/:id/:index', component: LandRightForm3Component },
            { path: 'form4/:id/:index', component: LandRightForm4Component },
            { path: 'form5/:id/:index', component: LandRightForm5Component },
            { path: 'form6/:id/:index', component: LandRightForm6Component },
            { path: 'form7/:id/:index', component: LandRightForm7Component },
            { path: 'form8/:id/:index', component: LandRightForm8Component },
            { path: 'form9/:id/:index', component: LandRightForm9Component },

            { path: 'form/:id/:mm', component: LandRightFormComponent },
            { path: 'form1/:id/:mm', component: LandRightForm1Component },
            { path: 'form2/:id/:mm', component: LandRightForm2Component },
            { path: 'form3/:id/:index/:mm', component: LandRightForm3Component },
            { path: 'form4/:id/:index/:mm', component: LandRightForm4Component },
            { path: 'form5/:id/:index/:mm', component: LandRightForm5Component },
            { path: 'form6/:id/:index/:mm', component: LandRightForm6Component },
            { path: 'form7/:id/:index/:mm', component: LandRightForm7Component },
            { path: 'form8/:id/:index/:mm', component: LandRightForm8Component },
            { path: 'form9/:id/:index/:mm', component: LandRightForm9Component },

            { path: 'submission/:id', component: LandRightSubmissionComponent },
            { path: 'submission/:id/:mm', component: LandRightSubmissionComponent },

            { path: 'success/:id', component: LandRightSuccessComponent },
            { path: 'success/:id/:mm', component: LandRightSuccessComponent },

            { path: 'payment/:id', component: LandRightPaymentComponent },            
            { path: 'payment/:id/:mm', component: LandRightPaymentComponent },
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class LandRightRoutingModule { }
