import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomComponentModule } from './../../custom-component/custom-component.module';
import { LandRightRoutingModule } from './land-right-routing.module';
import { GlobalModule } from './../global.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandRightFormComponent } from './land-right-form/land-right-form.component';
import { LandRightForm1Component } from './land-right-form1/land-right-form1.component';
import { LandRightForm2Component } from './land-right-form2/land-right-form2.component';
import { LandRightForm3Component } from './land-right-form3/land-right-form3.component';
import { LandRightForm4Component } from './land-right-form4/land-right-form4.component';
import { LandRightForm5Component } from './land-right-form5/land-right-form5.component';
import { LandRightForm6Component } from './land-right-form6/land-right-form6.component';
import { LandRightForm7Component } from './land-right-form7/land-right-form7.component';
import { LandRightForm8Component } from './land-right-form8/land-right-form8.component';
import { LandRightForm9Component } from './land-right-form9/land-right-form9.component';
import { LandRightSubmissionComponent } from './land-right-submission/land-right-submission.component';
import { LandRightSuccessComponent } from './land-right-success/land-right-success.component';
import { LandrightChooseLanguageComponent } from './landright-choose-language/landright-choose-language.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { LandRightSideMenuComponent } from './land-right-side-menu/land-right-side-menu.component';
import { LandRightStateAndRegionComponent } from './land-right-state-and-region/land-right-state-and-region.component';
import { LandRightChooseInvestmentComponent } from './land-right-choose-investment/land-right-choose-investment.component';
import { LandRightPaymentComponent } from './land-right-payment/land-right-payment.component';
@NgModule({
  declarations: [
        LandRightFormComponent,
        LandRightForm1Component,
        LandRightForm2Component,
        LandRightForm3Component,
        LandRightForm4Component,
        LandRightForm5Component,
        LandRightForm6Component,
        LandRightForm7Component,
        LandRightForm8Component,
        LandRightForm9Component,
        LandRightSubmissionComponent,
        LandRightSuccessComponent,
        LandrightChooseLanguageComponent,
        LandRightSideMenuComponent,
        LandRightStateAndRegionComponent,
        LandRightChooseInvestmentComponent,
        LandRightPaymentComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        GlobalModule,
        LandRightRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // closeButton: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ],
    exports: [
        LandRightFormComponent,
        LandRightForm1Component,
        LandRightForm2Component,
        LandRightForm3Component,
        LandRightForm4Component,
        LandRightForm5Component,
        LandRightForm6Component,
        LandRightForm7Component,
        LandRightForm8Component,
        LandRightForm9Component,
        LandRightSubmissionComponent,
        LandRightSuccessComponent,
        LandrightChooseLanguageComponent,
        LandRightSideMenuComponent,
        LandRightStateAndRegionComponent,
        LandRightChooseInvestmentComponent,
    ]
    , providers: [
        TranslateService,
    ],
})
export class LandRightModule { }
