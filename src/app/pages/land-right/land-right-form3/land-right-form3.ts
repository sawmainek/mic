import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

export class LandRightForm3 {

    public forms: BaseForm<string>[] = [];
    index: any = 0;

    constructor(
        private lotService: LocationService,
        private localService: LocalService,
    ) {
        let individual: BaseForm < string > [] =[
            new FormInput({
                key: 'full_name',
                label: 'c. Full Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'passport_or_nrc',
                label: 'd. National Registration Card No.',
                required: true,
                columns: 'col-md-6'
            }),
            new FormTitle({
                label: 'e. Address'
            }),
            new FormSelect({
                key: 'individual_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'individual_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'individual_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'individual_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'individual_country',
                    value: ['Myanmar'],
                }
            }),        
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormInput({
                key: 'phone_no',
                label: 'f.  Phone number',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'g. E-mail address',
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let company: BaseForm < string > [] =[
            new FormInput({
                key: 'company_name',
                label: 'c. Name of company',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'company_registration_no',
                label: 'd. Company Registration No.',
                required: true,
                columns: 'col-md-6'
            }),
            new FormTitle({
                label: 'e. Address'
            }),
            new FormSelect({
                key: 'company_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'company_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormTitle({
                label: 'f. Contact person'
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                type: 'number',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let government: BaseForm < string > [] =[
            new FormInput({
                key: 'name',
                label: 'c. Name of government department / organization',
                required: true
            }),
            new FormTitle({
                label: 'd. Address '
            }),
            new FormSelect({
                key: 'government_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'government_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'government_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'government_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'government_township',
                label: 'Township:',
                options$: this.lotService.getTownship(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'government_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'government_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
                required: true
            }),  
            new FormTitle({
                label: 'e. Contact person',
            }),
            new FormInput({
                key: 'contact_full_name',
                label: 'Full Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_phone_no',
                label: 'Phone number',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'contact_email',
                label: 'E-mail Address',
                required: true,
                validators: [Validators.email],
                columns: 'col-md-6'
            }),
        ];

        let owner_land: BaseForm < string > [] =[
            new FormNote({
                label: 'Please fill out the following information for each specific location of the investment.'
            }),
            new FormSectionTitle({
                label: 'Location ' + (this.index + 1),
                style: { 'font-size': '18px' }
            }),
            new FormSectionTitle({
                label: '3. Owner of land and/or building(s)'
            }),
            new FormTitle({
                label: 'Please upload evidence of land ownership/ land use rights here (unless location is in an Industrial Zone):',
            }),
            new FormFile({
                key: 'land_evidence_docs',
                label: 'File name:',
                required: true,
            }),
            new FormTitle({
                label: 'If the current owner has not changed the name of the former owner on the ownership evidence, please upload contract/agreement (e.g. special power, general power, etc.) between former owner and current owner:',
            }),
            new FormFile({
                label: 'File name:',
                key: 'contract_docs',
                required: true,
            }),
            new FormTitle({
                label: 'a. Period of land grant validity / initial period permitted to use the land'
            }),
            new FormDate({
                key: 'land_grant_start_date',
                label: 'From Date',
                required: true,
                columns: 'col-md-6',
            }),
            new FormDate({
                key: 'land_grant_end_date',
                label: 'To Date',
                required: true,
                columns: 'col-md-6',
            }),
            new FormSelect({
                key: 'owner_type_id',
                label: 'b. Type of owner',
                placeholder: 'Select owner type',
                options$: this.localService.getType(),
                required: true,
            }),
            new BaseFormGroup({
                key: 'individual',
                formGroup: individual,
                criteriaValue: {
                    key: 'owner_type_id',
                    value: 'Individual'
                }
            }),
            new BaseFormGroup({
                key: 'company',
                formGroup: company,
                criteriaValue: {
                    key: 'owner_type_id',
                    value: 'Company'
                }
            }),
            new BaseFormGroup({
                key: 'government',
                formGroup: government,
                criteriaValue: {
                    key: 'owner_type_id',
                    value: 'Government department/organization'
                }
            })
        ];

        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new BaseFormGroup({
                        key: 'owner_land',
                        formGroup: owner_land,
                    })
                ]
            })
            
        ];
    }
}