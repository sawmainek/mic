import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm3Component } from './land-right-form3.component';

describe('LandRightForm3Component', () => {
  let component: LandRightForm3Component;
  let fixture: ComponentFixture<LandRightForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
