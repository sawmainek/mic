import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LandRightService } from 'src/services/land_right.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { LandRight } from 'src/app/models/land-right';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { Location } from '@angular/common';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { LandRightFormData } from '../land-right-formdata';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
declare var jQuery: any;
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { LocalService } from 'src/services/local.service';
import { cloneDeep } from 'lodash';
import { LocationService } from 'src/services/location.service';

@Component({
    selector: 'app-land-right-form3',
    templateUrl: './land-right-form3.component.html',
    styleUrls: ['./land-right-form3.component.scss']
})
export class LandRightForm3Component extends LandRightFormData implements OnInit {
    index = Number(this.activatedRoute.snapshot.paramMap.get('index'));
    menu: any = "formData";
    page = "land-right/form3/";
    modelId: any;
    @Input() id: any;
    mm: any;

    user: any = {};

    landRightModel: any = {};
    formGroup: FormGroup;
    form: any;
    service: any;

    submitted = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    individual: BaseForm<string>[] = [
        new FormInput({
            key: 'full_name',
            label: 'c. Full Name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'passport_or_nrc',
            label: 'd. National Registration Card No.',
            required: true,
            columns: 'col-md-6'
        }),
        new FormTitle({
            label: 'e. Address'
        }),
        new FormSelect({
            key: 'individual_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'individual_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'individual_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'individual_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'individual_township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'individual_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'individual_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormInput({
            key: 'phone_no',
            label: 'f.  Phone number',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'g. E-mail address',
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ];
    company: BaseForm<string>[] = [
        new FormInput({
            key: 'company_name',
            label: 'c. Name of company',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'company_registration_no',
            label: 'd. Company Registration No.',
            required: true,
            columns: 'col-md-6'
        }),
        new FormTitle({
            label: 'e. Address'
        }),
        new FormSelect({
            key: 'company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormTitle({
            label: 'f. Contact person'
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full Name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_phone_no',
            label: 'Phone number',
            type: 'number',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ];
    government: BaseForm<string>[] = [
        new FormInput({
            key: 'name',
            label: 'c. Name of government department / organization',
            required: true
        }),
        new FormTitle({
            label: 'd. Address '
        }),
        new FormSelect({
            key: 'government_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'government_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'government_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'government_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'government_township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'government_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'government_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'address',
            label: 'Additional address details',
            required: true
        }),
        new FormTitle({
            label: 'e. Contact person',
        }),
        new FormInput({
            key: 'contact_full_name',
            label: 'Full Name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_phone_no',
            label: 'Phone number',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'contact_email',
            label: 'E-mail Address',
            required: true,
            validators: [Validators.email],
            columns: 'col-md-6'
        }),
    ];
    owner_land: BaseForm<string>[] = [
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location ' + (this.index + 1),
            style: { 'font-size': '18px' }
        }),
        new FormSectionTitle({
            label: '3. Owner of land and/or building(s)'
        }),
        new FormTitle({
            label: 'Please upload evidence of land ownership/ land use rights here (unless location is in an Industrial Zone):',
        }),
        new FormFile({
            key: 'land_evidence_docs',
            label: 'File name:',
            required: true,
        }),
        new FormTitle({
            label: 'If the current owner has not changed the name of the former owner on the ownership evidence, please upload contract/agreement (e.g. special power, general power, etc.) between former owner and current owner:',
        }),
        new FormFile({
            label: 'File name:',
            key: 'contract_docs',
            required: true,
        }),
        new FormTitle({
            label: 'a. Period of land grant validity / initial period permitted to use the land'
        }),
        new FormDate({
            key: 'land_grant_start_date',
            label: 'From Date',
            required: true,
            columns: 'col-md-6',
        }),
        new FormDate({
            key: 'land_grant_end_date',
            label: 'To Date',
            required: true,
            columns: 'col-md-6',
        }),
        new FormSelect({
            key: 'owner_type_id',
            label: 'b. Type of owner',
            placeholder: 'Select owner type',
            options$: this.localService.getType(),
            required: true,
        }),
        new BaseFormGroup({
            key: 'individual',
            formGroup: this.individual,
            criteriaValue: {
                key: 'owner_type_id',
                value: 'Individual'
            }
        }),
        new BaseFormGroup({
            key: 'company',
            formGroup: this.company,
            criteriaValue: {
                key: 'owner_type_id',
                value: 'Company'
            }
        }),
        new BaseFormGroup({
            key: 'government',
            formGroup: this.government,
            criteriaValue: {
                key: 'owner_type_id',
                value: 'Government department/organization'
            }
        })
    ];
    landRightForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'owner_land',
            formGroup: this.owner_land,
        })
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private landService: LandRightService,
        private formService: LandRightFormService,
        private landMMService: LandRightMMService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
        private localService: LocalService,
        private lotService: LocationService
    ) {
        super(formCtlService);
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm && this.mm == 'mm' ? this.landMMService : this.landService;
            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.landRightModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.landRightModel.owner = this.landRightModel?.owner || form?.data?.owner || [];
                        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm, this.landRightModel.owner[this.index] || form?.data?.owner && form?.data?.owner[this.index] || {});

                        this.page = "land-right/form3/";
                        this.page += this.mm && this.mm == 'mm' ? (this.landRightModel.id + '/' + this.index + '/mm') : (this.landRightModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Land Right', this.landRightModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.landRightModel?.id, type: 'Land Right', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var owner_type_id = this.owner_land.filter(x => x.key == "owner_type_id")[0];
        owner_type_id.options$ = this.localService.getType(lan);

        this.authService.changeLanguage$.subscribe(x => {
            owner_type_id.options$ = this.localService.getType(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }
    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveLandRightInfo();
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveLandRightInfo();
    }

    saveLandRightInfo() {
        this.formCtlService.lazyUpload(this.landRightForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let owner = this.landRightModel.owner ? this.landRightModel.owner.map(x => Object.assign({}, x)) || [] : [];
            owner[this.index] = { ...owner[this.index], ...formValue };

            let data = { 'owner': owner };
            this.service.create({ ...this.landRightModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.landRightModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';


        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/form4', id, this.index, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/land-right/form4', id, this.index]);
                } else {
                    this.router.navigate([page + '/land-right/form3', id, this.index, 'mm']);
                }
            }
        }
    }

}
