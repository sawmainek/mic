import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { LocationService } from 'src/services/location.service';

export class LandRightForm7{
    
    public forms: BaseForm<string>[] = [];
    index = 0;

    constructor(
        private lotService: LocationService,
    ) {
        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location ' + (this.index + 1),
                        style: { 'font-size': '18px' }
                    }),
                    new BaseFormGroup({
                        key: 'location',
                        formGroup: [
                            new FormSectionTitle({
                                label: '6. Location'
                            }),
                            new FormNote({
                                label: 'This information should be as per land grant or land rights evidence.'
                            }),
                            new FormSectionTitle({
                                label: 'a.  Address'
                            }),
                            new FormSelect({
                                key: 'state',
                                label: 'State / Region:',
                                options$: this.lotService.getState(),
                                required: true,
                                columns: 'col-md-6',
                            }),
                            new FormSelect({
                                key: 'district',
                                label: 'District:',
                                options$: this.lotService.getDistrict(),
                                required: true,
                                columns: 'col-md-6',
                                filter: {
                                    parent: 'state',
                                    key: 'state'
                                }
                            }),
                            new FormSelect({
                                key: 'township',
                                label: 'Township:',
                                options$: this.lotService.getTownship(),
                                required: true,
                                columns: 'col-md-6',
                                filter: {
                                    parent: 'district',
                                    key: 'district'
                                }
                            }),
                            new FormInput({
                                label: 'Ward:',
                                key: 'ward',
                                required: true,
                                columns: 'col-md-6',
                            }),
                            new FormTextArea({
                                label: 'Additional address details:',
                                key: 'address_detail',
                                required: true,
                            }),
                            new FormTitle({
                                label: 'b. Please indicate if the location is in a relevant business zone:'
                            }),
                            new FormRadio({
                                label: 'Not in a business zone',
                                key: 'zone_type',
                                required: true,
                                value: 'Not in a business zone',
                                columns: 'col-md-4',
                            }),
                            new FormRadio({
                                label: 'Industrial zone',
                                key: 'zone_type',
                                required: true,
                                value: 'Industrial zone',
                                columns: 'col-md-4'
                            }),
                            new FormRadio({
                                label: 'Hotel zone',
                                key: 'zone_type',
                                required: true,
                                value: 'Hotel zone',
                                columns: 'col-md-4'
                            }),
                            new FormRadio({
                                label: 'Trade zone',
                                key: 'zone_type',
                                required: true,
                                value: 'Trade zone',
                                columns: 'col-md-4'
                            }),
                            new FormRadio({
                                label: 'If other',
                                key: 'zone_type',
                                required: true,
                                value: 'Other',
                                columns: 'col-md-4'
                            }),


                            new FormInput({
                                label: 'Describe chosen zone specifically',
                                key: 'zone_description',
                                required: true,
                                criteriaValue: {
                                    key: 'zone_type',
                                    value: ['Industrial zone', 'Hotel zone', 'Trade zone', 'Other']
                                }
                            }),
                        ]
                    }),
                ]
            })
            
        ];
    }
}