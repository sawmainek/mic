import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LandRightService } from 'src/services/land_right.service';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { Location } from '@angular/common';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { LandRightFormData } from '../land-right-formdata';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-land-right-form7',
    templateUrl: './land-right-form7.component.html',
    styleUrls: ['./land-right-form7.component.scss']
})
export class LandRightForm7Component extends LandRightFormData implements OnInit {

    menu: any = "formData";
    page = "land-right/form7/";
    modelId: any;
    mm: any = null;

    user: any = {};

    form: any;
    service: any;
    @Input() id: any;
    index = 0;
    landRightModel: any = {};
    formGroup: FormGroup;
    submitted = false;

    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    landRightForm: BaseForm<string>[] = [
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location ' + (this.index + 1),
            style: { 'font-size': '18px' }
        }),
        new BaseFormGroup({
            key: 'location',
            formGroup: [
                new FormSectionTitle({
                    label: '6. Location'
                }),
                new FormNote({
                    label: 'This information should be as per land grant or land rights evidence.'
                }),
                new FormSectionTitle({
                    label: 'a.  Address'
                }),
                new FormSelect({
                    key: 'state',
                    label: 'State / Region:',
                    options$: this.lotService.getState(),
                    required: true,
                    columns: 'col-md-6',
                }),
                new FormSelect({
                    key: 'district',
                    label: 'District:',
                    options$: this.lotService.getDistrict(),
                    required: true,
                    columns: 'col-md-6',
                    filter: {
                        parent: 'state',
                        key: 'state'
                    }
                }),
                new FormSelect({
                    key: 'township',
                    label: 'Township:',
                    options$: this.lotService.getTownship(),
                    required: true,
                    columns: 'col-md-6',
                    filter: {
                        parent: 'district',
                        key: 'district'
                    }
                }),
                new FormInput({
                    label: 'Ward:',
                    key: 'ward',
                    required: true,
                    columns: 'col-md-6',
                }),
                new FormTextArea({
                    label: 'Additional address details:',
                    key: 'address_detail',
                    required: true,
                }),
                new FormTitle({
                    label: 'b. Please indicate if the location is in a relevant business zone:'
                }),
                new FormRadio({
                    label: 'Not in a business zone',
                    key: 'zone_type',
                    required: true,
                    value: 'Not in a business zone',
                    columns: 'col-md-4',
                }),
                new FormRadio({
                    label: 'Industrial zone',
                    key: 'zone_type',
                    required: true,
                    value: 'Industrial zone',
                    columns: 'col-md-4'
                }),
                new FormRadio({
                    label: 'Hotel zone',
                    key: 'zone_type',
                    required: true,
                    value: 'Hotel zone',
                    columns: 'col-md-4'
                }),
                new FormRadio({
                    label: 'Trade zone',
                    key: 'zone_type',
                    required: true,
                    value: 'Trade zone',
                    columns: 'col-md-4'
                }),
                new FormRadio({
                    label: 'If other',
                    key: 'zone_type',
                    required: true,
                    value: 'Other',
                    columns: 'col-md-4'
                }),


                new FormInput({
                    label: 'Describe chosen zone specifically',
                    key: 'zone_description',
                    required: true,
                    criteriaValue: {
                        key: 'zone_type',
                        value: ['Industrial zone', 'Hotel zone', 'Trade zone', 'Other']
                    }
                }),
            ]
        }),
    ];

    constructor(
        private http: HttpClient,
        private lotService: LocationService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private landService: LandRightService,
        private formService: LandRightFormService,
        private landMMService: LandRightMMService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService
    ) {
        super(formCtlService);
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm && this.mm == 'mm' ? this.landMMService : this.landService;
            this.index = +paramMap.get('index');
            this.landRightForm[1].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.landRightModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.landRightModel.owner = this.landRightModel.owner ? this.landRightModel?.owner || [] : form?.data?.owner || [];

                        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm, this.landRightModel.owner[this.index] || {}, form?.data?.owner && form?.data?.owner[this.index] || {});

                        this.page = "land-right/form7/";
                        this.page += this.mm && this.mm == 'mm' ? (this.landRightModel.id + '/' + this.index + '/mm') : (this.landRightModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Land Right', this.landRightModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.landRightModel?.id, type: 'Land Right', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var state = this.landRightForm[2].formGroup.filter(x => x.key == "state")[0];
        state.options$ = this.lotService.getState(lan);

        var district = this.landRightForm[2].formGroup.filter(x => x.key == "district")[0];
        district.options$ = this.lotService.getDistrict(lan);

        var township = this.landRightForm[2].formGroup.filter(x => x.key == "township")[0];
        township.options$ = this.lotService.getTownship(lan);

        this.authService.changeLanguage$.subscribe(x => {
            state.options$ = this.lotService.getState(x);
            district.options$ = this.lotService.getDistrict(x);
            township.options$ = this.lotService.getTownship(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveLandRightInfo();
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveLandRightInfo();
    }

    saveLandRightInfo() {
        this.formCtlService.lazyUpload(this.landRightForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let owner = this.landRightModel.owner ? this.landRightModel.owner.map(x => Object.assign({}, x)) || [] : [];
            owner[this.index] = { ...owner[this.index], ...formValue };

            let data = { 'owner': owner };
            this.service.create({ ...this.landRightModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.landRightModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/form8', id, this.index, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/land-right/form8', id, this.index]);
                } else {
                    this.router.navigate([page + '/land-right/form7', id, this.index, 'mm']);
                }
            }
        }
    }
}
