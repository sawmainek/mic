import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm7Component } from './land-right-form7.component';

describe('LandRightForm7Component', () => {
  let component: LandRightForm7Component;
  let fixture: ComponentFixture<LandRightForm7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
