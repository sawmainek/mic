import { FormProgress } from './../../../core/form/_actions/form.actions';
import { AppState } from './../../../core/reducers/index';
import { Store } from '@ngrx/store';
import { Component, Input, OnInit } from '@angular/core';
import { LandRight } from 'src/app/models/land-right';
import { AuthService } from 'src/services/auth.service';
import { LandRightService } from 'src/services/land_right.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LandRightLanguage } from './landright-choose-language';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { forkJoin } from 'rxjs';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { FormProgressService } from 'src/services/form-progress.service';

@Component({
    selector: 'app-landright-choose-language',
    templateUrl: './landright-choose-language.component.html',
    styleUrls: ['./landright-choose-language.component.scss']
})
export class LandrightChooseLanguageComponent extends LandRightLanguage implements OnInit {
    @Input() id: any;
    page = "Choose Language";
    language: any;

    user: any = {};
    landRightModel: any = {};
    landRightMMModel: any = {};
    form: any;

    loading = true;
    spinner: boolean = false;
    isDraft: boolean = false;

    constructor(
        private store: Store<AppState>,
        private authService: AuthService,
        private landRightservice: LandRightService,
        private formService: LandRightFormService,
        private landRightMMService: LandRightMMService,
        private router: Router,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private activatedRoute: ActivatedRoute,
    ) {
        super(formCtlService);

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                if (this.user) {
                    this.getFormData();
                }
            });
        }).unsubscribe();


    }

    getFormData() {
        if (this.id) {
            this.formService.show(this.id)
                .subscribe(result => {
                    this.form = result || {};
                    this.language = this.form?.language || 'Myanmar';
                    this.openLanguage(this.language || 'Myanmar');
                    this.landRightModel = this.form?.data || null;
                    this.landRightMMModel = this.form?.data_mm || null;

                    this.loading = false;
                })
        } else {
            this.formService.get({ search: `user_id:equal:${this.user.id}|type:Land Right` })
                .subscribe(results => {
                    this.form = results[0] || {};
                    this.language = this.form?.language || 'Myanmar';
                    this.openLanguage(this.language || 'Myanmar');
                    this.landRightModel = this.form?.data || null;
                    this.landRightMMModel = this.form?.data_mm || null;

                    this.loading = false;
                })
        }
    }

    ngOnInit(): void { }

    openLanguage(language) {
        if (window.location.href.indexOf('officer') == -1) {
            this.language = language;
        }
    }

    goToHome() {
        this.router.navigate(['/']);
    }

    getProgressData(form_id, model_id) {
        this.formProgressService.get({
            page: 1,
            rows: 100,
            search: `form_id:equal:${form_id || 0}|type:equal:Land Right` // For New Flow this.id => this.micModel.id
        }).subscribe((progress: any) => {
            this.getProgressForms(progress || [])
            this.store.dispatch(new FormProgress({ progress }));
            this.saveFormProgress(this.form?.id, model_id, this.page);
        });
    }

    onSubmit() {
        this.spinner = true;
        this.form.user_id = this.form.user_id || this.user.id;
        this.form.type = 'Land Right';
        this.form.language = this.language;
        this.formService.create(this.form)
            .subscribe(result => {
                this.form = result;
                if (this.language == 'Myanmar') {
                    this.landRightMMModel = {
                        ...this.landRightMMModel || {},
                        ...{ user_id: this.form.user_id || this.user.id, form_id: this.form.id, choose_language: this.language, land_to_state: this.form.state, apply_to: this.form.apply_to }
                    } as LandRight;
                    this.landRightMMService.create(this.landRightMMModel).subscribe(result => {
                        this.form.data_mm = result;

                        this.page = `land-right/choose-language/${result.id}/mm`;
                        this.getProgressData(this.form.id, result.id);

                        this.redirectLink(this.form.id);
                    })
                } else {
                    // For both English / Myanmar
                    this.landRightModel = {
                        ...this.landRightModel || {},
                        ...{ user_id: this.form.user_id || this.user.id, form_id: this.form.id, choose_language: this.language, land_to_state: this.form.state }
                    } as LandRight;

                    this.landRightMMModel = {
                        ...this.landRightMMModel || {},
                        ...{ user_id: this.form.user_id || this.user.id, form_id: this.form.id, choose_language: this.language, land_to_state: this.form.state }
                    } as LandRight;
                    forkJoin([
                        this.landRightservice.create(this.landRightModel),
                        this.landRightMMService.create(this.landRightMMModel)
                    ])
                        .subscribe(results => {
                            this.page = `land-right/choose-language/${results[0].id}`;
                            this.getProgressData(this.form.id, results[0].id);

                            this.page = `land-right/choose-language/${results[1].id}/mm`;
                            this.getProgressData(this.form.id, results[1].id);

                            this.redirectLink(this.form.id);
                        });
                }
            });
    }

    redirectLink(id: number) {
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form.language == 'Myanmar') {
                this.router.navigate(['/officer/land-right/form', id, 'mm']);
            }
            else {
                this.router.navigate(['/officer/land-right/form', id]);
            }

        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate(['/pages/land-right/form', id, 'mm']);
            }
            else {
                this.router.navigate(['/pages/land-right/form', id]);
            }
        }
    }

}
