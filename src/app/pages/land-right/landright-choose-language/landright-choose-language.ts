import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { LandRightProgress } from '../land-right-progress';

export class LandRightLanguage extends LandRightProgress {
    static section = "Choose Language";
    static totalForm = 1;

    constructor(public formCtlService: FormControlService) {
        super();
    }

    saveFormProgress(form_id: number, id: number, page: string) {
        if (this.progressForms?.length > 0) {
            if (this.progressForms.filter(x => x.type == LandRightProgress.type && x.section == LandRightLanguage.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: LandRightProgress.type,
                    type_id: id,
                    section: LandRightLanguage.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }

        }
        else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: LandRightLanguage.type,
                type_id: id,
                section: LandRightLanguage.section,
                page: page,
                status: true,
                revise: false
            });
        }
    }
}