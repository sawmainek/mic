import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandrightChooseLanguageComponent } from './landright-choose-language.component';

describe('LandrightChooseLanguageComponent', () => {
  let component: LandrightChooseLanguageComponent;
  let fixture: ComponentFixture<LandrightChooseLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandrightChooseLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandrightChooseLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
