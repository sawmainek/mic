import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightForm6Component } from './land-right-form6.component';

describe('LandRightForm6Component', () => {
  let component: LandRightForm6Component;
  let fixture: ComponentFixture<LandRightForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

