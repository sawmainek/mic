import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { BaseFormData } from 'src/app/core/form';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { LandRightService } from 'src/services/land_right.service';
import { LandRightFormData } from '../land-right-formdata';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
declare var jQuery: any;
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-land-right-form6',
    templateUrl: './land-right-form6.component.html',
    styleUrls: ['./land-right-form6.component.scss']
})
export class LandRightForm6Component extends LandRightFormData implements OnInit {
    menu: any = "formData";
    page = "land-right/form6/";
    modelId: any;
    index = 0;
    @Input() id: any;
    mm: any;

    user: any = {};

    landRightModel: any = {};
    formGroup: FormGroup;
    form: any;
    service: any;

    submitted = false;
    spinner = false;
    is_draft = false;
    loading = false;

    isRevision: boolean = false;

    lease_agreement: BaseForm<string>[] = [
        new FormSectionTitle({
            label: '5.  Particulars on lease agreement'
        }),
        new FormFile({
            label: 'Please upload the draft of the lease agreement:',
            key: 'draft_lease_docs',
            required: true,
        }),
        new FormNote({
            label: 'Registered final copy should be submitted to DICA once received or after receiving the permit/endorsement.'
        }),
        new FormInput({
            key: 'lessee_name',
            label: 'i. Name of lessee',
            required: true,
        }),
        new FormTitle({
            label: 'ii. Proposed lease period',
        }),
        new FormDate({
            label: 'From Date',
            key: 'proposed_lease_start_date',
            required: true,
            columns: 'col-md-6',
        }),
        new FormDate({
            label: 'To Date',
            key: 'proposed_lease_end_date',
            required: true,
            columns: 'col-md-6',
        }),
        new FormTitle({
            label: 'iii. Specify fees(s)',
        }),
        new FormInput({
            key: 'annual_fees',
            label: 'i. Annual rental fees of land and/or buildings',
            required: true,
            type: 'number',
        }),
        new FormInput({
            key: 'rental_per_year',
            label: 'ii. Rental fees of 1 square meter per year of land and/or buildings.',
            required: true,
            type: 'number',
        }),
        new FormInput({
            key: 'land_used_premium_fees',
            label: 'iii. Land Use Premium fees (rate per acre):',
            required: true,
            type: 'number',
        }),
        new FormNote({
            label: 'If the land and/or buildings belong to a Government Department/Organization, the LUP shall be paid in cash (Kyat) by the lessee.'
        }),
        new FormTitle({
            label: 'iv. Is the payment or part of the payment in form of capital?'
        }),
        new FormRadio({
            label: 'Yes',
            key: 'is_payment_or_part',
            required: true,
            value: 'Yes',
            columns: 'col-md-4'
        }),
        new FormRadio({
            label: 'No',
            key: 'is_payment_or_part',
            required: true,
            value: 'No',
            columns: 'col-md-8'
        }),
        new FormInput({
            key: 'choose_desc',
            label: 'Describe specifically to choosing yes',
            required: true,
            criteriaValue: {
                key: 'is_payment_or_part',
                value: 'Yes'
            }
        }),
    ];

    landRightForm: BaseForm<string>[] = [
        new FormNote({
            label: 'Please fill out the following information for each specific location of the investment.'
        }),
        new FormSectionTitle({
            label: 'Location ' + (this.index + 1),
            style: { 'font-size': '18px' }
        }),
        new BaseFormGroup({
            key: 'lease_agreement',
            formGroup: this.lease_agreement,
        }),
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private landService: LandRightService,
        private formService: LandRightFormService,
        private landMMService: LandRightMMService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm);
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm && this.mm == 'mm' ? this.landMMService : this.landService;
            this.index = +paramMap.get('index');
            this.landRightForm[1].label = 'Location ' + (this.index + 1);
            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.landRightModel = this.mm && this.mm == 'mm' ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.landRightModel.owner = this.landRightModel.owner ? this.landRightModel?.owner || [] : form?.data?.owner || [];

                        this.formGroup = this.formCtlService.toFormGroup(this.landRightForm, this.landRightModel.owner[this.index] || {}, form?.data?.owner && form?.data?.owner[this.index] || {});

                        this.page = "land-right/form6/";
                        this.page += this.mm && this.mm == 'mm' ? (this.landRightModel.id + '/' + this.index + '/mm') : (this.landRightModel.id + '/' + this.index);

                        this.formCtlService.getComments$('Land Right', this.landRightModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.landRightModel?.id, type: 'Land Right', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
        console.log(this.formGroup);
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
        // var lan = this.mm || 'en';
        // localStorage.setItem('lan', lan);
        // this.translateService.use(lan);
        // this.authService.changeLanguage$.next(lan);
        // this.cdr.detectChanges();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.spinner = true;
        this.is_draft = false;
        this.saveLandRightInfo();
    }

    saveDraft() {
        this.spinner = true;
        this.is_draft = true;
        this.saveLandRightInfo();
    }

    saveLandRightInfo() {
        this.formCtlService.lazyUpload(this.landRightForm, this.formGroup, { type_id: this.id }, (formValue) => {
            let owner = this.landRightModel.owner ? this.landRightModel.owner.map(x => Object.assign({}, x)) || [] : [];
            owner[this.index] = { ...owner[this.index], ...formValue };

            let data = { 'owner': owner };
            this.service.create({ ...this.landRightModel, ...data }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.landRightModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/land-right/form7', id, this.index, 'mm']);
            } else {
                if (this.mm && this.mm == 'mm') {
                    this.router.navigate([page + '/land-right/form7', id, this.index]);
                } else {
                    this.router.navigate([page + '/land-right/form6', id, this.index, 'mm']);
                }
            }
        }
    }
}
