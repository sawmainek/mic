import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';

export class LandRightForm6 {

    public forms: BaseForm<string>[] = [];
    index = 0;

    constructor(

    ) {
        let lease_agreement: BaseForm < string > [] =[
            new FormSectionTitle({
                label: '5.  Particulars on lease agreement'
            }),
            new FormFile({
                label: 'Please upload the draft of the lease agreement:',
                key: 'draft_lease_docs',
                required: true,
            }),
            new FormNote({
                label: 'Registered final copy should be submitted to DICA once received or after receiving the permit/endorsement.'
            }),
            new FormInput({
                key: 'lessee_name',
                label: 'i. Name of lessee',
                required: true,
            }),
            new FormTitle({
                label: 'ii. Proposed lease period',
            }),
            new FormDate({
                label: 'From Date',
                key: 'proposed_lease_start_date',
                required: true,
                columns: 'col-md-6',
            }),
            new FormDate({
                label: 'To Date',
                key: 'proposed_lease_end_date',
                required: true,
                columns: 'col-md-6',
            }),
            new FormTitle({
                label: 'iii. Specify fees(s)',
            }),
            new FormInput({
                key: 'annual_fees',
                label: 'i. Annual rental fees of land and/or buildings',
                required: true,
                type: 'number',
            }),
            new FormInput({
                key: 'rental_per_year',
                label: 'ii. Rental fees of 1 square meter per year of land and/or buildings.',
                required: true,
                type: 'number',
            }),
            new FormInput({
                key: 'land_used_premium_fees',
                label: 'iii. Land Use Premium fees (rate per acre):',
                required: true,
                type: 'number',
            }),
            new FormNote({
                label: 'If the land and/or buildings belong to a Government Department/Organization, the LUP shall be paid in cash (Kyat) by the lessee.'
            }),
            new FormTitle({
                label: 'iv. Is the payment or part of the payment in form of capital?'
            }),
            new FormRadio({
                label: 'Yes',
                key: 'is_payment_or_part',
                required: true,
                value: 'Yes',
                columns: 'col-md-4'
            }),
            new FormRadio({
                label: 'No',
                key: 'is_payment_or_part',
                required: true,
                value: 'No',
                columns: 'col-md-8'
            }),
            new FormInput({
                key: 'choose_desc',
                label: 'Describe specifically to choosing yes',
                required: true,
                criteriaValue: {
                    key: 'is_payment_or_part',
                    value: 'Yes'
                }
            }),
        ];

        this.forms = [
            new BaseFormArray({
                key: 'owner',
                formArray: [
                    new FormNote({
                        label: 'Please fill out the following information for each specific location of the investment.'
                    }),
                    new FormSectionTitle({
                        label: 'Location ' + (this.index + 1),
                        style: { 'font-size': '18px' }
                    }),
                    new BaseFormGroup({
                        key: 'lease_agreement',
                        formGroup: lease_agreement,
                    })
                ]
            })
            
        ];
    }
}