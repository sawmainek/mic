import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightChooseInvestmentComponent } from './land-right-choose-investment.component';

describe('LandRightChooseInvestmentComponent', () => {
  let component: LandRightChooseInvestmentComponent;
  let fixture: ComponentFixture<LandRightChooseInvestmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightChooseInvestmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightChooseInvestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
