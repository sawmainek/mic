import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LandRight } from 'src/app/models/land-right';
import { AuthService } from 'src/services/auth.service';
import { LandRightFormService } from 'src/services/land-right-form.service';

@Component({
    selector: 'app-land-right-choose-investment',
    templateUrl: './land-right-choose-investment.component.html',
    styleUrls: ['./land-right-choose-investment.component.scss']
})
export class LandRightChooseInvestmentComponent implements OnInit {
    @Input() id: any;
    landRightForm: FormGroup;
    landRightModel: LandRight;
    user: any = {};
    form: any = {};

    loaded: boolean = false;
    loading = true;
    submitted = false;
    spinner: boolean = false;
    isDraft: boolean = false;

    constructor(
        private router: Router,
        private landFormService: LandRightFormService,
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private toast: ToastrService,
        private activatedRoute: ActivatedRoute,
        public location: Location,
    ) {       

        this.landRightForm = this.formBuilder.group({
            apply_to: ['Myanmar Investment Commision']
        });

    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            
            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                this.loading = this.user ? true : false;
    
                if (this.user) {
                    this.getFormData();
                }
            });
        }).unsubscribe();
    }

    getFormData() {
        if (this.id > 0) {
            this.landFormService.show(this.id)
                .subscribe(result => {
                    this.form = result || {};
                    this.landRightForm.patchValue(this.form || {});

                    this.loading = false;
                })
        } else {
            this.loading = false;
        }
    }

    openChoose(choose) {
        if( window.location.href.indexOf('officer') == -1) {
            this.landRightForm.get('apply_to').setValue(choose);
        }        
    }

    onSubmit() {
        this.spinner = true;
        //For New Flow
        this.form.user_id = this.form?.user_id || this.user.id;
        this.form.apply_to = this.landRightForm.get('apply_to').value;
        this.form.state = this.landRightForm.get('apply_to').value === 'States and Regions Investment Committees' ? this.form?.state : null;
        this.form.status = this.form.status || 'Draft';
        this.landFormService.create(this.form)
            .subscribe(result => {
                this.form = result;
                this.redirectLink(this.form.id);
            })
    }

    redirectLink(id: number) {
        if (id == null) {
            this.toast.error("Please login with investor account.");
            return;
        }
        if (this.landRightForm.get('apply_to').value === 'States and Regions Investment Committees') {
            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['/officer/land-right/landright-state-and-region', id]);
            } else {
                this.router.navigate(['/pages/land-right/landright-state-and-region', id]);
            }

        } else {
            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['/officer/land-right/landright-choose-language', id]);
            } else {
                this.router.navigate(['/pages/land-right/landright-choose-language', id]);
            }

        }
    }

}
