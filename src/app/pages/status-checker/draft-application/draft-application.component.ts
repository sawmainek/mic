import { Component, OnInit } from '@angular/core';
import { MICFormService } from 'src/services/micform.service';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { InquiryService } from 'src/services/inquiry.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { Router } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { MICMMService } from 'src/services/micmm.service';
import { forkJoin, of } from 'rxjs';
import { EndorsementSectionA } from '../../endorsement/sectionA/endorsement-section-a';
import { EndorsementService } from 'src/services/endorsement.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { LandRightService } from 'src/services/land_right.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { catchError } from 'rxjs/operators';

@Component({
    selector: 'app-draft-application',
    templateUrl: './draft-application.component.html',
    styleUrls: ['./draft-application.component.scss']
})
export class DraftApplicationComponent implements OnInit {
    user: any = {};
    id: any;
    loading: any = {};
    lang = 'en';

    params: any;

    empty: any = {};

    drafts: any = [
        { 'id': 0, 'type': 'inquiry', 'name': 'Inquiry (Form 1)', 'data': [] },
        { 'id': 0, 'type': 'mic', 'name': 'Permit (Form 2)', 'data': [] },
        { 'id': 0, 'type': 'endorsement', 'name': 'Endorsement (Form 4ab)', 'data': [] },
        { 'id': 0, 'type': 'tax', 'name': 'Tax Incentive Application Authorization (Form 6)', 'data': [] },
        { 'id': 0, 'type': 'landright', 'name': 'Land Rights Authorization (Form 7ab)', 'data': [] },
    ];

    constructor(
        private authService: AuthService,
        private translateService: TranslateService,
        private inquiryService: InquiryService,
        private router: Router,

        private micFormService: MICFormService,
        private micService: MicService,
        private micMMService: MICMMService,

        private endoFormService: EndorsementFormService,
        private endoService: EndorsementService,
        private endoMMService: EndorsementMMService,

        private landFormService: LandRightFormService,
        private landService: LandRightService,
        private landMMService: LandRightMMService,

        private taxFormService: TaxIncentiveFormService,
        private taxService: TaxIncentiveService,
        private taxMMService: TaxIncentiveMMService,
    ) {
        this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
        this.translateService.use(this.lang ? this.lang : 'en');

        this.authService.getCurrentUser().subscribe(result => {
            this.user = result;
            if (this.user) {
                this.params = {
                    search: `user_id:equal:${this.user.id}|status:equal:Draft`,
                };

                this.getInquiryDraft();
                this.getMICDraft();
                this.getEndoDraft();
                this.getTaxDraft();
                this.getLandDraft();
            }
        }, err => {
            console.log(err);
        });
    }

    ngOnInit(): void { }

    getInquiryDraft() {
        this.loading['inquiry'] = true;
        this.empty['inquiry'] = true;
        this.drafts[0].data = [];

        this.inquiryService.get({ search: `user_id:equal:${this.user.id}|draft:equal:true` }).subscribe((results: any) => {
            const forms = results.filter(x => x.submission == null);
            if (forms.length > 0) {
                this.drafts[0].data = forms;
                this.drafts[0].name = "Inquiry (Form 1)";
                this.empty['inquiry'] = false;

                this.drafts[0].data.forEach((x, i) => {
                    this.drafts[0].data[i].status = "Draft";
                });
            }
            this.loading['inquiry'] = false;
        });
    }

    getMICDraft() {
        this.loading['mic'] = true;
        this.empty['mic'] = true;
        this.drafts[1].data = [];

        this.micFormService.get(this.params).subscribe((results: any) => {
            const forms = results.filter(x => x.submission == null);
            if (forms.length > 0) {
                this.drafts[1].data = forms || [];
                this.drafts[1].name = "Permit (Form 2)";
                this.empty['mic'] = false;
            }
            this.loading['mic'] = false;
        });
    }

    getEndoDraft() {
        this.loading['endo'] = true;
        this.empty['endo'] = true;
        this.drafts[2].data = [];

        this.endoFormService.get(this.params).subscribe((results: any) => {
            const forms = results.filter(x => x.submission == null);
            if (forms.length > 0) {
                forms.forEach((x, i) => {
                    if(x?.apply_to == 'Myanmar Investment Commision') {
                        forms[i].application_type = "MIC(4a)";
                    }
                    else {
                        forms[i].application_type = "States/Regions (4b)";
                    }
                });

                this.drafts[2].data = forms || [];
                this.drafts[2].name = "Endorsement (Form 4ab)";
                this.empty['endo'] = false;
            }
            this.loading['endo'] = false;
        });
    }

    getTaxDraft() {
        this.loading['tax'] = true;
        this.empty['tax'] = true;
        this.drafts[3].data = [];

        this.taxFormService.get(this.params).subscribe((results: any) => {
            const forms = results.filter(x => x.submission == null);
            if (forms.length > 0) {
                this.drafts[3].data = forms || [];
                this.drafts[3].name = "Tax Incentive Application Authorization (Form 6)";
                this.empty['tax'] = false;
            }
            this.loading['tax'] = false;
        });
    }

    getLandDraft() {
        this.loading['land'] = true;
        this.empty['land'] = true;
        this.drafts[4].data = [];

        this.landFormService.get(this.params).subscribe((results: any) => {
            const forms = results.filter(x => x.submission == null);
            if (forms.length > 0) {
                forms.forEach((x, i) => {
                    if(x?.apply_to == 'Myanmar Investment Commision') {
                        forms[i].application_type = "MIC(7a)";
                    }
                    else {
                        forms[i].application_type = "States/Regions (7b)";
                    }
                });
                
                this.drafts[4].data = forms || [];
                this.drafts[4].name = "Land Rights Authorization (Form 7ab)";
                this.empty['land'] = false;
            }
            this.loading['land'] = false;
        });
    }

    delete(form, type) {
        if(confirm('Are you sure want to delete?')) {
            if (type == 'inquiry') {
                this.loading['inquiry'] = true;
                this.inquiryService.destroy(form.id).subscribe(result => {
                    this.getInquiryDraft();
                })
            }
    
            if (type == 'mic') {
                this.loading['mic'] = true;
                const mic = forkJoin([
                    this.micFormService.destroy(form.id),
                    this.micMMService.destroy(form.data_mm?.id || 0),
                    this.micService.destroy(form.data?.id || 0),
                ]).pipe(catchError(error => of(error)));
    
                mic.subscribe(results => {
                    this.getMICDraft();
                })
            }
    
            if (type == 'endorsement') {
                this.loading['endo'] = true;
                const endorsement = forkJoin([
                    this.endoFormService.destroy(form.id),
                    this.endoMMService.destroy(form.data_mm?.id || 0),
                    this.endoService.destroy(form.data?.id || 0),
                ]).pipe(catchError(error => of(error)));
    
                endorsement.subscribe(results => {
                    this.getEndoDraft();
                })
            }
    
            if (type == 'tax') {
                this.loading['tax'] = true;
                const tax = forkJoin([
                    this.taxFormService.destroy(form.id),
                    this.taxMMService.destroy(form.data_mm?.id || 0),
                    this.taxService.destroy(form.data?.id || 0),
                ]).pipe(catchError(error => of(error)));
    
                tax.subscribe(results => {
                    this.getTaxDraft();
                })
            }
    
            if (type == 'landright') {
                this.loading['land'] = true;
                const landright = forkJoin([
                    this.landFormService.destroy(form.id),
                    this.landMMService.destroy(form.data_mm?.id || 0),
                    this.landService.destroy(form.data?.id || 0),
                ]).pipe(catchError(error => of(error)));
    
                landright.subscribe(results => {
                    this.getLandDraft();
                })
            }
        }
    }

    openDraftForm(form, type) {
        if (type == 'inquiry') {
            this.router.navigate(['pages/inquiry/form', form.id]);
        }
        if (type == 'mic') {
            this.router.navigate(['pages/mic/choose-language', form.id]);
        }
        if (type == 'endorsement') {
            this.router.navigate(['pages/endorsement/form', form.id]);
        }
        if (type == 'tax') {
            this.router.navigate(['pages/tax-incentive/choose-language', form.id]);
        }
        if (type == 'landright') {
            this.router.navigate(['pages/land-right/landright-choose-investment', form.id]);
        }
    }

}
