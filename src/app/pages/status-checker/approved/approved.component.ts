import { TaxincentiveSubmissionService } from './../../../../services/taxincentive-submission.service';
import { LandrightSubmissionService } from './../../../../services/landright-submission.service';
import { EndorsementSubmissionService } from './../../../../services/endorsement-submission.service';
import { MicSubmissionService } from './../../../../services/mic-submission.service';
import { Component, OnInit } from '@angular/core';
import { MICFormService } from 'src/services/micform.service';
import { EndorsementFormService } from 'src/services/endorsement-form.service';
import { LandRightFormService } from 'src/services/land-right-form.service';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { InquiryService } from 'src/services/inquiry.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { Router } from '@angular/router';
import { MicService } from 'src/services/mic.service';
import { MICMMService } from 'src/services/micmm.service';
import { forkJoin, of } from 'rxjs';
import { EndorsementSectionA } from '../../endorsement/sectionA/endorsement-section-a';
import { EndorsementService } from 'src/services/endorsement.service';
import { EndorsementMMService } from 'src/services/endorsement-mm.service';
import { LandRightService } from 'src/services/land_right.service';
import { LandRightMMService } from 'src/services/land-right-mm.service';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { catchError } from 'rxjs/operators';

@Component({
    selector: 'app-approved',
    templateUrl: './approved.component.html',
    styleUrls: ['./approved.component.scss']
})
export class ApprovedComponent implements OnInit {
    user: any = {};
    id: any;
    loading: any = {};
    lang = 'en';

    params: any;

    empty: any = {};

    approveds: any = [
        { 'id': 0, 'type': 'inquiry', 'name': 'Inquiry (Form 1)', 'data': [] },
        { 'id': 0, 'type': 'mic', 'name': 'Permit (Form 2)', 'data': [] },
        { 'id': 0, 'type': 'endorsement', 'name': 'Endorsement (Form 4ab)', 'data': [] },
        { 'id': 0, 'type': 'tax', 'name': 'Tax Incentive Application Authorization (Form 6)', 'data': [] },
        { 'id': 0, 'type': 'landright', 'name': 'Land Rights Authorization (Form 7ab)', 'data': [] },
    ];

    constructor(
        private router: Router,
        private authService: AuthService,
        private translateService: TranslateService,
        private inquiryService: InquiryService,
        private micSubmissionService: MicSubmissionService,
        private endoSubmissionService: EndorsementSubmissionService,
        private landSubmissionService: LandrightSubmissionService,
        private taxSubmissionService: TaxincentiveSubmissionService
    ) {
        this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
        this.translateService.use(this.lang ? this.lang : 'en');

        this.authService.getCurrentUser().subscribe(result => {
            this.user = result;
            if (this.user) {
                this.params = {
                    search: `submitted_user_id:equal:${this.user.id}|status:equal:Acceptance`,
                };

                //   this.getInquiryApproved();
                this.getMICApproved();
                this.getEndoApproved();
                this.getTaxApproved();
                this.getLandApproved();
            }
        }, err => {
            console.log(err);
        });
    }

    ngOnInit(): void { }

    getInquiryApproved() {
        this.loading['inquiry'] = true;
        this.empty['inquiry'] = true;
        this.approveds[0].data = [];

        this.inquiryService.get({ search: `user_id:equal:${this.user.id}|draft:equal:true` }).subscribe((results: any) => {
            const forms = results.filter(x => x.submission == null);
            if (forms.length > 0) {
                this.approveds[0].data = forms;
                this.approveds[0].name = "Inquiry (Form 1)";
                this.empty['inquiry'] = false;

                this.approveds[0].data.forEach((x, i) => {
                    this.approveds[0].data[i].status = "Approved";
                });
            }
            this.loading['inquiry'] = false;
        });
    }

    getMICApproved() {
        this.loading['mic'] = true;
        this.empty['mic'] = true;
        this.approveds[1].data = [];

        this.micSubmissionService.get(this.params).subscribe((results: any) => {
            if (results.length > 0) {
                this.approveds[1].data = results || [];
                this.approveds[1].name = "Permit (Form 2)";
                this.approveds[1].type = "mic";
                this.empty['mic'] = false;
            }
            this.loading['mic'] = false;
        });
    }

    getEndoApproved() {
        this.loading['endo'] = true;
        this.empty['endo'] = true;
        this.approveds[2].data = [];

        this.endoSubmissionService.get(this.params).subscribe((results: any) => {
            if (results.length > 0) {
                results.forEach((x, i) => {
                    if(x.form?.apply_to == 'Myanmar Investment Commision') {
                        results[i].application_type = "MIC(4a)";
                    }
                    else {
                        results[i].application_type = "States/Regions (4b)";
                    }
                });

                this.approveds[2].data = results || [];
                this.approveds[2].name = "Endorsement (Form 4ab)";
                this.approveds[2].type = "endorsement";
                this.empty['endo'] = false;

                console.log(this.approveds)
            }
            this.loading['endo'] = false;
        });
    }

    getTaxApproved() {
        this.loading['tax'] = true;
        this.empty['tax'] = true;
        this.approveds[3].data = [];

        this.taxSubmissionService.get(this.params).subscribe((results: any) => {
            if (results.length > 0) {
                this.approveds[3].data = results || [];
                this.approveds[3].name = "Tax Incentive Application Authorization (Form 6)";
                this.approveds[3].type = "tax";
                this.empty['tax'] = false;
            }
            this.loading['tax'] = false;
        });
    }

    getLandApproved() {
        this.loading['land'] = true;
        this.empty['land'] = true;
        this.approveds[4].data = [];

        this.landSubmissionService.get(this.params).subscribe((results: any) => {
            if (results.length > 0) {
                results.forEach((x, i) => {
                    if(x.form?.apply_to == 'Myanmar Investment Commision') {
                        results[i].application_type = "MIC(7a)";
                    }
                    else {
                        results[i].application_type = "States/Regions (7b)";
                    }
                });
                
                this.approveds[4].data = results || [];
                this.approveds[4].name = "Land Rights Authorization (Form 7ab)";
                this.approveds[4].type = "landright";
                this.empty['land'] = false;
            }
            this.loading['land'] = false;
        });
    }

    openForm(submission, type) {
        if (type == 'inquiry') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
        if (type == 'mic') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
        if (type == 'endorsement') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
        if (type == 'tax') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
        if (type == 'landright') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
    }

}
