import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/services/auth.service';
declare var jQuery: any;

@Component({
    selector: 'app-form-history',
    templateUrl: './form-history.component.html',
    styleUrls: ['./form-history.component.scss']
})
export class FormHistoryComponent implements OnInit {

    id: any;
    type: any;
    user: any = {};
    histories: any[] = [];
    displayedColumns: any[] = [];
    datatable: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.type = paramMap.get('type') || null;

            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                if (this.type == 'inquiry') {
                    this.type = 'Inquiry ( Form 1 )';
                }
                if (this.type == 'mic') {
                    this.type = 'MIC-Permit';
                }
                if (this.type == 'endorsement') {
                    this.type = 'Endorsement';
                }
                if (this.type == 'tax') {
                    this.type = 'Tax Incentive';
                }
                if (this.type == 'landright') {
                    this.type = 'Land Right';
                }

                this.displayedColumns = [
                    {
                        field: 'created_at',
                        title: 'Date',
                        sortable: true,
                        template: (row) => {
                            const datePipe = new DatePipe('en');
                            return datePipe.transform(row.created_at, 'dd-MM-yyyy hh:mm a');
                        }
                    },
                    {
                        field: 'user_id',
                        title: 'Submitted By',
                        sortable: true,
                        template: (row) => {
                            return row.incoming == true ? row?.user?.name || '' : '';
                        }
                    },
                    {
                        field: 'officer_id',
                        title: 'Replied By',
                        sortable: true,
                        template: (row) => {
                            return row.incoming == false ? row?.officer?.name || '' : '';
                        }
                    },
                    {
                        field: 'status',
                        title: 'Status',
                        sortable: true,
                    },
                    {
                        field: 'sub_status',
                        title: 'Action',
                        sortable: true,
                        template: (row) => {
                            return row.sub_status == 'Reply' ? 'Received' : 'Sent - ' + (row?.sub_status || '');
                        }
                    },
                    {
                        field: 'goto',
                        title: '',
                        sortable: false,
                        width: 80,
                        template: (row) => {
                            if (row.message_id) {
                                return `<button class="btn btn-outline-info btn-message" data-id="${row.message_id}">
                                    <i class="fa fa-commenting"></i>
                                </button>`;
                            }
                            return ``;
                        }
                    }
                ];

                this.initDatatable();
            });

        });
    }

    initDatatable() {
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };

        var search = `type:equal:${this.type}|type_id:equal:${this.id}`;

        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/formlog',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: search,
                            query: '',
                            datatable: true,
                            sort: 'created_at',
                            order: 'desc'
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            }
        };
        if (this.datatable) { jQuery('.kt-datatable-form-history').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-form-history').KTDatatable(options);
        console.log(this.datatable)
        jQuery('.kt-datatable-form-history').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;
            jQuery('.btn-message').on('click', function () {
                setTimeout(() => {
                    const id = jQuery(this).data('id');
                    ctx.goto(id);
                }, 300);
            });
        });
    }

    goto(id) {
        this.router.navigateByUrl(`${window.location.pathname}?tab=reply&message_id=${id}`);
    }

}
