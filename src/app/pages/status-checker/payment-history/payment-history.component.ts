import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { EndorsementSubmissionService } from 'src/services/endorsement-submission.service';
import { MicSubmissionService } from 'src/services/mic-submission.service';
import { TaxincentiveSubmissionService } from 'src/services/taxincentive-submission.service';
import { InquirySubmissionService } from 'src/services/inquiry-submission.service';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { forkJoin } from 'rxjs';
import printPDF from './../print';
import { LocalService } from 'src/services/local.service';

const basePrintData = {
    'addressSender': {
        'person': 'No. 1, ',
        'street': 'Thitsar Road, ',
        'city': 'Yankin Tsp, Yangon.',
        'email': 'dica.ip.mm@gmail.com',
        'phone': '+951 658102, 658 103'
    }
};

@Component({
    selector: 'app-payment-history',
    templateUrl: './payment-history.component.html',
    styleUrls: ['./payment-history.component.scss']
})
export class PaymentHistoryComponent implements OnInit {
    user: any = {};
    invoices: any[] = [];
    type: any = [];
    contact: any;
    loading: boolean = true;
    is_payment_empty: boolean = true;

    @Input() param: any;

    constructor(
        private router: Router,
        private store: Store<AppState>,
        private inquirySubmissionService: InquirySubmissionService,
        private micSubmissionService: MicSubmissionService,
        private endoSubmissionService: EndorsementSubmissionService,
        private taxSubmissionService: TaxincentiveSubmissionService,
        private landSubmissionService: LandrightSubmissionService,
        private localService: LocalService,
    ) {
        this.type = ["Permit (Form 2)", "Endorsement (Form 4ab)", "Tax Incentive Application Authorization (Form 6)", "Land Rights Authorization (Form 7ab)"];
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                    this.getSubmissionData();
                }
            });

        this.localService.getContact().subscribe(x => {
            this.contact = x;
        });
    }

    ngOnInit(): void { }

    getSubmissionData() {
        this.loading = true;
        const params = {
            page: 1,
            search: `submitted_user_id:equal:${this.user?.id}`,
            secure: true,
        };
        forkJoin([
            this.micSubmissionService.get(params),
            this.endoSubmissionService.get(params),
            this.taxSubmissionService.get(params),
            this.landSubmissionService.get(params)
        ]).subscribe(result => {
            this.invoices = [];
            this.loading = false;
            result.forEach((x, index) => {
                for (const [i, submission] of x.entries()) {
                    if (submission.invoices && submission.invoices.length > 0) {
                        submission.invoices.forEach(y => {
                            var temp = {};
                            temp['id'] = y.id;
                            temp['type'] = this.type[index];
                            temp['application_no'] = x[0].application_no;
                            temp['description'] = y.description;
                            temp['amount'] = y.amount;
                            temp['created_at'] = y.created_at;
                            temp['status'] = y.status;
                            temp['company_name'] = submission?.investment_enterprise_name || '';
                            temp['role'] = submission?.submit_to_role || 'division-1';

                            this.invoices.push(temp);
                            this.is_payment_empty = false;
                        });
                    }
                }
            });
            this.loading = false;
        });
    }

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    downloadInvoice(invoice) {
        const date = new Date();
        const invoices = [];
        const datePipe = new DatePipe('en');    
        
        invoice['address'] = this.contact.filter(y => y.name == invoice['role'])[0] || invoice[0];

        let totalAmount = 0;
        totalAmount += invoice.amount;
        invoices.push({
            'title': invoice.type,
            'description': invoice.description,
            'total': `Ks ${this.formatNumber(invoice.amount)}`
        })

        printPDF(Object.assign(basePrintData, {
            'title': 'Invoice',
            'filename': `DICA-${invoice.application_no}-${invoice.id}`,
            'invoice': {
                'number': `DICA-${invoice.application_no}-${invoice.id}`,
                'date': datePipe.transform(invoices[0].created_at || new Date()),
                'total': `Ks ${this.formatNumber(totalAmount)}`,
                'company_name': invoice.company_name
            },
            'items': invoices,
            'address': {
                name: invoice.address.name,
                branch: invoice.address.branch,
                address: invoice.address.address,
                phone: invoice.address.phone
            }
        }));
    }

    downloadReceipt(invoice) {
        const date = new Date();
        const invoices = [];
        const datePipe = new DatePipe('en');    
        
        invoice['address'] = this.contact.filter(y => y.name == invoice['role'])[0] || invoice[0];

        let totalAmount = 0;
        totalAmount += invoice.amount;
        invoices.push({
            'title': invoice.type,
            'description': invoice.description,
            'total': `Ks ${this.formatNumber(invoice.amount)}`
        })

        printPDF(Object.assign(basePrintData, {
            'title': 'Receipt',
            'filename': `DICA-${invoice.application_no}-${invoice.id}`,
            'invoice': {
                'number': `DICA-${invoice.application_no}-${invoice.id}`,
                'date': datePipe.transform(invoices[0].created_at || new Date()),
                'total': `Ks ${this.formatNumber(totalAmount)}`,
                'company_name': invoice.company_name
            },
            'items': invoices,
            'address': {
                name: invoice.address.name,
                branch: invoice.address.branch,
                address: invoice.address.address,
                phone: invoice.address.phone
            }
        }));

        
    }

    openPayment(type, id) {
        // this.payment_url = 'pages/inquiry/payment/' + id;
        if (type == 'Permit (Form 2)') {
            this.router.navigate(['pages/mic/payment/' + id])
        }
        if (type == 'Endorsement (Form 4ab)') {
            this.router.navigate(['pages/endorsement/payment/' + id]);
        }
        if (type == 'Tax Incentive Application Authorization (Form 6)') {
            this.router.navigate(['pages/tax-incentive/payment/' + id]);
        }
        if (type == 'Land Rights Authorization (Form 7ab)') {
            this.router.navigate(['pages/land-right/payment/' + id]);
        }
    }

}
