import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { EndorsementSubmissionService } from 'src/services/endorsement-submission.service';
import { InquirySubmissionService } from 'src/services/inquiry-submission.service';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { MicSubmissionService } from 'src/services/mic-submission.service';
import { TaxincentiveSubmissionService } from 'src/services/taxincentive-submission.service';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-status-checker-form2',
    templateUrl: './status-checker.component.html',
    styleUrls: ['./status-checker.component.scss']
})
export class StatusCheckerComponent implements OnInit {
    user: any = {};
    id: any;
    loading: boolean = false;
    lang = 'en';

    is_submissions_empty: boolean = true;

    open = "submission";

    submissions: any = [
        { 'id': 0, 'type': 'inquiry', 'name': 'Inquiry (Form 1)', 'data': [] },
        { 'id': 0, 'type': 'mic', 'name': 'Permit (Form 2)', 'data': [] },
        { 'id': 0, 'type': 'endorsement', 'name': 'Endorsement (Form 4ab)', 'data': [] },
        { 'id': 0, 'type': 'tax', 'name': 'Tax Incentive Application Authorization (Form 6)', 'data': [] },
        { 'id': 0, 'type': 'landright', 'name': 'Land Rights Authorization (Form 7ab)', 'data': [] },
    ];

    constructor(
        private authService: AuthService,
        private inquirySubmissionService: InquirySubmissionService,
        private micSubmissionService: MicSubmissionService,
        private endorsementSubmissionService: EndorsementSubmissionService,
        private taxSubmissionService: TaxincentiveSubmissionService,
        private translateService: TranslateService,
        private landrightSubmissionService: LandrightSubmissionService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) {
        this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
        this.translateService.use(this.lang ? this.lang : 'en');
        this.loading = true;
        this.authService.getCurrentUser().subscribe(result => {
            this.user = result;
            if (this.user) {
                this.getSubmission();
            }
        }, err => {
            console.log(err);
        });
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.open = paramMap.get('open') || 'submission';
        });
    }

    openTab(tab) {
        this.open = tab;
    }

    getLastReplyMessage(reply: any[] = []) {
        const officerReply = reply.filter(x => x.role == 'Officer');
        if (officerReply.length > 0) {
            return officerReply[officerReply.length - 1].reply_message;
        }
        return '';
    }

    getSubmission() {
        const params = {
            search: `submitted_user_id:equal:${this.user.id}`,
        };

        this.inquirySubmissionService.get(params).subscribe((results: any) => {
            if (results.length > 0) {
                results.forEach((x, i) => {
                    if (x.form?.apply_to == 'Myanmar Investment Commision') {
                        results[i].application_type = "MIC - Inquiry (Form 1)";
                    }
                    else {
                        results[i].application_type = "States/Regions - Inquiry (Form 1)";
                    }
                });

                this.submissions[0].data = results;
                this.submissions[0].name = "Inquiry (Form 1)";
                this.is_submissions_empty = false;
            }
            this.loading = false;
        });

        this.micSubmissionService.get(params).subscribe((results: any) => {
            if (results.length > 0) {
                this.submissions[1].data = results || [];
                this.submissions[1].name = "Permit (Form 2)";

                this.is_submissions_empty = false;
            }
            this.loading = false;
        });

        this.endorsementSubmissionService.get(params).subscribe((results: any) => {
            if (results.length > 0) {
                results.forEach((x, i) => {
                    if (x.form?.apply_to == 'Myanmar Investment Commision') {
                        results[i].application_type = "MIC(4a)";
                    }
                    else {
                        results[i].application_type = "States/Regions (4b)";
                    }
                });

                this.submissions[2].data = results || [];
                this.submissions[2].name = "Endorsement (Form 4ab)";
                this.is_submissions_empty = false;

                console.log(results)
            }
            this.loading = false;
        });

        this.taxSubmissionService.get(params).subscribe((results: any) => {
            if (results.length > 0) {
                this.submissions[3].data = results || [];
                this.submissions[3].name = "Tax Incentive Application Authorization (Form 6)";
                this.is_submissions_empty = false;
            }
            this.loading = false;
        });

        this.landrightSubmissionService.get(params).subscribe((results: any) => {
            if (results.length > 0) {
                results.forEach((x, i) => {
                    if (x.form?.apply_to == 'Myanmar Investment Commision') {
                        results[i].application_type = "MIC(7a)";
                    }
                    else {
                        results[i].application_type = "States/Regions (7b)";
                    }
                });

                this.submissions[4].data = results || [];
                this.submissions[4].name = "Land Rights Authorization (Form 7ab)";
                this.is_submissions_empty = false;
            }
            this.loading = false;
        });
    }

    openForm(submission, type) {
        if (type == 'inquiry') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
        if (type == 'mic') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
        if (type == 'endorsement') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
        if (type == 'tax') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
        if (type == 'landright') {
            this.router.navigate(['/pages/status-checker-detail', submission.id, type]);
        }
    }

    openPaymentHistory() {
        this.router.navigate(['/pages/payment-history']);
    }
}
