import { jsPDF } from 'jspdf';
// import addFontNormal from '../fonts/WorkSans-normal';
// import addFontBold from '../fonts/WorkSans-bold';
import svg2pdf from 'svg2pdf.js';
import fetchSvg from './fetchSvg';
import heading from './pdf/heading';
import table from './pdf/table';
import totals from './pdf/totals';
import addressSender from './pdf/addressSender';
import footer from './pdf/footer';
import logo from './pdf/logo';

export default (printData) => {

    const doc = new jsPDF('p', 'pt');

    // <><>><><>><>><><><><><>>><><<><><><><>
    // SETTINGS
    // <><>><><>><>><><><><><>>><><<><><><><>

    const fontSizes = {
        TitleFontSize:13,
        SubTitleFontSize: 13,
        NormalFontSize: 13,
        SmallFontSize: 10
    };
    const lineSpacing = 12;

    let startY = 75; // bit more then 45mm

    const pageWidth = doc.internal.pageSize.width;
    const pageHeight = doc.internal.pageSize.height;
    const pageCenterX = pageWidth / 2;

    // <><>><><>><>><><><><><>>><><<><><><><>
    // COMPONENTS
    // <><>><><>><>><><><><><>>><><<><><><><>

    // <><>><><>><>><><><><><>>><><<><><><><>
    // Background init
    // need to print the background before other elements get printed on
    fetchSvg(doc, 'img/background.svg').then((svg) => {
        // if (svg) {
        //     doc.setPage(1);

        //     svg2pdf(svg, doc, {
        //         xOffset: -70,
        //         yOffset: 250
        //     });
        //     localStorage.setItem('bgSvg', new XMLSerializer().serializeToString(svg));
        // }

        // <><>><><>><>><><><><><>>><><<><><><><>
        // Sender's address

        startY = heading(doc, printData, startY, fontSizes, lineSpacing);

        // <><>><><>><>><><><><><>>><><<><><><><>
        // Table with items

        startY = table(doc, printData, startY, fontSizes.NormalFontSize, lineSpacing);
        startY = totals(doc, printData, startY, fontSizes.NormalFontSize, lineSpacing);

        // <><>><><>><>><><><><><>>><><<><><><><>
        // Footer

        // footer(doc, printData, fontSizes.SmallFontSize, lineSpacing);

        // <><>><><>><>><><><><><>>><><<><><><><>
        // Logo
        const logoLoaded = logo(doc);

        Promise.all([logoLoaded]).then(() => {
            doc.save(`${printData.filename}.pdf`);
        });
    });
}
