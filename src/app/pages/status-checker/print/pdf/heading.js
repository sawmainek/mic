export default (doc, data, startY, fontSizes, lineSpacing) => {

    const invoiceNrTxt = 'No. ';
    let startX = 57;
    const pageWidth = doc.internal.pageSize.width;
    const endX = pageWidth - startX;

    doc.setFontSize(fontSizes.SubTitleFontSize);

    // startY = 150;
    doc.setFontSize(40);
    doc.text(data.title, startX, startY);

    startY += 30;
    doc.setFontSize(fontSizes.TitleFontSize);
    doc.text(`${invoiceNrTxt} ${data.invoice.number} `, startX, startY);

    console.log(data)
    doc.text(data.address.branch, endX, startY, 'right');
    startY += 30;

    doc.text(`Date: ${data.invoice.date}`, startX, startY);

    doc.text(data.address.address, endX, startY, 'right');
    startY += 30;

    doc.text(`Company Name: ${data.invoice.company_name}`, startX, startY);

    doc.text(data.address.phone, endX, startY, 'right');
    startY += 30;



    startY += 60;
    startX = 57;

    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(0.5);
    startY += lineSpacing;
    doc.line(startX, startY, endX, startY);

    return startY;
}
