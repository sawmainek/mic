import { FormCommentService } from './../../../../services/form-comment.service';
import printPDF from './../print';
import { from, of, forkJoin } from 'rxjs';
import { NotifyService } from './../../../../services/notify.service';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../../core/reducers/index';
import { Loaded } from './../../../core/form/_actions/form.actions';
import { BaseService } from './../../../../services/base.service';
import { InquiryMessageService } from './../../../../services/inquiry-message.service';
import { EndorsementMessageService } from './../../../../services/endorsement-message.service';
import { TaxIncentiveMessageService } from './../../../../services/tax-incentive-message.service';
import { MicMessageService } from './../../../../services/mic-message.service';
import { MICReplyService } from './../../../../services/micreply.service';
import { LandRightReplyService } from './../../../../services/land-right-reply.service';
import { TaxIncentiveReplyService } from './../../../../services/tax-incentive-reply.service';
import { FileUploadService } from 'src/services/file_upload.service';
import { finalize, tap, concatMap, delay, mergeMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { InquiryReplyService } from 'src/services/inquiry-reply.service';
import { InquirySubmissionService } from 'src/services/inquiry-submission.service';
import { DatePipe, Location } from '@angular/common';
import { MicSubmissionService } from 'src/services/mic-submission.service';
import { EndorsementSubmissionService } from 'src/services/endorsement-submission.service';
import { TaxincentiveSubmissionService } from 'src/services/taxincentive-submission.service';
import { EndorsementReplyService } from 'src/services/endorsement-reply.service';
import { ToastrService } from 'ngx-toastr';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { LandRightMessageService } from 'src/services/land-right-message.service';
import { CertificateExport } from './endorsement-word';
import { InquiryPreviewService } from '../../inquiry/inquiry-preview/inquiry-preview.service';
import { MICFormPreviewService } from '../../mic-permit/mic-preview/mic-preview.service';
import { EndorsementPreviewService } from '../../endorsement/endorsement-preview/endorsement-preview.service';
import { TaxIncentivePreviewService } from '../../tax-incentive/tax-incentive-preview/tax-incentive-preview.service';
import { LandRightPreviewService } from '../../land-right/land-right-preview/land-right-preview.service';
import { FormLogService } from 'src/services/form-log.service';
import { FormService } from 'src/services/form.service';
declare var jQuery: any;


declare var jQuery: any;
const basePrintData = {
    'addressSender': {
        'person': 'No. 1, ',
        'street': 'Thitsar Road, ',
        'city': 'Yankin Tsp, Yangon.',
        'email': 'dica.ip.mm@gmail.com',
        'phone': '+951 658102, 658 103'
    }
};

@Component({
    selector: 'app-status-checker-detail',
    templateUrl: './status-checker-detail.component.html',
    styleUrls: ['./status-checker-detail.component.scss']
})
export class StatusCheckerDetailComponent implements OnInit {
    user: any = {};
    id: any;
    type: any;
    typeUpload: string;
    mm: any;

    submission: any = {};
    selectedMessage: any;
    msg: any = {};

    lang: any;
    params: any = {};
    tab: any = 'application';
    reply_no: any;
    lastReplyMessage: any;
    edit_url: string = '';
    payment_url: string = '';
    showMore: boolean = false;
    loading: boolean = false;
    submitted: boolean = false;
    messages: any[] = [];
    host: string = environment.host;
    statusReplyService: BaseService;
    statusMsgService: BaseService;
    submissionService: BaseService;

    revisions: any[] = [];
    histories: any[] = [];
    previewService: any;

    constructor(
        private router: Router,
        private toast: ToastrService,
        private authService: AuthService,

        private inquiryReplyService: InquiryReplyService,
        private inquirySubmissionService: InquirySubmissionService,
        private inquiryMsgService: InquiryMessageService,

        private micReplyService: MICReplyService,
        private micSubmissionService: MicSubmissionService,
        private micMsgService: MicMessageService,

        private endorReplyService: EndorsementReplyService,
        private endorSubmissionService: EndorsementSubmissionService,
        private endorMsgService: EndorsementMessageService,

        private landRightReplyService: LandRightReplyService,
        private landRightSubmissionService: LandrightSubmissionService,
        private landRightMsgService: LandRightMessageService,

        private taxIncReplyService: TaxIncentiveReplyService,
        private taxIncMsgService: TaxIncentiveMessageService,
        private taxSubmissionService: TaxincentiveSubmissionService,

        private inquiryPreviewService: InquiryPreviewService,
        private micPreviewService: MICFormPreviewService,
        private endoPreviewService: EndorsementPreviewService,
        private taxInvPreviewService: TaxIncentivePreviewService,
        private landRightPreviewService: LandRightPreviewService,

        private fileUploadService: FileUploadService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
        private notifyService: NotifyService,
        private store: Store<AppState>,
        public location: Location,

        private formLogService: FormLogService,
        private formCommentService: FormCommentService,
        private formService: FormService,
    ) {
        this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
        this.translateService.use(this.lang ? this.lang : 'en');
        
    }

    ngOnInit(): void { 
        const params = new URLSearchParams(window.location.search);
        this.tab = params.get('tab') || 'application';
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.type = paramMap.get('type') || null;

            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                if (this.user) {
                    this.params = {
                        page: 1,
                        search: '',
                        secure: true,
                    };

                    if (this.type == 'inquiry') {
                        this.typeUpload = 'Inquiry';
                        this.statusReplyService = this.inquiryReplyService;
                        this.statusMsgService = this.inquiryMsgService;
                        this.submissionService = this.inquirySubmissionService;
                        this.getInquiry();
                        this.getHistoryLog('Inquiry ( Form 1 )');
                        this.previewService = this.inquiryPreviewService;
                    }
                    if (this.type == 'mic') {
                        this.typeUpload = 'MIC Permit';
                        this.statusReplyService = this.micReplyService;
                        this.statusMsgService = this.micMsgService;
                        this.submissionService = this.micSubmissionService;
                        this.getMIC();
                        this.getHistoryLog('MIC-Permit');
                        this.previewService = this.micPreviewService;
                    }
                    if (this.type == 'endorsement') {
                        this.typeUpload = 'Endorsement';
                        this.statusReplyService = this.endorReplyService;
                        this.statusMsgService = this.endorMsgService;
                        this.submissionService = this.endorSubmissionService;
                        this.getEndorsement();
                        this.getHistoryLog('Endorsement');
                        this.previewService = this.endoPreviewService;
                    }
                    if (this.type == 'tax') {
                        this.typeUpload = 'Tax Incentive';
                        this.statusReplyService = this.taxIncReplyService;
                        this.statusMsgService = this.taxIncMsgService;
                        this.submissionService = this.taxSubmissionService;
                        this.getTax();
                        this.getHistoryLog('Tax Incentive');
                        this.previewService = this.taxInvPreviewService;
                    }
                    if (this.type == 'landright') {
                        this.typeUpload = 'Land Right';
                        this.statusReplyService = this.landRightReplyService;
                        this.statusMsgService = this.landRightMsgService;
                        this.submissionService = this.landRightSubmissionService;
                        this.getLandRight();
                        this.getHistoryLog('Land Right');
                        this.previewService = this.landRightPreviewService;
                    }
                }
            }, err => {
                console.log(err);
            });

        });

        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            const params = new URLSearchParams(window.location.search);
            this.tab = params.get('tab') || 'form';
            if (params.get('message_id')) {
                this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
            }
        });
    }

    loadShowMore() {
        this.showMore = true;
    }

    getInquiry() {
        this.store.dispatch(new Loaded({ loaded: true }));
        this.inquirySubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "Inquiry (Form 1)";
            this.edit_url = 'pages/inquiry/form/' + this.submission.inquiry_id+'?action=Edit';
            this.payment_url = 'pages/inquiry/payment/' + this.submission.inquiry_id;
            this.inquiryMsgService.get({ search: `submission_id:equal:${this.submission.id}`, order: 'desc' }).subscribe(res => {
                this.messages = res;
                this.selectedMessage = this.messages[0] || null;
                const params = new URLSearchParams(window.location.search);
                if (params.get('message_id')) {
                    this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
                }
                if (this.selectedMessage) {
                    this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
                }
                for (let msg of this.messages) {
                    if (msg.attachments && msg.attachments.length > 0) {
                        msg.hasAttach = true;
                    } else {
                        if (msg.reply && msg.reply.filter(x => x.attachments.length > 0).length > 0) {
                            msg.hasAttach = true;
                        }
                    }
                }
            })
        })
    }

    getMIC() {
        this.micSubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "MIC Permit Application ";
            this.edit_url = 'pages/mic/sectionA-form1/' + this.submission.mic_id + '?action=Edit';
            this.payment_url = 'pages/mic/payment/' + this.submission.mic_id;
            if (this.submission) {
                this.micMsgService.get({ search: `submission_id:equal:${this.submission.id || null}`, order: 'desc' }).subscribe(res => {
                    this.messages = res;
                    this.selectedMessage = this.messages[0] || null;
                    const params = new URLSearchParams(window.location.search);
                    if (params.get('message_id')) {
                        this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
                    }
                    if (this.selectedMessage) {
                        this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
                    }
                    for (let msg of this.messages) {
                        if (msg.attachments && msg.attachments.length > 0) {
                            msg.hasAttach = true;
                        } else {
                            if (msg.reply?.attachments && msg.reply.filter(x => x.attachments.length > 0).length > 0) {
                                msg.hasAttach = true;
                            }
                        }
                    }
                });

                // For Revision
                if (this.submission.allow_revision) {
                    forkJoin([
                        this.formCommentService.get({
                            search: `type:equal:MIC Permit|type_id:equal:${this.submission?.form?.data?.id || 0}`,
                            rows: 999
                        }),
                        this.formCommentService.get({
                            search: `type:equal:MIC Permit|type_id:equal:${this.submission?.form?.data_mm?.id || 0}`,
                            rows: 999
                        })
                    ]).subscribe(results => {
                        this.revisions = [...results[0], ... results[1]];
                    })
                }
            }
        })
    }

    getEndorsement() {
        this.endorSubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "Endorsement (Form 4ab)";
            this.edit_url = 'pages/endorsement/sectionA-form1/' + this.submission.endorsement_id + '?action=Edit';
            this.payment_url = 'pages/endorsement/payment/' + this.submission.endorsement_id;
            if (this.submission) {
                this.endorMsgService.get({ search: `submission_id:equal:${this.submission.id || null}`, order: 'desc' }).subscribe(res => {
                    this.messages = res;
                    this.selectedMessage = this.messages[0] || null;
                    const params = new URLSearchParams(window.location.search);
                    if (params.get('message_id')) {
                        this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
                    }
                    if (this.selectedMessage) {
                        this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
                    }
                    for (let msg of this.messages) {
                        if (msg.attachments && msg.attachments.length > 0) {
                            msg.hasAttach = true;
                        } else {
                            if (msg.reply && msg.reply.filter(x => x.attachments.length > 0).length > 0) {
                                msg.hasAttach = true;
                            }
                        }
                    }
                })
            }
        });
    }

    getTax() {
        this.taxSubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "Tax Incentive Application Authorization (Form 6)";
            this.edit_url = 'pages/tax-incentive/form1/' + this.submission.tax_incentive_id + '?action=Edit';
            this.payment_url = 'pages/tax-incentive/payment/' + this.submission.tax_incentive_id;
            if (this.submission) {
                this.taxIncMsgService.get({ search: `submission_id:equal:${this.submission.id || null}`, order: 'desc' }).subscribe(res => {
                    this.messages = res;
                    this.selectedMessage = this.messages[0] || null;
                    const params = new URLSearchParams(window.location.search);
                    if (params.get('message_id')) {
                        this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
                    }
                    if (this.selectedMessage) {
                        this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
                    }
                    for (let msg of this.messages) {
                        if (msg.attachments && msg.attachments.length > 0) {
                            msg.hasAttach = true;
                        } else {
                            if (msg.reply && msg.reply.filter(x => x.attachments?.length > 0).length > 0) {
                                msg.hasAttach = true;
                            }
                        }
                    }
                })
            }
        })
    }

    getLandRight() {
        this.landRightSubmissionService.show(this.id).subscribe(submission => {
            this.submission = submission || null;
            this.submission.type = "Land Rights Authorization (Form 7ab)";
            this.edit_url = 'pages/land-right/form/' + this.submission.land_right_id + '?action=Edit';
            this.payment_url = 'pages/land-right/payment/' + this.submission.land_right_id;
            if (this.submission) {
                this.landRightMsgService.get({ search: `submission_id:equal:${this.submission.id || null}`, order: 'desc' }).subscribe(res => {
                    this.messages = res;
                    this.selectedMessage = this.messages[0] || null;
                    const params = new URLSearchParams(window.location.search);
                    if (params.get('message_id')) {
                        this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
                    }
                    if (this.selectedMessage) {
                        this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
                    }
                    for (let msg of this.messages) {
                        if (msg.attachments && msg.attachments.length > 0) {
                            msg.hasAttach = true;
                        } else {
                            if (msg.reply && msg.reply.filter(x => x.attachments.length > 0).length > 0) {
                                msg.hasAttach = true;
                            }
                        }
                    }
                });

                
            }
        })
    }

    getHistoryLog(type) {
        this.formLogService.get({
            search: `type:equal:${type}|type_id:equal:${this.id}`
        }).subscribe(results => {
            this.histories = results;
        })
    }

    previewFormHistory(data) {
        if (data) {
            this.previewService.toFormPDF(data.data_mm || data, { readOnly: true });
        }
    }

    openTab(tab) {
        this.reply_no = 0;
        this.router.navigateByUrl(`${window.location.pathname}?tab=${tab}`);
    }

    editForm() {
        this.router.navigateByUrl(this.edit_url);
    }

    paymentForm() {
        this.router.navigate(['/pages/status-checker', 'payment']);
    }

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    downloadFile(path: string, type: string) {
        this.fileUploadService.download(path)
            .subscribe(url => {
                window.open(url, '_blank');
            });
    }

    get checkPaymentNeed() {
        if (this.submission.invoices) {
            const invoices = this.submission.invoices.filter(x => x.status == 'Unpaid');
            return invoices.length > 0 ? true : false;
        }
        return false;
    }

    downloadInvoiceForm() {
        const date = new Date();
        const invoices = [];
        const datePipe = new DatePipe('en');

        let totalAmount: number = 0;
        this.submission.invoices.forEach(x => {
            totalAmount += x.amount;
            invoices.push({
                'title': this.submission.type,
                'description': x.description,
                'total': `Ks ${this.formatNumber(x.amount)}`
            })
        });

        printPDF(Object.assign(basePrintData, {
            'title': 'Invoice',
            'filename': `${date.getFullYear()}-${this.submission.application_no}`,
            'invoice': {
                'number': `${date.getFullYear()}-${this.submission.application_no}`,
                'date': datePipe.transform(invoices[0].created_at || new Date()),
                'total': `Ks ${this.formatNumber(totalAmount)}`
            },
            'items': invoices
        }));
    }

    downloadInvoiceReceipt() {
        const date = new Date();
        const invoices = [];
        const datePipe = new DatePipe('en');

        let totalAmount: number = 0;
        this.submission.invoices.forEach(x => {
            totalAmount += x.amount;
            invoices.push({
                'title': this.submission.type,
                'description': x.description,
                'total': `Ks ${this.formatNumber(x.amount)}`
            })
        });

        printPDF(Object.assign(basePrintData, {
            'title': 'Receipt',
            'filename': `${date.getFullYear()}-${this.submission.application_no}`,
            'invoice': {
                'number': `${date.getFullYear()}-${this.submission.application_no}`,
                'date': datePipe.transform(invoices[0].created_at || new Date()),
                'total': `Ks ${this.formatNumber(totalAmount)}`
            },
            'items': invoices
        }));
    }

    openReply(reply_no) {
        this.reply_no = reply_no;
    }

    loadDatePicker() {
        jQuery('.form-datetimepicker').datetimepicker({
            format: 'dd-mm-yyyy hh:ii',
            minDate: 'today',
            todayHighlight: true,
            autoclose: true
        });
        jQuery('body').on('change', '.form-datetimepicker', function () {
            jQuery(this).trigger('click');
        });
    }

    getStatusStyle(status) {
        switch (status) {
            case 'Revision':
                return 'text-warning';
            case 'PAT Meetings':
                return 'text-info';
            case 'MIC Meetings':
                return 'text-info';
            case 'Acceptance':
                return 'text-success';
            case 'Rejects':
                return 'text-danger';
            case 'Payment':
                return 'text-warning';
            default:
                break;
        }
    }

    onFileChange(file) {
        this.msg.attachments = [...this.msg.attachments || []];
        this.msg.attachments.push(file);
    }

    onDateChange(value, key) {
        this.msg[key] = value;
    }

    openMessage(msg: any = null) {
        this.selectedMessage = msg ? msg : null;
        if (this.selectedMessage) {
            this.selectedMessage.reply = msg.reply || [];
            this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
        }

        this.msg.subject = msg ? msg.subject : null;
    }

    get checkDateConfirm() {
        const confirm = this.selectedMessage.reply.filter(x => x.confirm_date != null).length;
        return confirm > 0 ? true : false;
    }

    createMessage() {
        if (this.selectedMessage && (this.selectedMessage.status == 'MIC Meetings' || this.selectedMessage.status == 'PAT Meetings')) {
            if (!this.checkDateConfirm && !this.msg.confirm_date) {
                this.toast.error("Please choose your confirm date");
                return false;
            }
        }
        let files: any[] = [];
        let uploadedFiles = [];
        this.submitted = true;
        this.loading = true;
        files = this.msg.attachments || [];
        of(files).pipe(
            mergeMap((x: [any]) => from(x)),
            concatMap(x => {
                return of(x).pipe(delay(100))
            }),
            concatMap((file: any) => {
                if (file instanceof File) {
                    const formData: any = new FormData();
                    formData.append('type_id', this.id);
                    formData.append('type', this.typeUpload);
                    formData.append('name', file.name);
                    formData.append('file', file);
                    return this.fileUploadService.upload(formData, { secure: true });
                }
                return of(file);
            }),
            tap(x => {
                uploadedFiles.push(x);
            }),
            finalize(() => {

                this.msg.user_id = this.user.id;
                this.msg.attachments = uploadedFiles;
                this.msg.submission_id = this.submission.id;
                this.msg.status = this.submission.status;
                this.msg.role = 'Investor';

                // Save to Form Data/Status Log Model;
                
                this.sentNotify();

                if (this.selectedMessage) {
                    this.msg.reply_user_id = this.user.id;
                    this.msg.status = this.selectedMessage.status;
                    this.msg.message_id = this.selectedMessage.id;
                    this.msg.reply_subject = this.msg.subject;
                    this.msg.reply_message = this.msg.message;
                    if (this.selectedMessage.status == 'MIC Meetings') {
                        this.submissionService.update(this.submission.id, {
                            confirm_date_mic: this.msg.confirm_date
                        }).subscribe();
                    }
                    if (this.selectedMessage.status == 'PAT Meetings') {
                        this.submissionService.update(this.submission.id, {
                            confirm_date_pat: this.msg.confirm_date
                        }).subscribe();
                    }
                    this.statusReplyService.create(this.msg).subscribe(res => {
                        this.loading = false;
                        this.router.navigateByUrl(`/pages/status-checker`);
                    });
                    this.createHistoryLog();
                } else {
                    this.statusMsgService.create(this.msg).subscribe(res => {
                        this.loading = false;
                        this.selectedMessage = res;
                        this.createHistoryLog(); 
                        this.router.navigateByUrl(`/pages/status-checker`);
                    })
                }
            }),
        ).subscribe();
    }

    sentNotify() {
        let message = ``;
        if (this.type == 'inquiry') {
            message = `Please check Inquiry (Form 1) Application No. <a href="http://investor.digitaldots.com.mm/officer/inquiry/form/${this.submission.inquiry_id}">${this.submission?.application_no}</a>, as the investor sent you a message`;
        }
        if (this.type == 'mic') {
            message = `Please check MIC (Form 2) Application No. <a href="http://investor.digitaldots.com.mm/officer/mic/choose-language/${this.submission.mic_id}">${this.submission?.application_no}</a>, as the investor sent you a message`;
        }
        if (this.type == 'endorsement') {
            message = `Please check Endorsement (Form 4ab) Application No. <a href="http://investor.digitaldots.com.mm/officer/endorsement/form/${this.submission.endorsement_id}">${this.submission?.application_no}</a>, as the investor sent you a message`;
        }
        if (this.type == 'tax') {
            message = `Please check Tax Incentive (Form 6) Application No. <a href="http://investor.digitaldots.com.mm/officer/tax-incentive/choose-language/${this.submission.tax_incentive_id}">${this.submission?.application_no}</a>, as the investor sent you a message`;
        }
        if (this.type == 'landright') {
            message = `Please check Land Right (Form 7) Application No. <a href="http://investor.digitaldots.com.mm/officer/land-right/landright-choose-investment/${this.submission.land_right_id}">${this.submission?.application_no}</a>, as the investor sent you a message`;
        }
        if (this.submission?.reply_user_id) {
            this.notifyService.create({
                user_id: this.submission?.reply_user_id,
                subject: `Message from ${this.user?.name}`,
                message: message
            }).subscribe();
        }
    }

    createHistoryLog() {
        let type = ``;
        let form_id = null;
        if (this.type == 'inquiry') {
            type = "Inquiry ( Form 1 )";
            form_id = this.submission?.inquiry_id;
        }
        if (this.type == 'mic') {
            type = "MIC-Permit";
            form_id = this.submission?.mic_id;
        }
        if (this.type == 'endorsement') {
            type = "Endorsement";
            form_id = this.submission?.endorsement_id;
        }
        if (this.type == 'tax') {
            type = "Tax Incentive";
            form_id = this.submission?.tax_incentive_id;
        }
        if (this.type == 'landright') {
            type = "Land Right";
            form_id = this.submission?.land_right_id;
        }
        if (this.submission?.reply_user_id) {
            forkJoin([
                this.submissionService.update(this.submission.id, {
                    sub_status: 'Reply'
                }),
                this.formLogService.create({
                    type: type,
                    type_id: this.submission.id,
                    status: this.submission.status,
                    sub_status: 'Message',
                    message_id: this.selectedMessage.id,
                    submitted_user_id: this.submission?.submitted_user_id,
                    reply_user_id: this.submission?.reply_user_id,
                    incoming: true,
                    extra: {
                        application_no: this.submission?.application_no,
                        form_id: form_id,
                        history_data: this.submission?.form
                    }
                })
            ]).subscribe();
        }
        
    }

    downloadCertificate(submission) {
        const certificateExport = new CertificateExport(submission, localStorage.getItem('lan'));
        certificateExport.download().subscribe();
    }

    previewForm(lan) {
        if (this.type == 'inquiry') {
            this.inquiryPreviewService.toFormPDF(this.submission?.form, { readOnly: true, language: lan });
        } else {
            var data = lan == 'en' ? this.submission?.form.data : this.submission?.form.data_mm;
            if (this.type == 'mic') {
                this.micPreviewService.toFormPDF(data, { readOnly: true, language: lan });
            }
            if (this.type == 'endorsement') {
                this.endoPreviewService.toFormPDF(data, { readOnly: true, language: lan });
            }
            if (this.type == 'tax') {
                this.taxInvPreviewService.toFormPDF(data, { readOnly: true, language: lan });
            }
            if (this.type == 'landright') {
                this.landRightPreviewService.toFormPDF(data, { readOnly: true, language: lan });
            }
        }
    }

    openRevision() {
        if (this.type == 'inquiry') {
            this.router.navigate(['/pages/revision', this.submission.id, this.type]);
        }
        if (this.type == 'mic') {
            this.router.navigate(['/pages/revision', this.submission.id, this.type]);
        }
        if (this.type == 'endorsement') {
            this.router.navigate(['/pages/revision', this.submission.id, this.type]);
        }
        if (this.type == 'tax') {
            this.router.navigate(['/pages/revision', this.submission.id, this.type]);
        }
        if (this.type == 'landright') {
            this.router.navigate(['/pages/revision', this.submission.id, this.type]);
        }
    }

}
