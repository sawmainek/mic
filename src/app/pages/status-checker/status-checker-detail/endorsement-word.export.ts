import { AlignmentType, Document, HeadingLevel, HeightRule, Paragraph, Table, TableCell, TableRow, TabStopPosition, TabStopType, TextRun, WidthType } from "docx";

export class EndorsementWordExport {
    // tslint:disable-next-line: typedef
    public create(
        permit: {
            section: string,
            form: { label?: string, value?: string, data?: any[], type?: string, option?: { italics?: boolean, bold?: boolean } }[],
        }[], title: string
    ): Document {
        const document = new Document();

        document.addSection({
            children: [
                new Paragraph({
                    text: title,
                    heading: HeadingLevel.TITLE,
                    alignment: AlignmentType.CENTER
                }),
                ...permit.map(x => {
                    const arr: any[] = [];
                    arr.push(this.createTextLine());
                    arr.push(this.createHeading(x.section));
                    arr.push(this.createTextLine());
                    x.form.forEach(form => {
                        switch (form.type) {
                            case 'Table':
                                arr.push(this.createTextLine(form.label, form.option));
                                arr.push(this.createTextLine());
                                arr.push(this.createTable(form.data));
                                arr.push(this.createTextLine());
                                break;
                            case 'Title':
                                arr.push(this.createTextLine(form.label, form.option));
                                arr.push(this.createTextLine());
                                break;
                            default:
                                arr.push(this.createTextLine(form.label, form.option));
                                arr.push(this.createTextLine(form.value, form.option));
                                arr.push(this.createTextLine());
                                break;
                        }
                    });
                    return arr;
                }).reduce((prev, curr) => prev.concat(curr), [])

            ],
        });

        return document;
    }

    public createHeading(text: string): Paragraph {
        return new Paragraph({
            text: text,
            heading: HeadingLevel.HEADING_1,
            thematicBreak: true,
        });
    }

    public createSubHeading(text: string): Paragraph {
        return new Paragraph({
            text: text,
            heading: HeadingLevel.HEADING_2,
        });
    }

    public createTextLine(text?: string, option?: { italics?: boolean, bold?: boolean }): Paragraph {
        return new Paragraph({
            children: [
                new TextRun({
                    text: text || '',
                    italics: option?.italics || false,
                    bold: option?.bold || false
                }),
            ],
        });
    }

    public createTable(table: any[]) {
        return new Table({
            rows: [
                ...table.map(x => {
                    return this.createTableRow(x);
                }).reduce((prev, curr) => prev.concat(curr), [])
            ],
            width: {
                size: 100,
                type: WidthType.PERCENTAGE,
            }
        })
    }

    public createTableRow(row: string[]) {
        return new TableRow({
            children: [
                ...row.map(x => {
                    return this.createTableCell(x);
                }).reduce((prev, curr) => prev.concat(curr), [])
            ],
            height: { height: 250, rule: HeightRule.AUTO }
        })
    }

    public createTableCell(value) {
        return new TableCell({
            children: [
                this.createTextLine(value || '')
            ],
            width: { size: 25, type: WidthType.PERCENTAGE },
            margins: { top: 100, left: 100, right: 100, bottom: 100 }
        })
    }
}