import { saveAs } from 'file-saver';
import { Packer } from 'docx';
import { EndorsementWordExport } from './endorsement-word.export';
import { Observable } from 'rxjs';

export class CertificateExport {
    docName: string = 'ENDORSEMENT CERTIFICATE';
    fileName: string = 'endorsement-certificate'
    endoForm: any = [];
    data: any;
    language: any = 'en';

    constructor(submission: any, lan: any) {
        this.language = lan;
        this.data = submission?.form?.language == 'English' ? submission?.form?.data : submission?.form?.data_mm;

        this.fileName = `${this.fileName}-${submission.application_no}.docx`;

        var isic_class_ids = '';
        if (Array.isArray(this.data?.company_info?.specific_business_sector?.isic_class_ids)) {
            this.data?.company_info?.specific_business_sector?.isic_class_ids.forEach(x => {
                isic_class_ids += x + ', ';
            });
            if (isic_class_ids.length > 2)
                isic_class_ids = isic_class_ids.substring(0, isic_class_ids.length - 2);
        }
        else {
            isic_class_ids = this.data?.company_info?.specific_business_sector?.isic_class_ids;
        }

        var totalMyanmarCash = Number(this.data?.expected_investmentdt?.cash_equivalent[0]?.myan_origin || 0) +
            Number(this.data?.expected_investmentdt?.cash_equivalent[1]?.myan_origin || 0) +
            Number(this.data?.expected_investmentdt?.cash_equivalent[2]?.myan_origin || 0);

        console.log(this.data);

        if (this.language == 'en') {
            this.docName = 'PERMIT/ENDORSEMENT CERTIFICATE';
            this.endoForm = [
                {
                    form: [
                        {
                            label: '1. Permit/endorsement number',
                            value: submission?.permit_endo_no || this.data?.application_no || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '2. Name of Enterprise',
                            value: this.data?.company_info?.investment_name || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '3. Form of Investment',
                            value: this.data?.invest_form?.form_investment,
                            type: 'Text', option: {}
                        },
                        {
                            label: '4. Type of Company',
                            value: this.data?.company_info?.company_type,
                            type: 'Text', option: {}
                        },
                        {
                            label: '5. Specific sector of investment',
                            value: isic_class_ids,
                            type: 'Text', option: {}
                        },
                        {
                            label: '6. Location(s) of the investment project', type: 'Table', data: [
                                ['State/Region', 'District', 'Township'],
                                ...this.data?.location?.map((x, index) => {
                                    return [x?.state, x?.district, x?.township]
                                })
                            ]
                        },
                        {
                            label: '7. Amount of Foreign Capital',
                            value: this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd ? this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd + ' Equivalent US$' : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '8. Period during which Foreign Cash and Equivalents will be brought in USD',
                            value: this.data?.expected_investmentdt?.start_date && this.data?.expected_investmentdt?.end_date ? this.data?.expected_investmentdt?.start_date + '  -  ' + this.data?.expected_investmentdt?.end_date : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '9. (Expected) investment value',
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Total Amount in Myanmar Kyat',
                            value: totalMyanmarCash,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Equivalent Amount in USD (Million)',
                            // value: this.data?.expected_investment?.spent_amount_usd,
                            value: this.data?.expected_investmentdt?.total_equivalent_usd,
                            type: 'Text', option: {}
                        },
                        {
                            label: '10. (Expected) Construction or preparatory period',
                            value: this.data?.timeline?.construction_start_date,
                            type: 'Text', option: {}
                        },
                        {
                            label: '11.  (Expected) Investment Period',
                            value: this.data?.timeline?.investment_start_date,
                            type: 'Text', option: {}
                        },
                        {
                            label: '12. Name(s) of investor(s)', type: 'Table', data: [
                                ['Name', 'Address', 'Country of Incorporation or citizenship'],
                                ...this.data?.investor_list?.map((x, index) => {
                                    if (x.company?.company_name) {
                                        return [x?.company?.company_name, x?.company?.address, x?.company?.incorporate_country]
                                    }
                                    else if (x.government?.name) {
                                        return [x?.government?.name, x?.government?.address, 'Myanmar']
                                    }
                                    else {
                                        return [x?.individual?.full_name, x?.individual?.address, x?.individual?.citizenship]
                                    }
                                })
                            ]
                        },
                    ]
                }
            ]
        }
        else {
            this.docName = 'ခွင့်ပြုမိန့် လက်မှတ်/ အတည်ပြုမိန့် လက်မှတ်';
            this.endoForm = [
                {
                    form: [
                        {
                            label: '၁။ ခွင့်ပြုမိန့် / အတည်ပြုမိန့် အမှတ်စဥ်',
                            value: submission?.permit_endo_no || this.data?.application_no || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၂။  စီးပွားရေးအဖွဲ့အစည်း၏ အမည်',
                            value: this.data?.company_info?.investment_name || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၃။ ရင်းနှီးမြှုပ်နှံမှု  ပုံစံ',
                            value: this.data?.invest_form?.form_investment,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၄။ ကုမ္ပဏီအမျိုးအစား',
                            value: this.data?.company_info?.company_type,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၅။ အဆိုပြုရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်း၏ လုပ်ငန်းကဏ္ဍအသေးစိတ်',
                            value: isic_class_ids,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၆။. ရင်းနှီးမြှုပ်နှံမှု တည်နေရာ (များ)', type: 'Table', data: [
                                ['တိုင်း/ပြည်နယ်', 'ခရိုင်', 'မြို့နယ်'],
                                ...this.data?.location?.map((x, index) => {
                                    return [x?.state, x?.district, x?.township]
                                })
                            ]
                        },
                        {
                            label: '၇။ နိုင်ငံခြား ငွေကြေးရင်းမြစ်ပမာဏ',
                            value: this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd ? this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd + ' ညီမျှသည့် ခန့်မှန်းအမေရိကန်ဒေါ်လာ' : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၈။ နိုင်ငံခြား ငွေကြေး ယူဆောင်လာမည့် ကာလ',
                            value: this.data?.expected_investmentdt?.start_date && this.data?.expected_investmentdt?.end_date ? this.data?.expected_investmentdt?.start_date + '  -  ' + this.data?.expected_investmentdt?.end_date : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၉။ ခန့်မှန်း ရင်းနှီးမြှုပ်နှံမှုတန်ဖိုး',
                            type: 'Text', option: {}
                        },
                        {
                            label: 'စုစုပေါင်း မြန်မာကျပ် ပမာဏ',
                            value: totalMyanmarCash,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'အမေရိကန်ဒေါ်လာနှင့် ညီမျှသောပမာဏ ( သန်း )',
                            // value: this.data?.expected_investment?.spent_amount_usd,
                            value: this.data?.expected_investmentdt?.total_equivalent_usd,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၁၀။ ( ခန့်မှန်း ) တည်ဆောက်ရေးကာလ သို့မဟုတ် ပြင်ဆင်မှု ကာလ',
                            value: this.data?.timeline?.construction_start_date,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၁၁။ ( ခန့်မှန်း ) ရင်းနှီးမြှုပ်နှံမှုကာလ',
                            value: this.data?.timeline?.investment_start_date,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၁၂။ ရင်းနှီးမြှုပ်နှံသူ(များ)၏ အမည်(များ)', type: 'Table', data: [
                                ['အမည်', 'လိပ်စာ', 'နိုင်ငံသား သို့မဟုတ် စီးပွားရေးဖွဲ့စည်းတည်ထောင်ထားသော နိုင်ငံ'],
                                ...this.data?.investor_list?.map((x, index) => {
                                    if (x.company?.company_name) {
                                        return [x?.company?.company_name, x?.company?.address, x?.company?.incorporate_country]
                                    }
                                    else if (x.government?.name) {
                                        return [x?.government?.name, x?.government?.address, 'Myanmar']
                                    }
                                    else {
                                        return [x?.individual?.full_name, x?.individual?.address, x?.individual?.citizenship]
                                    }
                                })
                            ]
                        },
                    ]
                }
            ]
        }
    }

    public download(): Observable<any> {
        return Observable.create((observer) => {
            const documentCreator = new EndorsementWordExport();
            const doc = documentCreator.create(this.endoForm, this.docName);
            Packer.toBlob(doc).then(blob => {
                try {
                    saveAs(blob, this.fileName);
                    observer.next("Document created successfully");
                    observer.complete();
                } catch (err) {
                    observer.error(err);
                }
            });
        });

    }
}

