import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusCheckerDetailComponent } from './status-checker-detail.component';

describe('StatusCheckerDetailComponent', () => {
  let component: StatusCheckerDetailComponent;
  let fixture: ComponentFixture<StatusCheckerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusCheckerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusCheckerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
