import { QuillModule } from 'ngx-quill';
import { GlobalModule } from './global.module';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { TaxIncentiveModule } from './tax-incentive/tax-incentive.module';
import { LandRightModule } from './land-right/land-right.module';
import { MicPermitModule } from './mic-permit/mic-permit.module';
import { EndorsementModule } from './endorsement/endorsement.module';
import { CustomComponentModule } from './../custom-component/custom-component.module';
import { InquiryModule } from './inquiry/inquiry.module';
import { PagesRoutingModule } from './page-routing.module';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        CustomComponentModule,
        TaxIncentiveModule,
        LandRightModule,
        PagesRoutingModule,
        MicPermitModule,
        EndorsementModule,
        InquiryModule,
        BrowserAnimationsModule,
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ]
})
export class PagesModule { }
