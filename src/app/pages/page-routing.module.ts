import { RevisionsComponent } from './revisions/revisions.component';
// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AuthGuard } from '../core/auth/_guards/auth.guards';

//pages
import { PaymentComponent } from './payment/payment.component';

import { StatusCheckerDetailComponent } from './status-checker/status-checker-detail/status-checker-detail.component';
import { StatusCheckerComponent } from './status-checker/status-checker/status-checker.component';
import { BaseComponent } from './base-page/base-component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app-routing.module';
import { ProfileComponent } from './account/profile/profile.component';
import { ChangeEmailComponent } from './account/change-email/change-email.component';
import { ChangePasswordComponent } from './account/change-password/change-password.component';
import { PaymentHistoryComponent } from './status-checker/payment-history/payment-history.component';

// My Component

const routes: Routes = [
    {
        path: '',
        component: BaseComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', component: HomePageComponent },
            { path: 'payment/:id', component: PaymentComponent },
            { path: 'inquiry', loadChildren: () => import('./inquiry/inquiry-routing.module').then(m => m.InquiryRoutingModule) },
            { path: 'endorsement', loadChildren: () => import('./endorsement/endorsement-routing.module').then(m => m.EndorsementRoutingModule) },
            { path: 'mic', loadChildren: () => import('./mic-permit/mic-permit-routing.module').then(m => m.MicPermitRoutingModule) },
            { path: 'land-right', loadChildren: () => import('./land-right/land-right-routing.module').then(m => m.LandRightRoutingModule) },
            { path: 'tax-incentive', loadChildren: () => import('./tax-incentive/tax-incentive-routing.module').then(m => m.TaxIncentiveRoutingModule) },

            { path: 'status-checker-detail', component: StatusCheckerDetailComponent },
            { path: 'status-checker-detail/:id/:type', component: StatusCheckerDetailComponent },
            { path: 'status-checker', component: StatusCheckerComponent },
            { path: 'status-checker/:open', component: StatusCheckerComponent },
            { path: 'status-checker/:id/:type', component: StatusCheckerComponent },
            
            { path: 'revision/:id/:type', component: RevisionsComponent },
            { path: 'payment-history', component: PaymentHistoryComponent },

            { path: 'profile', component: ProfileComponent },
            { path: 'change-email', component: ChangeEmailComponent },
            { path: 'change-password', component: ChangePasswordComponent },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        AuthGuard,
        TranslateService,
    ],
    exports: [RouterModule]
})
export class PagesRoutingModule {
}
