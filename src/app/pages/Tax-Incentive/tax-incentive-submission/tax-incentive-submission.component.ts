import { NotifyService } from './../../../../services/notify.service';
import { FormLogService } from './../../../../services/form-log.service';
import { TaxIncentivePreviewService } from './../tax-incentive-preview/tax-incentive-preview.service';
import { readyToSubmit } from './../../../core/form/_selectors/form.selectors';
import { ToastrService } from 'ngx-toastr';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormCheckBox } from 'src/app/custom-component/dynamic-forms/base/form-checkbox';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { TaxIncentiveSubmission } from '../tax-incentive-submission'
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentive } from 'src/app/models/tax_incentive';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { Form } from 'src/app/models/form';
import { SubmissionTaxIncentive } from 'src/app/models/submission_tax_incentive';
import { FormService } from 'src/services/form.service';
import { TaxincentiveSubmissionService } from 'src/services/taxincentive-submission.service';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { cloneDeep } from 'lodash';

declare var jQuery: any;
@Component({
    selector: 'app-tax-incentive-submission',
    templateUrl: './tax-incentive-submission.component.html',
    styleUrls: ['./tax-incentive-submission.component.scss']
})
export class TaxIncentiveSubmissionComponent extends TaxIncentiveSubmission implements OnInit {
    @Input() id: any;
    menu: any = "submission";
    page = "tax-incentive/submission/";
    mm: any;

    user: any = {};

    formModel: Form;
    taxIncentiveSubmissionModel: SubmissionTaxIncentive;
    service: any;

    taxModel: TaxIncentive;
    taxMMModel: TaxIncentive;
    formGroup: FormGroup;
    model: any = {};
    form: any;

    readyToSubmitNow: boolean = false;
    submitted = false;
    spinner = false;
    is_draft = false;
    isPreview = false;
    is_save = false;
    loading = false;

    section = [
        { 'id': '75(a)', 'name': 'Exemption or Relief under section 75(a) of the Myanmar Investment Law' },
        { 'id': '77(a)', 'name': 'Exemption or Relief under section 77(a) of the Myanmar Investment Law' },
        { 'id': '77(b)', 'name': 'Exemption or Relief under section 77(b) of the Myanmar Investment Law' },
        { 'id': '77(c)', 'name': 'Exemption or Relief under section 77(c) of the Myanmar Investment Law' },
        { 'id': '77(d)', 'name': 'Exemption or Relief under section 77(d) of the Myanmar Investment Law' },
        { 'id': '78(a)', 'name': 'Exemption or Relief under section 78(a) of the Myanmar Investment Law' }
    ]

    taxIncentiveForm: BaseForm<any>[] = [
        new FormCheckBox({
            key: 'is_true',
            required: true,
            label: 'I/We hereby declare that the above statements are true and correct to the best of my/our knowledge and belief.'
        }),
        new FormCheckBox({
            key: 'is_understand',
            required: true,
            value: false,
            label: 'I/We fully understand that application may be denied or unnecessarily delayed if the applicant fails to provide required information to the Commission.'
        }),
        new FormCheckBox({
            key: 'is_strictly_comply',
            required: true,
            value: false,
            label: 'I/We hereby declare to strictly comply with terms and conditions set out by the Myanmar Investment Commission.'
        }),
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtrService: FormControlService,
        private taxFormService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxincentiveSubmissionService: TaxincentiveSubmissionService,
        private taxInvPreviewService: TaxIncentivePreviewService,
        private taxService: TaxIncentiveService,
        private notifyService: NotifyService,
        private formService: FormService,
        private formLogService: FormLogService,
        private translateService: TranslateService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,

    ) {
        super(formCtrService);
        this.loading = true;
        this.store.pipe(select(currentUser)).subscribe(result => {
            this.user = result;
        });
        this.store.pipe(select(readyToSubmit)).subscribe(submission => {
            if (submission == 'Tax Incentive') {
                this.readyToSubmitNow = true;
            }
        })

        this.formGroup = this.formCtrService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
    }

    ngOnInit(): void { }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.taxFormService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.formGroup.patchValue(this.taxModel?.submission || {})

                        this.page = "tax-incentive/submission/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });

                        this.taxIncentiveSubmissionModel = this.form.submission || {};

                        this.changeLanguage();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    openMenu(menu) {
        this.menu = menu;
    }

    onReply() {
        jQuery('.nav-tabs a[href="#reply"]').tab('show');
    }

    saveDraft() {
        this.is_draft = true;
        this.spinner = true;
        const submission = { submission: this.formGroup.value };
        this.taxMMService.create({ ...this.taxModel, ...submission }).subscribe(results => {
            this.spinner = false;
            this.is_draft = false;
            this.toast.success('Saved successfully.');
        });
    }

    onPreview() {
        this.isPreview = true;
        this.spinner = true;
        const submission = { submission: this.formGroup.value };
        this.taxMMService.create({ ...this.taxModel, ...submission }).subscribe(result => {
            this.spinner = false;
            this.isPreview = false;
            this.taxInvPreviewService.toFormPDF(result, {
                readOnly: true,
                onSubmit$: () => {

                }
            })
        });
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);

        if (this.mm) {
            this.saveFormData();
        }
        else {
            this.saveTaxData();
        }

    }

    saveTaxData() {
        this.spinner = true;
        const submission = { submission: this.formGroup.value };
        this.taxService.create({ ...this.taxModel, ...submission }).subscribe(results => {
            this.spinner = false;
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            this.router.navigate([page+'/tax-incentive/submission', this.id, 'mm']);
        });
    }

    saveFormData() {

        if (!this.readyToSubmitNow) {
            this.toast.error("Please complete all section.");
            return false;
        }

        this.spinner = true;
        let data = {
            'tax_incentive_id': this.id,
            'submitted_user_id': this.user.id,
            'status': this.taxIncentiveSubmissionModel?.status || 'New',
            'sub_status': 'New',
            'allow_revision': false
        };
        data['application_no'] = this.taxIncentiveSubmissionModel.application_no || `${Math.floor(Math.random() * 999999)}`;

        if (this.taxModel.investment.is_tax_incentive == 'Application is still in progress') {
            data['permit_endorsement_no'] = this.taxModel.investment.serial_no;
        }
        else {
            data['permit_endorsement_no'] = this.taxModel.endorsement_no;
        }
        data['investment_enterprise_name'] = this.user?.name;
        data['applied'] = null;

        data['applied'] = '';
        if (this.taxModel.relief_section.applied_75_a) {
            data['applied'] += "Exemption or Relief under section 75(a) of the Myanmar Investment Law, ";
        }
        if (this.taxModel.relief_section.applied_77_a) {
            data['applied'] += "Exemption or Relief under section 77(a) of the Myanmar Investment Law, ";
        }
        if (this.taxModel.relief_section.applied_77_b) {
            data['applied'] += "Exemption or Relief under section 77(b) of the Myanmar Investment Law, ";
        }
        if (this.taxModel.relief_section.applied_77_c) {
            data['applied'] += "Exemption or Relief under section 77(c) of the Myanmar Investment Law, ";
        }
        if (this.taxModel.relief_section.applied_77_d) {
            data['applied'] += 'Exemption or Relief under section 77(d) of the Myanmar Investment Law, ';
        }
        if (this.taxModel.relief_section.applied_78_a) {
            data['applied'] += 'Exemption or Relief under section 78(a) of the Myanmar Investment Law, ';
        }

        if (data['applied'].length > 2) {
            data['applied'] = data['applied'].substring(0, data['applied'].length - 2);
        }

        // Todo for permission
        data['submit_to_role'] = 'monitoring-division';

        this.taxIncentiveSubmissionModel = { ...this.taxIncentiveSubmissionModel, ...data };
        this.taxincentiveSubmissionService.create(this.taxIncentiveSubmissionModel).subscribe(submission => {
            this.formModel = new Form;
            this.formModel.user_id = this.user.id;
            this.formModel.type = "Tax Incentive";
            this.formModel.type_id = submission.id;
            this.formModel.status = this.taxIncentiveSubmissionModel.id ? 'Resubmit' : 'New';
            this.formModel.data = this.form.data;
            this.formModel.data_mm = { ...this.taxModel, ...{ submission: this.formGroup.value } };
            // Save to Form Data/Status Log Model;
            forkJoin([
                this.taxMMService.create({ ...this.taxModel, ...{ submission: this.formGroup.value } }),
                this.taxFormService.create({ ...this.form, ...{ status: 'Submitted' } }),
                this.formService.create(this.formModel)
            ]).subscribe(results => {
                this.spinner = false;
                this.formLogService.create({
                    type: this.formModel.type,
                    type_id: this.formModel.type_id,
                    status: this.taxIncentiveSubmissionModel.id ? 'Revision' : 'New',
                    sub_status: 'Submission',
                    submitted_user_id: this.formModel.user_id,
                    reply_user_id: submission?.reply_user_id,
                    incoming: true,
                    extra: {
                        application_no: submission?.application_no,
                        form_id: submission?.tax_incentive_id,
                        history_data: { ...this.form, ...{ data_mm: this.formModel.data_mm } }
                    }
                }).subscribe();
                if (submission?.reply_user_id) {
                    this.notifyService.create({
                        user_id: submission?.reply_user_id,
                        subject: `${this.formModel.status} Tax Incentive: # ${submission?.application_no} received.`,
                        message: `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/tax-incentive/choose-language/${submission.tax_incentive_id}">${submission?.application_no}</a> for new submission.`
                    }).subscribe();
                }
                this.router.navigate(['/pages/tax-incentive/success', this.id]);
            });
        });

    }

}