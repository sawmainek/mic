import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveSubmissionComponent } from './tax-incentive-submission.component';

describe('TaxIncentiveSubmissionComponent', () => {
  let component: TaxIncentiveSubmissionComponent;
  let fixture: ComponentFixture<TaxIncentiveSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
