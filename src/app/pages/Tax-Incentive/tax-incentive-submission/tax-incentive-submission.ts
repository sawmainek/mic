import { FormCheckBox } from './../../../custom-component/dynamic-forms/base/form-checkbox';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { ISICService } from 'src/services/isic.service';
import { LocationService } from 'src/services/location.service';

export class TaxIncentiveSubmissionForm {

    public forms: BaseForm<string>[] = [];

    constructor() {

        this.forms = [
            new BaseFormGroup({
                key: 'submission',
                formGroup: [
                    new FormCheckBox({
                        key: 'is_true',
                        required: true,
                        label: 'I/We hereby declare that the above statements are true and correct to the best of my/our knowledge and belief.'
                    }),
                    new FormCheckBox({
                        key: 'is_understand',
                        required: true,
                        value: false,
                        label: 'I/We fully understand that application may be denied or unnecessarily delayed if the applicant fails to provide required information to the Commission.'
                    }),
                    new FormCheckBox({
                        key: 'is_strictly_comply',
                        required: true,
                        value: false,
                        label: 'I/We hereby declare to strictly comply with terms and conditions set out by the Myanmar Investment Commission.'
                    })
                ]
            })
        ];
    }

}