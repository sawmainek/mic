import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveSuccessComponent } from './tax-incentive-success.component';

describe('TaxIncentiveSuccessComponent', () => {
  let component: TaxIncentiveSuccessComponent;
  let fixture: ComponentFixture<TaxIncentiveSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
