import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm8Component } from './tax-incentive-form8.component';

describe('TaxIncentiveForm8Component', () => {
  let component: TaxIncentiveForm8Component;
  let fixture: ComponentFixture<TaxIncentiveForm8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
