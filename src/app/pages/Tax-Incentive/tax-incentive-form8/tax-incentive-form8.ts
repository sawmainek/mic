import { FormGroup, Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { LocationService } from 'src/services/location.service';

export class TaxIncentiveForm8{
    
    public forms: BaseForm<string>[] = [];
    formGroup: FormGroup;

    totalValue: any[] = [
        { 'total_kyat': 0, 'total_usd': 0 }
    ];

    constructor(
        private lotService: LocationService,
    ) {
        let raw_material: BaseForm<any>[] = [
            new BaseFormArray({
                key: 'items',
                formArray: [
                    new FormSectionTitleCounter({
                        label: '',
                        style: { 'text-align': 'center', 'vertical-align': 'middle' }
                    }),
                    new FormInput({
                        key: 'name',
                        required: true,
                    }),
                    new FormInput({
                        key: 'code',
                        type: 'number',
                        required: true,
                        validators: [Validators.minLength(4), Validators.maxLength(4)],
                    }),
                    new FormInput({
                        key: 'unit',
                        // type: 'number',
                        required: true
                    }),
                    new FormInput({
                        key: 'price',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup);
                        }
                    }),
                    new FormInput({
                        key: 'quantity',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup);
                        }
                    }),
                    new FormInput({
                        key: 'total',
                        type: 'number',
                        value: '0',
                        required: true,
                        readonly: true
                    }),
                    new FormSelect({
                        key: 'currency',
                        options$: this.lotService.getCurrency(),
                        required: true,
                        value: '102',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup);
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup);
                        }
                    }),
                    new FormInput({
                        key: 'usd',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup);
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: this.lotService.getCountry(),
                        required: true
                    }),
                ],
                useTable: true,
                headerRows: 2,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue();
                },
                tablePDFHeader: [
                    [
                        { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        {},
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        {},
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Total', colSpan: 8 }, {}, {}, {}, {}, {}, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                })
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['usd'] || 0);
                                })
                                return total;
                            }
                        },
                        {}
                    ]
                ],
                tableHeader: [
                    [
                        { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Total', colSpan: 8 },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_kyat;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_usd;
                            }
                        },
                    ]
                ]
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
            new FormHidden({
                key: 'total_usd'
            }),
        ];
    
        this.forms = [
            new BaseFormGroup({
                key: 'section_77_b',
                formGroup: [
                    new FormSectionTitle({
                        label: '8. Exemption or Relief under section 77(b) of the Myanmar Investment Law',
                        pageBreak: 'before',
                        pageOrientation: 'landscape'
                    }),
                    new FormTitle({
                        label: 'a. Expected average yearly revenue from investment'
                    }), 
                    new FormInput({
                        key: 'revenue_kyat',
                        label: 'Equivalent Kyat',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6',
                    }),
                    new FormInput({
                        key: 'revenue_usd',
                        label: 'Equivalent USD',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6',
                    }),
                    new FormTitle({
                        label: 'b. Expected average yearly foreign currency from investment'
                    }),
                    new FormInput({
                        key: 'foreign_kyat',
                        label: 'Equivalent Kyat',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6',
                    }),
                    new FormInput({
                        key: 'foreign_usd',
                        label: 'Equivalent USD',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6',
                    }),
                    new FormTitle({
                        label: 'c. Please indicate a list of raw materials and partially manufactured goods which need to be imported annually for the investment business'
                    }),
                    new BaseFormGroup({
                        key: 'raw_material',
                        formGroup: raw_material
                    }),
                    new FormNote({
                        label: 'Please review Myanmar Investment Rules 97 and 110 for more information on section 77(b) of the Myanmar Investment Law.'
                    })
                ]
            })
        ];
    }

    calculateTotalValue() {
        let formValue = this.formGroup.get('section_77_b').value;
        this.totalValue[0].total_kyat = 0;
        this.totalValue[0].total_usd = 0;

        formValue['raw_material']['items'].map(x => {
            this.totalValue[0].total_kyat += parseFloat(x.kyat);
            this.totalValue[0].total_usd += parseFloat(x.usd);

            this.formGroup.get('section_77_b').get('raw_material').get('total_kyat').setValue(this.totalValue[0].total_kyat);
            this.formGroup.get('section_77_b').get('raw_material').get('total_usd').setValue(this.totalValue[0].total_usd);
        });
    }

    updateFormValue(value, index, form, formGroup) {
        let formValue = this.formGroup.get('section_77_b').value;
        formValue['raw_material']['items'][index][form.key] = value;

        // Update Total Value
        let rowValue = formGroup.value;
        formGroup.get('total').setValue(Number(rowValue.quantity | 0) * Number(rowValue.price | 0));

        this.calculateTotalValue();
    }

    calculateEquivalent(value, index, form, formGroup) {
        let formValue = this.formGroup.get('section_77_b').value;
        formValue['raw_material']['items'][index][form.key] = value;
        this.calculateTotalValue();
    }
}