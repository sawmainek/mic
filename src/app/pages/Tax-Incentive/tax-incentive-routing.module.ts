import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxIncentiveForm1Component } from './tax-incentive-form1/tax-incentive-form1.component';
import { TaxIncentiveForm2Component } from './tax-incentive-form2/tax-incentive-form2.component';
import { TaxIncentiveForm3Component } from './tax-incentive-form3/tax-incentive-form3.component';
import { TaxIncentiveForm4Component } from './tax-incentive-form4/tax-incentive-form4.component';
import { TaxIncentiveForm6Component } from './tax-incentive-form6/tax-incentive-form6.component';
import { TaxIncentiveForm7Component } from './tax-incentive-form7/tax-incentive-form7.component';
import { TaxIncentiveForm5Component } from './tax-incentive-form5/tax-incentive-form5.component';
import { TaxIncentiveForm8Component } from './tax-incentive-form8/tax-incentive-form8.component';
import { TaxIncentiveForm9Component } from './tax-incentive-form9/tax-incentive-form9.component';
import { TaxIncentiveForm10Component } from './tax-incentive-form10/tax-incentive-form10.component';
import { TaxIncentiveForm11Component } from './tax-incentive-form11/tax-incentive-form11.component';
import { TaxIncentiveForm12Component } from './tax-incentive-form12/tax-incentive-form12.component';
import { TaxIncentiveSubmissionComponent } from './tax-incentive-submission/tax-incentive-submission.component';
import { TaxIncentiveSuccessComponent } from './tax-incentive-success/tax-incentive-success.component';
import { TaxincentiveChooseLanguageComponent } from './taxincentive-choose-language/taxincentive-choose-language.component';
import { TaxIncentivePaymentComponent } from './tax-incentive-payment/tax-incentive-payment.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'choose-language', pathMatch: 'full' },
            { path: 'success/:id', component: TaxIncentiveSuccessComponent },
            { path: 'success/:id/:mm', component: TaxIncentiveSuccessComponent },

            { path: 'form1', component: TaxIncentiveForm1Component },
            { path: 'form1/:id', component: TaxIncentiveForm1Component },
            { path: 'form1/:id/:mm', component: TaxIncentiveForm1Component },

            { path: 'form2/:id', component: TaxIncentiveForm2Component },
            { path: 'form2/:id/:mm', component: TaxIncentiveForm2Component },

            { path: 'form3/:id', component: TaxIncentiveForm3Component },
            { path: 'form3/:id/:mm', component: TaxIncentiveForm3Component },

            { path: 'form4/:id', component: TaxIncentiveForm4Component },
            { path: 'form4/:id/:mm', component: TaxIncentiveForm4Component },

            { path: 'form5/:id', component: TaxIncentiveForm5Component },
            { path: 'form5/:id/:mm', component: TaxIncentiveForm5Component },

            { path: 'form6/:id', component: TaxIncentiveForm6Component },
            { path: 'form6/:id/:mm', component: TaxIncentiveForm6Component },

            { path: 'form7/:id', component: TaxIncentiveForm7Component },
            { path: 'form7/:id/:mm', component: TaxIncentiveForm7Component },

            { path: 'form8/:id', component: TaxIncentiveForm8Component },
            { path: 'form8/:id/:mm', component: TaxIncentiveForm8Component },

            { path: 'form9/:id', component: TaxIncentiveForm9Component },
            { path: 'form9/:id/:mm', component: TaxIncentiveForm9Component },

            { path: 'form10/:id', component: TaxIncentiveForm10Component },
            { path: 'form10/:id/:mm', component: TaxIncentiveForm10Component },

            { path: 'form11/:id', component: TaxIncentiveForm11Component },
            { path: 'form11/:id/:mm', component: TaxIncentiveForm11Component },

            { path: 'form12/:id', component: TaxIncentiveForm12Component },
            { path: 'form12/:id/:mm', component: TaxIncentiveForm12Component },

            { path: 'submission/:id', component: TaxIncentiveSubmissionComponent },
            { path: 'submission/:id/:mm', component: TaxIncentiveSubmissionComponent },

            { path: 'choose-language', component: TaxincentiveChooseLanguageComponent },
            { path: 'choose-language/:id', component: TaxincentiveChooseLanguageComponent },
            { path: 'choose-language/:id/:mm', component: TaxincentiveChooseLanguageComponent },

            { path: 'payment/:id', component: TaxIncentivePaymentComponent },
            { path: 'payment/:id/:mm', component: TaxIncentivePaymentComponent },

        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class TaxIncentiveRoutingModule { }
