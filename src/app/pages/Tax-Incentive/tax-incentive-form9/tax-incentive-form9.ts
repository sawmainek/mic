import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocationService } from 'src/services/location.service';

export class TaxIncentiveForm9{
    
    public forms: BaseForm<string>[] = [];

    constructor(
        private lotService: LocationService,
    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'section_77_c',
                formGroup: [
                    new FormSectionTitle({
                        label: '9. Exemption or Relief under section 77(c) of the Myanmar Investment Law'
                    }),
                    new FormTitle({
                        label: 'a. Expected average yearly revenue from investment'
                    }),
                    new FormInput({
                        key: 'revenue_kyat',
                        label: 'Equivalent Kyat',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6',
                    }),
                    new FormInput({
                        key: 'revenue_usd',
                        label: 'Equivalent USD',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6',
                    }),
                    new FormTitle({
                        label: 'b. Expected average yearly foreign currency from investment'
                    }),
                    new FormInput({
                        key: 'foreign_kyat',
                        label: 'Equivalent Kyat',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6',
                    }),
                    new FormInput({
                        key: 'foreign_usd',
                        label: 'Equivalent USD',
                        type: 'number',
                        required: true,
                        columns: 'col-md-6',
                    }),
                    new FormSectionTitle({
                        label: 'c. Please upload the following documents:'
                    }),
                    new FormTitle({
                        label: 'Import declaration on imported raw materials and partially manufactured goods which are used to manufacture products for export:'
                    }),
                    new BaseFormArray({
                        key: 'used_for_export_document',
                        formArray: [
                            new FormFile({
                                key: 'used_for_export_document_doc',
                                label: 'Name of document:',
                                multiple: true
                            })
                        ],
                    }),
                    new FormTitle({
                        label: 'Export declaration for manufactured products:'
                    }),
                    new BaseFormArray({
                        key: 'declare_for_manufacture_document',
                        formArray: [
                            new FormFile({
                                key: 'declare_for_manufacture_document_doc',
                                label: 'Name of document:',
                                multiple: true
                            })
                        ],
                    }),
                    new FormTitle({
                        label: 'Recommendation from respective association committee:'
                    }),
                    new BaseFormArray({
                        key: 'recommend_document',
                        formArray: [
                            new FormFile({
                                key: 'recommend_document_doc',
                                label: 'Name of document:',
                                multiple: true
                            })
                        ],
                    }),
                    new FormNote({
                        label: 'Please review Myanmar Investment Rules 77 and 98 for more information on section 77(c) of the Myanmar Investment Law.'
                    })
                ]
            })
        ];
    }

}