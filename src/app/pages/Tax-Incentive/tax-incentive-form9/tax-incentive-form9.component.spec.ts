import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm9Component } from './tax-incentive-form9.component';

describe('TaxIncentiveForm9Component', () => {
  let component: TaxIncentiveForm9Component;
  let fixture: ComponentFixture<TaxIncentiveForm9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
