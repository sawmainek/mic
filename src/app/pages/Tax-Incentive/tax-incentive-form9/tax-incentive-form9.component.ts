import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormGroup } from '@angular/forms';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { Observable } from 'rxjs';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { HttpClient } from '@angular/common/http';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';

@Component({
    selector: 'app-tax-incentive-form9',
    templateUrl: './tax-incentive-form9.component.html',
    styleUrls: ['./tax-incentive-form9.component.scss']
})
export class TaxIncentiveForm9Component extends TaxIncentiveFormData implements OnInit {
    menu: any = "formData";
    page = "tax-incentive/form9/";
    @Input() id: any;
    mm: any;

    user: any = {};

    formGroup: FormGroup;
    taxModel: any = {};
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    taxIncentiveForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'section_77_c',
            formGroup: [
                new FormSectionTitle({
                    label: '9. Exemption or Relief under section 77(c) of the Myanmar Investment Law'
                }),
                new FormTitle({
                    label: 'a. Expected average yearly revenue from investment'
                }),
                new FormInput({
                    key: 'revenue_kyat',
                    label: 'Equivalent Kyat',
                    type: 'number',
                    required: true,
                    columns: 'col-md-6',
                }),
                new FormInput({
                    key: 'revenue_usd',
                    label: 'Equivalent USD',
                    type: 'number',
                    required: true,
                    columns: 'col-md-6',
                }),
                new FormTitle({
                    label: 'b. Expected average yearly foreign currency from investment'
                }),
                new FormInput({
                    key: 'foreign_kyat',
                    label: 'Equivalent Kyat',
                    type: 'number',
                    required: true,
                    columns: 'col-md-6',
                }),
                new FormInput({
                    key: 'foreign_usd',
                    label: 'Equivalent USD',
                    type: 'number',
                    required: true,
                    columns: 'col-md-6',
                }),
                new FormSectionTitle({
                    label: 'c. Please upload the following documents:'
                }),
                new FormTitle({
                    label: 'Import declaration on imported raw materials and partially manufactured goods which are used to manufacture products for export:'
                }),
                new BaseFormArray({
                    key: 'used_for_export_document',
                    formArray: [
                        new FormFile({
                            key: 'used_for_export_document_doc',
                            label: 'Name of document:',
                            multiple: true
                        })
                    ],
                }),
                new FormTitle({
                    label: 'Export declaration for manufactured products:'
                }),
                new BaseFormArray({
                    key: 'declare_for_manufacture_document',
                    formArray: [
                        new FormFile({
                            key: 'declare_for_manufacture_document_doc',
                            label: 'Name of document:',
                            multiple: true
                        })
                    ],
                }),
                new FormTitle({
                    label: 'Recommendation from respective association committee:'
                }),
                new BaseFormArray({
                    key: 'recommend_document',
                    formArray: [
                        new FormFile({
                            key: 'recommend_document_doc',
                            label: 'Name of document:',
                            multiple: true
                        })
                    ],
                }),
                new FormNote({
                    label: 'Please review Myanmar Investment Rules 77 and 98 for more information on section 77(c) of the Myanmar Investment Law.'
                })
            ]
        })
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private lotService: LocationService,
        private formService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxService: TaxIncentiveService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        super(formCtlService);
        this.loading = true;

        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.taxModel.section_77_c = this.taxModel?.section_77_c || form?.data?.section_77_c || {};

                        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm, this.taxModel);

                        this.page = "tax-incentive/form9/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });


                        this.changeLanguage();
                        this.loading = false;
                        this.loaded = true;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.taxIncentiveForm, this.formGroup, { type_id: this.id }, (formValue) => {
            this.taxModel = { ...this.taxModel, ...formValue };
            this.service.create(this.taxModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }
    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            // TODO: check for multiple route
            var section = this.taxModel.relief_section;
            if (section.applied_77_d == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form10', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form10', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form9', id, 'mm']);
                    }
                }
                return;
            }
            else if (section.applied_78_a == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form12', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form12', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form9', id, 'mm']);
                    }
                }
                return;
            } else {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/submission', id, 'mm']);
                } else {
                    //For choose English/Myanmar Language
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/submission', id, 'mm']);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form9', id, 'mm']);
                    }
                }
            }
        }

    }
}
