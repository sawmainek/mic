import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { TaxIncentiveProgress } from './tax-incentive-progress';

export class TaxIncentiveSubmission extends TaxIncentiveProgress {
    static section = "Submission";
    static totalForm = 1;
    totalForm = 1;

    constructor(public formCtlService: FormControlService) {
        super();
    }

    saveFormProgress(form_id: number, id: number, page: string) {
        if (this.progressForms?.length > 0) {
            if (this.progressForms.filter(x => x.type == TaxIncentiveProgress.type && x.section == TaxIncentiveSubmission.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: TaxIncentiveProgress.type,
                    type_id: id,
                    section: TaxIncentiveSubmission.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }

        }
        else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: TaxIncentiveSubmission.type,
                type_id: id,
                section: TaxIncentiveSubmission.section,
                page: page,
                status: true,
                revise: false
            });
        }
    }
}