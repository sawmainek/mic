import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm11Component } from './tax-incentive-form11.component';

describe('TaxIncentiveForm11Component', () => {
  let component: TaxIncentiveForm11Component;
  let fixture: ComponentFixture<TaxIncentiveForm11Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm11Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
