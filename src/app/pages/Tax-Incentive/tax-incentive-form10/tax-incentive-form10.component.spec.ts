import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm10Component } from './tax-incentive-form10.component';

describe('TaxIncentiveForm10Component', () => {
  let component: TaxIncentiveForm10Component;
  let fixture: ComponentFixture<TaxIncentiveForm10Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm10Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
