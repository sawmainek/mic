import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocalService } from 'src/services/local.service';
import { LocationService } from 'src/services/location.service';

export class TaxIncentiveForm7 {

    public forms: BaseForm<string>[] = []

    constructor(
        private lotService: LocationService,
        private localService: LocalService,
    ) {
        let company: BaseForm < any > [] =[
            new FormInput({
                key: 'company_name',
                label: 'a. Name',
                required: true
            }),
            new FormSelect({
                key: 'company_type',
                label: 'b. Type of company',
                options$: this.localService.getCompany(),
                required: true
            }),
            new FormTitle({
                label: 'Please upload a copy of the company registration certificate:',
            }),
            new FormFile({
                key: 'registration_document',
                label: 'File Name',
                required: true
            }),
            new FormTitle({
                label: 'c. Address'
            }),
            new FormSelect({
                key: 'company_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'company_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'company_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'company_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: this.lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'company_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'company_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'company_address',
                label: 'Additional address details',
                required: true
            }),  
            new FormSectionTitle({
                label: 'd. Contact Person Information'
            }),
            new FormInput({
                key: 'contact_person_name',
                label: 'Full name',
                required: true,
                columns: 'col-md-6',
            }),
            new FormInput({
                key: 'contact_person_position',
                label: 'Position',
                required: true,
                columns: 'col-md-6',
            }),
            new FormInput({
                key: 'contact_person_phone',
                label: 'Phone',
                required: true,
                type: 'number',
                validators: [Validators.minLength(6)],
                columns: 'col-md-6',
            }),
            new FormInput({
                key: 'contact_person_email',
                label: 'Email',
                required: true,
                validators: [Validators.minLength(6), Validators.email],
                columns: 'col-md-6',
            }),
        ];

        let person: BaseForm < any > [] =[
            new FormInput({
                key: 'person_name',
                label: 'a. Full name',
                required: true
            }),
            new FormSelect({
                key: 'citizenship',
                label: 'b. Citizenship',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormInput({
                key: 'nrc',
                label: 'c. Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
                required: true,
            }),
            new FormTitle({
                label: 'd. Address',
                required: true
            }),
            new FormSelect({
                key: 'person_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                required: true,
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'person_state',
                label: 'State / Region:',
                options$: this.lotService.getState(),
                required: true,
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'person_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'person_district',
                label: 'District:',
                options$: this.lotService.getDistrict(),
                required: true,
                columns: 'col-md-4',
                filter: {
                    parent: 'person_state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'person_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'person_township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: this.lotService.getTownship(),
                required: true,
                filter: {
                    parent: 'person_district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'person_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'person_address',
                label: 'Additional address details',
                required: true
            }),  
            new FormInput({
                key: 'person_phone',
                label: 'e. Phone number',
                required: true,
                type: 'number',
                validators: [Validators.minLength(6)],
                columns: 'col-md-6',
            }),
            new FormInput({
                key: 'person_email',
                label: 'f. E -mail address',
                required: true,
                validators: [Validators.minLength(6), Validators.email],
                columns: 'col-md-6',
            }),
            new FormTitle({
                label: 'Please upload a copy of passport (foreign nationals) or National Registration Card (Myanmar citizens):',
            }),
            new FormFile({
                key: 'passport_nrc_document',
                label: 'File Name',
                required: true
            }),
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'section_77_a_investor',
                formGroup: [
                    new FormTitle({
                        label: 'b. Does the investor wish to authorize a person or company to import the materials on their behalf?'
                    }),
                    new FormRadio({
                        key: 'authorize_person',
                        label: 'Yes, a company',
                        value: 'Company',
                        columns: 'col-md-4',
                        required: true
                    }),
                    new FormRadio({
                        key: 'authorize_person',
                        label: 'Yes, a person',
                        value: 'Person',
                        columns: 'col-md-4',
                        required: true
                    }),
                    new FormRadio({
                        key: 'authorize_person',
                        label: 'No',
                        value: 'No',
                        columns: 'col-md-4',
                        required: true
                    }),
                    new BaseFormGroup({
                        key: 'company',
                        formGroup: company,
                        criteriaValue: {
                            key: 'authorize_person',
                            value: 'Company'
                        }
                    }),
                    new BaseFormGroup({
                        key: 'person',
                        formGroup: person,
                        criteriaValue: {
                            key: 'authorize_person',
                            value: 'Person'
                        }
                    }),
                    new FormNote({
                        label: 'Please review Myanmar Investment Rules 106, 108, 109 and 112 for more information on section 77(a) of the Myanmar Investment Law.'
                    })
                ]
            })
        ];
    }
}