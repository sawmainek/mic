import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm7Component } from './tax-incentive-form7.component';

describe('TaxIncentiveForm7Component', () => {
  let component: TaxIncentiveForm7Component;
  let fixture: ComponentFixture<TaxIncentiveForm7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
