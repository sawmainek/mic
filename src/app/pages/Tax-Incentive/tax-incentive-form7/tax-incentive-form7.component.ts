import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormGroup, Validators } from '@angular/forms';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { Observable } from 'rxjs';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { HttpClient } from '@angular/common/http';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { TaxIncentive } from 'src/app/models/tax_incentive';
import { LocationService } from 'src/services/location.service';
import { LocalService } from 'src/services/local.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
declare var jQuery: any;
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-tax-incentive-form7',
    templateUrl: './tax-incentive-form7.component.html',
    styleUrls: ['./tax-incentive-form7.component.scss']
})
export class TaxIncentiveForm7Component extends TaxIncentiveFormData implements OnInit {
    menu: any = "formData";
    page = "tax-incentive/form7/";
    @Input() id: any;
    mm: any;

    user: any = {};

    formGroup: FormGroup;
    taxModel: any = {};
    form: any;
    service: any;

    company_types: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    company: BaseForm<any>[] = [
        new FormInput({
            key: 'company_name',
            label: 'a. Name',
            required: true
        }),
        new FormSelect({
            key: 'company_type',
            label: 'b. Type of company',
            options$: this.localService.getCompany(),
            required: true
        }),
        new FormTitle({
            label: 'Please upload a copy of the company registration certificate:',
        }),
        new FormFile({
            key: 'registration_document',
            label: 'File Name',
            required: true
        }),
        new FormTitle({
            label: 'c. Address'
        }),
        new FormSelect({
            key: 'company_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'company_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'company_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'company_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'company_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'company_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'company_address',
            label: 'Additional address details',
            required: true
        }),
        new FormSectionTitle({
            label: 'd. Contact Person Information'
        }),
        new FormInput({
            key: 'contact_person_name',
            label: 'Full name',
            required: true,
            columns: 'col-md-6',
        }),
        new FormInput({
            key: 'contact_person_position',
            label: 'Position',
            required: true,
            columns: 'col-md-6',
        }),
        new FormInput({
            key: 'contact_person_phone',
            label: 'Phone',
            required: true,
            type: 'number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6',
        }),
        new FormInput({
            key: 'contact_person_email',
            label: 'Email',
            required: true,
            validators: [Validators.minLength(6), Validators.email],
            columns: 'col-md-6',
        }),
    ];

    person: BaseForm<any>[] = [
        new FormInput({
            key: 'person_name',
            label: 'a. Full name',
            required: true
        }),
        new FormSelect({
            key: 'citizenship',
            label: 'b. Citizenship',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormInput({
            key: 'nrc',
            label: 'c. Passport number (foreign nationals) or National Registration Card number (Myanmar citizens)',
            required: true,
        }),
        new FormTitle({
            label: 'd. Address'
        }),
        new FormSelect({
            key: 'person_country',
            label: 'Country',
            options$: this.lotService.getCountry(),
            required: true,
            value: 'Myanmar'
        }),
        new FormSelect({
            key: 'person_state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-4',
            criteriaValue: {
                key: 'person_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'person_district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-4',
            filter: {
                parent: 'person_state',
                key: 'state'
            },
            criteriaValue: {
                key: 'person_country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'person_township',
            label: 'Township:',
            columns: 'col-md-4',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'person_district',
                key: 'district'
            },
            criteriaValue: {
                key: 'person_country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'person_address',
            label: 'Additional address details',
            required: true
        }),
        new FormInput({
            key: 'person_phone',
            label: 'e. Phone number',
            required: true,
            type: 'number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6',
        }),
        new FormInput({
            key: 'person_email',
            label: 'f. E -mail address',
            required: true,
            validators: [Validators.minLength(6), Validators.email],
            columns: 'col-md-6',
        }),
        new FormTitle({
            label: 'Please upload a copy of passport (foreign nationals) or National Registration Card (Myanmar citizens):',
        }),
        new FormFile({
            key: 'passport_nrc_document',
            label: 'File Name',
            required: true
        }),
    ];

    taxIncentiveForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'section_77_a_investor',
            formGroup: [
                new FormTitle({
                    label: 'b. Does the investor wish to authorize a person or company to import the materials on their behalf?'
                }),
                new FormRadio({
                    key: 'authorize_person',
                    label: 'Yes, a company',
                    value: 'Company',
                    columns: 'col-md-4',
                    required: true
                }),
                new FormRadio({
                    key: 'authorize_person',
                    label: 'Yes, a person',
                    value: 'Person',
                    columns: 'col-md-4',
                    required: true
                }),
                new FormRadio({
                    key: 'authorize_person',
                    label: 'No',
                    value: 'No',
                    columns: 'col-md-4',
                    required: true
                }),
                new BaseFormGroup({
                    key: 'company',
                    formGroup: this.company,
                    criteriaValue: {
                        key: 'authorize_person',
                        value: 'Company'
                    }
                }),
                new BaseFormGroup({
                    key: 'person',
                    formGroup: this.person,
                    criteriaValue: {
                        key: 'authorize_person',
                        value: 'Person'
                    }
                }),
                new FormNote({
                    label: 'Please review Myanmar Investment Rules 106, 108, 109 and 112 for more information on section 77(a) of the Myanmar Investment Law.'
                })
            ]
        })
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private taxIncentiveservice: TaxIncentiveService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private formService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxService: TaxIncentiveService,
        public location: Location,
        private lotService: LocationService,
        private localService: LocalService,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        super(formCtlService);
        this.loading = true;

        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.taxModel.section_77_a_investor = this.taxModel?.section_77_a_investor || form?.data?.section_77_a_investor || {};

                        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm, this.taxModel);

                        this.page = "tax-incentive/form7/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                        this.loaded = true;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var company_type = this.company.filter(x => x.key == "company_type")[0];
        company_type.options$ = this.localService.getCompany(lan);

        this.authService.changeLanguage$.subscribe(x => {
            company_type.options$ = this.localService.getCompany(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.taxIncentiveForm, this.formGroup, { type_id: this.id }, (formValue) => {
            this.taxModel = { ...this.taxModel, ...formValue };
            this.service.create(this.taxModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }
    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            // TODO: check for multiple route
            var section = this.taxModel.relief_section;
            if (section.applied_77_b == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form8', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form8', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form7', id, 'mm']);
                    }
                }
                return;
            }
            else if (section.applied_77_c == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form9', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form9', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form7', id, 'mm']);
                    }
                }
                return;
            }
            else if (section.applied_77_d == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form10', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form10', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form7', id, 'mm']);
                    }
                }
                return;
            }
            else if (section.applied_78_a == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form12', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form12', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form7', id, 'mm']);
                    }
                }
                return;
            } else {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/submission', id, 'mm']);
                } else {
                    //For choose English/Myanmar Language
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/submission', id, 'mm']);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form7', id, 'mm']);
                    }
                }
            }
        }

    }
}
