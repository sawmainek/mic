import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveSideMenuComponent } from './tax-incentive-side-menu.component';

describe('TaxIncentiveSideMenuComponent', () => {
  let component: TaxIncentiveSideMenuComponent;
  let fixture: ComponentFixture<TaxIncentiveSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
