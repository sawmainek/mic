import { formProgress } from './../../../core/form/_selectors/form.selectors';
import { currentForm } from 'src/app/core/form';
import { AppState } from './../../../core/reducers/index';
import { Store, select } from '@ngrx/store';
import { ReadyToSubmit, FormProgress } from './../../../core/form/_actions/form.actions';
import { TaxIncentiveFormService } from './../../../../services/tax-incentive-form.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { FormProgressService } from 'src/services/form-progress.service';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { TaxIncentiveSubmission } from '../tax-incentive-submission';
import { TaxIncentiveLanguage } from '../taxincentive-choose-language/taxincentive-choose-language';
import { TaxIncentive } from 'src/app/models/tax_incentive';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';

@Component({
    selector: 'app-tax-incentive-side-menu',
    templateUrl: './tax-incentive-side-menu.component.html',
    styleUrls: ['./tax-incentive-side-menu.component.scss']
})
export class TaxIncentiveSideMenuComponent implements OnInit {
    @Input() menu: any = "choose-language";
    @Input() progress: any = false;
    @Output() progressForms = new EventEmitter<any>();

    pages = [
        'tax-incentive/form2',
        'tax-incentive/form3',
        'tax-incentive/form4',
        'tax-incentive/form5',
        'tax-incentive/form6',
        'tax-incentive/form7',
        'tax-incentive/form8',
        'tax-incentive/form9',
        'tax-incentive/form10',
        'tax-incentive/form11',
        'tax-incentive/form12'
    ];

    user: any;
    is_officer: boolean = false;

    form: any = {};
    model: any = {};
    taxModel: TaxIncentive;
    taxMMModel: TaxIncentive;
    taxSubmissionModel: any = {};

    completeSections = [];
    progressLists: any[] = [];

    id: any = 0;
    mm: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private taxFormService: TaxIncentiveFormService,
        private formProgressService: FormProgressService,
        public formCtlService: FormControlService,
        private store: Store<AppState>
    ) {
        
    }

    ngOnInit(): void { 
        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            if (this.id > 0) {
                // Get data from state
                this.store.pipe(select(currentForm))
                    .subscribe(data => {

                        if (data.form) {
                            this.form = data.form || {};
                            this.taxModel = this.form.data || {};
                            this.taxMMModel = this.form.data_mm || {};
                            this.model = this.form?.language == 'Myanmar' ? this.form?.data_mm || {} : this.form?.data || {};
                            this.taxSubmissionModel = this.form.submission || {};
                            this.createProgressList();
                            this.editTotalForms();
                        } else {
                            // Get data from API
                            this.taxFormService.show(this.id)
                                .subscribe((form: any) => {
                                    this.form = form || {};
                                    this.taxModel = this.form.data || {};
                                    this.taxMMModel = this.form.data_mm || {};
                                    this.model = this.form?.language == 'Myanmar' ? this.form?.data_mm || {} : this.form?.data || {};
                                    this.taxSubmissionModel = this.form.submission || {};
                                    this.createProgressList();
                                    this.editTotalForms();
                                });
                        }
                    }).unsubscribe();
            }

        }).unsubscribe();
    }

    createProgressList() {
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = progress;
                    this.getFormProgress();
                    this.editTotalForms();
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:Tax Incentive`
                    }).subscribe(results => {
                        this.progressLists = results;
                        this.getFormProgress();
                        this.editTotalForms();
                        this.store.dispatch(new FormProgress({ progress: this.progressLists }));
                    });
                }
            }).unsubscribe();
    }

    getFormProgress() {
        if (this.progressLists.length > 0) {
            this.progressForms.emit(this.progressLists);
            let sections = ["Choose Language", "Form Data", "Submission"];
            this.completeSections = sections.map(x => this.isSectionComplete(x));

            //Enable submission button
            if (this.completeSections[0] && this.completeSections[1]) {
                setTimeout(() => {
                    this.store.dispatch(new ReadyToSubmit({ submission: 'Tax Incentive' }));
                }, 600);
            }
        }
        else {
            this.progressForms.emit([]);
        }
    }

    checkRevise(section) {
        const revise = this.progressLists.filter(x => x.section == section && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    editTotalForms() {
        if (this.form?.language == 'Myanmar') {
            if (this.model?.relief_section?.applied_77_a || this.model?.relief_section?.applied_77_d) {
                TaxIncentiveFormData.totalForm = new TaxIncentiveFormData(this.formCtlService).totalForm + 1;
            }
            else {
                TaxIncentiveFormData.totalForm = new TaxIncentiveFormData(this.formCtlService).totalForm;
            }
            TaxIncentiveSubmission.totalForm = new TaxIncentiveSubmission(this.formCtlService).totalForm;
        }
        else {
            TaxIncentiveFormData.totalForm = new TaxIncentiveFormData(this.formCtlService).totalForm * 2;
            TaxIncentiveSubmission.totalForm = new TaxIncentiveSubmission(this.formCtlService).totalForm * 2;
        }
    }

    checkPageComplete(page) {
        const length = this.progressLists.filter(x => x.page == `${page}/${this.taxMMModel?.id || 0}/mm`).length;
        return length > 0 ? 1 : 0;
    }

    checkCompleteFormData() {
        this.pages = [
            'tax-incentive/form2',
            'tax-incentive/form3',
            'tax-incentive/form4'
        ];

        if (this.model.relief_section?.applied_75_a) {
            this.pages.push('tax-incentive/form5');
        }
        if (this.model.relief_section?.applied_77_a) {
            this.pages.push('tax-incentive/form6');
        }
        if (this.model.relief_section?.applied_77_a && this.form?.language == 'Myanmar') {
            this.pages.push('tax-incentive/form7');
        }
        if (this.model.relief_section?.applied_77_b) {
            this.pages.push('tax-incentive/form8');
        }
        if (this.model.relief_section?.applied_77_c) {
            this.pages.push('tax-incentive/form9');
        }
        if (this.model.relief_section?.applied_77_d) {
            this.pages.push('tax-incentive/form10');
        }
        if (this.model.relief_section?.applied_77_d && this.form?.language == 'Myanmar') {
            this.pages.push('tax-incentive/form11');
        }
        if (this.model.relief_section?.applied_78_a) {
            this.pages.push('tax-incentive/form12');
        }

        var totalLength = 0;
        this.pages.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.taxMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.taxModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.taxMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    checkCompleteSubmission() {
        this.pages = [
            'tax-incentive/submission'
        ];

        var totalLength = 0;
        this.pages.forEach(page => {
            if (this.form?.language == 'Myanmar') {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.taxMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0);
            } else {
                const length = this.progressLists.filter(x => x.page == `${page}/${this.taxModel.id}`).length;
                const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.taxMMModel.id}/mm`).length;
                totalLength = totalLength + (length > 0 ? 1 : 0) + (lengthMM > 0 ? 1 : 0);
            }
        });
        return totalLength;
    }

    isSectionComplete(menu) {
        let hasComplete = false;
        switch (menu) {
            case TaxIncentiveLanguage.section: { //"Choose Language"
                if (TaxIncentiveLanguage.totalForm <= this.checkPageComplete('tax-incentive/choose-language')) {
                    hasComplete = true;
                }
                break;
            }
            case TaxIncentiveFormData.section: { // "Form Data"
                if (TaxIncentiveFormData.totalForm <= this.checkCompleteFormData()) {
                    hasComplete = true;
                }
                break;
            }
            case TaxIncentiveSubmission.section: { // "Submission"
                if (TaxIncentiveSubmission.totalForm <= this.checkCompleteSubmission()) {
                    hasComplete = true;
                }
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
        return hasComplete;
    }

    openSection(index, link) {
        // For New Flow
        if(this.allowRevision) {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            if (this.form?.language == 'Myanmar') {
                this.router.navigate([page + '/tax-incentive/' + link, this.form?.id, 'mm']);
            } else {
                this.router.navigate([page + '/tax-incentive/' + link, this.form?.id]);
            }
        }
    }

    get allowRevision(): boolean {
        if (window.location.href.indexOf('officer') == -1 && this.form?.submission) {
            return false;
        }
        return true;
    }

}
