import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { TaxIncentiveProgress } from './tax-incentive-progress';

export class TaxIncentiveFormData extends TaxIncentiveProgress {
    static section = "Form Data";
    static totalForm = 4;
    totalForm = 4;
    constructor(public formCtlService: FormControlService) {
        super();
    }

    saveFormProgress(form_id: number, id: number, page: string) {
        if (this.progressForms?.length > 0) {
           if (this.progressForms.filter(x => x.type == TaxIncentiveProgress.type && x.section == TaxIncentiveFormData.section && x.page == page).length <= 0) {
                this.formCtlService.saveFormProgress({
                    form_id: form_id,
                    type: TaxIncentiveProgress.type,
                    type_id: id,
                    section: TaxIncentiveFormData.section,
                    page: page,
                    status: true,
                    revise: false
                });
            }

        }
        else {
            this.formCtlService.saveFormProgress({
                form_id: form_id,
                type: TaxIncentiveFormData.type,
                type_id: id,
                section: TaxIncentiveFormData.section,
                page: page,
                status: true,
                revise: false
            });
        }
    }
}