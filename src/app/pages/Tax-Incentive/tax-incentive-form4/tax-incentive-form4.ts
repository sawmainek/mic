import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormCheckBox } from 'src/app/custom-component/dynamic-forms/base/form-checkbox';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';

export class TaxIncentiveForm4{
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'relief_section',
                formGroup: [
                    new FormSectionTitle({
                        label: '5. Please select which of the following tax incentives are being applied for'
                    }),
                    new FormCheckBox({
                        key: 'applied_75_a',
                        label: 'Exemption or Relief under section 75(a) of the Myanmar Investment Law',
                        value: false
                    }),
                    new FormCheckBox({
                        key: 'applied_77_a',
                        label: 'Exemption or Relief under section 77(a) of the Myanmar Investment Law',
                        value: false
                    }),
                    new FormCheckBox({
                        key: 'applied_77_b',
                        label: 'Exemption or Relief under section 77(b) of the Myanmar Investment Law',
                        value: false
                    }),
                    new FormCheckBox({
                        key: 'applied_77_c',
                        label: 'Exemption or Relief under section 77(c) of the Myanmar Investment Law',
                        value: false
                    }),
                    new FormCheckBox({
                        key: 'applied_77_d',
                        label: 'Exemption or Relief under section 77(d) of the Myanmar Investment Law',
                        value: false
                    }),
                    new FormCheckBox({
                        key: 'applied_78_a',
                        label: 'Exemption or Relief under section 78(a) of the Myanmar Investment Law',
                        value: false
                    }),
                    new FormNote({
                        label: 'When applying for incentives under section 75 (a), 77(a) and 78 (a), please first see Notification No. 84/2017 of the Myanmar Investment Commission to check the eligibility of your investment:'
                    }),
                ]
            })
        ];
    }
}