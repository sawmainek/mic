import { FormCheckBox } from './../../../custom-component/dynamic-forms/base/form-checkbox';
import { ToastrService } from 'ngx-toastr';
import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormGroup } from '@angular/forms';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-tax-incentive-form4',
    templateUrl: './tax-incentive-form4.component.html',
    styleUrls: ['./tax-incentive-form4.component.scss']
})
export class TaxIncentiveForm4Component extends TaxIncentiveFormData implements OnInit {
    menu: any = "formData";
    page = "tax-incentive/form4/";
    @Input() id: any;
    mm: any;

    user: any = {};

    formGroup: FormGroup;
    taxModel: any = {};
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    taxIncentiveForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'relief_section',
            formGroup: [
                new FormSectionTitle({
                    label: '5. Please select which of the following tax incentives are being applied for'
                }),
                new FormCheckBox({
                    key: 'applied_75_a',
                    label: 'Exemption or Relief under section 75(a) of the Myanmar Investment Law',
                    value: false
                }),
                new FormCheckBox({
                    key: 'applied_77_a',
                    label: 'Exemption or Relief under section 77(a) of the Myanmar Investment Law',
                    value: false
                }),
                new FormCheckBox({
                    key: 'applied_77_b',
                    label: 'Exemption or Relief under section 77(b) of the Myanmar Investment Law',
                    value: false
                }),
                new FormCheckBox({
                    key: 'applied_77_c',
                    label: 'Exemption or Relief under section 77(c) of the Myanmar Investment Law',
                    value: false
                }),
                new FormCheckBox({
                    key: 'applied_77_d',
                    label: 'Exemption or Relief under section 77(d) of the Myanmar Investment Law',
                    value: false
                }),
                new FormCheckBox({
                    key: 'applied_78_a',
                    label: 'Exemption or Relief under section 78(a) of the Myanmar Investment Law',
                    value: false
                }),
                new FormNote({
                    label: 'When applying for incentives under section 75 (a), 77(a) and 78 (a), please first see Notification No. 84/2017 of the Myanmar Investment Commission to check the eligibility of your investment:'
                }),
            ]
        })
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private formService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxService: TaxIncentiveService,
        public location: Location,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;

        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.taxModel.relief_section = this.taxModel.relief_section || form?.data?.relief_section || {};
                        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm, this.taxModel);

                        this.page = "tax-incentive/form4/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                        this.loaded = true;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));

        if (!this.formGroup.get('relief_section').get('applied_75_a').value &&
            !this.formGroup.get('relief_section').get('applied_77_a').value &&
            !this.formGroup.get('relief_section').get('applied_77_b').value &&
            !this.formGroup.get('relief_section').get('applied_77_c').value &&
            !this.formGroup.get('relief_section').get('applied_77_d').value &&
            !this.formGroup.get('relief_section').get('applied_78_a').value) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.taxModel = { ...this.taxModel, ...this.formGroup.value };

        this.service.create(this.taxModel)
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);
                    this.redirectLink(this.form.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }

    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            const section = this.taxModel.relief_section;
            if (section.applied_75_a == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form5', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form5', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form4', id, 'mm']);
                    }
                }
                return;
            }
            if (section.applied_77_a == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form6', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form6', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form4', id, 'mm']);
                    }
                }
                return;
            }
            if (section.applied_77_b == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form8', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form8', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form4', id, 'mm']);
                    }
                }
                return;
            }
            if (section.applied_77_c == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form9', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form9', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form4', id, 'mm']);
                    }
                }
                return;
            }
            if (section.applied_77_d == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form10', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form10', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form4', id, 'mm']);
                    }
                }
                return;
            }
            if (section.applied_78_a == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form12', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form12', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form4', id, 'mm']);
                    }
                }
                return;
            }
        }
    }
}
