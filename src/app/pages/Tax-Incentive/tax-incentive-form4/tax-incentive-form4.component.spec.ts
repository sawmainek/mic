import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm4Component } from './tax-incentive-form4.component';

describe('TaxIncentiveForm4Component', () => {
  let component: TaxIncentiveForm4Component;
  let fixture: ComponentFixture<TaxIncentiveForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
