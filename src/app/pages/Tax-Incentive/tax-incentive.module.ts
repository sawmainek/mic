import { TaxIncentiveRoutingModule } from './tax-incentive-routing.module';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { GlobalModule } from './../global.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxIncentiveForm1Component } from './tax-incentive-form1/tax-incentive-form1.component';
import { TaxIncentiveForm2Component } from './tax-incentive-form2/tax-incentive-form2.component';
import { TaxIncentiveForm3Component } from './tax-incentive-form3/tax-incentive-form3.component';
import { TaxIncentiveForm4Component } from './tax-incentive-form4/tax-incentive-form4.component';
import { TaxIncentiveForm5Component } from './tax-incentive-form5/tax-incentive-form5.component';
import { TaxIncentiveForm7Component } from './tax-incentive-form7/tax-incentive-form7.component';
import { TaxIncentiveForm6Component } from './tax-incentive-form6/tax-incentive-form6.component';
import { TaxIncentiveForm9Component } from './tax-incentive-form9/tax-incentive-form9.component';
import { TaxIncentiveForm8Component } from './tax-incentive-form8/tax-incentive-form8.component';
import { TaxIncentiveForm10Component } from './tax-incentive-form10/tax-incentive-form10.component';
import { TaxIncentiveForm11Component } from './tax-incentive-form11/tax-incentive-form11.component';
import { TaxIncentiveForm12Component } from './tax-incentive-form12/tax-incentive-form12.component';
import { TaxIncentiveSuccessComponent } from './tax-incentive-success/tax-incentive-success.component';
import { TaxIncentiveSubmissionComponent } from './tax-incentive-submission/tax-incentive-submission.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomComponentModule } from 'src/app/custom-component/custom-component.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { TaxIncentiveSideMenuComponent } from './tax-incentive-side-menu/tax-incentive-side-menu.component';
import { TaxincentiveChooseLanguageComponent } from './taxincentive-choose-language/taxincentive-choose-language.component';
import { TaxIncentiveSidemenuMiniComponent } from './tax-incentive-sidemenu-mini/tax-incentive-sidemenu-mini.component';
import { TaxIncentivePaymentComponent } from './tax-incentive-payment/tax-incentive-payment.component';

@NgModule({
  declarations: [
        TaxIncentiveForm1Component,
        TaxIncentiveForm2Component,
        TaxIncentiveForm3Component,
        TaxIncentiveForm4Component,
        TaxIncentiveForm5Component,
        TaxIncentiveForm6Component,
        TaxIncentiveForm7Component,
        TaxIncentiveForm8Component,
        TaxIncentiveForm9Component,
        TaxIncentiveForm10Component,
        TaxIncentiveForm11Component,
        TaxIncentiveForm12Component,
        TaxIncentiveSubmissionComponent,
        TaxIncentiveSuccessComponent,
        TaxIncentiveSideMenuComponent,
        TaxincentiveChooseLanguageComponent,
        TaxIncentiveSidemenuMiniComponent,
        TaxIncentivePaymentComponent,
    ], imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        GlobalModule,
        TaxIncentiveRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // closeButton: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ],
    exports: [
        TaxIncentiveForm1Component,
        TaxIncentiveForm2Component,
        TaxIncentiveForm3Component,
        TaxIncentiveForm4Component,
        TaxIncentiveForm5Component,
        TaxIncentiveForm6Component,
        TaxIncentiveForm7Component,
        TaxIncentiveForm8Component,
        TaxIncentiveForm9Component,
        TaxIncentiveForm10Component,
        TaxIncentiveForm11Component,
        TaxIncentiveForm12Component,
        TaxIncentiveSubmissionComponent,
        TaxIncentiveSuccessComponent,
        TaxIncentiveSideMenuComponent,
        TaxincentiveChooseLanguageComponent,
        TaxIncentiveSidemenuMiniComponent,
    ],
     providers: [
        TranslateService,
    ],
})
export class TaxIncentiveModule { }
