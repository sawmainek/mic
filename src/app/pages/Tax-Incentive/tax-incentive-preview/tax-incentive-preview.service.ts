import { TaxIncentiveSubmissionForm } from './../tax-incentive-submission/tax-incentive-submission';
import { TaxIncentiveForm12 } from './../tax-incentive-form12/tax-incentive-form12';
import { TaxIncentiveForm11 } from './../tax-incentive-form11/tax-incentive-form11';
import { TaxIncentiveForm10 } from './../tax-incentive-form10/tax-incentive-form10';
import { TaxIncentiveForm9 } from './../tax-incentive-form9/tax-incentive-form9';
import { TaxIncentiveForm8 } from './../tax-incentive-form8/tax-incentive-form8';
import { TaxIncentiveForm7 } from './../tax-incentive-form7/tax-incentive-form7';
import { TaxIncentiveForm6 } from './../tax-incentive-form6/tax-incentive-form6';
import { TaxIncentiveForm5 } from './../tax-incentive-form5/tax-incentive-form5';
import { TaxIncentiveForm4 } from './../tax-incentive-form4/tax-incentive-form4';
import { TaxIncentiveForm3 } from './../tax-incentive-form3/tax-incentive-form3';
import { TaxIncentiveForm2 } from './../tax-incentive-form2/tax-incentive-form2';
import { PrintPDFService } from './../../../custom-component/print.pdf.service';
import { CPCService } from './../../../../services/cpc.service';
import { ISICService } from './../../../../services/isic.service';
import { LocalService } from './../../../../services/local.service';
import { LocationService } from './../../../../services/location.service';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { Injectable } from '@angular/core';

@Injectable()
export class TaxIncentivePreviewService {

    forms: BaseForm<string | boolean>[] = [];
    constructor(
        public lotService: LocationService,
        public localService: LocalService,
        public isicService: ISICService,
        public cpcService: CPCService,
        public printPDFService: PrintPDFService
    ) {

    }

    toFormPDF(data: any, option: {
        readOnly?: boolean,
        language?: string,
        onSubmit$?: any
    }) {
        const taxInvForm2 = new TaxIncentiveForm2(this.lotService);
        const taxInvForm3 = new TaxIncentiveForm3();
        const taxInvForm4 = new TaxIncentiveForm4();
        const taxInvForm5 = new TaxIncentiveForm5(this.lotService);
        const taxInvForm6 = new TaxIncentiveForm6(this.lotService);
        const taxInvForm7 = new TaxIncentiveForm7(this.lotService, this.localService);
        const taxInvForm8 = new TaxIncentiveForm8(this.lotService);
        const taxInvForm9 = new TaxIncentiveForm9(this.lotService);
        const taxInvForm10 = new TaxIncentiveForm10(this.lotService);
        const taxInvForm11 = new TaxIncentiveForm11(this.lotService, this.localService);
        const taxInvForm12 = new TaxIncentiveForm12(this.lotService, this.isicService);
        const taxInvSubmissionForm = new TaxIncentiveSubmissionForm();

        
        this.forms = [
            ...taxInvForm2.forms,
            ...taxInvForm3.forms,
            ...taxInvForm4.forms,
        ]
        const section = data?.relief_section || {};
        if (section?.applied_75_a == true) {
            this.forms = [
                ...this.forms,
                ...taxInvForm5.forms
            ]
        }
        if (section?.applied_77_a == true) {
            this.forms = [
                ...this.forms,
                ...taxInvForm6.forms,
                ...taxInvForm7.forms
            ]
        }
        if (section?.applied_77_b == true) {
            this.forms = [
                ...this.forms,
                ...taxInvForm8.forms
            ]
        }
        if (section?.applied_77_c == true) {
            this.forms = [
                ...this.forms,
                ...taxInvForm9.forms
            ]
        }
        if (section?.applied_77_d == true) {
            this.forms = [
                ...this.forms,
                ...taxInvForm10.forms,
                ...taxInvForm11.forms
            ]
        }
        if (section?.applied_78_a == true) {
            this.forms = [
                ...this.forms,
                ...taxInvForm12.forms
            ]
        }
        this.forms = [
            ...this.forms,
            ...taxInvSubmissionForm.forms
        ]
        this.printPDFService.toFormPDF(this.forms, data, 'Tax Incentive Application Authorization (Form 6)', option);
    }

}