import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormGroup, Validators } from '@angular/forms';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { LocationService } from 'src/services/location.service';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';


@Component({
    selector: 'app-tax-incentive-form2',
    templateUrl: './tax-incentive-form2.component.html',
    styleUrls: ['./tax-incentive-form2.component.scss']
})
export class TaxIncentiveForm2Component extends TaxIncentiveFormData implements OnInit {
    menu: any = "formData";
    page = "tax-incentive/form2/";
    @Input() id: any;
    mm: any;

    user: any = {};

    formGroup: FormGroup;
    taxModel: any = {};
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    taxIncentiveForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'authorized_representative',
            formGroup: [
                new FormSectionTitle({
                    label: '1. Is an authorized representative filling in the application on behalf of the investor(s)?'
                }),
                new FormRadio({
                    key: 'is_authorized',
                    label: 'Yes',
                    value: 'Yes',
                    required: true,
                    columns: 'col-md-3',
                }),
                new FormRadio({
                    key: 'is_authorized',
                    label: 'No, investors are filling in the application by themselves',
                    value: 'No',
                    required: true,
                    columns: 'col-md-9'
                }),
                new FormInput({
                    key: 'name',
                    label: 'a. Name of company, if applicable',
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormTitle({
                    label: 'b. Address of company, if applicable',
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormSelect({
                    key: 'country',
                    label: 'Country',
                    options$: this.lotService.getCountry(),
                    value: 'Myanmar',
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormSelect({
                    key: 'state',
                    label: 'State / Region:',
                    options$: this.lotService.getState(),
                    columns: 'col-md-4',
                    criteriaValue: {
                        key: 'country',
                        value: ['Myanmar'],
                    }
                }),
                new FormSelect({
                    key: 'district',
                    label: 'District:',
                    options$: this.lotService.getDistrict(),
                    columns: 'col-md-4',
                    filter: {
                        parent: 'state',
                        key: 'state'
                    },
                    criteriaValue: {
                        key: 'country',
                        value: ['Myanmar'],
                    }
                }),
                new FormSelect({
                    key: 'township',
                    label: 'Township:',
                    columns: 'col-md-4',
                    options$: this.lotService.getTownship(),
                    filter: {
                        parent: 'district',
                        key: 'district'
                    },
                    criteriaValue: {
                        key: 'country',
                        value: ['Myanmar'],
                    }
                }),
                new FormInput({
                    key: 'address',
                    label: 'Additional address details',
                    required: true,
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormSectionTitle({
                    label: 'c. Contact Person Information',
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'person_name',
                    label: 'Full name',
                    columns: 'col-md-6',
                    required: true,
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'person_position',
                    label: 'Position',
                    columns: 'col-md-6',
                    required: true,
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'person_phone',
                    label: 'Phone',
                    columns: 'col-md-6',
                    required: true,
                    type: 'number',
                    validators: [Validators.minLength(6)],
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'person_email',
                    label: 'Email',
                    columns: 'col-md-6',
                    required: true,
                    validators: [Validators.minLength(6), Validators.email],
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormInput({
                    key: 'person_passport',
                    label: 'Passport number / National Registration Card Number',
                    required: true,
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormSelect({
                    key: 'person_citizen',
                    label: 'Citizenship',
                    options$: this.lotService.getCountry(),
                    required: true,
                    value: 'Myanmar',
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormFile({
                    key: 'official_document',
                    label: 'Please upload an official letter of legal representation:',
                    required: true,
                    criteriaValue: {
                        key: 'is_authorized',
                        value: 'Yes'
                    }
                }),
                new FormSectionTitle({
                    label: '2. Is the tax incentive application being submitted for a Permit or Endorsement investment?'
                }),
                new FormRadio({
                    key: 'is_tax',
                    label: 'Permit',
                    value: 'Permit',
                    columns: 'col-md-3',
                    required: true
                }),
                new FormRadio({
                    key: 'is_tax',
                    label: 'Endorsement',
                    value: 'Endorsement',
                    columns: 'col-md-9',
                    required: true
                })
            ]
        })
    ]

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxService: TaxIncentiveService,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private translateService: TranslateService,
        private lotService: LocationService,
    ) {
        super(formCtlService);
        this.loading = true;

        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;
            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));


                        this.taxModel.authorized_representative = this.taxModel.authorized_representative || form?.data?.authorized_representative || {}
                        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm, this.taxModel);

                        this.page = "tax-incentive/form2/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loading = false;
                        this.loaded = true;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.taxIncentiveForm, this.formGroup, { type_id: this.id }, (formValue) => {
            this.taxModel = { ...this.taxModel, ...formValue };
            this.service.create(this.taxModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }

    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            //For choose Myanmar Language
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/tax-incentive/form3', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/tax-incentive/form3', id]);
                }
                else {
                    this.router.navigate([page + '/tax-incentive/form2', id, 'mm']);
                }
            }
        }

    }
}
