import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm2Component } from './tax-incentive-form2.component';

describe('TaxIncentiveForm2Component', () => {
  let component: TaxIncentiveForm2Component;
  let fixture: ComponentFixture<TaxIncentiveForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
