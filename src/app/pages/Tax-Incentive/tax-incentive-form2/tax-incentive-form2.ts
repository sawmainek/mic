import { Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocationService } from 'src/services/location.service';

export class TaxIncentiveForm2{
    
    public forms: BaseForm<string>[] = []

    constructor(
        private lotService: LocationService,
    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'authorized_representative',
                formGroup: [
                    new FormSectionTitle({
                        label: '1. Is an authorized representative filling in the application on behalf of the investor(s)?'
                    }),
                    new FormRadio({
                        key: 'is_authorized',
                        label: 'Yes',
                        value: 'Yes',
                        required: true,
                        columns: 'col-md-3',
                    }),
                    new FormRadio({
                        key: 'is_authorized',
                        label: 'No, investors are filling in the application by themselves',
                        value: 'No',
                        required: true,
                        columns: 'col-md-9'
                    }),
                    new FormInput({
                        key: 'name',
                        label: 'a. Name of company, if applicable',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormTitle({
                        label: 'b. Address of company, if applicable',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        label: 'Country',
                        options$: this.lotService.getCountry(),
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'state',
                        label: 'State / Region:',
                        options$: this.lotService.getState(),
                        columns: 'col-md-4',
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'district',
                        label: 'District:',
                        options$: this.lotService.getDistrict(),
                        columns: 'col-md-4',
                        filter: {
                            parent: 'state',
                            key: 'state'
                        },
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormSelect({
                        key: 'township',
                        label: 'Township:',
                        columns: 'col-md-4',
                        options$: this.lotService.getTownship(),
                        filter: {
                            parent: 'district',
                            key: 'district'
                        },
                        criteriaValue: {
                            key: 'country',
                            value: ['Myanmar'],
                        }
                    }),
                    new FormInput({
                        key: 'address',
                        label: 'Additional address details',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),  
                    new FormSectionTitle({
                        label: 'c. Contact Person Information',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'person_name',
                        label: 'Full name',
                        columns: 'col-md-6',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'person_position',
                        label: 'Position',
                        columns: 'col-md-6',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'person_phone',
                        label: 'Phone',
                        columns: 'col-md-6',
                        required: true,
                        type: 'number',
                        validators: [Validators.minLength(6)],
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'person_email',
                        label: 'Email',
                        columns: 'col-md-6',
                        required: true,
                        validators: [Validators.minLength(6), Validators.email],
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormInput({
                        key: 'person_passport',
                        label: 'Passport number / National Registration Card Number',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSelect({
                        key: 'person_citizen',
                        label: 'Citizenship',
                        options$: this.lotService.getCountry(),
                        required: true,
                        value: 'Myanmar',
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormFile({
                        key: 'official_document',
                        label: 'Please upload an official letter of legal representation:',
                        required: true,
                        criteriaValue: {
                            key: 'is_authorized',
                            value: 'Yes'
                        }
                    }),
                    new FormSectionTitle({
                        label: '2. Is the tax incentive application being submitted for a Permit or Endorsement investment?'
                    }),
                    new FormRadio({
                        key: 'is_tax',
                        label: 'Permit',
                        value: 'Permit',
                        columns: 'col-md-3',
                        required: true
                    }),
                    new FormRadio({
                        key: 'is_tax',
                        label: 'Endorsement',
                        value: 'Endorsement',
                        columns: 'col-md-9',
                        required: true
                    })
                ]
            })
        ];
    }
}