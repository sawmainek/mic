import { ActivatedRoute } from '@angular/router';
import { TaxIncentivePaymentService } from './../../../../services/tax-incentive-payment.service';
import { mergeMap, concatMap, delay, tap, finalize } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { TaxincentiveSubmissionService } from 'src/services/taxincentive-submission.service';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';

@Component({
    selector: 'app-tax-incentive-payment',
    templateUrl: './tax-incentive-payment.component.html',
    styleUrls: ['./tax-incentive-payment.component.scss']
})
export class TaxIncentivePaymentComponent implements OnInit {

    id: any;
    invoice: any;
    user: any = {};
    submission: any = {};
    total: number = 0;
    credit: boolean = false;
    mpu: boolean = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private taxSubmissionService: TaxincentiveSubmissionService,
        private taxInvPaymentService: TaxIncentivePaymentService,
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                    this.getPayment();
                }
            });
    }

    getPayment() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            if(this.id) {
                this.taxInvPaymentService.show(this.id).subscribe(invoice => {
                    this.invoice = invoice;
                    this.total = Number(invoice.amount);
                })
            }
        }).unsubscribe();
    }

    get checkPaymentNeed() {
        return this.invoice.status == 'Unpaid' ? true : false;
    }

    saveCredit() {
        this.credit = true;
        this.taxInvPaymentService.update(this.id, { status: 'Paid' }).subscribe(res => {
            this.credit = false;
            window.location.href = environment.host + "/payments/mpu?request_id="+this.invoice.id+"&user_id=" + this.user.id + "&amount=" + this.total + "&success_redirect_url=" + window.location.hostname+"&fail_redirect_url="+window.location.hostname
        });
    }

    saveMPU() {
        this.mpu = true;
        this.taxInvPaymentService.update(this.id, { status: 'Paid' }).subscribe(res => {
            this.mpu = false;
            window.location.href = environment.host + "/payments/mpu?request_id=" + this.invoice.id + "&user_id=" + this.user.id + "&amount=" + this.total + "&success_redirect_url=" + window.location.hostname + "&fail_redirect_url=" + window.location.hostname
        });
       
    }
}

