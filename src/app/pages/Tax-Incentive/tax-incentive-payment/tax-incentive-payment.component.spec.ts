import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentivePaymentComponent } from './tax-incentive-payment.component';

describe('TaxIncentivePaymentComponent', () => {
  let component: TaxIncentivePaymentComponent;
  let fixture: ComponentFixture<TaxIncentivePaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentivePaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentivePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
