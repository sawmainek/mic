import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';

export abstract class TaxIncentiveProgress {
    static type = "Tax Incentive";
    private _progressForms: any;

    get progressForms(): any {
        return this._progressForms;
    }

    getProgressForms(data) {
        this._progressForms = data ? data : [];
    }

}