import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxincentiveChooseLanguageComponent } from './taxincentive-choose-language.component';

describe('TaxincentiveChooseLanguageComponent', () => {
  let component: TaxincentiveChooseLanguageComponent;
  let fixture: ComponentFixture<TaxincentiveChooseLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxincentiveChooseLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxincentiveChooseLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
