import { FormProgress } from './../../../core/form/_actions/form.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../../../core/reducers/index';
import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TaxIncentive } from 'src/app/models/tax_incentive';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { TaxIncentiveLanguage } from './taxincentive-choose-language';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { TaxIncentiveFormService } from './../../../../services/tax-incentive-form.service';
import { FormProgressService } from 'src/services/form-progress.service';
import { forkJoin } from 'rxjs';
import { ToastrService } from 'ngx-toastr';



@Component({
    selector: 'app-taxincentive-choose-language',
    templateUrl: './taxincentive-choose-language.component.html',
    styleUrls: ['./taxincentive-choose-language.component.scss']
})
export class TaxincentiveChooseLanguageComponent extends TaxIncentiveLanguage implements OnInit {
    @Input() id: any;
    menu: any = "choose-language";
    page = "Choose Language";
    language: any = "Myanmar";

    user: any = {};
    taxModel: TaxIncentive;
    taxMMModel: TaxIncentive;
    form: any = {};

    loading = true;
    spinner: boolean = false;
    is_draft = false;

    constructor(
        private authService: AuthService,
        private router: Router,
        private store: Store<AppState>,
        private taxFormService: TaxIncentiveFormService, //For New Flow
        private TaxService: TaxIncentiveService,
        private TaxMMService: TaxIncentiveMMService,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private activatedRoute: ActivatedRoute,
        private toast: ToastrService
    ) {
        super(formCtlService);
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;

            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                this.loading = this.user ? true : false;
    
                if (this.user) {
                    this.getFormData();
                }
            });
        }).unsubscribe();      
    }

    getFormData() {
        if (this.id) {
            this.taxFormService.show(this.id)
                .subscribe(result => {
                    this.form = result || {};
                    this.language = this.form?.language || 'Myanmar'
                    this.openLanguage(this.language || 'Myanmar');
                    this.taxModel = this.form?.data || null;
                    this.taxMMModel = this.form?.data_mm || null;

                    this.loading = false;
                })
        } else {
            this.loading = false;
        }
    }

    checkSubmission(tax_form, submission) {
        var found = false;
        tax_form.forEach((x, index) => {
            if (submission.filter(y => y.data.form_id == x.id).length <= 0) {
                found = true;
                this.form = tax_form.length > 0 ? tax_form[index] : {};
                return;
            }
        });
        if (!found) {
            this.form = {};
            return;
        }
    }

    openLanguage(language) {
        if (window.location.href.indexOf('officer') == -1) {
            this.language = language;
        }
    }

    goToHome() {
        this.router.navigate(['/']);
    }

    getTaxProgressData(form_id, model_id) {
        this.formProgressService.get({
            page: 1,
            rows: 100,
            search: `form_id:equal:${form_id || 0}|type:equal:Tax Incentive` // For New Flow this.id => this.micModel.id
        }).subscribe((progress: any) => {
            this.getProgressForms(progress || [])
            this.store.dispatch(new FormProgress({progress}));
            this.saveFormProgress(this.form?.id, model_id, this.page);
        });
    }

    redirectLink(id: number) {
        if (id == null) {
            this.toast.error("Please login with investor account.");
            return;
        }
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form.language == 'Myanmar') {
                this.router.navigate(['/officer/tax-incentive/form1', id, 'mm']);
            }
            else {
                this.router.navigate(['/officer/tax-incentive/form1', id]);
            }

        } else {
            if (this.form.language == 'Myanmar') {
                this.router.navigate(['/pages/tax-incentive/form1', id, 'mm']);
            }
            else {
                this.router.navigate(['/pages/tax-incentive/form1', id]);
            }
        }
    }

    onSubmit() {
        this.spinner = true;
        //For New Flow
        this.form.user_id = this.form.user_id || this.user.id;
        this.form.type = 'Tax Incentive';
        this.form.language = this.language || 'Myanmar';
        this.form.status = this.form.status || 'Draft';
        this.taxFormService.create(this.form)
            .subscribe(result => {
                this.form = result;
                if (this.language == 'Myanmar') {
                    this.taxMMModel = {
                        ...this.taxMMModel || {},
                        ...{ user_id: result.user_id, form_id: this.form.id, state: result.state, choose_language: result.language, apply_to: result.apply_to } || {}
                    } as TaxIncentive;

                    this.TaxMMService.create(this.taxMMModel).subscribe(result => {
                        this.form.data_mm = result;

                        this.page = `tax-incentive/choose-language/${result.id}/mm`;
                        this.getTaxProgressData(this.form.id, result.id);

                        this.redirectLink(this.form.id);
                    })
                } else {
                    // For both English / Myanmar
                    this.taxModel = {
                        ...this.taxModel || {},
                        ...{ user_id: result.user_id, form_id: this.form.id, state: result.state, choose_language: result.language, apply_to: result.apply_to } || {}
                    } as TaxIncentive;

                    this.taxMMModel = {
                        ...this.taxMMModel || {},
                        ...{ user_id: result.user_id, form_id: this.form.id, state: result.state, choose_language: result.language, apply_to: result.apply_to } || {}
                    } as TaxIncentive;

                    forkJoin([
                        this.TaxService.create(this.taxModel),
                        this.TaxMMService.create(this.taxMMModel)
                    ])
                        .subscribe(results => {
                            this.page = `tax-incentive/choose-language/${results[0].id}`;
                            this.getTaxProgressData(this.form.id, results[0].id);

                            this.page = `tax-incentive/choose-language/${results[1].id}/mm`;
                            this.getTaxProgressData(this.form.id, results[1].id);

                            this.redirectLink(this.form.id);
                        })
                }
            })
    }


}
