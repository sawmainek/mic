import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { ISICService } from 'src/services/isic.service';
import { LocationService } from 'src/services/location.service';

export class TaxIncentiveForm12 {

    public forms: BaseForm<string>[] = [];

    constructor(
        private lotService: LocationService,
        private isicService: ISICService,
    ) {
        let businessSector: BaseForm < any > [] =[
            new FormSelect({
                key: 'isic_section_id',
                label: 'Choose business sections ( ISIC Section )',
                options$: this.isicService.getSection(),
                required: true,
                multiple: true,
            }),
            new FormSelect({
                key: 'isic_division_id',
                label: 'Choose business divisions ( ISIC Division )',
                options$: this.isicService.getDivison(),
                filter: {
                    parent: 'isic_section_id',
                    key: 'section'
                },
                required: true,
                multiple: true,
            }),
            new FormSelect({
                key: 'isic_group_id',
                label: 'Choose business groups ( ISIC Group )',
                options$: this.isicService.getGroup(),
                filter: {
                    parent: 'isic_division_id',
                    key: 'division'
                },
                required: true,
                multiple: true,
            }),
            new FormSelect({
                key: 'isic_class_ids',
                label: 'Choose business class(es)',
                required: true,
                multiple: true,
                options$: this.isicService.getClasses(),
                filter: {
                    parent: 'isic_group_id',
                    key: 'group'
                },
            })
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'section_78_a',
                formGroup: [
                    new FormSectionTitle({
                        label: '11. Exemption or Relief under section 78(a) of the Myanmar Investment Law'
                    }),
                    new FormInput({
                        key: 'earned_year',
                        label: 'a. Financial year in which reinvested profits were earned:',
                        type: 'number',
                        required: true,
                    }),
                    new FormInput({
                        key: 'reinvested_year',
                        label: 'b. Financial year in which profits were reinvested:',
                        type: 'number',
                        required: true,
                    }),
                    new FormTitle({
                        label: 'c. Value of reinvestment:'
                    }),
                    // new FormSelect({
                    //     key: 'currency',
                    //     label: 'Select currency',
                    //     options$: this.lotService.getCurrency(),
                    //     required: true
                    // }),
                    new FormInput({
                        key: 'kyat',
                        label: 'Equivalent Kyat',
                        required: true,
                        columns: 'col-md-6',
                        type: 'number'
                    }),
                    new FormInput({
                        key: 'usd',
                        label: 'Equivalent USD',
                        required: true,
                        columns: 'col-md-6',
                        type: 'number'
                    }),
                    new FormSectionTitle({
                        label: 'd. Please state the sector in which this amount is reinvested:'
                    }),
                    new BaseFormGroup({
                        key: 'business_sector',
                        formGroup: businessSector
                    }),
                    new FormTextArea({
                        key: 'description',
                        label: 'e. Description of the reinvestment',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Board of Directors\' resolution (full quorum)'
                    }),
                    new FormFile({
                        key: 'resolution_document',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Audit report'
                    }),
                    new FormFile({
                        key: 'audit_document',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Self-assessment system (SAS) tax form of Internal Revenue Department of the investment enterprise'
                    }),
                    new FormFile({
                        key: 'sas_document',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Tax payment receipt of Myanmar Economic Bank of the investment enterprise'
                    }),
                    new FormFile({
                        key: 'receipt_document',
                        label: 'File Name',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Recommendation/Memo from respective Ministry (if investment implemented with any contractual agreement with the government)'
                    }),
                    new FormFile({
                        key: 'recommendation_document',
                        label: 'File Name',
                        required: true
                    }),
                    new FormNote({
                        label: 'Please review Myanmar Investment Rules 99, 113 and 114 for more information on section 78(a) of the Myanmar Investment Law.'
                    })
                ]
            })
        ];
    }

}