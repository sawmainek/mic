import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm12Component } from './tax-incentive-form12.component';

describe('TaxIncentiveForm12Component', () => {
  let component: TaxIncentiveForm12Component;
  let fixture: ComponentFixture<TaxIncentiveForm12Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm12Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
