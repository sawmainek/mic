import { LocationService } from './../../../../services/location.service';
import { ISICService } from './../../../../services/isic.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormGroup } from '@angular/forms';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTextArea } from 'src/app/custom-component/dynamic-forms/base/form-textarea';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { HttpClient } from '@angular/common/http';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { of, Observable } from 'rxjs';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentive } from 'src/app/models/tax_incentive';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-tax-incentive-form12',
    templateUrl: './tax-incentive-form12.component.html',
    styleUrls: ['./tax-incentive-form12.component.scss']
})
export class TaxIncentiveForm12Component extends TaxIncentiveFormData implements OnInit {
    menu: any = "formData";
    page = "tax-incentive/form12/";
    @Input() id: any;
    mm: any;

    user: any = {};

    formGroup: FormGroup;
    taxModel: any = {};
    form: any;
    service: any;

    business_sectors: any;

    spinner = false;
    is_draft = false;
    is_save = false;
    is_last = false;
    loading = false;

    isRevision: boolean = false;

    businessSector: BaseForm<any>[] = [
        new FormSelect({
            key: 'isic_section_id',
            label: 'Choose business sections ( ISIC Section )',
            options$: this.isicService.getSection(),
            required: true,
            multiple: true,
        }),
        new FormSelect({
            key: 'isic_division_id',
            label: 'Choose business divisions ( ISIC Division )',
            options$: this.isicService.getDivison(),
            filter: {
                parent: 'isic_section_id',
                key: 'section'
            },
            required: true,
            multiple: true,
        }),
        new FormSelect({
            key: 'isic_group_id',
            label: 'Choose business groups ( ISIC Group )',
            options$: this.isicService.getGroup(),
            filter: {
                parent: 'isic_division_id',
                key: 'division'
            },
            required: true,
            multiple: true,
        }),
        new FormSelect({
            key: 'isic_class_ids',
            label: 'Choose business class(es)',
            required: true,
            multiple: true,
            options$: this.isicService.getClasses(),
            filter: {
                parent: 'isic_group_id',
                key: 'group'
            },
        })
    ];

    taxIncentiveForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'section_78_a',
            formGroup: [
                new FormSectionTitle({
                    label: '11. Exemption or Relief under section 78(a) of the Myanmar Investment Law'
                }),
                new FormInput({
                    key: 'earned_year',
                    label: 'a. Financial year in which reinvested profits were earned:',
                    type: 'number',
                    required: true,
                }),
                new FormInput({
                    key: 'reinvested_year',
                    label: 'b. Financial year in which profits were reinvested:',
                    type: 'number',
                    required: true,
                }),
                new FormTitle({
                    label: 'c. Value of reinvestment:'
                }),
                // new FormSelect({
                //     key: 'currency',
                //     label: 'Select currency',
                //     options$: this.lotService.getCurrency(),
                //     required: true
                // }),
                new FormInput({
                    key: 'kyat',
                    label: 'Equivalent Kyat',
                    required: true,
                    columns: 'col-md-6',
                    type: 'number'
                }),
                new FormInput({
                    key: 'usd',
                    label: 'Equivalent USD',
                    required: true,
                    columns: 'col-md-6',
                    type: 'number'
                }),
                new FormSectionTitle({
                    label: 'd. Please state the sector in which this amount is reinvested:'
                }),
                new BaseFormGroup({
                    key: 'business_sector',
                    formGroup: this.businessSector
                }),
                new FormTextArea({
                    key: 'description',
                    label: 'e. Description of the reinvestment',
                    required: true
                }),
                new FormTitle({
                    label: 'Board of Directors\' resolution (full quorum)'
                }),
                new FormFile({
                    key: 'resolution_document',
                    label: 'File Name',
                    required: true
                }),
                new FormTitle({
                    label: 'Audit report'
                }),
                new FormFile({
                    key: 'audit_document',
                    label: 'File Name',
                    required: true
                }),
                new FormTitle({
                    label: 'Self-assessment system (SAS) tax form of Internal Revenue Department of the investment enterprise'
                }),
                new FormFile({
                    key: 'sas_document',
                    label: 'File Name',
                    required: true
                }),
                new FormTitle({
                    label: 'Tax payment receipt of Myanmar Economic Bank of the investment enterprise'
                }),
                new FormFile({
                    key: 'receipt_document',
                    label: 'File Name',
                    required: true
                }),
                new FormTitle({
                    label: 'Recommendation/Memo from respective Ministry (if investment implemented with any contractual agreement with the government)'
                }),
                new FormFile({
                    key: 'recommendation_document',
                    label: 'File Name',
                    required: true
                }),
                new FormNote({
                    label: 'Please review Myanmar Investment Rules 99, 113 and 114 for more information on section 78(a) of the Myanmar Investment Law.'
                })
            ]
        })
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private formService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxService: TaxIncentiveService,
        private isicService: ISICService,
        private lotService: LocationService,
        public location: Location,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        super(formCtlService);
        this.loading = true;

        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.taxModel.section_78_a = this.taxModel?.section_78_a || form?.data?.section_78_a || {};

                        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm, this.taxModel);

                        this.page = "tax-incentive/form12/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var isic_class_ids = this.businessSector.filter(x => x.key == "isic_class_ids")[0];
        isic_class_ids.options$ = this.isicService.getClasses(lan);

        this.authService.changeLanguage$.subscribe(x => {
            isic_class_ids.options$ = this.isicService.getClasses(x);
        })
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.taxIncentiveForm, this.formGroup, { type_id: this.id }, (formValue) => {
            this.taxModel = { ...this.taxModel, ...formValue };
            console.log(this.taxModel)
            this.service.create(this.taxModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);
                        this.redirectLink(this.form.id)
                    } else {
                        this.toast.success('Saved successfully.');
                    }


                });
        });
    }
    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            //For choose Myanmar Language
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/tax-incentive/submission', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/tax-incentive/submission', id]);
                }
                else {
                    this.router.navigate([page + '/tax-incentive/form12', id, 'mm']);
                }
            }
        }

    }
}
