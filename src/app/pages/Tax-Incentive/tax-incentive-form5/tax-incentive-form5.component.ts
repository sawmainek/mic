import { FormDummy } from './../../../custom-component/dynamic-forms/base/form-dummy';
import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { Observable } from 'rxjs';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { HttpClient } from '@angular/common/http';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { TaxIncentive } from 'src/app/models/tax_incentive';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-tax-incentive-form5',
    templateUrl: './tax-incentive-form5.component.html',
    styleUrls: ['./tax-incentive-form5.component.scss']
})
export class TaxIncentiveForm5Component extends TaxIncentiveFormData implements OnInit {

    @Input() id: any;
    menu: any = "formData";
    page = "tax-incentive/form5/";
    mm: any = null;

    user: any = {};

    service: any;
    form: any;
    taxModel: TaxIncentive;
    formGroup: FormGroup;
    count: number;
    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    totalValue: any[] = [
        { 'total_percent': 0, 'total_kyat': 0 },
        { 'total_percent': 0, 'total_kyat': 0 },
        { 'total_percent': 0, 'total_kyat': 0 }
    ]

    zone1: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Zone 1'
        }),
        new BaseFormArray({
            key: 'zones',
            formArray: [
                new FormDummy({
                    key: 'zone',
                    value: '1'
                }),
                new FormSelect({
                    key: 'state',
                    options$: this.lotService.getState(),
                    filter: {
                        parent: 'zone',
                        key: 'zone'
                    }
                }),
                new FormSelect({
                    key: 'district',
                    options$: this.lotService.getDistrict(),
                    filter: {
                        parent: 'state',
                        key: 'state'
                    }
                }),
                new FormSelect({
                    key: 'township',
                    options$: this.lotService.getTownship(),
                    filter: {
                        parent: 'district',
                        key: 'district'
                    }
                }),
                new FormInput({
                    key: 'percent',
                    type: 'number',
                    validators: [Validators.min(0), Validators.max(100)],
                    value: '0',
                    style: { 'min-width': '150px' },
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueZone1(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    style: { 'min-width': '150px' },
                    readonly: true,
                    // valueChangeEvent: (value, index, form, formGroup) => {
                    //     this.updateFormValueZone1(value, index, form, formGroup);
                    // }
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue();
            },
            tableHeader: [
                [
                    { label: 'State / Region', style: { 'text-align': 'center' } },
                    { label: 'District', style: { 'text-align': 'center' } },
                    { label: 'Township', style: { 'text-align': 'center' } },
                    { label: 'Percent of total expected investment value', style: { 'text-align': 'center', 'min-width': '150px' } },
                    { label: 'Expected Value (Kyat)', style: { 'text-align': 'center', 'min-width': '150px' } },
                ]
            ],
            tableFooter: [
                [
                    { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_percent;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_kyat;
                        }
                    },
                ]
            ]
        }),

        new FormHidden({
            key: 'total_percent'
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
    ];

    zone2: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Zone 2'
        }),
        new BaseFormArray({
            key: 'zones',
            formArray: [
                new FormDummy({
                    key: 'zone',
                    value: '2'
                }),
                new FormSelect({
                    key: 'state',
                    options$: this.lotService.getState(),
                    filter: {
                        parent: 'zone',
                        key: 'zone'
                    }
                }),
                new FormSelect({
                    key: 'district',
                    options$: this.lotService.getDistrict(),
                    filter: {
                        parent: 'state',
                        key: 'state'
                    }
                }),
                new FormSelect({
                    key: 'township',
                    options$: this.lotService.getTownship(),
                    filter: {
                        parent: 'district',
                        key: 'district'
                    }
                }),
                new FormInput({
                    key: 'percent',
                    type: 'number',
                    value: '0',
                    validators: [Validators.min(0), Validators.max(100)],
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueZone2(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    readonly: true,
                    // valueChangeEvent: (value, index, form, formGroup) => {
                    //     this.updateFormValueZone2(value, index, form, formGroup);
                    // }
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue();
            },
            tableHeader: [
                [
                    { label: 'State / Region', style: { 'text-align': 'center' } },
                    { label: 'District', style: { 'text-align': 'center' } },
                    { label: 'Township', style: { 'text-align': 'center' } },
                    { label: 'Percent of total expected investment value', style: { 'text-align': 'center', 'min-width': '150px' } },
                    { label: 'Expected Value (Kyat)', style: { 'text-align': 'center', 'min-width': '150px' } },
                ]
            ],
            tableFooter: [
                [
                    { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_percent;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_kyat;
                        }
                    },
                ]
            ]
        }),

        new FormHidden({
            key: 'total_percent'
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
    ];

    zone3: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Zone 3'
        }),
        new BaseFormArray({
            key: 'zones',
            formArray: [
                new FormDummy({
                    key: 'zone',
                    value: '3'
                }),
                new FormSelect({
                    key: 'state',
                    options$: this.lotService.getState(),
                    filter: {
                        parent: 'zone',
                        key: 'zone'
                    }
                }),
                new FormSelect({
                    key: 'district',
                    options$: this.lotService.getDistrict(),
                    filter: {
                        parent: 'state',
                        key: 'state'
                    }
                }),
                new FormSelect({
                    key: 'township',
                    options$: this.lotService.getTownship(),
                    filter: {
                        parent: 'district',
                        key: 'district'
                    }
                }),
                new FormInput({
                    key: 'percent',
                    type: 'number',
                    validators: [Validators.min(0), Validators.max(100)],
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValueZone3(value, index, form, formGroup);
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    readonly: true,
                    // valueChangeEvent: (value, index, form, formGroup) => {
                    //     this.updateFormValueZone3(value, index, form, formGroup);
                    // }
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue();
            },
            tableHeader: [
                [
                    { label: 'State / Region', style: { 'text-align': 'center' } },
                    { label: 'District', style: { 'text-align': 'center' } },
                    { label: 'Township', style: { 'text-align': 'center' } },
                    { label: 'Percent of total expected investment value', style: { 'text-align': 'center', 'min-width': '150px' } },
                    { label: 'Expected Value (Kyat)', style: { 'text-align': 'center', 'min-width': '150px' } },
                ]
            ],
            tableFooter: [
                [
                    { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 },
                    {
                        cellFn: () => {
                            return this.totalValue[2].total_percent;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[2].total_kyat;
                        }
                    },
                ]
            ]
        }),

        new FormHidden({
            key: 'total_percent'
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
    ];

    taxIncentiveForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'section_75_a',
            formGroup: [
                new FormSectionTitle({
                    label: '6. Exemption or Relief under section 75(a) of the Myanmar Investment Law'
                }),
                new FormNote({
                    label: 'Locations by Zone were announced in Notification No. 10 / 2017 of the Myanmar Investment Commission. Please review Myanmar Investment Rules 83, 96, 103, 113, and 114 for more information on section 75(a) of the Myanmar Investment Law.'
                }),
                new FormTitle({
                    label: 'a. Total expected investment value, as indicated in the permit/endorsement or in application :'
                }),
                // new FormSelect({
                //     key: 'currency',
                //     label: 'Select currency',
                //     columns: 'col-md-4',
                //     options$: this.lotService.getCurrency(),
                //     required: true
                // }), 
                new FormInput({
                    key: 'kyat',
                    label: 'Equivalent Kyat',
                    type: 'number',
                    columns: 'col-md-6',
                    required: true,
                }),
                new FormInput({
                    key: 'usd',
                    label: 'Equivalent USD',
                    type: 'number',
                    columns: 'col-md-6',
                    required: true
                }),
                new FormTitle({
                    label: 'b. Please indicate the expected investment value in each Zone:'
                }),
                new BaseFormGroup({
                    key: 'zone_1',
                    formGroup: this.zone1
                }),
                new BaseFormGroup({
                    key: 'zone_2',
                    formGroup: this.zone2
                }),
                new BaseFormGroup({
                    key: 'zone_3',
                    formGroup: this.zone3
                }),
                new FormFile({
                    key: 'estimate_document',
                    label: 'Please upload the calculation for the estimation here:',
                    required: true
                }),
            ]
        })
    ];

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        public location: Location,
        private store: Store<AppState>,
        private toast: ToastrService,
        private lotService: LocationService,
        private formService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxService: TaxIncentiveService,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,

    ) {
        super(formCtlService);
        this.loading = true;

        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
        this.getCurrentUser();

        console.log(this.zone1);

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

    }

    ngOnInit(): void { }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.taxModel.section_75_a = this.taxModel?.section_75_a || form?.data?.section_75_a || {};
                        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm, this.taxModel);
                        this.calculateTotalValue();

                        this.page = "tax-incentive/form5/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });

                        this.formGroup.get('section_75_a').get('kyat').valueChanges.subscribe(val => {
                            this.updateEquKyatValue(val);
                        });

                        this.changeLanguage();
                        this.changeCboData();
                        this.loading = false;
                        this.loaded = true;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    changeCboData() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';

        var state1 = this.zone1[1].formArray.filter(x => x.key == "state")[0];
        state1.options$ = this.lotService.getState(lan);

        var district1 = this.zone1[1].formArray.filter(x => x.key == "district")[0];
        district1.options$ = this.lotService.getDistrict(lan);

        var township1 = this.zone1[1].formArray.filter(x => x.key == "township")[0];
        township1.options$ = this.lotService.getTownship(lan);

        var state2 = this.zone2[1].formArray.filter(x => x.key == "state")[0];
        state2.options$ = this.lotService.getState(lan);

        var district2 = this.zone2[1].formArray.filter(x => x.key == "district")[0];
        district2.options$ = this.lotService.getDistrict(lan);

        var township2 = this.zone2[1].formArray.filter(x => x.key == "township")[0];
        township2.options$ = this.lotService.getTownship(lan);

        var state3 = this.zone3[1].formArray.filter(x => x.key == "state")[0];
        state3.options$ = this.lotService.getState(lan);

        var district3 = this.zone3[1].formArray.filter(x => x.key == "district")[0];
        district3.options$ = this.lotService.getDistrict(lan);

        var township3 = this.zone3[1].formArray.filter(x => x.key == "township")[0];
        township3.options$ = this.lotService.getTownship(lan);

        this.authService.changeLanguage$.subscribe(x => {
            state1.options$ = this.lotService.getState(x);
            district1.options$ = this.lotService.getDistrict(x);
            township1.options$ = this.lotService.getTownship(x);
            state2.options$ = this.lotService.getState(x);
            district2.options$ = this.lotService.getDistrict(x);
            township2.options$ = this.lotService.getTownship(x);
            state3.options$ = this.lotService.getState(x);
            district3.options$ = this.lotService.getDistrict(x);
            township3.options$ = this.lotService.getTownship(x);
        })
    }


    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    calculateTotalValue() {
        let formValue = this.formGroup.get('section_75_a').value;
        for (let i = 0; i < this.totalValue.length; i++) {
            this.totalValue[i].total_percent = 0;
            this.totalValue[i].total_kyat = 0;

            formValue['zone_' + (i + 1)]['zones'].map((x, j) => {
                const equ_kyat = formValue['kyat'] || 0;
                // const kyat = (Number(x.percent) / 100) * Number(equ_kyat);
                const kyat = Number(x.percent) * Number(equ_kyat);
                (this.formGroup.get('section_75_a').get('zone_' + (i + 1)).get('zones') as FormArray).
                    controls[j].get('kyat').setValue(kyat);


                this.totalValue[i].total_percent += Number(x.percent);
                this.totalValue[i].total_kyat += Number(kyat);
                this.formGroup.get('section_75_a').get('zone_' + (i + 1)).get('total_percent').setValue(this.totalValue[i].total_percent);
                this.formGroup.get('section_75_a').get('zone_' + (i + 1)).get('total_kyat').setValue(this.totalValue[i].total_kyat);
            });
        }
    }

    updateEquKyatValue(value) {
        let formValue = this.formGroup.get('section_75_a').value;
        formValue['kyat'] = value;
        this.calculateTotalValue();
    }

    updateFormValueZone1(value, index, form, formGroup) {
        let formValue = this.formGroup.get('section_75_a').value;
        formValue['zone_1']['zones'][index][form.key] = value;

        this.calculateTotalValue();
    }

    updateFormValueZone2(value, index, form, formGroup) {
        let formValue = this.formGroup.get('section_75_a').value;
        formValue['zone_2']['zones'][index][form.key] = value;
        this.calculateTotalValue();
    }

    updateFormValueZone3(value, index, form, formGroup) {
        let formValue = this.formGroup.get('section_75_a').value;
        formValue['zone_3']['zones'][index][form.key] = value;
        this.calculateTotalValue();
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }

        // let total_percent = 0;
        // for (let i = 0; i < this.totalValue.length; i++) {
        //     total_percent += this.totalValue[i].total_percent;
        // }
        // if (total_percent != 100) {
        //     this.toast.error('Your total expected investment must be 100%.');
        //     return;
        // }

        this.is_draft = false;
        this.saveFormData();
    }
    saveFormData() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.taxIncentiveForm, this.formGroup, { type_id: this.id }, (formValue) => {
            this.taxModel = { ...this.taxModel, ...formValue };
            this.service.create(this.taxModel)
                .subscribe(x => {
                    this.spinner = false;
                    if (!this.is_draft) {
                        this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);
                        this.redirectLink(this.form.id);
                    } else {
                        this.toast.success('Saved successfully.');
                    }
                });
        });
    }
    redirectLink(id: number) {

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            // TODO: check for multiple route
            var section = this.taxModel.relief_section;
            if (section.applied_77_a == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form6', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form6', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form5', id, 'mm']);
                    }
                }
                return;
            }
            else if (section.applied_77_b == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form8', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form8', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form5', id, 'mm']);
                    }
                }
                return;
            }
            else if (section.applied_77_c == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form9', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form9', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form5', id, 'mm']);
                    }
                }
                return;
            }
            else if (section.applied_77_d == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form10', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form10', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form5', id, 'mm']);
                    }
                }
                return;
            }
            else if (section.applied_78_a == true) {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/form12', id, 'mm']);
                } else {
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/form12', id]);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form5', id, 'mm']);
                    }
                }
                return;
            } else {
                if (this.form.language == 'Myanmar') {
                    this.router.navigate([page + '/tax-incentive/submission', id, 'mm']);
                } else {
                    //For choose English/Myanmar Language
                    if (this.mm) {
                        this.router.navigate([page + '/tax-incentive/submission', id, 'mm']);
                    }
                    else {
                        this.router.navigate([page + '/tax-incentive/form5', id, 'mm']);
                    }
                }
            }
        }
    }
}
