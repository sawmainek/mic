import { FormArray, FormGroup, Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormNote } from 'src/app/custom-component/dynamic-forms/base/form-note';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormFile } from 'src/app/custom-component/dynamic-forms/base/form.file';
import { LocationService } from 'src/services/location.service';

export class TaxIncentiveForm5 {

    public forms: BaseForm<string>[] = [];
    formGroup: FormGroup;

    totalValue: any[] = [
        { 'total_percent': 0, 'total_kyat': 0 },
        { 'total_percent': 0, 'total_kyat': 0 },
        { 'total_percent': 0, 'total_kyat': 0 }
    ];

    constructor(
        private lotService: LocationService,
    ) {
        let zone1: BaseForm < any > [] =[
            new FormSectionTitle({
                'label': 'Zone 1'
            }),
            new BaseFormArray({
                key: 'zones',
                formArray: [
                    new FormDummy({
                        key: 'zone',
                        value: '1'
                    }),
                    new FormSelect({
                        key: 'state',
                        options$: this.lotService.getState(),
                        filter: {
                            parent: 'zone',
                            key: 'zone'
                        }
                    }),
                    new FormSelect({
                        key: 'district',
                        options$: this.lotService.getDistrict(),
                        filter: {
                            parent: 'state',
                            key: 'state'
                        }
                    }),
                    new FormSelect({
                        key: 'township',
                        options$: this.lotService.getTownship(),
                        filter: {
                            parent: 'district',
                            key: 'district'
                        }
                    }),
                    new FormInput({
                        key: 'percent',
                        type: 'number',
                        validators: [Validators.min(0), Validators.max(100)],
                        value: '0',
                        style: { 'min-width': '150px' },
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValueZone1(value, index, form, formGroup);
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        style: { 'min-width': '150px' },
                        readonly: true,
                        // valueChangeEvent: (value, index, form, formGroup) => {
                        //     this.updateFormValueZone1(value, index, form, formGroup);
                        // }
                    }),
                ],
                useTable: true,
                headerWidths: ['15%', '15%', '15%', 'auto', '*'],
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue();
                },
                tableHeader: [
                    [
                        { label: 'State / Region', style: { 'text-align': 'center' } },
                        { label: 'District', style: { 'text-align': 'center' } },
                        { label: 'Township', style: { 'text-align': 'center' } },
                        { label: 'Percent of total expected investment value', style: { 'text-align': 'center', 'min-width': '150px' } },
                        { label: 'Expected Value (Kyat)', style: { 'text-align': 'center', 'min-width': '150px' } },
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 }, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['percent'] || 0);
                                });
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                });
                                return total;
                            }
                        },
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Percentage (%) of Expected Investment Value (Zone 1)', colSpan: 3 },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_percent;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_kyat;
                            }
                        },
                    ]
                ]
            }),

            new FormHidden({
                key: 'total_percent'
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
        ];

        let zone2: BaseForm < any > [] =[
            new FormSectionTitle({
                'label': 'Zone 2'
            }),
            new BaseFormArray({
                key: 'zones',
                formArray: [
                    new FormDummy({
                        key: 'zone',
                        value: '2'
                    }),
                    new FormSelect({
                        key: 'state',
                        options$: this.lotService.getState(),
                        filter: {
                            parent: 'zone',
                            key: 'zone'
                        }
                    }),
                    new FormSelect({
                        key: 'district',
                        options$: this.lotService.getDistrict(),
                        filter: {
                            parent: 'state',
                            key: 'state'
                        }
                    }),
                    new FormSelect({
                        key: 'township',
                        options$: this.lotService.getTownship(),
                        filter: {
                            parent: 'district',
                            key: 'district'
                        }
                    }),
                    new FormInput({
                        key: 'percent',
                        type: 'number',
                        value: '0',
                        validators: [Validators.min(0), Validators.max(100)],
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValueZone2(value, index, form, formGroup);
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        readonly: true,
                        // valueChangeEvent: (value, index, form, formGroup) => {
                        //     this.updateFormValueZone2(value, index, form, formGroup);
                        // }
                    }),
                ],
                useTable: true,
                headerWidths: ['15%', '15%', '15%', 'auto', '*'],
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue();
                },
                tableHeader: [
                    [
                        { label: 'State / Region', style: { 'text-align': 'center' } },
                        { label: 'District', style: { 'text-align': 'center' } },
                        { label: 'Township', style: { 'text-align': 'center' } },
                        { label: 'Percent of total expected investment value', style: { 'text-align': 'center', 'min-width': '150px' } },
                        { label: 'Expected Value (Kyat)', style: { 'text-align': 'center', 'min-width': '150px' } },
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 }, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['percent'] || 0);
                                });
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                });
                                return total;
                            }
                        },
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Percentage (%) of Expected Investment Value (Zone 2)', colSpan: 3 },
                        {
                            cellFn: () => {
                                return this.totalValue[1].total_percent;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[1].total_kyat;
                            }
                        },
                    ]
                ]
            }),

            new FormHidden({
                key: 'total_percent'
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
        ];

        let zone3: BaseForm < any > [] =[
            new FormSectionTitle({
                'label': 'Zone 3'
            }),
            new BaseFormArray({
                key: 'zones',
                formArray: [
                    new FormDummy({
                        key: 'zone',
                        value: '3'
                    }),
                    new FormSelect({
                        key: 'state',
                        options$: this.lotService.getState(),
                        filter: {
                            parent: 'zone',
                            key: 'zone'
                        }
                    }),
                    new FormSelect({
                        key: 'district',
                        options$: this.lotService.getDistrict(),
                        filter: {
                            parent: 'state',
                            key: 'state'
                        }
                    }),
                    new FormSelect({
                        key: 'township',
                        options$: this.lotService.getTownship(),
                        filter: {
                            parent: 'district',
                            key: 'district'
                        }
                    }),
                    new FormInput({
                        key: 'percent',
                        type: 'number',
                        validators: [Validators.min(0), Validators.max(100)],
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValueZone3(value, index, form, formGroup);
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        readonly: true,
                        // valueChangeEvent: (value, index, form, formGroup) => {
                        //     this.updateFormValueZone3(value, index, form, formGroup);
                        // }
                    }),
                ],
                useTable: true,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue();
                },
                headerWidths: ['15%', '15%', '15%','auto','*'],
                tableHeader: [
                    [
                        { label: 'State / Region', style: { 'text-align': 'center' } },
                        { label: 'District', style: { 'text-align': 'center' } },
                        { label: 'Township', style: { 'text-align': 'center' } },
                        { label: 'Percent of total expected investment value', style: { 'text-align': 'center', 'min-width': '150px' } },
                        { label: 'Expected Value (Kyat)', style: { 'text-align': 'center', 'min-width': '150px' } },
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 }, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['percent'] || 0);
                                });
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                });
                                return total;
                            }
                        },
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Percentage (%) of Expected Investment Value (Zone 3)', colSpan: 3 },
                        {
                            cellFn: () => {
                                return this.totalValue[2].total_percent;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[2].total_kyat;
                            }
                        },
                    ]
                ]
            }),

            new FormHidden({
                key: 'total_percent'
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'section_75_a',
                formGroup: [
                    new FormSectionTitle({
                        label: '6. Exemption or Relief under section 75(a) of the Myanmar Investment Law'
                    }),
                    new FormNote({
                        label: 'Locations by Zone were announced in Notification No. 10 / 2017 of the Myanmar Investment Commission. Please review Myanmar Investment Rules 83, 96, 103, 113, and 114 for more information on section 75(a) of the Myanmar Investment Law.'
                    }),
                    new FormTitle({
                        label: 'a. Total expected investment value, as indicated in the permit/endorsement or in application :'
                    }),
                    // new FormSelect({
                    //     key: 'currency',
                    //     label: 'Select currency',
                    //     columns: 'col-md-4',
                    //     options$: this.lotService.getCurrency(),
                    //     required: true
                    // }), 
                    new FormInput({
                        key: 'kyat',
                        label: 'Equivalent Kyat',
                        type: 'number',
                        columns: 'col-md-6',
                        required: true,
                    }),
                    new FormInput({
                        key: 'usd',
                        label: 'Equivalent USD',
                        type: 'number',
                        columns: 'col-md-6',
                        required: true
                    }),
                    new FormTitle({
                        label: 'b. Please indicate the expected investment value in each Zone:'
                    }),
                    new BaseFormGroup({
                        key: 'zone_1',
                        formGroup: zone1
                    }),
                    new BaseFormGroup({
                        key: 'zone_2',
                        formGroup: zone2
                    }),
                    new BaseFormGroup({
                        key: 'zone_3',
                        formGroup: zone3
                    }),
                    new FormFile({
                        key: 'estimate_document',
                        label: 'Please upload the calculation for the estimation here:',
                        required: true
                    }),
                ]
            })
        ];
    }

    updateFormValueZone1(value, index, form, formGroup) {
        let formValue = this.formGroup.get('section_75_a').value;
        formValue['zone_1']['zones'][index][form.key] = value;

        this.calculateTotalValue();
    }

    updateFormValueZone2(value, index, form, formGroup) {
        let formValue = this.formGroup.get('section_75_a').value;
        formValue['zone_2']['zones'][index][form.key] = value;
        this.calculateTotalValue();
    }

    updateFormValueZone3(value, index, form, formGroup) {
        let formValue = this.formGroup.get('section_75_a').value;
        formValue['zone_3']['zones'][index][form.key] = value;
        this.calculateTotalValue();
    }

    calculateTotalValue() {
        let formValue = this.formGroup.get('section_75_a').value;
        for (let i = 0; i < this.totalValue.length; i++) {
            this.totalValue[i].total_percent = 0;
            this.totalValue[i].total_kyat = 0;

            formValue['zone_' + (i + 1)]['zones'].map((x, j) => {
                const equ_kyat = formValue['kyat'] || 0;
                // const kyat = (Number(x.percent) / 100) * Number(equ_kyat);
                const kyat = Number(x.percent) * Number(equ_kyat);
                (this.formGroup.get('section_75_a').get('zone_' + (i + 1)).get('zones') as FormArray).
                    controls[j].get('kyat').setValue(kyat);


                this.totalValue[i].total_percent += Number(x.percent);
                this.totalValue[i].total_kyat += Number(kyat);
                this.formGroup.get('section_75_a').get('zone_' + (i + 1)).get('total_percent').setValue(this.totalValue[i].total_percent);
                this.formGroup.get('section_75_a').get('zone_' + (i + 1)).get('total_kyat').setValue(this.totalValue[i].total_kyat);
            });
        }
    }
}