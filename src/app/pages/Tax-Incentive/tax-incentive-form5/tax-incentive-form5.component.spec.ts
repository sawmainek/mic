import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm5Component } from './tax-incentive-form5.component';

describe('TaxIncentiveForm5Component', () => {
  let component: TaxIncentiveForm5Component;
  let fixture: ComponentFixture<TaxIncentiveForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
