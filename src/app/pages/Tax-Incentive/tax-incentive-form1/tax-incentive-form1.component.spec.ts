import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm1Component } from './tax-incentive-form1.component';

describe('TaxIncentiveForm1Component', () => {
  let component: TaxIncentiveForm1Component;
  let fixture: ComponentFixture<TaxIncentiveForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
