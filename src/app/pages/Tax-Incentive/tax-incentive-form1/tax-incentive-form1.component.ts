import { formProgress } from './../../../core/form/_selectors/form.selectors';
import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppState } from 'src/app/core';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { TaxIncentive } from 'src/app/models/tax_incentive';
import { FormProgressService } from 'src/services/form-progress.service';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { cloneDeep } from 'lodash';
import { CurrentForm } from 'src/app/core/form/_actions/form.actions';


@Component({
    selector: 'app-tax-incentive-form1',
    templateUrl: './tax-incentive-form1.component.html',
    styleUrls: ['./tax-incentive-form1.component.scss']
})
export class TaxIncentiveForm1Component extends TaxIncentiveFormData implements OnInit {
    menu: any = "formData";
    correct: any = false;
    @Input() id: any;
    mm: any;

    user: any = {};
    model: any = {};
    taxModel: TaxIncentive
    taxMMModel: TaxIncentive
    form: any;

    progressLists: any[] = [];
    pages: any[] = [];

    loading = false;

    constructor(
        private formService: TaxIncentiveFormService,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private formProgressService: FormProgressService,
        private store: Store<AppState>,
        private router: Router,
        private translateService: TranslateService,

    ) {
        super(formCtlService);
        this.loading = true;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            if (this.id > 0) {
                // Get data from API
                this.formService.show(this.id)
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.form?.data || {};
                        this.taxMMModel = this.form?.data_mm || {};
                        this.model = (this.form?.language == 'Myanmar') ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.getFormProgress();

                        this.changeLanguage();
                        this.loading = false;

                    });
            } else {
                this.loading = false;
            }
        }).unsubscribe();
    }

    ngOnInit(): void { }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getFormProgress() {
        this.pages = [
            'tax-incentive/form2',
            'tax-incentive/form3',
            'tax-incentive/form4',
            'tax-incentive/form5',
            'tax-incentive/form6',
            'tax-incentive/form7',
            'tax-incentive/form8',
            'tax-incentive/form9',
            'tax-incentive/form10',
            'tax-incentive/form11',
            'tax-incentive/form12'
        ];
        this.store.pipe(select(formProgress))
            .subscribe(progress => {
                progress = progress.filter(x => x.form_id == this.form?.id);
                if (progress?.length > 0) {
                    this.progressLists = [...[], ...progress];
                } else {
                    this.formProgressService.get({
                        rows: 999,
                        search: `form_id:equal:${this.form?.id || 0}|type:equal:Tax Incentive`
                    }).subscribe(result => {
                        this.progressLists = result;
                    });
                }
            }).unsubscribe();
    }

    checkComplete(index) {
        const page = this.pages[index] || null;
        if (this.form?.language == 'Myanmar') {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.taxMMModel.id}/mm`).length;
            return length > 0 ? true : false;
        } else {
            const length = this.progressLists.filter(x => x.page == `${page}/${this.taxModel.id}`).length;
            const lengthMM = this.progressLists.filter(x => x.page == `${page}/${this.taxMMModel.id}/mm`).length;
            return length > 0 && lengthMM > 0 ? true : false;
        }
    }

    checkRevise(page) {
        const revise = this.progressLists.filter(x => x.page.indexOf(page) !== -1 && x.revise == true).length;
        return revise > 0 ? true : false;
    }

    openPage(previous, link) {
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/officer/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/officer/' + link, this.id]);
            }

        } else {
            if (this.form?.language == 'Myanmar') {
                this.router.navigate(['/pages/' + link, this.id, 'mm']);
            } else {
                this.router.navigate(['/pages/' + link, this.id]);
            }
        }
    }

}
