import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm6Component } from './tax-incentive-form6.component';

describe('TaxIncentiveForm6Component', () => {
  let component: TaxIncentiveForm6Component;
  let fixture: ComponentFixture<TaxIncentiveForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
