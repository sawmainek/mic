import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormGroup, Validators } from '@angular/forms';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { forkJoin, Observable } from 'rxjs';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { HttpClient } from '@angular/common/http';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';
import { MICFormService } from 'src/services/micform.service';
import { EndorsementFormService } from 'src/services/endorsement-form.service';


@Component({
    selector: 'app-tax-incentive-form6',
    templateUrl: './tax-incentive-form6.component.html',
    styleUrls: ['./tax-incentive-form6.component.scss']
})
export class TaxIncentiveForm6Component extends TaxIncentiveFormData implements OnInit {
    menu: any = "formData";
    page = "tax-incentive/form6/";
    @Input() id: any;
    mm: any;

    user: any = {};

    formGroup: FormGroup;
    taxModel: any = {};
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    totalValue: any[] = [
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 }
    ]

    new_item: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Brand new items'
        }),
        new BaseFormArray({
            key: 'items',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                    required: true,
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    required: true,
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                    required: true
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    required: true,
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    required: true,
                    value: '102',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    required: true,
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                    required: true
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('new_item', 0);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                    { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[0].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    recondition_item: BaseForm<any>[] = [
        new FormSectionTitle({
            'label': 'Reconditioned Items'
        }),
        new BaseFormArray({
            key: 'items',
            formArray: [
                new FormSectionTitleCounter({
                    label: '',
                    style: { 'text-align': 'center' }
                }),
                new FormInput({
                    key: 'name',
                }),
                new FormInput({
                    key: 'code',
                    type: 'number',
                    validators: [Validators.minLength(4), Validators.maxLength(4)],
                }),
                new FormInput({
                    key: 'unit',
                    // type: 'number',
                }),
                new FormInput({
                    key: 'price',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'quantity',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'total',
                    type: 'number',
                    value: '0',
                    readonly: true
                }),
                new FormSelect({
                    key: 'currency',
                    options$: this.lotService.getCurrency(),
                    value: '102',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'kyat',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormInput({
                    key: 'usd',
                    type: 'number',
                    value: '0',
                    valueChangeEvent: (value, index, form, formGroup) => {
                        this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                    }
                }),
                new FormSelect({
                    key: 'country',
                    options$: this.lotService.getCountry(),
                }),
            ],
            useTable: true,
            rowDeleteEvent: (index) => {
                this.calculateTotalValue('recondition_item', 1);
            },
            tableHeader: [
                [
                    { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                    { label: 'Item', style: { 'text-align': 'center' } },
                    { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                    { label: 'Unit', style: { 'text-align': 'center' } },
                    { label: 'Unit Price', style: { 'text-align': 'center' } },
                    { label: 'Quantity', style: { 'text-align': 'center' } },
                    { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                    { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                    { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                    { label: 'Country procured from', style: { 'text-align': 'center' } }
                ],
                [
                    { label: '1', style: { 'text-align': 'center' } },
                    { label: '2', style: { 'text-align': 'center' } },
                    { label: '3', style: { 'text-align': 'center' } },
                    { label: '4', style: { 'text-align': 'center' } },
                    { label: '5', style: { 'text-align': 'center' } },
                    { label: '6', style: { 'text-align': 'center' } },
                    { label: '7', style: { 'text-align': 'center' } },
                    { label: '8', style: { 'text-align': 'center' } },
                    { label: '9', style: { 'text-align': 'center' } },
                    { label: '10', style: { 'text-align': 'center' } }
                ]
            ],
            tableFooter: [
                [
                    { label: 'Total', colSpan: 8 },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_kyat;
                        }
                    },
                    {
                        cellFn: () => {
                            return this.totalValue[1].total_usd;
                        }
                    },
                ]
            ]
        }),
        new FormHidden({
            key: 'total_kyat'
        }),
        new FormHidden({
            key: 'total_usd'
        }),
    ];

    taxIncentiveForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'section_77_a',
            formGroup: [
                new FormSectionTitle({
                    label: '7. Exemption or Relief under section 77(a) of the Myanmar Investment Law'
                }),
                new FormTitle({
                    label: 'a. Please indicate a list of machinery, equipment, instruments, machinery components, spare parts, construction materials unavailable locally, and materials used in the business, to be imported as they are actually required during the construction period or preparatory period of the investment:'
                }),
                new BaseFormGroup({
                    key: 'new_item',
                    formGroup: this.new_item
                }),
                new BaseFormGroup({
                    key: 'recondition_item',
                    formGroup: this.recondition_item
                })
            ]
        })
    ]

    constructor(
        private http: HttpClient,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private formService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxService: TaxIncentiveService,
        private lotService: LocationService,
        private translateService: TranslateService,
        private micFormService: MICFormService,
        private endoFormService: EndorsementFormService,
    ) {
        super(formCtlService);
        this.loading = true;

        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

    }

    ngOnInit() { }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};
                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.taxModel.section_77_a = this.taxModel.section_77_a || form?.data?.section_77_a || {};

                        this.get_mic_endo();

                        this.page = "tax-incentive/form6/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });

                        this.changeLanguage();

                        this.loading = false;
                        this.loaded = true;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    get_mic_endo() {
        const params = {
            search: `user_id:equal:${this.form.user_id}`,
        };
        forkJoin([
            this.micFormService.get(params),
            this.endoFormService.get(params)
        ]).subscribe(results => {
            var micModel = {};
            var endoModel = {};

            if (results[0].length > 0) {
                micModel = this.mm ? results[0][0]?.data_mm || {} : results[0][0]?.data || {};
            }
            if (results[1].length > 0) {
                endoModel = this.mm ? results[1][0]?.data_mm || {} : results[1][0]?.data || {};
            }

            this.taxModel.section_77_a.new_item = this.taxModel?.section_77_a?.new_item ? this.taxModel.section_77_a?.new_item : micModel?.['expected_investmentdt']?.new_item ? micModel?.['expected_investmentdt']?.new_item : endoModel?.['expected_investmentdt']?.new_item;
            this.taxModel.section_77_a.recondition_item = this.taxModel?.section_77_a?.recondition_item ? this.taxModel.section_77_a?.recondition_item : micModel?.['expected_investmentdt']?.recondition_item ? micModel?.['expected_investmentdt']?.recondition_item : endoModel?.['expected_investmentdt']?.recondition_item;

            this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm, this.taxModel);
            this.calculateTotalValue('new_item', 0);
            this.calculateTotalValue('recondition_item', 1);
        });
    }

    calculateEquivalent(value, index, form, formGroup, control) {
        let formValue = this.formGroup.get('section_77_a').value;
        formValue[control]['items'][index][form.key] = value;
        this.calculateTotalValue(control, control == 'new_item' ? 0 : 1);
    }

    calculateTotalValue(control, index) {
        let formValue = this.formGroup.get('section_77_a').value;
        this.totalValue[index].total_kyat = 0;
        this.totalValue[index].total_usd = 0;

        formValue[control]['items'].map(x => {
            this.totalValue[index].total_kyat += parseFloat(x.kyat);
            this.totalValue[index].total_usd += parseFloat(x.usd);

            this.formGroup.get('section_77_a').get(control).get('total_kyat').setValue(this.totalValue[index].total_kyat);
            this.formGroup.get('section_77_a').get(control).get('total_usd').setValue(this.totalValue[index].total_usd);
        });
    }

    updateFormValue(value, index, form, formGroup, control) {
        let formValue = this.formGroup.get('section_77_a').value;
        formValue[control]['items'][index][form.key] = value;

        // Update Total Value
        let rowValue = formGroup.value;
        formGroup.get('total').setValue(Number(rowValue.quantity | 0) * Number(rowValue.price | 0));

        // Update Exchange
        // var currency = this.currencies.filter(x => x.id == rowValue.currency);
        // if (currency.length > 0) {
        //   var exchange_choose = this.exchange_rates.filter(x => x.code === currency[0].name);
        //   var exchange_usd = this.exchange_rates.filter(x => x.code == 'USD');
        //   if (exchange_choose.length > 0) {
        //     formGroup.get('kyat').setValue(Number(formGroup.get('total').value | 0) * Number(exchange_choose[0].rate | 0));
        //     formGroup.get('usd').setValue(Number(formGroup.get('kyat').value | 0) / Number(exchange_usd[0].rate | 0));
        //   }
        // }

        this.calculateTotalValue(control, control == 'new_item' ? 0 : 1);
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.taxModel = { ...this.taxModel, ...this.formGroup.value };

        this.service.create(this.taxModel)
            .subscribe(x => {
                console.log(x);
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);
                    this.redirectLink(this.form.id)
                } else {
                    this.toast.success('Saved successfully.');
                }
            }, error => {
                console.log(error);
            });
    }
    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            //For choose Myanmar Language
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/tax-incentive/form7', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/tax-incentive/form7', id]);
                }
                else {
                    this.router.navigate([page + '/tax-incentive/form6', id, 'mm']);
                }
            }
        }
    }
}
