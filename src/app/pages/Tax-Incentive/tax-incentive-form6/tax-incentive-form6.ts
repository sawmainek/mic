import { FormGroup, Validators } from '@angular/forms';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { LocationService } from 'src/services/location.service';

export class TaxIncentiveForm6 {

    public forms: BaseForm<string>[] = [];
    formGroup: FormGroup;
    totalValue: any[] = [
        { 'total_kyat': 0, 'total_usd': 0 },
        { 'total_kyat': 0, 'total_usd': 0 }
    ]

    constructor(
        private lotService: LocationService,
    ) {
        let new_item: BaseForm < any > [] =[
            new FormSectionTitle({
                'label': 'Brand new items'
            }),
            new BaseFormArray({
                key: 'items',
                formArray: [
                    new FormSectionTitleCounter({
                        label: '',
                        style: { 'text-align': 'center' }
                    }),
                    new FormInput({
                        key: 'name',
                        required: true,
                    }),
                    new FormInput({
                        key: 'code',
                        type: 'number',
                        required: true,
                        validators: [Validators.minLength(4), Validators.maxLength(4)],
                    }),
                    new FormInput({
                        key: 'unit',
                        // type: 'number',
                        required: true
                    }),
                    new FormInput({
                        key: 'price',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormInput({
                        key: 'quantity',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormInput({
                        key: 'total',
                        type: 'number',
                        value: '0',
                        required: true,
                        readonly: true
                    }),
                    new FormSelect({
                        key: 'currency',
                        options$: this.lotService.getCurrency(),
                        required: true,
                        value: '102',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormInput({
                        key: 'usd',
                        type: 'number',
                        value: '0',
                        required: true,
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'new_item');
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: this.lotService.getCountry(),
                        required: true
                    }),
                ],
                useTable: true,
                headerRows: 2,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue('new_item', 0);
                },
                tablePDFHeader: [
                    [
                        { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        {},
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        {},
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Total', colSpan: 8 }, {}, {}, {}, {}, {}, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                })
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['usd'] || 0);
                                })
                                return total;
                            }
                        },
                        {}
                    ]
                ],
                tableHeader: [
                    [
                        { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Total', colSpan: 8 },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_kyat;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[0].total_usd;
                            }
                        },
                    ]
                ]
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
            new FormHidden({
                key: 'total_usd'
            }),
        ];

        let recondition_item: BaseForm < any > [] =[
            new FormSectionTitle({
                'label': 'Reconditioned Items'
            }),
            new BaseFormArray({
                key: 'items',
                formArray: [
                    new FormSectionTitleCounter({
                        label: '',
                        style: { 'text-align': 'center' }
                    }),
                    new FormInput({
                        key: 'name',
                    }),
                    new FormInput({
                        key: 'code',
                        type: 'number',
                        validators: [Validators.minLength(4), Validators.maxLength(4)],
                    }),
                    new FormInput({
                        key: 'unit',
                        // type: 'number',
                    }),
                    new FormInput({
                        key: 'price',
                        type: 'number',
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormInput({
                        key: 'quantity',
                        type: 'number',
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormInput({
                        key: 'total',
                        type: 'number',
                        value: '0',
                        readonly: true
                    }),
                    new FormSelect({
                        key: 'currency',
                        options$: this.lotService.getCurrency(),
                        value: '102',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.updateFormValue(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormInput({
                        key: 'kyat',
                        type: 'number',
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormInput({
                        key: 'usd',
                        type: 'number',
                        value: '0',
                        valueChangeEvent: (value, index, form, formGroup) => {
                            this.calculateEquivalent(value, index, form, formGroup, 'recondition_item');
                        }
                    }),
                    new FormSelect({
                        key: 'country',
                        options$: this.lotService.getCountry(),
                    }),
                ],
                useTable: true,
                headerRows: 2,
                rowDeleteEvent: (index) => {
                    this.calculateTotalValue('recondition_item', 1);
                },
                tablePDFHeader: [
                    [
                        { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        {},
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        {},
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tablePDFFooter: [
                    [
                        { label: 'Total', colSpan: 8 }, {}, {}, {}, {}, {}, {}, {},
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['kyat'] || 0);
                                })
                                return total;
                            }
                        },
                        {
                            cellFn: (data: any[]) => {
                                let total = 0;
                                data.forEach(x => {
                                    total += Number(x['usd'] || 0);
                                })
                                return total;
                            }
                        },
                        {}
                    ]
                ],
                tableHeader: [
                    [
                        { label: 'No.', style: { 'text-align': 'center', 'vertical-align': 'middle' }, rowSpan: 2 },
                        { label: 'Item', style: { 'text-align': 'center' } },
                        { label: 'HS Code ( with four digits )', style: { 'text-align': 'center' } },
                        { label: 'Unit', style: { 'text-align': 'center' } },
                        { label: 'Unit Price', style: { 'text-align': 'center' } },
                        { label: 'Quantity', style: { 'text-align': 'center' } },
                        { label: 'Total Value', style: { 'text-align': 'center' }, colSpan: 2 },
                        { label: 'Equivalent Kyat', style: { 'text-align': 'center' } },
                        { label: 'Equivalent USD', style: { 'text-align': 'center' } },
                        { label: 'Country procured from', style: { 'text-align': 'center' } }
                    ],
                    [
                        { label: '1', style: { 'text-align': 'center' } },
                        { label: '2', style: { 'text-align': 'center' } },
                        { label: '3', style: { 'text-align': 'center' } },
                        { label: '4', style: { 'text-align': 'center' } },
                        { label: '5', style: { 'text-align': 'center' } },
                        { label: '6', style: { 'text-align': 'center' } },
                        { label: '7', style: { 'text-align': 'center' } },
                        { label: '8', style: { 'text-align': 'center' } },
                        { label: '9', style: { 'text-align': 'center' } },
                        { label: '10', style: { 'text-align': 'center' } }
                    ]
                ],
                tableFooter: [
                    [
                        { label: 'Total', colSpan: 8 },
                        {
                            cellFn: () => {
                                return this.totalValue[1].total_kyat;
                            }
                        },
                        {
                            cellFn: () => {
                                return this.totalValue[1].total_usd;
                            }
                        },
                    ]
                ]
            }),
            new FormHidden({
                key: 'total_kyat'
            }),
            new FormHidden({
                key: 'total_usd'
            }),
        ];

        this.forms = [
            new BaseFormGroup({
                key: 'section_77_a',
                formGroup: [
                    new FormSectionTitle({
                        label: '7. Exemption or Relief under section 77(a) of the Myanmar Investment Law',
                        pageBreak: 'before',
                        pageOrientation: 'landscape'
                    }),
                    new FormTitle({
                        label: 'a. Please indicate a list of machinery, equipment, instruments, machinery components, spare parts, construction materials unavailable locally, and materials used in the business, to be imported as they are actually required during the construction period or preparatory period of the investment:'
                    }),
                    new BaseFormGroup({
                        key: 'new_item',
                        formGroup: new_item
                    }),
                    new BaseFormGroup({
                        key: 'recondition_item',
                        formGroup: recondition_item
                    })
                ]
            })
        ];
    }

    updateFormValue(value, index, form, formGroup, control) {
        let formValue = this.formGroup.get('section_77_a').value;
        formValue[control]['items'][index][form.key] = value;

        // Update Total Value
        let rowValue = formGroup.value;
        formGroup.get('total').setValue(Number(rowValue.quantity | 0) * Number(rowValue.price | 0));

        this.calculateTotalValue(control, control == 'new_item' ? 0 : 1);
    }

    calculateTotalValue(control, index) {
        let formValue = this.formGroup.get('section_77_a').value;
        this.totalValue[index].total_kyat = 0;
        this.totalValue[index].total_usd = 0;

        formValue[control]['items'].map(x => {
            this.totalValue[index].total_kyat += parseFloat(x.kyat);
            this.totalValue[index].total_usd += parseFloat(x.usd);

            this.formGroup.get('section_77_a').get(control).get('total_kyat').setValue(this.totalValue[index].total_kyat);
            this.formGroup.get('section_77_a').get(control).get('total_usd').setValue(this.totalValue[index].total_usd);
        });
    }

    calculateEquivalent(value, index, form, formGroup, control) {
        let formValue = this.formGroup.get('section_77_a').value;
        formValue[control]['items'][index][form.key] = value;
        this.calculateTotalValue(control, control == 'new_item' ? 0 : 1);
    }
}