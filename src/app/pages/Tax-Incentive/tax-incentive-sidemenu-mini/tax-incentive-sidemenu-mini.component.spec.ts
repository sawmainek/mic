import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveSidemenuMiniComponent } from './tax-incentive-sidemenu-mini.component';

describe('TaxIncentiveSidemenuMiniComponent', () => {
  let component: TaxIncentiveSidemenuMiniComponent;
  let fixture: ComponentFixture<TaxIncentiveSidemenuMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveSidemenuMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveSidemenuMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
