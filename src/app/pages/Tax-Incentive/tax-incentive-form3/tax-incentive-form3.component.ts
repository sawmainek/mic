import { ToastrService } from 'ngx-toastr';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxIncentiveService } from 'src/services/tax_incentive.service';
import { Location } from '@angular/common';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { FormGroup } from '@angular/forms';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { TaxIncentiveFormData } from '../tax-incentive-form-data';
import { BaseFormData } from 'src/app/core/form';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { CurrentForm, Submit } from 'src/app/core/form/_actions/form.actions';
import { TaxIncentiveFormService } from 'src/services/tax-incentive-form.service';
import { TaxIncentiveMMService } from 'src/services/tax-incentive-mm.service';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'app-tax-incentive-form3',
    templateUrl: './tax-incentive-form3.component.html',
    styleUrls: ['./tax-incentive-form3.component.scss']
})
export class TaxIncentiveForm3Component extends TaxIncentiveFormData implements OnInit {
    menu: any = "formData";
    page = "tax-incentive/form3/";
    @Input() id: any;
    mm: any;

    user: any = {};

    formGroup: FormGroup;
    taxModel: any = {};
    form: any;
    service: any;

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    is_draft = false;
    is_save = false;
    loading = false;

    isRevision: boolean = false;

    taxIncentiveForm: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'investment',
            formGroup: [
                new FormSectionTitle({
                    label: '3. For which investment is the tax incentive application being submitted?'
                }),
                new FormRadio({
                    key: 'is_tax_incentive',
                    label: 'Application is still in progress',
                    value: 'Application is still in progress',
                    columns: 'col-md-6',
                    required: true
                }),
                new FormRadio({
                    key: 'is_tax_incentive',
                    label: 'Application has been improved',
                    value: 'Application has been improved',
                    columns: 'col-md-6',
                    required: true
                }),
                new FormInput({
                    key: 'serial_no',
                    label: 'Application serial number',
                    required: true,
                    criteriaValue: {
                        key: 'is_tax_incentive',
                        value: 'Application is still in progress'
                    }
                }),
                new FormInput({
                    key: 'endorsement_no',
                    label: 'Permit or endorsement number',
                    required: true,
                    criteriaValue: {
                        key: 'is_tax_incentive',
                        value: 'Application has been improved'
                    }
                }),
                new FormDate({
                    key: 'receive_date',
                    label: 'Date of receiving permit or endorsement',
                    required: true,
                    criteriaValue: {
                        key: 'is_tax_incentive',
                        value: 'Application has been improved'
                    }
                }),
                new FormSectionTitle({
                    label: '4. Investment period'
                }),
                new FormTitle({
                    label: 'Construction or preparatory period of investment'
                }),
                new FormDate({
                    key: 'construction_from',
                    label: 'From',
                    columns: 'col-md-6',
                    required: true
                }),
                new FormDate({
                    key: 'construction_to',
                    label: 'To',
                    columns: 'col-md-6',
                    required: true
                }),
                new FormDate({
                    key: 'commencement_date',
                    label: 'Date of commencement of commercial operation',
                    required: true
                }),
                new FormTitle({
                    label: 'Planned investment period'
                }),
                new FormDate({
                    key: 'investment_from',
                    label: 'From',
                    columns: 'col-md-6',
                    required: true
                }),
                new FormDate({
                    key: 'investment_to',
                    label: 'To',
                    columns: 'col-md-6',
                    required: true
                }),
            ]
        })
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public formCtlService: FormControlService,
        private store: Store<AppState>,
        private toast: ToastrService,
        private formService: TaxIncentiveFormService, //For New Flow
        private taxMMService: TaxIncentiveMMService,
        private taxService: TaxIncentiveService,
        public location: Location,
        private translateService: TranslateService
    ) {
        super(formCtlService);
        this.loading = true;

        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm);
        this.getTaxIncentiveData();
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;
    }

    ngOnInit(): void { }

    getTaxIncentiveData() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.mm = paramMap.get('mm') || null;

            this.service = this.mm ? this.taxMMService : this.taxService;

            if (this.id) {
                this.formService.show(this.id, { secure: true })
                    .subscribe((form: any) => {
                        this.form = form || {};
                        this.taxModel = this.mm ? form?.data_mm || {} : form?.data || {};

                        this.store.dispatch(new CurrentForm({ form: cloneDeep(form) }));

                        this.formGroup = this.formCtlService.toFormGroup(this.taxIncentiveForm, this.taxModel, form?.data || {});

                        this.page = "tax-incentive/form3/";
                        this.page += this.mm && this.mm == 'mm' ? this.taxModel.id + '/mm' : this.taxModel.id;

                        this.formCtlService.getComments$('Tax Incentive', this.taxModel?.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.taxModel?.id, type: 'Tax Incentive', page: this.page, comments }));
                        });

                        this.changeLanguage();
                        this.loaded = true;
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    changeLanguage() {
        let lan = this.mm == 'mm' ? 'mm' : 'en';
        this.translateService.use(lan);
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.is_draft = true;
        this.saveFormData();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.is_draft = false;
        this.saveFormData();
    }

    saveFormData() {
        this.spinner = true;
        this.taxModel = { ...this.taxModel, ...this.formGroup.value };

        this.service.create(this.taxModel)
            .subscribe(x => {
                this.spinner = false;
                if (!this.is_draft) {
                    this.saveFormProgress(this.form?.id, this.taxModel.id, this.page);
                    this.redirectLink(this.form.id);
                } else {
                    this.toast.success('Saved successfully.');
                }
            });
    }
    redirectLink(id: number) {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            const page = window.location.href.indexOf('officer') !== -1 ? 'officer' : 'pages';
            //For choose Myanmar Language
            if (this.form.language == 'Myanmar') {
                this.router.navigate([page + '/tax-incentive/form4', id, 'mm']);
            } else {
                //For choose English/Myanmar Language
                if (this.mm) {
                    this.router.navigate([page + '/tax-incentive/form4', id]);
                }
                else {
                    this.router.navigate([page + '/tax-incentive/form3', id, 'mm']);
                }
            }
        }
    }
}
