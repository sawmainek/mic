import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveForm3Component } from './tax-incentive-form3.component';

describe('TaxIncentiveForm3Component', () => {
  let component: TaxIncentiveForm3Component;
  let fixture: ComponentFixture<TaxIncentiveForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
