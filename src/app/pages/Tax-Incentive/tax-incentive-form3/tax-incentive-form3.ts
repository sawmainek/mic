import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from 'src/app/custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';

export class TaxIncentiveForm3{
    
    public forms: BaseForm<string>[] = []

    constructor(

    ) {
        this.forms = [
            new BaseFormGroup({
                key: 'investment',
                formGroup: [
                    new FormSectionTitle({
                        label: '3. For which investment is the tax incentive application being submitted?'
                    }),
                    new FormRadio({
                        key: 'is_tax_incentive',
                        label: 'Application is still in progress',
                        value: 'Application is still in progress',
                        columns: 'col-md-6',
                        required: true
                    }),
                    new FormRadio({
                        key: 'is_tax_incentive',
                        label: 'Application has been improved',
                        value: 'Application has been improved',
                        columns: 'col-md-6',
                        required: true
                    }),
                    new FormInput({
                        key: 'serial_no',
                        label: 'Application serial number',
                        required: true,
                        criteriaValue: {
                            key: 'is_tax_incentive',
                            value: 'Application is still in progress'
                        }
                    }),
                    new FormInput({
                        key: 'endorsement_no',
                        label: 'Permit or endorsement number',
                        required: true,
                        criteriaValue: {
                            key: 'is_tax_incentive',
                            value: 'Application has been improved'
                        }
                    }),
                    new FormDate({
                        key: 'receive_date',
                        label: 'Date of receiving permit or endorsement',
                        required: true,
                        criteriaValue: {
                            key: 'is_tax_incentive',
                            value: 'Application has been improved'
                        }
                    }),
                    new FormSectionTitle({
                        label: '4. Investment period'
                    }),
                    new FormTitle({
                        label: 'Construction or preparatory period of investment'
                    }),
                    new FormDate({
                        key: 'construction_from',
                        label: 'From',
                        columns: 'col-md-6',
                        required: true
                    }),
                    new FormDate({
                        key: 'construction_to',
                        label: 'To',
                        columns: 'col-md-6',
                        required: true
                    }),
                    new FormDate({
                        key: 'commencement_date',
                        label: 'Date of commencement of commercial operation',
                        required: true
                    }),
                    new FormTitle({
                        label: 'Planned investment period'
                    }),
                    new FormDate({
                        key: 'investment_from',
                        label: 'From',
                        columns: 'col-md-6',
                        required: true
                    }),
                    new FormDate({
                        key: 'investment_to',
                        label: 'To',
                        columns: 'col-md-6',
                        required: true
                    }),
                ]
            })
        ];
    }
}