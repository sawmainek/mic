import { SafeHtmlPipePipe } from './../pipe/safe-html-pipe.pipe';
import { AuthGuard } from './../core/auth/_guards/auth.guards';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { CustomComponentModule } from './../custom-component/custom-component.module';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [
        SafeHtmlPipePipe,
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        CustomComponentModule,
        NgbModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // closeButton: true,
        }),
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            },
            isolate:false
        })
    ],
    exports: [
        SafeHtmlPipePipe
    ],
    providers: [
        AuthGuard,
        TranslateService
    ],
})
export class GlobalModule { }
