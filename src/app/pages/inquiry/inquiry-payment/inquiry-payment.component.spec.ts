import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryPaymentComponent } from './inquiry-payment.component';

describe('InquiryPaymentComponent', () => {
  let component: InquiryPaymentComponent;
  let fixture: ComponentFixture<InquiryPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
