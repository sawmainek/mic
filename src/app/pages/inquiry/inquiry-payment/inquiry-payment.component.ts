import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { AppState, currentUser } from 'src/app/core';
import { select, Store } from '@ngrx/store';
import { InquirySubmissionService } from 'src/services/inquiry-submission.service';

@Component({
    selector: 'app-inquiry-payment',
    templateUrl: './inquiry-payment.component.html',
    styleUrls: ['./inquiry-payment.component.scss']
})
export class InquiryPaymentComponent implements OnInit {

    user: any = {};
    inquirySubmissionModel: any = {};
    total: number = 0;

    constructor(
        private inquirySubmissionService: InquirySubmissionService,
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                    this.getSubmissionData();
                }
            });
    }

    getSubmissionData() {
        const params = {
            page: 1,
            search: `submitted_user_id:equal:${this.user?.id}`,
            secure: true,
        };
        this.inquirySubmissionService.get(params).subscribe((results: any) => {
            this.inquirySubmissionModel = results[0] || {};
            this.inquirySubmissionModel.invoices = this.inquirySubmissionModel?.invoices || [];

            this.total = 0;
            this.inquirySubmissionModel.invoices.forEach(invoice => {
                this.total = this.total + Number(invoice.amount);
            });
        });
    }

    saveCredit() {
        window.location.href = environment.host + "/payments/mpu?request_id=1&user_id="+ this.user.id +"&amount="+ this.total +"&success_redirect_url=lcoalhost:8000&fail_redirect_url=localhost:8000"
    }

    saveMPU() {
        window.location.href = environment.host + "/payments/mpu?request_id=1&user_id="+ this.user.id +"&amount="+ this.total +"&success_redirect_url=lcoalhost:8000&fail_redirect_url=localhost:8000"
    }
}

