import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryForm6Component } from './inquiry-form6.component';

describe('InquiryForm6Component', () => {
  let component: InquiryForm6Component;
  let fixture: ComponentFixture<InquiryForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
