import { TranslateService } from '@ngx-translate/core';
import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { FormDate } from './../../../custom-component/dynamic-forms/base/form.date';
import { FormControlService } from './../../../custom-component/dynamic-forms/form-control.service';
import { FormRadio } from './../../../custom-component/dynamic-forms/base/form-radio';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { FormSectionTitle } from './../../../custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from './../../../custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { Inquiry } from 'src/app/models/inquiry';
import { Location } from '@angular/common';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';
import { FormSectionTitleCounter } from 'src/app/custom-component/dynamic-forms/base/form-section-title-counter';
import { LocalService } from 'src/services/local.service';
import { AuthService } from 'src/services/auth.service';
import { InquiryForm6 } from './inquiry-form6';

@Component({
    selector: 'app-inquiry-form6',
    templateUrl: './inquiry-form6.component.html',
    styleUrls: ['./inquiry-form6.component.scss']
})
export class InquiryForm6Component implements OnInit {
    @Input() id: any;

    user: any = {};

    inquiryModel: Inquiry;
    inquiryFormGroup: FormGroup;

    submitted = false;
    loading = false;
    spinner = false;
    isDraft = false;

    isRevision: boolean = false;

    /* planLocation: BaseForm<any>[] = [
        new FormSectionTitleCounter({
            label: 'Location'
        }),
        new FormSectionTitle({
            label: 'a. Address'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-6'
        }),
        new FormSelect({
            key: 'district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-6',
            filter: {
                parent: 'state',
                key: 'state'
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'district',
                key: 'district'
            }
        }),
        new FormSectionTitle({
            label: 'b. Information on current administrator:'
        }),
        new FormInput({
            key: 'owner_name',
            label: 'Name',
            required: true,
            columns: 'col-md-6'
        }),
        new FormSelect({
            key: 'owner_type',
            label: 'Administrator Type',
            options$: this.localService.getType(),
            placeholder: 'Select administrator type',
            required: true,
            columns: 'col-md-6'
        }),
        new FormSelect({
            key: 'land_type',
            label: 'c. Type of land',
            placeholder: 'Select land type',
            options$: this.localService.getLandType(),
            required: true
        }),
        new FormSectionTitle({
            label: 'd. In case of sublease (i.e. if the current administrator is not the lessor), information on lessor:'
        }),
        new FormInput({
            key: 'lessor_name',
            label: 'Name:',
            columns: 'col-md-6'
        }),
        new FormSelect({
            key: 'lessor_type',
            label: 'Type',
            placeholder: 'Select lessor type',
            options$: this.localService.getType(),
            columns: 'col-md-6'
        }),
        new FormTitle({
            label: 'e. If subleasing state-owned land or buildings, please select which of the following apply to the lessor:'
        }),
        new FormRadio({
            key: 'lessor_type_apply',
            value: '1',
            label: 'A person who has previously obtained the right to use the state-owned and or buildings from the government department and government organization in accordance with the laws of the Union.',
            required: true
        }),
        new FormRadio({
            key: 'lessor_type_apply',
            value: '2',
            label: 'A person authorized to sub-lease or sub-license the state-owned land or building in accordance with the approval of the government department and government organization.',
            required: true
        }),
        new FormRadio({
            key: 'lessor_type_apply',
            value: '3',
            label: 'Not applicable',
            required: true
        }),
        new FormSectionTitle({
            label: 'f. Planned lease or use period:'
        }),
        new FormDate({
            key: 'plan_from',
            label: 'From Date',
            columns: 'col-md-6',
            required: true
        }),
        new FormDate({
            key: 'plan_to',
            label: 'To Date',
            columns: 'col-md-6',
            required: true
        })

    ];
    inquiryForm: BaseForm<any>[] = [
        new FormSectionTitle({
            label: '6. Planned location(s) of the investment project'
        }),
        new BaseFormArray({
            key: 'plan_location',
            addMoreText: 'Add more locations',
            formArray: this.planLocation
        })
    ] */

    baseForm: InquiryForm6;
    constructor(
        private http: HttpClient,
        private activatedRoute: ActivatedRoute,
        private service: InquiryService,
        private formCtlService: FormControlService,
        private router: Router,
        private lotService: LocationService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private localService: LocalService,
        private translateService: TranslateService
    ) {
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            this.baseForm = new InquiryForm6(null, this.lotService, this.localService);
            this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            if (this.id > 0) {
                this.service.show(this.id, { secure: true })
                    .subscribe((x: any) => {
                        this.inquiryModel = x as Inquiry;
                        const lan = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                        this.translateService.use(lan);
                        this.baseForm = new InquiryForm6(lan, this.lotService, this.localService);
                        this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.inquiryModel);

                        const page = `inquiry/form6/${this.inquiryModel.id}`;
                        this.formCtlService.getComments$('Inquiry', this.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.id, type: 'Inquiry', page: page, comments }));
                        });
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.isDraft = true;
        this.saveInquiry();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.inquiryFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.isDraft = false;
        this.saveInquiry();
    }

    saveInquiry() {
        this.spinner = true;
        this.inquiryModel = { ...this.inquiryModel, ...this.inquiryFormGroup.value };
        this.service.create(this.inquiryModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;

                if (!this.isDraft) {
                    this.redirectLink();
                }
                else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {

            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['officer/inquiry/form7', this.id]);
            } else {
                this.router.navigate(['pages/inquiry/form7', this.id]);
            }
        }
    }

}
