import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { LocalService } from './../../../../services/local.service';
import { LocationService } from './../../../../services/location.service';
import { FormSectionTitleCounter } from './../../../custom-component/dynamic-forms/base/form-section-title-counter';
import { FormInput } from './../../../custom-component/dynamic-forms/base/form-input';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { FormTitle } from './../../../custom-component/dynamic-forms/base/form-title';
import { FormRadio } from './../../../custom-component/dynamic-forms/base/form-radio';
import { FormSectionTitle } from './../../../custom-component/dynamic-forms/base/form-section-title';
import { FormDate } from './../../../custom-component/dynamic-forms/base/form.date';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
export class InquiryForm6 {
    public forms: BaseForm<string | boolean>[] = []
    constructor(
        private lang: string,
        private lotService: LocationService,
        private localService: LocalService
    ) {
        const planLocation: BaseForm < any > [] =[
            new FormSectionTitleCounter({
                label: 'Location'
            }),
            new FormSectionTitle({
                label: 'a. Address'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region:',
                options$: this.lotService.getState(this.lang),
                required: true,
                columns: 'col-md-6'
            }),
            new FormSelect({
                key: 'district',
                label: 'District:',
                options$: this.lotService.getDistrict(this.lang),
                required: true,
                columns: 'col-md-6',
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township:',
                options$: this.lotService.getTownship(this.lang),
                required: true,
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
            new FormSectionTitle({
                label: 'b. Information on current administrator:'
            }),
            new FormInput({
                key: 'owner_name',
                label: 'Name',
                required: true,
                columns: 'col-md-6'
            }),
            new FormSelect({
                key: 'owner_type',
                label: 'Administrator Type',
                options$: this.localService.getType(this.lang),
                required: true,
                columns: 'col-md-6'
            }),
            new FormSelect({
                key: 'land_type',
                label: 'c. Type of land',
                options$: this.localService.getLandType(this.lang),
                required: true
            }),
            new FormSectionTitle({
                label: 'd. In case of sublease (i.e. if the current administrator is not the lessor), information on lessor:'
            }),
            new FormInput({
                key: 'lessor_name',
                label: 'Name:',
                columns: 'col-md-6'
            }),
            new FormSelect({
                key: 'lessor_type',
                label: 'Type',
                options$: this.localService.getType(this.lang),
                columns: 'col-md-6'
            }),
            new FormTitle({
                label: 'e. If subleasing state-owned land or buildings, please select which of the following apply to the lessor:'
            }),
            new FormRadio({
                key: 'lessor_type_apply',
                value: '1',
                label: 'A person who has previously obtained the right to use the state-owned and or buildings from the government department and government organization in accordance with the laws of the Union.',
                required: true
            }),
            new FormRadio({
                key: 'lessor_type_apply',
                value: '2',
                label: 'A person authorized to sub-lease or sub-license the state-owned land or building in accordance with the approval of the government department and government organization.',
                required: true
            }),
            new FormRadio({
                key: 'lessor_type_apply',
                value: '3',
                label: 'Not applicable',
                required: true
            }),
            new FormSectionTitle({
                label: 'f. Planned lease or use period:'
            }),
            new FormDate({
                key: 'plan_from',
                label: 'From Date',
                columns: 'col-md-6',
                required: true
            }),
            new FormDate({
                key: 'plan_to',
                label: 'To Date',
                columns: 'col-md-6',
                required: true
            })

        ];
        this.forms = [
            new FormSectionTitle({
                label: '6. Planned location(s) of the investment project'
            }),
            new BaseFormArray({
                key: 'plan_location',
                addMoreText: 'Add more locations',
                formArray: planLocation
            })
        ]
    }
}