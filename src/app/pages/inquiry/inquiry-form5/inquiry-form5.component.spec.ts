import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryForm5Component } from './inquiry-form5.component';

describe('InquiryForm5Component', () => {
  let component: InquiryForm5Component;
  let fixture: ComponentFixture<InquiryForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
