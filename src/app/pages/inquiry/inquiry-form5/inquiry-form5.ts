import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { LocalService } from './../../../../services/local.service';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormInput } from './../../../custom-component/dynamic-forms/base/form-input';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
export class InquiryForm5 {
    public forms: BaseForm<string | boolean>[] = []
    constructor(
        private lang: string,
        private localService: LocalService
    ) {
        const investment: BaseForm < any > [] =[
            new FormSectionTitle({
                label: '4. Expected total value of the investment project'
            }),
            new FormInput({
                key: 'e_kyat',
                label: 'Total Amount in Myanmar Kyat',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'e_usd',
                label: 'Equivalent Amount in USD',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormSectionTitle({
                label: '5. Expected total land area needed for investment project'
            }),
            new FormInput({
                key: 'area',
                label: 'Area of the land',
                required: true,
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormSelect({
                key: 'unit_id',
                label: 'Select unit of measurement',
                options$: this.localService.getUnit(this.lang),
                required: true,
                columns: 'col-md-6'
            }),
        ];
        this.forms = [
            new BaseFormGroup({
                key: 'investment',
                formGroup: investment
            })
        ]
    }

}