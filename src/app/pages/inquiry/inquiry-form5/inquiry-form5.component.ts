import { TranslateService } from '@ngx-translate/core';
import { Inquiry } from 'src/app/models/inquiry';
import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { FormControlService } from './../../../custom-component/dynamic-forms/form-control.service';
import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { Location } from '@angular/common';
import { BaseFormData, Submit } from 'src/app/core/form/_actions/form.actions';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { LocalService } from 'src/services/local.service';
import { AuthService } from 'src/services/auth.service';
import { InquiryForm5 } from './inquiry-form5';

@Component({
    selector: 'app-inquiry-form5',
    templateUrl: './inquiry-form5.component.html',
    styleUrls: ['./inquiry-form5.component.scss']
})
export class InquiryForm5Component implements OnInit {
    @Input() id: any;

    user: any = {};

    inquiryModel: Inquiry;
    inquiryFormGroup: FormGroup;

    submitted = false;
    loading = false;
    spinner = false;
    isDraft = false;

    isRevision: boolean = false;

    /* investment: BaseForm<any>[] = [
        new FormSectionTitle({
            label: '4. Expected total value of the investment project'
        }),
        new FormInput({
            key: 'e_kyat',
            label: 'Total Amount in Myanmar Kyat',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'e_usd',
            label: 'Equivalent Amount in USD',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormSectionTitle({
            label: '5. Expected total land area needed for investment project'
        }),
        new FormInput({
            key: 'area',
            label: 'Area of the land',
            required: true,
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormSelect({
            key: 'unit_id',
            label: 'Select unit of measurement',
            options$: this.localService.getUnit(),
            required: true,
            columns: 'col-md-6'
        }),
    ];
    inquiryForm: BaseForm<any>[] = [
        new BaseFormGroup({
            key: 'investment',
            formGroup: this.investment
        })
    ]; */

    baseForm: InquiryForm5;
    constructor(
        private http: HttpClient,
        private activatedRoute: ActivatedRoute,
        private service: InquiryService,
        private formCtlService: FormControlService,
        private router: Router,
        private localService: LocalService,
        private toast: ToastrService,
        private store: Store<AppState>,
        public location: Location,
        private translateService: TranslateService
    ) {
        this.loading = true;
        
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true: false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            this.baseForm = new InquiryForm5(null, this.localService);
            this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            if (this.id > 0) {
                this.service.show(this.id, { secure: true })
                    .subscribe(x => {
                        this.inquiryModel = x as Inquiry;
                        const lan = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                        this.translateService.use(lan);
                        this.baseForm = new InquiryForm5(lan, this.localService);
                        this.inquiryModel.investment = this.inquiryModel?.investment;
                        this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.inquiryModel);

                        const page = `inquiry/form5/${this.inquiryModel.id}`;
                        this.formCtlService.getComments$('Inquiry', this.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.id, type: 'Inquiry', page: page, comments }));
                        });
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    saveDraft() {
        this.isDraft = true;
        this.saveInquiry();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.inquiryFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.isDraft = false;
        this.saveInquiry();
    }

    saveInquiry() {
        this.spinner = true;
        this.inquiryModel = { ...this.inquiryModel, ...this.inquiryFormGroup.value };
        this.service.create(this.inquiryModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                if (!this.isDraft) {
                    this.redirectLink();
                }
                else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {

            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['officer/inquiry/form6', this.id]);
            } else {
                this.router.navigate(['pages/inquiry/form6', this.id]);
            }
        }
    }
}
