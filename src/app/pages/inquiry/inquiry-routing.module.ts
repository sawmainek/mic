import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InquirySuccessComponent } from './inquiry-success/inquiry-success.component';
import { InquiryForm7Component } from './inquiry-form7/inquiry-form7.component';
import { InquiryForm6Component } from './inquiry-form6/inquiry-form6.component';
import { InquiryForm5Component } from './inquiry-form5/inquiry-form5.component';
import { InquiryForm4Component } from './inquiry-form4/inquiry-form4.component';
import { InquiryForm3Component } from './inquiry-form3/inquiry-form3.component';
import { InquiryForm2Component } from './inquiry-form2/inquiry-form2.component';
import { InquiryForm1Component } from './inquiry-form1/inquiry-form1.component';
import { InquiryFormComponent } from './inquiry-form/inquiry-form.component';
import { InquiryLanguageComponent } from './inquiry-language/inquiry-language.component';
import { InquiryStateAndRegionComponent } from './inquiry-state-and-region/inquiry-state-and-region.component';
import { InquiryPaymentComponent } from './inquiry-payment/inquiry-payment.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'language', pathMatch: 'full' },
            { path: 'language', component: InquiryLanguageComponent },
            { path: 'language/:id', component: InquiryLanguageComponent },
            { path: 'form', component: InquiryFormComponent },
            { path: 'form/:id', component: InquiryFormComponent },
            { path: 'state-and-region/:id', component: InquiryStateAndRegionComponent },
            { path: 'form1', component: InquiryForm1Component },
            { path: 'form1/:id', component: InquiryForm1Component },
            { path: 'form2/:id', component: InquiryForm2Component },
            { path: 'form3/:id', component: InquiryForm3Component },
            { path: 'form4/:id', component: InquiryForm4Component },
            { path: 'form5/:id', component: InquiryForm5Component },
            { path: 'form6/:id', component: InquiryForm6Component },
            { path: 'form7/:id', component: InquiryForm7Component },
            { path: 'success/:id', component: InquirySuccessComponent },
            { path: 'payment/:id', component: InquiryPaymentComponent }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class InquiryRoutingModule { }
