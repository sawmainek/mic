import { saveAs } from 'file-saver';
import { Packer } from 'docx';
import { InquiryWordExport } from './inquiry-word.export';
import { Observable } from 'rxjs';

export class InquiryExport {
    docName: string = 'INQUIRY PREVIEW';
    fileName: string = 'inquiry-preview'
    inquiryForm: any[] = [];
    data: any;
    number: any = ["၁", "၂", "၃", "၄", "၅", "၆", "၇", "၈", "၉", "၁၀", "၁၁", "၁၂", "၁၃", "၁၄", "၁၅", "၁၆", "၁၇", "၁၈", "၁၉", "၂၀", "၂၁", "၂၂", "၂၃", "၂၄", "၂၅", "၂၆", "၂၇", "၂၈", "၂၉", "၃၀"]

    salary: any = {};
    total_year: any = [];

    constructor(inquiry: any, language: any) {
        console.log(inquiry)

        this.fileName = `${this.fileName}-${inquiry.id}.docx`;

        var request_information = [];
        request_information.push({
            'info': 'Does my proposed investment require the submission of a proposal to the Commission under section 36 of the Myanmar Investment Law?',
            'info_mm': 'မြန်မာနိုင်ငံရင်းနှီးမြှုပ်နှံမှုဥပဒေပုဒ်မ ၃၆ အရအဆိုပြုချက်တင်ပြရန်လိုအပ်သောလုပ်ငန်းဖြစ်ပါသလား။',
            'check': inquiry?.request_information?.option1 ? 'True' : 'False'
        });
        request_information.push({
            'info': 'Is my proposed investment likely to be submitted to the Pyidaungsu Hluttaw for approval prior to the issue of a Permit under section 46 of the Law?',
            'info_mm': 'မြန်မာနိုင်ငံရင်းနှီးမြှုပ်နှံမှုဥပဒေပုဒ်မ ၄၆ အရခွင့်ပြုမိန့်မထုတ်ပေးမီကော်မရှင်မှပြည်ထောင်စုလွှတ်တော် သို့ တင်ပြ အတည်ပြုချက်ရယူရန် လိုအပ်သောလုပ်ငန်းဖြစ်ပါသလား။',
            'check': inquiry?.request_information?.option2 ? 'True' : 'False'
        });
        request_information.push({
            'info': 'Does my proposed investment fall under investment activities restricted under section 42 of the Law and its related notification?',
            'info_mm': 'မြန်မာနိုင်ငံရင်းနှီးမြှုပ်နှံမှုဥပဒေပုဒ်မ ၄၂ နှင့် ဆက်စပ်အမိန့်ကြေညာချက်များအရကန့်သတ်ထားသော ရင်းနှီးမြှုပ်နှံမှု လုပ်ငန်း ပါဝင်သောလုပ်ငန်းဖြစ်ပါသလား။',
            'check': inquiry?.request_information?.option3 ? 'True' : 'False'
        });
        request_information.push({
            'info': 'Is my proposed investment in an investment promoted sector as publicly notified by the Commission under section 43 of the Law?',
            'info_mm': 'မြန်မာနိုင်ငံရင်းနှီးမြှုပ်နှံမှုဥပဒေပုဒ်မ ၄၃အရ ကော်မရှင်မှထုတ်ပြန်ကြေညာထားသော ဦးစားပေးမြှင့်တင်မည့် ကဏ္ဍများတွင် ပါဝင်သောလုပ်ငန်း ဖြစ်ပါသလား။',
            'check': inquiry?.request_information?.option4 ? 'True' : 'False'
        });
        request_information.push({
            'info': 'Does my proposed investment fall under investment activities prohibited under section 41 of the Law?',
            'info_mm': 'မြန်မာနိုင်ငံရင်းနှီးမြှုုပ်နှံမှုုဥပဒေပုဒ်မ ၄၁ အရတားမြစ်သည့်ရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်းများတွင်ပါဝင်သော လုပ်ငန်း ဖြစ်ပါသလား။',
            'check': inquiry?.request_information?.option5 ? 'True' : 'False'
        });
        request_information.push({
            'info': 'Other',
            'info_mm': 'သီးခြားသိရှိလိုသည့်အချက်များရှိပါကဖော်ပြရန်။',
            'check': inquiry?.request_information?.option6 ? inquiry?.request_information?.other : '-'
        });

        var actual_share_ratio = [
            {
                'myanmar': inquiry?.citizen_share_ratio,
                'foreign': inquiry?.foreigner_share_ratio,
                'government': inquiry?.government_share_ratio
            }
        ];

        var isic_section_id = '';
        if (Array.isArray(inquiry?.business_sector?.isic_section_id)) {
            inquiry?.business_sector?.isic_section_id.forEach(x => {
                isic_section_id += x + ', ';
            });
            if (isic_section_id.length > 2)
                isic_section_id = isic_section_id.substring(0, isic_section_id.length - 2);
        }
        else {
            isic_section_id = inquiry?.business_sector?.isic_section_id;
        }

        var isic_division_id = '';
        if (Array.isArray(inquiry?.business_sector?.isic_division_id)) {
            inquiry?.business_sector?.isic_division_id.forEach(x => {
                isic_division_id += x + ', ';
            });
            if (isic_division_id.length > 2)
                isic_division_id = isic_division_id.substring(0, isic_division_id.length - 2);
        }
        else {
            isic_division_id = inquiry?.business_sector?.isic_division_id;
        }

        var isic_group_id = '';
        if (Array.isArray(inquiry?.business_sector?.isic_group_id)) {
            inquiry?.business_sector?.isic_group_id.forEach(x => {
                isic_group_id += x + ', ';
            });
            if (isic_group_id.length > 2)
                isic_group_id = isic_group_id.substring(0, isic_group_id.length - 2);
        }
        else {
            isic_group_id = inquiry?.business_sector?.isic_group_id;
        }

        var isic_class_ids = '';
        if (Array.isArray(inquiry?.business_sector?.isic_class_ids)) {
            inquiry?.business_sector?.isic_class_ids.forEach(x => {
                isic_class_ids += x + ', ';
            });
            if (isic_class_ids.length > 2)
                isic_class_ids = isic_class_ids.substring(0, isic_class_ids.length - 2);
        }
        else {
            isic_class_ids = inquiry?.business_sector?.isic_class_ids;
        }

        var business_sector = [
            {
                'sections': isic_section_id,
                'divisions': isic_division_id,
                'groups': isic_group_id,
                'classes': isic_class_ids
            }
        ];

        var cpc_section_id = '';
        if (Array.isArray(inquiry?.business_cpc?.cpc_section_id)) {
            inquiry?.business_cpc?.cpc_section_id.forEach(x => {
                cpc_section_id += x + ', ';
            });
            if (isic_class_ids.length > 2)
                cpc_section_id = cpc_section_id.substring(0, cpc_section_id.length - 2);
        }
        else {
            cpc_section_id = inquiry?.business_cpc?.cpc_section_id;
        }

        var cpc_division_id = '';
        if (Array.isArray(inquiry?.business_cpc?.cpc_division_id)) {
            inquiry?.business_cpc?.cpc_division_id.forEach(x => {
                cpc_division_id += x + ', ';
            });
            if (isic_class_ids.length > 2)
                cpc_division_id = cpc_division_id.substring(0, cpc_division_id.length - 2);
        }
        else {
            cpc_division_id = inquiry?.business_cpc?.cpc_division_id;
        }

        var cpc_group_id = '';
        if (Array.isArray(inquiry?.business_cpc?.cpc_group_id)) {
            inquiry?.business_cpc?.cpc_group_id.forEach(x => {
                cpc_group_id += x + ', ';
            });
            if (isic_class_ids.length > 2)
                cpc_group_id = cpc_group_id.substring(0, cpc_group_id.length - 2);
        }
        else {
            cpc_group_id = inquiry?.business_cpc?.cpc_group_id;
        }

        var cpc_class_id = '';
        if (Array.isArray(inquiry?.business_cpc?.cpc_class_id)) {
            inquiry?.business_cpc?.cpc_class_id.forEach(x => {
                cpc_class_id += x + ', ';
            });
            if (cpc_class_id.length > 2)
                cpc_class_id = cpc_class_id.substring(0, cpc_class_id.length - 2);
        }
        else {
            cpc_class_id = inquiry?.business_cpc?.cpc_class_id;
        }

        var cpc_sub_class_ids = '';
        if (Array.isArray(inquiry?.business_cpc?.cpc_sub_class_ids)) {
            inquiry?.business_cpc?.cpc_sub_class_ids.forEach(x => {
                cpc_sub_class_ids += x + ', ';
            });
            if (cpc_sub_class_ids.length > 2)
                cpc_sub_class_ids = cpc_sub_class_ids.substring(0, cpc_sub_class_ids.length - 2);
        }
        else {
            cpc_sub_class_ids = inquiry?.business_cpc?.cpc_sub_class_ids;
        }

        var business_cpc = [
            {
                'sections': cpc_section_id,
                'divisions': cpc_division_id,
                'groups': cpc_group_id,
                'classes': cpc_class_id,
                'subclasses': cpc_sub_class_ids
            }
        ];

        if (language == 'en') {
            this.inquiryForm = [
                {
                    section: 'Inquiry (Form 1)',
                    form: [
                        {
                            label: 'Submit To:',
                            value: inquiry?.apply_to == 'Myanmar Investment Commision' ? inquiry?.apply_to : `${inquiry?.apply_to} (${inquiry?.inquiry_to_state})`,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'In line with Myanmar Investment Rule 28, I hereby summit an investment screening form to the Commission to obtain non-binding guidance on my proposed investment. I request to receive the following information:', type: 'Table', data: [
                                ['Request Information', 'Checked'],
                                ...request_information.map((x, index) => {
                                    return [x?.info, x?.check]
                                })
                            ]
                        }
                    ]
                },
                {
                    section: '1. Details on the investor',
                    form: [
                        {
                            label: 'a. Full Name',
                            value: inquiry?.investor?.name,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'b. Country of incorporation (if company) or citizenship (if individual)',
                            value: inquiry?.investor?.country,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'c. Passport number (foreign nationals), National Registration Card Number (Myanmar citizens) or company registration number (if company)',
                            value: inquiry?.investor?.personal_id_no,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'd. Address (If applicable, address in Myanmar)',
                            value: inquiry?.investor?.address,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'd. Address (If applicable, address in country of origin)',
                            value: inquiry?.investor?.address_other,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'e. State / Region',
                            value: inquiry?.investor?.state,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'f. Phone number',
                            value: inquiry?.investor?.phone,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'g. Fax number',
                            value: inquiry?.investor?.fax,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'h. E-mail address',
                            value: inquiry?.investor?.email,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '2. If the investor is not submitting the investment screening form by herself/himself, the applicant’s:',
                    form: [
                        {
                            label: 'a. Full Name',
                            value: inquiry?.applicant_investor?.name,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'b. Country of incorporation (if company) or citizenship (if individual)',
                            value: inquiry?.applicant_investor?.country,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'c. Passport number (foreign nationals), National Registration Card Number (Myanmar citizens) or company registration number (if company)',
                            value: inquiry?.applicant_investor?.personal_id_no,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'd. Address',
                            value: inquiry?.applicant_investor?.address,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'e. State / Region',
                            value: inquiry?.applicant_investor?.state,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'f. Phone number',
                            value: inquiry?.applicant_investor?.phone,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'g. Fax number',
                            value: inquiry?.applicant_investor?.fax,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'h. E-mail address',
                            value: inquiry?.applicant_investor?.email,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '3. Basic information on planned investment',
                    form: [
                        {
                            label: 'a. Planned or actual share ratio of enterprise', type: 'Table', data: [
                                ['Myanmar citizen share ratio', 'Foreign share ratio', 'Government department/ organization share ratio'],
                                ...actual_share_ratio.map((x, index) => {
                                    return [`${x?.myanmar}%`, `${x?.foreign}%`, `${x?.government}%`]
                                })
                            ]
                        },
                        {
                            label: 'b. Planned or actual type of company incorporated in Myanmar',
                            value: inquiry?.company_type,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'c. General business sector of proposed investment',
                            value: inquiry?.business_sector?.business_sector_id,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'd. Specific sector of the proposed investment', type: 'Table', data: [
                                ['ISIC Sections', 'ISIC Divisions', 'ISIC Groups', 'ISIC Classes'],
                                ...business_sector.map((x, index) => {
                                    return [x?.sections, x?.divisions, x?.groups, x?.classes]
                                })
                            ]
                        },
                        {
                            label: 'e. Optional: please name the specific product(s)/service(s) of the investment', type: 'Table', data: [
                                ['CPC Section', 'CPC Division', 'CPC Group', 'CPC Classes', 'CPC Subclasses'],
                                ...business_cpc.map((x, index) => {
                                    return [x?.sections, x?.divisions, x?.groups, x?.classes, x?.subclasses]
                                })
                            ]
                        },
                        {
                            label: 'f. Detailed description of the proposed investment',
                            value: inquiry?.description,
                            type: 'Text', option: {}
                        },
                    ]
                },
                {
                    section: '4. Expected total value of the investment project',
                    form: [
                        {
                            label: 'Total Amount in Myanmar Kyat',
                            value: inquiry?.investment?.e_kyat,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Equivalent Amount in USD',
                            value: inquiry?.investment?.e_usd,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '5. Expected total land area needed for investment project',
                    form: [
                        {
                            label: 'Area of the land',
                            value: inquiry?.investment?.area,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Select unit of measurement',
                            value: inquiry?.investment?.unit_id,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '6. Planned location(s) of the investment project',
                    form: []
                },
                {
                    form: [
                        {
                            label: '7. Is the proposed investment a cross-border investment?',
                            value: inquiry?.cross_border?.is_cross_border,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '8. Regarding the acquisition of land used or leased for the investment',
                    form: [
                        {
                            label: 'a. Has it been or is it likely to be acquired through expropriation by paying compensation, compulsory acquisition procedure or by agreement in advance of such expropriation or compulsory acquisition procedure in accordance with the laws of Myanmar?',
                            value: inquiry?.relocation?.has_compensation,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    form: [
                        {
                            label: '9. Will the use or lease of land for the investment cause or be likely to cause involuntary restrictions on land use and access to natural resources to any person having a legal right to such land use or access?',
                            value: inquiry?.legal_right?.is_legal_right,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    form: [
                        {
                            label: '10. Will the use or lease of land for the investment cause or be likely to cause conflict with the proposed investment activity due to litigation in good faith by a person or disputing over ownership of land?',
                            value: inquiry?.conflict?.is_conflict,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    form: [
                        {
                            label: '11. Will the use or lease of land for the investment adversely impact or be likely to adversely impact individuals by continue occupying such land scrutinized by a body which has right to scrutinize in applying to occupy or use land?',
                            value: inquiry?.impact?.is_adversely_impact,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    form: [
                        {
                            label: '12. If necessary, please upload related documents or information', type: 'Table', data: [
                                ['No', 'Title of document'],
                                ...inquiry?.documents.map((x, index) => {
                                    return [index + 1, x?.doc?.name]
                                })
                            ]
                        }
                    ]
                }
            ];

            inquiry?.plan_location?.forEach((x, index) => {
                var data;

                data = {
                    label: 'Location ' + (index + 1),
                    type: 'Text', option: {}
                },
                {
                    label: 'a. Address',
                    type: 'Text', option: {}
                },
                {
                    label: 'State / Region',
                    value: x?.state,
                    type: 'Text', option: {}
                },
                {
                    label: 'District',
                    value: x?.district,
                    type: 'Text', option: {}
                },
                {
                    label: 'Township',
                    value: x?.township,
                    type: 'Text', option: {}
                },
                {
                    label: 'b. Information on current administrator',
                    type: 'Text', option: {}
                },
                {
                    label: 'Name',
                    value: x?.owner_name,
                    type: 'Text', option: {}
                },
                {
                    label: 'Administrator Type',
                    value: x?.owner_type,
                    type: 'Text', option: {}
                },
                {
                    label: 'c. Type of land',
                    value: x?.land_type,
                    type: 'Text', option: {}
                },
                {
                    label: 'd. In case of sublease (i.e. if the current administrator is not the lessor), information on lessor',
                    type: 'Text', option: {}
                },
                {
                    label: 'Name',
                    value: x?.lessor_name,
                    type: 'Text', option: {}
                },
                {
                    label: 'Type',
                    value: x?.lessor_type,
                    type: 'Text', option: {}
                },
                {
                    label: 'e. If subleasing state-owned land or buildings, please select which of the following apply to the lessor:',
                    type: 'Text', option: {}
                },
                {
                    label: 'A person who has previously obtained the right to use the state-owned and or buildings from the government department and government organization in accordance with the laws of the Union.',
                    value: x?.lessor_type_apply == 1 ? ' True' : 'False',
                    type: 'Text', option: {}
                },
                {
                    label: 'A person authorized to sub-lease or sub-license the state-owned land or building in accordance with the approval of the government department and government organization.',
                    value: x?.lessor_type_apply == 2 ? ' True' : 'False',
                    type: 'Text', option: {}
                },
                {
                    label: 'Not applicable',
                    value: x?.lessor_type_apply == 3 ? ' True' : 'False',
                    type: 'Text', option: {}
                },
                {
                    label: 'f. Planned lease or use period:',
                    type: 'Text', option: {}
                },
                {
                    label: 'From Date',
                    value: x?.plan_from,
                    type: 'Text', option: {}
                },
                {
                    label: 'To Date',
                    value: x?.plan_to,
                    type: 'Text', option: {}
                };

                this.inquiryForm[6].form.push(data);

            });

            if (inquiry?.cross_border?.is_cross_border == 'Yes') {
                this.inquiryForm[7].form.push({
                    label: 'Please elaborate',
                    value: inquiry?.cross_border?.elaborate,
                    type: 'Text', option: {}
                });
            }

            if (inquiry?.relocation?.has_compensation == 'Yes') {
                this.inquiryForm[8].form.push({
                    label: 'b. If investor selected “Yes” above:',
                    type: 'Text', option: {}
                },
                    {
                        label: 'i. Will it cause the relocation of individuals permanently residing on such land?',
                        value: inquiry?.relocation?.is_relocation,
                        type: 'Text', option: {}
                    },
                    {
                        label: 'Indicate the number of persons impacted',
                        value: inquiry?.relocation?.number_relocation,
                        type: 'Text', option: {}
                    },
                    {
                        label: 'ii. Does it comprise an area of more than 100 acres?',
                        value: inquiry?.relocation?.is_more_than_100,
                        type: 'Text', option: {}
                    });
            }

            if (inquiry?.legal_right?.is_legal_right == 'Yes') {
                this.inquiryForm[9].form.push({
                    label: 'Indicate the number of acres of land this applies to',
                    value: inquiry?.legal_right?.number_legal_right,
                    type: 'Text', option: {}
                });
            }

            if (inquiry?.conflict?.is_conflict == 'Yes') {
                this.inquiryForm[10].form.push({
                    label: 'Indicate the number of acres of land this applies to',
                    value: inquiry?.legal_right?.number_conflict,
                    type: 'Text', option: {}
                });
            }

            if (inquiry?.impact?.is_adversely_impact == 'Yes') {
                this.inquiryForm[11].form.push({
                    label: 'Indicate the number of persons impacted',
                    value: inquiry?.legal_right?.number_adversely_impact,
                    type: 'Text', option: {}
                });
            }
        }
        else {
            this.inquiryForm = [
                {
                    section: 'စုံစမ်းမေးမြန်းခြင်းလျှောက်ထားလွှာပုံစံ',
                    form: [
                        {
                            label: 'သို့:',
                            value: inquiry?.apply_to == 'Myanmar Investment Commision' ? 'မြန်မာနိုင်ငံရင်းနှီးမြှုပ်နှံမှုကော်မရှင်' : `တိုင်းဒေသကြီး / ပြည်နယ် ရင်းနှီးမြှုပ်နှံမှုကော်မတီ (${inquiry?.inquiry_to_state})`,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ကျွန်တော်/ကျွန်မ၏ အဆိုပြုရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်းသည် အောက်ပါတို့အနက် မည်သည့်အမျိုးအစားတွင်ပါဝင် နေကြောင်းသိရှိရန်အလို့ငှာ မြန်မာနိုင်ငံရင်းနှီးမြှုပ်နှံမှုနည်းဥပဒေ ၂၈ အရစည်းနှောင်မှုမရှိသည့်လမ်းညွှန်ချက် ရယူရန်အတွက်အောက်ပါအချက်အလက်များကိုသိရှိလိုပါသဖြင့် စုံစမ်းမေးမြန်းအပ်ပါသည်-', type: 'Table', data: [
                                ['Request Information', 'Checked'],
                                ...request_information.map((x, index) => {
                                    return [x?.info_mm, x?.check]
                                })
                            ]
                        }
                    ]
                },
                {
                    section: '၁။ ရင်းနှီးမြှုပ်နှံသူ၏ အချက်အလက်များ',
                    form: [
                        {
                            label: 'က။ အမည်အပြည့်အစုံ',
                            value: inquiry?.investor?.name,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ခ။ လုပ်ငန်းတည်ရှိရာ နိုင်ငံ(ကုမ္ပဏီဖြစ်ပါက) (သို့မဟုတ်) နိုင်ငံသား (လူပုဂ္ဂိုလ်ဖြစ်ပါက) ',
                            value: inquiry?.investor?.country,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဂ။ နိုင်ငံကူးလက်မှတ် အမှတ်(နိုင်ငံခြားသားဖြစ်ပါက) သို့မဟုတ် နိုင်ငံသားစိစစ်ရေးကတ် အမှတ်(မြန်မာနိုင်ငံသားဖြစ်ပါက) သို့မဟုတ် ကုမ္ပဏီမှတ်ပုံတင် လက်မှတ် အမှတ် (ကုမ္ပဏီဖြစ်ပါက) တင်ပြပါ။',
                            value: inquiry?.investor?.personal_id_no,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဃ။ လိပ်စာ (မြန်မာနိုင်ငံတွင် နေထိုင်သည့်လိပ်စာရှိပါကဖော်ပြပါ။)',
                            value: inquiry?.investor?.address,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဃ။ လိပ်စာ (မိခင်နိုင်ငံတွင်နေထိုင်သည့်လိပ်စာရှိပါကဖော်ပြပါ။)',
                            value: inquiry?.investor?.address_other,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'င။ ပြည်နယ်/တိုင်း',
                            value: inquiry?.investor?.state,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'စ။ ဖုန်းနံပါတ်',
                            value: inquiry?.investor?.phone,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဆ။ ဖက်စ်နံပါတ်',
                            value: inquiry?.investor?.fax,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဇ။ အီးမေးလ်လိပ်စာ',
                            value: inquiry?.investor?.email,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '၂။ ရင်းနှီးမြှုပ်နှံသူကိုယ်တိုင်လျှောက်ထားခြင်းမဟုတ်ပါကလျှောက်ထားသူ၏-',
                    form: [
                        {
                            label: 'က။ အမည်အပြည့်အစုံ',
                            value: inquiry?.applicant_investor?.name,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ခ။ လုပ်ငန်းတည်ရှိရာ နိုင်ငံ(ကုမ္ပဏီဖြစ်ပါက) (သို့မဟုတ်) နိုင်ငံသား (လူပုဂ္ဂိုလ်ဖြစ်ပါက)',
                            value: inquiry?.applicant_investor?.country,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဂ။ နိုင်ငံကူးလက်မှတ် အမှတ်(နိုင်ငံခြားသားဖြစ်ပါက) သို့မဟုတ် နိုင်ငံသားစိစစ်ရေးကတ် အမှတ်(မြန်မာနိုင်ငံသားဖြစ်ပါက) သို့မဟုတ် ကုမ္ပဏီမှတ်ပုံတင် လက်မှတ် အမှတ် (ကုမ္ပဏီဖြစ်ပါက) တင်ပြပါ။',
                            value: inquiry?.applicant_investor?.personal_id_no,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဃ။ လိပ်စာ',
                            value: inquiry?.applicant_investor?.address,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'င။ ပြည်နယ်/တိုင်း',
                            value: inquiry?.applicant_investor?.state,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'စ။ ဖုန်းနံပါတ်',
                            value: inquiry?.applicant_investor?.phone,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဆ။ ဖက်စ်နံပါတ်',
                            value: inquiry?.applicant_investor?.fax,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဇ။ အီးမေးလ်လိပ်စာ',
                            value: inquiry?.applicant_investor?.email,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '၃။ဆောင်ရွက်မည့် စီးပွားရေးအဖွဲ့အစည်းနှင့် သက်ဆိုင်သောအချက်အလက်များ',
                    form: [
                        {
                            label: 'က။ ဆောင်ရွက်မည့် ရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်း၏ လျာထား (သို့မဟုတ်) အမှန်တကယ် ရှယ်ယာပါဝင်မှု အချိုး', type: 'Table', data: [
                                ['မြန်မာနိုင်ငံသားအစုရှယ်ယာပိုင်ဆိုင်မှုအချိုး', 'နိုင်ငံခြားသားအစုရှယ်ယာပိုင်ဆိုင်မှုအချိုး', 'အစိုးရဌာန / အဖွဲ့အစည်း၏ အစုရှယ်ယာပိုင်ဆိုင်မှုအချိုး'],
                                ...actual_share_ratio.map((x, index) => {
                                    return [`${x?.myanmar}%`, `${x?.foreign}%`, `${x?.government}%`]
                                })
                            ]
                        },
                        {
                            label: 'ခ။ မြန်မာနိုင်ငံတွင် ဖွဲ့စည်းတည်ထောင်မည့် သို့မဟုတ် ဖွဲ့စည်းတည်ထောင်ထားသည့် ကုမ္ပဏီအမျိုးအစား',
                            value: inquiry?.company_type,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဂ။ အဆိုပြု ရင်းနှီးမြှုပ်နှံမှု လုပ်ငန်း၏ အထွေထွေစီးပွားရေးလုပ်ငန်းကဏ္ဍအားဖော်ပြပါ။',
                            value: inquiry?.business_sector?.business_sector_id,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ဃ။ ဆောင်ရွက်မည့် ရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်းကဏ္ဍအမည်အားဖော်ပြပါ။', type: 'Table', data: [
                                ['ISIC ကဏ္ဍများ', 'ISIC ဌာနခွဲများ', 'ISIC အုပ်စုများ', ' ISIC အမျိုးအစားများ'],
                                ...business_sector.map((x, index) => {
                                    return [x?.sections, x?.divisions, x?.groups, x?.classes]
                                })
                            ]
                        },
                        {
                            label: '(င) ဆောင်ရွက်မည့် ရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်းမှ ထုတ်လုပ်မည့် ထုတ်ကုန်(များ) သို့မဟုတ် ဝန်ဆောင်မှု (များ) အားသီးခြား code ဖြင့်ထပ်မံဖော်ပြလိုပါက-', type: 'Table', data: [
                                ['CPC ကဏ္ဍ', 'CPC ဌာနခွဲ', 'CPC အုပ်စုများ', 'CPC အမျိုးအစားများ', 'CPC အမျိုးအစားခွဲ'],
                                ...business_cpc.map((x, index) => {
                                    return [x?.sections, x?.divisions, x?.groups, x?.classes, x?.subclasses]
                                })
                            ]
                        },
                        {
                            label: 'စ။ ဆောင်ရွက်မည့် ရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်း၏ အသေးစိတ်ဖော်ပြချက်များ',
                            value: inquiry?.description,
                            type: 'Text', option: {}
                        },
                    ]
                },
                {
                    section: '၄။ ရင်းနှီး မြှုပ်နှံမှု စီမံကိန်း၏ စုစုပေါင်းခန့်မှန်းတန်ဖိုး',
                    form: [
                        {
                            label: 'စုစုပေါင်းပမာဏ (မြန်မာကျပ်ငွေ)',
                            value: inquiry?.investment?.e_kyat,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ညီမျှသည့်ခန့်မှန်းတန်ဖိုး (အမေရိကန်ဒေါ်လာ)',
                            value: inquiry?.investment?.e_usd,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '၅။ ရင်းနှီးမြှုပ်နှံမှုစီမံကိန်းအတွက် လိုအပ်သော စုစုပေါင်းခန့်မှန်းမြေဧရိယာ',
                    form: [
                        {
                            label: 'မြေဧရိယာ',
                            value: inquiry?.investment?.area,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'အတိုင်းအတာယူနစ်အားရွေးချယ်ပါ။',
                            value: inquiry?.investment?.unit_id,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '၆။ ဆောင်ရွက်မည့် ရင်းနှီး မြှုပ်နှံမှု စီမံကိန်း၏ တည်နေရာ (များ)',
                    form: []
                },
                {
                    form: [
                        {
                            label: '၇။ အဆိုပြု ရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်းသည်နယ်စပ်ဖြတ်ကျော် ရင်းနှီးမြှုပ်နှံခြင်း ဖြစ်ပါသလား။',
                            value: inquiry?.cross_border?.is_cross_border,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    section: '၈။ ရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်းအတွက် အသုံးပြုမည့်မြေသို့မဟုတ် ငှားရမ်းမည့်မြေသည်သိမ်းယူရန်လိုအပ်ခြင်းနှင့် စပ်လျဉ်း၍-',
                    form: [
                        {
                            label: 'က။ နိုင်ငံတော်၏ ဥပဒေများနှင့် အညီလျော်ကြေးပေး၍သိမ်းယူခြင်းသို့မဟုတ် နိုင်ငံတော်အတွင်းရှိမြေသိမ်းယူခြင်း လုပ်ထုံးလုပ်နည်းများဖြင့် ရယူခြင်း (သို့မဟုတ်) နိုင်ငံတော်အတွင်းရှိမြေသိမ်းယူခြင်းလုပ်ထုံး လုပ်နည်းများဖြင့် ရယူရန် ကြိုတင်သဘောတူညီချက်ဖြင့် ပြုလုပ်ခြင်းများဖြစ်ပေါ်နိုင်ခြင်း (သို့မဟုတ်) ဖြစ်ပေါ်ခြေရှိပါသလား။',
                            value: inquiry?.relocation?.has_compensation,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    form: [
                        {
                            label: '၉။ ရင်းနှီးမြှုပ်နှံမှုအတွက် မြေငှားရမ်းခြင်းသို့မဟုတ် အသုံးပြုခြင်းဖြင့် ဥပဒေအရအသုံးပြုခွင့်ရှိသောပုဂ္ဂိုလ်၏ မြေအသုံးပြုခွင့် နှင့် သဘာဝအရင်းအမြစ်ရယူခွင့်အားအတင်းအကြပ်ပိတ်ပင်ခြင်းများ ဖြစ်ပေါ်နိုင်ခြင်း (သို့မဟုတ်) ဖြစ် ပေါ်နိုင်ခြေရှိပါသလား။',
                            value: inquiry?.legal_right?.is_legal_right,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    form: [
                        {
                            label: '၁၀။ ရင်းနှီးမြှုပ်နှံမှုအတွက် မြေငှားရမ်းခြင်းသို့မဟုတ် အသုံးပြုခြင်းအားရယူရာတွင် ပုဂ္ဂိုလ်တစ်ဦးကရိုးဖြောင့်သော သဘောဖြင့် တရားစွဲဆိုတောင်းဆိုခြင်း (သို့မဟုတ်) မြေပိုင်ဆိုင်မှုနှင့်ပတ်သက်ပြီးအငြင်းပွားမှုဖြစ်နေခြင်းတို့ကြောင့် အဆိုပြုရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်းအား ပဋိပက္ခဖြစ်ပေါ်နိုင်ခြင်း (သို့မဟုတ်) ဖြစ်ပေါ်နိုင်ခြေရှိပါသလား။',
                            value: inquiry?.conflict?.is_conflict,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    form: [
                        {
                            label: '၁၁။ ရင်းနှီးမြှုပ်နှံမှုအတွက် မြေငှားရမ်းခြင်းသို့မဟုတ် အသုံးပြုခွင့်ကြောင့် လျှောက်ထားချိန်တွင်စိစစ်ခွင့်ရှိသည့် အဖွဲ့အစည်းကစိစစ်ထားသည့် မြေတွင်နေထိုင်သူများအားယင်းမြေတွင်ဆက်လက်နေထိုင်ခြင်းဖြင့် အခြားသောဆိုးကျိုးသက်ရောက်စေနိုင်ခြင်းရှိပါသလား။',
                            value: inquiry?.impact?.is_adversely_impact,
                            type: 'Text', option: {}
                        }
                    ]
                },
                {
                    form: [
                        {
                            label: '၁၂။ အခြားစာရွက်စာတမ်း၊ သတင်းအချက်အလက်များရှိပါက ဖော်ပြပါ။', type: 'Table', data: [
                                ['No', 'Title of document'],
                                ...inquiry?.documents.map((x, index) => {
                                    return [index + 1, x?.doc?.name]
                                })
                            ]
                        }
                    ]
                }
            ];

            var number = ["၁", "၂", "၃", "၄", "၅", "၆", "၇", "၈", "၉", "၁၀", "၁၁", "၁၂", "၁၃", "၁၄", "၁၅", "၁၆", "၁၇", "၁၈", "၁၉", "၂၀", "၂၁", "၂၂", "၂၃", "၂၄", "၂၅", "၂၆", "၂၇", "၂၈", "၂၉", "၃၀", "၃၁", "၃၂", "၃၃", "၃၄", "၃၅", "၃၆", "၃၇", "၃၈", "၃၉", "၄၀"];

            inquiry?.plan_location?.forEach((x, index) => {
                var data;

                data = {
                    label: 'တည်နေရာ ' + number[index],
                    type: 'Text', option: {}
                },
                {
                    label: 'က။ လိပ်စာ',
                    type: 'Text', option: {}
                },
                {
                    label: 'ပြည်နယ်/ တိုင်း',
                    value: x?.state,
                    type: 'Text', option: {}
                },
                {
                    label: 'ခရိုင်',
                    value: x?.district,
                    type: 'Text', option: {}
                },
                {
                    label: 'မြို့နယ်',
                    value: x?.township,
                    type: 'Text', option: {}
                },
                {
                    label: 'ခ။ လက်ရှိစီမံခန့်ခွဲခွင့် ရှိသူ၏အချက်အလက်များ-',
                    type: 'Text', option: {}
                },
                {
                    label: 'အမည်',
                    value: x?.owner_name,
                    type: 'Text', option: {}
                },
                {
                    label: 'စီမံခန့်ခွဲသူအမျိုးအစားရွေးချယ်ပါ။',
                    value: x?.owner_type,
                    type: 'Text', option: {}
                },
                {
                    label: 'ဂ။ မြေအမျိုးအစား',
                    value: x?.land_type,
                    type: 'Text', option: {}
                },
                {
                    label: '(ဃ) တဆင့်ငှားရမ်းခြင်း ဖြစ်ပါက- (လက်ရှိစီမံခန့်ခွဲခွင့် ရှိသူသည် အငှားချထားသူမဟုတ်ပါက)၊ အငှားချထားသူ (lessor) ၏ အချက်အလက်များ-',
                    type: 'Text', option: {}
                },
                {
                    label: 'အမည်',
                    value: x?.lessor_name,
                    type: 'Text', option: {}
                },
                {
                    label: 'အမျိုးအစား',
                    value: x?.lessor_type,
                    type: 'Text', option: {}
                },
                {
                    label: '(င)အစိုးရပိုင်အဆောက်အအုံ (သို့) မြေအားထပ်ဆင့်ငှားရမ်းခြင်းဖြစ်ပါက အငှားချထားသူသည် အောက်ဖော်ပြပါ အချက်များမှ မည်သည့်အချက်နှင့် ကိုက်ညီသနည်း။',
                    type: 'Text', option: {}
                },
                {
                    label: 'နိုင်ငံတော်၏ ဥပဒေများနှင့်အညီအစိုးရဌာန၊ အစိုးရအဖွဲ့အစည်းထံမှ နိုင်ငံတော်ပိုင်မြေသို့မဟုတ်အဆောက်အအုံအသုံးပြုခွင့်အားယခင်ကပင် ရရှိထားသောပုဂ္ဂိုလ်၊',
                    value: x?.lessor_type_apply == 1 ? ' ဟုတ်ပါသည်။' : 'မဟုတ်ပါ။',
                    type: 'Text', option: {}
                },
                {
                    label: 'အစိုးရဌာန၊ အစိုးရအဖွဲ့အစည်း၏ ခွင့်ပြုချက်နှင့်အညီ နိုင်ငံတော်ပိုင်မြေသို့မဟုတ်အဆောက်အအုံအားတစ်ဆင့်ငှားယူရန် သို့မဟုတ် တစ်ဆင့်လိုင်စင်ရယူရန် အခွင့်ရှိသည့် ပုဂ္ဂိုလ်။',
                    value: x?.lessor_type_apply == 2 ? ' ဟုတ်ပါသည်။' : 'မဟုတ်ပါ။',
                    type: 'Text', option: {}
                },
                {
                    label: 'မကိုက်ညီပါ။',
                    value: x?.lessor_type_apply == 3 ? ' ဟုတ်ပါသည်။' : 'မဟုတ်ပါ။',
                    type: 'Text', option: {}
                },
                {
                    label: '(စ) ငှားရမ်းမည့် (သို့) အသုံးပြုမည့် ကာလ',
                    type: 'Text', option: {}
                },
                {
                    label: 'စတင်သည့်နေ့',
                    value: x?.plan_from,
                    type: 'Text', option: {}
                },
                {
                    label: 'ပြီးဆုံးသည့်နေ့',
                    value: x?.plan_to,
                    type: 'Text', option: {}
                };

                this.inquiryForm[6].form.push(data);

            });

            if (inquiry?.cross_border?.is_cross_border == 'Yes') {
                this.inquiryForm[7].form.push({
                    label: 'ရှင်းလင်းဖော်ပြပါ။',
                    value: inquiry?.cross_border?.elaborate,
                    type: 'Text', option: {}
                });
            }

            if (inquiry?.relocation?.has_compensation == 'Yes') {
                this.inquiryForm[8].form.push(
                    {
                        label: '(ခ) ရှိပါသည် ဟုဖြေဆိုပါက-',
                        type: 'Text', option: {}
                    },
                    {
                        label: '(ကက) ယင်းမြေတွင် အမြဲတမ်းနေထိုင်သူများအားပြန်လည်နေရာချထားပေးရန် လိုအပ်ပါသလား။',
                        value: inquiry?.relocation?.is_relocation == 'Yes' ? ' ဟုတ်ပါသည်။' : 'မဟုတ်ပါ။',
                        type: 'Text', option: {}
                    },
                    {
                        label: '(ခခ) မြေဧက ၁၀၀ အထက်အား ပြန်လည်နေရာချထားပေးရန်လိုအပ်ပါသလား။',
                        value: inquiry?.relocation?.number_relocation == 'Yes' ? ' ဟုတ်ပါသည်။' : 'မဟုတ်ပါ။',
                        type: 'Text', option: {}
                    },
                    {
                        label: '(ခခ) မြေဧက ၁၀၀ အထက်အား ပြန်လည်နေရာချထားပေးရန်လိုအပ်ပါသလား။',
                        value: inquiry?.relocation?.is_more_than_100 == 'Yes' ? ' ဟုတ်ပါသည်။' : 'မဟုတ်ပါ။',
                        type: 'Text', option: {}
                    });
            }

            if (inquiry?.legal_right?.is_legal_right == 'Yes') {
                this.inquiryForm[9].form.push({
                    label: 'ရယူမည့်မြေဧကအားဖော်ပြပါ။',
                    value: inquiry?.legal_right?.number_legal_right,
                    type: 'Text', option: {}
                });
            }

            if (inquiry?.conflict?.is_conflict == 'Yes') {
                this.inquiryForm[10].form.push({
                    label: 'ရယူမည့်မြေဧကအားဖော်ပြပါ။',
                    value: inquiry?.legal_right?.number_conflict,
                    type: 'Text', option: {}
                });
            }

            if (inquiry?.impact?.is_adversely_impact == 'Yes') {
                this.inquiryForm[11].form.push({
                    label: 'ရှိပါကဆိုးကျိုးသက်ရောက်စေမည့်လူဦးရေအားဖော်ပြပါ။',
                    value: inquiry?.legal_right?.number_adversely_impact,
                    type: 'Text', option: {}
                });
            }
        }
    }

    public download(): Observable<any> {
        return Observable.create((observer) => {
            const documentCreator = new InquiryWordExport();
            const doc = documentCreator.create(this.inquiryForm, this.docName);
            Packer.toBlob(doc).then(blob => {
                try {
                    saveAs(blob, this.fileName);
                    observer.next("Document created successfully");
                    observer.complete();
                } catch (err) {
                    console.log(err)
                    observer.error(err);
                }
            });
        });

    }
}
