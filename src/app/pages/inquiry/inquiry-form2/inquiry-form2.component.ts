import { TranslateService } from '@ngx-translate/core';
import { currentUser } from './../../../core/auth/_selectors/auth.selectors';
import { LocationService } from './../../../../services/location.service';
import { Store, select } from '@ngrx/store';
import { BaseFormData, Submit } from './../../../core/form/_actions/form.actions';
import { Observable } from 'rxjs';
import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { FormInput } from './../../../custom-component/dynamic-forms/base/form-input';
import { FormControlService } from './../../../custom-component/dynamic-forms/form-control.service';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { Inquiry } from 'src/app/models/inquiry';
import { Location } from '@angular/common';
import { AppState } from 'src/app/core';
import { ToastrService } from 'ngx-toastr';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { InquiryForm2 } from './inquiry-form2';

@Component({
    selector: 'app-inquiry-form2',
    templateUrl: './inquiry-form2.component.html',
    styleUrls: ['./inquiry-form2.component.scss']
})
export class InquiryForm2Component implements OnInit {
    @Input() id: any;
    inquiryModel: Inquiry;
    inquiryFormGroup: FormGroup;

    user: any;

    loading = false;
    spinner = false;
    isDraft = false;
    submitted = false;

    isRevision: boolean = false;

    /* investor: BaseForm<any>[] = [
        new FormSectionTitle({
            label: '1. Details on the investor'
        }),
        new FormInput({
            key: 'name',
            label: 'a. Full Name',
            required: true
        }),
        new FormSelect({
            key: 'country',
            placeholder: 'Select country',
            label: 'b. Country of incorporation (if company) or citizenship (if individual)',
            options$: this.lotService.getCountry(),
            required: true
        }),
        new FormInput({
            key: 'personal_id_no',
            label: 'c. Passport number (foreign nationals), National Registration Card Number (Myanmar citizens) or company registration number (if company)',
            required: true
        }),
        new FormTitle({
            label: 'd.  Address'
        }),
        new FormInput({
            key: 'address',
            label: 'If applicable, address in Myanmar'
        }),
        new FormInput({
            key: 'address_other',
            label: 'If applicable, address in country of origin'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            required: true,
            columns: 'col-md-6',
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            required: true,
            columns: 'col-md-6',
            filter: {
                parent: 'state',
                key: 'state'
            },
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            required: true,
            filter: {
                parent: 'district',
                key: 'district'
            },
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'phone',
            label: 'e.  Phone number',
            type: 'number',
            required: true,
            validators: [Validators.minLength(6)],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'fax',
            label: 'f. Fax number',
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'g. E-mail address',
            validators: [Validators.email]
        }),
    ]
    inquiryForm: BaseForm<any>[] = [
        new BaseFormGroup({
            key: 'investor',
            formGroup: this.investor
        })
    ] */
    baseForm: InquiryForm2;
    constructor(
        private activatedRoute: ActivatedRoute,
        public service: InquiryService,
        private lotService: LocationService,
        private formCtlService: FormControlService,
        private translateService: TranslateService,
        private router: Router,
        private store: Store<AppState>,
        public location: Location,
        private toast: ToastrService,
    ) {
        this.loading = true;
    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.baseForm = new InquiryForm2(null, this.lotService);
            this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            if (this.id) {
                this.service.show(this.id, { secure: true })
                    .subscribe((x) => {
                        this.inquiryModel = x as Inquiry;
                        const lan = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                        this.translateService.use(lan);
                        this.baseForm = new InquiryForm2(lan, this.lotService);
                        this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.inquiryModel);

                        const page = `inquiry/form2/${this.inquiryModel.id}`;
                        this.formCtlService.getComments$('Inquiry', this.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.id, type: 'Inquiry', page: page, comments }));
                        });

                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.isDraft = true;
        this.saveInquiry();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.inquiryFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.isDraft = false;
        this.saveInquiry();
    }

    saveInquiry() {
        this.spinner = true;
        this.inquiryModel = { ...this.inquiryModel, ...this.inquiryFormGroup.value };
        this.service.create(this.inquiryModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;

                if (!this.isDraft) {
                    this.redirectLink();
                }
                else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['officer/inquiry/form3', this.id]);
            } else {
                this.router.navigate(['pages/inquiry/form3', this.id]);
            }
        }
    }

}
