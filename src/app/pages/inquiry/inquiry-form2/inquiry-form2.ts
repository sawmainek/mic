import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { LocationService } from './../../../../services/location.service';
import { FormSectionTitle } from './../../../custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { FormInput } from './../../../custom-component/dynamic-forms/base/form-input';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { Validators } from '@angular/forms';
export class InquiryForm2 {
    public forms: BaseForm<string | boolean>[] = []
    constructor(
        private lang: string,
        private lotService: LocationService
    ) {
        const investor: BaseForm < any > [] =[
            new FormSectionTitle({
                label: '1. Details on the investor'
            }),
            new FormInput({
                key: 'name',
                label: 'a. Full Name',
                required: true
            }),
            new FormSelect({
                key: 'country',
                label: 'b. Country of incorporation (if company) or citizenship (if individual)',
                options$: this.lotService.getCountry(),
                required: true
            }),
            new FormInput({
                key: 'personal_id_no',
                label: 'c. Passport number (foreign nationals), National Registration Card Number (Myanmar citizens) or company registration number (if company)',
                required: true
            }),
            new FormTitle({
                label: 'd.  Address'
            }),
            new FormInput({
                key: 'address',
                label: 'If applicable, address in Myanmar'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region:',
                options$: this.lotService.getState(this.lang),
                columns: 'col-md-4'
            }),
            new FormSelect({
                key: 'district',
                label: 'District:',
                options$: this.lotService.getDistrict(this.lang),
                columns: 'col-md-4',
                filter: {
                    parent: 'state',
                    key: 'state'
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: this.lotService.getTownship(this.lang),
                filter: {
                    parent: 'district',
                    key: 'district'
                }
            }),
            new FormInput({
                key: 'address_other',
                label: 'If applicable, address in country of origin'
            }),            
            new FormInput({
                key: 'phone',
                label: 'e.  Phone number',
                type: 'number',
                required: true,
                validators: [Validators.minLength(6)],
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'fax',
                label: 'f. Fax number',
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'g. E-mail address',
                validators: [Validators.email]
            }),
        ]
        this.forms = [
            new BaseFormGroup({
                key: 'investor',
                formGroup: investor
            })
        ]
    }
}