import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryForm2Component } from './inquiry-form2.component';

describe('InquiryForm2Component', () => {
  let component: InquiryForm2Component;
  let fixture: ComponentFixture<InquiryForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
