import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryForm1Component } from './inquiry-form1.component';

describe('InquiryForm1Component', () => {
  let component: InquiryForm1Component;
  let fixture: ComponentFixture<InquiryForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
