import { TranslateService } from '@ngx-translate/core';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { Store, select } from '@ngrx/store';
import { BaseFormData, Submit } from './../../../core/form/_actions/form.actions';
import { FormControlService } from './../../../custom-component/dynamic-forms/form-control.service';
import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { FormGroup } from '@angular/forms';
import { Inquiry } from 'src/app/models/inquiry';
import { Location } from '@angular/common';
import { AppState } from 'src/app/core';
import { ToastrService } from 'ngx-toastr';
import { InquiryForm1 } from './inquiry-form1';

@Component({
    selector: 'app-inquiry-form1',
    templateUrl: './inquiry-form1.component.html',
    styleUrls: ['./inquiry-form1.component.scss']
})
export class InquiryForm1Component implements OnInit {
    @Input() id: any;
    isRevision: boolean = false;
    isDraft: boolean = false;

    user: any = {};

    inquiryFormGroup: FormGroup;
    inquiryModel: Inquiry;

    loading = false;
    spinner = false;

    baseForm: InquiryForm1;
    constructor(
        private router: Router,
        private service: InquiryService,
        private activatedRoute: ActivatedRoute,
        private formCtlService: FormControlService,
        private translateService: TranslateService,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
    ) { }

    ngOnInit(): void {
        this.loading = true;
        this.getCurrentUser();
        
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true: false;

        this.baseForm = new InquiryForm1();
        this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
        

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || this.id || null;
            if (this.id) {
                this.service.show(this.id).subscribe((res: any) => {
                    this.inquiryModel = res || {};
                    const lan = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                    this.translateService.use(lan);
                    this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.inquiryModel);
                    const page = `inquiry/form1/${this.inquiryModel.id}`;
                    this.formCtlService.getComments$('Inquiry', this.id).subscribe(comments => {
                        this.store.dispatch(new BaseFormData({ id: this.id, type: 'Inquiry', page: page, comments }));
                    });

                    this.loading = false;
                })
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.isDraft = true;
        this.saveInquiry();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.inquiryFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.isDraft = false;
        this.saveInquiry();
    }

    saveInquiry() {
        this.spinner = true;
        this.inquiryModel = this.inquiryModel as Inquiry;
        this.inquiryModel = { ...this.inquiryModel, ...this.inquiryFormGroup.value };

        if (!this.inquiryModel.request_information.option1 &&
            !this.inquiryModel.request_information.option2 &&
            !this.inquiryModel.request_information.option3 &&
            !this.inquiryModel.request_information.option4 &&
            !this.inquiryModel.request_information.option5 &&
            !this.inquiryModel.request_information.option6) {
            this.toast.error('Please complete all required field(s).');
            this.spinner = false;
            return;
        }
        else {
            this.service.create(this.inquiryModel, { secure: true })
                .subscribe(x => {
                    this.spinner = false;

                    if (!this.isDraft) {
                        this.redirectLink();
                    }
                    else {
                        this.toast.success('Saved successfully.');
                    }
                }, err => {
                    this.spinner = false;
                    console.log('error ' + JSON.stringify(err));
                });
        }

    }

    redirectLink() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {
            
            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['officer/inquiry/form2', this.id]);
            } else {
                this.router.navigate(['pages/inquiry/form2', this.id]);
            }
        }
    }
}
