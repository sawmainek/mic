import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { FormTextArea } from './../../../custom-component/dynamic-forms/base/form-textarea';
import { FormCheckBox } from './../../../custom-component/dynamic-forms/base/form-checkbox';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
export class InquiryForm1 {
    public forms: BaseForm<string|boolean>[] = []
    constructor() {
        this.forms = [
            new BaseFormGroup({
                key: 'request_information',
                formGroup: [
                    new FormTitle({
                        label: "In line with Myanmar Investment Rule 28, I hereby summit an investment screening form to the Commission to obtain non-binding guidance on my proposed investment. I request to receive the following information:"
                    }),
                    new FormCheckBox({
                        key: 'option1',
                        label: 'Does my proposed investment require the submission of a proposal to the Commission under section 36 of the Myanmar Investment Law?',
                        value: false
                    }),

                    new FormCheckBox({
                        key: 'option2',
                        label: 'Is my proposed investment likely to be submitted to the Pyidaungsu Hluttaw for approval prior to the issue of a Permit under section 46 of the Law?',
                        value: false
                    }),

                    new FormCheckBox({
                        key: 'option3',
                        label: 'Does my proposed investment fall under investment activities restricted under section 42 of the Law and its related notification?',
                        value: false
                    }),

                    new FormCheckBox({
                        key: 'option4',
                        label: 'Is my proposed investment in an investment promoted sector as publicly notified by the Commission under section 43 of the Law?',
                        value: false
                    }),

                    new FormCheckBox({
                        key: 'option5',
                        label: 'Does my proposed investment fall under investment activities prohibited under section 41 of the Law?',
                        value: false
                    }),

                    new FormCheckBox({
                        key: 'option6',
                        label: 'Other:',
                        value: false
                    }),

                    new FormTextArea({
                        key: 'other',
                        label: 'Choose for Other',
                        value: '',
                        required: true,
                        criteriaValue: {
                            key: 'option6',
                            value: true
                        },
                    })
                ]
            })
        ]
    }
}