import { TranslateService } from '@ngx-translate/core';
import { Inquiry } from 'src/app/models/inquiry';
import { LocationService } from './../../../../services/location.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { Location } from '@angular/common';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core';
import { AuthService } from 'src/services/auth.service';

@Component({
    selector: 'app-inquiry-state-and-region',
    templateUrl: './inquiry-state-and-region.component.html',
    styleUrls: ['./inquiry-state-and-region.component.scss']
})
export class InquiryStateAndRegionComponent implements OnInit {
    @Input() id: any;

    states: any;
    inquiryForm: FormGroup;
    inquiryModel: any = {};

    submitted = false;
    loaded: boolean = false;
    spinner = false;
    loading: boolean = true;
    language: string = "en";
    is_officer: boolean = false;

    constructor(
        private router: Router,
        private lotService: LocationService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
        private service: InquiryService,
        private formBuilder: FormBuilder,
        private store: Store<AppState>,
        public location: Location,
    ) {
        this.is_officer = window.location.href.indexOf('officer') !== -1 ? true : false;

        this.inquiryForm = this.formBuilder.group({
            inquiry_to_state: ['', [Validators.required]]
        });

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id');
            if (this.id) {
                this.service.show(this.id).subscribe(x => {
                    this.loaded = true;
                    this.loading = false;
                    this.inquiryModel = x as Inquiry;
                    this.language = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                    this.translateService.use(this.language);
                    this.lotService.getState(this.language).subscribe(x => this.states = x);
                    this.inquiryForm.get('inquiry_to_state').setValue(this.inquiryModel.inquiry_to_state);
                });
            }
            else {
                this.loaded = true;
                this.lotService.getState().subscribe(x => this.states = x);
            }
        }).unsubscribe();

    }

    ngOnInit(): void { }

    onSubmit() {
        this.spinner = true;
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.inquiryForm.invalid) {
            this.spinner = false;
            return;
        }
        else {
            this.service.create({ ...this.inquiryModel, ...this.inquiryForm.value }, { secure: true })
                .subscribe(x => {
                    this.spinner = false;
                    this.redirectLink();
                });
        }
    }

    redirectLink() {
        if (window.location.href.indexOf('officer') !== -1) {
            this.router.navigate(['officer/inquiry/form1', this.id]);
        } else {
            this.router.navigate(['pages/inquiry/form1', this.id]);
        }
    }
}
