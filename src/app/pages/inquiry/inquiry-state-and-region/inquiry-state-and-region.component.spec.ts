import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryStateAndRegionComponent } from './inquiry-state-and-region.component';

describe('InquiryStateAndRegionComponent', () => {
  let component: InquiryStateAndRegionComponent;
  let fixture: ComponentFixture<InquiryStateAndRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryStateAndRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryStateAndRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
