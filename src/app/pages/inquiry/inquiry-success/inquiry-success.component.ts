import { NgDocumentModalContent } from './../../../custom-component/dynamic-forms/document.modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { Inquiry } from 'src/app/models/inquiry';
import { jsPDF } from 'jspdf';
import 'jspdf-autotable'
import { AuthService } from 'src/services/auth.service';
import { InquiryExport } from '../inquiry-word';

@Component({
    selector: 'app-inquiry-success',
    templateUrl: './inquiry-success.component.html',
    styleUrls: ['./inquiry-success.component.scss']
})
export class InquirySuccessComponent implements OnInit {
    id: any;

    language: any;
    inquiryModel: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private service: InquiryService,
        private locationStrategy: LocationStrategy,
        private translateService: TranslateService,
        private authService: AuthService,
        private modalService: NgbModal,
    ) {
        this.language = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.language ? this.language : 'en');
        this.translateService.use(this.language ? this.language : 'en');
        history.pushState(null, null, location.href);
        this.locationStrategy.onPopState(() => {
            history.pushState(null, null, location.href);
        });
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
        });

        this.authService.changeLanguage$.subscribe(x => {
            this.language = x;
        })

        this.getInquiry();
    }

    ngOnInit(): void { }

    getInquiry() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            if (this.id > 0) {
                this.service.show(this.id, { secure: true })
                    .subscribe((x: any) => {
                        this.inquiryModel = x as Inquiry;
                    });
            }
        }).unsubscribe();
    }

    centerText(doc, text, y) {
        var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
        var textOffset = (doc.internal.pageSize.width - textWidth) / 2;
        doc.text(textOffset, y, text);
    }

    print(): void {
        let lessor_types = [
            {
                value: '1',
                label: 'A person who has previously obtained the right to use the state-owned and or buildings from the government department and government organization in accordance with the laws of the Union.',
            },
            {
                value: '2',
                label: 'A person authorized to sub-lease or sub-license the state-owned land or building in accordance with the approval of the government department and government organization.',
            },
            {
                value: '3',
                label: 'Not applicable',
            }
        ];
        let currentPage = 0;

        let didDrawPage = (data) => {
            if (currentPage != doc.internal.getNumberOfPages()) {
                currentPage = doc.internal.getNumberOfPages();
                // Header
                doc.setFontSize(fontSizes.TitleFontSize);
                doc.setTextColor(40);
                // if (base64Img) {
                //   doc.addImage(base64Img, 'JPEG', data.settings.margin.left, 15, 10, 10)
                // }
                textToCenter(doc, "Inquiry (Form 1)", 22);

                // Footer
                var str = 'Page ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' of ' + totalPagesExp
                }
                doc.setFontSize(fontSizes.SmallFontSize);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            }
        };

        let textToCenter = this.centerText;
        let totalPagesExp = '{total_pages_count_string}';
        (jsPDF as any).autoTableSetDefaults({
            margin: { top: 30 },
            styles: { overflow: 'linebreak', fontSize: 9 },
            didDrawPage: didDrawPage,
            showHead: 'firstPage'
        });
        const doc: any = new jsPDF("p", "mm", "a4");

        const fontSizes = {
            TitleFontSize: 13,
            SubTitleFontSize: 13,
            NormalFontSize: 13,
            SmallFontSize: 10
        };

        const lineSpacing = 12;

        let startY = 75; // bit more then 45mm

        const pageWidth = doc.internal.pageSize.width;
        const pageHeight = doc.internal.pageSize.height;
        const pageCenterX = pageWidth / 2;


        if (this.id > 0) {
            this.service.show(this.id, { secure: true })
                .subscribe((x: any) => {
                    this.inquiryModel = x as Inquiry;
                    let head = [];
                    let form1_data = this.inquiryModel.request_information;
                    let form2_data = this.inquiryModel.investor;
                    let form3_data = this.inquiryModel.applicant_investor;

                    let requestInformation = [
                        [
                            form1_data.option1 ? './assets/images/checked.png' : 'assets/images/uncheck.png',
                            'Does my proposed investment require the submission of a proposal to the Commission under section 36 of the Myanmar Investment Law?',

                            ''
                        ],

                        [
                            form1_data.option2 ? './assets/images/checked.png' : 'assets/images/uncheck.png',
                            'Is my proposed investment likely to be submitted to the Pyidaungsu Hluttaw for approval prior to the issue of a Permit under section 46 of the Law?',

                            ''
                        ],

                        [
                            form1_data.option3 ? './assets/images/checked.png' : 'assets/images/uncheck.png',
                            'Does my proposed investment fall under investment activities restricted under section 42 of the Law and its related notification?',

                            ''
                        ],

                        [
                            form1_data.option4 ? './assets/images/checked.png' : 'assets/images/uncheck.png',
                            'Is my proposed investment in an investment promoted sector as publicly notified by the Commission under section 43 of the Law?',

                            ''
                        ],

                        [
                            form1_data.option5 ? './assets/images/checked.png' : 'assets/images/uncheck.png',
                            'Does my proposed investment fall under investment activities prohibited under section 41 of the Law?',

                            ''
                        ],

                        [
                            form1_data.option6 ? './assets/images/checked.png' : 'assets/images/uncheck.png',
                            'Other',
                            form1_data.other ? form1_data.other : '',
                        ],
                    ];
                    let investor = [
                        [
                            'Full Name',
                            form2_data.name || ''
                        ],
                        [

                            'Country of incorporation (if company) or citizenship (if individual)',
                            form2_data.country
                        ],
                        [
                            'Passport number (foreign nationals), National Registration Card Number (Myanmar citizens) or company registration number (if company)',
                            form2_data.personal_id_no
                        ],

                        [
                            'If applicable, address in Myanmar',
                            form2_data.address
                        ],
                        [
                            'If applicable, address in country of origin',
                            form2_data.address
                        ],
                        [
                            'Phone number',
                            form2_data.phone
                        ],
                        [
                            'Fax number',
                            form2_data.fax
                        ],
                        [
                            'E-mail address',
                            form2_data.email
                        ],
                    ];

                    let applicantInvestor = [

                        [
                            'Full Name',
                            form3_data.name
                        ],
                        [
                            'Country of incorporation (if company) or citizenship (if individual)',
                            form3_data.country
                        ],
                        [
                            'Passport number (foreign nationals), National Registration Card Number (Myanmar citizens) or company registration number (if company)',
                            form3_data.personal_id_no
                        ],
                        [
                            'Address',
                            form3_data.address
                        ],
                        [
                            'Phone number',
                            form3_data.phone,
                        ],
                        [
                            'Fax number',
                            form3_data.fax,
                        ],
                        [
                            'E-mail address',
                            form3_data.email,
                        ]
                    ];

                    let shareRatio = [
                        ['Myanmar citizen share ratio', this.inquiryModel.citizen_share_ratio + '%'],
                        ['Foreign share ratio', this.inquiryModel.foreigner_share_ratio + '%'],
                        ['Government department/ organization share ratio', this.inquiryModel.government_share_ratio + '%']
                    ];

                    let business_sector = [
                        [
                            'Planned or actual type of company incorporated in Myanmar',
                            this.inquiryModel.company_type
                        ],
                        [
                            'General business sector of proposed investment ',
                            this.inquiryModel.business_sector.business_sector_id
                        ],

                    ];

                    let specific_sector = [
                        [
                            'Business sections ( ISIC Sections ) ',
                            this.inquiryModel.business_sector.isic_section_id
                        ],
                        [
                            'Business divisions ( ISIC Divisions )',
                            this.inquiryModel.business_sector.isic_division_id.map(x => x.replace('–', '-'))
                        ],
                        [
                            'Business groups ( ISIC Groups )',
                            this.inquiryModel.business_sector.isic_group_id
                        ],
                        [
                            'Business class(es) ( ISIC Classes )',
                            this.inquiryModel.business_sector.isic_section_id
                        ],

                    ];

                    let description = [
                        [
                            'Detailed description of the proposed investment',
                            this.inquiryModel.description
                        ]
                    ];

                    let investment = [
                        ['Total Amount in Myanmar Kyat ',
                            this.inquiryModel.investment.e_kyat + ' Ks'
                        ],
                        ['Equivalent Amount in USD ',
                            this.inquiryModel.investment.e_usd + ' $'
                        ],
                        ['Expected total land area needed for investment project',
                            this.inquiryModel.investment.area + ' ' + this.inquiryModel.investment.unit_id
                        ],

                    ];

                    let cross_border = [
                        [
                            'Is the proposed investment a cross-border investment?',
                            this.inquiryModel.cross_border.is_cross_border

                        ],
                        [

                            'Elaborate',
                            this.inquiryModel.cross_border.elaborate

                        ],
                    ];

                    let relocation = [
                        [
                            'Has it been or is it likely to be acquired through expropriation by paying compensation, compulsory acquisition procedure or by agreement in advance of such expropriation or compulsory acquisition procedure in accordance with the laws of Myanmar?',
                            this.inquiryModel.relocation.has_compensation
                        ],
                        [
                            'Will it cause the relocation of individuals permanently residing on such land?',
                            this.inquiryModel.relocation.is_relocation
                        ],
                        [
                            'Does it comprise an area of more than 100 acres?',
                            this.inquiryModel.relocation.is_more_than_100
                        ],
                        [
                            'Will the use or lease of land for the investment cause or be likely to cause involuntary restrictions on land use and access to natural resources to any person having a legal right to such land use or access?',
                            this.inquiryModel.relocation.is_legal_right
                        ],
                        [
                            'Will the use or lease of land for the investment cause or be likely to cause conflict with the proposed investment activity due to litigation in good faith by a person or disputing over ownership of land?',
                            this.inquiryModel.relocation.is_conflict
                        ],
                        [
                            'Will the use or lease of land for the investment adversely impact or be likely to adversely impact individuals by continue occupying such land scrutinized by a body which has right to scrutinize in applying to occupy or use land?',
                            this.inquiryModel.relocation.is_adversely_impact
                        ],
                        [
                            'Other comments',
                            this.inquiryModel.relocation.comment
                        ],
                    ];
                    head = [
                        { id: { content: 'Myanmar Investment Rule 28, I hereby summit an investment screening form to the Commission to obtain non-binding guidance on my proposed investment. I request to receive the following information', colSpan: 3, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                    ];

                    doc.autoTable({
                        head: head,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: requestInformation,

                        columnStyles: {
                            0: { cellWidth: 20 },
                            1: { cellWidth: 100 },
                            2: { cellWidth: 'auto' }
                        },
                        didParseCell: function (data) {
                            if (data.column.index === 0 && data.cell.section === 'body') {
                                data.cell.text = [];
                            }
                        },
                        didDrawCell: function (data) {
                            if (data.column.index === 0 && data.cell.section === 'body') {
                                var img = data.cell.raw;
                                var dim = data.cell.y + ((data.cell.height + 5) / 4);
                                data.cell.raw = '';
                                doc.addImage(img, data.cell.x, data.cell.y + 3, 5, 5);
                            }
                        }
                    });

                    head = [
                        { id: { content: 'Details on the investor', colSpan: 3, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                    ];


                    let finalY = doc.lastAutoTable.finalY + lineSpacing;

                    doc.autoTable({
                        head: head,
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: investor,
                        columnStyles: {
                            0: { cellWidth: 100 },
                            1: { cellWidth: 'auto' },
                        },
                    });

                    head = [
                        { id: { content: 'If the investor is not submitting the investment screeningform by herself/himself\'s the applicant`', colSpan: 3, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                    ];

                    finalY = doc.lastAutoTable.finalY + lineSpacing;

                    doc.autoTable({
                        head: head,
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: applicantInvestor,
                        columnStyles: {
                            0: { cellWidth: 100 },
                            1: { cellWidth: 'auto' },
                        },
                    });

                    finalY = doc.lastAutoTable.finalY + lineSpacing;
                    doc.autoTable({
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: [['3. Basic information on planned investment']],
                        columnStyles: {
                            0: { cellWidth: 'auto', fontSize: 10, fontStyle: 'bold' }
                        },
                    });

                    finalY = doc.lastAutoTable.finalY + lineSpacing;
                    head = [
                        { id: { content: 'Planned or actual share ratio of enterprise', colSpan: 3, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                    ];

                    doc.autoTable({
                        head: head,
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: shareRatio,
                    });

                    finalY = doc.lastAutoTable.finalY + lineSpacing;
                    doc.autoTable({
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: business_sector,
                        columnStyles: {
                            0: { cellWidth: 50 },
                            1: { cellWidth: 'auto' },
                        }
                    });

                    head = [
                        { id: { content: 'Specific sector of the proposed investment', colSpan: 2, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                    ];

                    finalY = doc.lastAutoTable.finalY + lineSpacing;
                    doc.autoTable({
                        head: head,
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: specific_sector,
                        columnStyles: {
                            0: { cellWidth: 50 },
                            1: { cellWidth: 'auto' },
                        },
                    });
                    if (this.inquiryModel.business_cpc) {
                        head = [
                            { id: { content: 'The specific product(s)/service(s) of the investment:', colSpan: 2, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                        ];
                        let business_cpc = [
                            [
                                'Business sections ( CPC Section ) ',
                                this.inquiryModel.business_cpc.cpc_section_id
                            ],
                            [
                                'Business divisions ( CPC Division )',
                                this.inquiryModel.business_cpc?.cpc_division_id?.map(x => x.replace('–', '-'))
                            ],
                            [
                                'Business groups ( CPC Group )',
                                this.inquiryModel.business_cpc.cpc_group_id
                            ],
                            [
                                'Business class(es) ( CPC Classes )',
                                this.inquiryModel.business_cpc.cpc_class_id
                            ],
                            [
                                'Business subclass(es) ( CPC Subclasses )',
                                this.inquiryModel.business_cpc.cpc_sub_class_ids
                            ],
                        ];
                        finalY = doc.lastAutoTable.finalY + lineSpacing;
                        doc.autoTable({
                            head: head,
                            startY: finalY,
                            styles: { overflow: 'linebreak' },
                            theme: 'plain',
                            body: business_cpc,
                            columnStyles: {
                                0: { cellWidth: 50 },
                                1: { cellWidth: 'auto' },
                            },
                        });
                    }


                    finalY = doc.lastAutoTable.finalY + lineSpacing;
                    doc.autoTable({
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: description,
                        columnStyles: {
                            0: { cellWidth: 50 },
                            1: { cellWidth: 'auto' },
                        },
                    });

                    head = [
                        { id: { content: 'Expected total value  And total land area needed of the investment project', colSpan: 2, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                    ];

                    finalY = doc.lastAutoTable.finalY + lineSpacing;
                    doc.autoTable({
                        head: head,
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: investment,
                        columnStyles: {
                            0: { cellWidth: 100 },
                            1: { cellWidth: 'auto' },
                        },
                    });
                    this.inquiryModel.plan_location.forEach((x, index) => {
                        head = [
                            { id: { content: `Location ${index + 1}`, colSpan: 2, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                        ];
                        let location = [
                            [
                                'Name', `${x.owner_name}`
                            ],
                            [
                                'Administrator Type', `${x.owner_type}`
                            ],
                            [
                                'Type of land', `${x.land_type}`
                            ],
                            [
                                'Lessor Name', `${x.lessor_name}`
                            ],
                            [
                                'Lessor Type', `${x.lessor_type}`
                            ],
                            [
                                'Lessor Type Apply', `${lessor_types.filter(y => y.value == x.lessor_type_apply)[0].label}`
                            ],
                            [
                                'Planned From Date', `${x.plan_from}`
                            ],
                            [
                                'Planned To Date', `${x.plan_to}`
                            ],
                            [
                                'Address', `${x.township}, ${x.districe},  ${x.state}`
                            ]
                        ];
                        finalY = doc.lastAutoTable.finalY + lineSpacing;
                        doc.autoTable({
                            head: head,
                            startY: finalY,
                            styles: { overflow: 'linebreak' },
                            theme: 'plain',
                            body: location,
                            columnStyles: {
                                0: { cellWidth: 50 },
                                1: { cellWidth: 'auto' },
                            },
                        });
                    });
                    head = [
                        { id: { content: 'Corss border investment', colSpan: 2, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                    ];

                    finalY = doc.lastAutoTable.finalY + lineSpacing;
                    doc.autoTable({
                        head: head,
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: cross_border,
                        columnStyles: {
                            0: { cellWidth: 100 },
                            1: { cellWidth: 'auto' },
                        },
                    });

                    head = [
                        { id: { content: 'Corss border investment relocation', colSpan: 2, styles: { halign: 'left', fillColor: [11, 175, 78] } } },
                    ];
                    finalY = doc.lastAutoTable.finalY + lineSpacing;
                    doc.autoTable({
                        head: head,
                        startY: finalY,
                        styles: { overflow: 'linebreak' },
                        theme: 'plain',
                        body: relocation,
                        columnStyles: {
                            0: { cellWidth: 100 },
                            1: { cellWidth: 'auto' },
                        },
                    });

                    doc.setProperties({
                        title: "Inquiry (Form 1)"
                    });
                    doc.putTotalPages(totalPagesExp);
                    // window.open(doc.output('bloburl'), '_blank');
                    const modalRef = this.modalService.open(NgDocumentModalContent, {
                        size: 'xl'
                    });
                    modalRef.componentInstance.file = {
                        url: doc.output('bloburl'),
                        type: 'application/pdf'
                    };
                    modalRef.result.then((result) => {
                    });
                });
        }
    }

    printPDF() {
        const previewExport = new InquiryExport(this.inquiryModel, localStorage.getItem('lan'));
        previewExport.download().subscribe();
    }
}
