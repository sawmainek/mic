import { CPCService } from './../../../../services/cpc.service';
import { ISICService } from './../../../../services/isic.service';
import { LocalService } from './../../../../services/local.service';
import { InquiryForm3 } from './../inquiry-form3/inquiry-form3';
import { InquiryForm4 } from './../inquiry-form4/inquiry-form4';
import { InquiryForm5 } from './../inquiry-form5/inquiry-form5';
import { InquiryForm6 } from './../inquiry-form6/inquiry-form6';
import { InquiryForm7 } from './../inquiry-form7/inquiry-form7';
import { LocationService } from './../../../../services/location.service';
import { InquiryForm2 } from './../inquiry-form2/inquiry-form2';
import { InquiryForm1 } from './../inquiry-form1/inquiry-form1';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { PrintPDFService } from './../../../custom-component/print.pdf.service';
import { Injectable } from '@angular/core';

@Injectable()
export class InquiryPreviewService {
    forms: BaseForm<string|boolean>[] = [];
    constructor(
        public lotService: LocationService,
        public localService: LocalService,
        public isicServcie: ISICService,
        public cpcService: CPCService,
        public printPDFService: PrintPDFService
    ) {
        
    }

    toFormPDF(data: any, option: {
        readOnly?: boolean,
        language?: string,
        onSubmit$?: any
    }) {
        const inquiryForm1 = new InquiryForm1();
        const inquiryForm2 = new InquiryForm2(option.language, this.lotService);
        const inquiryForm3 = new InquiryForm3(option.language, this.lotService);
        const inquiryForm4 = new InquiryForm4(option.language, this.isicServcie, this.cpcService, this.localService);
        const inquiryForm5 = new InquiryForm5(option.language, this.localService);
        const inquiryForm6 = new InquiryForm6(option.language, this.lotService, this.localService);
        const inquiryForm7 = new InquiryForm7();

        this.forms = [
            ...inquiryForm1.forms,
            ...inquiryForm2.forms,
            ...inquiryForm3.forms,
            ...inquiryForm4.forms,
            ...inquiryForm5.forms,
            ...inquiryForm6.forms,
            ...inquiryForm7.forms,
        ]

        this.printPDFService.toFormPDF(this.forms, data, 'Inquiry (Form 1)', option);
    }
}