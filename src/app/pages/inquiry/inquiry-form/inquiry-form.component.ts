import { TranslateService } from '@ngx-translate/core';
import { InquiryService } from 'src/services/inquiry.service';
import { AuthService } from 'src/services/auth.service';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-inquiry-form',
    templateUrl: './inquiry-form.component.html',
    styleUrls: ['./inquiry-form.component.scss']
})
export class InquiryFormComponent implements OnInit {
    @Input() id: any;
    inquiryForm: FormGroup;
    submitted = false;
    inquiryModel: any = {};
    user: any = {};
    loading = true;
    spinner = false;

    constructor(
        private router: Router,
        private toast: ToastrService,
        private authService: AuthService,
        private activatedRoute: ActivatedRoute,
        private inquiryService: InquiryService,
        private formBuilder: FormBuilder,
        private translateService: TranslateService,
        private store: Store<AppState>,
        public location: Location
    ) {
        this.inquiryForm = this.formBuilder.group({
            apply_to: ["Myanmar Investment Commision"],
            user_id: ['']
        });
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || this.id || null;

            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                if (this.user) {
                    // Checked for Officer
                    if (this.id > 0) {
                        this.inquiryService.show(this.id).subscribe(result => {
                            this.inquiryModel = result || {};
                            const lan = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                            this.translateService.use(lan);
                            this.inquiryForm.patchValue(this.inquiryModel);
                            this.loading = false;
                        })
                    } else {
                        this.loading = false;
                    }

                }
            }, err => {
                console.log(err);
            });

        }).unsubscribe();
    }

    openChoose(choose) {
        if (window.location.href.indexOf('officer') == -1) {
            this.inquiryForm.get('apply_to').setValue(choose);
        }
    }

    onSubmit() {
        this.spinner = true;
        this.store.dispatch(new Submit({ submit: true }));
        this.inquiryModel = this.inquiryModel || {};
        this.inquiryModel.draft = this.inquiryModel.draft || true;
        this.inquiryForm.get('user_id').setValue(this.inquiryModel.user_id || this.user.id);
        this.inquiryService.create({ ...this.inquiryModel, ...this.inquiryForm.value }, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                this.redirectLink(x.id);
            });
    }

    redirectLink(id: number) {
        if (id == null) {
            this.toast.error("Please login with investor account.");
            return;
        }
        if (window.location.href.indexOf('officer') !== -1) {
            if (this.inquiryForm.get('apply_to').value === 'Myanmar Investment Commision') {
                this.router.navigate(['/officer/inquiry/form1', id]);
            } else {
                this.router.navigate(['/officer/inquiry/state-and-region', id]);
            }
        } else {
            if (this.inquiryForm.get('apply_to').value === 'Myanmar Investment Commision') {
                this.router.navigate(['/pages/inquiry/form1', id]);
            } else {
                this.router.navigate(['/pages/inquiry/state-and-region', id]);
            }
        }
    }
}
