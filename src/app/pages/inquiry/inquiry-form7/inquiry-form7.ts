import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { BaseFormArray } from './../../../custom-component/dynamic-forms/base/form-array';
import { FormFile } from './../../../custom-component/dynamic-forms/base/form.file';
import { FormCheckBox } from './../../../custom-component/dynamic-forms/base/form-checkbox';
import { FormTextArea } from './../../../custom-component/dynamic-forms/base/form-textarea';
import { FormTitle } from './../../../custom-component/dynamic-forms/base/form-title';
import { FormSectionTitle } from './../../../custom-component/dynamic-forms/base/form-section-title';
import { FormInput } from './../../../custom-component/dynamic-forms/base/form-input';
import { FormRadio } from './../../../custom-component/dynamic-forms/base/form-radio';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
export class InquiryForm7 {
    public forms: BaseForm<string | boolean>[] = []
    constructor() {
        const crossBorder: BaseForm < any > [] =[
            new FormSectionTitle({
                label: '7. Is the proposed investment a cross-border investment?'
            }),
            new FormRadio({
                key: 'is_cross_border',
                required: true,
                columns: 'col-md-3',
                value: 'Yes',
                label: 'Yes'
            }),
            new FormRadio({
                key: 'is_cross_border',
                required: true,
                columns: 'col-md-3',
                value: 'No',
                label: 'No'
            }),
            new FormInput({
                key: 'elaborate',
                label: 'Please elaborate:',
                required: true,
                criteriaValue: {
                    key: 'is_cross_border',
                    value: 'Yes'
                }
            }),
        ];
        const relocation: BaseForm < any > [] =[
            new FormSectionTitle({
                label: '8. Regarding the acquisition of land used or leased for the investment:'
            }),
            new FormTitle({
                label: 'a. Has it been or is it likely to be acquired through expropriation by paying compensation, compulsory acquisition procedure or by agreement in advance of such expropriation or compulsory acquisition procedure in accordance with the laws of Myanmar?'
            }),
            new FormRadio({
                key: 'has_compensation',
                required: true,
                columns: 'col-md-3',
                value: 'Yes',
                label: 'Yes'
            }),
            new FormRadio({
                key: 'has_compensation',
                required: true,
                columns: 'col-md-3',
                value: 'No',
                label: 'No'
            }),
            new FormTitle({
                label: 'b. If investor selected “Yes” above:',
                criteriaValue: {
                    key: 'has_compensation',
                    value: 'Yes'
                }
            }),
            new FormTitle({
                label: 'i. Will it cause the relocation of individuals permanently residing on such land?',
                criteriaValue: {
                    key: 'has_compensation',
                    value: 'Yes'
                }
            }),
            new FormRadio({
                key: 'is_relocation',
                required: true,
                columns: 'col-md-3',
                value: 'Yes',
                label: 'Yes',
                criteriaValue: {
                    key: 'has_compensation',
                    value: 'Yes'
                }
            }),
            new FormRadio({
                key: 'is_relocation',
                required: true,
                columns: 'col-md-3',
                value: 'No',
                label: 'No',
                criteriaValue: {
                    key: 'has_compensation',
                    value: 'Yes'
                }
            }),
            new FormInput({
                key: 'number_relocation',
                label: 'Indicate the number of persons impacted:',
                required: true,
                criteriaValue: {
                    key: 'is_relocation',
                    value: 'Yes'
                }
            }),
            new FormTitle({
                label: 'ii. Does it comprise an area of more than 100 acres?',
                criteriaValue: {
                    key: 'has_compensation',
                    value: 'Yes'
                }
            }),
            new FormRadio({
                key: 'is_more_than_100',
                required: true,
                columns: 'col-md-3',
                value: 'Yes',
                label: 'Yes',
                criteriaValue: {
                    key: 'has_compensation',
                    value: 'Yes'
                }
            }),
            new FormRadio({
                key: 'is_more_than_100',
                required: true,
                columns: 'col-md-3',
                value: 'No',
                label: 'No',
                criteriaValue: {
                    key: 'has_compensation',
                    value: 'Yes'
                }
            }),
        ];
        const legalRight: BaseForm < any > [] =[
            new FormSectionTitle({
                label: '9. Will the use or lease of land for the investment cause or be likely to cause involuntary restrictions on land use and access to natural resources to any person having a legal right to such land use or access?'
            }),
            new FormRadio({
                key: 'is_legal_right',
                columns: 'col-md-3',
                required: true,
                value: 'Yes',
                label: 'Yes',
            }),
            new FormRadio({
                key: 'is_legal_right',
                columns: 'col-md-3',
                required: true,
                value: 'No',
                label: 'No',
            }),
            new FormInput({
                key: 'number_legal_right',
                label: 'Indicate the number of acres of land this applies to :',
                type: 'number',
                required: true,
                criteriaValue: {
                    key: 'is_legal_right',
                    value: 'Yes'
                }
            }),
        ]
        const conflict: BaseForm < any > [] =[
            new FormSectionTitle({
                label: '10. Will the use or lease of land for the investment cause or be likely to cause conflict with the proposed investment activity due to litigation in good faith by a person or disputing over ownership of land?'
            }),
            new FormRadio({
                key: 'is_conflict',
                required: true,
                columns: 'col-md-3',
                value: 'Yes',
                label: 'Yes'
            }),
            new FormRadio({
                key: 'is_conflict',
                columns: 'col-md-3',
                required: true,
                value: 'No',
                label: 'No'
            }),
            new FormInput({
                key: 'number_conflict',
                label: 'Indicate the number of acres of land this applies to :',
                type: 'number',
                required: true,
                criteriaValue: {
                    key: 'is_conflict',
                    value: 'Yes'
                }
            }),
        ];
        const impact: BaseForm < any > [] =[
            new FormSectionTitle({
                label: '11. Will the use or lease of land for the investment adversely impact or be likely to adversely impact individuals by continue occupying such land scrutinized by a body which has right to scrutinize in applying to occupy or use land?'
            }),
            new FormRadio({
                key: 'is_adversely_impact',
                required: true,
                columns: 'col-md-3',
                value: 'Yes',
                label: 'Yes'
            }),
            new FormRadio({
                key: 'is_adversely_impact',
                required: true,
                columns: 'col-md-3',
                value: 'No',
                label: 'No'
            }),
            new FormInput({
                key: 'number_adversely_impact',
                label: 'Indicate the number of persons impacted :',
                required: true,
                type: 'number',
                criteriaValue: {
                    key: 'is_adversely_impact',
                    value: 'Yes'
                }
            }),
        ];
        this.forms = [
            new BaseFormGroup({
                key: 'cross_border',
                formGroup: crossBorder
            }),
            new BaseFormGroup({
                key: 'relocation',
                formGroup: relocation
            }),
            new BaseFormGroup({
                key: 'legal_right',
                formGroup: legalRight
            }),
            new BaseFormGroup({
                key: 'conflict',
                formGroup: conflict
            }),
            new BaseFormGroup({
                key: 'impact',
                formGroup: impact
            }),
            new FormSectionTitle({
                label: '12. If necessary, please upload related documents or information:'
            }),
            new BaseFormArray({
                key: 'documents',
                formArray: [
                    new FormFile({
                        key: 'doc',
                        label: 'Title of document:',
                        multiple: true,
                        columns: 'col-md-9'
                    }),
                ],
            }),
            // For Document Upload
            new FormTextArea({
                key: 'comment',
                label: 'Other comments:'
            }),
            new FormCheckBox({
                key: 'agree',
                label: 'By submitting this inquiry form I / we hereby declare that the above statements fully disclose the nature of the investment and are true and correct to the best of my/our knowledge and belief.',
                required: true,
                value: false,
            })
        ]
    }
}