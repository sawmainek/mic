import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryForm7Component } from './inquiry-form7.component';

describe('InquiryForm7Component', () => {
  let component: InquiryForm7Component;
  let fixture: ComponentFixture<InquiryForm7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryForm7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryForm7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
