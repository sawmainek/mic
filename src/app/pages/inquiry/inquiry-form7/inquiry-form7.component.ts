import { NotifyService } from './../../../../services/notify.service';
import { FormLogService } from './../../../../services/form-log.service';
import { TranslateService } from '@ngx-translate/core';
import { InquiryPreviewService } from './../inquiry-preview/inquiry-preview.service';
import { Inquiry } from 'src/app/models/inquiry';
import { ToastrService } from 'ngx-toastr';
import { FormFile } from './../../../custom-component/dynamic-forms/base/form.file';
import { BaseFormArray } from '../../../custom-component/dynamic-forms/base/form-array';
import { FormControlService } from './../../../custom-component/dynamic-forms/form-control.service';
import { FormCheckBox } from '../../../custom-component/dynamic-forms/base/form-checkbox';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormRadio } from './../../../custom-component/dynamic-forms/base/form-radio';
import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { FormTitle } from './../../../custom-component/dynamic-forms/base/form-title';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { Location } from '@angular/common';
import { BaseFormData } from 'src/app/core/form';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { Submit } from 'src/app/core/form/_actions/form.actions';
import { FormService } from 'src/services/form.service';
import { InquirySubmissionService } from 'src/services/inquiry-submission.service';
import { Form } from 'src/app/models/form';
import { SubmissionInquiry } from 'src/app/models/submission_inquiry';
import { InquiryForm7 } from './inquiry-form7';
declare var jQuery: any;

@Component({
    selector: 'app-inquiry-form7',
    templateUrl: './inquiry-form7.component.html',
    styleUrls: ['./inquiry-form7.component.scss']
})
export class InquiryForm7Component implements OnInit {
    @Input() id: any;

    formModel: Form;
    inquirySubmissionModel: SubmissionInquiry;
    inquiryModel: Inquiry;
    inquiryFormGroup: FormGroup;

    user: any = {};

    submitted = false;
    loading = false;
    spinner = false;
    isDraft = false;
    isPreview = false;

    baseForm: InquiryForm7;
    constructor(
        private activatedRoute: ActivatedRoute,
        private inquiryService: InquiryService,
        private formCtlService: FormControlService,
        private router: Router,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
        private formService: FormService,
        private formLogService: FormLogService,
        private notifyService: NotifyService,
        private translateService: TranslateService,
        private inquiryPreviewService: InquiryPreviewService,
        private inquirySubmissionService: InquirySubmissionService,
    ) {
        
        this.loading = true;
        this.getCurrentUser();
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            this.baseForm = new InquiryForm7();
            this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            if (this.id > 0) {
                this.inquiryService.show(this.id, { secure: true })
                    .subscribe((x: any) => {
                        this.inquiryModel = x as Inquiry;
                        const lan = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                        this.translateService.use(lan);
                        this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.inquiryModel);

                        const page = `inquiry/form7/${this.inquiryModel.id}`;
                        this.formCtlService.getComments$('Inquiry', this.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.id, type: 'Inquiry', page: page, comments }));
                        });
                        this.inquirySubmissionModel = this.inquiryModel.submission || {};
                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onReply() {
        jQuery('.nav-tabs a[href="#reply"]').tab('show');
    }

    saveDraft() {
        this.isDraft = true;
        this.isPreview = false;
        this.saveInquiry();
    }

    onPreview() {
        this.isPreview = true;
        this.isDraft = false;
        this.saveInquiry();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.inquiryFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.isDraft = false;
        this.isPreview = false;
        this.saveInquiry();
    }

    saveInquiry() {
        this.spinner = true;
        this.formCtlService.lazyUpload(this.baseForm.forms, this.inquiryFormGroup, { type_id: this.id }, (formValue) => {
            this.inquiryModel.draft = false;
            this.inquiryService.create({ ...this.inquiryModel, ...formValue })
                .subscribe(x => {
                    this.spinner = false;
                    if(this.isPreview) {
                        this.inquiryPreviewService.toFormPDF(x, {
                            readOnly: true,
                            onSubmit$: () => {
                            }
                        })
                    }
                    else if (!this.isDraft && !this.isPreview) {
                        this.saveFormData();
                    }
                    else {
                        this.toast.success('Saved successfully.');
                    }
                });
        })

    }


    saveFormData() {
        this.spinner = true;

        let data = {
            'application_no': this.inquirySubmissionModel.application_no || `${Math.floor(Math.random() * 999999)}`,
            'inquiry_id': this.inquiryModel.id,
            'submitted_user_id': this.user.id,
            'status': this.inquirySubmissionModel.status || 'New',
            'sub_status': 'New',
            'allow_revision': false
        };

        data['investment_value'] = this.inquiryModel.investment.e_kyat;
        data['investor_name'] = this.inquiryModel?.investor?.name || (window.location.href.indexOf('officer') !== -1 ? null : this.user?.name);
        data['country'] = this.inquiryModel.investor.country;
        data['submit_to'] = this.inquiryModel.apply_to;
        // Todo for permission
        if (this.inquiryModel.apply_to == 'Myanmar Investment Commision') {
            if (this.inquiryModel.business_sector.business_sector_id && this.inquiryModel.business_sector.business_sector_id.indexOf('Investment Division 1') !== -1) {
                data['submit_to_role'] = 'division-1';
            }
            if (this.inquiryModel.business_sector.business_sector_id && this.inquiryModel.business_sector.business_sector_id.indexOf('Investment Division 2') !== -1) {
                data['submit_to_role'] = 'division-2';
            }
            if (this.inquiryModel.business_sector.business_sector_id && this.inquiryModel.business_sector.business_sector_id.indexOf('Investment Division 3') !== -1) {
                data['submit_to_role'] = 'division-3';
            }
            if (this.inquiryModel.business_sector.business_sector_id && this.inquiryModel.business_sector.business_sector_id.indexOf('Investment Division 4') !== -1) {
                data['submit_to_role'] = 'division-4';
            }
        } else {
            data['submit_to_role'] = `${this.inquiryModel.inquiry_to_state.toLowerCase()}-branch`;
        }

        data['sector_of_investment'] = this.inquiryModel.business_sector?.isic_section_id;

        this.inquirySubmissionModel = { ...this.inquirySubmissionModel, ...data };
        this.inquirySubmissionService.create(this.inquirySubmissionModel).subscribe(submission => {
     
            this.formModel = new Form;
            this.formModel.user_id = this.user.id;
            this.formModel.type = "Inquiry ( Form 1 )";
            this.formModel.type_id = submission.id;
            this.formModel.status = this.inquirySubmissionModel.id ? 'Resubmit' : 'New';
            this.formModel.data = this.inquiryModel;
            this.formService.create(this.formModel).subscribe(history => {
                this.spinner = false;
                // Save to Form Data/Status Log Model;
                this.formLogService.create({
                    type: this.formModel.type,
                    type_id: this.formModel.type_id,
                    status: this.inquirySubmissionModel.id ? 'Revision' : 'New',
                    sub_status: 'Submission',
                    submitted_user_id: this.formModel.user_id,
                    reply_user_id: submission?.reply_user_id,
                    incoming: true,
                    extra: {
                        application_no: submission?.application_no,
                        form_id: submission?.inquiry_id,
                        history_data: this.inquiryModel
                    }
                }).subscribe();
                if (submission?.reply_user_id) {
                    this.notifyService.create({
                        user_id: submission?.reply_user_id,
                        subject: `${this.formModel.status} Inquiry ( Form 1 ): # ${submission?.application_no} received.`,
                        message: `Please check Application No. <a href="http://investor.digitaldots.com.mm/officer/inquiry/form/${submission.inquiry_id}">${submission?.application_no}</a> for new submission.`
                    }).subscribe();
                }
                this.router.navigate(['/pages/inquiry/success', this.id]);
            });
            
        });

    
        
    }
}
