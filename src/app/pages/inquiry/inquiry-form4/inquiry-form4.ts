import { LocalService } from './../../../../services/local.service';
import { FormInput } from './../../../custom-component/dynamic-forms/base/form-input';
import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { FormTextArea } from './../../../custom-component/dynamic-forms/base/form-textarea';
import { CPCService } from './../../../../services/cpc.service';
import { ISICService } from './../../../../services/isic.service';
import { FormDummy } from './../../../custom-component/dynamic-forms/base/form-dummy';
import { FormSectionTitle } from './../../../custom-component/dynamic-forms/base/form-section-title';
import { FormNote } from './../../../custom-component/dynamic-forms/base/form-note';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
export class InquiryForm4 {
    public forms: BaseForm<string | boolean>[] = [];
    public businessSector: BaseForm<string | boolean>[] = [];
    public businessCPC: BaseForm<string | boolean>[] = [];
    constructor(
        private lang: string,
        private isicService: ISICService,
        private cpcService: CPCService,
        private localService: LocalService
    ) {
        this.businessSector = [
            new FormDummy({
                key: 'apply_to',
                value: 'Myanmar Investment Commision'
            }),
            new FormSelect({
                key: 'business_sector_id',
                label: 'c. General business sector of proposed investment',
                options$: this.isicService.getInvestment(this.lang),
                required: true,
                criteriaValue: {
                    key: 'apply_to',
                    value: ['Myanmar Investment Commision'],
                }
            }),
            new FormNote({
                label: 'The general business sectors are classified according to the DICA Investment Divisions.',
                criteriaValue: {
                    key: 'apply_to',
                    value: ['Myanmar Investment Commision'],
                }
            }),
            new FormSectionTitle({
                label: 'd. Specific sector of the proposed investment :'
            }),
            new FormSelect({
                key: 'isic_section_id',
                label: 'Choose business sections ( ISIC Sections )',
                options$: this.isicService.getSection(),
                multiple: true,
                filter: {
                    parent: 'business_sector_id',
                    key: 'investment'
                },
                required: true,
            }),
            new FormSelect({
                key: 'isic_division_id',
                label: 'Choose business divisions ( ISIC Divisions )',
                options$: this.isicService.getDivison(),
                multiple: true,
                filter: {
                    parent: 'isic_section_id',
                    key: 'section'
                },
                required: true,
            }),
            new FormSelect({
                key: 'isic_group_id',
                label: 'Choose business groups ( ISIC Groups )',
                options$: this.isicService.getGroup(),
                multiple: true,
                filter: {
                    parent: 'isic_division_id',
                    key: 'division'
                },
                required: true,
            }),
            new FormSelect({
                key: 'isic_class_ids',
                label: 'Choose business class(es) ( ISIC Classes )',
                required: true,
                multiple: true,
                options$: this.isicService.getClasses(this.lang),
                filter: {
                    parent: 'isic_group_id',
                    key: 'group'
                },
            }),
            new FormNote({
                label: 'The specific sectors are classified according to International Standard Industrial Classification (ISIC) Rev. 4.'
            }),
        ];
        this.businessCPC = [
            new FormSelect({
                key: 'cpc_section_id',
                label: 'Choose business sections ( CPC Section )',
                options$: this.cpcService.getSection(),
                multiple: true,
            }),
            new FormSelect({
                key: 'cpc_division_id',
                label: 'Choose business divisions ( CPC Division )',
                options$: this.cpcService.getDivison(),
                multiple: true,
                filter: {
                    parent: 'cpc_section_id',
                    key: 'section'
                }
            }),
            new FormSelect({
                key: 'cpc_group_id',
                label: 'Choose business groups ( CPC Group )',
                options$: this.cpcService.getGroup(),
                multiple: true,
                filter: {
                    parent: 'cpc_division_id',
                    key: 'division'
                }
            }),
            new FormSelect({
                key: 'cpc_class_id',
                label: 'Choose business class(es) ( CPC Classes )',
                options$: this.cpcService.getClasses(),
                multiple: true,
                filter: {
                    parent: 'cpc_group_id',
                    key: 'group'
                }
            }),
            new FormSelect({
                key: 'cpc_sub_class_ids',
                label: 'Choose business subclass(es) ( CPC Subclasses )',
                options$: this.cpcService.getSubclasses(),
                multiple: true,
                filter: {
                    parent: 'cpc_class_id',
                    key: 'class'
                }
            })
        ];
        this.forms = [
            new FormSectionTitle({
                label: '3. Basic information on planned investment'
            }),
            new FormSectionTitle({
                label: 'a. Planned or actual share ratio of enterprise'
            }),
            new FormInput({
                key: 'citizen_share_ratio',
                label: 'Myanmar citizen share ratio:',
                type: 'number',
                min: 0,
                endfix: '%',
                columns: 'col-md-4 d-flex',
                required: true,
                value: "0"
            }),
            new FormInput({
                key: 'foreigner_share_ratio',
                label: 'Foreign share ratio:',
                type: 'number',
                endfix: '%',
                columns: 'col-md-4 d-flex',
                required: true,
                value: "0"
            }),
            new FormInput({
                key: 'government_share_ratio',
                label: 'Government department/ organization share ratio:',
                columns: 'col-md-4 d-flex',
                type: 'number',
                endfix: '%',
                required: true,
                value: "0"
            }),
            new FormSelect({
                key: 'company_type',
                label: 'b. Planned or actual type of company incorporated in Myanmar',
                options$: this.localService.getCompany(this.lang),
                required: true
            }),
            new BaseFormGroup({
                key: 'business_sector',
                formGroup: this.businessSector,
            }),
            new FormSectionTitle({
                label: 'e. Optional: please name the specific product(s)/service(s) of the investment:'
            }),
            new BaseFormGroup({
                key: 'business_cpc',
                formGroup: this.businessCPC
            }),
            new FormNote({
                label: 'Products/services are classified according to Central Product Classification (CPC) Version 2.1.'
            }),
            new FormTextArea({
                key: 'description',
                label: 'f. Detailed description of the proposed investment:',
                required: true
            })
        ]
    }
}