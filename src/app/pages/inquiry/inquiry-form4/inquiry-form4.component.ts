import { TranslateService } from '@ngx-translate/core';
import { BusinessSector } from './../../../models/inquiry';
import { Inquiry } from 'src/app/models/inquiry';
import { FormGroup } from '@angular/forms';
import { CPCService } from './../../../../services/cpc.service';
import { FormHidden } from 'src/app/custom-component/dynamic-forms/base/form-hidden';
import { ISICService } from './../../../../services/isic.service';
import { ToastrService } from 'ngx-toastr';
import { FormState } from './../../../core/form/_reducers/form.reducers';
import { BaseFormData, Submit } from './../../../core/form/_actions/form.actions';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { FormControlService } from './../../../custom-component/dynamic-forms/form-control.service';
import { FormTextArea } from './../../../custom-component/dynamic-forms/base/form-textarea';
import { FormNote } from '../../../custom-component/dynamic-forms/base/form-note';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { FormSectionTitle } from './../../../custom-component/dynamic-forms/base/form-section-title';
import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { Location } from '@angular/common';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { AppState, currentUser } from 'src/app/core';
import { FormDummy } from 'src/app/custom-component/dynamic-forms/base/form-dummy';
import { LocationService } from 'src/services/location.service';
import { LocalService } from 'src/services/local.service';
import { AuthService } from 'src/services/auth.service';
import { InquiryForm4 } from './inquiry-form4';

@Component({
    selector: 'app-inquiry-form4',
    templateUrl: './inquiry-form4.component.html',
    styleUrls: ['./inquiry-form4.component.scss']
})
export class InquiryForm4Component implements OnInit {
    @Input() id: any;

    user: any = {};

    inquiryModel: Inquiry;
    inquiryFormGroup: FormGroup;

    submitted = false;
    loading = false;
    spinner = false;
    isDraft = false;

    isRevision: boolean = false;

    /* businessSector: BaseForm<any>[] = [
        new FormDummy({
            key: 'apply_to',
            value: 'Myanmar Investment Commision'
        }),
        new FormSelect({
            key: 'business_sector_id',
            placeholder: 'Select sector',
            label: 'c. General business sector of proposed investment',
            options$: this.isicService.getInvestment(),
            required: true,
            criteriaValue: {
                key: 'apply_to',
                value: ['Myanmar Investment Commision'],
            }
        }),
        new FormNote({
            label: 'The general business sectors are classified according to the DICA Investment Divisions.',
            criteriaValue: {
                key: 'apply_to',
                value: ['Myanmar Investment Commision'],
            }
        }),
        new FormSectionTitle({
            label: 'd. Specific sector of the proposed investment :'
        }),
        new FormSelect({
            key: 'isic_section_id',
            label: 'Choose business sections ( ISIC Sections )',
            options$: this.isicService.getSection(),
            placeholder: 'Select sector',
            multiple: true,
            filter: {
                parent: 'business_sector_id',
                key: 'investment'
            },
            required: true,
        }),
        new FormSelect({
            key: 'isic_division_id',
            label: 'Choose business divisions ( ISIC Divisions )',
            options$: this.isicService.getDivison(),
            placeholder: 'Select sector',
            multiple: true,
            filter: {
                parent: 'isic_section_id',
                key: 'section'
            },
            required: true,
        }),
        new FormSelect({
            key: 'isic_group_id',
            label: 'Choose business groups ( ISIC Groups )',
            options$: this.isicService.getGroup(),
            placeholder: 'Select sector',
            multiple: true,
            filter: {
                parent: 'isic_division_id',
                key: 'division'
            },
            required: true,
        }),
        new FormSelect({
            key: 'isic_class_ids',
            label: 'Choose business class(es) ( ISIC Classes )',
            required: true,
            placeholder: 'Select sector',
            multiple: true,
            options$: this.isicService.getClasses(),
            filter: {
                parent: 'isic_group_id',
                key: 'group'
            },
        }),
        new FormNote({
            label: 'The specific sectors are classified according to International Standard Industrial Classification (ISIC) Rev. 4.'
        }),
    ];
    businessCPC: BaseForm<any>[] = [
        new FormSelect({
            key: 'cpc_section_id',
            label: 'Choose business sections ( CPC Section )',
            placeholder: 'Select product(s)/service(s)',
            options$: this.cpcService.getSection(),
            multiple: true,
        }),
        new FormSelect({
            key: 'cpc_division_id',
            label: 'Choose business divisions ( CPC Division )',
            options$: this.cpcService.getDivison(),
            placeholder: 'Select product(s)/service(s)',
            multiple: true,
            filter: {
                parent: 'cpc_section_id',
                key: 'section'
            }
        }),
        new FormSelect({
            key: 'cpc_group_id',
            label: 'Choose business groups ( CPC Group )',
            options$: this.cpcService.getGroup(),
            placeholder: 'Select product(s)/service(s)',
            multiple: true,
            filter: {
                parent: 'cpc_division_id',
                key: 'division'
            }
        }),
        new FormSelect({
            key: 'cpc_class_id',
            label: 'Choose business class(es) ( CPC Classes )',
            options$: this.cpcService.getClasses(),
            placeholder: 'Select product(s)/service(s)',
            multiple: true,
            filter: {
                parent: 'cpc_group_id',
                key: 'group'
            }
        }),
        new FormSelect({
            key: 'cpc_sub_class_ids',
            label: 'Choose business subclass(es) ( CPC Subclasses )',
            options$: this.cpcService.getSubclasses(),
            placeholder: 'Select product(s)/service(s)',
            multiple: true,
            filter: {
                parent: 'cpc_class_id',
                key: 'class'
            }
        })
    ];
    inquiryForm: BaseForm<any>[] = [
        new FormSectionTitle({
            label: '3. Basic information on planned investment'
        }),
        new FormSectionTitle({
            label: 'a. Planned or actual share ratio of enterprise'
        }),
        new FormInput({
            key: 'citizen_share_ratio',
            label: 'Myanmar citizen share ratio:',
            type: 'number',
            min: 0,
            endfix: '%',
            columns: 'col-md-4 d-flex',
            required: true,
            value: "0"
        }),
        new FormInput({
            key: 'foreigner_share_ratio',
            label: 'Foreign share ratio:',
            type: 'number',
            endfix: '%',
            columns: 'col-md-4 d-flex',
            required: true,
            value: "0"
        }),
        new FormInput({
            key: 'government_share_ratio',
            label: 'Government department/ organization share ratio:',
            columns: 'col-md-4 d-flex',
            type: 'number',
            endfix: '%',
            required: true,
            value: "0"
        }),
        new FormSelect({
            key: 'company_type',
            placeholder: 'Select type',
            label: 'b. Planned or actual type of company incorporated in Myanmar',
            options$: this.localService.getCompany(),
            required: true
        }),
        new BaseFormGroup({
            key: 'business_sector',
            formGroup: this.businessSector,
        }),
        new FormSectionTitle({
            label: 'e. Optional: please name the specific product(s)/service(s) of the investment:'
        }),
        new BaseFormGroup({
            key: 'business_cpc',
            formGroup: this.businessCPC
        }),
        new FormNote({
            label: 'Products/services are classified according to Central Product Classification (CPC) Version 2.1.'
        }),
        new FormTextArea({
            key: 'description',
            label: 'f. Detailed description of the proposed investment:',
            required: true
        })
    ]; */

    baseForm: InquiryForm4;
    constructor(
        private router: Router,
        private service: InquiryService,
        public location: Location,
        private toast: ToastrService,
        private isicService: ISICService,
        private cpcService: CPCService,
        private store: Store<AppState>,
        private localService: LocalService,
        private authService: AuthService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
        private formCtlService: FormControlService,
    ) {
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            this.baseForm = new InquiryForm4(null, this.isicService, this.cpcService, this.localService);
            this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            if (this.id > 0) {
                this.service.show(this.id, { secure: true })
                    .subscribe((data: any) => {
                        this.inquiryModel = data as Inquiry;
                        const lan = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                        this.translateService.use(lan);
                        this.baseForm = new InquiryForm4(lan, this.isicService, this.cpcService, this.localService);
                        this.baseForm.businessSector[0].value = this.inquiryModel?.apply_to || "Myanmar Investment Commision";
                        this.inquiryModel.business_sector = this.inquiryModel?.business_sector || {};
                        // Make sure for dummy value;
                        this.inquiryModel.business_sector.apply_to = this.inquiryModel?.apply_to || "Myanmar Investment Commision";

                        this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.inquiryModel);

                        const page = `inquiry/form4/${this.inquiryModel.id}`;
                        this.formCtlService.getComments$('Inquiry', this.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.id, type: 'Inquiry', page: page, comments }));
                        });

                        this.loading = false;
                    });
            }
        }).unsubscribe();

    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.isDraft = true;
        this.saveInquiry();
    }

    onSubmit() {
        this.submitted = true;
        this.store.dispatch(new Submit({ submit: true }));
        if (this.inquiryFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            this.submitted = false;
            return;
        } else {
            const shares = (Number(this.inquiryFormGroup.value.citizen_share_ratio) || 0) + (Number(this.inquiryFormGroup.value.foreigner_share_ratio) || 0) + (Number(this.inquiryFormGroup.value.government_share_ratio) || 0);
            if (Number(shares) === 100) {
                this.isDraft = false;
                this.saveInquiry();
            } else {
                this.toast.error('Your planned or actual share ratio must be 100%.');
            }
        }
    }

    saveInquiry() {
        this.spinner = true;
        this.inquiryModel = {
            ...this.inquiryModel, ...this.inquiryFormGroup.value
        };

        this.service.create(this.inquiryModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;

                if (!this.isDraft) {
                    this.redirectLink();
                }
                else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    redirectLink() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {

            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['officer/inquiry/form5', this.id]);
            } else {
                this.router.navigate(['pages/inquiry/form5', this.id]);
            }
        }
    }
}
