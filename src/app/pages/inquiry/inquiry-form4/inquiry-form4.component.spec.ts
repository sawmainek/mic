import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryForm4Component } from './inquiry-form4.component';

describe('InquiryForm4Component', () => {
  let component: InquiryForm4Component;
  let fixture: ComponentFixture<InquiryForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
