import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryLanguageComponent } from './inquiry-language.component';

describe('InquiryLanguageComponent', () => {
  let component: InquiryLanguageComponent;
  let fixture: ComponentFixture<InquiryLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
