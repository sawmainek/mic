import { Submit } from './../../../core/form/_actions/form.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../../../core/reducers/index';
import { InquiryService } from './../../../../services/inquiry.service';
import { AuthService } from './../../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { FormControlService } from './../../../custom-component/dynamic-forms/form-control.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-inquiry-language',
    templateUrl: './inquiry-language.component.html',
    styleUrls: ['./inquiry-language.component.scss']
})
export class InquiryLanguageComponent implements OnInit {

    id: any;
    user: any = {};
    inquiryModel: any = {};
    loading: boolean = false;
    language: any = "English";
    spinner: boolean = false;

    constructor(
        private router: Router,
        private toast: ToastrService,
        private store: Store<AppState>,
        private authService: AuthService,
        private inquiryService: InquiryService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
    ) {
        this.language = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.language ? this.language : 'en');
        this.translateService.use(this.language ? this.language : 'en');
    }

    ngOnInit(): void {
        this.authService.getCurrentUser().subscribe(result => {
            this.user = result || null;
        });
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            if (this.id > 0) {
                this.inquiryService.show(this.id)
                    .subscribe(result => {
                        this.inquiryModel = result || {};
                        this.language = this.inquiryModel?.language || 'English'
                        this.openLanguage(this.language || 'English');
                        this.loading = false;
                    });
            } else {
                this.loading = false;
                this.language = 'English';
                this.openLanguage(this.language || 'English');
            }
        }).unsubscribe();
    }

    openLanguage(language) {
        if (window.location.href.indexOf('officer') == -1) {
            this.language = language;
        }
    }

    onSubmit() {
        this.spinner = true;
        this.store.dispatch(new Submit({ submit: true }));
        this.inquiryModel = this.inquiryModel || {};
        this.inquiryModel.language = this.language;
        this.inquiryModel.draft = this.inquiryModel.draft || true;
        this.inquiryService.create(this.inquiryModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                this.redirectLink(x.id);
            });
    }

    redirectLink(id: number) {
        if (id == null) {
            this.toast.error("Please login with investor account.");
            return;
        }
        if (window.location.href.indexOf('officer') !== -1) {
            this.router.navigate(['/officer/inquiry/form', id]);
        } else {
            this.router.navigate(['/pages/inquiry/form', id]);
        }
    }

    goToHome() {
        this.router.navigate(['/']);
    }

}
