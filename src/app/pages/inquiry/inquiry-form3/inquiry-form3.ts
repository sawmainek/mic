import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { LocationService } from './../../../../services/location.service';
import { FormSectionTitle } from './../../../custom-component/dynamic-forms/base/form-section-title';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { Validators } from '@angular/forms';
import { FormInput } from './../../../custom-component/dynamic-forms/base/form-input';
import { BaseForm } from './../../../custom-component/dynamic-forms/base/base-form';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
export class InquiryForm3 {
    public forms: BaseForm<string | boolean>[] = []
    constructor(
        private lang: string,
        private lotService: LocationService
    ) {
        const applicantInvestor: BaseForm < any > [] =[
            new FormSectionTitle({
                label: '2. If the investor is not submitting the investment screening form by herself/himself, the applicant’s:'
            }),
            new FormInput({
                key: 'name',
                label: 'a. Full Name'
            }),
            new FormSelect({
                key: 'country',
                label: 'b. Country of incorporation (if company) or citizenship (if individual)',
                options$: this.lotService.getCountry()
            }),
            new FormInput({
                key: 'personal_id_no',
                label: 'c. Passport number (foreign nationals), National Registration Card Number (Myanmar citizens) or company registration number (if company)'
            }),            
            new FormTitle({
                label: 'd.  Address'
            }),
            new FormSelect({
                key: 'address_country',
                label: 'Country',
                options$: this.lotService.getCountry(),
                value: 'Myanmar'
            }),
            new FormSelect({
                key: 'state',
                label: 'State / Region:',
                options$: this.lotService.getState(this.lang),
                columns: 'col-md-4',
                criteriaValue: {
                    key: 'address_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'district',
                label: 'District:',
                options$: this.lotService.getDistrict(this.lang),
                columns: 'col-md-4',
                filter: {
                    parent: 'state',
                    key: 'state'
                },
                criteriaValue: {
                    key: 'address_country',
                    value: ['Myanmar'],
                }
            }),
            new FormSelect({
                key: 'township',
                label: 'Township:',
                columns: 'col-md-4',
                options$: this.lotService.getTownship(this.lang),
                filter: {
                    parent: 'district',
                    key: 'district'
                },
                criteriaValue: {
                    key: 'address_country',
                    value: ['Myanmar'],
                }
            }),
            new FormInput({
                key: 'address',
                label: 'Additional address details',
            }),   
            new FormInput({
                key: 'phone',
                label: 'e.  Phone number',
                type: 'number',
                validators: [Validators.minLength(6)],
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'fax',
                label: 'f. Fax number',
                type: 'number',
                columns: 'col-md-6'
            }),
            new FormInput({
                key: 'email',
                label: 'g. E-mail address',
                validators: [Validators.email]
            }),
        ]
        this.forms = [
            new BaseFormGroup({
                key: 'applicant_investor',
                formGroup: applicantInvestor
            })
        ]
    }
}