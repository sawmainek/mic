import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryForm3Component } from './inquiry-form3.component';

describe('InquiryForm3Component', () => {
  let component: InquiryForm3Component;
  let fixture: ComponentFixture<InquiryForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
