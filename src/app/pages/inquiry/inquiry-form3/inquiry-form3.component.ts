import { TranslateService } from '@ngx-translate/core';
import { LocationService } from './../../../../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { select, Store } from '@ngrx/store';
import { BaseFormData, Submit } from './../../../core/form/_actions/form.actions';
import { Observable } from 'rxjs';
import { FormControlService } from './../../../custom-component/dynamic-forms/form-control.service';
import { BaseFormGroup } from './../../../custom-component/dynamic-forms/base/form-group';
import { FormSelect } from './../../../custom-component/dynamic-forms/base/form-select';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InquiryService } from 'src/services/inquiry.service';
import { Inquiry } from 'src/app/models/inquiry';
import { Location } from '@angular/common';
import { AppState, currentUser } from 'src/app/core';
import { InquiryForm3 } from './inquiry-form3';

@Component({
    selector: 'app-inquiry-form3',
    templateUrl: './inquiry-form3.component.html',
    styleUrls: ['./inquiry-form3.component.scss']
})
export class InquiryForm3Component implements OnInit {
    @Input() id: any;

    user: any = {};

    inquiryModel: Inquiry;
    inquiryFormGroup: FormGroup;

    loading = false;
    spinner = false;
    isDraft = false;

    isRevision: boolean = false;

    /* applicantInvestor: BaseForm<any>[] = [
        new FormSectionTitle({
            label: '2. If the investor is not submitting the investment screening form by herself/himself, the applicant’s:'
        }),
        new FormInput({
            key: 'name',
            label: 'a. Full Name'
        }),
        new FormSelect({
            key: 'country',
            placeholder: 'Select country',
            label: 'b. Country of incorporation (if company) or citizenship (if individual)',
            options$: this.lotService.getCountry(),
        }),
        new FormInput({
            key: 'personal_id_no',
            label: 'c. Passport number (foreign nationals), National Registration Card Number (Myanmar citizens) or company registration number (if company)',
        }),
        new FormInput({
            key: 'address',
            label: 'd.  Address'
        }),
        new FormSelect({
            key: 'state',
            label: 'State / Region:',
            options$: this.lotService.getState(),
            columns: 'col-md-6',
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'district',
            label: 'District:',
            options$: this.lotService.getDistrict(),
            columns: 'col-md-6',
            filter: {
                parent: 'state',
                key: 'state'
            },
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormSelect({
            key: 'township',
            label: 'Township:',
            options$: this.lotService.getTownship(),
            filter: {
                parent: 'district',
                key: 'district'
            },
            criteriaValue: {
                key: 'country',
                value: ['Myanmar'],
            }
        }),
        new FormInput({
            key: 'phone',
            label: 'e.  Phone number',
            type: 'number',
            validators: [Validators.minLength(6)],
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'fax',
            label: 'f. Fax number',
            type: 'number',
            columns: 'col-md-6'
        }),
        new FormInput({
            key: 'email',
            label: 'g. E-mail address',
            validators: [Validators.email]
        }),
    ]
    inquiryForm: BaseForm<any>[] = [
        new BaseFormGroup({
            key: 'applicant_investor',
            formGroup: this.applicantInvestor
        })
    ] */

    baseForm: InquiryForm3;
    constructor(
        private http: HttpClient,
        private activatedRoute: ActivatedRoute,
        private service: InquiryService,
        private lotService: LocationService,
        private formCtlService: FormControlService,
        private translateService: TranslateService,
        private router: Router,
        private store: Store<AppState>,
        private toast: ToastrService,
        public location: Location,
    ) {
        this.loading = true;

    }

    ngOnInit(): void {
        this.getCurrentUser();

        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        this.isRevision = params.get('action') == 'Edit' ? true : false;

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = +paramMap.get('id') ? +paramMap.get('id') : 0;
            this.baseForm = new InquiryForm3(null, this.lotService);
            this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms);
            if (this.id > 0) {
                this.service.show(this.id, { secure: true })
                    .subscribe((x: any) => {
                        this.inquiryModel = x as Inquiry;
                        const lan = this.inquiryModel?.language == 'Myanmar' ? 'mm' : 'en';
                        this.translateService.use(lan);
                        this.baseForm = new InquiryForm3(lan, this.lotService);
                        this.inquiryFormGroup = this.formCtlService.toFormGroup(this.baseForm.forms, this.inquiryModel);

                        const page = `inquiry/form3/${this.inquiryModel.id}`;
                        this.formCtlService.getComments$('Inquiry', this.id).subscribe(comments => {
                            this.store.dispatch(new BaseFormData({ id: this.id, type: 'Inquiry', page: page, comments }));
                        });

                        this.loading = false;
                    });
            }
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    saveDraft() {
        this.isDraft = true;
        this.saveInquiry();
    }

    onSubmit() {
        this.store.dispatch(new Submit({ submit: true }));
        if (this.inquiryFormGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }
        this.isDraft = false;
        this.saveInquiry();
    }

    saveInquiry() {
        this.spinner = true;
        this.inquiryModel = { ...this.inquiryModel, ...this.inquiryFormGroup.value };
        this.service.create(this.inquiryModel, { secure: true })
            .subscribe(x => {
                this.spinner = false;

                if (!this.isDraft) {
                    this.redirectLink();
                }
                else {
                    this.toast.success('Saved successfully.');
                }
            });
    }

    fromUrl(url: string): Observable<any[]> {
        return this.http.get<any[]>(url);
    }

    redirectLink() {
        // Checking for Revision
        const params = new URLSearchParams(window.location.search);
        if (params.get('action') == 'Edit') {
            this.location.back();
        } else {

            if (window.location.href.indexOf('officer') !== -1) {
                this.router.navigate(['officer/inquiry/form4', this.id]);
            } else {
                this.router.navigate(['pages/inquiry/form4', this.id]);
            }
        }
    }

}
