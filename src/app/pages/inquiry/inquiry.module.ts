import { InquirySuccessComponent } from './inquiry-success/inquiry-success.component';
import { CustomComponentModule } from './../../custom-component/custom-component.module';
import { InquiryForm7Component } from './inquiry-form7/inquiry-form7.component';
import { InquiryForm6Component } from './inquiry-form6/inquiry-form6.component';
import { InquiryForm5Component } from './inquiry-form5/inquiry-form5.component';
import { InquiryForm4Component } from './inquiry-form4/inquiry-form4.component';
import { InquiryForm3Component } from './inquiry-form3/inquiry-form3.component';
import { InquiryForm2Component } from './inquiry-form2/inquiry-form2.component';
import { InquiryForm1Component } from './inquiry-form1/inquiry-form1.component';
import { InquiryRoutingModule } from './inquiry-routing.module';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { GlobalModule } from '../global.module';
import { InquiryFormComponent } from './inquiry-form/inquiry-form.component';
import { InquiryStateAndRegionComponent } from './inquiry-state-and-region/inquiry-state-and-region.component';
import { InquiryPaymentComponent } from './inquiry-payment/inquiry-payment.component';
import { InquiryLanguageComponent } from './inquiry-language/inquiry-language.component';

@NgModule({
    declarations: [
        InquiryForm1Component,
        InquiryForm2Component,
        InquiryForm3Component,
        InquiryForm4Component,
        InquiryForm5Component,
        InquiryForm6Component,
        InquiryForm7Component,
        InquirySuccessComponent,
        InquiryFormComponent,
        InquiryStateAndRegionComponent,
        InquiryPaymentComponent,
        InquiryLanguageComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        GlobalModule,
        InquiryRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // closeButton: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ],
    exports: [
        InquiryForm1Component,
        InquiryForm2Component,
        InquiryForm3Component,
        InquiryForm4Component,
        InquiryForm5Component,
        InquiryForm6Component,
        InquiryForm7Component,
        InquirySuccessComponent,
        InquiryFormComponent,
        InquiryStateAndRegionComponent,
    ], providers: [
        TranslateService,
    ],
})
export class InquiryModule { }
