import { FormLogService } from './../../../services/form-log.service';
import { Loaded } from './../../core/form/_actions/form.actions';
import { loaded } from './../../core/form/_selectors/form.selectors';
import { ChangeDetectorRef, Component } from '@angular/core';
import { AppsConstantProvider } from 'src/app/provider/constant.app';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AppState, currentUser, Logout } from 'src/app/core';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/services/auth.service';
declare var jQuery: any;

@Component({
    selector: 'dica-base',
    templateUrl: './base-component.html',
    styleUrls: ['./base-component.scss'],
})
export class BaseComponent {

    username: any;
    user: any;
    language: any = 'en';
    is_status: boolean = false;
    enableLang: boolean = true;
    formLogs: any[] = [];

    constructor(
        public appsConstant: AppsConstantProvider,
        private router: Router,
        private store: Store<AppState>,
        private formLogService: FormLogService,
        private translateService: TranslateService,
        private authService: AuthService,
        private cdr: ChangeDetectorRef,
    ) {
        this.language = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
        this.translateService.setDefaultLang(this.language ? this.language : 'en');
        this.translateService.use(this.language ? this.language : 'en');

        router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.is_status = window.location.href.indexOf('status') !== -1 ? true : false;
                this.enableLang = (window.location.href.indexOf('pages/inquiry') !== -1 || window.location.href.indexOf('pages/mic') !== -1 || window.location.href.indexOf('pages/endorsement') !== -1 || window.location.href.indexOf('pages/tax-incentive') !== -1 || window.location.href.indexOf('pages/land-right') !== -1)  ? false : true;
            }           
            
        });

        this.authService.changeLanguage$.subscribe(lan => {
            this.language = lan;
            this.translateService.setDefaultLang(lan);
            this.translateService.use(lan);
        })

        jQuery(document).on("wheel", "input[type=number]", function (e) {
            jQuery(this).blur();
        });
        
    }

    ngOnInit(): void {
        this.hideLoading();
        
        this.store
            .pipe(
                select(currentUser),
                map((result: any) => {
                    return result;
                })).subscribe(user => {
                    if (user) {
                        this.user = user;
                        this.username = user.name;
                        this.formLogService.get({
                            search: `incoming:equal:false|submitted_user_id:equal:${this.user?.id}`,
                            // search: `submitted_user_id:equal:${this.user?.id}`,
                            sort: 'id',
                            order: 'desc',
                            rows: 6
                        }).subscribe(results => {
                            this.formLogs = results;
                        });
                    }
                });

        this.store
            .pipe(select(loaded)).subscribe(loaded => {
                if (loaded) {
                //    this.hideLoading();
                } else {
                    // this.showLoading();
                }
            });

        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            this.showLoading();
            // this.store.dispatch(new Loaded({loaded: false}));
            window.scrollTo(0, 0);
        });
    }

    gotoStatusCheckerDetail(log) {
        log.seen = true;
        this.formLogService.update(log.id, { seen: true }).subscribe();
        if (log.type == 'Inquiry ( Form 1 )' && log?.type_id) {
            this.router.navigate(['/pages/status-checker-detail', log?.type_id, 'inquiry']);
        }
        if (log.type == 'MIC-Permit' && log?.type_id) {
            this.router.navigate(['/pages/status-checker-detail', log?.type_id, 'mic']);
        }
        if (log.type == 'Endorsement' && log?.type_id) {
            this.router.navigate(['/pages/status-checker-detail', log?.type_id, 'endorsement']);
        }
        if (log.type == 'Tax Incentive' && log?.type_id) {
            this.router.navigate(['/pages/status-checker-detail', log?.type_id, 'tax']);
        }
        if (log.type == 'Land Right' && log?.type_id) {
            this.router.navigate(['/pages/status-checker-detail', log?.type_id, 'landright']);
        }
    }

    showLoading() {
        jQuery('body').removeClass('loaded');
    }

    hideLoading() {
        jQuery('body').addClass('loaded');
    }

    logout() {
        this.store.dispatch(new Logout());
        this.router.navigateByUrl('/auth/login');
    }

    changeLanguage(lan) {
        this.language = lan;
        localStorage.setItem('lan', lan);
        this.translateService.use(lan);
        this.authService.changeLanguage$.next(lan);
        this.cdr.detectChanges();
    }

    get showNoti(): boolean {
        return this.formLogs.filter(x => !x.seen).length > 0 ? true : false;
    }

}
