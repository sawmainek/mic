import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
declare var jQuery: any;

@Component({
  selector: 'app-common-footer',
  templateUrl: './common-footer.component.html',
  styleUrls: ['./common-footer.component.scss']
})
export class CommonFooterComponent implements OnInit {
  @Input() page = 'home';
  lang = 'en';

  constructor(
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
  }

  ngOnInit(): void {
  }

  changeLanguage(lan) {
    this.lang = lan;
    localStorage.setItem('lan', lan);
    this.translateService.use(lan);
    this.cdr.detectChanges();
  }

}
