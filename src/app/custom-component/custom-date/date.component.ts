import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';
import { Component, Input, OnInit, forwardRef, Output, EventEmitter } from '@angular/core';
declare var jQuery: any;

@Component({
    selector: 'ng-date-picker',
    templateUrl: './date.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DatePickerComponent),
            multi: true
        }
    ]
})

export class DatePickerComponent implements OnInit, ControlValueAccessor {

    @Input() id;
    @Input() placeholder;
    @Input() dateView;
    @Input() readOnly;
    @Input() invalid;
    @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

    onChange: any = () => { }
    onTouch: any = () => { }

    @Input() datePickerControl: FormControl;
    public disabled: boolean;

    constructor() {
        this.datePickerControl = new FormControl('', []);
    }

    ngOnInit() {
        this.datePickerControl.valueChanges.subscribe((data) => {
            if (data && !this.disabled) {
                this.propagateChange();
            }
        });
        // this.loadScripts();
    }

    ngAfterViewInit(): void {
        if (!this.readOnly) {
            setTimeout(() => {
                if (this.dateView == 'year') {
                    jQuery(`.m-date-picker`).datepicker({
                        format: "yyyy",
                        viewMode: "years",
                        minViewMode: "years"
                    });
                }
                else if (this.dateView == 'month') {
                    jQuery(`.m-date-picker`).datepicker({
                        format: 'mm-yyyy',
                        startView: "months",
                        minViewMode: "months",

                        todayHighlight: true,
                        autoclose: true
                    });
                }
                else {
                    jQuery(`.m-date-picker`).datepicker({
                        format: 'dd-mm-yyyy',
                        todayHighlight: true,
                        autoclose: true
                    });
                }

            }, 300);
            jQuery('body').on('change', `.m-date-picker`, function () {
                jQuery(this).trigger('click');
            });
        }
    }

    ngOnDestroy() {
        //TODO destroy dakepicker
    }

    /**
   * Function registered to propagate a change to the parent
   */
    public propagateChange: any = () => { };

    /**
     * Function registered to propagate touched to the parent
     */
    public propagateTouched: any = () => { };
    /**
     * ControlValueAccessor Interface Methods to be implemented
     */
    writeValue(value: any): void {
        this.datePickerControl.setValue(value);
    }
    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.propagateTouched = fn;
    }
    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }
    /** ControlValueAccessor Interface Methods to be implemented */

    onDateChange(value) {
        this.onTouch(value);
        this.onChange(value);
        this.datePickerControl.setValue(value);
        this.valueChange.emit(value);
    }

    // private loadScripts() {
    //     // You can load multiple scripts by just providing the key as argument into load method of the service
    //     this.dynamicScriptLoader.load('date-jquery').then(data => {
    //         // Script Loaded Successfully
    //     }).catch(error => console.log(error));

    //     this.dynamicScriptLoader.load('date-bootstrap').then(data => {
    //         // Script Loaded Successfully
    //     }).catch(error => console.log(error));

    // }

}
