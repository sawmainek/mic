import { RabbitConverterService } from './../../services/rabbit-converter.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from './../../environments/environment';
import { FormHidden } from './dynamic-forms/base/form-hidden';
import { FormDummy } from './dynamic-forms/base/form-dummy';
import { FormTextArea } from './dynamic-forms/base/form-textarea';
import { NgPreviewModalContent } from './preview.modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { FormSelect } from './dynamic-forms/base/form-select';
import { FormDate } from './dynamic-forms/base/form.date';
import { FormInput } from './dynamic-forms/base/form-input';
import { FormSectionTitle } from './dynamic-forms/base/form-section-title';
import { FormTitle } from './dynamic-forms/base/form-title';
import { BaseForm } from './dynamic-forms/base/base-form';
import { FormRadio } from './dynamic-forms/base/form-radio';
import { FormCheckBox } from './dynamic-forms/base/form-checkbox';
import { FormFile } from './dynamic-forms/base/form.file';
import { BaseFormGroup } from './dynamic-forms/base/form-group';

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import pdfMakeUnicode from 'pdfmake-unicode';
// pdfMake.vfs = pdfFonts.pdfMake.vfs;
// pdfMake.vfs = pdfMakeUnicode.pdfMake.vfs;
@Injectable()
export class PrintPDFService {
    translates: any = {};
    constructor(
        private modalService: NgbModal,
        private translateService: TranslateService,
        private rabbitConverter: RabbitConverterService
    ) {
        
    }

    toFormPDF(forms: BaseForm<string|boolean>[], data: any, title: string, option: {
        readOnly?: boolean,
        language?: string,
        onSubmit$?: any
    }) {
        this.translateService.getTranslation(option.language || 'en').subscribe(result => {
            this.translates = result;
            let fonts = {
                unicode: {
                    normal: window.location.origin + '/assets/fonts/Pyidaungsu/Pyidaungsu-2.5.3_Regular.ttf',
                    bold: window.location.origin + '/assets/fonts/Pyidaungsu/Pyidaungsu-2.5.3_Bold.ttf'
                },
                zawgyi: {
                    normal: window.location.origin + '/assets/fonts/Zawgyi-One_V3.1.ttf',
                    bold: window.location.origin + '/assets/fonts/Zawgyi-One_V3.1.ttf'
                }
            }
            var docDefinition = {
                footer: function (currentPage, pageCount) {
                    return [
                        { text: currentPage.toString() + ' of ' + pageCount, margin: [50, 20], alignment: 'right' }
                    ]
                },
                pageSize: 'A4',
                pageMargins: [50, 60, 50, 60],
                defaultStyle: {
                    font: 'zawgyi',
                    lineHeight: 1
                },
                content: [
                    { text: this.rabbitConverter.uni2zg(title || ''), fontSize: 22, bold: true, alignment: 'center', margin: [0, 0, 0, 20] },
                    ...this.toFormData(forms, data)
                ]
            };

            // pdfMake.createPdf(docDefinition).open();
            const pdfDocGenerator = pdfMake.createPdf(docDefinition, null, fonts);
            pdfDocGenerator.getBlob((blob) => {
                const modalRef = this.modalService.open(NgPreviewModalContent, {
                    size: 'xl'
                });
                modalRef.componentInstance.file = {
                    url: URL.createObjectURL(blob),
                    type: 'application/pdf',
                    readOnly: option.readOnly,
                    onSubmit: option.onSubmit$
                };
                modalRef.result.then((result) => {
                });
            });
        })
        
    }

    toFormData(forms, data: any = {}) {
        let _content: any[] = [];
        forms.forEach((x: BaseForm<string>) => {
            switch (x.controlType) {
                case 'form_group':
                    if (this.hasCriteria(x, data)) {
                        _content = [
                            ..._content,
                            ...this.toFormData(x.formGroup, data[x.key] || {})
                        ]
                    }
                    break;
                case 'form_array':
                    if (this.hasCriteria(x, data)) {
                        if (x.useTable) {
                            _content = [
                                ..._content,
                                ...this.getTable(x, data[x.key] && data[x.key] || [])
                            ]
                        } else {
                            x.defaultLength = data[x.key] && data[x.key].length || x.defaultLength || 1;

                            for (let i = 0; i < x.defaultLength; i++) {
                                _content = [
                                    ..._content,
                                    ...this.toFormData(x.formArray, data[x.key] && data[x.key][i] || {})
                                ];
                            }
                        }
                    }
                    break;
                default:
                    if (this.hasCriteria(x, data)) {
                        _content = [
                            ..._content,
                            ...this.getContent(x, data)
                        ]
                    }
                    break;
            }
        })
        return _content;
    }

    getTable(form: BaseForm<string>, data: any[] = []): any[] {
        let headers = [];
        let footers = [];
        if (form.tablePDFHeader || form.tableHeader) {
            (form.tablePDFHeader || form.tableHeader).map((x: any) => {
                let header = [];
                x.forEach(table => {
                    header.push({ text: this.rabbitConverter.uni2zg(table?.label || ''), bold: true, fillColor: '#cccccc', colSpan: table.colSpan || 0, rowSpan: table.rowSpan || 0, alignment: table?.style?.alignment, width: table?.style?.width, margin: [5, 3, 5, 3] });
                });
                headers.push(header);
            })
        } else {
            let header = []
            form.formArray.map((x: any) => {
                if(!(x instanceof FormDummy)) {
                    header.push({ text: this.rabbitConverter.uni2zg(x?.label || ''), bold: true, fillColor: '#cccccc', margin: [5, 3, 5, 3] })
                }
            })
            headers.push(header);
        }
        if(form.tablePDFFooter) {
            form.tablePDFFooter.map(rows => {
                let footer = [];
                rows.forEach(x => {
                    if (x.cellFn) {
                        footer.push({ text: x.cellFn(data), bold: true, fillColor: '#cccccc', colSpan: x.colSpan || 0, rowSpan: x.rowSpan , margin: [5, 8, 5, 8]});
                    } else {
                        footer.push({ text: this.rabbitConverter.uni2zg(x.label || ''), bold: true, fillColor: '#cccccc', colSpan: x.colSpan || 0, rowSpan: x.rowSpan, margin: [5, 8, 5, 8] });
                    }
                })
                footers.push(footer);
            });
        }
        let content = [
            {
                table: {
                    widths: form?.headerWidths,
                    headerRows: form.headerRows || 1,
                    body: [
                        ...headers,
                        ...(data || []).map((row) => {
                            let _data = [];
                            (form.formArray).forEach((form) => {
                                if (!(form instanceof FormDummy)) {
                                    if(form.callbackFn) {
                                        _data.push({ text: form.callbackFn(row), margin: [5, 8, 5, 8] });
                                    } else {
                                        _data.push({ text: this.rabbitConverter.uni2zg(row[form.key] || ''), margin: [5, 8, 5, 8] });
                                    }
                                }
                            })
                            return _data;
                        }),
                        ...footers
                    ]
                }
            },
            '\n'
        ];
        
        return content;
    }

    getContent(form: BaseForm<string>, data: any = {}): any[] {
        let content: any = [];
        
        if (form instanceof FormTitle || form instanceof FormSectionTitle) {
            if (this.hasCriteria(form, data)) {
                content = ['\n', { text: `${this.rabbitConverter.uni2zg(this.translates[form.label] || form.label)}`, type: form.controlType, bold: true, color: '#1A944E', pageBreak: form.pageBreak || '', pageOrientation: form.pageOrientation, font: 'zawgyi' }, '\n']
            }
        } else if (form instanceof FormInput || form instanceof FormDate || form instanceof FormSelect) {
            if(form.label) {
                content = [
                    {
                        columns: [
                            { width: '40%', text: `${this.rabbitConverter.uni2zg(this.translates[form.label] || form.label)}` },
                            { width: '60%', text: `: ${this.rabbitConverter.uni2zg(data[form.key] || '')}` }
                        ],
                        columnGap: 10
                    },
                    '\n'
                ]
            }
            
        } else if (form instanceof FormTextArea) {
            content = [
                { text: `${this.rabbitConverter.uni2zg(this.translates[form.label] || form.label)}` },
                '\n',
                { text: `${this.rabbitConverter.uni2zg(data[form.key] || '')}` },
                '\n'
            ]
        } else if (form instanceof FormRadio) {
            content = [
                {
                    columns: [
                        { width: 50, fit: [16, 16], image: form.value == `${data[form.key] || ''}` ? 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABO1BMVEUAAADItgDGuADFuADFugDFuQDFuQDFuQDFuQDEsQDEtwDFuQDFuQDFuQDGuQDIugDMswDFugDFuQDGuQC/vwDFuQDFuADFugDFuQDFuQDCuADGuwDFuQDEugDDtADFuQDFuQDFuQDItwDFuQC/vwDFuQDFuQDFuQD//wDGugDGugDGuwDFuQDFuQDEuQDFuADFuQDKtQDVqgDFuQDFuQDFuQDEuADCtgDEuQDFuQDGugDGuADEuQDEuADFuQDFuQDGuQDGuQDGuQDFuADGugDFuQDFuQDFuQDFuQDDtwDFuQDGugDEuADGugDFuQDFuADFuADCtgDGugDEuQDJvADGugC/vwDEuQDMzADEuADFuQDFuQDDvADFuQDEuQDFuQC2tgDFuQDEuQDFuQDGuQDGugDEuADFuQAAAACiBZb1AAAAZ3RSTlMADlqbyur6mlgNJ5329ZwlCoz7igjTvHY+xxkx8HoRe/HjLqgUqv58AX2BLZHU9JAsGAaW/fyUFXm9kxJXK76ZjnjI69Xp+fPyQPg/QVnJd40qv5gTVQy7BX4WrCJ/h8UH4lvGhpdW9XmKEwAAAAFiS0dEAIgFHUgAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfkCxQOMSeAY50+AAAB10lEQVQ4y31T51rCQBA86ahYCSTYUMEYxCBiREVpSrA3ECt23fd/A3cvQHLwfd6vm9m5LZMNY70z5HJ7vD6f1+MPBNngGR4Zhd4JjY33hScmp0A4vumwMz4uETkVibpk2RWNcLESs+MzXiRm5+a7eH4hjsTiUu89xZcTzpSJJCk6OSYw/4q6Kja1qq1gFauPSRSrg2NpSKf4fNjSsvV+Lb2u6+uZDSsHVtGzeBnB/nj9zdyWNaKxvUk4j53m0L8QwA6P79ou7HKFiimCzIWm8PlyTp/2iClg8X3mBojw+ltOgXFAXBGgxDwAUQJp0eoycRWAKjsECBA4EgUScVi+xkwAmUBdFBwTdwJwSoIzAseiIE7cOQmwxMW/JS4BrghkRMF1p0mF+dEWAhuGMOYNcbcADRboGrXtFKSJaaJRd6ylAyxwqyN2/P6BmEe0usXYGLac54q9ThUjzeNPOJif9skHkLQ+90FZqtel6xvrcz8DmC90m8ZH2uDCtJF+5bewgiun9a9cG1fu7d0CsUUUJ/PO+BPmh49sFy6RIq4WuripkfEfa/aDmMJ/nGLlU5Y/K0X+43xlnSnDKV202vx+72s6m3NI9NLL4Fistd+o1kyzVv35bdnsH2hYuK3k9ICZAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIwLTExLTIwVDE0OjQ5OjM5KzAwOjAwYEjcswAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMC0xMS0yMFQxNDo0OTozOSswMDowMBEVZA8AAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC' : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABCFBMVEUAAADItgDGugDFuADFugDFuQDFuQDFuQDFuQDEsQDDvADFuQDFuQDGuQDIugDGxgDFugDFuQDGuQC/vwDFuQDFuQDCuADGuwDFuQDFuQDGuwDEtwC5uQDGugDFuQDFuQDItwDGuQCqqgDEuQDFuQDKtQDFuQC/vwDFuAC2tgDFuAD/gADFuQDGugDFuQDFuQDFuQDFuADEugDEuQDEuQDEuADFuQDGuADEugDGuQDFuADFuQDFuQDFuQDFuQDFuQDEuQC/vwDGugDGugDFuQDGuQDDvADEuQDFuQDFuQDGugDGuADGvQDEuQDGuQDGuwDCtgDFuQDFuQDGuQDGugDEuADFuQAAAABhvPmZAAAAVnRSTlMADlmbyur6mlgNJp31nCUJjPuKCNPHGTHhkkcnC5Pi4y5iA2PkGK8MsQeQApGB8+WWYWT0V5SZSErI6+n5+MmVmARVZ+ayIoezxW9eG0koLRXBxoaXVg4+takAAAABYktHRACIBR1IAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH5AsUDjMYBDPSgQAAAXFJREFUOMuFU+lawkAMTIEWyiWCqJzFQikICFpAkCrggTfeff9HMVmk9NgP8yPNZmZ3stkUwDYhEAyJkiSGwhEZ/BaNxS3b4omkB95J7Vouk9IZJ57cs3yW3d/gByJLHebyhWKxkC+V2VKp2PsZrhyp64RarbGM9qfPzq/rTk25wVSabJGi+LjlrrrVpmyH3Y/qr3twZNAZJyQSIzUdfCZ3Eehh/6g/p5zOwRkChgwBup/KI6gKQn0Ios8B1wYIDSGE/pxPGCE0hgv0UT6hgNAETPQ6n6AjdLmNIDDCvxJX24vMQhh9iU+YIjSDCPoyt1FzatQ16AZ+qjzCDbUa60/gt8YZY+EWgTAGSQmDhv+57zBtLihM02i0vQOTo+w9izNZihsuFeGBco+rkQONTbH4NLe3P5O+1X1ZJyrKas4Hr0tdX46mq2X5bXPi+4f/x/nUnJrNjuGGza+m51paz0Exhgve8/dn44lpTsbfP473/wWoAnJHosyiBgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMC0xMS0yMFQxNDo1MToyNCswMDowMB9sItkAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMTEtMjBUMTQ6NTE6MjQrMDA6MDBuMZplAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==' },
                        { width: '90%', text: `${this.rabbitConverter.uni2zg(this.translates[form.label] || form.label)}` }
                    ]
                },
                '\n'
            ]
        } else if (form instanceof FormCheckBox) {

            content = [
                {
                    columns: [
                        { width: 50, fit: [16, 16], image: data[form.key] ? 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAt1BMVEUAAADGuADEuQDFuQDFuQDFugDEuADFuQDFuQDEuQDFuQDFuQDGuQDFuQDFuQDFuQDFuADFuQDEuQD//wDFuQDFugDCuACqqgDEuQDFugDGxgDGuwDFuQDFuQDMswDFugDFuQDGuwDFugDFuQDFuADDuADFuQDFuADItwDHtwDFuQDHuADIugDFuADGugDFuQDDuQD/gADFugDFuQDFuADItgDGuQDFuQDFuADGugDEuQDFuQAAAACnVS8kAAAAO3RSTlMASLvyukZ+fP5F94NmhLnxhfBfAVh2GQOl4AkxpOIKouotcuzOL6PrLiDtMiWxl+4zAp5UdxyG+LhDebb7WZQAAAABYktHRACIBR1IAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH5AsUDwEPa1JpAAAAANJJREFUOMvNk1cXwUAQhYcgCUIKovfeu3D///+yqxyW3X3lvn7flpkzQ0SxuAFpjESSWFLQxGTndRyWTXEgnclK46SBHOUBhxRxAY/YPb5K8Bn8lRAUiiWdEJaBSlUtcA7UlEJY57zRVAlhi/N2R/WHdy4K3V5/wPiQ89H4u8yJBUxnIheEOSfTxY0PpI1aPkfgxUUheBirtarVd+Odf5a52Qr3S/oQ7PaHvxkYQWBj76qEIx/7BFscV744xxMQUVK/emci09LwC3/Jznly7EXs/BWl/GOimOywGQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMC0xMS0yMFQxNTowMToxNSswMDowMJtVW0kAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMTEtMjBUMTU6MDE6MTUrMDA6MDDqCOP1AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==' : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAilBMVEUAAADVqgDFuQDFuQDEuQDFuQDMzADFuQDFugDFuQD/gADEuQDFuQDFuQDEuQDFuADGugDFuADEuADFuQDFuQDFuQDGuADDuQDEuQDGugDFuQDFuQDFuQDFuQDFuQDEuwDGugDFuQDGuwDEuQDEuQDFuQDCtgDGuQDFuQDEuADFuQDGugDFuQAAAABVAOJMAAAALHRSTlMABmrG9GkFFsrJAsT70LvDWdkrLFiwNjeu1erp39avOK3aLVfRwRXIx2jyZxV/HeMAAAABYktHRACIBR1IAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH5AsUDwIZtKuPkgAAAJdJREFUOMvtk80agVAURQ+lKEmKRH4iIev9n8+JYfd+ZkatwdmDvb87uktEZDR2XHq4zsSTD/4UCzO/64MQ5tGiRzSHONDBEpKVGEgTyDTXsBEjOWw1CnZiYU+ht+RgGxSUw2AY/H/w89MeITf3JzhrZFClpv5SwVUziFW9uq9ererdOvWkudvkfTTft7xn2PbbNnx1+r8BcAUqDkJp++kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjAtMTEtMjBUMTU6MDI6MjUrMDA6MDD+7eepAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIwLTExLTIwVDE1OjAyOjI1KzAwOjAwj7BfFQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII=' },
                        { width: '90%', text: `${this.rabbitConverter.uni2zg(this.translates[form.label] || form.label)}` }
                    ]
                },
                '\n'
            ]
        } else if (form instanceof FormFile) {
            content = [
                {
                    type: form.controlType,
                    columns: [
                        { width: '40%', text: `${this.rabbitConverter.uni2zg(this.translates[form.label] || form.label)}` },
                        { width: '60%', text: `: ${this.rabbitConverter.uni2zg(data[form.key] && data[form.key]['name'] || '')}` }
                    ],
                    columnGap: 10
                },
                '\n'
            ]
        }
        return content;
    }

    hasCriteria(form: BaseFormGroup, data: any): boolean {
        if (form.criteriaValue) {
            if (form.criteriaValue && this.matchValue(form.criteriaValue.value, data[form.criteriaValue.key])) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    matchValue(values, criteriaValue): boolean {
        if (Array.isArray(values)) {
            return values.includes(criteriaValue);
        }
        return values == criteriaValue || false;
    }
}