import { NgPreviewModalContent } from './preview.modal.component';
import { DatePickerComponent } from './custom-date/date.component';
import { NgDocumentModalContent } from './dynamic-forms/document.modal.component';
import { NgCommentModalContent } from './dynamic-forms/comment.modal.componect';
import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { FormLayoutComponent } from './dynamic-forms/form-layout.component';
import { MSDTSelectComponent } from './custom-select2/msdt-select2-component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './dynamic-forms/form.component';
import { FilterPipe } from './dynamic-forms/filter.pipe';
import { TooltipModule } from 'ng2-tooltip-directive';
import { LoaderComponent } from './loader/loader.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';



@NgModule({
    declarations: [
        MSDTSelectComponent,
        DatePickerComponent,
        FormComponent,
        FormLayoutComponent,
        FilterPipe,
        NgCommentModalContent,
        NgDocumentModalContent,
        NgPreviewModalContent,
        LoaderComponent
    ],
    entryComponents: [
        MSDTSelectComponent,
        DatePickerComponent,
        FormComponent,
        FormLayoutComponent,
        NgCommentModalContent,
        NgDocumentModalContent,
        NgPreviewModalContent
    ],
    imports: [
        CommonModule,
        TooltipModule,
        ReactiveFormsModule,
        NgxDocViewerModule,
        FormsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
    ], 
    exports: [
        MSDTSelectComponent,
        DatePickerComponent,
        FormComponent,
        LoaderComponent,
        FormLayoutComponent
    ], providers: [
        TranslateService,
    ]
})
export class CustomComponentModule { }
