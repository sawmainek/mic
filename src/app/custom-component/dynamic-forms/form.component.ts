import { NgDocumentModalContent } from './document.modal.component';
import { FileUploadService } from './../../../services/file_upload.service';
import { currentRole } from './../../core/auth/_selectors/auth.selectors';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { environment } from 'src/environments/environment';
import { NgCommentModalContent } from './comment.modal.componect';
import { currentForm, submitted } from './../../core/form/_selectors/form.selectors';
import { Store, select } from '@ngrx/store';
import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseForm } from './base/base-form';
import { AppState } from 'src/app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
declare var jQuery: any;

@Component({
    selector: 'ng-form-control',
    styleUrls: ['./form.component.scss'],
    templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

    @Input() unqKey: string;
    @Input() formField: BaseForm<string>;
    @Input() formGroup: FormGroup;
    @Input() index: number;


    host: string = environment.host;
    currentForm: any = {};
    comments: any[] = [];
    commentable: boolean = false;
    submitted: boolean = false;

    constructor(
        private store: Store<AppState>,
        private modalService: NgbModal,
        private fileUploadService: FileUploadService,
    ) {
        this.store.pipe(select(submitted))
            .subscribe(submit => {
                this.submitted = submit
            });
    }

    ngOnInit(): void {
        this.formField.unqKey = this.unqKey;
        if (this.formField.key) {
            this.formGroup.get(this.formField.key).valueChanges.subscribe(value => {
                if (this.formField.valueChangeEvent) {
                    this.formField.valueChangeEvent(value, this.index, this.formField, this.formGroup);
                }
                if (this.formField.valueChanges$) {
                    this.formField.valueChanges$.next(value);
                }
            })
        }
        this.store.pipe(select(currentForm))
            .subscribe(currentForm => {
                this.currentForm = currentForm;
                if (Array.isArray(this.currentForm.comments)) {
                    this.comments = currentForm.comments.filter(x => x.page == this.currentForm.page).slice() || [];
                }
            });
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user && user.type == 'officer' && window.location.href.indexOf('officer') !== -1) {
                    this.formField.readonly = this.formField.editable ? false : true;
                }
            });
        this.store.pipe(select(currentRole))
            .subscribe(roles => {
                if (window.location.href.indexOf('officer') !== -1 && roles.indexOf('checking') !== -1 || roles.indexOf('approval') !== -1) {
                    this.commentable = this.formField.commentable == false ? false : true;
                }
            });
    }

    get commented() { return this.comments.filter(x => x.key == this.unqKey)[0] || null }
    get filterKey() { return this.formField.filter ? this.formField.filter.key : null; }
    get filterValue() { return this.formField.filter ? this.values[this.formField.filter.parent].value || this.formField.value : null; }
    get values() { return this.formGroup.controls; }
    get isValid() { return this.formGroup.controls[this.formField.key].valid; }
    get hasCriteria(): boolean {
        if (this.formField.criteriaValue) {
            if (this.formField.criteriaValue && this.matchValue(this.formField.criteriaValue.value, this.values[this.formField.criteriaValue.key].value)) {
                if (this.formField.key) {
                    let validators: any[] = this.formField.required ? [Validators.required] : [];
                    validators = [...validators, ...this.formField.validators];
                    this.formGroup.controls[this.formField.key].setValidators(validators);
                    this.formGroup.controls[this.formField.key].updateValueAndValidity();
                }
                return true;
            } else {
                if (this.formField.key) {
                    this.formGroup.controls[this.formField.key].setValue('');
                    this.formGroup.controls[this.formField.key].clearValidators();
                    this.formGroup.controls[this.formField.key].updateValueAndValidity();
                }
                return false;
            }
        }
        return true;
    }

    matchValue(values, criteriaValue): boolean {
        if (Array.isArray(values)) {
            return values.includes(criteriaValue);
        }
        return values == criteriaValue || false;
    }

    onCheckChange(value) {
        this.formGroup.controls[this.formField.key].setValue(value ? value : '');
    }
    onRadioChange(value) {
        this.formGroup.controls[this.formField.key].setValue(value);
    }
    onSelectChange(value) {
        if (this.formField.optionTriggerFn) {
            this.formField.optionTriggerFn(value, this.index);
        }
        if (this.formField.valueChanges$) {
            this.formField.valueChanges$.next(value);
        }
    }
    onDateChange(value) {
        this.formGroup.controls[this.formField.key].setValue(value);
    }

    onFileChange(file) {
        if ((file.size / 1024) > (2 * 1024)) { // 2 MB
            alert('The file size must be less than 2 MB.');
            return;
        }

        this.formGroup.controls[this.formField.key].setValue(file);
    }

    removeFile() {
        if(confirm('Are you sure to remove this file?')) {
            if (this.values[this.formField.key].value) {
                if (this.values[this.formField.key].value instanceof File) {
                    this.formGroup.controls[this.formField.key].setValue(null);
                } else {
                    if(this.values[this.formField.key].value.id) {
                        this.fileUploadService.destroy(this.values[this.formField.key].value.id).subscribe();
                    }
                    this.formGroup.controls[this.formField.key].setValue(null);
                }
            }
        }
    }

    downloadFile(path: string, type: string) {
        this.fileUploadService.download(path)
            .subscribe(url => {
                const modalRef = this.modalService.open(NgDocumentModalContent, { 
                    size: 'xl' 
                });
                modalRef.componentInstance.file = {
                    url: url,
                    type: type
                };
                modalRef.result.then((result) => {
                });
                // window.open(url, '_blank');
            });
    }

    openCmtModal() {
        const modalRef = this.modalService.open(NgCommentModalContent);
        modalRef.componentInstance.comment = {
            ...this.commented || {}, ...{
                key: this.unqKey,
                type: this.currentForm.type,
                type_id: this.currentForm.id,
                page: this.currentForm.page
            }
        };
        modalRef.result.then((result) => {
        });
    }

    toSlug(str) {
        if (str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to = "aaaaeeeeiiiioooouuuunc------";
            for (var i = 0, l = from.length; i < l; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;
        }
        else {
            return '';
        }
    }

}
