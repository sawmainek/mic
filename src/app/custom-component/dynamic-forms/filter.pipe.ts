import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {

    transform(values: any[] = [], ...args: unknown[]): unknown {
        if (values && args[0] && args[1]) {
            if (Array.isArray(args[1])) {
                let subclass = [];
                args[1].forEach(value => {
                    subclass.push(values.filter(x => x[`${args[0]}`] == value));
                });
                return this.uniqueArray([].concat(...subclass));
            }
            return this.uniqueArray(values.filter(y => y[`${args[0]}`] == args[1]));
        }
        return this.uniqueArray(values) || [];
    }

    uniqueArray(list: any[]) {
        let values = list || [];
        let unique: any = [];
        values.forEach(x => {
            if(unique.filter(y => y.id == x.id).length == 0) {
                unique.push(x);
            }
        });
        unique.sort((a, b) => (a.name > b.name) ? 1 : -1);
        return unique;
    }

}
