import { cloneDeep } from 'lodash';
import { formProgress } from './../../core/form/_selectors/form.selectors';
import { FormRadio } from './base/form-radio';
import { BaseFormData, Comments, FormProgress } from './../../core/form/_actions/form.actions';
import { Store, select } from '@ngrx/store';
import { FormCommentService } from './../../../services/form-comment.service';
import { Observable } from 'rxjs/internal/Observable';
import { FormProgressService } from './../../../services/form-progress.service';
import { FileUploadService } from './../../../services/file_upload.service';
import { mergeMap, concatMap, delay, tap, finalize } from 'rxjs/operators';
import { of, from } from 'rxjs';
import { FormControl, Validators, FormGroup, FormArray } from '@angular/forms';
import { Injectable } from '@angular/core';
import { BaseForm } from './base/base-form';
import { FormFile } from './base/form.file';
import { FormCheckBox } from './base/form-checkbox';
import { AppState } from 'src/app/core';
import { currentForm } from 'src/app/core/form';
@Injectable()
export class FormControlService {
    currentForm: any;
    progressList: any[] = [];
    constructor(
        private fileUploadService: FileUploadService,
        private formProgressService: FormProgressService,
        private formComment: FormCommentService,
        private store: Store<AppState>
    ) {
        this.store.pipe(select(currentForm))
            .subscribe(currentForm => {
                this.currentForm = currentForm;
            });
        this.store.pipe(select(formProgress)).subscribe(progress => {
            this.progressList = cloneDeep(progress);
        });
     }

    toFormGroup(form: BaseForm<string|boolean>[], data: any = {}, clone: any = {}) {
        data = data || {};
        clone = clone || {};
        const group: any = {};
        form.forEach(x => {
            switch (x.controlType) {
                case 'form_group':
                    if(x.key) {
                        let formGroup = {};
                        x.formGroup.forEach(y => {
                            if(y.key) {
                                switch (y.controlType) {
                                    case 'form_group':
                                        formGroup[y.key] = this.toFormGroup(y.formGroup, data[x.key] && data[x.key][y.key] || clone[x.key] && clone[x.key][y.key] || {});
                                        break;
                                    case 'form_array':
                                        y.defaultLength = data[x.key] && data[x.key][y.key] && data[x.key][y.key].length || clone[x.key] && clone[x.key][y.key] && clone[x.key][y.key].length || y.defaultLength || 1;
                                        let formArray: FormArray = new FormArray([]);
                                        for (let i = 0; i < y.defaultLength; i++) {
                                            formArray.push(this.toFormGroup(y.formArray, data[x.key] && data[x.key][y.key] && data[x.key][y.key][i] || clone[x.key] && clone[x.key][y.key] && clone[x.key][y.key][i] || {}));
                                        }
                                        formGroup[y.key] = formArray;
                                        break;
                                    default:
                                        let validators: any[] = y.required ? [Validators.required] : [];
                                        let value = data[x.key] && data[x.key][y.key] || clone[x.key] && clone[x.key][y.key] || null;
                                        if (y instanceof FormCheckBox) {
                                            value = value || '';
                                        } else if (y instanceof FormRadio) {
                                            value = y.required ? value || y.value : value || '';
                                        } else {
                                            value = value || y.value || '';
                                        }
                                        if (y instanceof FormFile) {
                                            value = value && value.file && value.file['url'] ? value : null;
                                        }
                                        formGroup[y.key] = new FormControl(value, [...validators, ...y.validators]);
                                        break;
                                }
                                
                            }
                        });
                        group[x.key] = new FormGroup(formGroup);
                    }
                    break;
                case 'form_array':
                    if (x.key) {
                        x.defaultLength = data[x.key] && data[x.key].length || clone[x.key] && clone[x.key].length || x.defaultLength || 1;
                        let formArray: FormArray = new FormArray([]);
                        for (let i = 0; i < x.defaultLength; i++) {
                            formArray.push(this.toFormGroup(x.formArray, data[x.key] && data[x.key][i] || clone[x.key] && clone[x.key][i] || {}));
                        }
                        group[x.key] = formArray;
                    }
                    break;
                default:
                    if(x.key) {
                        let validators: any[] = x.required ? [Validators.required] : [];
                        let value = data[x.key] || clone[x.key] || null;
                        if (x instanceof FormCheckBox)  {
                            value = value || '';
                        } else if (x instanceof FormRadio) {
                            value = x.required ? value || x.value : value || '';
                        } else {
                            value = value || x.value || '';
                        }
                        if (x instanceof FormFile) {
                            value = value && value.file && value.file['url'] ? value : null;
                        }
                        group[x.key] = new FormControl(value, [...validators, ...x.validators]);
                    }
                    break;
            }
            
        });
        
        return new FormGroup(group);
    }

    checkEmpty(value) {
        if(Array.isArray(value) && value.length == 0 ) {
            return null;
        }
        if(value instanceof Object && Object.keys(value).length == 0) {
            return null;
        }
        return value;
    }

    //type: Inquiry | MIC Pemit | Endorsement | Land Right | Tax Incentive
    getComments$(type: string = '', type_id: number = 0): Observable<any>  {
        return Observable.create((observer) => {
            // TODO for investor role
            this.formComment.get({ search: `type:equal:${type}|type_id:equal:${type_id}|submitted:equal:false`, rows: 999 }).subscribe(res => {
                observer.next(res);
                observer.complete();
            }, err => observer.error(err));
        });
    }

    getCommentsByPage$(page: string = ''): Observable<any> {
        return Observable.create((observer) => {
            this.formComment.get({ search: `page:equal:${page}`, rows: 999 }).subscribe(res => {
                observer.next(res);
                observer.complete();
            }, err => observer.error(err));
        });
    }

    getProgressByPage$(page: string = ''): Observable<any> {
        return Observable.create((observer) => {
            this.formProgressService.get({ search: `page:equal:${page}`, rows: 999 }).subscribe(res => {
                observer.next(res);
                observer.complete();
            }, err => observer.error(err));
        });
    }

    updateProgressRevise$(id: number, data: any): Observable<any> {
        return Observable.create((observer) => {
            this.formProgressService.update(id, data).subscribe(res => {
                observer.next(res);
                observer.complete();
            }, err => observer.error(err));
        });
    }

    addComment$(data: any = {}): Observable<any>  {
        let files = [];
        let uploadedFiles = [];
        data.attachments = data.attachments || [];
        data.attachments.map(x => {
            files.push({
                type_id: data.type_id || null,
                type: data.type || null,
                name: x.name,
                file: x instanceof File ? x : (x && x.file || {})
            });
        })
        return Observable.create((observer) => {
            of(files).pipe(
                mergeMap((x: [any]) => from(x)),
                concatMap(x => {
                    return of(x).pipe(delay(100))
                }),
                concatMap(x => {
                    if (x.file instanceof File) {
                        const formData: any = new FormData();
                        formData.append('type_id', x.type_id || '');
                        formData.append('type', x.type || '');
                        formData.append('name', x.name || x.file.name);
                        formData.append('file', x.file);
                        return this.fileUploadService.upload(formData, { secure: true });
                    }
                    return of(x);
                }),
                tap(x => {
                    uploadedFiles.push(x);
                }),
                finalize(() => {
                    data.attachments = uploadedFiles;
                    this.formComment.create(data).subscribe(res => {
                        observer.next(res);
                        observer.complete();
                        this.getComments$(res.type, res.type_id).subscribe(comments => {
                            this.store.dispatch(new Comments({comments}));
                        })
                    }, err => observer.error(err))
                }),
            ).subscribe();
        });
    }

    deleteComment$(data: any): Observable<any> {
        return Observable.create((observer) => {
            this.formComment.destroy(data.id).subscribe(res => {
                observer.next(res);
                observer.complete();
                this.getComments$(data.type, data.type_id).subscribe(comments => {
                    this.store.dispatch(new Comments({ comments }));
                })
            }, err => observer.error(err));
        });
    }
    
    fileUpload$(form: BaseForm<string | boolean>[], formGroup: FormGroup, options: {
        type?: string;
        type_id?: string;
        name?: string;
    } = {}): Observable<any> {
        return Observable.create((observer) => {
            let formGroupValue = Object.assign({}, formGroup.value);
            let callbackFiles: { id:number, key: string, originFile: any }[] = []
            this.getFiles(form, formGroupValue, callbackFiles);
            let files = [];
            let uploadedFiles = [];
            callbackFiles.map(x => {
                files.push({
                    id: x.id,
                    type_id: options.type_id || null,
                    type: options.type || this.currentForm.type || null,
                    key: x.key || null,
                    name: options.name || x.originFile.name,
                    file: x.originFile instanceof File ? x.originFile : (x.originFile && x.originFile.file || {})
                });
            })

            of(files).pipe(
                mergeMap((x: [any]) => from(x)),
                concatMap(x => {
                    return of(x).pipe(delay(100))
                }),
                concatMap(x => {
                    if (x.file instanceof File) {
                        const formData: any = new FormData();
                        formData.append('type_id', x.type_id || '');
                        formData.append('type', x.type || '');
                        formData.append('key', x.key || '');
                        formData.append('name', x.name || x.file.name);
                        formData.append('file', x.file);
                        return this.fileUploadService.upload(formData, { secure: true });
                    }
                    return of(x);
                }),
                tap(x => {
                    uploadedFiles.push(x);
                }),
                finalize(() => {
                    this.restoreFiles(form, formGroupValue, uploadedFiles);
                    observer.next(formGroupValue);
                    observer.complete();
                    // observer.error(error);
                }),
            ).subscribe();
        });
    }

    lazyUpload(form: BaseForm<string | boolean>[], formGroup: FormGroup, options: {
        type?: string;
        type_id?: string;
        name?: string;
    } = {}, callbackFn) {
        let formGroupValue = Object.assign({}, formGroup.value);
        let callbackFiles: {id: number, key: string, originFile: any }[] = [];
        this.getFiles(form, formGroupValue, callbackFiles);
        let files = [];
        let uploadedFiles = [];
        callbackFiles.map(x => {
            files.push({
                id: x.id || null,
                type_id: options.type_id || null, 
                type: options.type || this.currentForm.type || null,
                key: x.key || null, 
                name: options.name || (x.originFile && x.originFile.name), 
                file: x.originFile instanceof File ? x.originFile : (x.originFile && x.originFile.file || {})
            });
        })

        of(files).pipe(
            mergeMap((x: [any]) => from(x)),
            concatMap(x => {
                return of(x).pipe(delay(100))
            }),
            concatMap(x => {
                if (x.file instanceof File) {
                    const formData: any = new FormData();
                    formData.append('type_id', x.type_id || '');
                    formData.append('type', x.type || '');
                    formData.append('key', x.key || '');
                    formData.append('name', x.name || x.file.name);
                    formData.append('file', x.file );
                    return this.fileUploadService.upload(formData, { secure: true });
                }
                return of(x);
            }),
            tap(x => {
                uploadedFiles.push(x);
            }),
            finalize(() => {
                this.restoreFiles(form, formGroupValue, uploadedFiles);
                callbackFn(formGroupValue);
            })
        ).subscribe();
    }

    getFiles(form: BaseForm<string | boolean>[], formGroupValue, uploadFiles: { id: number, key: string, originFile: any }[] = [], index: number = 0) {
        form.forEach(x => {
            switch (x.controlType) {
                case 'form_group':
                    this.getFiles(x.formGroup, formGroupValue[x.key], uploadFiles, index);
                    break;
                case 'form_array':
                    let values = formGroupValue[x.key] || [];
                    values.map((value, index2) => {
                        x.formArray.forEach(y => {
                            switch (y.controlType) {
                                case 'form_group':
                                    this.getFiles(y.formGroup, value[y.key] || [], uploadFiles, index2);
                                    break;
                                case 'form_array':
                                    let values = value[y.key] || [];
                                    values.map((value, index3)=> {
                                        y.formArray.forEach(z => {
                                            switch (z.controlType) {
                                                case 'form_group':
                                                    this.getFiles(z.formGroup, value[z.key] || [], uploadFiles, index3);
                                                    break;
                                                default:
                                                    if (z instanceof FormFile) {
                                                        uploadFiles.push({ id: value[z.key]?.id,key: `${z.key}:${index}:${index2}:${index3}`, originFile: value[z.key] });
                                                    }
                                                    break;
                                            }
                                        });
                                    })
                                    break;
                                default:
                                    if(y instanceof FormFile) {
                                        uploadFiles.push({ id: value[y.key]?.id,key: `${y.key}:${index}:${index2}`, originFile: value[y.key] });
                                    }
                                    break;
                            }
                        })
                    })
                    break;
                default:
                    if (x instanceof FormFile) {
                        uploadFiles.push({ id: formGroupValue[x.key]?.id, key: `${x.key}:${index}`, originFile: formGroupValue[x.key] });
                    }
                    break;
            }
        });
    }

    restoreFiles(form: BaseForm<string | boolean>[], formGroupValue: any = {}, uploadFiles: any[] = [], index: number = 0) {
        form.forEach((x) => {
            switch (x.controlType) {
                case 'form_group':
                    this.restoreFiles(x.formGroup, formGroupValue[x.key], uploadFiles);
                    break;
                case 'form_array':
                    let values = formGroupValue[x.key] || [];
                    values.map((value, index2) => {
                        x.formArray.forEach(y => {
                            switch (y.controlType) {
                                case 'form_group':
                                    this.restoreFiles(y.formGroup, value[y.key] || [], uploadFiles, index2);
                                    break;
                                case 'form_array':
                                    let values = value[y.key] || [];
                                    values.map((value, index3) => {
                                        y.formArray.forEach(z => {
                                            switch (z.controlType) {
                                                case 'form_group':
                                                    this.restoreFiles(z.formGroup, value[z.key] || [], uploadFiles, index3);
                                                    break;
                                                default:
                                                    if (z instanceof FormFile) {
                                                        this.getFormType(form, z.key, (type: BaseForm<string>) => {
                                                            if (type.multiple) { //Need to remove for multiple
                                                                value[z.key] = uploadFiles.filter(a => a.key == `${z.key}:${index}:${index2}:${index3}`)[0] || {};
                                                            } else {
                                                                value[z.key] = uploadFiles.filter(a => a.key == `${z.key}:${index}:${index2}:${index3}`)[0] || {};
                                                            }
                                                        })
                                                    }
                                                    break;
                                            }
                                        });
                                    })
                                    break;
                                default:
                                    if (y instanceof FormFile) {
                                        this.getFormType(form, y.key, (type: BaseForm<string>) => {
                                            if (type.multiple) { //Need to remove for multiple
                                                value[y.key] = uploadFiles.filter(a => a.key == `${y.key}:${index}:${index2}`)[0] || {};
                                            } else {
                                                value[y.key] = uploadFiles.filter(a => a.key == `${y.key}:${index}:${index2}`)[0] || {};
                                            }
                                        })
                                        
                                    }
                                    break;
                            }
                        })
                    })
                    break;
                default:
                    if (x instanceof FormFile) {
                        this.getFormType(form, x.key, (type: BaseForm<string>) => {
                            if (type.multiple) { //Need to remove for multiple
                                formGroupValue[x.key] = uploadFiles.filter(a => a.key == `${x.key}:${index}`)[0] || {};
                            } else {
                                formGroupValue[x.key] = uploadFiles.filter(a => a.key == `${x.key}:${index}`)[0] || {};
                            }
                        })
                    }
                    break;
            }
        });
    }

    getFormType(form: BaseForm<string | boolean>[], key: string, callbackType) {
        form.forEach(x => {
            switch (x.controlType) {
                case 'form_group':
                    this.getFormType(x.formGroup, key, callbackType);
                    break;
                case 'form_array':
                    x.formArray.forEach(y => {
                        switch (y.controlType) {
                            case 'form_group':
                                this.getFormType(y.formGroup, key, callbackType);
                                break;
                            case 'form_array':
                                y.formArray.forEach(z => {
                                    switch (z.controlType) {
                                        case 'form_group':
                                            this.getFormType(z.formGroup, key, callbackType);
                                            break;
                                        default:
                                            if (z.key == key) {
                                                callbackType(z);
                                            }
                                            break;
                                    }
                                })
                                break;
                            default:
                                if (y.key == key) {
                                    callbackType(y);
                                }
                                break;
                        }
                    });
                    break;
                default:
                    if (x.key == key) {
                        callbackType(x);
                    }
                    break;
            }
            
        });
    }

    saveFormProgress(data: any = {}) {
        if(!data.type && !data.type_id) {
            return;
        }
        this.formProgressService.create(data).subscribe(result => {
            this.progressList.push(result);
            this.store.dispatch(new FormProgress({ progress: cloneDeep(this.progressList)}));
        });
    }

}