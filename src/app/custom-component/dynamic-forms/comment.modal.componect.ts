import { FileUploadService } from './../../../services/file_upload.service';
import { forkJoin } from 'rxjs';
import { FormProgressService } from '../../../services/from-progress.service';
import { currentRole } from '../../core/auth/_selectors/auth.selectors';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { Store, select } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControlService } from './form-control.service';
import { AppState } from 'src/app/core';

@Component({
    selector: 'comment-modal-content',
    template: `
    <div class="modal-header">
      <h4 class="modal-title">{{!comment.comment ? "Add Comment" : "Comment"|translate}}</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body m-3">
      <ng-container *ngIf="comment?.attachments && comment?.attachments.length > 0">
        <label class="form-control-label mb-3 text-uppercase">{{"Attached Files"|translate}}</label>
        <ul class="list-unstyled mb-0">
            <li *ngFor="let x of comment.attachments">
                <a href="javascript:;" (click)="downloadFile(x?.file?.url, x?.file?.type)">
                    <img src="../../assets/images/attachment.svg" width="14px" class="mr-3">
                    <label class="form-label">{{x.name}}</label>
                </a>
            </li>
        </ul>
    </ng-container>
    <div class="form-group">
        <label class="form-control-label">Comment:</label>
        <textarea rows="8" name="comment" [(ngModel)]="comment.comment" class="form-control" required [readOnly]="!editable">{{comment.comment}}</textarea>
    </div>
    <div *ngIf="editable" >
        <div class="d-sm-flex justify-content-between align-items-center">
            <div>
                <button type="button" class="btn btn-lg btn-primary mr-3" [ngClass]="{'spinner spinner-white spinner-right':loading}" (click)="postComment()" [disabled]="!comment.comment" [ngStyle]="{'min-width': '80px'}">
                    {{"Send"|translate}}
                </button>
                <button type="button" *ngIf="comment.id && comment.comment && !comment.resolved"  class="btn btn-lg btn-warning mr-3" [ngClass]="{'spinner spinner-white spinner-right':resolved}" (click)="postResolved()" [ngStyle]="{'min-width': '80px'}">
                    {{"Mark as Resolved"|translate}}
                </button>
                <button class="btn btn-lg btn-outline-light" type="button" title="" data-toggle="tooltip"
                    data-original-title="Attach files" (click)="fileInput.click()">
                    <input type="file" #fileInput (change)="onFileChange($event.target.files[0])" hidden />
                    <img src="../../assets/images/attachment.svg" width="14px">
                    <span class="ml-1">{{comment?.attachments && comment?.attachments.length || 0}}</span>
                </button>
            </div>
            <button type="button" class="btn btn-lg btn-danger" *ngIf="comment?.id" 
                (click)="deleteComment()" 
                [ngClass]="{'spinner spinner-white spinner-right':deleting}" 
                [disabled]="deleting">Delete</button>
        </div>
    </div>
    </div>
  `
})
export class NgCommentModalContent implements OnInit {
    @Input() comment: any;
    loading: boolean = false;
    resolved: boolean = false;
    deleting: boolean = false;
    user: any = {};
    editable: boolean = false;
    host: string = environment.host;
    constructor(
        public store: Store<AppState>,
        public activeModal: NgbActiveModal,
        public formCtlService: FormControlService,
        public formPrgService: FormProgressService,
        public fileUploadService: FileUploadService
    ) {

        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            }).unsubscribe();
        this.store.pipe(select(currentRole))
            .subscribe(roles => {
                if (window.location.href.indexOf('officer') !== -1 && roles.indexOf('checking') !== -1 || roles.indexOf('approval') !== -1) {
                    this.editable = true;
                }
            });
    }

    ngOnInit(): void {

    }

    downloadFile(path: string, type: string) {
        this.fileUploadService.download(path)
            .subscribe(url => {
                window.open(url, '_blank');
            });
    }

    postComment() {
        this.loading = true;
        this.comment = this.comment || {};
        this.comment.submitted = false;
        this.comment.resolved = false;
        this.comment.user_id = this.comment.user_id || this.user.id;
        this.formCtlService.addComment$(this.comment).subscribe(x => {
            this.formPrgService.get({
                search: `type_id:equal:${this.comment.type_id}|page:equal:${this.comment.page}`
            }).subscribe(progress => {
                this.comment = {};
                this.loading = false;
                this.activeModal.close(x);
                for (const prog of progress) {
                    this.formPrgService.update(prog.id, { revise: true }).subscribe();
                }
            })

        });
    }

    deleteComment() {
        this.deleting = true;
        this.formCtlService.deleteComment$(this.comment).subscribe(x => {
            forkJoin([
                this.formCtlService.getCommentsByPage$(this.comment.page),
                this.formCtlService.getProgressByPage$(this.comment.page)
            ]).subscribe(result => {
                this.deleting = false;
                this.comment = {};
                const revise = result[0].filter(x => x.resolved != true).length || 0;
                if (revise == 0) {
                    result[1].forEach(x => {
                        this.formCtlService.updateProgressRevise$(x.id, { revise: false }).subscribe();
                    });
                }
                this.activeModal.close(x);
            })
        })
    }

    postResolved() {
        this.resolved = true;
        this.comment.resolved = true;
        this.formCtlService.addComment$(this.comment).subscribe(x => {
            forkJoin([
                this.formCtlService.getCommentsByPage$(this.comment.page),
                this.formCtlService.getProgressByPage$(this.comment.page)
            ]).subscribe(result => {
                this.resolved = false;
                const revise = result[0].filter(x => x.resolved != true).length || 0;
                if (result[0].length > 0 && revise == 0) {
                    result[1].forEach(x => {
                        this.formCtlService.updateProgressRevise$(x.id, { revise: false }).subscribe();
                    });
                }
                this.activeModal.close(x);
            })
        });
    }

    onFileChange(file) {
        this.comment.attachments = [...this.comment.attachments || []];
        this.comment.attachments.push(file);
    }
}