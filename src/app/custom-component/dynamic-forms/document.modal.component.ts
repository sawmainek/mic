import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'document-modal-content',
    styles: [' .modal-dialog { max-width: 80vw; } '],
    template: `
        <div class="modal-header">
            <h4 class="modal-title">{{"File Preview"|translate}}</h4>
            <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <ngx-doc-viewer
                [url]="file.url"
                [viewer]="getType(file.type)" style="width:100%;height:90vh;">
            </ngx-doc-viewer>
        </div>
    `
})
export class NgDocumentModalContent implements OnInit {
    @Input() file: any = {};
    constructor(
        public activeModal: NgbActiveModal,
    ) {
    }

    getType(type) {
        switch (type) {
            case 'application/pdf':
                return 'pdf'
            case 'image/png':
                return 'pdf'
            case 'image/jpg':
                return 'pdf'
            case 'image/jpeg':
                return 'pdf'
            default:
                return 'office';
        }
    }

    ngOnInit(): void {

    }
}