import { Store, select } from '@ngrx/store';
import { AppState } from './../../core/reducers/index';
import { FormHidden } from './base/form-hidden';
import { FormDummy } from './base/form-dummy';
import { BaseFormArray } from './base/form-array';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { FormControlService } from './form-control.service';
import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormArray, Validators } from '@angular/forms';
import { BaseFormGroup } from './base/form-group';
import { currentUser } from 'src/app/core';
declare var jQuery: any;
@Component({
    selector: 'ng-form-layout',
    templateUrl: './form-layout.component.html',
    styleUrls: ['./form-layout.component.scss']
})
export class FormLayoutComponent implements OnInit {
    @Input() formLayout: BaseForm<string>[];
    @Input() formGroup: FormGroup;

    readonly: boolean = false;
    constructor(
        private store: Store<AppState>,
        private formCtlService: FormControlService,
        private cdr: ChangeDetectorRef,
    ) {

    }

    ngOnInit(): void {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user && user.type == 'officer' && window.location.href.indexOf('officer') !== -1) {
                    this.readonly = true;
                }
            });
    }

    ngAfterViewInit(): void {
    }

    visibleColumn(form: BaseForm<string>) {
        if (form instanceof FormDummy || form instanceof FormHidden) {
            return false;
        }
        return true;
    }

    hasCriteria(form: BaseFormGroup, group: any): boolean {
        if (form.key && form.criteriaValue) {
            if (form.criteriaValue && this.matchValue(form.criteriaValue.value, group.value[form.criteriaValue.key])) {
                this.addValidators(form, group);
                return true;
            } else {
                this.removeValidators(form, group);
                return false;
            }
        }
        return true;
    }

    addValidators(form: BaseFormGroup, formGroup: FormGroup) {
        if (form.key && form instanceof BaseFormGroup) {
            let group = formGroup.get(form.key);
            form.formGroup.forEach((x: any) => {
                if (x.key && x instanceof BaseFormGroup) {
                    this.addValidators(x, group as FormGroup);
                } else if (x.key && x instanceof BaseFormArray) {
                    this.addValidators(x, group as FormGroup);
                } else if (x.key) {
                    let validators: any[] = x.required ? [Validators.required] : [];
                    (group as FormGroup).controls[x.key].setValidators([...validators, ...x.validators]);
                }
            });
        }
        if (form.key && form instanceof BaseFormArray) {
            let group = (formGroup.get(form.key) as FormArray).controls;
            group.forEach(y => {
                form.formArray.forEach((x: any) => {
                    if (x.key && x instanceof BaseFormGroup) {
                        this.addValidators(x, y as FormGroup);
                    } else if (x.key && x instanceof BaseFormArray) {
                        this.addValidators(x, y as FormGroup);
                    } else if (x.key) {
                        let validators: any[] = x.required ? [Validators.required] : [];
                        (y as FormGroup).controls[x.key].setValidators([...validators, ...x.validators]);
                    }
                });
            });
        }
    }

    removeValidators(form: BaseFormGroup, formGroup: FormGroup) {
        if (form.key && form instanceof BaseFormGroup) {
            let group = formGroup.get(form.key);
            form.formGroup.forEach((x: any) => {
                if (x.key && x instanceof BaseFormGroup) {
                    this.removeValidators(x, group as FormGroup);
                } else if (x.key && x instanceof BaseFormArray) {
                    this.removeValidators(x, group as FormGroup);
                } else if (x.key) {
                    (group as FormGroup).controls[x.key].clearValidators();
                    (group as FormGroup).controls[x.key].updateValueAndValidity();
                }
            });
        }
        if (form.key && form instanceof BaseFormArray) {
            let group = (formGroup.get(form.key) as FormArray).controls;
            group.forEach(y => {
                form.formArray.forEach((x: any) => {
                    if (x.key && x instanceof BaseFormGroup) {
                        this.removeValidators(x, y as FormGroup);
                    } else if (x.key && x instanceof BaseFormArray) {
                        this.removeValidators(x, y as FormGroup);
                    } else if (x.key) {
                        (y as FormGroup).controls[x.key].clearValidators();
                        (y as FormGroup).controls[x.key].updateValueAndValidity();
                    }
                });
            });
        }
    }

    matchValue(values, criteriaValue): boolean {
        if (Array.isArray(values)) {
            return values.includes(criteriaValue);
        }
        return values == criteriaValue || false;
    }

    addMore(form: any[], formArr: any) {
        let formArray = formArr as FormArray;
        formArray.push(this.formCtlService.toFormGroup(form));
        this.cdr.detectChanges();
    }

    remove(formArray: any, index: number, removeEvent: any) {        
        formArray = formArray as FormArray;
        if (formArray.length > 1) {
            formArray.removeAt(index);
            if (removeEvent) {
                removeEvent(index);
            }
        }

    }

}