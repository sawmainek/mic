import { BaseForm } from './base-form';

export class FormSectionTitleCounter extends BaseForm<string> {
    controlType = 'section_title_counter';
}