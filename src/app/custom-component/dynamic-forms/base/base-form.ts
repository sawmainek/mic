import { Observable, Subject } from 'rxjs';
export class BaseForm<T> {
    value: T;
    key: string;
    unqKey: string;
    label: string;
    group: string;
    prefix: string;
    endfix: string;
    required: boolean;
    validators: any[];
    order: number;
    controlType: string;
    acceptFiles: string;
    type: string;
    min: number; 
    style: {};
    placeholder: string;
    dateView: string;
    tooltip: string;
    multiple: boolean;
    columns: string;
    criteriaValue: { key: string, value: string | [] };
    formGroup: BaseForm<any>[];
    formArray: BaseForm<any>[];
    defaultLength: number;
    options: { id: string, name: string }[];
    options$: Observable<{ id: string, name: string }[]>;
    filter: { parent: string, key: string };
    readonly: boolean;
    editable: boolean;
    commentable: boolean;
    optionTriggerFn: any;
    valueChangeEvent: any;
    valueChanges$: Subject<any>;
    callbackFn: any;
    pageBreak: string;
    pageOrientation: string;
    useTable: boolean;
    disableAddRow: boolean;
    addMoreText: string;
    rowDeleteEvent: any;
    headerRows: number;
    headerWidths: any[];
    tablePDFHeader: {
        label?: string, rowSpan?: number, colSpan?: number, style?: {}, cellFn?: any, pipe?: string
    }[][];
    tableHeader: {
        label?: string, rowSpan?: number, colSpan?: number, style?: {}, cellFn?: any, pipe?: string
    }[][];
    tableFooter: {
        label?: string, rowSpan?: number, colSpan?: number, style?: {}, cellFn?: any, pipe?: string
    }[][];
    tablePDFFooter: {
        label?: string, rowSpan?: number, colSpan?: number, style?: {}, cellFn?: any, pipe?: string
    }[][];
    constructor(options: {
        value?: T;
        key?: string;
        unqKey?: string;
        label?: string;
        prefix?: string;
        endfix?: string;
        group?: string;
        required?: boolean;
        validators?: any[];
        order?: number;
        controlType?: string;
        acceptFiles?: string;
        type?: string;
        min?: number; 
        style?: {};
        placeholder?: string;
        dateView?: string;
        tooltip?: string;
        multiple?: boolean;
        columns?: string;
        criteriaValue?: { key: string, value: any | [] };
        formGroup?: BaseForm<any>[];
        formDocument?: BaseForm<any>[];
        formArray?: BaseForm<any>[];
        defaultLength?: number;
        options?: { id: string, name: string }[];
        options$?: Observable<{ id: string, name: string }[]>;
        filter?: { parent: string, key: string };
        readonly?: boolean;
        editable?: boolean;
        commentable?: boolean;
        optionTriggerFn?: any;
        valueChangeEvent?: any;
        valueChanges$?: Subject<any>;
        callbackFn?: any;
        pageBreak?: string;
        pageOrientation?: string;
        useTable?: boolean;
        disableAddRow?: boolean;
        addMoreText?: string;
        rowDeleteEvent?: any;
        headerRows?: number;
        headerWidths?: any[];
        tablePDFHeader?: {
            label?: string, rowSpan?: number, colSpan?: number, style?: {}, cellFn?: any, pipe?: string
        }[][];
        tableHeader?: {
            label?: string, rowSpan?: number, colSpan?: number, style?: {}, cellFn?: any, pipe?: string
        }[][];
        tableFooter?: {
            label?: string, rowSpan?: number, colSpan?: number, style?: {}, cellFn?: any, pipe?: string
        }[][];
        tablePDFFooter?: {
            label?: string, rowSpan?: number, colSpan?: number, style?: {}, cellFn?: any, pipe?: string
        }[][];
    } = {}
    ) {
        this.value = options.value;
        this.key = options.key || '';
        this.unqKey = options.unqKey || '';
        this.label = options.label || '';
        this.group = options.group || null;
        this.prefix = options.prefix || null;
        this.endfix = options.endfix || null;
        this.required = options.required || false;
        this.validators = options.validators || [];
        this.order = options.order === undefined ? 1 : options.order;
        this.controlType = options.controlType || '';
        this.acceptFiles = options.acceptFiles || '*';
        this.type = options.type || 'text';
        this.min = options.min || 0;
        this.style = options.style || {};
        this.placeholder = options.placeholder || '';
        this.dateView = options.dateView || 'day';
        this.tooltip = options.tooltip || '';
        this.multiple = options.multiple || false;
        this.columns = options.columns || 'col-12';
        this.criteriaValue = options.criteriaValue;
        this.formGroup = options.formGroup || [];
        this.formArray = options.formArray || [];
        this.filter = options.filter;
        this.readonly = options.readonly || false;
        this.editable = options.editable || false;
        this.commentable = options.commentable;
        this.defaultLength = options.defaultLength || 1;
        this.options = options.options || []
        this.options$ = options.options$;
        this.optionTriggerFn = options.optionTriggerFn;
        this.valueChangeEvent = options.valueChangeEvent;
        this.valueChanges$ = options.valueChanges$;
        this.callbackFn = options.callbackFn;
        this.pageBreak = options.pageBreak;
        this.pageOrientation = options.pageOrientation;
        this.useTable = options.useTable;
        this.disableAddRow = options.disableAddRow || false;
        this.addMoreText = options.addMoreText || 'Add More';
        this.rowDeleteEvent = options.rowDeleteEvent;
        this.headerRows = options.headerRows;
        this.headerWidths = options.headerWidths;
        this.tablePDFHeader = options.tablePDFHeader;
        this.tableHeader = options.tableHeader;
        this.tableFooter = options.tableFooter;
        this.tablePDFFooter = options.tablePDFFooter;
    }
}