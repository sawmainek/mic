import { BaseForm } from './base-form';

export class FormSectionTitle extends BaseForm<string> {
    controlType = 'section_title';
}