import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'preview-modal-content',
    styles: [' .modal-dialog { max-width: 80vw; } '],
    template: `
        <div class="modal-header">
            <h4 class="modal-title">{{"Preview"|translate}}</h4>
            <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <ngx-doc-viewer
                [url]="file.url"
                [viewer]="getType(file.type)" style="width:100%;height:80vh;" [ngStyle]="!file?.readOnly ? {width:'100%', height:'80vh'} : {width:'100%', height:'90vh'}">
            </ngx-doc-viewer>
        </div>
        <div class="modal-footer text-right" *ngIf="!file?.readOnly" >
            <button type="button" class="btn btn-lg btn-default" (click)="activeModal.dismiss('Cross click')">{{"Close"|translate}}</button>
            <button type="button" class="btn btn-lg btn-primary" (click)="onSubmit()">{{"SUBMIT"|translate}}</button>
        </div>
    `
})
export class NgPreviewModalContent implements OnInit {
    @Input() file: any = {};
    constructor(
        public activeModal: NgbActiveModal,
    ) {
    }

    getType(type) {
        switch (type) {
            case 'application/pdf':
                return 'pdf'
            case 'image/png':
                return 'pdf'
            case 'image/jpg':
                return 'pdf'
            case 'image/jpeg':
                return 'pdf'
            default:
                return 'office';
        }
    }

    onSubmit() {
        if ( this.file.onSubmit ) {
            this.file.onSubmit();
            this.activeModal.dismiss();
        }
    }

    ngOnInit(): void {

    }
}