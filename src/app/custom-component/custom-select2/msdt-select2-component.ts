import { Component, ViewChild, ElementRef, forwardRef, Input, Output, EventEmitter, SimpleChanges, Renderer2 } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';

declare let $: any;

@Component({
    selector: 'ng-select',
    templateUrl: './msdt-select2-component.html',
    styleUrls: ['./msdt-select2-component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MSDTSelectComponent),
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => MSDTSelectComponent),
            multi: true
        }
    ]
})
export class MSDTSelectComponent implements ControlValueAccessor, Validator {

    @ViewChild('selectControll') selectControll: ElementRef;
    @Input() isSubmit: boolean = false;
    @Input() data: Array<any>;
    @Input() placeholder: string = "Select Options"
    @Input() disabled: boolean;
    @Input() options: any = {};
    @Input() required: boolean = false;
    @Input() maxCount: number = Number.MAX_SAFE_INTEGER;
    @Input() minCount: number = Number.MIN_SAFE_INTEGER;
    @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

    public selectedValue: any | Array<any>
    public isValid = true;

    private _jqueryElement: any;
    private _onChange = (_: any) => { };
    private _onTouched = () => { };

    constructor(
        private _renderer: Renderer2
    ) {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this._jqueryElement = $(this.selectControll.nativeElement);
        this.initSelect2();
        this._jqueryElement.on('select2:select select2:unselect', (e: any) => {
            let data = this._jqueryElement.select2('data');
            data.allowClear = true;
            for (let item of data) {
                delete item.element;
                delete item.disabled;
                delete item.selected;
            }
            if (!this.options.multiple) {
                data = (e.type == 'select2:unselect') ? null : data[0];
                this.selectedValue = data?.id;
                this._onChange(data?.id);
                this.valueChange.emit(data?.id);
            }
            else {
                this.selectedValue = data.map(x => x.id);
                this._onChange(data.map(x => x.id));
                this.valueChange.emit(data.map(x => x.id));
            }


        });
        if (this.selectedValue) {
            this.setSelect2Value(this.selectedValue);
        }
        else {
            this.setSelect2Value('');
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (!this._jqueryElement) return;
        if (this.hasChanged(changes)) {
            this.initSelect2();
            if (this.selectedValue) {
                this.setSelect2Value(this.selectedValue);
            }
        }
    }

    ngOnDestroy() {
        this._jqueryElement.select2('destroy');
    }

    writeValue(value: any | Array<any>): void {
        this.selectedValue = value;
        if (value !== null && value !== undefined)
            this.setSelect2Value(value);
    }

    registerOnChange(fn: (_: any) => {}): void {
        this._onChange = fn;
    }

    registerOnTouched(fn: () => {}): void {
        this._onTouched = fn;
    }

    setDisabledState(isDisabled: boolean) {
        this.disabled = isDisabled;
    }

    validate(c: AbstractControl): ValidationErrors {
        if (this.disabled) {
            return null;
        }
        let length = this.selectedValue ? this.selectedValue.length : 0;
        if (this.required === true && length === 0) {
            this.isValid = false;
            return { required: true };
        }
        if (this.minCount > 0 && length < this.minCount) {
            this.isValid = false;
            return { minCount: true };
        }
        if (this.maxCount > 0 && length > this.maxCount) {
            this.isValid = false;
            return { maxCount: true };
        }
        this.isValid = true;
        return null;
    }

    public initSelect2() {
        if (this._jqueryElement.hasClass('select2-hidden-accessible') == true) {
            this._jqueryElement.select2('destroy');
            this._renderer.setProperty(this.selectControll.nativeElement, 'innerHTML', '');
        }
        this.data = this.data ? this.uniqueArray(this.data) : [];
        if(!this.options.multiple) {
            this.data.unshift({ id: '', text: '' });
            this.options.allowClear = true;
        }
        var data = $.map(this.data, function (obj) {
            obj.text = obj.text || obj.name; // replace name with the property used for the text
            return obj;
        });
        let options: any = {
            data: data
        };
        this._jqueryElement.attr("data-placeholder", this.placeholder);
        options = Object.assign(options, this.options);
        this._jqueryElement.select2(options);
    }

    private setSelect2Value(value: any | Array<any>) {
        if (!this._jqueryElement || !value) {
            this.selectedValue = value;
            return;
        };
        let targetVal = value['id'] || value;
        if (Array.isArray(value) && value.length>0) {
            if(typeof value[0] === 'object'){
                targetVal = value.map(x => x['id']);
            }
            else{
                targetVal=value;
            }
        }
        
        this._jqueryElement.val(targetVal).trigger('change');
    }

    private hasChanged(changes: any) {
        if (changes['data'] && JSON.stringify(changes['data'].previousValue) !== JSON.stringify(changes['data'].currentValue)) {
            return true;
        }
        if (changes['options'] && JSON.stringify(changes['options'].previousValue) !== JSON.stringify(changes['options'].currentValue)) {
            return true;
        }
        return false;
    }

    uniqueArray(list: any[], alias: string = 'id') {
        let values = list || [];
        let unique: any = [];
        values.forEach(x => {
            if (x.id && unique.filter(y => y.id == x.id).length == 0) {
                unique.push(x);
            }
        });
        unique.sort((a, b) => (a.name > b.name) ? 1 : -1);
        return unique;
    }
}