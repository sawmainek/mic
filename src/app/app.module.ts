import { RevisionsComponent } from './pages/revisions/revisions.component';
import { TaxIncentivePreviewService } from './pages/tax-incentive/tax-incentive-preview/tax-incentive-preview.service';
import { LandRightPreviewService } from './pages/land-right/land-right-preview/land-right-preview.service';
import { EndorsementPreviewService } from './pages/endorsement/endorsement-preview/endorsement-preview.service';
import { MICFormPreviewService } from './pages/mic-permit/mic-preview/mic-preview.service';
import { InquiryPreviewService } from './pages/inquiry/inquiry-preview/inquiry-preview.service';
import { QuillModule } from 'ngx-quill';
import { PrintPDFService } from './custom-component/print.pdf.service';
import { GlobalModule } from './pages/global.module';
import { OfficerComponent } from './officer/officer.component';
import { OfficerModule } from './officer/officer.module';
import { CustomComponentModule } from './custom-component/custom-component.module';
import { PagesModule } from './pages/pages.module';
import { FormControlService } from './custom-component/dynamic-forms/form-control.service';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PaymentComponent } from './pages/payment/payment.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './core/reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';

import { authReducer, AuthEffects } from './core';
import { AuthGuard } from './core/auth/_guards/auth.guards';
import { StatusCheckerDetailComponent } from './pages/status-checker/status-checker-detail/status-checker-detail.component';
import { StatusCheckerComponent } from './pages/status-checker/status-checker/status-checker.component';
import { BaseComponent } from './pages/base-page/base-component';
import { Injector } from '@angular/core';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ToastrModule } from 'ngx-toastr';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './pages/account/profile/profile.component';

import { SideMenuComponent } from './pages/account/side-menu/side-menu.component';
import { ChangeEmailComponent } from './pages/account/change-email/change-email.component';
import { ChangePasswordComponent } from './pages/account/change-password/change-password.component';
import { CommonHeaderComponent } from './common-header/common-header.component';
import { CommonFooterComponent } from './common-footer/common-footer.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { formReducer } from './core/form';
import { PaymentHistoryComponent } from './pages/status-checker/payment-history/payment-history.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { DraftApplicationComponent } from './pages/status-checker/draft-application/draft-application.component';

import * as LogRocket from 'logrocket';
import createNgrxMiddleware from 'logrocket-ngrx';
import { ApprovedComponent } from './pages/status-checker/approved/approved.component';
import { ChartsModule } from 'ng2-charts';
import { FormHistoryComponent } from './pages/status-checker/form-history/form-history.component';

// options is the same object you would pass to LogRocket.reduxMiddleware()
// const logrocketMiddleware = createNgrxMiddleware(LogRocket, null);

// export function getMetaReducers(): MetaReducer<AppState>[] {
//     return metaReducers.concat([logrocketMiddleware]);
// }

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
export let InjectorInstance: Injector;

@NgModule({
    declarations: [
        AppComponent,
        HomePageComponent,
        PaymentComponent,
        StatusCheckerDetailComponent,
        StatusCheckerComponent,
        PaymentHistoryComponent,
        BaseComponent,
        OfficerComponent,
        ProfileComponent,
        ChangePasswordComponent,
        SideMenuComponent,
        ChangeEmailComponent,
        CommonHeaderComponent,
        CommonFooterComponent,
        DraftApplicationComponent,
        ApprovedComponent,
        RevisionsComponent,
        FormHistoryComponent
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        NgxDocViewerModule,
        HttpClientModule,
        AppRoutingModule,
        CustomComponentModule,
        GlobalModule,
        QuillModule.forRoot(),
        NgbModule,
        TooltipModule,
        PagesModule,
        OfficerModule,
        ChartsModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
        }),
        
        StoreModule.forFeature('auth', authReducer),
        StoreModule.forFeature('form', formReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([AuthEffects]),
        StoreModule.forRoot(reducers, { metaReducers }),
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ],
    providers: [
        AuthGuard,
        TranslateService,
        FormControlService,
        InquiryPreviewService,
        MICFormPreviewService,
        EndorsementPreviewService,
        LandRightPreviewService,
        TaxIncentivePreviewService,
        PrintPDFService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(private injector: Injector) {
        InjectorInstance = this.injector;
    }
}
