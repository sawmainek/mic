import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/auth/login/user.service';
import { AppState, UpdateUser } from 'src/app/core';
import { MustMatch } from 'src/app/_helper/must-match.validator';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  menu: any = 'password';
  hide = true;
  user: any;
  loading = false;
  lang = 'en';
  passwordForm: FormGroup;
  passwordSubmit = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private translateService: TranslateService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private toastr: ToastrService,
  ) {
    this.lang = localStorage.getItem('lan') ? localStorage.getItem('lan') : 'en';
    this.translateService.setDefaultLang(this.lang ? this.lang : 'en');
    this.translateService.use(this.lang ? this.lang : 'en');
   }

  ngOnInit(): void {
    this.passwordForm = this.formBuilder.group({
      current_password: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password_confirmation: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validator: MustMatch('password', 'password_confirmation')
    });
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.loading = false;
    this.authService.getCurrentUser().subscribe((result: any) => {
      this.loading = false;
      this.user = result;
    });
  }

  checkPasswordForm() {
    this.passwordSubmit = true;
    if (this.passwordForm.invalid) {
      return;
    } else {
      this.changeNewPassword();
    }
  }

  get ControlPasswordForm() {
    return this.passwordForm.controls;
  }

  changeNewPassword() {
    this.loading = true;
    this.userService.changePassword(this.passwordForm.value, this.user.id, this.user.personal_access_token).subscribe(result => {
      window.location.reload();
      this.loading = false;
      this.store.dispatch(new UpdateUser({ user: result }));
      this.toastr.success('Successfully changed password.');
    },
      error => {
        this.loading = false;
        console.log(JSON.stringify(error.error.message));
        this.toastr.error('Your Current Password is Wrong.');
      });
  }

}
