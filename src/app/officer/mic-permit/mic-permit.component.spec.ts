import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicPermitComponent } from './mic-permit.component';

describe('MicPermitComponent', () => {
  let component: MicPermitComponent;
  let fixture: ComponentFixture<MicPermitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicPermitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicPermitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
