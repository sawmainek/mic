import { QuillModule } from 'ngx-quill';
import { MicPermitModule as PageMicPermitModule } from './../../pages/mic-permit/mic-permit.module';
import { MicPermitReplyComponent } from './mic-permit-reply/mic-permit-reply.component';
import { GlobalModule } from './../../pages/global.module';
import { MicPermitComponent } from './mic-permit.component';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomComponentModule } from './../../custom-component/custom-component.module';
import { MicPermitRoutingModule } from './mic-permit-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MicPermitDetailComponent } from './mic-permit-detail/mic-permit-detail.component';
import { MicPaymentTransComponent } from './mic-payment-trans/mic-payment-trans.component';
import { MICPermitIssuedComponent } from './micpermit-issued/micpermit-issued.component';
import { CostBenefitAnalysisComponent } from './cost-benefit-analysis/cost-benefit-analysis.component';
import { MicPermitHistoryComponent } from './mic-permit-history/mic-permit-history.component';


@NgModule({
    declarations: [
        MicPermitComponent,
        MicPermitReplyComponent,
        MicPermitDetailComponent,
        MicPaymentTransComponent,
        MICPermitIssuedComponent,
        CostBenefitAnalysisComponent,
        MicPermitHistoryComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        GlobalModule,
        QuillModule.forRoot(),
        PageMicPermitModule,
        MicPermitRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ], providers: [
        TranslateService,
    ],
})
export class MicPermitModule { }
