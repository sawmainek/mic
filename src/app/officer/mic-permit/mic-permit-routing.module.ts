import { MicPermitDetailComponent } from './mic-permit-detail/mic-permit-detail.component';
import { MicPermitReplyComponent } from './mic-permit-reply/mic-permit-reply.component';
import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: MicPermitReplyComponent,
        children: [
            { path: '', component: MicPermitDetailComponent }
        ]
    },
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class MicPermitRoutingModule { }