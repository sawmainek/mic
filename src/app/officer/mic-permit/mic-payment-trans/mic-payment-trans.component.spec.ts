import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicPaymentTransComponent } from './mic-payment-trans.component';

describe('MicPaymentTransComponent', () => {
  let component: MicPaymentTransComponent;
  let fixture: ComponentFixture<MicPaymentTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicPaymentTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicPaymentTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
