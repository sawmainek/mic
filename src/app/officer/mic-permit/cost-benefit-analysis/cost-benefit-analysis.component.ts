import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { DatePipe, Location } from '@angular/common';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { LocalService } from 'src/services/local.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { MicSubmissionService } from 'src/services/mic-submission.service';

@Component({
    selector: 'app-cost-benefit-analysis',
    templateUrl: './cost-benefit-analysis.component.html',
    styleUrls: ['./cost-benefit-analysis.component.scss']
})
export class CostBenefitAnalysisComponent implements OnInit {
    @Input() id: any;
    mm: any;

    formGroup: FormGroup;
    micModel: any;
    user: any = {};
    model: any = {};
    form: any;

    loading = false;
    loaded: boolean = false;
    spinner = false;


    forms: BaseForm<string>[] = [
        new BaseFormGroup({
            key: 'cost_benefit_analysis',
            formGroup: [
                new FormInput({
                    key: 'investing_in_promoted_sector',
                    label: 'Investing in Promoted Sector (Mark)',
                    required: true,
                    commentable: false,
                    editable: true
                }),
                new FormInput({
                    key: 'investing_in_priority_sector',
                    label: 'Investing in Priority Sector (Mark)',
                    required: true,
                    commentable: false,
                    editable: true
                }),
                new FormInput({
                    key: 'zone',
                    label: 'Zone (Mark)',
                    required: true,
                    commentable: false,
                    editable: true
                }),
                new FormTitle({
                    label: 'Quality of Company'
                }),
                new FormInput({
                    key: 'quality_of_company_data',
                    label: 'Actual Data',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'quality_of_company_mark',
                    label: 'Given Marks',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormTitle({
                    label: 'Technical standard'
                }),
                new FormInput({
                    key: 'technical_standard_data',
                    label: 'Actual Data',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'technical_standard_mark',
                    label: 'Given Marks',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormTitle({
                    label: 'CSR Activities'
                }),
                new FormInput({
                    key: 'csr_activities_data',
                    label: 'Actual Data',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'csr_activities_mark',
                    label: 'Given Marks',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormTitle({
                    label: 'Basic Resources Use (%)'
                }),
                new FormInput({
                    key: 'basic_resources_Data',
                    label: 'Actual Data',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'basic_resources_mark',
                    label: 'Given Marks',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormTitle({
                    label: 'The Amount of the Rest of Waste Disposal'
                }),
                new FormInput({
                    key: 'amount_rest_waste_disposal_data',
                    label: 'Actual Data',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'amount_rest_waste_disposal_mark',
                    label: 'Given Marks',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormTitle({
                    label: 'Environmental Impact Assessment'
                }),
                new FormInput({
                    key: 'environmental_impact_assessment_data',
                    label: 'Actual Data',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'environmental_impact_assessment_mark',
                    label: 'Given Marks',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormTitle({
                    label: 'Social Impact Assessment'
                }),
                new FormInput({
                    key: 'social_impact_assessment_data',
                    label: 'Actual Data',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'social_impact_assessment_mark',
                    label: 'Given Marks',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormTitle({
                    label: 'Domestic Raw Material Use (%)'
                }),
                new FormInput({
                    key: 'domestic_raw_material_data',
                    label: 'Actual Data',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
                new FormInput({
                    key: 'domestic_raw_material_mark',
                    label: 'Given Marks',
                    required: true,
                    commentable: false,
                    editable: true,
                    columns: 'col-md-6'
                }),
            ]
        })
    ];

    constructor(
        private store: Store<AppState>,
        public formCtlService: FormControlService,
        public location: Location,
        private localService: LocalService,
        private toast: ToastrService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private micSubmissionService: MicSubmissionService
    ) {
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.forms);
    }

    ngOnInit(): void {
        this.getCurrentUser();
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;

            this.micSubmissionService.get({ search: `mic_id:equal:${this.id}` }).subscribe(submissions => {
                this.micModel = submissions[0] || null;
                this.micModel.cost_benefit_analysis = this.micModel?.cost_benefit_analysis || {};
                this.formGroup = this.formCtlService.toFormGroup(this.forms, this.micModel, {});
            });
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onSubmit() {
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }

        this.spinner = true;
        this.micModel.cost_benefit_analysis.entry_by = this.user.id;
        this.micSubmissionService.update(this.micModel.id, { ...this.micModel, ...this.formGroup.value }, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                this.toast.success('Saved successfully.');
            }, error => {
                console.log(error);
            });
    }

}
