import { ISICService } from 'src/services/isic.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../../services/auth.service';
import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { getStatus } from '../officer';
declare var jQuery: any;
@Component({
    selector: 'app-mic-permit',
    templateUrl: './mic-permit.component.html',
    styleUrls: ['./mic-permit.component.scss']
})
export class MicPermitComponent implements OnInit {

    user: any = {};
    datatable: any;
    loading: boolean = true;
    sector: any;
    sectors: any[] = [];
    dateRange: any;
    status: string = 'All';
    displayedColumns: any[] = [];
    sub_status: string = '';
    statuses: any[] = [
        { id: 'All', name: 'All' },
        { id: 'New', name: 'Ready for Review' },
        { id: 'Revision', name: 'Waiting for Reply' },
    ]
    constructor(
        private authService: AuthService,
        private activatedRoute: ActivatedRoute,
        private isicService: ISICService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.getSector();
        this.activatedRoute.queryParams.subscribe(params => {
            this.status = params['status'] || 'All';
            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                this.displayedColumns = [
                    {
                        field: 'application_no',
                        title: 'Application Number',
                        type: 'number',
                        width: 100,
                    },
                    {
                        field: 'updated_at',
                        title: 'Date of Submissions',
                        sortable: true,
                        width: 100,
                        template: (row) => {
                            const datePipe = new DatePipe('en');
                            return datePipe.transform(row.updated_at, 'dd-MM-yyyy');
                        }
                    },
                    {
                        field: 'investment_enterprise_name',
                        title: 'Name of Investment Enterprise',
                        sortable: true,
                        width: 150,
                    },
                    {
                        field: 'investment_type',
                        title: 'Investment Type',
                        sortable: true,
                        width: 150,
                    },
                    {
                        field: 'investment_value',
                        title: 'Investment Value  (Million in Kyats)',
                        type: 'number',
                        width: 100,
                        sortable: true,
                        template: (row) => {
                            return this.formatNumber(row?.investment_value || 0);
                        }
                    },
                    {
                        field: 'sector',
                        title: 'Sector of Investment',
                        sortable: true,
                        width: 150
                    }
                ]
                if (this.status == 'All' || this.status == 'Pending' || this.status == 'Revision' || this.status == 'PAT Meetings' || this.status == 'MIC Meetings' || this.status == 'Acceptance' || this.status == 'Rejects') {
                    this.displayedColumns.push({
                        field: 'officer',
                        title: 'Replied By',
                        sortable: true,
                        width: 100,
                        template: (row) => {
                            return row?.officer?.name || '';
                        }
                    })
                }
                if (this.status == 'All') {
                    this.displayedColumns.push({
                        field: 'status',
                        title: 'Status',
                        sortable: true,
                        width: 170,
                        template: (row) => {
                            let status = getStatus(row.status)
                            return '<button class="btn ' + status + '">' + row.status + '</button>';
                        }
                    })
                }
                if (this.status == 'Revision') {
                    this.displayedColumns.push({
                        field: 'sub_status',
                        title: 'Status',
                        sortable: true,
                        width: 170,
                        template: (row) => {
                            if (row.status == 'Revision' && (row.sub_status == 'New' || row.sub_status == 'Reply')) {
                                return '<button class="btn btn-warning">Ready for Review</button>';
                            } else {
                                return '<button class="btn btn-info">Waiting for Reply</button>';
                            }
                        }
                    })
                }
                this.initDatatable();
            }, err => {
                console.log(err);
            });
        });
    }

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    ngAfterViewInit(): void {
        this.loadDatePicker();
    }

    loadDatePicker() {
        const $this = this;
        jQuery('.form-daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-light-primary',
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            }
        });
        jQuery('.form-daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            $this.dateRange = {};
            $this.dateRange['start_date'] = picker.startDate.format('YYYY-MM-DD');
            $this.dateRange['end_date'] = picker.endDate.format('YYYY-MM-DD');
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
        jQuery('.form-daterangepicker').on('cancel.daterangepicker', function (ev, picker) {
            $this.dateRange = null;
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val('');
        });
    }

    getSector() {
        this.isicService.getSector().subscribe(res => {
            this.sectors = this.isicService.uniqueArray(res);
        });
    }

    onChangeSector() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    onChangeStatus() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    buildSearch() {
        let search = this.status != 'All' ? `status:equal:${this.status}` : ``;
        if (this.sector) {
            search += `|sector:equal:${this.sector}`;
        }
        if (this.sub_status && this.sub_status != 'All') {
            search += `|sub_status:equal:${this.sub_status}`;
        }
        if (this.dateRange) {
            search += `|created_at:bigger_equal:${this.dateRange.start_date}|created_at:smaller_equal:${this.dateRange.end_date}`
        }
        return search;
    }

    initDatatable() {
        this.user.personal_access_token
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };
        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/submissionmic',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: this.buildSearch(),
                            query: '',
                            datatable: true
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            },
            search: {
                input: jQuery('#mic-permit-search'),
            }
        };
        if (this.datatable) { jQuery('.kt-datatable-mic-permit').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-mic-permit').KTDatatable(options);
        console.log(this.datatable)
        jQuery('.kt-datatable-mic-permit').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;
            jQuery('.onclick').on('click', function () {
                setTimeout(() => {
                    const index = jQuery(this).data('row');
                    const data = ctx.datatable.getDataSet();
                    if (data[index].mic_id) {
                        ctx.goto(data[index]?.mic_id); 
                    }
                }, 300);
            });
        });
    }

    goto(id) {
        this.router.navigate(['/officer/mic/choose-language', id]);
    }

}
