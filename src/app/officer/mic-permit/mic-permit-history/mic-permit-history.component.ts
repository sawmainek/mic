import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/services/auth.service';
declare var jQuery: any;

@Component({
    selector: 'app-mic-permit-history',
    templateUrl: './mic-permit-history.component.html',
    styleUrls: ['./mic-permit-history.component.scss']
})
export class MicPermitHistoryComponent implements OnInit {
    type: any;
    user: any = {};
    histories: any[] = [];
    displayedColumns: any[] = [];
    datatable: any;

    @Input() submission_id: number;
    @Input() form_id: number;

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.authService.getCurrentUser().subscribe(user => {
            this.user = user;
            this.displayedColumns = [
                {
                    field: 'created_at',
                    title: 'Date',
                    sortable: true,
                    template: (row) => {
                        const datePipe = new DatePipe('en');
                        return datePipe.transform(row.created_at, 'dd-MM-yyyy hh:mm a');
                    }
                },
                {
                    field: 'user_id',
                    title: 'Submitted By',
                    sortable: true,
                    template: (row) => {
                        return row.incoming == true ? row?.user?.name || '' : '';
                    }
                },
                {
                    field: 'officer_id',
                    title: 'Replied By',
                    sortable: true,
                    template: (row) => {
                        return row.incoming == false ? row?.officer?.name || '' : '';
                    }
                },
                {
                    field: 'status',
                    title: 'Status',
                    sortable: true,
                },
                {
                    field: 'sub_status',
                    title: 'Action',
                    sortable: true,
                    template: (row) => {
                        return row.sub_status == 'Reply' ? 'Sent' : 'Received - ' + (row?.sub_status || '');
                    }
                },
                {
                    field: 'goto',
                    title: '',
                    width: 80,
                    sortable: false,
                    template: (row) => {
                        if (row.message_id) {
                            return `<button class="btn btn-outline-info btn-message" data-id="${row.message_id}">
                                <i class="fa fa-commenting"></i>
                            </button>`;
                        }
                        return ``;
                    }
                }
            ];

            this.initDatatable();
        })
    }

    initDatatable() {
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };

        var search = `type:equal:MIC-Permit|type_id:equal:${this.submission_id}`;

        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/formlog',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: search,
                            query: '',
                            datatable: true,
                            sort: 'created_at',
                            order: 'desc'
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            }
        };
        if (this.datatable) { jQuery('.kt-datatable-officer-mic-history').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-officer-mic-history').KTDatatable(options);
        console.log(this.datatable)
        jQuery('.kt-datatable-officer-mic-history').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;
            jQuery('.btn-message').on('click', function () {
                setTimeout(() => {
                    const id = jQuery(this).data('id');
                    ctx.goto(id);
                }, 300);
            });
        });
    }

    goto(id) {
        this.router.navigateByUrl(`/officer/mic/choose-language/${this.form_id}?tab=reply&message_id=${id}`);
    }
}
