import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicPermitHistoryComponent } from './mic-permit-history.component';

describe('MicPermitHistoryComponent', () => {
  let component: MicPermitHistoryComponent;
  let fixture: ComponentFixture<MicPermitHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicPermitHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicPermitHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
