import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MICPermitIssuedComponent } from './micpermit-issued.component';

describe('MICPermitIssuedComponent', () => {
  let component: MICPermitIssuedComponent;
  let fixture: ComponentFixture<MICPermitIssuedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MICPermitIssuedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MICPermitIssuedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
