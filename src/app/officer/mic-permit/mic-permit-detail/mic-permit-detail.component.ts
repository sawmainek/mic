import { ActivatedRoute } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-mic-permit-detail',
    templateUrl: './mic-permit-detail.component.html',
    styleUrls: ['./mic-permit-detail.component.scss']
})
export class MicPermitDetailComponent implements OnInit {
    id: any;
    index: any;
    form: any;
    constructor(
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.index = paramMap.get('index') || null;
            this.form = paramMap.get('form');
        });
    }
}
