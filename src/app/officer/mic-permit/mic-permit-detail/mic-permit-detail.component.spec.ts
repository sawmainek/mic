import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicPermitDetailComponent } from './mic-permit-detail.component';

describe('MicPermitDetailComponent', () => {
  let component: MicPermitDetailComponent;
  let fixture: ComponentFixture<MicPermitDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicPermitDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicPermitDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
