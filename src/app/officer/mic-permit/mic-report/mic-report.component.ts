import { Component, OnInit } from '@angular/core';
import { MicReportService } from 'src/services/mic-report.service';

@Component({
  selector: 'app-mic-report',
  templateUrl: './mic-report.component.html',
  styleUrls: ['./mic-report.component.scss']
})
export class MicReportComponent implements OnInit {

  loading: boolean = true;
  reports: any;

  constructor(
    private micReport: MicReportService
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.micReport.get().subscribe(x => {
      this.reports = x;
      this.loading = false;
      console.log(this.reports)
    })
  }

}
