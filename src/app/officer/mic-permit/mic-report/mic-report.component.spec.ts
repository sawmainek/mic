import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicReportComponent } from './mic-report.component';

describe('MicReportComponent', () => {
  let component: MicReportComponent;
  let fixture: ComponentFixture<MicReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
