import { FormService } from './../../../../services/form.service';
import { FormLogService } from './../../../../services/form-log.service';
import { MICFormPreviewService } from '../../../pages/mic-permit/mic-preview/mic-preview.service';
import { MICPaymentService } from './../../../../services/micpayment.service';
import { currentRole } from './../../../core/auth/_selectors/auth.selectors';
import { MicMessageService } from './../../../../services/mic-message.service';
import { Comments } from './../../../core/form/_actions/form.actions';
import { MessageTemplateService } from './../../../../services/message-template.service';
import { MicSubmissionService } from 'src/services/mic-submission.service';
import { MICReplyService } from './../../../../services/micreply.service';
import { mergeMap, concatMap, delay, tap, finalize } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { comments } from './../../../core/form/_selectors/form.selectors';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { FileUploadService } from 'src/services/file_upload.service';
import { FormCommentService } from './../../../../services/form-comment.service';
import { environment } from './../../../../environments/environment';
import { AppState } from 'src/app/core/reducers';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CostBenefitExport, SalientProintExport, SummaryExport, CertificateExport } from './../mic-permit-word';
import { ISICService } from 'src/services/isic.service';
declare var jQuery: any;

@Component({
    selector: 'app-mic-permit-reply',
    templateUrl: './mic-permit-reply.component.html',
    styleUrls: ['./mic-permit-reply.component.scss']
})
export class MicPermitReplyComponent implements OnInit {
    user: any;
    roles: string[] = [];
    comments: any[] = [];
    templates: any[] = [];
    id: any;
    msg: any = {};
    messages: any[] = [];
    selectedMessage: any;
    changeStatus: boolean = false;
    allow_revision: boolean;
    payment: any = {};
    histories: any[] = [];
    submission: any = {};
    showMore: boolean = false;
    loading: boolean = false;
    submitted: boolean = false;
    host: string = environment.host;
    selectedTab: string = 'form';
    isic: any = [];

    constructor(
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute,
        private micPermitReplyService: MICReplyService,
        private micPermitMsgService: MicMessageService,
        private micPermitSubmissionService: MicSubmissionService,
        private formCmtService: FormCommentService,
        private micPaymentService: MICPaymentService,
        private messageTplService: MessageTemplateService,
        private toast: ToastrService,
        private router: Router,
        private micPreviewService: MICFormPreviewService,
        private formLogService: FormLogService,
        private fileUploadService: FileUploadService,
        private isicService: ISICService
    ) {
        this.isicService.getISIC().subscribe(x => {
            this.isic = x;
        })

        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
        this.store.pipe(select(comments))
            .subscribe(res => {
                this.comments = [...res.filter(x => x.resolved != true)];
            });
        this.store.pipe(select(currentRole))
            .subscribe(roles => {
                this.roles = roles;
            });
        
    }

    loadDatePicker() {
        jQuery('.form-datetimepicker').datetimepicker({
            format: 'dd-mm-yyyy hh:ii',
            minDate: 'today',
            todayHighlight: true,
            autoclose: true
        });
        jQuery('body').on('change', '.form-datetimepicker', function () {
            jQuery(this).trigger('click');
        });
    }

    onChangeStatus() {
        this.msg.subject = this.msg.status;
        setTimeout(() => {
            this.loadDatePicker();
        }, 300);
    }

    onChangeTemplate() {
        this.msg.message = this.templates.filter(x => x.id == this.msg.template)[0].template || null;
    }

    downloadFile(path: string, type: string) {
        this.fileUploadService.download(path)
            .subscribe(url => {
                window.open(url, '_blank');
            });
    }

    getStatusStyle(status) {
        switch (status) {
            case 'Pending':
                return 'text-muted';
            case 'Revision':
                return 'text-warning';
            case 'PAT Meetings':
                return 'text-info';
            case 'MIC Meetings':
                return 'text-info';
            case 'Acceptance':
                return 'text-success';
            case 'Rejects':
                return 'text-danger';
            case 'Payment':
                return 'text-warning';
            default:
                break;
        }
    }

    onFileChange(file) {
        this.msg.attachments = [...this.msg.attachments || []];
        this.msg.attachments.push(file);
    }

    onDateChange(value, key) {
        this.msg[key] = value;
    }

    ngOnInit(): void {
        this.loadDatePicker();
        const params = new URLSearchParams(window.location.search);
        this.selectedTab = params.get('tab') || 'form';
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.formCmtService.get({
                search: `type:equal:MIC Permit|type_id:equal:${this.id}|submitted:equal:false|resolved:equal:false`
            }).subscribe(comments => {
                this.store.dispatch(new Comments({ comments }));
            })
            this.micPermitSubmissionService.get({ search: `mic_id:equal:${this.id}` }).subscribe(submissions => {
                this.submission = submissions[0] || null;
                this.getHistoryLog();

                this.messageTplService.get({
                    search: `form_type:equal:MIC Permit`,
                    rows: 999
                }).subscribe(res => {
                    this.templates = res.sort((a, b) => a.title > b.title ? 1 : -1);
                })

                if (this.submission) {
                    this.micPermitMsgService.get({ search: `submission_id:equal:${this.submission.id || null}`, order: 'desc' }).subscribe(res => {
                        this.messages = res;
                        this.selectedMessage = this.messages[0] || null;
                        if (params.get('message_id')) {
                            this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
                        }
                        if (this.selectedMessage) {
                            this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
                        }
                        for (let msg of this.messages) {
                            if (msg.attachments && msg.attachments.length > 0) {
                                msg.hasAttach = true;
                            } else {
                                if (msg.reply && msg.reply.filter(x => x.attachments.length > 0).length > 0) {
                                    msg.hasAttach = true;
                                }
                            }
                        }
                    })
                }
            })
        });

        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            const params = new URLSearchParams(window.location.search);
            this.selectedTab = params.get('tab') || 'form';
            if (params.get('message_id')) {
                this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
            }
        });
    }

    openTab(tab: string) {
        this.router.navigateByUrl(`/officer/mic/choose-language/${this.id}?tab=${tab}`);
    }

    getHistoryLog() {
        this.formLogService.get({
            search: `type:equal:MIC-Permit|type_id:equal:${this.submission.id}`
        }).subscribe(results => {
            this.histories = results;
        })
    }

    previewFormHostory(data) {
        if (data) {
            this.micPreviewService.toFormPDF(data, { readOnly: true });
        }
    }

    previewForm(lan) {
        var data = lan == 'en' ? this?.submission?.form.data : this?.submission?.form.data_mm;
        this.micPreviewService.toFormPDF(data, { readOnly: true, language: lan });
    }

    printDocFile(index, lan) {
        switch (index) {
            case 1:
                const salientPointExport = new SalientProintExport(this.submission, lan);
                salientPointExport.download().subscribe();
                break;
            case 2:
                const summaryExport = new SummaryExport(this.submission, lan);
                summaryExport.download().subscribe();
                break;
            case 3:
                const costBenefitExport = new CostBenefitExport(this.submission, lan, this.isic);
                costBenefitExport.download().subscribe();
                break;
            case 4:
                const certificateExport = new CertificateExport(this.submission, lan);
                certificateExport.download().subscribe();
            default:
                break;
        }
    }

    openMessage(msg: any = null) {
        this.selectedMessage = msg ? msg : null;
        if (this.selectedMessage) {
            this.selectedMessage.reply = msg.reply || [];
            this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
        }
        this.msg.subject = msg ? msg.subject : null;
    }

    createMessage() {
        let files: any[] = [];
        let uploadedFiles = [];
        this.submitted = true;
        if (this.changeStatus && (this.msg.status == 'PAT Meetings' || this.msg.status == 'MIC Meetings')) {
            if (!this.msg.appointment_date_1 || !this.msg.appointment_date_2) {
                this.toast.error('Please enter appointment date.');
                return;
            }
        }
        if (this.changeStatus && this.msg.status == 'Payment') {
            if (!this.payment.amount || !this.payment.description) {
                this.toast.error('Please enter payment information.');
                return;
            }
        }
        this.loading = true;
        files = this.msg.attachments || [];
        of(files).pipe(
            mergeMap((x: [any]) => from(x)),
            concatMap(x => {
                return of(x).pipe(delay(100))
            }),
            concatMap((file: any) => {
                if (file instanceof File) {
                    const formData: any = new FormData();
                    formData.append('type_id', this.id);
                    formData.append('type', 'MIC Permit');
                    formData.append('name', file.name);
                    formData.append('file', file);
                    return this.fileUploadService.upload(formData, { secure: true });
                }
                return of(file);
            }),
            tap(x => {
                uploadedFiles.push(x);
            }),
            finalize(() => {

                this.msg.role = 'Officer';
                this.msg.user_id = this.user.id;
                this.msg.attachments = uploadedFiles;
                this.msg.submission_id = this.submission.id;

                this.formCmtService.get({
                    search: `type:equal:MIC Permit|type_id:equal:${this.id}|submitted:equal:false`
                }).subscribe(comments => {
                    for (let x of comments) {
                        x.submitted = `true`;
                    }
                    this.formCmtService.import(comments).subscribe();
                });

                let log = {
                    type: "MIC-Permit",
                    type_id: this.submission.id,
                    submitted_user_id: this.submission?.submitted_user_id,
                    reply_user_id: this.user.id,
                    incoming: false,
                    extra: {
                        application_no: this.submission?.application_no,
                        form_id: this.submission?.mic_id,
                        history_data: this.submission?.form
                    }
                }

                if (this.selectedMessage) {
                    this.msg.reply_user_id = this.user.id;
                    this.msg.status = this.selectedMessage.status;
                    this.msg.message_id = this.selectedMessage.id;
                    this.msg.reply_subject = this.selectedMessage.subject;
                    this.msg.reply_message = this.msg.message;
                    this.formLogService.create({
                        ...log, ...{
                            status: this.submission.status,
                            sub_status: 'Reply',
                            message_id: this.selectedMessage.id
                        }
                    }).subscribe();
                    this.micPermitReplyService.create(this.msg).subscribe(message => {
                        this.msg = {};
                        this.loading = false;
                        this.submitted = false;
                        this.selectedMessage.reply = this.selectedMessage.reply || [];
                        message.reply_user = this.user;
                        this.selectedMessage.reply.push(message);
                        if (this.allow_revision) {
                            this.submission.reply_user_id = this.user?.id;
                            this.submission.allow_revision = true;
                            this.micPermitSubmissionService.create(this.submission).subscribe();
                        } else {
                            this.submission.reply_user_id = this.user?.id;
                            this.submission.sub_status = 'Ask';
                            this.micPermitSubmissionService.create(this.submission).subscribe();
                        }
                    })

                } else {
                    if (this.changeStatus) {
                        this.submission.reply_user_id = this.user?.id;
                        if (this.msg.status == 'Payment') {
                            this.payment.submission_id = this.submission.id;
                            this.payment.status = 'Unpaid';
                            this.micPaymentService.create(this.payment).subscribe();
                            this.submission.sub_status = this.msg.status || this.submission.status;
                            this.submission.allow_revision = false;
                            this.micPermitSubmissionService.create(this.submission).subscribe();
                        } else {
                            this.submission.status = this.msg.status || this.submission.status;
                            this.submission.sub_status = this.msg.status || this.submission.status;
                            if (this.msg.status == 'Revision') {
                                this.submission.allow_revision = true;
                            } else {
                                this.submission.allow_revision = false;
                            }
                            this.micPermitSubmissionService.create(this.submission).subscribe();
                            
                        }
                    }
                    this.msg.status = this.msg.status || this.submission.status;
                    this.micPermitMsgService.create(this.msg).subscribe(res => {
                        res.reply = [];
                        res.user = this.user;
                        this.msg = {};
                        this.payment = {};
                        this.selectedMessage = res;
                        this.messages.unshift(res);
                        this.loading = false;
                        this.submitted = false;
                        this.changeStatus = false;
                        this.formLogService.create({
                            ...log, ...{
                                status: this.submission.status,
                                sub_status: 'Reply',
                                message_id: this.selectedMessage.id
                            }
                        }).subscribe();
                        //this.router.navigateByUrl(`/officer/mic?status=${this.msg.status}`);
                    })

                }
            }),
        ).subscribe();
    }
}
