import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicPermitReplyComponent } from './mic-permit-reply.component';

describe('MicPermitReplyComponent', () => {
  let component: MicPermitReplyComponent;
  let fixture: ComponentFixture<MicPermitReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicPermitReplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicPermitReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
