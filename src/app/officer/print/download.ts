import { DatePipe } from '@angular/common';

import printPDF from './';
const basePrintData = {
    'addressSender': {
        'person': 'No. 1, ',
        'street': 'Thitsar Road, ',
        'city': 'Yankin Tsp, Yangon.',
        'email': 'dica.ip.mm@gmail.com',
        'phone': '+951 658102, 658 103'
    }
};
export default class Print {
    constructor() {

    }

    downloadInvoice(invoice) {
        const date = new Date();
        const invoices = [];
        const datePipe = new DatePipe('en');

        let totalAmount = 0;
        totalAmount += invoice.amount;
        invoices.push({
            'title': invoice.type,
            'description': invoice.description,
            'total': `Ks ${this.formatNumber(invoice.amount)}`
        })

        printPDF(Object.assign(basePrintData, {
            'title': 'Invoice',
            'filename': `DICA-${invoice.application_no}-${invoice.id}`,
            'invoice': {
                'number': `DICA-${invoice.application_no}-${invoice.id}`,
                'date': datePipe.transform(invoices[0].created_at || new Date()),
                'total': `Ks ${this.formatNumber(totalAmount)}`,
                'company_name': invoice.company_name
            },
            'items': invoices,
            'address': {
                name: invoice.address.name,
                branch: invoice.address.branch,
                address: invoice.address.address,
                phone: invoice.address.phone
            }
        }));
    }

    downloadReceipt(invoice) {
        const date = new Date();
        const invoices = [];
        const datePipe = new DatePipe('en');

        let totalAmount = 0;
        totalAmount += invoice.amount;
        invoices.push({
            'title': invoice.type,
            'description': invoice.description,
            'total': `Ks ${this.formatNumber(invoice.amount)}`
        })

        printPDF(Object.assign(basePrintData, {
            'title': 'Receipt',
            'filename': `DICA-${invoice.application_no}-${invoice.id}`,
            'invoice': {
                'number': `DICA-${invoice.application_no}-${invoice.id}`,
                'date': datePipe.transform(invoices[0].created_at || new Date()),
                'total': `Ks ${this.formatNumber(totalAmount)}`,
                'company_name': invoice.company_name
            },
            'items': invoices,
            'address': {
                name: invoice.address.name,
                branch: invoice.address.branch,
                address: invoice.address.address,
                phone: invoice.address.phone
            }
        }));
    }

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
}


