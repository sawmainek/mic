import newPage from '../newPage';

export default (doc, printData, startY, fontSize, lineSpacing) => {

    let startX = 57;
    const pageWidth = doc.internal.pageSize.width;
    const endX =  pageWidth - startX;

    const tablecol2X = 386;
    const tablecol3X = 460;

    doc.setFontSize(fontSize);

    //-------Table Header---------------------
    startY += lineSpacing * 1.5;

    doc.text('Description', startX, startY);
    doc.text('Amount', endX, startY, 'right');

    startY += lineSpacing;

    doc.line(startX, startY, endX, startY);

    startY += lineSpacing * 1.5;

    //-------Table Body---------------------

    const items = printData.items || [];
    items.map(item => {

        const splitTitle = doc.splitTextToSize(
            item.title,
            tablecol2X - startX - lineSpacing * 1.5
        );
        const heightTitle = splitTitle.length * doc.internal.getLineHeight();

        const splitDescription = doc.splitTextToSize(
            item.description,
            tablecol2X - startX - lineSpacing * 1.5
        );

        const heightDescription = splitDescription.length * doc.internal.getLineHeight();

        startY = newPage(doc, startY, heightDescription + heightTitle);
        doc.text(splitTitle, startX, startY);

        startY += heightTitle;
        doc.text(splitDescription, startX, startY);
        doc.text(item.total, endX, startY, 'right');

        startY += heightDescription + lineSpacing;
    });

    return startY;
}
