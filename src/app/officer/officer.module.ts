import { InquiryModule as OfficerInquiryModule }  from './inquiry/inquiry.module';
import { TaxIncentiveModule as OfficerTaxIncentiveModule} from './tax-incentive/tax-incentive.module';
import { LandRightModule as OfficerLandRightModule} from './land-right/land-right.module';
import { MicPermitModule as OfficerMicPermitModule} from './mic-permit/mic-permit.module';
import { EndorsementModule as OfficerEndorsementModule} from './endorsement/endorsement.module';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { CustomComponentModule } from './../custom-component/custom-component.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from '../officer/account/profile/profile.component';
import { ChangePasswordComponent } from './account/change-password/change-password.component';
import { EndorsementReportComponent } from './endorsement/endorsement-report/endorsement-report.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app-routing.module';
import { MicReportComponent } from './mic-permit/mic-report/mic-report.component';
import { SummaryReportComponent } from './summary-report/summary-report.component';
import { ShareholderDetailReportComponent } from './shareholder-detail-report/shareholder-detail-report.component';
import { LocationReportComponent } from './location-report/location-report.component';
import { ChartsModule } from 'ng2-charts';
import { NotificationLogsComponent } from './notification-logs/notification-logs.component';
@NgModule({
    declarations: [
        
    DashboardComponent,        
    ProfileComponent, ChangePasswordComponent, MicReportComponent, EndorsementReportComponent, MicReportComponent, SummaryReportComponent, ShareholderDetailReportComponent, LocationReportComponent, NotificationLogsComponent],
    exports: [
        MicReportComponent,
        EndorsementReportComponent,
        SummaryReportComponent,
        ShareholderDetailReportComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        ChartsModule,
        HttpClientModule,
        CustomComponentModule,
        OfficerInquiryModule,
        OfficerEndorsementModule,
        OfficerMicPermitModule,
        OfficerLandRightModule,
        OfficerTaxIncentiveModule,
        BrowserAnimationsModule,
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
    ]
})
export class OfficerModule { }
