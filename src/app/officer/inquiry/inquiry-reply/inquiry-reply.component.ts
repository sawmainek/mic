import { FormService } from './../../../../services/form.service';
import { FormLogService } from './../../../../services/form-log.service';
import { InquiryPreviewService } from './../../../pages/inquiry/inquiry-preview/inquiry-preview.service';
import { InquiryMessageService } from './../../../../services/inquiry-message.service';
import { environment } from 'src/environments/environment';
import { InquirySubmissionService } from 'src/services/inquiry-submission.service';
import { FileUploadService } from 'src/services/file_upload.service';
import { mergeMap, concatMap, delay, tap, finalize } from 'rxjs/operators';
import { from, of, forkJoin } from 'rxjs';
import { currentRole, currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { AppState } from 'src/app/core/reducers';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { InquiryReplyService } from 'src/services/inquiry-reply.service';

@Component({
    selector: 'app-inquiry-reply',
    templateUrl: './inquiry-reply.component.html',
    styleUrls: ['./inquiry-reply.component.scss']
})
export class InquiryReplyComponent implements OnInit {

    user: any;
    roles: string[] = [];
    comments: any[] = [];
    id: any;
    inquiryMessages: any[] = [];
    selectedMessage: any;
    submission: any = {};
    msg: any = {};
    status: string;
    allow_revision: boolean;
    showMore: boolean = false;
    loading: boolean = false;
    host: string = environment.host;
    selectedTab: string = 'form';
    constructor(
        private store: Store<AppState>,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private inquiryReplyService: InquiryReplyService,
        private inquiryMsgService: InquiryMessageService,
        private inquirySubmissionService: InquirySubmissionService,
        private inquiryPreviewService: InquiryPreviewService,
        private formLogService: FormLogService,
        private fileUploadService: FileUploadService,
    ) { 
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
        this.store.pipe(select(currentRole))
            .subscribe(roles => {
                this.roles = roles;
            });
    }

    onFileChange(file) {
        this.msg.attachments = [...this.msg.attachments || []];
        this.msg.attachments.push(file);
    }

    downloadFile(path: string, type: string) {
        this.fileUploadService.download(path)
            .subscribe(url => {
                window.open(url, '_blank');
            });
    }

    ngOnInit(): void {
        const params = new URLSearchParams(window.location.search);
        this.selectedTab = params.get('tab') || 'form';
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.inquirySubmissionService.get({search:`inquiry_id:equal:${this.id}`}).subscribe(submissions => {
                this.submission = submissions[0] || null;
                this.inquiryMsgService.get({ search: `submission_id:equal:${this.submission.id}`, order: 'desc' }).subscribe(res => {
                    this.inquiryMessages = res;
                    this.selectedMessage = this.inquiryMessages[0] || null;
                    
                    if (params.get('message_id')) {
                        this.selectedMessage = this.inquiryMessages.filter(x => x.id == params.get('message_id'))[0];
                    }
                    if(this.selectedMessage) {
                        this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
                    }
                    for (let msg of this.inquiryMessages) {
                        if (msg.attachments && msg.attachments.length > 0) {
                            msg.hasAttach = true;
                        } else {
                            if (msg.reply && msg.reply.filter(x => x.attachments.length > 0).length > 0) {
                                msg.hasAttach = true;
                            }
                        }
                    }
                })
            })
            
        });

        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            const params = new URLSearchParams(window.location.search);
            this.selectedTab = params.get('tab') || 'form';
            if (params.get('message_id')) {
                this.selectedMessage = this.inquiryMessages.filter(x => x.id == params.get('message_id'))[0];
            }
        });
    }

    previewFormHostory(data) {
        if(data) {
            this.inquiryPreviewService.toFormPDF(data, { readOnly: true });
        }
    }

    previewForm() {
        this.inquiryPreviewService.toFormPDF(this.submission?.form, { readOnly: true });
    }

    loadShowMore() {
        this.showMore = true;
    }

    openTab(tab: string) {
        this.router.navigateByUrl(`/officer/inquiry/form/${this.id}?tab=${tab}`);
    }

    openMessage(msg: any = null) {
        this.selectedMessage = msg ? msg : null;
        if (this.selectedMessage) {
            this.selectedMessage.reply = msg.reply || [];
            this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
        }
        if (msg && msg.status != 'Read') {
            msg.status = 'Read';
            this.inquiryMsgService.update(msg.id, {status: 'Read'}).subscribe();
        }
    }

    createMessage() {
        let files: any[] = [];
        let uploadedFiles = [];
        this.loading = true;
        files = this.msg.attachments || [];
        of(files).pipe(
            mergeMap((x: [any]) => from(x)),
            concatMap(x => {
                return of(x).pipe(delay(100))
            }),
            concatMap((file: any) => {
                if (file instanceof File) {
                    const formData: any = new FormData();
                    formData.append('type_id', this.id);
                    formData.append('type', 'Inquiry Reply');
                    formData.append('name', file.name);
                    formData.append('file', file);
                    return this.fileUploadService.upload(formData, { secure: true });
                }
                return of(file);
            }),
            tap(x => {
                uploadedFiles.push(x);
            }),
            finalize(() => {
                this.msg.attachments = uploadedFiles || [];
                this.msg.user_id = this.user.id;
                this.msg.submission_id = this.submission.id;
                this.msg.role = 'Officer';
                this.msg.status = this.selectedMessage ? 'Reply' : 'New';
                let log = {
                    type: "Inquiry ( Form 1 )",
                    type_id: this.submission.id,
                    status: this.submission.status,
                    sub_status: 'Reply',
                    submitted_user_id: this.submission?.submitted_user_id,
                    reply_user_id: this.user.id,
                    incoming: false,
                    extra: {
                        application_no: this.submission?.application_no,
                        form_id: this.submission?.inquiry_id,
                        history_data: this.submission?.form
                    }
                }
                if (this.selectedMessage) {
                    this.msg.reply_user_id = this.user.id;
                    this.msg.message_id = this.selectedMessage.id;
                    this.msg.reply_subject = this.msg.subject;
                    this.msg.reply_message = this.msg.message;
                    this.selectedMessage.status = 'Reply';
                    this.inquiryMsgService.create(this.selectedMessage).subscribe();
                    this.formLogService.create({
                        ...log, ...{
                            status: this.submission.status,
                            sub_status: 'Reply',
                            message_id: this.selectedMessage.id
                        }
                    }).subscribe();
                    this.inquiryReplyService.create(this.msg).subscribe(message => {
                        this.msg = {};
                        this.loading = false;
                        this.selectedMessage.reply = this.selectedMessage.reply || [];
                        message.reply_user = this.user;
                        this.selectedMessage.reply.push(message);
                        if (this.allow_revision ) {
                            this.submission.reply_user_id = this.user?.id;
                            this.submission.allow_revision = true;
                            this.inquirySubmissionService.create(this.submission).subscribe();
                        } else {
                            this.submission.reply_user_id = this.user?.id;
                            this.submission.sub_status = 'Reply';
                            this.inquirySubmissionService.create(this.submission).subscribe();
                        }
                    })
                } else {
                    this.submission.status = this.status ? this.status : this.submission.status;
                    this.submission.sub_status = this.status ? this.status : this.submission.status;
                    this.submission.reply_user_id = this.user?.id;
                    if (this.submission.status == 'Revision') {
                        this.submission.allow_revision = true;
                    } else {
                        this.submission.allow_revision = false;
                    }
                    this.inquirySubmissionService.create(this.submission).subscribe();
                    this.inquiryMsgService.create(this.msg).subscribe(res => {
                        res.reply = [];
                        res.user = this.user;
                        this.msg = {};
                        this.selectedMessage = res;
                        this.inquiryMessages.unshift(res);
                        this.formLogService.create({
                            ...log, ...{
                                status: this.submission.status,
                                sub_status: 'Reply',
                                message_id: this.selectedMessage.id
                            }
                        }).subscribe();
                        this.loading = false;
                    })
                }
            }),
        ).subscribe();
    }

}
