import { InquiryModule as PageInquiryModule} from './../../pages/inquiry/inquiry.module';
import { InquiryReplyComponent } from './inquiry-reply/inquiry-reply.component';
import { InquiryComponent } from './inquiry.component';
import { GlobalModule } from './../../pages/global.module';
import { CustomComponentModule } from '../../custom-component/custom-component.module';
import { InquiryRoutingModule } from './inquiry-routing.module';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { InquiryDetailComponent } from './inquiry-detail/inquiry-detail.component';
import { QuillModule } from 'ngx-quill';
import { InquiryPaymentTransComponent } from './inquiry-payment-trans/inquiry-payment-trans.component';
import { InquiryHistoryComponent } from './inquiry-history/inquiry-history.component'


@NgModule({
    declarations: [
        InquiryComponent,
        InquiryReplyComponent,
        InquiryDetailComponent,
        InquiryPaymentTransComponent,
        InquiryHistoryComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        GlobalModule,
        InquiryRoutingModule,
        PageInquiryModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        QuillModule.forRoot(),
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // closeButton: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ], providers: [
        TranslateService,
    ],
})
export class InquiryModule { }
