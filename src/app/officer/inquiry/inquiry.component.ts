import { ISICService } from './../../../services/isic.service';
import { currentPermission } from './../../core/auth/_selectors/auth.selectors';
import { Store, select } from '@ngrx/store';
import { AppState } from './../../core/reducers/index';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../../services/auth.service';
import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
declare var jQuery: any;
@Component({
    selector: 'app-inquiry',
    templateUrl: './inquiry.component.html',
    styleUrls: ['./inquiry.component.scss']
})
export class InquiryComponent implements OnInit {

    user: any = {};
    datatable: any;
    status: string = 'All';
    sub_status: string = '';
    sector: any;
    sectors: any[] = [];
    dateRange: any;
    loading: boolean = true;
    perms: string[] = [];
    displayedColumns: any = [];
    submitted: string = '';
    sumitto: any[] = [
        { id: 'All', name: 'All' },
        { id: 'MIC', name: 'Myanmar Investment Commision' },
        { id: 'State/Region', name: 'States and Regions Investment Committees' },
    ]
    statuses: any[] = [
        { id: 'All', name: 'All' },
        { id: 'New', name: 'Ready for Review' },
        { id: 'Revision', name: 'Waiting for Reply' },
    ]
    constructor(
        private authService: AuthService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private isicService: ISICService,
        private store: Store<AppState>
    ) { }

    ngOnInit(): void {
        this.isicService.getSector().subscribe(res => {
            this.sectors = this.isicService.uniqueArray(res);
        });
        this.store.pipe(select(currentPermission))
            .subscribe(perms => {
                this.perms = perms;
            });
        this.activatedRoute.queryParams.subscribe(params => {
            this.status = params['status'] || 'All';
            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                this.displayedColumns = [
                    {
                        field: 'application_no',
                        title: 'Application Number',
                        type: 'number',
                        width: 100,
                    },
                    {
                        field: 'updated_at',
                        title: 'Date of Submission',
                        sortable: true,
                        width: 100,
                        template: (row) => {
                            const datePipe = new DatePipe('en');
                            return datePipe.transform(row.updated_at, 'dd-MM-yyyy');
                        }
                    },
                    {
                        field: 'investor_name',
                        title: 'Investor Name',
                        sortable: true,
                        width: 150,
                    },
                    {
                        field: 'country',
                        title: 'Country of incorporation or Citizenship',
                        sortable: true,
                        width: 150,
                    },
                    {
                        field: 'investment_value',
                        title: 'Investment Value (Million in Kyats)',
                        type: 'number',
                        sortable: true,
                        width: 100,
                        template: (row) => {
                            return this.formatNumber(row?.investment_value || 0);
                        }
                    },
                    {
                        field: 'sector',
                        title: 'Sector of Investment',
                        width: 150,
                        sortable: true
                    },
                    {
                        field: 'submit_to',
                        title: 'MIC or State/Region',
                        type: 'number',
                        sortable: true,
                        width: 150,
                        template: (row) => {
                            return row?.form?.apply_to == 'States and Regions Investment Committees' ? row?.form?.inquiry_to_state || '' : 'Myanmar Investment Commision';
                        }
                    }
                ];
                if (this.status == 'All' || this.status == 'Revision' || this.status == 'Completed') {
                    this.displayedColumns.push({
                        field: 'officer',
                        title: 'Replied By',
                        sortable: true,
                        template: (row) => {
                            return row?.officer?.name || '';
                        }
                    })
                }
                if (this.status == 'All') {
                    this.displayedColumns.push({
                        field: 'status',
                        title: 'Status',
                        sortable: true,
                        width: 170,
                        template: (row) => {
                            if (row.status == 'New') {
                                return `<button class="btn btn-info">${row.status}</button>`;
                            } else if (row.status == 'Revision') {
                                return `<button class="btn btn-warning">${row.status}</button>`;
                            } else if (row.status == 'Completed') {
                                return `<button class="btn btn-success">${row.status}</button>`;
                            } else {
                                return ``;
                            }
                        }
                    })
                }
                if (this.status == 'Revision') {
                    this.displayedColumns.push({
                        field: 'sub_status',
                        title: 'Status',
                        width: 170,
                        sortable: false,
                        template: (row) => {
                            if (row.status == 'Revision' && (row.sub_status == 'New' || row.sub_status == 'Reply')) {
                                return '<button class="btn btn-warning">Ready for Review</button>';
                            } else {
                                return '<button class="btn btn-info">Waiting for Reply</button>';
                            }
                        }
                    })
                }
                this.initDatatable();
            }, err => {
                console.log(err);
            });
        });
    }

    formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    ngAfterViewInit(): void {
        this.loadDatePicker();
    }

    loadDatePicker() {
        const $this = this;
        jQuery('.form-daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-light-primary',
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            }
        });
        jQuery('.form-daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            $this.dateRange = {};
            $this.dateRange['start_date'] = picker.startDate.format('YYYY-MM-DD');
            $this.dateRange['end_date'] = picker.endDate.format('YYYY-MM-DD');
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
        jQuery('.form-daterangepicker').on('cancel.daterangepicker', function (ev, picker) {
            $this.dateRange = null;
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val('');
        });
    }

    onChangeSector() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    onChangeStatus() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    onChangeSubmission() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    buildSearch() {
        let search = this.status != 'All' ? `status:equal:${this.status}` : ``;
        if (this.sector && this.sector != 'All') {
            search += `|sector:equal:${this.sector}`;
        }
        if (this.sub_status && this.sub_status != 'All') {
            search += `|sub_status:equal:${this.sub_status}`;
        }
        if (this.submitted && this.submitted != 'All') {
            if (this.submitted == 'MIC') {
                search += `|submit_to:equal:Myanmar Investment Commision`;
            } else {
                search += `|submit_to:equal:States and Regions Investment Committees`;
            }
        }
        if (this.dateRange) {
            search += `|created_at:bigger_equal:${this.dateRange.start_date}|created_at:smaller_equal:${this.dateRange.end_date}`
        }
        return search;
    }

    initDatatable() {
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };
        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/submissioninquiry',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: this.buildSearch(),
                            query: '',
                            datatable: true
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            },
            search: {
                input: jQuery('#inquiry-search'),
            }
        };
        if (this.datatable) { jQuery('.kt-datatable-inquiry').KTDatatable('destroy'); }

        this.datatable = jQuery('.kt-datatable-inquiry').KTDatatable(options);
        jQuery('.kt-datatable-inquiry').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;
            jQuery('.onclick').on('click', function () {
                setTimeout(() => {
                    const index = jQuery(this).data('row');
                    const data = ctx.datatable.getDataSet();
                    ctx.goto(data[index]?.inquiry_id);
                }, 300);
            });
        });
    }

    goto(id) {
        if (id) {
            this.router.navigate(['/officer/inquiry/form', id]);
        }
    }

}
