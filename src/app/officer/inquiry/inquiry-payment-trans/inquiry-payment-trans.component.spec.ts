import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryPaymentTransComponent } from './inquiry-payment-trans.component';

describe('InquiryPaymentTransComponent', () => {
  let component: InquiryPaymentTransComponent;
  let fixture: ComponentFixture<InquiryPaymentTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryPaymentTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryPaymentTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
