import { environment } from 'src/environments/environment';
import { AuthService } from 'src/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import printPDF from './../../print';
import { LocalService } from 'src/services/local.service';
import Print from '../../print/download';
declare var jQuery: any;

const basePrintData = {
    'addressSender': {
        'person': 'No. 1, ',
        'street': 'Thitsar Road, ',
        'city': 'Yankin Tsp, Yangon.',
        'email': 'dica.ip.mm@gmail.com',
        'phone': '+951 658102, 658 103'
    }
};

@Component({
    selector: 'app-inquiry-payment-trans',
    templateUrl: './inquiry-payment-trans.component.html',
    styleUrls: ['./inquiry-payment-trans.component.scss']
})
export class InquiryPaymentTransComponent implements OnInit {

    user: any = {};
    datatable: any;
    loading: boolean = true;
    sector: any;
    sectors: any[] = [];
    dateRange: any;
    status: string = '';
    contact: any;

    displayedColumns: any = [
        {
            field: 'id',
            title: 'No.',
            type: 'number',
            width: 30
        },
        {
            field: 'submission',
            title: 'Application Number',
            sortable: false,
            width: 100,
            template: (row) => {
                return row?.submission?.application_no || '';
            }
        },
        {
            field: 'description',
            title: 'Description',
            sortable: false,
            width: 300,
        },
        {
            field: 'amount',
            title: 'Amount',
            sortable: false,
            width: 200,
        },
        {
            field: 'status',
            title: 'Status',
            sortable: false,
            width: 100,
            template: (row) => {
                let status = row.status == 'Paid' ? 'btn-success' : 'btn-warning';
                return '<button class="btn ' + status + '">' + row.status + '</button>';
            }
        },
        {
            field: 'download',
            title: 'Download',
            sortable: false,
            width: 100,
            template: (row) => {
                let style = row.status == 'Paid'
                    ? 'background-color: #FFF; color: #1BC5BD; border: 1px solid #1BC5BD;'
                    : 'background-color: #FFF; color: rgba(251, 197, 98, 0.9); border: 1px solid rgba(251, 197, 98, 0.9);';
                let str = row.status == 'Paid' ? 'Receipt Download' : 'Invoice Download'
                return `<button id="download" type="button" data-id="${row.id}" class="btn" style="${style}">${str}</button>`;
            }
        }
    ];
    constructor(
        private authService: AuthService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private localService: LocalService
    ) { }

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe(params => {
            // this.status = params['status'] || '';
            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                this.initDatatable();
            }, err => {
                console.log(err);
            });
        });
    }

    ngAfterViewInit(): void {
        this.loadDatePicker();
    }

    loadDatePicker() {
        const $this = this;
        jQuery('.form-daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-light-primary',
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            }
        });
        jQuery('.form-daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            $this.dateRange = {};
            $this.dateRange['start_date'] = picker.startDate.format('YYYY-MM-DD');
            $this.dateRange['end_date'] = picker.endDate.format('YYYY-MM-DD');
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
        jQuery('.form-daterangepicker').on('cancel.daterangepicker', function (ev, picker) {
            $this.dateRange = null;
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val('');
        });
    }

    onChangeSector() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    buildSearch() {
        let search = ``;
        if (this.status && this.status.length > 0) {
            search += `|status:equal:${this.status}`;
        }
        if (this.dateRange) {
            search += `|created_at:bigger_equal:${this.dateRange.start_date}|created_at:smaller_equal:${this.dateRange.end_date}`
        }
        return search;
    }

    initDatatable() {
        this.user.personal_access_token
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };
        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/inquiryinvoice',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: this.buildSearch(),
                            query: '',
                            datatable: true
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            },
            search: {
                input: jQuery('#generalSearch'),
            }
        };
        if (this.datatable) { jQuery('.kt-datatable-inquiry-payment').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-inquiry-payment').KTDatatable(options);
        jQuery('.kt-datatable-inquiry-payment').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;

            jQuery('.download').on('click', function () {
                setTimeout(() => {
                    const print = new Print();
                    const id = jQuery(this).data('id');
                    const data = ctx.datatable.getDataSet();
                    let invoice = data.filter(x => x.id == id)[0] || {};
                    
                    invoice['type'] = 'Inquiry (Form 1)';
                    invoice['application_no'] = invoice?.submission?.application_no;
                    invoice['company_name']  = invoice?.submission?.investment_enterprise_name || '';
                    const role = invoice.submission.submit_to_role || 'division-1';
                    ctx.localService.getContact().subscribe(x => {
                        invoice.address = x.filter(y => y.name == role)[0] || x[0];
                        if (invoice.status == 'Paid') {
                            print.downloadInvoice(invoice);
                        } else {
                            print.downloadReceipt(invoice);
                        }
                    });
                }, 300);
            });
        });
    }

    goto(id) {
        this.router.navigate(['/officer/inquiry/form', id]);
    }

}

