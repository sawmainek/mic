import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-inquiry-detail',
    templateUrl: './inquiry-detail.component.html',
    styleUrls: ['./inquiry-detail.component.scss']
})
export class InquiryDetailComponent implements OnInit {

    form: any;
    id: any;
    constructor(
        private activatedRoute: ActivatedRoute
    ) {
        
    }
    
    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.form = paramMap.get('form');
            this.id = paramMap.get('id');
        });
    }

}
