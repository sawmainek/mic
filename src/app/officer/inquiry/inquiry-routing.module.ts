import { InquiryDetailComponent } from './inquiry-detail/inquiry-detail.component';
import { InquiryReplyComponent } from './inquiry-reply/inquiry-reply.component';
import { TranslateService } from '@ngx-translate/core';
import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: InquiryReplyComponent,
        children: [
            { path: '', component: InquiryDetailComponent }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard,
        TranslateService
    ],
    exports: [RouterModule]
})
export class InquiryRoutingModule { }
