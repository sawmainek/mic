import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-land-right-detail',
  templateUrl: './land-right-detail.component.html',
  styleUrls: ['./land-right-detail.component.scss']
})
export class LandRightDetailComponent implements OnInit {

    id: any;
    form: any;
    constructor(
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id');
            this.form = paramMap.get('form');
            console.log('form : ' + this.form);
        });
    }

}
