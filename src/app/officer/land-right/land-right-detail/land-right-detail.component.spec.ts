import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightDetailComponent } from './land-right-detail.component';

describe('LandRightDetailComponent', () => {
  let component: LandRightDetailComponent;
  let fixture: ComponentFixture<LandRightDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
