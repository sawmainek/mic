import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightHistoryComponent } from './land-right-history.component';

describe('LandRightHistoryComponent', () => {
  let component: LandRightHistoryComponent;
  let fixture: ComponentFixture<LandRightHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
