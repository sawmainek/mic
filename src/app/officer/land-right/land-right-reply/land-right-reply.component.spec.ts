import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightReplyComponent } from './land-right-reply.component';

describe('LandRightReplyComponent', () => {
  let component: LandRightReplyComponent;
  let fixture: ComponentFixture<LandRightReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightReplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
