import { FormService } from './../../../../services/form.service';
import { FormLogService } from './../../../../services/form-log.service';
import { LandRightPreviewService } from './../../../pages/land-right/land-right-preview/land-right-preview.service';
import { LandRightPaymentService } from './../../../../services/land-right-payment.service';
import { currentRole, currentPermission } from './../../../core/auth/_selectors/auth.selectors';
import { LandRightMessageService } from './../../../../services/land-right-message.service';
import { LandrightSubmissionService } from 'src/services/landright-submission.service';
import { LandRightReplyService } from './../../../../services/land-right-reply.service';
import { Comments } from './../../../core/form/_actions/form.actions';
import { MessageTemplateService } from './../../../../services/message-template.service';
import { mergeMap, concatMap, delay, tap, finalize } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { comments } from './../../../core/form/_selectors/form.selectors';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { FileUploadService } from 'src/services/file_upload.service';
import { FormCommentService } from './../../../../services/form-comment.service';
import { environment } from './../../../../environments/environment';
import { AppState } from 'src/app/core/reducers';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
declare var jQuery: any;

@Component({
    selector: 'app-land-right-reply',
    templateUrl: './land-right-reply.component.html',
    styleUrls: ['./land-right-reply.component.scss']
})
export class LandRightReplyComponent implements OnInit {
    user: any;
    roles: string[] = [];
    perms: string[] = [];
    comments: any[] = [];
    templates: any[] = [];
    id: any;
    messages: any[] = [];
    selectedMessage: any;
    payment: any = {};
    msg: any = {};
    changeStatus: boolean = false;
    allow_revision: boolean;
    histories: any[] = [];
    submission: any = {};
    showMore: boolean = false;
    loading: boolean = false;
    submitted: boolean = false;
    host: string = environment.host;
    selectedTab: string = 'form';
    constructor(
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute,
        private landRightReplyService: LandRightReplyService,
        private landRightMsgService: LandRightMessageService,
        private landRightSubmissionService: LandrightSubmissionService,
        private formCmtService: FormCommentService,
        private landRightPaymentService: LandRightPaymentService,
        private messageTplService: MessageTemplateService,
        private formLogService: FormLogService,
        private toast: ToastrService,
        private router: Router,
        private landRightPreviewService: LandRightPreviewService,
        private fileUploadService: FileUploadService
    ) {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
        this.store.pipe(select(comments))
            .subscribe(res => {
                this.comments = [...res.filter(x => x.resolved != true)];
            });
        this.store.pipe(select(currentRole))
            .subscribe(roles => {
                this.roles = roles;
            });
        this.store.pipe(select(currentPermission))
            .subscribe(perms => {
                this.perms = perms;
            });
    }

    checkBranch() {
        const branch = this.perms.filter(x => x.indexOf('branch') !== -1).length;
        return branch > 0 ? true : false;
    }

    loadDatePicker() {
        jQuery('.form-datetimepicker').datetimepicker({
            format: 'dd-mm-yyyy hh:ii',
            minDate: 'today',
            todayHighlight: true,
            autoclose: true
        });
        jQuery('body').on('change', '.form-datetimepicker', function () {
            jQuery(this).trigger('click');
        });
    }

    onChangeStatus() {
        this.msg.subject = this.msg.status;
        setTimeout(() => {
            this.loadDatePicker();
        }, 300);
    }

    downloadFile(path: string, type: string) {
        this.fileUploadService.download(path)
            .subscribe(url => {
                window.open(url, '_blank');
            });
    }

    onChangeTemplate() {
        this.msg.message = this.templates.filter(x => x.id == this.msg.template)[0].template || null;
    }

    getStatusStyle(status) {
        switch (status) {
            case 'Pending':
                return 'text-muted';
            case 'Revision':
                return 'text-warning';
            case 'PAT Meetings':
                return 'text-info';
            case 'MIC Meetings':
                return 'text-info';
            case 'Acceptance':
                return 'text-success';
            case 'Rejects':
                return 'text-danger';
            case 'Payment':
                return 'text-warning';
            default:
                break;
        }
    }

    onFileChange(file) {
        this.msg.attachments = [...this.msg.attachments || []];
        this.msg.attachments.push(file);
    }

    onDateChange(value, key) {
        this.msg[key] = value;
    }

    ngOnInit(): void {
        this.loadDatePicker();
        const params = new URLSearchParams(window.location.search);
        this.selectedTab = params.get('tab') || 'form';
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;
            this.formCmtService.get({
                search: `type:equal:Land Right|type_id:equal:${this.id}|submitted:equal:false|resolved:equal:false`
            }).subscribe(comments => {
                this.store.dispatch(new Comments({ comments }));
            })
            this.landRightSubmissionService.get({ search: `land_right_id:equal:${this.id}` }).subscribe(submissions => {
                this.submission = submissions[0] || null;
                
                this.messageTplService.get({
                    search: `form_type:ilike:Land Authorization`,
                    rows: 999
                }).subscribe(res => {
                    if (this.submission.submit_to_role != 'mic') {
                        res.sort((a, b) => (a.title > b.title) ? 1 : -1);
                        this.templates = res.filter(x => x.form_type == 'Land Authorization(States and Regions)');
                    } else {
                        res.sort((a, b) => (a.title > b.title) ? 1 : -1);
                        this.templates = res.filter(x => x.form_type == 'Land Authorization(MIC)');
                    }
                })

                this.getHistoryLog();
                if (this.submission) {
                    this.landRightMsgService.get({ search: `submission_id:equal:${this.submission.id || null}`, order: 'desc' }).subscribe(res => {
                        this.messages = res;
                        this.selectedMessage = this.messages[0] || null;
                        if (params.get('message_id')) {
                            this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
                        }
                        if(this.selectedMessage) {
                            this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
                        }
                        for (let msg of this.messages) {
                            if (msg.attachments && msg.attachments.length > 0) {
                                msg.hasAttach = true;
                            } else {
                                if (msg.reply && msg.reply.filter(x => x.attachments.length > 0).length > 0) {
                                    msg.hasAttach = true;
                                }
                            }
                        }
                    })
                    
                    this.messageTplService.get({
                        search: `form_type:ilike:Land Authorization`,
                        rows: 999
                    }).subscribe(res => {
                        res.sort((a, b) => (a.title > b.title) ? 1 : -1);
                        if (this.submission?.form?.apply_to == 'States and Regions Investment Committees') {
                            this.templates = res.filter(x => x.form_type == 'Land Authorization(States and Regions)');
                        } else {
                            this.templates = res.filter(x => x.form_type == 'Land Authorization(MIC)');
                        }
                    })

                }
            })
        });

        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            const params = new URLSearchParams(window.location.search);
            this.selectedTab = params.get('tab') || 'form';
            if (params.get('message_id')) {
                this.selectedMessage = this.messages.filter(x => x.id == params.get('message_id'))[0];
            }
        });
    }

    openTab(tab: string) {
        this.router.navigateByUrl(`/officer/land-right/landright-choose-investment/${this.id}?tab=${tab}`);
    }

    getHistoryLog() {
        this.formLogService.get({
            search: `type:equal:Land Right|type_id:equal:${this.submission.id}`
        }).subscribe(results => {
            this.histories = results;
        })
    }

    previewFormHostory(data) {
        if (data) {
            this.landRightPreviewService.toFormPDF(data, { readOnly: true });
        }
    }

    previewForm(lan) {
        var data = lan == 'en' ? this?.submission?.form.data : this?.submission?.form.data_mm;
        this.landRightPreviewService.toFormPDF(data, { readOnly: true, language: lan });
    }

    loadShowMore() {
        this.showMore = true;
    }

    openMessage(msg: any = null) {
        this.selectedMessage = msg ? msg : null;
        if (this.selectedMessage) {
            this.selectedMessage.reply = msg.reply || [];
            this.selectedMessage.reply.sort((a, b) => a.id > b.id ? 1 : -1);
        }
        this.msg.subject = msg ? msg.subject : null;
    }

    createMessage() {
        let files: any[] = [];
        let uploadedFiles = [];
        this.submitted = true;
        if (this.changeStatus && this.msg.status == 'Payment') {
            if (!this.payment.amount || !this.payment.description) {
                this.toast.error('Please enter payment information.');
                return;
            }
        }

        this.loading = true;
        files = this.msg.attachments || [];
        of(files).pipe(
            mergeMap((x: [any]) => from(x)),
            concatMap(x => {
                return of(x).pipe(delay(100))
            }),
            concatMap((file: any) => {
                if (file instanceof File) {
                    const formData: any = new FormData();
                    formData.append('type_id', this.id);
                    formData.append('type', 'Land Right');
                    formData.append('name', file.name);
                    formData.append('file', file);
                    return this.fileUploadService.upload(formData, { secure: true });
                }
                return of(file);
            }),
            tap(x => {
                uploadedFiles.push(x);
            }),
            finalize(() => {

                this.msg.role = 'Officer';
                this.msg.user_id = this.user.id;
                this.msg.attachments = uploadedFiles;
                this.msg.submission_id = this.submission.id;
               
                this.formCmtService.get({
                    search: `type:equal:MIC Permit|type_id:equal:${this.id}|submitted:equal:false`
                }).subscribe(comments => {
                    for (let x of comments) {
                        x.submitted = `true`;
                    }
                    this.formCmtService.import(comments).subscribe();
                });
                let log = {
                    type: "Land Right",
                    type_id: this.submission.id,
                    submitted_user_id: this.submission?.submitted_user_id,
                    reply_user_id: this.user.id,
                    incoming: false,
                    extra: {
                        application_no: this.submission?.application_no,
                        form_id: this.submission?.land_right_id,
                        history_data: this.submission?.form
                    }
                }
                if (this.selectedMessage) {
                    this.msg.reply_user_id = this.user.id;
                    this.msg.status = this.selectedMessage.status;
                    this.msg.message_id = this.selectedMessage.id;
                    this.msg.reply_subject = this.selectedMessage.subject;
                    this.msg.reply_message = this.msg.message;
                    this.formLogService.create({
                        ...log, ...{
                            status: this.submission.status,
                            sub_status: 'Reply',
                            message_id: this.selectedMessage.id
                        }
                    }).subscribe();
                    this.landRightReplyService.create(this.msg).subscribe(message => {
                        this.msg = {};
                        this.loading = false;
                        this.submitted = false;
                        this.selectedMessage.reply = this.selectedMessage.reply || [];
                        message.reply_user = this.user;
                        this.selectedMessage.reply.push(message);
                        if (this.allow_revision) {
                            this.submission.reply_user_id = this.user?.id;
                            this.submission.allow_revision = true;
                            this.landRightSubmissionService.create(this.submission).subscribe();
                        } else {
                            this.submission.reply_user_id = this.user?.id;
                            this.submission.sub_status = 'Ask';
                            this.landRightSubmissionService.create(this.submission).subscribe();
                        }
                    })
                } else {
                    if (this.changeStatus) {
                        
                        this.submission.reply_user_id = this.user?.id;
                        if (this.msg.status == 'Payment') {
                            this.payment.submission_id = this.submission.id;
                            this.payment.status = 'Unpaid';
                            this.landRightPaymentService.create(this.payment).subscribe();
                            this.submission.sub_status = this.msg.status || this.submission.status;
                            this.submission.allow_revision = false;
                            this.landRightSubmissionService.create(this.submission).subscribe();
                        } else {
                            this.submission.status = this.msg.status || this.submission.status;
                            this.submission.sub_status = this.msg.status || this.submission.status;
                            if (this.msg.status == 'Revision') {
                                this.submission.allow_revision = true;
                            } else {
                                this.submission.allow_revision = false;
                            }
                            this.landRightSubmissionService.create(this.submission).subscribe();
                        }
                    }
                    this.msg.status = this.msg.status || this.submission.status;
                    this.landRightMsgService.create(this.msg).subscribe(res => {
                        res.reply = [];
                        res.user = this.user;
                        this.payment = {};
                        this.msg = {};
                        this.selectedMessage = res;
                        this.messages.unshift(res);
                        this.loading = false;
                        this.submitted = false;
                        this.changeStatus = false;
                        this.formLogService.create({
                            ...log, ...{
                                status: this.submission.status,
                                sub_status: 'Reply',
                                message_id: this.selectedMessage.id
                            }
                        }).subscribe();
                        // this.router.navigateByUrl(`/officer/land-right?status=${this.msg.status}`);
                    })
                }
            }),
        ).subscribe();
    }

}

