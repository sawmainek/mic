import { QuillModule } from 'ngx-quill';
import { LandRightModule as PageLandRightModule} from './../../pages/land-right/land-right.module';
import { LandRightReplyComponent } from './land-right-reply/land-right-reply.component';
import { GlobalModule } from './../../pages/global.module';
import { LandRightComponent } from './land-right.component';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomComponentModule } from './../../custom-component/custom-component.module';
import { LandRightRoutingModule } from './land-right-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { LandRightDetailComponent } from './land-right-detail/land-right-detail.component';
import { LandRightPaymentTransComponent } from './land-right-payment-trans/land-right-payment-trans.component';
import { LandRightHistoryComponent } from './land-right-history/land-right-history.component';

@NgModule({
    declarations: [
        LandRightComponent,
        LandRightReplyComponent,
        LandRightDetailComponent,
        LandRightPaymentTransComponent,
        LandRightHistoryComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        PageLandRightModule,
        GlobalModule,
        QuillModule.forRoot(),
        LandRightRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ], providers: [
        TranslateService,
    ],
})
export class LandRightModule { }
