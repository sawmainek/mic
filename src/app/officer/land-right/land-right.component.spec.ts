import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightComponent } from './land-right.component';

describe('LandRightComponent', () => {
  let component: LandRightComponent;
  let fixture: ComponentFixture<LandRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
