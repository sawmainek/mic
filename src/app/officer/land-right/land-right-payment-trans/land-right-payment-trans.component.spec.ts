import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandRightPaymentTransComponent } from './land-right-payment-trans.component';

describe('LandRightPaymentTransComponent', () => {
  let component: LandRightPaymentTransComponent;
  let fixture: ComponentFixture<LandRightPaymentTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandRightPaymentTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandRightPaymentTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
