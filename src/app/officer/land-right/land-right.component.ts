import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../../services/auth.service';
import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { getStatus } from '../officer';
declare var jQuery: any;
@Component({
    selector: 'app-land-right',
    templateUrl: './land-right.component.html',
    styleUrls: ['./land-right.component.scss']
})
export class LandRightComponent implements OnInit {


    user: any = {};
    datatable: any;
    loading: boolean = true;
    status: string = 'All';
    sector: any;
    sectors: any[] = [];
    dateRange: any;
    displayedColumns: any[] = [];
    sub_status: string = '';
    statuses: any[] = [
        { id: 'All', name: 'All' },
        { id: 'New', name: 'Ready for Review' },
        { id: 'Revision', name: 'Waiting for Reply' },
    ]
    submitted: string = '';
    sumitto: any[] = [
        { id: 'All', name: 'All' },
        { id: 'MIC', name: 'Myanmar Investment Commision' },
        { id: 'State/Region', name: 'States and Regions Investment Committees' },
    ]
    constructor(
        private authService: AuthService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.getSector();
        this.activatedRoute.queryParams.subscribe(params => {
            this.status = params['status'] || 'All';
            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                this.displayedColumns = [
                    {
                        field: 'application_no',
                        title: 'Application Number',
                        type: 'number',
                        width: 100,
                    },
                    {
                        field: 'updated_at',
                        title: 'Date of Submission',
                        sortable: true,
                        width: 100,
                        template: (row) => {
                            const datePipe = new DatePipe('en');
                            return datePipe.transform(row.updated_at, 'dd-MM-yyyy');
                        }
                    },
                    {
                        field: 'permit_endorsement_no',
                        title: 'Permit/Endorsement Number',
                        sortable: true,
                        width: 150,
                    },
                    {
                        field: 'investment_enterprise_name',
                        title: 'Name of Investment Enterprise',
                        sortable: true,
                        width: 200,
                    },
                    {
                        field: 'state',
                        title: 'MIC or State/Region',
                        type: 'number',
                        sortable: true,
                        width: 250,
                        template: (row) => {
                            return row?.form?.apply_to == 'States and Regions Investment Committees' ? row?.form?.state : 'Myanmar Investment Commision';
                        }
                    }
                ];
                if (this.status == 'All' || this.status == 'Pending' || this.status == 'Revision' || this.status == 'Acceptance' || this.status == 'Rejects') {
                    this.displayedColumns.push({
                        field: 'officer',
                        title: 'Replied By',
                        sortable: true,
                        width: 200,
                        template: (row) => {
                            return row?.officer?.name || '';
                        }
                    })
                }
                if (this.status == 'All') {
                    this.displayedColumns.push({
                        field: 'status',
                        title: 'Status',
                        sortable: true,
                        width: 170,
                        template: (row) => {
                            let status = getStatus(row.status)
                            return '<button class="btn ' + status + '">' + row.status + '</button>';
                        }
                    })
                }
                if (this.status == 'Revision') {
                    this.displayedColumns.push({
                        field: 'sub_status',
                        title: 'Status',
                        sortable: true,
                        width: 170,
                        template: (row) => {
                            if (row.status == 'Revision' && (row.sub_status == 'New' || row.sub_status == 'Reply')) {
                                return '<button class="btn btn-warning">Ready for Review</button>';
                            } else {
                                return '<button class="btn btn-info">Waiting for Reply</button>';
                            }
                        }
                    })
                }
                this.initDatatable();
            }, err => {
                console.log(err);
            });
        });
    }

    ngAfterViewInit(): void {
        this.loadDatePicker();
    }

    loadDatePicker() {
        const $this = this;
        jQuery('.form-daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-light-primary',
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            }
        });
        jQuery('.form-daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            $this.dateRange = {};
            $this.dateRange['start_date'] = picker.startDate.format('YYYY-MM-DD');
            $this.dateRange['end_date'] = picker.endDate.format('YYYY-MM-DD');
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
        jQuery('.form-daterangepicker').on('cancel.daterangepicker', function (ev, picker) {
            $this.dateRange = null;
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val('');
        });
    }

    getSector() {
        this.sectors = [
            { id: 1, name: 'Investment Division 1 - Agriculture, Wood based industry, Livestock and Fisheries, Food stuff industry' },
            { id: 2, name: 'Investment Division 2 - Manufacturing (except for food and wood processing industry)' },
            { id: 3, name: 'Investment Division 3 - Hotel & Tourism, Real Estate Development, Transport & Telecommunication, Construction, Establishment of Industrial Estate' },
            { id: 4, name: 'Investment Division 4 - Extractive Industries, Power and Other Services (Health, Education, Logistics, etc.)' }
        ];
    }

    onChangeSector() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    onChangeStatus() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    onChangeSubmission() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    buildSearch() {
        let search = this.status != 'All' ? `status:equal:${this.status}` : ``;
        const sector = this.sectors.filter(x => x.id == this.sector)[0] || null;
        if (sector) {
            search += `|sector_of_investment:equal:${sector.name}`;
        }
        if (this.sub_status && this.sub_status != 'All') {
            search += `|sub_status:equal:${this.sub_status}`;
        }
        if (this.submitted && this.submitted != 'All') {
            if(this.submitted == 'MIC') {
                search += `|submit_to_role:equal:mic`;
            } else {
                search += `|submit_to_role:ilike:branch`;
            }
        }
        if (this.dateRange) {
            search += `|created_at:bigger_equal:${this.dateRange.start_date}|created_at:smaller_equal:${this.dateRange.end_date}`
        }
        return search;
    }

    initDatatable() {
        this.user.personal_access_token
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };
        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/submissionlandright',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: this.buildSearch(),
                            query: '',
                            datatable: true
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            },
            search: {
                input: jQuery('#land-right-search'),
            }
        };
        if (this.datatable) { jQuery('.kt-datatable-land-right').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-land-right').KTDatatable(options);
        jQuery('.kt-datatable-land-right').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;
            jQuery('.onclick').on('click', function () {
                setTimeout(() => {
                    const index = jQuery(this).data('row');
                    const data = ctx.datatable.getDataSet();
                    ctx.goto(data[index]?.land_right_id);
                }, 300);
            });
        });
    }

    goto(id) {
        if(id) {
            this.router.navigate(['/officer/land-right/landright-choose-investment', id]);
        }
    }

}

