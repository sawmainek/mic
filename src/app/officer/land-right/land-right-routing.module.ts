import { LandRightDetailComponent } from './land-right-detail/land-right-detail.component';
import { LandRightReplyComponent } from './land-right-reply/land-right-reply.component';
import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: LandRightReplyComponent,
        children: [
            { path: '', component: LandRightDetailComponent }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class LandRightRoutingModule { }
