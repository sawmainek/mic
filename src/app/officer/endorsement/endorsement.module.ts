import { GlobalModule } from './../../pages/global.module';
import { QuillModule } from 'ngx-quill';
import { EndorsementModule as PageEndorsementModule} from './../../pages/endorsement/endorsement.module';
import { EndorsementReplyComponent } from './endorsement-reply/endorsement-reply.component';
import { EndorsementComponent } from './endorsement.component';
import { CustomComponentModule } from './../../custom-component/custom-component.module';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { EndorsementRoutingModule } from './endorsement-routing.module';
import { EndorsementDetailComponent } from './endorsement-detail/endorsement-detail.component';
import { EndorsementPaymentTransComponent } from './endorsement-payment-trans/endorsement-payment-trans.component';
import { EndorsementIssuedComponent } from './endorsement-issued/endorsement-issued.component';
import { CostBenefitAnalysisComponent } from './cost-benefit-analysis/cost-benefit-analysis.component';
import { EndorsementHistoryComponent } from './endorsement-history/endorsement-history.component';

@NgModule({
    declarations: [
        EndorsementComponent,
        EndorsementReplyComponent,
        EndorsementDetailComponent,
        EndorsementPaymentTransComponent,
        EndorsementIssuedComponent,
        CostBenefitAnalysisComponent,
        EndorsementHistoryComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        GlobalModule,
        QuillModule.forRoot(),
        PageEndorsementModule,
        EndorsementRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ], providers: [
        TranslateService,
    ],
})
export class EndorsementModule { }
