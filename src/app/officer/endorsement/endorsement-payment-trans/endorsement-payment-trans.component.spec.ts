import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementPaymentTransComponent } from './endorsement-payment-trans.component';

describe('EndorsementPaymentTransComponent', () => {
  let component: EndorsementPaymentTransComponent;
  let fixture: ComponentFixture<EndorsementPaymentTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementPaymentTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementPaymentTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
