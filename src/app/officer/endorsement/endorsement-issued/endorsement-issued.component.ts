import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { AppState, currentUser } from 'src/app/core';
import { BaseForm } from 'src/app/custom-component/dynamic-forms/base/base-form';
import { BaseFormGroup } from 'src/app/custom-component/dynamic-forms/base/form-group';
import { FormSectionTitle } from 'src/app/custom-component/dynamic-forms/base/form-section-title';
import { FormControlService } from 'src/app/custom-component/dynamic-forms/form-control.service';
import { DatePipe, Location } from '@angular/common';
import { FormInput } from 'src/app/custom-component/dynamic-forms/base/form-input';
import { FormDate } from 'src/app/custom-component/dynamic-forms/base/form.date';
import { FormSelect } from 'src/app/custom-component/dynamic-forms/base/form-select';
import { LocalService } from 'src/services/local.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { EndorsementSubmissionService } from 'src/services/endorsement-submission.service';
import { FormTitle } from 'src/app/custom-component/dynamic-forms/base/form-title';
import { BaseFormArray } from 'src/app/custom-component/dynamic-forms/base/form-array';

@Component({
    selector: 'app-endorsement-issued',
    templateUrl: './endorsement-issued.component.html',
    styleUrls: ['./endorsement-issued.component.scss']
})
export class EndorsementIssuedComponent implements OnInit {
    @Input() id: any;
    mm: any;

    formGroup: FormGroup;
    endoModel: any;
    user: any = {};
    model: any = {};
    data: any;
    form: any;

    loading = false;
    loaded: boolean = false;
    spinner = false;

    investment_location: BaseForm<any>[] = [
        new FormInput({
            key: 'state',
            label: 'State / Region',
            style: {
                'width': '35%'
            },
            commentable: false
        }),
        new FormInput({
            key: 'district',
            label: 'District',
            style: {
                'width': '35%'
            },
        }),
        new FormInput({
            key: 'township',
            label: 'Township',
            style: {
                'width': '35%'
            },
        }),
    ];

    investor: BaseForm<any>[] = [
        new FormInput({
            key: 'name',
            label: 'Name',
            style: {
                'width': '35%'
            },
        }),
        new FormInput({
            key: 'address',
            label: 'Address',
            style: {
                'width': '35%'
            },
        }),
        new FormInput({
            key: 'country',
            label: 'Country of Incorporation or citizenship',
            style: {
                'width': '35%'
            },
        }),
    ];

    forms: BaseForm<string>[] = [
        new FormInput({
            key: 'permit_endo_no',
            label: '1. Permit/endorsement number',
            editable: true,
            commentable: false
        }),
        new FormInput({
            key: 'investment_name',
            label: '2. Name of Enterprise',
            commentable: false
        }),
        new FormInput({
            key: 'form_investment',
            label: '3. Form of Investment',
            commentable: false
        }),
        new FormInput({
            key: 'company_type',
            label: '4. Type of Company',
            commentable: false,
            options$: this.localService.getCompany()
        }),
        new FormInput({
            key: 'isic_class_ids',
            commentable: false,
            label: '5. Specific sector of investment'
        }),
        new FormTitle({
            label: '6. Location(s) of the investment project'
        }),
        new BaseFormArray({
            key: 'investment_location',
            formArray: this.investment_location,
            commentable: false,
            useTable: true
        }),
        new FormInput({
            key: 'foreign_capital_usd',
            commentable: false,
            label: '7. Amount of Foreign Capital',
        }),
        new FormInput({
            key: 'brought_duration',
            commentable: false,
            label: '8. Period during which Foreign Cash and Equivalents will be brought in USD',
        }),
        new FormTitle({
            label: '9. (Expected) investment value',
            commentable: false,
        }),
        new FormInput({
            key: 'amount_kyat',
            label: 'Total Amount in Myanmar Kyat',
            commentable: false,
            columns: 'col-md-6',
        }),
        new FormInput({
            key: 'amount_usd',
            label: 'Equivalent Amount in USD (Million)',
            commentable: false,
            columns: 'col-md-6',
        }),
        new FormInput({
            key: 'construction_start_date',
            label: '10. (Expected) Construction or preparatory period',
            commentable: false,
        }),
        new FormInput({
            key: 'investment_start_date',
            label: '11.  (Expected) Investment Period',
            commentable: false,
            required: true
        }),
        new FormTitle({
            label: '12. Name(s) of investor(s)'
        }),
        new BaseFormArray({
            key: 'investor',
            formArray: this.investor,
            commentable: false,
            useTable: true
        }),
        ////
        new FormDate({
            key: 'issued_date',
            label: '13.Issued Date',
            commentable: false,
            required: true,
            editable: true
        }),
        new FormInput({
            key: 'project_duration_no',
            type: 'number',
            required: true,
            label: '14. Project Duration',
            columns: 'col-md-6 d-flex',
            commentable: false,
            editable: true
        }),
        new FormSelect({
            key: 'project_duration_label',
            required: true,
            options$: this.localService.getDate(),
            columns: 'col-md-3 d-flex',
            value: 'Months',
            commentable: false,
            editable: true
        }),
        new FormDate({
            key: 'commercial_date',
            label: '15. Date of Commercial Operation Period',
            required: true,
            commentable: false,
            editable: true
        }),
    ];

    constructor(
        private store: Store<AppState>,
        public formCtlService: FormControlService,
        public location: Location,
        private localService: LocalService,
        private toast: ToastrService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: EndorsementSubmissionService
    ) {
        this.loading = true;
        this.formGroup = this.formCtlService.toFormGroup(this.forms);
    }

    ngOnInit(): void {
        this.getCurrentUser();
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });

        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id') || null;

            this.service.get({ search: `endorsement_id:equal:${this.id}` }).subscribe(submissions => {
                this.endoModel = submissions[0] || null;

                this.data = this.endoModel?.form?.language == 'English' ? this.endoModel?.form?.data : this.endoModel?.form?.data_mm;

                this.endoModel.permit_endo_no = this.endoModel?.permit_endo_no || this.data?.application_no || '';
                this.endoModel.investment_name = this.data?.company_info?.investment_name || '';
                this.endoModel.form_investment = this.data?.invest_form?.form_investment;
                this.endoModel.company_type = this.data?.company_info?.company_type;

                this.endoModel.isic_class_ids = '';
                if (Array.isArray(this.data?.company_info?.specific_business_sector?.isic_class_ids)) {
                    this.data?.company_info?.specific_business_sector?.isic_class_ids.forEach(x => {
                        this.endoModel.isic_class_ids += x + ', ';
                    });
                    if (this.endoModel.isic_class_ids.length > 2)
                        this.endoModel.isic_class_ids = this.endoModel.isic_class_ids.substring(0, this.endoModel.isic_class_ids.length - 2);
                }
                else {
                    this.endoModel.isic_class_ids = this.data?.company_info?.specific_business_sector?.isic_class_ids;
                }

                this.endoModel.investment_location = this.data?.location;
                this.endoModel.foreign_capital_usd = this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd ? this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd + ' Equivalent US$' : '';
                this.endoModel.brought_duration = this.data?.expected_investmentdt?.start_date && this.data?.expected_investmentdt?.end_date ? this.data?.expected_investmentdt?.start_date + '  -  ' + this.data?.expected_investmentdt?.end_date : '';

                this.endoModel.amount_kyat = Number(this.data?.expected_investmentdt?.cash_equivalent[0]?.myan_origin || 0) +
                    Number(this.data?.expected_investmentdt?.cash_equivalent[1]?.myan_origin || 0) +
                    Number(this.data?.expected_investmentdt?.cash_equivalent[2]?.myan_origin || 0);

                this.endoModel.amount_usd = this.data?.expected_investmentdt?.total_equivalent_usd;
                this.endoModel.construction_start_date = this.data?.timeline?.construction_start_date;
                this.endoModel.investment_start_date = this.data?.timeline?.investment_start_date;

                this.endoModel.investor = [];
                this.data?.investor_list?.map((x, index) => {
                    if (x.company?.company_name) {
                        this.endoModel.investor.push({ 'name': x?.company?.company_name, 'address': x?.company?.address, 'country': x?.company?.incorporate_country });
                    }
                    else if (x.government?.name) {
                        this.endoModel.investor.push({ 'name': x?.government?.name, 'address': x?.government?.address, 'country': 'Myanmar' });
                    }
                    else {
                        this.endoModel.investor.push({ 'name': x?.individual?.full_name, 'address': x?.individual?.address, 'country': x?.individual?.citizenship });
                    }
                })

                var date = new DatePipe('en');
                this.endoModel.issued_date = date.transform(this.endoModel.issued_date, 'dd-MM-yyyy');
                this.endoModel.commercial_date = date.transform(this.endoModel.commercial_date, 'dd-MM-yyyy');
                this.formGroup = this.formCtlService.toFormGroup(this.forms, this.endoModel, {});
            });
        }).unsubscribe();
    }

    getCurrentUser() {
        this.store.pipe(select(currentUser))
            .subscribe(user => {
                if (user) {
                    this.user = user;
                }
            });
    }

    onSubmit() {
        if (this.formGroup.invalid) {
            this.toast.error('Please complete all required field(s).');
            return;
        }

        this.spinner = true;
        this.service.update(this.endoModel.id, { ...this.endoModel, ...this.formGroup.value }, { secure: true })
            .subscribe(x => {
                this.spinner = false;
                this.toast.success('Saved successfully.');
            }, error => {
                console.log(error);
            });
    }

}
