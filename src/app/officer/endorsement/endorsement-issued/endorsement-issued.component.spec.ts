import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementIssuedComponent } from './endorsement-issued.component';

describe('EndorsementIssuedComponent', () => {
  let component: EndorsementIssuedComponent;
  let fixture: ComponentFixture<EndorsementIssuedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementIssuedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementIssuedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
