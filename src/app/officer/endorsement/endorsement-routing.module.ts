import { EndorsementDetailComponent } from './endorsement-detail/endorsement-detail.component';
import { EndorsementReplyComponent } from './endorsement-reply/endorsement-reply.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: EndorsementReplyComponent,
        children: [
            { path: '', component: EndorsementDetailComponent }
        ]
    },
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class EndorsementRoutingModule { }
