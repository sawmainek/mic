import { saveAs } from 'file-saver';
import { Packer } from 'docx';
import { EndorsementWordExport } from './endorsement-word.export';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';

export class SalientProintExport {
    docName: string = 'SALIENT POINTS';
    fileName: string = 'salient-point'
    permitForm: any[] = [];
    data: any;
    language: any = 'en';
    number: any = ["၁", "၂", "၃", "၄", "၅", "၆", "၇", "၈", "၉", "၁၀", "၁၁", "၁၂", "၁၃", "၁၄", "၁၅", "၁၆", "၁၇", "၁၈", "၁၉", "၂၀", "၂၁", "၂၂", "၂၃", "၂၄", "၂၅", "၂၆", "၂၇", "၂၈", "၂၉", "၃၀"]

    salary: any = {};
    total_year: any = [];

    constructor(permit: any, lan: any) {
        this.language = lan;
        if (lan == 'en' && permit.form.language == 'English') {
            this.data = permit?.form?.data || permit?.form?.data_mm;
        }
        else {
            this.data = permit?.form?.data_mm || {};
        }
        console.log(this.data)

        this.fileName = `${this.fileName}-${permit.application_no}.docx`;

        this.salary.mm_salary_max = 0;
        this.salary.mm_salary_min = 0;
        this.salary.fr_salary_max = 0;
        this.salary.fr_salary_min = 0;

        for (var i = 0; i < 5; i++) {
            this.total_year.push({
                mm_citizens_qty_total: 0,
                mm_citizens_salary_rate_total: 0,
                fr_citizens_qty_total: 0,
                fr_citizens_salary_rate_total: 0,
            })
        }

        var is_assign = false;

        this.data.employment_plan.emp_year = this.data?.employment_plan?.emp_year || [];
        this.data.employment_plan.emp_year.forEach(x => {
            for (var i = 0; i < 5; i++) {
                this.total_year[i].mm_citizens_qty_total += Number(x[`myanmar_citizens_qty_${i + 1}`] || 0);
                // this.total_year[i].mm_citizens_salary_rate_total += Number(x[`myanmar_citizens_salary_rate_${i + 1}`] || 0);
                this.total_year[i].fr_citizens_qty_total += Number(x[`foreign_nationals_qty_${i + 1}`] || 0);
                // this.total_year[i].fr_citizens_salary_rate_total += Number(x[`foreign_nationals_salary_rate_${i + 1}`] || 0);

                if (is_assign) {
                    if (this.salary.mm_salary_max < (x[`myanmar_citizens_salary_rate_${i + 1}`] || 0)) {
                        this.salary.mm_salary_max = (x[`myanmar_citizens_salary_rate_${i + 1}`]);
                    }
                    if (this.salary.mm_salary_min > (x[`myanmar_citizens_salary_rate_${i + 1}`] || 0)) {
                        this.salary.mm_salary_min = (x[`myanmar_citizens_salary_rate_${i + 1}`]);
                    }
                    if (this.salary.fr_salary_max < (x[`foreign_nationals_salary_rate_${i + 1}`] || 0)) {
                        this.salary.fr_salary_max = (x[`foreign_nationals_salary_rate_${i + 1}`]);
                    }
                    if (this.salary.fr_salary_min > (x[`foreign_nationals_salary_rate_${i + 1}`] || 0)) {
                        this.salary.fr_salary_min = (x[`foreign_nationals_salary_rate_${i + 1}`]);
                    }
                }
                else {
                    this.salary.mm_salary_max = x[`myanmar_citizens_salary_rate_1`] || 0;
                    this.salary.mm_salary_min = x[`myanmar_citizens_salary_rate_1`] || 0;
                    this.salary.fr_salary_max = x[`foreign_nationals_salary_rate_1`] || 0;
                    this.salary.fr_salary_min = x[`foreign_nationals_salary_rate_1`] || 0;

                    is_assign = true;
                }
            }
        });

        var totalMyanmarCash = Number(this.data?.expected_investmentdt?.cash_equivalent[0]?.myan_origin || 0) +
            Number(this.data?.expected_investmentdt?.cash_equivalent[1]?.myan_origin || 0) +
            Number(this.data?.expected_investmentdt?.cash_equivalent[2]?.myan_origin || 0);

        if (this.language == 'en') {
            this.docName = 'SALIENT POINTS';
            this.permitForm = [
                {
                    section: '1. Basic Information',
                    form: [
                        {
                            label: 'Form of Investment:',
                            value: this.data?.invest_form?.form_investment || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Name of Company Incorporated in Myanmar:',
                            value: this.data?.company_info?.investment_name || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Type of Company:',
                            value: this.data?.company_info?.company_type,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Specific sector of the proposed investment:',
                            value: `${this.data?.company_info?.general_business_sector?.isic_class_ids}`,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Shareholders', type: 'Table', data: [
                                ['No', 'Name of shareholder', 'Position in investment', 'N.R.C No. / Passport No. / Company Registration No.', 'Citizenship or country of incorporation / registration / origin', 'Address', 'Share percentage', 'Issued share capital (equivalent in MMK)', 'Issued share capital (equivalent in USD)'],
                                ...this.data?.share_holder?.sharholder_list.map((x, index) => {
                                    return [index + 1, x?.name, x?.position, x?.nrc, x?.country, x?.address, x?.share_percentage, x?.share_capital_MMK, x?.share_capital_USD]
                                })
                            ]
                        }
                    ]
                },
                {
                    section: '2. Land and Buildings',
                    form: [
                        ...this.data?.owner.map((owner, index) => {
                            const arr: any[] = [];
                            arr.push({ label: `Location ${index + 1}`, type: 'Title', option: { bold: true } }); // 0
                            arr.push({
                                label: 'Location:', type: 'Table', data: [
                                    ['Location No.', 'State/Region', 'District', 'Township'],
                                    [1, this.data.location[index]['state'] || '', this.data.location[index]['district'] || '', this.data.location[index]['township'] || '']
                                ]
                            })
                            arr.push({
                                label: 'Land Owner:',
                                value: owner?.owner_type == 'Individual' ? owner?.individual_name :
                                    owner?.owner_type == 'Company' ? owner?.company_name :
                                        owner?.owner_type == 'Government' ? owner?.government_name : '',
                                type: 'Text', option: {}
                            })
                            arr.push({
                                label: 'Area of land used/leased:',
                                value: this.data?.land[0]?.area + '/' + this.data?.land[0]?.unit,
                                type: 'Text', option: {}
                            })
                            arr.push({
                                label: 'Area of buildings used/leased:', type: 'Table', data: [
                                    ['No.', 'Area', 'Unit of measurement'],
                                    ...this.data?.building[index]?.building?.map((x, j) => {
                                        return [(j + 1), x.area || '', x.unit || '']
                                    })
                                ]
                            })
                            arr.push({
                                label: 'Proposed lease period:',
                                value: (this.data?.agreement[0]?.lease_from && this.data?.agreement[0]?.lease_to) ? this.data?.agreement[0]?.lease_from + ' - ' + this.data?.agreement[0]?.lease_to : '',
                                type: 'Text', option: {}
                            })
                            arr.push({
                                label: 'Annual rental fees of land and/or buildings:',
                                value: Math.round(this.data.agreement[index]['rental_fees'] || 0),
                                type: 'Text', option: {}
                            })
                            return arr;
                        }).reduce((prev, curr) => prev.concat(curr), [])
                    ]
                },
                {
                    section: '3. Expected investment period:',
                    form: [
                        {
                            label: 'Construction or preparatory period:',
                            value: `${this.data?.timeline['construction_date_no'] + this.data?.timeline['construction_date_label'] || ''}`,
                            type: 'Text'
                        },
                        {
                            label: 'Commercial operation period:',
                            value: `${this.data?.timeline['commercial_date_no'] + this.data?.timeline['commercial_date_label'] || ''}`,
                            type: 'Text'
                        },
                        {
                            label: 'Investment period:',
                            value: `${this.data?.timeline['investment_start_date'] + ' Years' || ''}`,
                            type: 'Text'
                        }
                    ]
                },
                {
                    section: '4. Expected investment value',
                    form: [
                        {
                            label: 'Expected Investment Value (Kyat):',
                            value: Math.round(this.data?.expected_investment?.spent_amount_kyat || 0),
                            type: 'Text'
                        },
                        {
                            label: 'Expected Investment Value (USD):',
                            value: Math.round(this.data?.expected_investment?.spent_amount_usd || 0),
                            type: 'Text'
                        },
                        {
                            label: 'Expected Investment Value - Detail (Kyat):',
                            value: Math.round(totalMyanmarCash || 0),
                            type: 'Text'
                        },
                        {
                            label: 'Total amount of Foreign cash to be brough in (Equivalent in USD):',
                            value: Math.round(this.data?.expected_investmentdt?.total_equivalent_usd || 0),
                            type: 'Text'
                        },
                        {
                            label: 'Period during which Foreign Cash and Equivalents will be brought in USD',
                            value: (this.data?.expected_investmentdt?.period_start_date || '') + '  -  ' + (this.data?.expected_investmentdt?.period_end_date || ''),
                            type: 'Text', option: {}
                        },
                    ]
                },
                {
                    section: '5. Summary of production and sales strategy',
                    form: [
                        {
                            value: this.data.company_info?.invest_description
                        },
                    ]
                },
                {
                    section: '6. Employment',
                    form: [
                        {
                            label: 'Maximum number of employment created over lifetime of investment business', type: 'Table', data: [
                                ['Employment Classification', 'Myanmar citizens', 'Foreign nationals'],
                                ...this.data?.employment_plan?.emp_max?.map((x, j) => {
                                    return [x?.classification, x?.myanmar_citizens_qty || '', x?.foreign_nationals_qty || '']
                                })
                            ]
                        },
                        { label: 'Expected number of employees over the first five years of investment:', value: '', type: 'Text', option: { bold: true } },
                        // { label: 'Name of Company Incorporated in Myanmar:', value: '', type: 'Text', option: {} },
                        {
                            label: '(a) Myanmar citizens – Qty (pax)', type: 'Table', data: [
                                ['Employment Classification', 'Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5'],
                                ...this.data?.employment_plan?.emp_year?.map((x, j) => {
                                    return [x?.classification, x?.myanmar_citizens_qty_1 || '', x?.myanmar_citizens_qty_2 || '', x?.myanmar_citizens_qty_3 || '', x?.myanmar_citizens_qty_4 || '', x?.myanmar_citizens_qty_5 || '']
                                })
                            ]
                        },
                        {
                            label: '(b) Foreign citizens – Qty (pax)', type: 'Table', data: [
                                ['Employment Classification', 'Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5'],
                                ...this.data?.employment_plan?.emp_year?.map((x, j) => {
                                    return [x?.classification, x?.foreign_nationals_qty_1 || '', x?.foreign_nationals_qty_2 || '', x?.foreign_nationals_qty_3 || '', x?.foreign_nationals_qty_4 || '', x?.foreign_nationals_qty_5 || '']
                                })
                            ]
                        },
                        { label: 'Highest salary in first ten years of investment:', type: 'Title', option: { bold: true } },
                        {
                            label: '(a) Myanmar citizens',
                            value: Math.round(this.salary.mm_salary_max || 0) + ' Kyat',
                            type: 'Text',
                            option: {}
                        },
                        {
                            label: '(b) Foreign',
                            value: Math.round(this.salary.fr_salary_max || 0) + ' Kyat',
                            type: 'Text'
                        },
                        { label: 'Lowest salary in first five years of investment:', type: 'Title', option: { bold: true } },
                        {
                            label: '(a) Myanmar citizens',
                            value: Math.round(this.salary.mm_salary_min || 0) + ' Kyat',
                            type: 'Text',
                        },
                        {
                            label: '(b) Foreign',
                            value: Math.round(this.salary.fr_salary_min || 0) + ' Kyat',
                            type: 'Text'
                        },
                    ]
                },
                {
                    section: 'Commentary by case officer:',
                    form: [
                        {},
                        {},
                        {},
                        {}
                    ]
                },
            ]
        }
        else {
            this.docName = 'SALIENT POINTS';
            this.permitForm = [
                {
                    section: '၁။ ယေဘုယျ အချက်အလက်မျာ',
                    form: [
                        {
                            label: 'ရင်းနှီးမြှုပ်နှံမှု ပုံစံ',
                            value: this.data?.invest_form?.form_investment || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: 'မြန်မာနိုင်ငံတွင် ဖွဲ့စည်းတည်ထောင်သည့် ကုမ္ပဏီအမည်',
                            value: this.data?.company_info?.investment_name || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: 'ကုမ္ပဏီ အမျိုးအစား',
                            value: this.data?.company_info?.company_type,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'အဆိုပြု ရင်းနှီးမြှုပ်နှံမှု ၏ အသေးစိတ် လုပ်ငန်းကဏ္ဍ',
                            value: `${this.data?.company_info?.general_business_sector?.isic_class_ids}`,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Shareholders', type: 'Table', data: [
                                ['စဉ်', 'အစုရှယ်ယာရှင် အမည်', 'ရင်းနှီးမြှုပ်နှံမှုအတွက်တာဝန်ယူထားသောရာထူး', 'နိုင်ငံသားစီစစ်ရေးကတ်အမှတ်/နိုင်ငံကူးလက်မှတ်အမှတ် (နိုင်ငံခြားသား)/ကုမ္ပဏီမှတ်ပုံတင်လက်မှတ်အမှတ်', 'နိုင်ငံသားသို့မဟုတ် စီးပွားရေးဖွဲ့စည်းတည်ထောင်ထားသော/မှတ်ပုံတင်ထားသော နိုင်ငံ', 'နေရပ်လိပ်စာ', 'အစုရှယ်ယာပိုင်ဆိုင်မှု %', 'ရှယ်ယာပိုင်ဆိုင်မှုပမာဏ(ညီမျှသောမြန်မာကျပ်ငွေဖြင့်)', 'ရှယ်ယာပိုင်ဆိုင်မှုပမာဏ(ညီမျှသောအမေရိကန်ဒေါ်လာဖြင့်)'],
                                ...this.data?.share_holder?.sharholder_list.map((x, index) => {
                                    return [index + 1, x?.name, x?.position, x?.nrc, x?.country, x?.address, x?.share_percentage, x?.share_capital_MMK, x?.share_capital_USD]
                                })
                            ]
                        }
                    ]
                },
                {
                    section: '၂။ မြေနှင့် အဆောက်အဦးများ',
                    form: [
                        ...this.data?.owner.map((owner, index) => {
                            const arr: any[] = [];
                            arr.push({ label: `တည်နေရာ ${this.number[index]}`, type: 'Title', option: { bold: true } }); // 0
                            arr.push({
                                label: 'တည်နေရာ', type: 'Table', data: [
                                    ['တည်နေရာ', 'တိုင်းဒေသကြီး/ပြည်နယ်', 'ခရိုင်', 'မြို့နယ်'],
                                    [1, this.data.location[index]['state'] || '', this.data.location[index]['district'] || '', this.data.location[index]['township'] || '']
                                ]
                            })
                            arr.push({
                                label: 'မြေပိုင်ရှင်',
                                value: owner?.owner_type == 'Individual' ? owner?.individual_name :
                                    owner?.owner_type == 'Company' ? owner?.company_name :
                                        owner?.owner_type == 'Government' ? owner?.government_name : '',
                                type: 'Text', option: {}
                            })
                            arr.push({
                                label: 'ငှားရမ်း/ အသုံးပြု မည့် မြေ ဧရိယာ',
                                value: this.data?.land[0]?.area + '/' + this.data?.land[0]?.unit,
                                type: 'Text', option: {}
                            })
                            arr.push({
                                label: 'ငှားရမ်း/ အသုံးပြု မည့် အဆောက်အဦး ဧရိယာ', type: 'Table', data: [
                                    ['စဉ်', 'ဧရိယာ', 'တိုင်းတာသည့် ယူနစ်'],
                                    ...this.data?.building[index]?.building?.map((x, j) => {
                                        return [(j + 1), x.area || '', x.unit || '']
                                    })
                                ]
                            })
                            arr.push({
                                label: 'အဆိုပြု ငှားရမ်းမည့် ကာလ',
                                value: (this.data?.agreement[0]?.lease_from && this.data?.agreement[0]?.lease_to) ? this.data?.agreement[0]?.lease_from + ' - ' + this.data?.agreement[0]?.lease_to : '',
                                type: 'Text', option: {}
                            })
                            arr.push({
                                label: 'မြေ နှင့် / သို့မဟုတ် အဆောက်အဦးအား နှစ်စဉ် ငှားရမ်းခနှုန်း',
                                value: Math.round(this.data.agreement[index]['rental_fees'] || ''),
                                type: 'Text', option: {}
                            })
                            return arr;
                        }).reduce((prev, curr) => prev.concat(curr), [])
                    ]
                },
                {
                    section: '၃။  ရင်းနှီးမြှုပ်နှံမည့် ကာလ',
                    form: [
                        {
                            label: 'တည်ဆောက်ခြင်း သို့မဟုတ် ပြင်ဆင်ခြင်း ကာလ',
                            value: `${this.data?.timeline['construction_date_no'] + (this.data?.timeline['construction_date_label'] == 'Years' ? ' နှစ်' : ' လ') || ''}`,
                            type: 'Text'
                        },
                        {
                            label: 'စီးပွားဖြစ် စတင်မည့် ကာလ',
                            value: `${this.data?.timeline['commercial_date_no'] + (this.data?.timeline['commercial_date_label'] == 'Years' ? ' နှစ်' : ' လ') || ''}`,
                            type: 'Text'
                        },
                        {
                            label: 'ရင်းနှီးမြှုပ်နှံမည့် ကာလ',
                            value: `${this.data?.timeline['investment_start_date'] + ' နှစ်' || ''}`,
                            type: 'Text'
                        }
                    ]
                },
                {
                    section: '၄။ ရင်းနှီးမြှုပ်နှံမည့် တန်ဖိုး ပမာဏ',
                    form: [
                        {
                            label: 'ခန့်မှန်းရင်းနှီးမြှုပ်နှံမှုတန်ဖိုး (ကျပ်):',
                            value: Math.round(this.data?.expected_investment?.spent_amount_kyat || 0),
                            type: 'Text'
                        },
                        {
                            label: 'ခန့်မှန်းရင်းနှီးမြှုပ်နှံမှုတန်ဖိုး (အမေရိကန်ဒေါ်လာ):',
                            value: Math.round(this.data?.expected_investment?.spent_amount_usd || 0),
                            type: 'Text'
                        },
                        {
                            label: 'ခန့်မှန်း ရင်းနှီးမြှုပ်နှံမှု တန်ဖိုး - အသေးစိတ် (ကျပ်):',
                            value: Math.round(totalMyanmarCash || 0),
                            type: 'Text'
                        },
                        {
                            label: 'ယူဆောင်လာမည့် စုစုပေါင်း နိုင်ငံခြားငွေကြေး ပမာဏ (အမေရိကန်ဒေါ်လာဖြင့် ညီမျှသော ပမာဏ)',
                            value: Math.round(this.data?.expected_investmentdt?.total_equivalent_usd || 0),
                            type: 'Text'
                        },
                        {
                            label: 'နိုင်ငံခြား ငွေကြေး ယူဆောင်လာမည့် ကာလ',
                            value: (this.data?.expected_investmentdt?.period_start_date || '') + '  -  ' + (this.data?.expected_investmentdt?.period_end_date || ''),
                            type: 'Text', option: {}
                        },
                    ]
                },
                {
                    section: '၅။ ထုတ်လုပ်ခြင်းနှင့် ရောင်းချခြင်း နည်းစနစ် အနှစ်ချုပ်',
                    form: [
                        {
                            value: this.data.company_info?.invest_description
                        },
                    ]
                },
                {
                    section: '၆။ ဝန်ထမ်းခန့်ထားမှု',
                    form: [
                        {
                            label: 'ရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်းကာလအတွင်း အများဆုံးခန့်ထားမည့် ဝန်ထမ်းအရေအတွက်', type: 'Table', data: [
                                ['ဝန်ထမ်းအမျိုးအစား', 'မြန်မာနိုင်ငံသား', 'နိုင်ငံခြားသား'],
                                ...(this.data?.employment_plan?.emp_max || []).map((x, j) => {
                                    return [x?.classification, x?.myanmar_citizens_qty || '', x?.foreign_nationals_qty || '']
                                })
                            ]
                        },
                        { label: 'ရင်းနှီးမြှုပ်နှံမှု ပထမ (၅)နှစ်တွင် ခန့်ထားမည့် ခန့်မှန်း ဝန်ထမ်း အရေအတွက်', value: '', type: 'Text', option: { bold: true } },
                        // { label: 'Name of Company Incorporated in Myanmar:', value: '', type: 'Text', option: {} },
                        {
                            label: '(က) မြန်မာ နိုင်ငံသား (လူဦးရေ)', type: 'Table', data: [
                                ['ဝန်ထမ်းအမျိုးအစား', 'Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5'],
                                ...this.data?.employment_plan?.emp_year?.map((x, j) => {
                                    return [x?.classification, x?.myanmar_citizens_qty_1 || '', x?.myanmar_citizens_qty_2 || '', x?.myanmar_citizens_qty_3 || '', x?.myanmar_citizens_qty_4 || '', x?.myanmar_citizens_qty_5 || '']
                                })
                            ]
                        },
                        {
                            label: '(ခ) နိုင်ငံခြားသား (လူဦးရေ)', type: 'Table', data: [
                                ['ဝန်ထမ်းအမျိုးအစား', 'Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5'],
                                ...this.data?.employment_plan?.emp_year?.map((x, j) => {
                                    return [x?.classification, x?.foreign_nationals_qty_1 || '', x?.foreign_nationals_qty_2 || '', x?.foreign_nationals_qty_3 || '', x?.foreign_nationals_qty_4 || '', x?.foreign_nationals_qty_5 || '']
                                })
                            ]
                        },
                        { label: 'ရင်းနှီးမြှုပ်နှံမှု (၅) နှစ် အတွင်း သုံးစွဲမည့် အမြင့်ဆုံး လစာ', type: 'Title', option: { bold: true } },
                        {
                            label: '(က) မြန်မာ နိုင်ငံသား',
                            value: Math.round(this.salary.mm_salary_max || 0) + ' ကျပ်',
                            type: 'Text',
                            option: {}
                        },
                        {
                            label: '(ခ) နိုင်ငံခြားသား',
                            value: Math.round(this.salary.fr_salary_max || 0) + ' ကျပ်',
                            type: 'Text'
                        },
                        { label: 'ရင်းနှီးမြှုပ်နှံမှု (၅) နှစ် အတွင်း သုံးစွဲမည့် အနိမ့်ဆုံး လစာ', type: 'Title', option: { bold: true } },
                        {
                            label: '(က) မြန်မာ နိုင်ငံသား',
                            value: Math.round(this.salary.mm_salary_min || 0) + ' ကျပ်',
                            type: 'Text',
                        },
                        {
                            label: '(ခ) နိုင်ငံခြားသား',
                            value: Math.round(this.salary.fr_salary_min || 0) + ' ကျပ်',
                            type: 'Text'
                        },
                    ]
                },
                {
                    section: 'သုံးသပ်တင်ပြချက်',
                    form: [
                        {},
                        {},
                        {},
                        {}
                    ]
                },
            ]
        }
    }

    public download(): Observable<any> {
        return Observable.create((observer) => {
            const documentCreator = new EndorsementWordExport();
            const doc = documentCreator.create(this.permitForm, this.docName);
            Packer.toBlob(doc).then(blob => {
                try {
                    saveAs(blob, this.fileName);
                    observer.next("Document created successfully");
                    observer.complete();
                } catch (err) {
                    observer.error(err);
                }
            });
        });

    }
}

export class SummaryExport {
    docName: string = 'SUMMARY OF PROPOSED INVESTMENT';
    fileName: string = 'summary'
    permitForm: any[] = [];
    data: any;
    language: any = 'en';

    total_year: any = [];

    constructor(permit: any, lan: any) {
        this.language = lan;
        if (lan == 'en' && permit.form.language == 'English') {
            this.data = permit?.form?.data || permit?.form?.data_mm;
        }
        else {
            this.data = permit?.form?.data_mm || {};
        }
        this.fileName = `${this.fileName}-${permit.application_no}.docx`;

        var totalMyanmarCash = Number(this.data?.expected_investmentdt?.cash_equivalent[0]?.myan_origin || 0) +
            Number(this.data?.expected_investmentdt?.cash_equivalent[1]?.myan_origin || 0) +
            Number(this.data?.expected_investmentdt?.cash_equivalent[2]?.myan_origin || 0);

        this.permitForm = [
            {
                section: '1. Please describe all natural and legal persons who have a significant direct or indirect interest in the investment.',
                form: [
                    {
                        label: '(a) Natural and legal persons entitled to possess more than 10% of the profit distribution:', type: 'Table', data: [
                            ['Name', 'Type', 'Citizenship or country of incorporation/registration/origin'],
                            ...this.data.significant_list?.signi_person_list?.map(x => {
                                return [x.name, x.investor_type_id, x.country];
                            })
                        ]
                    },
                    {
                        label: '(b) Subsidiaries directly involved in carrying out the proposed investment:',
                        value: this.data.significant_list?.sub_company_list?.map(x => {
                            return [x.name];
                        }).reduce((prev, curr) => prev.concat(curr), []).join(', '),
                        type: 'Text'
                    },
                ]
            },
            {
                section: 'The principal location or locations of the investment:',
                form: [
                    {
                        label: 'Locations:', type: 'Table', data: [
                            ['Location No.', 'State/Region', 'District', 'Township'],
                            ...this.data.location.map((x, index) => {
                                return [index + 1, x.state, x.district, x.township];
                            })
                        ]
                    }
                ]
            },
            {
                section: '3. Description of the sector in which the investment is to be made and the activities and operations to be conducted',
                form: [
                    {
                        label: 'Sector of investment:',
                        value: this.data.company_info.general_business_sector.isic_section_id || '',
                        type: 'Text'
                    },
                    {
                        label: 'Activities and operation to be conducted:',
                        value: this.data.company_info.invest_description || '',
                        type: 'Text'
                    }
                ]
            },
            {
                section: '4. The proposed amount of the investment (in Kyat and US$',
                form: [
                    {
                        label: 'Expected Investment Value (Kyat):',
                        value: Math.round(this.data?.expected_investment?.spent_amount_kyat || 0),
                        type: 'Text'
                    },
                    {
                        label: 'Expected Investment Value (USD):',
                        value: Math.round(this.data?.expected_investment?.spent_amount_usd || 0),
                        type: 'Text'
                    },
                    {
                        label: 'Expected Investment Value - Detail (Kyat):',
                        value: Math.round(totalMyanmarCash || 0),
                        type: 'Text'
                    },
                    {
                        label: 'Total amount of Foreign cash to be brough in (Equivalent in USD):',
                        value: Math.round(this.data?.expected_investmentdt?.total_equivalent_usd || 0),
                        type: 'Text'
                    },
                    {
                        label: 'Period during which Foreign Cash and Equivalents will be brought in USD',
                        value: (this.data?.expected_investmentdt?.period_start_date || '') + '  -  ' + (this.data?.expected_investmentdt?.period_end_date || ''),
                        type: 'Text', option: {}
                    },
                ]
            },
            {
                section: '5. Expected timetable of the investment:',
                form: [
                    {
                        label: 'Construction or preparatory period:',
                        value: `${this.data?.timeline['construction_date_no'] + this.data?.timeline['construction_date_label'] || ''}`,
                        type: 'Text'
                    },
                    {
                        label: 'Commercial operation period:',
                        value: `${this.data?.timeline['commercial_date_no'] + this.data?.timeline['commercial_date_label'] || ''}`,
                        type: 'Text'
                    },
                    {
                        label: 'Planned investment period:',
                        value: `${this.data?.timeline['investment_start_date'] + ' Years' || ''}`,
                        type: 'Text'
                    }
                ]
            },
            {
                section: '6. Expected number of employees over the first ten years of investment:',
                form: [
                    {
                        label: '(a) Myanmar citizens – Qty (pax)', type: 'Table', data: [
                            ['Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5', 'Year 6', 'Year 7', 'Year 8', 'Year 9', 'Year 10'],
                            [this.data?.employment_plan?.year_1?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_2?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_3?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_4?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_5?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_6?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_7?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_8?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_9?.mm_citizens_qty_total || '', this.data?.employment_plan?.year_10?.mm_citizens_qty_total || '']
                        ]
                    },
                    {
                        label: '(b) Foreign citizens – Qty (pax)', type: 'Table', data: [
                            ['Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5', 'Year 6', 'Year 7', 'Year 8', 'Year 9', 'Year 10'],
                            [this.data?.employment_plan?.year_1?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_2?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_3?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_4?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_5?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_6?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_7?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_8?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_9?.fr_citizens_qty_total || '', this.data?.employment_plan?.year_10?.fr_citizens_qty_total || '']
                        ]
                    },
                ]
            },
            {
                section: '7. Form of investment',
                form: [
                    { value: this.data.invest_form.form_investment },
                    {
                        label: 'Note: The Commission may publish the summary of the proposal for public awareness. The investor may request the Commission to refrain from publishing commercial-in-confidential information of its investment.', type: 'Text', option: { italics: true }
                    }
                ]
            },
        ]
    }

    public download(): Observable<any> {
        return Observable.create((observer) => {
            const documentCreator = new EndorsementWordExport();
            const doc = documentCreator.create(this.permitForm, this.docName);
            Packer.toBlob(doc).then(blob => {
                try {
                    saveAs(blob, this.fileName);
                    observer.next("Document created successfully");
                    observer.complete();
                } catch (err) {
                    observer.error(err);
                }
            });
        });

    }
}

export class CostBenefitExport {
    docName: string = 'COST BENEFIT ANALYSIS';
    fileName: string = 'cost-benefit-analysis'
    permitForm: any[] = [];
    data: any;
    language: any = 'en';
    isic: any = [];

    total_emp: any = {};
    cost_benefit_analysis: any = {};

    total_mark: number = 0;

    constructor(permit: any, lan: any, isic: any) {
        this.language = lan;
        this.total_mark = 0;

        if (lan == 'en' && permit.form.language == 'English') {
            this.data = permit?.form?.data || permit?.form?.data_mm;
        }
        else {
            this.data = permit?.form?.data_mm || {};
        }
        this.cost_benefit_analysis = permit?.cost_benefit_analysis || {};
        this.fileName = `${this.fileName}-${permit.application_no}.docx`;

        this.isic = isic.filter(x => x.group == this.data?.company_info?.specific_business_sector?.isic_group_id || '') || [];

        // For Row 6 
        var promoted_sector = 0;
        if (this.isic.length > 0 && this.isic[0].promoted_sector) {
            promoted_sector = 10;
        }
        if (this.isic.length > 0 && !this.isic[0].promoted_sector) {
            promoted_sector = 0;
        }

        // For Row 7
        var priority_sector = 0;
        if (this.isic.length > 0) {
            switch (this.isic[0].priority) {
                case 'Agriculture sector': priority_sector = 80; break;
                case 'Manufacture Sector': priority_sector = 70; break;
                case 'Basic Infrastructure': priority_sector = 60; break;
                case 'Live-stock': priority_sector = 50; break;
                case 'Logistic': priority_sector = 40; break;
                case 'Education': priority_sector = 30; break;
                case 'Health': priority_sector = 20; break;
                case 'Affordable Housing  and Industrial City': priority_sector = 10; break;
                default: priority_sector = 0;
            }
        }

        // For Row 2 and Row 3
        this.total_emp = { unskill: 0, skill: 0 }
        this.data.employment_plan.emp_max = this.data?.employment_plan?.emp_max || [];
        this.data.employment_plan.emp_max.forEach(x => {
            if (x.classification == 'Elementary occupations') {
                this.total_emp.unskill += Number(x[`myanmar_citizens_qty`] || 0);
            }
            else {
                this.total_emp.skill += Number(x[`myanmar_citizens_qty`] || 0);
            }
        });

        if (this.total_emp.unskill == 0) {
            this.total_emp.unskill_mark = 0;
        }
        else if (this.total_emp.unskill >= 1 && this.total_emp.unskill <= 100) {
            this.total_emp.unskill_mark = 10;
        }
        else if (this.total_emp.unskill >= 101 && this.total_emp.unskill <= 200) {
            this.total_emp.unskill_mark = 20;
        }
        else if (this.total_emp.unskill >= 201 && this.total_emp.unskill <= 300) {
            this.total_emp.unskill_mark = 30;
        }
        else if (this.total_emp.unskill >= 301 && this.total_emp.unskill <= 400) {
            this.total_emp.unskill_mark = 40;
        }
        else if (this.total_emp.unskill >= 401 && this.total_emp.unskill <= 500) {
            this.total_emp.unskill_mark = 50;
        }
        else if (this.total_emp.unskill >= 501 && this.total_emp.unskill <= 600) {
            this.total_emp.unskill_mark = 60;
        }
        else if (this.total_emp.unskill >= 601 && this.total_emp.unskill <= 700) {
            this.total_emp.unskill_mark = 70;
        }
        else if (this.total_emp.unskill >= 701 && this.total_emp.unskill <= 800) {
            this.total_emp.unskill_mark = 80;
        }
        else if (this.total_emp.unskill >= 801 && this.total_emp.unskill <= 900) {
            this.total_emp.unskill_mark = 90;
        }
        else {
            this.total_emp.unskill_mark = 100;
        }

        if (this.total_emp.skill >= 1 && this.total_emp.skill <= 49) {
            this.total_emp.skill_mark = this.total_emp.skill;
        }
        else {
            this.total_emp.skill_mark = 100;
        }

        // For Row 5
        var investment_amount = this.data?.expected_investment?.spent_amount_usd || 0, investment_amount_mark = 0;
        if (investment_amount <= 10000000) {
            investment_amount_mark = 0;
        }
        else if (investment_amount > 10000000 && investment_amount <= 20000000) {
            investment_amount_mark = 10;
        }
        else if (investment_amount > 20000000 && investment_amount <= 30000000) {
            investment_amount_mark = 20;
        }
        else if (investment_amount > 30000000 && investment_amount <= 40000000) {
            investment_amount_mark = 30;
        }
        else if (investment_amount > 40000000 && investment_amount <= 50000000) {
            investment_amount_mark = 40;
        }
        else if (investment_amount > 50000000 && investment_amount <= 60000000) {
            investment_amount_mark = 50;
        }
        else if (investment_amount > 60000000 && investment_amount <= 70000000) {
            investment_amount_mark = 60;
        }
        else if (investment_amount > 70000000 && investment_amount <= 80000000) {
            investment_amount_mark = 70;
        }
        else if (investment_amount > 80000000 && investment_amount <= 90000000) {
            investment_amount_mark = 80;
        }
        else if (investment_amount > 90000000 && investment_amount <= 100000000) {
            investment_amount_mark = 90;
        }
        else {
            investment_amount_mark = 100;
        }

        // For Row 8
        var investment_date_diff = this.data?.timeline?.investment_start_date;
        var period_investment_mark = 0;
        if (investment_date_diff < 10) {
            period_investment_mark = 0;
        }
        else if (investment_date_diff >= 10 && investment_date_diff <= 20) {
            period_investment_mark = 10;
        }
        else if (investment_date_diff > 20 && investment_date_diff <= 30) {
            period_investment_mark = 20;
        }
        else if (investment_date_diff > 30 && investment_date_diff <= 40) {
            period_investment_mark = 30;
        }
        else if (investment_date_diff > 40 && investment_date_diff <= 50) {
            period_investment_mark = 40;
        }
        else {
            period_investment_mark = 50;
        }

        this.total_mark = Number(this.data?.share_holder?.myanmar_share_ratio || 0) +
            Number(this.total_emp.unskill_mark) +
            Number(this.total_emp.skill_mark) +
            Number(this.data?.company_info?.estimated_export || 0) +
            Number(investment_amount_mark) +
            Number(this.cost_benefit_analysis?.investing_in_promoted_sector || 0) +
            Number(this.cost_benefit_analysis?.investing_in_priority_sector || 0) +
            Number(period_investment_mark) +
            Number(this.cost_benefit_analysis?.zone || 0) +
            Number(this.cost_benefit_analysis?.quality_of_company_mark || 0) +
            Number(this.cost_benefit_analysis?.technical_standard_mark || 0) +
            Number(this.cost_benefit_analysis?.csr_activities_mark || 0) +
            Number(this.cost_benefit_analysis?.basic_resources_mark || 0) +
            Number(this.cost_benefit_analysis?.amount_rest_waste_disposal_mark || 0) +
            Number(this.cost_benefit_analysis?.environmental_impact_assessment_mark || 0) +
            Number(this.cost_benefit_analysis?.social_impact_assessment_mark || 0) +
            Number(this.cost_benefit_analysis?.domestic_raw_material_mark || 0);

        if (this.language == 'en') {
            this.docName = 'COST BENEFIT ANALYSIS';
            this.permitForm = [
                {
                    form: [
                        {
                            label: '', type: 'Table', data: [
                                ['Sr. No', 'Description', 'Actual data', 'Given Marks', 'Maximum Marks'],
                                [1, 'Myanmar citizens’ shareholding percentage (%)',
                                    `Myanmar Citizen share ratio: ${this.data?.share_holder?.myanmar_share_ratio || 0} %`,
                                    `${this.data?.share_holder?.myanmar_share_ratio || 0} Marks`, 100],
                                [2, 'Citizen Employment (Unskilled labour)',
                                    `Myanmar Citizen Employment (Unskilled labor) over lifetime of investment: ${this.total_emp.unskill} Employees`,
                                    `${this.total_emp.unskill_mark} Marks`, 100],
                                [3, 'Citizen Employment (Skilled labour)',
                                    `Myanmar Citizen Employment (Skilled labor) over lifetime of investment: ${this.total_emp.skill} Employees`,
                                    `${this.total_emp.skill_mark} Marks`, 100],
                                [4, 'Export market sales (%)',
                                    `Estimated export market sales / Total sales (%): ${this.data?.company_info?.estimated_export || 0}`,
                                    `${this.data?.company_info?.estimated_export || 0} Marks`, 100],
                                [5, 'Amount of Investment',
                                    `Estimated investment amount: ${this.data?.expected_investment?.spent_amount_usd || 0} USD`,
                                    `Score: ${investment_amount_mark} Marks`, 100],
                                // [6, 'Investing in Promoted Sector',
                                //     `Specific sector(s) or investment: ${this.data?.company_info?.general_sector_id}`,
                                //     `${promoted_sector} Marks`, 10],
                                // [7, 'Investing in Priority Sector',
                                //     `Specific sector(s) or investment: ${this.data?.company_info?.general_sector_id}
                                //     Priority sector or other: ${this.isic.length > 0 ? this.isic[0].priority : ''}`,
                                //     `${priority_sector} Marks`, 80],
                                [6, 'Investing in Promoted Sector',
                                    `Specific sector(s) or investment: ${this.data?.company_info?.general_sector_id}`,
                                    this.cost_benefit_analysis?.investing_in_promoted_sector ? `${this.cost_benefit_analysis?.investing_in_promoted_sector} Marks` : '', 10],
                                [7, 'Investing in Priority Sector',
                                    `Specific sector(s) or investment: ${this.data?.company_info?.general_sector_id}
                                    Priority sector or other: ${this.isic.length > 0 ? this.isic[0].priority : ''}`,
                                    this.cost_benefit_analysis?.investing_in_priority_sector ? `${this.cost_benefit_analysis?.investing_in_priority_sector} Marks` : '', 80],
                                [8, 'Period of Investment',
                                    `Expected investment period: ${this.data?.timeline?.investment_start_date}`,
                                    `${period_investment_mark} Marks`, 50],
                                [9, 'Zone', `Percentage (%) of Expected Investment Value (Zone 1): ${this.data?.expected_by_zone?.zone_1?.zone1_total || 0} 
                                    Percentage (%) of Expected Investment Value (Zone 2): ${this.data?.expected_by_zone?.zone_2?.zone1_total || 0}
                                    Percentage (%) of Expected Investment Value (Zone 3): ${this.data?.expected_by_zone?.zone_3?.zone1_total || 0}`,
                                    this.cost_benefit_analysis?.zone ? `${this.cost_benefit_analysis?.zone} Marks` : '', 30],
                                [10, 'Quality of Company',
                                    this.cost_benefit_analysis?.quality_of_company_data || '',
                                    this.cost_benefit_analysis?.quality_of_company_mark ? `${this.cost_benefit_analysis?.quality_of_company_mark} Marks` : '', 130],
                                [11, 'Technical standard',
                                    this.cost_benefit_analysis?.technical_standard_data || '',
                                    this.cost_benefit_analysis?.technical_standard_mark ? `${this.cost_benefit_analysis?.technical_standard_mark} Marks` : '', 30],
                                [12, 'CSR Activities',
                                    this.cost_benefit_analysis?.csr_activities_data || '',
                                    this.cost_benefit_analysis?.csr_activities_mark ? `${this.cost_benefit_analysis?.csr_activities_mark} Marks` : '', 10],
                                [13, 'Basic Resources Use (%)',
                                    this.cost_benefit_analysis?.basic_resources_Data || '',
                                    this.cost_benefit_analysis?.basic_resources_mark ? `${this.cost_benefit_analysis?.basic_resources_mark} Marks` : '', 100],
                                [14, 'The Amount of the Rest of Waste Disposal',
                                    this.cost_benefit_analysis?.amount_rest_waste_disposal_data || '',
                                    this.cost_benefit_analysis?.amount_rest_waste_disposal_mark ? `${this.cost_benefit_analysis?.amount_rest_waste_disposal_mark} Marks` : '', 100],
                                [15, 'Environmental Impact Assessment',
                                    this.cost_benefit_analysis?.environmental_impact_assessment_data || '',
                                    this.cost_benefit_analysis?.environmental_impact_assessment_mark ? `${this.cost_benefit_analysis?.environmental_impact_assessment_mark} Marks` : '', 100],
                                [16, 'Social Impact Assessment',
                                    this.cost_benefit_analysis?.social_impact_assessment_data || '',
                                    this.cost_benefit_analysis?.social_impact_assessment_mark ? `${this.cost_benefit_analysis?.social_impact_assessment_mark} Marks` : '', 100],
                                [17, 'Domestic Raw Material Use (%)',
                                    this.cost_benefit_analysis?.domestic_raw_material_data || '',
                                    this.cost_benefit_analysis?.domestic_raw_material_mark ? `${this.cost_benefit_analysis?.domestic_raw_material_mark} Marks` : '', 100],
                                ['', 'Total amount of given marks', '', `${this.total_mark} Marks`, '1340']
                            ]
                        },
                    ]
                }
            ]
        }
        else {
            this.docName = 'အရည်အသွေးအကဲဖြတ်ခြင်းနှင့် ရင်းနှီးမြှုပ်နှံမှုချိန်ညှိခြင်း';
            this.permitForm = [
                {
                    form: [
                        {
                            label: '', type: 'Table', data: [
                                ['စဉ်', 'အကြောင်းအရာ', 'အဆိုပြုချက်ပါအချက်များ', 'ပေးမှတ်', 'အများဆုံး ပေးမှတ်'],
                                ['၁။', 'မြန်မာနိုင်ငံသား အစု ရှယ်ယာ ပါဝင်မှု %',
                                    `မြန်မာနိုင်ငံသား အစုရှယ် ယာပါဝင်မှု အချိုး ${this.data?.share_holder?.myanmar_share_ratio || 0} %`,
                                    `${this.data?.share_holder?.myanmar_share_ratio || 0} Marks`, '၁၀၀'],
                                ['၂။', 'နိုင်ငံသားအလုပ်သမား ခန့်အပ်မှု (ကျွမ်းကျင်မှု မလို သောအလုပ်)',
                                    `ရင်းနှီးမြှပ်နှံမှုလုပ်ငန်း သက်တမ်းတစ်လျှောက် နိုင်ငံ သားအလုပ်သမား ခန့်အပ်မှု (ကျွမ်းကျင်မှု မလို သော အလုပ်) ${this.total_emp.unskill} Employees`,
                                    `${this.total_emp.unskill_mark} Marks`, '၁၀၀'],
                                ['၃။', 'နိုင်ငံသား အလုပ် သမား ခန့်အပ်မှု (ကျွမ်းကျင်မှုလိုအပ်သောအလုပ်- အကြီး တန်း/ နည်းပညာ/ အကြံပေး/ လုပ်ငန်း ကျွမ်းကျင်)',
                                    `ရင်းနှီးမြှပ်နှံမှုလုပ်ငန်း သက်တမ်းတစ်လျှောက် နိုင်ငံ သားအလုပ်သမား ခန့်အပ်မှု (ကျွမ်းကျင်မှုလိုအပ်သောအလုပ်) ${this.total_emp.skill} Employees`,
                                    `${this.total_emp.skill_mark} Marks`, '၁၀၀'],
                                ['၄။', 'ပြည်ပစျေးကွက် တင်ပို့ရောင်းချမှု (%)',
                                    `ခန့်မှန်း ပြည်ပစျေးကွက် တင်ပို့ရောင်းချမှု / စုစုပေါင်း ရောင်းချမှု (%) ${this.data?.company_info?.estimated_export || 0}`,
                                    `${this.data?.company_info?.estimated_export || 0} Marks`, '၁၀၀'],
                                ['၅။', 'ရင်းနှီးမြှုပ်နှံမှုပမာဏ',
                                    `ခန့်မှန်း ရင်းနှီးမြှုပ်နှံမှု ပမာဏ ${this.data?.expected_investment?.spent_amount_usd || 0} USD`,
                                    `Score: ${investment_amount_mark} Marks`, '၁၀၀'],
                                // ['၆။', 'ရင်းနှီးမြှုပ်နှံမှုမြှင့် တင်သည့် ကဏ္ဍတွင် ဆောင်ရွက်ခြင်း',
                                //     `Specific sector(s) or investment: ${this.data?.company_info?.general_sector_id}`,
                                //     `${promoted_sector} Marks`, '၁၀'],
                                // ['၇။', 'ဦးစားပေးဆောင်ရွက်မည့် ရင်းနှီးမြှုပ်နှံမှု လုပ်ငန်းများ',
                                //     `Specific sector(s) or investment: ${this.data?.company_info?.general_sector_id}
                                //     Priority sector or other: ${this.isic.length > 0 ? this.isic[0].priority : ''}`,
                                //     `${priority_sector} Marks`, '၈၀'],
                                ['၆။', 'ရင်းနှီးမြှုပ်နှံမှုမြှင့် တင်သည့် ကဏ္ဍတွင် ဆောင်ရွက်ခြင်း',
                                    `Specific sector(s) or investment: ${this.data?.company_info?.general_sector_id}`,
                                    this.cost_benefit_analysis?.investing_in_promoted_sector ? `${this.cost_benefit_analysis?.investing_in_promoted_sector} Marks` : '', '၁၀'],
                                ['၇။', 'ဦးစားပေးဆောင်ရွက်မည့် ရင်းနှီးမြှုပ်နှံမှု လုပ်ငန်းများ',
                                    `Specific sector(s) or investment: ${this.data?.company_info?.general_sector_id}
                                    Priority sector or other: ${this.isic.length > 0 ? this.isic[0].priority : ''}`,
                                    this.cost_benefit_analysis?.investing_in_priority_sector ? `${this.cost_benefit_analysis?.investing_in_priority_sector} Marks` : '', '၈၀'],
                                ['၈။', 'ရင်းနှီးမြှုပ်နှံမှုသက်တမ်း',
                                    `Expected investment period: ${this.data?.timeline?.investment_start_date}`,
                                    `${period_investment_mark} Marks`, '၅၀'],
                                ['၉။', 'ဇုန်သတ်မှတ်ချက်', `Percentage (%) of Expected Investment Value (Zone 1): ${this.data?.expected_by_zone?.zone_1?.zone1_total || 0} 
                                    Percentage (%) of Expected Investment Value (Zone 2): ${this.data?.expected_by_zone?.zone_2?.zone1_total || 0}
                                    Percentage (%) of Expected Investment Value (Zone 3): ${this.data?.expected_by_zone?.zone_3?.zone1_total || 0}`,
                                    this.cost_benefit_analysis?.zone ? `${this.cost_benefit_analysis?.zone} Marks` : '', '၃၀'],
                                ['၁၀။', 'ကုမ္ပဏီ အရည်အသွေး',
                                    this.cost_benefit_analysis?.quality_of_company_data || '',
                                    this.cost_benefit_analysis?.quality_of_company_mark ? `${this.cost_benefit_analysis?.quality_of_company_mark} Marks` : '', '၁၃၀'],
                                ['၁၁။', 'နည်းပညာအဆင့်အတန်း',
                                    this.cost_benefit_analysis?.technical_standard_data || '',
                                    this.cost_benefit_analysis?.technical_standard_mark ? `${this.cost_benefit_analysis?.technical_standard_mark} Marks` : '', '၃၀'],
                                ['၁၂။', 'CSRဆောင်ရွက်မှု အစီအစဉ်',
                                    this.cost_benefit_analysis?.csr_activities_data || '',
                                    this.cost_benefit_analysis?.csr_activities_mark ? `${this.cost_benefit_analysis?.csr_activities_mark} Marks` : '', '၁၀'],
                                ['၁၃။', 'သယံဇာတအခြေခံ သုံးစွဲမှု (%)',
                                    this.cost_benefit_analysis?.basic_resources_Data || '',
                                    this.cost_benefit_analysis?.basic_resources_mark ? `${this.cost_benefit_analysis?.basic_resources_mark} Marks` : '', '၁၀၀'],
                                ['၁၄။', 'မြန်မာနိုင်ငံအတွင်း စွန့်ပစ်ပစ္စည်းကျန်ရှိမှု ပမာဏ',
                                    this.cost_benefit_analysis?.amount_rest_waste_disposal_data || '',
                                    this.cost_benefit_analysis?.amount_rest_waste_disposal_mark ? `${this.cost_benefit_analysis?.amount_rest_waste_disposal_mark} Marks` : '', '၁၀၀'],
                                ['၁၅။', 'ပတ်ဝန်းကျင်ထိခိုက်မှုနိုင်မှု အခြေအနေ',
                                    this.cost_benefit_analysis?.environmental_impact_assessment_data || '',
                                    this.cost_benefit_analysis?.environmental_impact_assessment_mark ? `${this.cost_benefit_analysis?.environmental_impact_assessment_mark} Marks` : '', '၁၀၀'],
                                ['၁၆။', 'လူမှုရေးထိခိုက်နိုင်မှု အခြေအနေ',
                                    this.cost_benefit_analysis?.social_impact_assessment_data || '',
                                    this.cost_benefit_analysis?.social_impact_assessment_mark ? `${this.cost_benefit_analysis?.social_impact_assessment_mark} Marks` : '', '၁၀၀'],
                                ['၁၇။', 'ပြည်တွင်းကုန်ကြမ်း အသုံးပြုမှု (%)',
                                    this.cost_benefit_analysis?.domestic_raw_material_data || '',
                                    this.cost_benefit_analysis?.domestic_raw_material_mark ? `${this.cost_benefit_analysis?.domestic_raw_material_mark} Marks` : '', '၁၀၀'],
                                ['', 'စုစုပေါင်းအများဆုံးပေးမှတ်', '', `${this.total_mark} Marks`, '၁၃၄၀']
                            ]
                        },
                    ]
                }
            ]
        }

    }

    public download(): Observable<any> {
        return Observable.create((observer) => {
            const documentCreator = new EndorsementWordExport();
            const doc = documentCreator.create(this.permitForm, this.docName);
            Packer.toBlob(doc).then(blob => {
                try {
                    saveAs(blob, this.fileName);
                    observer.next("Document created successfully");
                    observer.complete();
                } catch (err) {
                    observer.error(err);
                }
            });
        });

    }
}

export class CertificateExport {
    docName: string = 'ENDORSEMENT CERTIFICATE';
    fileName: string = 'endorsement-certificate'
    endoForm: any = [];
    data: any;
    language: any = 'en';

    constructor(submission: any, lan: any) {
        this.language = lan;
        this.data = submission?.form?.language == 'English' ? submission?.form?.data : submission?.form?.data_mm;

        this.fileName = `${this.fileName}-${submission.application_no}.docx`;

        var isic_class_ids = '';
        if (Array.isArray(this.data?.company_info?.specific_business_sector?.isic_class_ids)) {
            this.data?.company_info?.specific_business_sector?.isic_class_ids.forEach(x => {
                isic_class_ids += x + ', ';
            });
            if (isic_class_ids.length > 2)
                isic_class_ids = isic_class_ids.substring(0, isic_class_ids.length - 2);
        }
        else {
            isic_class_ids = this.data?.company_info?.specific_business_sector?.isic_class_ids;
        }

        var totalMyanmarCash = Number(this.data?.expected_investmentdt?.cash_equivalent[0]?.myan_origin || 0) +
            Number(this.data?.expected_investmentdt?.cash_equivalent[1]?.myan_origin || 0) +
            Number(this.data?.expected_investmentdt?.cash_equivalent[2]?.myan_origin || 0);

        console.log(this.data);

        var date = new DatePipe('en');

        if (this.language == 'en') {
            this.docName = 'PERMIT/ENDORSEMENT CERTIFICATE';
            this.endoForm = [
                {
                    form: [
                        {
                            label: '1. Permit/endorsement number',
                            value: submission?.permit_endo_no || this.data?.application_no || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '2. Name of Enterprise',
                            value: this.data?.company_info?.investment_name || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '3. Form of Investment',
                            value: this.data?.invest_form?.form_investment,
                            type: 'Text', option: {}
                        },
                        {
                            label: '4. Type of Company',
                            value: this.data?.company_info?.company_type,
                            type: 'Text', option: {}
                        },
                        {
                            label: '5. Specific sector of investment',
                            value: isic_class_ids,
                            type: 'Text', option: {}
                        },
                        {
                            label: '6. Location(s) of the investment project', type: 'Table', data: [
                                ['State/Region', 'District', 'Township'],
                                ...this.data?.location?.map((x, index) => {
                                    return [x?.state, x?.district, x?.township]
                                })
                            ]
                        },
                        {
                            label: '7. Amount of Foreign Capital',
                            value: this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd ? this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd + ' Equivalent US$' : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '8. Period during which Foreign Cash and Equivalents will be brought in USD',
                            value: this.data?.expected_investmentdt?.start_date && this.data?.expected_investmentdt?.end_date ? this.data?.expected_investmentdt?.start_date + '  -  ' + this.data?.expected_investmentdt?.end_date : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '9. (Expected) investment value',
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Total Amount in Myanmar Kyat',
                            value: totalMyanmarCash,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'Equivalent Amount in USD (Million)',
                            // value: this.data?.expected_investment?.spent_amount_usd,
                            value: this.data?.expected_investmentdt?.total_equivalent_usd,
                            type: 'Text', option: {}
                        },
                        {
                            label: '10. (Expected) Construction or preparatory period',
                            value: this.data?.timeline?.construction_start_date,
                            type: 'Text', option: {}
                        },
                        {
                            label: '11.  (Expected) Investment Period',
                            value: this.data?.timeline?.investment_start_date,
                            type: 'Text', option: {}
                        },
                        {
                            label: '12. Name(s) of investor(s)', type: 'Table', data: [
                                ['Name', 'Address', 'Country of Incorporation or citizenship'],
                                ['', '', '']
                            ]
                        },
                        {
                            label: '13. Issued Date',
                            value: date.transform(submission?.issued_date, 'dd-MM-yyyy'),
                            type: 'Text', option: {}
                        },
                        {
                            label: '14. Project Duration',
                            value: submission?.project_duration_no && submission?.project_duration_label ? submission?.project_duration_no + ' ' + submission?.project_duration_label : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '15. Date of Commercial Operation Period',
                            value: date.transform(submission?.commercial_date, 'dd-MM-yyyy'),
                            type: 'Text', option: {}
                        },
                    ]
                }
            ];

            if (Array.isArray(this.data?.investor_list)) {
                this.endoForm[0].form[11] = {
                    label: '12. Name(s) of investor(s)', type: 'Table', data: [
                        ['Name', 'Address', 'Country of Incorporation or citizenship'],
                        ...this.data?.investor_list?.map((x, index) => {
                            if (x.company?.company_name) {
                                return [x?.company?.company_name, x?.company?.address, x?.company?.incorporate_country]
                            }
                            else if (x.government?.name) {
                                return [x?.government?.name, x?.government?.address, 'Myanmar']
                            }
                            else {
                                return [x?.individual?.full_name, x?.individual?.address, x?.individual?.citizenship]
                            }
                        })
                    ]
                };
            }
        }
        else {
            this.docName = 'ခွင့်ပြုမိန့် လက်မှတ်/ အတည်ပြုမိန့် လက်မှတ်';
            this.endoForm = [
                {
                    form: [
                        {
                            label: '၁။ ခွင့်ပြုမိန့် / အတည်ပြုမိန့် အမှတ်စဥ်',
                            value: submission?.permit_endo_no || this.data?.application_no || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၂။  စီးပွားရေးအဖွဲ့အစည်း၏ အမည်',
                            value: this.data?.company_info?.investment_name || '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၃။ ရင်းနှီးမြှုပ်နှံမှု  ပုံစံ',
                            value: this.data?.invest_form?.form_investment,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၄။ ကုမ္ပဏီအမျိုးအစား',
                            value: this.data?.company_info?.company_type,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၅။ အဆိုပြုရင်းနှီးမြှုပ်နှံမှုလုပ်ငန်း၏ လုပ်ငန်းကဏ္ဍအသေးစိတ်',
                            value: isic_class_ids,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၆။. ရင်းနှီးမြှုပ်နှံမှု တည်နေရာ (များ)', type: 'Table', data: [
                                ['တိုင်း/ပြည်နယ်', 'ခရိုင်', 'မြို့နယ်'],
                                ...this.data?.location?.map((x, index) => {
                                    return [x?.state, x?.district, x?.township]
                                })
                            ]
                        },
                        {
                            label: '၇။ နိုင်ငံခြား ငွေကြေးရင်းမြစ်ပမာဏ',
                            value: this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd ? this.data?.proposed_invest_basic_info?.total.foreign_capital?.usd + ' ညီမျှသည့် ခန့်မှန်းအမေရိကန်ဒေါ်လာ' : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၈။ နိုင်ငံခြား ငွေကြေး ယူဆောင်လာမည့် ကာလ',
                            value: this.data?.expected_investmentdt?.start_date && this.data?.expected_investmentdt?.end_date ? this.data?.expected_investmentdt?.start_date + '  -  ' + this.data?.expected_investmentdt?.end_date : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၉။ ခန့်မှန်း ရင်းနှီးမြှုပ်နှံမှုတန်ဖိုး',
                            type: 'Text', option: {}
                        },
                        {
                            label: 'စုစုပေါင်း မြန်မာကျပ် ပမာဏ',
                            value: totalMyanmarCash,
                            type: 'Text', option: {}
                        },
                        {
                            label: 'အမေရိကန်ဒေါ်လာနှင့် ညီမျှသောပမာဏ ( သန်း )',
                            // value: this.data?.expected_investment?.spent_amount_usd,
                            value: this.data?.expected_investmentdt?.total_equivalent_usd,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၁၀။ ( ခန့်မှန်း ) တည်ဆောက်ရေးကာလ သို့မဟုတ် ပြင်ဆင်မှု ကာလ',
                            value: this.data?.timeline?.construction_start_date,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၁၁။ ( ခန့်မှန်း ) ရင်းနှီးမြှုပ်နှံမှုကာလ',
                            value: this.data?.timeline?.investment_start_date,
                            type: 'Text', option: {}
                        },
                        {
                            label: '၁၂။ ရင်းနှီးမြှုပ်နှံသူ(များ)၏ အမည်(များ)', type: 'Table', data: [
                                ['အမည်', 'လိပ်စာ', 'နိုင်ငံသား သို့မဟုတ် စီးပွားရေးဖွဲ့စည်းတည်ထောင်ထားသော နိုင်ငံ'],
                                ['', '', '']
                            ]
                        },
                        {
                            label: '၁၃။ ထုတ်ပေးသည့်နေ့စွဲ',
                            value: date.transform(submission?.issued_date, 'dd-MM-yyyy'),
                            type: 'Text', option: {}
                        },
                        {
                            label: '၁၄။ စီမံကိန်းကာလ',
                            value: submission?.project_duration_no && submission?.project_duration_label ? submission?.project_duration_no + ' ' + submission?.project_duration_label : '',
                            type: 'Text', option: {}
                        },
                        {
                            label: '၁၅။ စီးပွားဖြစ်လုပ်ငန်းလည်ပတ်သည့်ကာလ',
                            value: date.transform(submission?.commercial_date, 'dd-MM-yyyy'),
                            type: 'Text', option: {}
                        },
                    ]
                }
            ];

            if (Array.isArray(this.data?.investor_list)) {
                this.endoForm[0].form[11] = {
                    label: '၁၂။ ရင်းနှီးမြှုပ်နှံသူ(များ)၏ အမည်(များ)', type: 'Table', data: [
                        ['အမည်', 'လိပ်စာ', 'နိုင်ငံသား သို့မဟုတ် စီးပွားရေးဖွဲ့စည်းတည်ထောင်ထားသော နိုင်ငံ'],
                        ...this.data?.investor_list?.map((x, index) => {
                            if (x.company?.company_name) {
                                return [x?.company?.company_name, x?.company?.address, x?.company?.incorporate_country]
                            }
                            else if (x.government?.name) {
                                return [x?.government?.name, x?.government?.address, 'Myanmar']
                            }
                            else {
                                return [x?.individual?.full_name, x?.individual?.address, x?.individual?.citizenship]
                            }
                        })
                    ]
                };
            }
        }
    }

    public download(): Observable<any> {
        return Observable.create((observer) => {
            const documentCreator = new EndorsementWordExport();
            const doc = documentCreator.create(this.endoForm, this.docName);
            Packer.toBlob(doc).then(blob => {
                try {
                    saveAs(blob, this.fileName);
                    observer.next("Document created successfully");
                    observer.complete();
                } catch (err) {
                    observer.error(err);
                }
            });
        });

    }
}
