import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-endorsement-detail',
    templateUrl: './endorsement-detail.component.html',
    styleUrls: ['./endorsement-detail.component.scss']
})
export class EndorsementDetailComponent implements OnInit {
    form: any;
    id: any;
    index: any;
    constructor(
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.form = paramMap.get('form');
            this.id = paramMap.get('id');
            this.index = paramMap.get('index');
        });
    }
}
