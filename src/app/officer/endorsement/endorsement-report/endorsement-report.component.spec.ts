import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementReportComponent } from './endorsement-report.component';

describe('EndorsementReportComponent', () => {
  let component: EndorsementReportComponent;
  let fixture: ComponentFixture<EndorsementReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
