import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementHistoryComponent } from './endorsement-history.component';

describe('EndorsementHistoryComponent', () => {
  let component: EndorsementHistoryComponent;
  let fixture: ComponentFixture<EndorsementHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
