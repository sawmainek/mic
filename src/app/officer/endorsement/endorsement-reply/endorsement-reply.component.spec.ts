import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndorsementReplyComponent } from './endorsement-reply.component';

describe('EndorsementReplyComponent', () => {
  let component: EndorsementReplyComponent;
  let fixture: ComponentFixture<EndorsementReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndorsementReplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndorsementReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
