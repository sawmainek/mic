export function getStatus(status: string) {
    switch (status) {
        case 'New':
            return `btn-danger`
        case 'Pending':
            return `btn-warning`
        case 'Revision':
            return `btn-warning`
        case 'PAT Meetings':
            return `btn-info`
        case 'MIC Meetings':
            return `btn-info`
        case 'Acceptance':
            return `btn-success`
        case 'Rejects':
            return `btn-danger`
        default:
            return `btn-info`
    }
}