import { ISICService } from 'src/services/isic.service';
import { environment } from './../../../environments/environment.prod';
import { AuthService } from './../../../services/auth.service';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ShareholderDetailReportService } from 'src/services/shareholder-detail-report.service';
declare var jQuery: any;

@Component({
    selector: 'app-shareholder-detail-report',
    templateUrl: './shareholder-detail-report.component.html',
    styleUrls: ['./shareholder-detail-report.component.scss']
})
export class ShareholderDetailReportComponent implements OnInit {
    loading: boolean = true;
    reports: any;
    user: any;
    datatable: any;
    isicSectors: any[] = [];
    displayedColumns: any = [
        {
            field: 'application_no',
            title: 'Permit or Endorsement Application Number',
            type: 'number',
            width: 100
        },
        {
            field: 'company_name',
            title: 'Name of Company',
            sortable: false,
            width: 200
        },
        {
            field: 'investment_form',
            title: 'Form of Investment',
            sortable: false,
            width: 200
        },
        {
            field: 'permit_endo_no',
            title: 'Permit or Endorsement Number',
            sortable: false,
            width: 100
        },
        {
            field: 'issued_date',
            title: 'Issued Date',
            sortable: false,
            width: 100,
            template: (row) => {
                const datePipe = new DatePipe('en');
                return datePipe.transform(row.issued_date, 'dd-MM-yyyy');
            }
        },
        {
            field: 'country',
            title: 'Country',
            sortable: false,
            width: 100,
        },
        {
            field: 'share_percentage',
            title: 'Share %',
            sortable: true,
            width: 100,
        },
        {
            field: 'share_capital_usd',
            title: 'Issued Share Capital (in USD)',
            sortable: false,
            width: 100,
            template: (row) => {
                return this.formatNumber(row?.share_capital_usd || 0);
            }
        },
        {
            field: 'share_capital_kyat',
            title: 'Issued Share Capital (in MMK)',
            sortable: false,
            width: 100,
            template: (row) => {
                return this.formatNumber(row?.share_capital_kyat || 0);
            }
        },
        {
            field: 'business_type',
            title: 'Type of Business',
            sortable: false,
            width: 200
        },
        {
            field: 'sector',
            title: 'Sector',
            sortable: false,
            width: 200,
            template: (row) => {
                let classes = this.isicSectors.filter(x => {
                    let types: any[] = row.business_type || [];
                    return types.includes(x.name) ? true : false;
                })[0] || {};
                return classes.sector || '';
            }
        },
        {
            field: 'invest_location_1',
            title: 'Region 1',
            width: 150,
            template: (row) => {
                let address = '';
                if (row.invest_location) {
                    address = row.invest_location.zone_1.zone_1_list[0].state || '';
                }
                return address;
            }
        },
        {
            field: 'invest_location_2',
            title: 'Region 2',
            width: 150,
            template: (row) => {
                let address = '';
                console.log(row.invest_location);
                if (row.invest_location) {
                    address = row.invest_location.zone_2.zone_2_list[0].state || '';
                }
                return address;
            }
        },
        {
            field: 'invest_location_3',
            title: 'Region 3',
            width: 150,
            template: (row) => {
                let address = '';
                if (row.invest_location) {
                    address = row.invest_location.zone_3.zone_3_list[0].state || '';
                }
                return address;
            }
        }
    ];
    constructor(
        private authService: AuthService,
        private isicService: ISICService,
        private shareholderReport: ShareholderDetailReportService
    ) { }

    ngOnInit(): void {
        this.authService.getCurrentUser().subscribe(result => {
            this.user = result;
            this.isicService.getSectors().subscribe(results => {
                this.isicSectors = results;
                this.initDatatable();
            })
        }, err => {
            console.log(err);
        });
    }

    initDatatable() {
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };
        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/shareholderdetailreport',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: '',//this.buildSearch(),
                            query: '',
                            datatable: true
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            sortable: false,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            },
            search: {
                //input: jQuery('#summary-report-search'),
            }
        };
        // if (this.datatable) { jQuery('.kt-datatable-shareholder-report').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-shareholder-report').KTDatatable(options);
        jQuery('.kt-datatable-shareholder-report').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;
            jQuery('.onclick').on('click', function () {
                // setTimeout(() => {
                //     const index = jQuery(this).data('row');
                //     const data = ctx.datatable.getDataSet();
                //     ctx.goto(data[index].tax_incentive_id);
                // }, 300);
            });
        });
    }

    formatNumber(num) {
        let digit = Math.round(Number(num));
        return digit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    download(type) {
        let data = [];
        this.shareholderReport.get({ rows: 99999 }).subscribe(result => {

            const datePipe = new DatePipe('en');
            result.map((x, idx) => {
                // let flat = this.flattenObject(x);
                // this.deletePorperty(flat);
                let classes = this.isicSectors.filter(x => {
                    let types: any[] = x.business_type || [];
                    return types.includes(x.name) ? true : false;
                })[0] || {};
                data.push({
                    'Permit or Endorsement Application Number': x.application_no || '',
                    'Name of Company': x.company_name || '',
                    'Form of Investment': x.investment_form || '',
                    'Permit or Endorsement Number': x.permit_endo_no || '',
                    'Issued Date': x.issued_date ? datePipe.transform(x.issued_date, 'dd-MM-yyyy') : '',
                    'Country': x.country || '',
                    'Share %': x.share_percentage || '',
                    'Issued Share Capital (in USD)': this.formatNumber(x.share_capital_usd || 0),
                    'Issued Share Capital (in MMK)': this.formatNumber(x.share_capital_kyat || 0),
                    'Type of Business': x.business_type || '',
                    'Sector': classes.sector || '',
                    'Region 1': x.invest_location ? x.invest_location.zone_1.zone_1_list[0].state || '' : '',
                    'Region 2': x.invest_location ? x.invest_location.zone_2.zone_2_list[0].state || '' : '',
                    'Region 3': x.invest_location ? x.invest_location.zone_3.zone_3_list[0].state || '' : '',
                })
            });
            var csv = this.arrayToCSV(data);
            this.exportCSVFile(undefined, csv, 'shareholder-report');
        });
    }

    arrayToCSV(objArray) {
        const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
        var sizes = array.map(x => Object.keys(x).length);
        var maxIdx = sizes.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
        let str = `${Object.keys(array[maxIdx]).map(value => `"${value}"`).join(',')}` + '\r\n';
        for (let i in array[maxIdx]) {
            array.map(x => {
                if (!x.hasOwnProperty(i)) {
                    x[i] = '';
                }
            });
        }
        return array.reduce((str, next) => {
            str += `${Object.values(next).map(value => `"${value}"`).join(',')}` + '\r\n';
            return str;
        }, str);
    }

    exportCSVFile(headers, csv, fileTitle) {

        var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

        var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, exportedFilenmae);
        } else {
            var link = document.createElement('a');
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute('href', url);
                link.setAttribute('download', exportedFilenmae);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

}
