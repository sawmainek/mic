import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareholderDetailReportComponent } from './shareholder-detail-report.component';

describe('ShareholderDetailReportComponent', () => {
  let component: ShareholderDetailReportComponent;
  let fixture: ComponentFixture<ShareholderDetailReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareholderDetailReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareholderDetailReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
