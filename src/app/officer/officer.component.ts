import { FormLogService } from './../../services/form-log.service';
import { UserService } from 'src/app/auth/login/user.service';
import { Logout, Role } from './../core/auth/_actions/auth.actions';
import { Router, NavigationEnd } from '@angular/router';
import { map } from 'rxjs/operators';
import { currentUser } from 'src/app/core/auth/_selectors/auth.selectors';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { AppState } from 'src/app/core/reducers';
import { Store, select } from '@ngrx/store';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { environment } from 'src/environments/environment';

declare var $: any;
@Component({
  selector: 'app-officer',
  templateUrl: './officer.component.html',
  styleUrls: ['./officer.component.scss']
})
export class OfficerComponent implements OnInit {
    user: any = {};
    config: any = {};
    imageSrc = '';
    roles: any[] = [];
    menuList: any[] = [];
    openProfileMenu: boolean = true;
    formLogs: any[] = [];
    constructor(
        private router: Router,
        private store: Store<AppState>,
        private translate: TranslateService,
        private userService: UserService,
        private cdr: ChangeDetectorRef,
        private formLogService: FormLogService,
        private http: HttpClient
    ) {
        const lang = localStorage.getItem('lan');
        this.translate.setDefaultLang(lang ? lang : 'en');
        this.translate.use(lang ? lang : 'en');
        

        this.formLogService.get({
            search: 'incoming:equal:true',
            sort: 'id',
            order: 'desc',
            rows: 6
        }).subscribe(results => {
            this.formLogs = results;
        });

        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            if (this.menuList.length) {
                for (const menu of this.menuList) {
                    menu.active = window.location.href.indexOf(menu.target) !== -1 ? true : false;
                    for (const sub of menu.submenu) {
                        if(sub?.has_child) {
                            for (const child of sub.submenu) {
                                const params = new URLSearchParams(window.location.search);
                                if (params.get('filter')) {
                                    child.active = params.get('filter') == child.target ? true : false;
                                } else {
                                    child.active = params.get('status') == child.target ? true : false;
                                }
                            }
                            sub.active = sub.submenu.filter(x => x.active == true).length > 0 ? true : false;
                        } else {
                            const params = new URLSearchParams(window.location.search);
                            if (params.get('filter')) {
                                sub.active = params.get('filter') == sub.target ? true : false;
                            } else {
                                sub.active = params.get('status') == sub.target ? true : false;
                            }
                        }
                        
                    }
                }
            }
            window.scrollTo(0, 0);
        });
    }

    get showNoti(): boolean {
        return this.formLogs.filter(x => !x.seen).length > 0 ? true : false;
    }

    gotoFormDetail(log) {
        log.seen = true;
        this.formLogService.update(log.id, {seen: true}).subscribe();
        if (log.type == 'Inquiry ( Form 1 )' && log?.extra?.form_id) {
            this.router.navigate(['/officer/inquiry/form', log?.extra?.form_id]);
        }
        if (log.type == 'MIC-Permit' && log?.extra?.form_id) {
            this.router.navigate(['/officer/mic/choose-language', log?.extra?.form_id]);
        }
        if (log.type == 'Endorsement' && log?.extra?.form_id) {
            this.router.navigate(['/officer/endorsement/form', log?.extra?.form_id]);
        }
        if (log.type == 'Tax Incentive' && log?.extra?.form_id) {
            this.router.navigate(['/officer/tax-incentive/choose-language', log?.extra?.form_id]);
        }
        if (log.type == 'Land Right' && log?.extra?.form_id) {
            this.router.navigate(['/officer/land-right/landright-choose-investment', log?.extra?.form_id]);
        }
    }

    gotoNotifications() {
        this.router.navigateByUrl('/officer/notifications?status=Incoming');
    }

    loadSideMenu() {
        var wind_ = $(window),
            body_ = $('body');
        $(document).on('click', '.header-toggler a', function () {
            $('.header ul.navbar-nav').toggleClass('open');
            return false;
        });
        $(document).on('click', '*', function (e) {
            if (!$(e.target).is($('.navigation, .navigation *, .navigation-toggler *')) && body_.hasClass('navigation-toggle-one')) {
                body_.removeClass('navigation-show');
            }
        });
        $(document).on('click', '*', function (e) {
            if (!$(e.target).is('.header ul.navbar-nav, .header ul.navbar-nav *, .header-toggler, .header-toggler *')) {
                $('.header ul.navbar-nav').removeClass('open');
            }
        });

    }

    ngOnInit(): void {
        this.store
            .pipe(
                select(currentUser),
                map((result: any) => {
                    return result;
                })).subscribe(user => {
                    if (user) {
                        this.user = user;
                        this.imageSrc = this.user.avatar ? `${environment.host}/img/${this.user.avatar}` : null;
                        this.userService.roles(this.user.personal_access_token)
                            .subscribe(security => {

                                this.store.dispatch(new Role({ security: security }));
                                this.roles = security.roles;

                                this.http.get('/assets/json/config.json').subscribe((res: any) => {
                                    this.config = res;
                                    if (this.config.aside) {
                                        for (const menu of this.config.aside.items) {
                                            let hasRole = menu.role.filter(x => {
                                                return this.roles.filter(y => y.slug == x).length > 0 ? true : false;
                                            })
                                              if (hasRole.length > 0) {
                                                menu.active = window.location.href.indexOf(menu.target) !== -1 ? true : false;
                                                let submenu: any[] = [];
                                                for (const sub of menu.submenu) {
                                                    let hasRole = sub.role.filter(x => {
                                                        return this.roles.filter(y => y.slug == x).length > 0 ? true : false;
                                                    });
                                                    if(hasRole.length > 0) {
                                                        if (sub?.has_child) {
                                                            for (const child of sub.submenu) {
                                                                const params = new URLSearchParams(window.location.search);
                                                                if (params.get('filter')) {
                                                                    child.active = params.get('filter') == child.target ? true : false;
                                                                } else {
                                                                    child.active = params.get('status') == child.target ? true : false;
                                                                }
                                                            }
                                                            sub.active = sub.submenu.filter(x => x.active == true).length > 0 ? true : false;
                                                        } else {
                                                            const params = new URLSearchParams(window.location.search);
                                                            if (params.get('filter')) {
                                                                sub.active = params.get('filter') == sub.target ? true : false;
                                                            } else {
                                                                sub.active = params.get('status') == sub.target ? true : false;
                                                            }
                                                        }
                                                        submenu.push(sub);
                                                    }
                                                }
                                                menu.submenu =  submenu;
                                                this.menuList.push(menu);
                                            }
                                        }
                                        
                                    }
                                })

                            });
                    }
                });

        
    }

    get hasAccountOpen() {
        return window.location.href.indexOf('account') != -1 && this.openProfileMenu ? true: false;
    }

    get hasProfileActive() {
        return window.location.href.indexOf('edit-profile') != -1 && this.openProfileMenu ? true : false;
    }

    get hasChangePwdActive() {
        return window.location.href.indexOf('change-password') != -1 && this.openProfileMenu ? true : false;
    }

    ngAfterViewInit(): void {
        this.loadSideMenu();
    }

    openTab(target) {
        this.openProfileMenu = false;
        if (this.menuList.length) {
            
            for (const menu of this.menuList) {
                // /////////////////////////////////////
                if (menu.target == target) {
                    this.openPage(menu.submenu[0]);
                }
                menu.active = menu.target == target ? true : false;
                for (const sub of menu.submenu) {
                    sub.active =false;
                }
            }
        }
        this.cdr.detectChanges();
    }

    openPage(menu) {
        this.openProfileMenu = false;
        if(menu?.has_child) {
            menu.active = true;
        } else {
            this.router.navigateByUrl(menu.page);
            setTimeout(() => {
                if (this.menuList.length) {
                    for (const menu of this.menuList) {
                        if (window.location.href.indexOf(menu.target) !== -1) {
                            menu.active = true;
                            for (const sub of menu.submenu) {
                                if (sub?.has_child) {
                                    for (const child of sub.submenu) {
                                        const params = new URLSearchParams(window.location.search);
                                        if (params.get('filter')) {
                                            child.active = params.get('filter') == child.target ? true : false;
                                        } else {
                                            child.active = params.get('status') == child.target ? true : false;
                                        }
                                    }
                                    sub.active = sub.submenu.filter(x => x.active == true).length > 0 ? true : false;
                                } else {
                                    const params = new URLSearchParams(window.location.search);
                                    if (params.get('filter')) { 
                                        sub.active = params.get('filter') == sub.target ? true : false;
                                    } else {
                                        sub.active = params.get('status') == sub.target ? true : false;
                                    }
                                }
                            }
                        }
                    }
                }
            }, 300);
        }
        
    }

    logout() {
        this.store.dispatch(new Logout());
        this.router.navigateByUrl('/auth/login');
    }

    openProfile(){
        this.openProfileMenu = true;
        for (const menu of this.menuList) {
            menu.active = false;
        }
        this.router.navigateByUrl('officer/account/edit-profile');
    }

}
