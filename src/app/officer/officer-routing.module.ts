import { NotificationLogsComponent } from './notification-logs/notification-logs.component';
import { LocationReportComponent } from './location-report/location-report.component';
import { ShareholderDetailReportComponent } from './shareholder-detail-report/shareholder-detail-report.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InquiryPaymentTransComponent } from './inquiry/inquiry-payment-trans/inquiry-payment-trans.component';
import { TaxIncentivePaymentTransComponent } from './tax-incentive/tax-incentive-payment-trans/tax-incentive-payment-trans.component';
import { LandRightPaymentTransComponent } from './land-right/land-right-payment-trans/land-right-payment-trans.component';
import { EndorsementPaymentTransComponent } from './endorsement/endorsement-payment-trans/endorsement-payment-trans.component';
import { MicPaymentTransComponent } from './mic-permit/mic-payment-trans/mic-payment-trans.component';
import { AdminGuard } from './../core/auth/_guards/admin.gurads';
import { LandRightComponent } from './land-right/land-right.component';
import { TaxIncentiveComponent } from './tax-incentive/tax-incentive.component';
import { EndorsementComponent } from './endorsement/endorsement.component';
import { MicPermitComponent } from './mic-permit/mic-permit.component';
import { InquiryComponent } from './inquiry/inquiry.component';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { OfficerComponent } from './officer.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpLoaderFactory } from '../app.module';
import { ProfileComponent } from '../officer/account/profile/profile.component';
import { ChangePasswordComponent } from './account/change-password/change-password.component';
import { SummaryReportComponent } from './summary-report/summary-report.component';

const routes: Routes = [
    {
        path: '',
        component: OfficerComponent,
        canActivate: [AdminGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },

            { path: 'dashboard', component: DashboardComponent },
            { path: 'dashboard/summary-report', component: SummaryReportComponent },
            { path: 'dashboard/shareholder-report', component: ShareholderDetailReportComponent },
            { path: 'dashboard/location-report', component: LocationReportComponent },

            { path: 'notifications', component: NotificationLogsComponent },

            { path: 'inquiry', component: InquiryComponent },
            { path: 'inquiry-payment', component: InquiryPaymentTransComponent },
            { path: 'inquiry/:form/:id', loadChildren: () => import('./inquiry/inquiry-routing.module').then(m => m.InquiryRoutingModule) },

            { path: 'mic', component: MicPermitComponent },
            { path: 'mic-payment', component: MicPaymentTransComponent },
            { path: 'mic/:form/:id', loadChildren: () => import('./mic-permit/mic-permit-routing.module').then(m => m.MicPermitRoutingModule) },
            { path: 'mic/:form/:id/:mm', loadChildren: () => import('./mic-permit/mic-permit-routing.module').then(m => m.MicPermitRoutingModule) },
            { path: 'mic/:form/:id/:index', loadChildren: () => import('./mic-permit/mic-permit-routing.module').then(m => m.MicPermitRoutingModule) },
            { path: 'mic/:form/:id/:index/:mm', loadChildren: () => import('./mic-permit/mic-permit-routing.module').then(m => m.MicPermitRoutingModule) },
           
            { path: 'endorsement', component: EndorsementComponent },
            { path: 'endorsement-payment', component: EndorsementPaymentTransComponent },
            { path: 'endorsement/:form/:id', loadChildren: () => import('./endorsement/endorsement-routing.module').then(m => m.EndorsementRoutingModule) },
            { path: 'endorsement/:form/:id/:mm', loadChildren: () => import('./endorsement/endorsement-routing.module').then(m => m.EndorsementRoutingModule) },
            { path: 'endorsement/:form/:id/:index', loadChildren: () => import('./endorsement/endorsement-routing.module').then(m => m.EndorsementRoutingModule) },
            { path: 'endorsement/:form/:id/:index/:mm', loadChildren: () => import('./endorsement/endorsement-routing.module').then(m => m.EndorsementRoutingModule) },

            { path: 'tax-incentive', component: TaxIncentiveComponent },
            { path: 'tax-incentive-payment', component: TaxIncentivePaymentTransComponent },
            { path: 'tax-incentive/:form/:id', loadChildren: () => import('./tax-incentive/tax-incentive-routing.module').then(m => m.TaxIncentiveRoutingModule) },
            { path: 'tax-incentive/:form/:id/:mm', loadChildren: () => import('./tax-incentive/tax-incentive-routing.module').then(m => m.TaxIncentiveRoutingModule) },
            
            { path: 'land-right', component: LandRightComponent },
            { path: 'land-right-payment', component: LandRightPaymentTransComponent },
            { path: 'land-right/:form/:id', loadChildren: () => import('./land-right/land-right-routing.module').then(m => m.LandRightRoutingModule) },
            { path: 'land-right/:form/:id/:mm', loadChildren: () => import('./land-right/land-right-routing.module').then(m => m.LandRightRoutingModule) },
            { path: 'land-right/:form/:id/:index', loadChildren: () => import('./land-right/land-right-routing.module').then(m => m.LandRightRoutingModule) },
            { path: 'land-right/:form/:id/:index/:mm', loadChildren: () => import('./land-right/land-right-routing.module').then(m => m.LandRightRoutingModule) },

            { path: 'account/edit-profile', component: ProfileComponent},
            { path: 'account/change-password', component: ChangePasswordComponent},
           
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        AdminGuard,
        TranslateService,
    ],
    exports: [RouterModule]
})
export class OfficerRoutingModule { }
