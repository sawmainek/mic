import { FormLogService } from './../../../services/form-log.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../../services/auth.service';
import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { getStatus } from '../officer';
import { Extractor } from '@angular/compiler';
declare var jQuery: any;
@Component({
    selector: 'app-notification-logs',
    templateUrl: './notification-logs.component.html',
    styleUrls: ['./notification-logs.component.scss']
})
export class NotificationLogsComponent implements OnInit {
    user: any = {};
    datatable: any;
    loading: boolean = true;
    status: string = 'All';
    filter: string = '';
    sector: any;
    sectors: any[] = [];
    dateRange: any;
    displayedColumns: any[] = [];
    sub_status: string = '';
    constructor(
        private authService: AuthService,
        private formLogService: FormLogService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe(params => {
            this.status = params['status'] || 'All';
            this.filter = params['filter'] || '';
            this.authService.getCurrentUser().subscribe(result => {
                this.user = result;
                this.displayedColumns = [
                    {
                        field: 'type',
                        title: 'Application Type',
                        width: 150,
                    },
                    {
                        field: 'updated_at',
                        title: 'Date of Submission',
                        sortable: true,
                        width: 100,
                        template: (row) => {
                            const datePipe = new DatePipe('en');
                            return datePipe.transform(row.updated_at, 'dd-MM-yyyy');
                        }
                    },
                    {
                        field: 'application_no',
                        title: 'Application No',
                        sortable: true,
                        width: 100,
                        template: (row) => {
                            return row?.extra?.application_no || '';
                        }
                    },
                    {
                        field: 'user',
                        title: 'Submitted User',
                        sortable: true,
                        width: 150,
                        template: (row) => {
                            return row?.user?.name;
                        }
                    },
                    {
                        field: 'status',
                        title: 'Status',
                        sortable: true,
                        width: 100,
                    },
                    // {
                    //     field: 'seen',
                    //     title: 'Seen',
                    //     sortable: true,
                    //     width: 150,
                    //     template: (row) => {
                    //         return row.seen ? 'Mark as read' : 'Mark as unread';
                    //     }
                    // }
                ];
                this.initDatatable();
            }, err => {
                console.log(err);
            });
        });
    }

    ngAfterViewInit(): void {
        this.loadDatePicker();
    }

    loadDatePicker() {
        const $this = this;
        jQuery('.form-daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-light-primary',
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            }
        });
        jQuery('.form-daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            $this.dateRange = {};
            $this.dateRange['start_date'] = picker.startDate.format('YYYY-MM-DD');
            $this.dateRange['end_date'] = picker.endDate.format('YYYY-MM-DD');
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
        jQuery('.form-daterangepicker').on('cancel.daterangepicker', function (ev, picker) {
            $this.dateRange = null;
            $this.datatable.setDataSourceParam("search", $this.buildSearch())
            $this.datatable.load();
            jQuery(this).val('');
        });
    }

    onChangeSector() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    onChangeStatus() {
        this.datatable.setDataSourceParam("search", this.buildSearch())
        this.datatable.load();
    }

    buildSearch() {
        let search = ``;
        if (this.status == 'Incoming') {
            search = this.filter ? `type:equal:${this.filter}|` : ``;
            search += `incoming:equal:true`;
        } else {
            search = this.status != 'All' ? `status:equal:${this.status}` : ``;
        }
        if (this.dateRange) {
            search += `|created_at:bigger_equal:${this.dateRange.start_date}|created_at:smaller_equal:${this.dateRange.end_date}`
        }
        return search;
    }

    initDatatable() {
        this.user.personal_access_token
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };
        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/formlog',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: this.buildSearch(),
                            query: '',
                            datatable: true
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
                callback: (row, data, index) => {
                    row.css("background-color", data.seen ? "#ffffff" : '#f5f5f5')
                }
            },
            columns: this.displayedColumns,
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            },
            search: {
                input: jQuery('#notification-log-search'),
            }
        };
        if (this.datatable) { jQuery('.kt-datatable-notification-log').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-notification-log').KTDatatable(options);
        jQuery('.kt-datatable-notification-log').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;

            jQuery('.onclick').on('click', function () {
                setTimeout(() => {
                    const index = jQuery(this).data('row');
                    const data = ctx.datatable.getDataSet();
                    ctx.gotoFormDetail(data[index]);
                }, 300);
            });
        });
    }

    gotoFormDetail(log) {
        log.seen = true;
        this.formLogService.update(log.id, { seen: true }).subscribe();
        
        if (log.type == 'Inquiry ( Form 1 )' && log?.extra?.form_id) {
            this.router.navigate(['/officer/inquiry/form', log?.extra?.form_id]);
        }
        if (log.type == 'MIC-Permit' && log?.extra?.form_id) {
            this.router.navigate(['/officer/mic/choose-language', log?.extra?.form_id]);
        }
        if (log.type == 'Endorsement' && log?.extra?.form_id) {
            this.router.navigate(['/officer/endorsement/form', log?.extra?.form_id]);
        }
        if (log.type == 'Tax Incentive' && log?.extra?.form_id) {
            this.router.navigate(['/officer/tax-incentive/choose-language', log?.extra?.form_id]);
        }
        if (log.type == 'Land Right' && log?.extra?.form_id) {
            this.router.navigate(['/officer/land-right/landright-choose-investment', log?.extra?.form_id]);
        }
    }

}

