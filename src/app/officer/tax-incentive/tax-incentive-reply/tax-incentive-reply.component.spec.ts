import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveReplyComponent } from './tax-incentive-reply.component';

describe('TaxIncentiveReplyComponent', () => {
  let component: TaxIncentiveReplyComponent;
  let fixture: ComponentFixture<TaxIncentiveReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveReplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
