import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentivePaymentTransComponent } from './tax-incentive-payment-trans.component';

describe('TaxIncentivePaymentTransComponent', () => {
  let component: TaxIncentivePaymentTransComponent;
  let fixture: ComponentFixture<TaxIncentivePaymentTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentivePaymentTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentivePaymentTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
