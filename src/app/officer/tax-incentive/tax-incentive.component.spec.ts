import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveComponent } from './tax-incentive.component';

describe('TaxIncentiveComponent', () => {
  let component: TaxIncentiveComponent;
  let fixture: ComponentFixture<TaxIncentiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
