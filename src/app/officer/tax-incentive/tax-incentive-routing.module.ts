import { TaxIncentiveDetailComponent } from './tax-incentive-detail/tax-incentive-detail.component';
import { TaxIncentiveReplyComponent } from './tax-incentive-reply/tax-incentive-reply.component';
import { AuthGuard } from './../../core/auth/_guards/auth.guards';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: TaxIncentiveReplyComponent,
        children: [
            { path: '', component: TaxIncentiveDetailComponent }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        AuthGuard
    ],
    exports: [RouterModule]
})
export class TaxIncentiveRoutingModule { }
