import { QuillModule } from 'ngx-quill';
import { TaxIncentiveModule as PageTaxIncentiveModule} from './../../pages/tax-incentive/tax-incentive.module';
import { TaxIncentiveReplyComponent } from './tax-incentive-reply/tax-incentive-reply.component';
import { GlobalModule } from './../../pages/global.module';
import { TaxIncentiveComponent } from './tax-incentive.component';
import { TaxIncentiveRoutingModule } from './tax-incentive-routing.module';
import { HttpLoaderFactory } from 'src/app/app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomComponentModule } from 'src/app/custom-component/custom-component.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ErrorTailorModule } from '@ngneat/error-tailor';
import { TaxIncentiveDetailComponent } from './tax-incentive-detail/tax-incentive-detail.component';
import { TaxIncentivePaymentTransComponent } from './tax-incentive-payment-trans/tax-incentive-payment-trans.component';
import { TaxHistoryComponent } from './tax-history/tax-history.component';


@NgModule({
    declarations: [
        TaxIncentiveComponent,
        TaxIncentiveDetailComponent,
        TaxIncentiveReplyComponent,
        TaxIncentivePaymentTransComponent,
        TaxHistoryComponent
    ], imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        GlobalModule,
        QuillModule.forRoot(),
        PageTaxIncentiveModule,
        TaxIncentiveRoutingModule,
        CustomComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            // closeButton: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ErrorTailorModule.forRoot({
            errors: {
                useValue: {
                    required: 'This field is required',
                    minlength: ({ requiredLength, actualLength }) =>
                        `Expect ${requiredLength} but got ${actualLength}`,
                    invalidAddress: error => `Address isn't valid`,
                    email: 'The email is invalid.',
                    pattern: 'Only number allowed.'
                }
            }
        })
    ], providers: [
        TranslateService,
    ],
})
export class TaxIncentiveModule { }
