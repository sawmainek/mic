import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxIncentiveDetailComponent } from './tax-incentive-detail.component';

describe('TaxIncentiveDetailComponent', () => {
  let component: TaxIncentiveDetailComponent;
  let fixture: ComponentFixture<TaxIncentiveDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxIncentiveDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxIncentiveDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
