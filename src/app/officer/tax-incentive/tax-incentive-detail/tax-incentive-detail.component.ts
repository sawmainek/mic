import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tax-incentive-detail',
  templateUrl: './tax-incentive-detail.component.html',
  styleUrls: ['./tax-incentive-detail.component.scss']
})
export class TaxIncentiveDetailComponent implements OnInit {

    id: any;
    form: any;
    constructor(
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(paramMap => {
            this.id = paramMap.get('id');
            this.form = paramMap.get('form');

        });
    }

}
