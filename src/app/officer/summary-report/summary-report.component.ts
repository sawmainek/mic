import { ISICService } from './../../../services/isic.service';
import { LocationReportService } from './../../../services/location-report.service';
import { environment } from './../../../environments/environment.prod';
import { AuthService } from './../../../services/auth.service';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
declare var jQuery: any;

@Component({
    selector: 'app-summary-report',
    templateUrl: './summary-report.component.html',
    styleUrls: ['./summary-report.component.scss']
})
export class SummaryReportComponent implements OnInit {
    loading: boolean = true;
    reports: any;
    user: any;
    datatable: any;
    isicSectors: any[] = [];
    displayedColumns: any = [
        {
            field: 'application_no',
            title: 'Permit or Endorsement Application Number',
            type: 'number',
            width: 100,
        },
        {
            field: 'company_name',
            title: 'Name of Company',
            sortable: false,
            width: 200
        },
        {
            field: 'investment_form',
            title: 'Form of Investment',
            sortable: false,
            width: 200
        },
        {
            field: 'authority',
            title: 'Authority',
            sortable: false,
            width: 200
        },
        {
            field: 'permit_endo_no',
            title: 'Permit or Endorsement Number',
            sortable: false,
            width: 100
        },
        {
            field: 'issued_date',
            title: 'Issued Date',
            sortable: false,
            width: 100,
            template: (row) => {
                const datePipe = new DatePipe('en');
                return datePipe.transform(row.issued_date, 'dd-MM-yyyy');
            }
        },
        {
            field: 'project_duration',
            title: 'Project Duration',
            sortable: false,
            width: 100,
        },
        {
            field: 'commercial_operation_date',
            title: 'Date of Commercial Operation Period',
            width: 100,
            sortable: false,
            template: (row) => {
                const datePipe = new DatePipe('en');
                return datePipe.transform(row.commercial_operation_date, 'dd-MM-yyyy');
            }
        },
        {
            field: 'expected_investment_usd',
            title: 'Total Amount of Investment in USD',
            width: 100,
            sortable: false,
            template: (row) => {
                return this.formatNumber(row?.expected_investment_usd || 0);
            }
        },
        {
            field: 'expected_investment_kyat',
            title: 'Total Amount of Investment in MMK',
            width: 100,
            sortable: false,
            template: (row) => {
                return this.formatNumber(row?.expected_investment_kyat || 0);
            }
        },
        {
            field: 'foreign_captial_brought_in_usd',
            title: 'Amount of Foreign captial brought in USD',
            width: 100,
            sortable: false,
            template: (row) => {
                return this.formatNumber(row?.foreign_captial_brought_in_usd || 0);
            }
        },
        {
            field: 'loan_repay_in_usd',
            title: 'Foreign Loan Amount (USD)',
            width: 100,
            sortable: false,
            template: (row) => {
                return this.formatNumber(row?.loan_repay_in_usd || 0);
            }
        },
        {
            field: 'loan_repay_in_kyat',
            title: 'Domestic Loan Amount (MMK)',
            width: 100,
            sortable: false,
            template: (row) => {
                return this.formatNumber(row?.loan_repay_in_kyat || 0);
            }
        },
        {
            field: 'business_type',
            title: 'Type of Business',
            width: 200,
            sortable: false,
        },
        {
            field: 'sector',
            title: 'Sector',
            sortable: false,
            width: 200,
            template: (row) => {
                let classes = this.isicSectors.filter(x => {
                    let types: any[] = row.business_type || [];
                    return types.includes(x.name) ? true : false;
                })[0] || {};
                return classes.sector || '';
            }
        },
        {
            field: 'workers',
            title: 'Number of foreign workers',
            width: 100,
            sortable: false,
            template: (row) => {
                let total = 0;
                (row?.emp_max || []).forEach(y => {
                    total += Number(y.foreign_nationals_qty);
                });
                return total;
            }
        },
        {
            field: 'emp_max',
            title: 'Number of local workers',
            width: 100,
            sortable: false,
            template: (row) => {
                let total = 0;
                (row?.emp_max || []).forEach(y => {
                    total += Number(y.myanmar_citizens_qty);
                });
                return total;
            }
        },
        /*  {
             field: 'invest_location_1',
             title: 'Investment Location (Address)',
             width: 200,
             template: (row) => {
                 let address = '';
                 if (row.invest_location) {
                     address = '';
                 }
                 return address;
             }
         }, */
        {
            field: 'invest_location_2',
            title: 'Investment Location (Township)',
            width: 100,
            template: (row) => {
                let address = '';
                if (row.invest_location) {
                    address = row.invest_location.zone_1.zone_1_list[0].township || '';
                }
                return address;
            }
        },
        {
            field: 'invest_location_3',
            title: 'Investment Location (Region)',
            width: 100,
            template: (row) => {
                let address = '';
                if (row.invest_location) {
                    address = row.invest_location.zone_1.zone_1_list[0].state || '';
                }
                return address;
            }
        }
    ];
    constructor(
        private authService: AuthService,
        private isicService: ISICService,
        private summaryReport: LocationReportService
    ) { }

    ngOnInit(): void {
        this.authService.getCurrentUser().subscribe(result => {
            this.user = result;
            this.isicService.getSectors().subscribe(results => {
                this.isicSectors = results;
                this.initDatatable();
            })
        }, err => {
            console.log(err);
        });
    }

    initDatatable() {
        let httpHeaders = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.personal_access_token
        };
        let options: any = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: environment.host + '/api/summaryreport',
                        method: 'GET',
                        headers: httpHeaders,
                        params: {
                            search: '',//this.buildSearch(),
                            query: '',
                            datatable: true
                        }
                    },
                },
                saveState: {
                    cookie: false
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            rows: {
                autoHide: false,
            },
            columns: this.displayedColumns,
            sortable: false,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 50, 100],
                    }
                }
            },
            search: {
                //input: jQuery('#summary-report-search'),
            }
        };
        // if (this.datatable) { jQuery('.kt-datatable-summary-report').KTDatatable('destroy'); }
        this.datatable = jQuery('.kt-datatable-summary-report').KTDatatable(options);
        jQuery('.kt-datatable-summary-report').on('datatable-on-layout-updated', () => {
            let ctx: any = this;
            ctx.loading = false;
            jQuery('.onclick').on('click', function () {
                // setTimeout(() => {
                //     const index = jQuery(this).data('row');
                //     const data = ctx.datatable.getDataSet();
                //     ctx.goto(data[index].tax_incentive_id);
                // }, 300);
            });
        });
    }

    formatNumber(num) {
        let digit = Math.round(Number(num));
        return digit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    download(type) {
        let data = [];
        this.summaryReport.get({ rows: 99999 }).subscribe(result => {

            const datePipe = new DatePipe('en');
            result.map((x, idx) => {
                // let flat = this.flattenObject(x);
                // this.deletePorperty(flat);
                let classes = this.isicSectors.filter(x => {
                    let types: any[] = x.business_type || [];
                    return types.includes(x.name) ? true : false;
                })[0] || {};
                data.push({
                    'Permit or Endorsement Application Number': x.application_no || '',
                    'Name of Company': x.company_name || '',
                    'Form of Investment': x.investment_form || '',
                    'Authority': x.authority || '',
                    'Permit or Endorsement Number': x.permit_endo_no || '',
                    'Issued Date': x.issued_date ? datePipe.transform(x.issued_date, 'dd-MM-yyyy') : '',
                    'Project Duration': x.project_duration || '',
                    'Date of Commercial Operation Period': x.commercial_operation_date || '',
                    'Total Amount of Investment in USD': x.expected_investment_usd || '',
                    'Total Amount of Investment in MMK': x.expected_investment_kyat || '',
                    'Amount of Foreign captial brought in USD': x.foreign_captial_brought_in_usd || '',
                    'Foreign Loan Amount (USD)': x.loan_repay_in_usd || '',
                    'Domestic Loan Amount (MMK)': x.loan_repay_in_kyat || '',
                    'Type of Business': x.business_type || '',
                    'Sector': classes.sector || '',
                    'Number of foreign workers': x.workers || '',
                    'Number of local workers': x.emp_max || '',
                    'Investment Location (Township)': x.invest_location?.zone_1?.zone_1_list[0].township || '',
                    'Investment Location (Region)': x.invest_location?.zone_1?.zone_1_list[0].state || '',
                })
            });
            var csv = this.arrayToCSV(data);
            this.exportCSVFile(undefined, csv, 'summary-report');
        });
    }

    flattenObject(obj) {
        var root = {};
        (function tree(obj, index) {
            var suffix = toString.call(obj) == '[object Array]' ? ']' : '';
            for (var key in obj) {
                if (!obj.hasOwnProperty(key)) continue;
                root[index + key + suffix] = obj[key];
                if (toString.call(obj[key]) == '[object Array]') tree(obj[key], index + key + suffix + '[');
                if (toString.call(obj[key]) == '[object Object]') tree(obj[key], index + key + suffix + '.');
            }
        })(obj, '');
        return root;
    }

    deletePorperty(obj: any) {
        for (let i in obj) {
            if (Array.isArray(obj[i]) || (obj[i]) instanceof Object) {
                delete obj[i];
            }
        }
    }

    arrayToCSV(objArray) {
        const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
        var sizes = array.map(x => Object.keys(x).length);
        var maxIdx = sizes.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
        let str = `${Object.keys(array[maxIdx]).map(value => `"${value}"`).join(',')}` + '\r\n';
        for (let i in array[maxIdx]) {
            array.map(x => {
                if (!x.hasOwnProperty(i)) {
                    x[i] = '';
                }
            });
        }
        return array.reduce((str, next) => {
            str += `${Object.values(next).map(value => `"${value}"`).join(',')}` + '\r\n';
            return str;
        }, str);
    }

    exportCSVFile(headers, csv, fileTitle) {

        var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

        var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, exportedFilenmae);
        } else {
            var link = document.createElement('a');
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute('href', url);
                link.setAttribute('download', exportedFilenmae);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }
}
