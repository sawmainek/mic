import { EndorsementInvestmentByZone3Service } from './../../../services/endorsement-investment-by-zone3.service';
import { EndorsementInvestmentByZone2Service } from './../../../services/endorsement-investment-by-zone2.service';
import { EndorsementInvestmentByZone1Service } from './../../../services/endorsement-investment-by-zone1.service';
import { MicInvestmentByZone3Service } from './../../../services/mic-investment-by-zone3.service';
import { MicInvestmentByZone2Service } from './../../../services/mic-investment-by-zone2.service';
import { MicInvestmentByZone1Service } from './../../../services/mic-investment-by-zone1.service';
import { EndorsementInvestmentByFormService } from './../../../services/endorsement-investment-by-form.service';
import { EndorsementInvestmentBySectorService } from './../../../services/endorsement-investment-by-sector.service';
import { MicInvestmentByFormService } from './../../../services/mic-investment-by-form.service';
import { MicInvestmentBySectorService } from './../../../services/mic-investment-by-sector.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { SummaryTotalReportService } from 'src/services/summary-total-report.service';
import { Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, SingleDataSet } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    inquiry: any = [];
    mic: any = [];
    endo: any = [];
    tax: any = [];
    land: any = [];
    summary_totals: any;
    loading: boolean = true;
    paymentRecords: number = 0;
    public pieChartEndoInvestbySectorLabels: Label[] = [];
    public pieChartEndoInvestbySectorData: SingleDataSet = [];

    public pieChartMicInvestbySectorLabels: Label[] = [];
    public pieChartMicInvestbySectorData: SingleDataSet = [];

    public pieChartEndoInvestbyFormLabels: Label[] = [];
    public pieChartEndoInvestbyFormData: SingleDataSet = [];

    public pieChartMicInvestbyFormLabels: Label[] = [];
    public pieChartMicInvestbyFormData: SingleDataSet = [];

    public pieChartType: ChartType = 'pie';
    public pieChartLegend = true;
    public pieChartPlugins = [];
    public pieChartOptions: ChartOptions = {
        legend: { 
            position: 'right', 
            labels: {
                usePointStyle: true,
            } 
        },
        tooltips: {
            callbacks: {
                label: (tooltipItem, chart) => {
                    console.log(chart, tooltipItem);
                    
                    var datasetLabel = chart.labels[tooltipItem.index] || '';
                    return datasetLabel + ': $ ' + this.formatNumber(chart.datasets[0].data[tooltipItem.index]);
                }
            }
        }
    };

    public barChartOptions: ChartOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: (value, index, values)=> {
                        return '$ ' + this.formatNumber(value);
                    }
                }
            }]
        },
        tooltips: {
            callbacks: {
                label: (tooltipItem, chart) => {
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': $ ' + this.formatNumber(tooltipItem.yLabel);
                }
            }
        }
    };

    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [];
    
    public barChartMICZone1Labels: Label[] = [];
    public barChartMICZone1Data: ChartDataSets[] = [];

    public barChartEndoZone1Labels: Label[] = [];
    public barChartEndoZone1Data: ChartDataSets[] = [];


    constructor(
        private summaryTotalReportService: SummaryTotalReportService,
        private micInvestbySector: MicInvestmentBySectorService,
        private micInvestbyForm: MicInvestmentByFormService,
        private endoInvestSector: EndorsementInvestmentBySectorService,
        private endoInvestForm: EndorsementInvestmentByFormService,
        private micInvestbyZone1: MicInvestmentByZone1Service,
        private micInvestbyZone2: MicInvestmentByZone2Service,
        private micInvestbyZone3: MicInvestmentByZone3Service,
        private endoInvestbyZone1: EndorsementInvestmentByZone1Service,
        private endoInvestbyZone2: EndorsementInvestmentByZone2Service,
        private endoInvestbyZone3: EndorsementInvestmentByZone3Service,
        private router: Router,
    ) {
        monkeyPatchChartJsTooltip();
        monkeyPatchChartJsLegend();
     }

    ngOnInit(): void {
        this.loading = true;
        this.getSummaryTotal();
        forkJoin([
            this.micInvestbySector.get(),
            this.micInvestbyForm.get(),
            this.endoInvestSector.get(),
            this.endoInvestForm.get(),
            this.micInvestbyZone1.get({rows: 99}),
            this.micInvestbyZone2.get({rows: 99}),
            this.micInvestbyZone3.get({rows: 99}),
            this.endoInvestbyZone1.get({rows: 99}),
            this.endoInvestbyZone2.get({rows: 99}),
            this.endoInvestbyZone3.get({rows: 99}),
        ]).subscribe(results => {
            console.log(results);
            this.loading = false;

            // MIC Investment
            const micInvestBySector = results[0].filter(x => x.sector != null);
            micInvestBySector.forEach(x => {
                this.pieChartMicInvestbySectorLabels.push(x.sector);
                this.pieChartMicInvestbySectorData.push(Math.round(x.investment_value_usd || 0));
            });
            this.pieChartMicInvestbySectorLabels.sort((a, b) => (a > b) ? 1 : -1);

            const micInvestByForm = results[1].filter(x => x.investment_form != null);
            micInvestByForm.forEach(x => {
                this.pieChartMicInvestbyFormLabels.push(x.investment_form);
                this.pieChartMicInvestbyFormData.push(Math.round(x.foreign_investment_amount || 0));
            });
            this.pieChartMicInvestbyFormLabels.sort((a, b) => (a > b) ? 1 : -1);

            // Endo Investment
            const endoInvestBySector = results[2].filter(x => x.sector != null);
            endoInvestBySector.forEach(x => {
                this.pieChartEndoInvestbySectorLabels.push(x.sector);
                this.pieChartEndoInvestbySectorData.push(Math.round(x.investment_value_usd || 0));
            });
            this.pieChartEndoInvestbySectorLabels.sort((a, b) => (a > b) ? 1 : -1);

            const endoInvestByForm = results[3].filter(x => x.investment_form != null);
            endoInvestByForm.forEach(x => {
                this.pieChartEndoInvestbyFormLabels.push(x.investment_form);
                this.pieChartEndoInvestbyFormData.push(Math.round(x.foreign_investment_amount || 0));
            });
            this.pieChartMicInvestbyFormLabels.sort((a, b) => (a > b) ? 1 : -1);

            // MIC Permit
            const micInvestByZone1 = results[4].filter(x => x.state != null && x.state != 'null');
            this.barChartMICZone1Data[0] = { label: 'Zone 1', data: [] };
            micInvestByZone1.sort((a, b) => (a.state > b.state) ? 1 : -1);
            micInvestByZone1.forEach(x => {
                this.barChartMICZone1Labels.push(x.state);
                this.barChartMICZone1Data[0].data.push(Math.round(x.foreign_investment_amount || 0));
            })
            const micInvestByZone2 = results[5].filter(x => x.state != null && x.state != 'null');
            this.barChartMICZone1Data[1] = { label: 'Zone 2', data: [] };
            micInvestByZone2.sort((a, b) => (a.state > b.state) ? 1 : -1);
            micInvestByZone2.forEach(x => {
                if (this.barChartMICZone1Labels.filter(state => state == x.state).length < 0) {
                    this.barChartMICZone1Labels.push(x.state);
                }
                this.barChartMICZone1Data[1].data.push(Math.round(x.foreign_investment_amount || 0));
            })
            const micInvestByZone3 = results[6].filter(x => x.state != null && x.state != 'null');
            this.barChartMICZone1Data[2] = { label: 'Zone 3', data: [] };
            micInvestByZone3.sort((a, b) => (a.state > b.state) ? 1 : -1);
            micInvestByZone3.forEach(x => {
                if (this.barChartMICZone1Labels.filter(state => state == x.state).length < 0) {
                    this.barChartMICZone1Labels.push(x.state);
                }
                this.barChartMICZone1Data[2].data.push(Math.round(x.foreign_investment_amount || 0));
            })
            
            // Endorsement
            const endoInvestByZone1 = results[7].filter(x => x.state != null && x.state != 'null');
            this.barChartEndoZone1Data[0] = { label: 'Zone 1', data: [] };
            endoInvestByZone1.sort((a, b) => (a.state > b.state) ? 1 : -1);
            endoInvestByZone1.forEach(x => {
                this.barChartEndoZone1Labels.push(x.state);
                this.barChartEndoZone1Data[0].data.push(Math.round(x.foreign_investment_amount || 0));
            })
            const endoInvestByZone2 = results[8].filter(x => x.state != null && x.state != 'null');
            this.barChartEndoZone1Data[1] = { label: 'Zone 2', data: [] };
            endoInvestByZone2.sort((a, b) => (a.state > b.state) ? 1 : -1);
            endoInvestByZone2.forEach(x => {
                if (this.barChartEndoZone1Labels.filter(state => state == x.state).length < 0) {
                    this.barChartEndoZone1Labels.push(x.state);
                }
                this.barChartEndoZone1Data[1].data.push(Math.round(x.foreign_investment_amount || 0));
            })
            const endoInvestByZone3 = results[9].filter(x => x.state != null && x.state != 'null');
            this.barChartEndoZone1Data[2] = { label: 'Zone 3', data: [] };
            endoInvestByZone3.sort((a, b) => (a.state > b.state) ? 1 : -1);
            endoInvestByZone3.forEach(x => {
                if (this.barChartEndoZone1Labels.filter(state => state == x.state).length < 0) {
                    this.barChartEndoZone1Labels.push(x.state);
                }
                this.barChartEndoZone1Data[2].data.push(Math.round(x.foreign_investment_amount || 0));
            })
           
        })
    }
  
    getSummaryTotal() {
        const params: any = {
            page: 1,
            rows: 99999,
            secure: true,
        };
        this.summaryTotalReportService.get(params).subscribe((results: any) => {
            results = results || [];
            this.summary_totals = [{}, {}, {}, {}, {}, {}];

            results.forEach(x => {
                if (x.type == 'Inquiry') {
                    this.summary_totals[0] = { 'id': x.id, 'type': 'Total Inquiry', 'css': 'card total0', url: '/officer/inquiry?status=New' };
                }
                if (x.type == 'MIC') {
                    this.summary_totals[1] = { 'id': x.id, 'type': 'Total Permit', 'css': 'card total1', url: '/officer/mic?status=New'};
                }
                if (x.type == 'Endorsement') {
                    this.summary_totals[2] = { 'id': x.id, 'type': 'Total Endorsement', 'css': 'card total2', url: '/officer/endorsement?status=New' };
                }
                if (x.type == 'Tax Incentive') {
                    this.summary_totals[3] = { 'id': x.id, 'type': 'Total Tax Incentive', 'css': 'card total3', url: '/officer/tax-incentive?status=New' };
                }
                if (x.type == 'Land Right') {
                    this.summary_totals[4] = { 'id': x.id, 'type': 'Total Land Right', 'css': 'card total4', url: '/officer/land-right?status=New' };
                }
                if (x.type == 'Investor') {
                    this.summary_totals[5] = { 'id': x.id, 'type': 'Total Investors', 'css': 'card total5' };
                }
            });

        });
    }

    formatNumber(num) {
        let digit = Math.round(Number(num));
        return digit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    openPage(page) {
        if(page) {
            this.router.navigateByUrl(page);
        }
    }
}
