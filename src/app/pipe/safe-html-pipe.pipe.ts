import DOMPurify from 'dompurify';
import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'safeHtmlPipe'
})
export class SafeHtmlPipePipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) { }
    transform(value: string, ...args: unknown[]): unknown {
        value = value.replace(/<p><br><\/p>/g, '');
        const sanitizedContent = DOMPurify.sanitize(value);
        return this.sanitizer.bypassSecurityTrustHtml(sanitizedContent);
    }

}
