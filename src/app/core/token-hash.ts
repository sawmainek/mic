import * as crypto from 'crypto-js';
import { Buffer } from 'buffer';
export class TokenHash {

    static hash(token: any, key: any) {
        let base64_string = Buffer.from(token.toString()).toString("base64");
        let hashString = crypto.SHA256(base64_string);
        hashString = crypto.enc.Base64.stringify(hashString);
        return hashString;
    }

    static encrypt(token: any, key: any): any {
        let ciphertext = crypto.AES.encrypt(token, key).toString();
        let base64_string = Buffer.from(ciphertext.toString()).toString("base64");
        return base64_string;
    }

    static decrypt(encodedString: any, key: any): any {
        if(encodedString!=null){
            try {
                let text = Buffer.from(encodedString, 'base64').toString('ascii');
                let bytes = crypto.AES.decrypt(text, key);
                let decryptedData = bytes.toString(crypto.enc.Utf8);
                return decryptedData;
            } catch (error) {
                
            }
        }
        return null;
    }

    static getToken(): any {
        return "admin@mail.com";
    }

    static getKey(): any {
        return "123456";
    }
}
