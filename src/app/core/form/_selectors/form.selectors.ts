// NGRX
import { createSelector } from '@ngrx/store';

export const selectFormState = state => state;


export const currentForm = createSelector(
    selectFormState,
    state => state.form
);

export const formProgress = createSelector(
    selectFormState,
    state => state.form.progress
);

export const comments = createSelector(
    selectFormState,
    state => state.form.comments
);

export const submitted = createSelector(
    selectFormState,
    state => state.form.submit
);

export const loaded = createSelector(
    selectFormState,
    state => state.form.loaded
);

export const readyToSubmit = createSelector(
    selectFormState,
    state => state.form.submission
);
