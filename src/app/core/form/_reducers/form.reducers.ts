import { FormActions, FormActionTypes } from './../_actions/form.actions';
export interface FormState {
    id?: number;
    type?: string;
    form?: any;
    progress?: any[];
    page?: string,
    comments?: any[],
    submit?: boolean,
    loaded?: boolean,
    submission: string
}

export const initialFormState: FormState = {
    id: null,
    type: null,
    form: null,
    progress: [],
    page: null,
    comments: [],
    submit: false,
    loaded: true,
    submission: null
};

export function formReducer(state = initialFormState, action: FormActions): FormState {
    switch (action.type) {
        case FormActionTypes.BaseFormData: {
            const _id: number = action.payload.id;
            const _type: string = action.payload.type;
            const _page: string = action.payload.page;
            const _comments: any[] = action.payload.comments;
            const _submission: string = action.payload.submission;
            return {
                ...state,
                id: _id,
                type: _type,
                page: _page,
                comments: _comments,
                submission: _submission
            };
        }
        case FormActionTypes.CurrentForm: {
            const _form: any = action.payload.form;
            return {
                ...state,
                form: _form
            };
        }
        case FormActionTypes.FormProgress: {
            const _progress: any = action.payload.progress;
            return {
                ...state,
                progress: _progress
            };
        }
        case FormActionTypes.Comments: {
            const _comments: any[] = action.payload.comments;
            return {
                ...state,
                comments: _comments
            };
        }
        case FormActionTypes.Submit: {
            const _submit: boolean = action.payload.submit;
            return {
                ...state,
                submit: _submit
            };
        }
        case FormActionTypes.Loaded: {
            const _loaded: boolean = action.payload.loaded;
            return {
                ...state,
                submit: _loaded
            };
        }
        case FormActionTypes.ReadyToSubmit: {
            const _submission: string = action.payload.submission;
            return {
                ...state,
                submission: _submission
            };
        }
        default:
            return state;
    }
}
