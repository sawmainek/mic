import { Action } from '@ngrx/store';

export enum FormActionTypes {
    BaseFormData = '[BaseFormData] Action',
    CurrentForm = '[CurrentForm] Action',
    FormProgress = '[FormProgress] Action',
    Comments = '[Comments] Action',
    Submit = '[Submit] Action',
    Loaded = '[Loaded] Action',
    ReadyToSubmit = '[ReadyToSubmit] Action',
}

export class BaseFormData implements Action {
    readonly type = FormActionTypes.BaseFormData;
    constructor(public payload: {
        id?: number, type?: string, page?: string, comments?: any[], submission?: string
    }) { }
}

export class CurrentForm implements Action {
    readonly type = FormActionTypes.CurrentForm;
    constructor(public payload: {
        form?: any
    }) { }
}

export class FormProgress implements Action {
    readonly type = FormActionTypes.FormProgress;
    constructor(public payload: {
        progress?: any
    }) { }
}

export class Comments implements Action {
    readonly type = FormActionTypes.Comments;
    constructor(public payload: {
        comments?: any[]
    }) { }
}

export class Submit implements Action {
    readonly type = FormActionTypes.Submit;
    constructor(public payload: {
        submit?: boolean
    }) { }
}

export class Loaded implements Action {
    readonly type = FormActionTypes.Loaded;
    constructor(public payload: {
        loaded?: boolean
    }) { }
}

export class ReadyToSubmit implements Action {
    readonly type = FormActionTypes.ReadyToSubmit;
    constructor(public payload: {
        submission?: string
    }) { }
}

export type FormActions = BaseFormData | CurrentForm | FormProgress | Comments | Submit | Loaded | ReadyToSubmit;
