import { BaseComponent } from './../../../pages/base-page/base-component';
// Angular
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
// RxJS
import { Observable, of, interval } from 'rxjs';
import { tap, map, delay } from 'rxjs/operators';
// NGRX
import { select, Store } from '@ngrx/store';
// Auth reducers and selectors
import { AppState } from '../../../core/reducers/';
import { isLoggedIn } from '../_selectors/auth.selectors';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private store: Store<AppState>, private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        let roles: any[] = [];
        return this.store
            .pipe(
                delay(0),
                select(isLoggedIn),
                map((result: any) => {
                    roles = result[2] && result[2].roles || [];
                    return result[0] == true && result[1] == true;
                }),
                tap(x => {
                    if (x) {
                        if (state.url == '/pages/dashboard') {
                            if (roles.filter(x => x.slug == 'view-mic' || x.slug == 'view-pat' || x.slug == 'view-region' || x.slug == 'checking' || x.slug == 'approval').length > 0) {
                                this.router.navigateByUrl('/officer');
                            }
                        }
                    } else {
                        this.router.navigateByUrl('/auth');
                    }

                })
            );
    }
}
