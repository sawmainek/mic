// Actions
import { AuthActions, AuthActionTypes } from '../_actions/auth.actions';


export interface AuthState {
    loggedIn: boolean;
    verify: boolean,
    noti: number,
    user_id: number,
    user: any,
    access_token: any,
    security: any;
}

export const initialAuthState: AuthState = {
    loggedIn: false,
    verify: false,
    noti: 0,
    user_id: -1,
    user: null,
    access_token: null,
    security: null
};

export function authReducer(state = initialAuthState, action: AuthActions): AuthState {
    switch (action.type) {
        case AuthActionTypes.Login: {
            const _user_id: number = action.payload.user_id;
            const _vertify: boolean = action.payload.isVertify;
            const _user: any = action.payload.user;
            const _loggedIn: boolean = action.payload.loggedIn;
            const _security: any = action.payload.security;
            return {
                ...state,
                loggedIn: _loggedIn,
                verify: _vertify,
                user_id: _user_id,
                user: _user,
                security: _security,
            };
        }

        case AuthActionTypes.Role: {
            const _security: any = action.payload.security;
            return {
                ...state,
                security: _security,
            };
        }

      case AuthActionTypes.AccessToken: {
        const _token: any = action.payload.access_token;
        return {
          ...state,
          access_token: _token,
        };
      }

        case AuthActionTypes.Register: {
            const _user: any = action.payload.user;
            return {
                ...state,
                loggedIn: true,
                verify: false,
                user_id: _user.id,
                user: _user,
            };
        }

        case AuthActionTypes.UserVerify: {
            const isVertify: boolean = action.payload.is_verify;
            return {
                ...state,
                verify: isVertify
            };
        }

        case AuthActionTypes.Notify: {
            const noti: number = action.payload.noti;
            return {
                ...state,
                noti: noti
            };
        }

        case AuthActionTypes.UpdateUser: {
            const _user: any = action.payload.user;
            return {
                ...state,
                user: _user
            };
        }

        case AuthActionTypes.Logout:
            return initialAuthState;

        default:
            return state;
    }
}
