import { UserVerify, UpdateUser, Notify, AccessToken } from './../_actions/auth.actions';
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// RxJS
import { tap } from 'rxjs/operators';
import { of, Observable, defer } from 'rxjs';
// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
// Auth actions
import { AuthActionTypes, Login, Logout, Register } from '../_actions/auth.actions';
import { environment } from '../../../../environments/environment';
import { TokenHash } from '../../token-hash';

@Injectable()
export class AuthEffects {
    @Effect({ dispatch: false })
    login$ = this.actions$.pipe(
        ofType<Login>(AuthActionTypes.Login),
        tap(action => {
            localStorage.setItem(environment.loginUserKey, TokenHash.encrypt(JSON.stringify(action.payload.user), 'mic-dica'));
            localStorage.setItem(environment.isVertifyKey, TokenHash.encrypt(`${action.payload.isVertify}`, 'mic-dica'));
            localStorage.setItem(environment.rolePermission, TokenHash.encrypt(`${JSON.stringify(action.payload.security)}`, 'mic-dica'));
        }),
    );

    @Effect({ dispatch: false })
    role$ = this.actions$.pipe(
        ofType<Login>(AuthActionTypes.Role),
        tap(action => {
            localStorage.setItem(environment.rolePermission, TokenHash.encrypt(`${JSON.stringify(action.payload.security)}`, 'mic-dica'));
        }),
    );

    @Effect({ dispatch: false })
    logout$ = this.actions$.pipe(
        ofType<Logout>(AuthActionTypes.Logout),
        tap(() => {
            Object.keys(localStorage)
                .filter(x => x != environment.tokenKey && x != environment.language)
                .map(y => localStorage.removeItem(y));
        })
    );

    @Effect({ dispatch: false })
    token$ = this.actions$.pipe(
        ofType<AccessToken>(AuthActionTypes.AccessToken),
        tap((action) => {
            localStorage.setItem(environment.tokenKey, TokenHash.encrypt(JSON.stringify(action.payload.access_token), 'mic-dica'));
        })
    );

    @Effect({ dispatch: false })
    vertify$ = this.actions$.pipe(
        ofType<UserVerify>(AuthActionTypes.UserVerify),
        tap((action) => {
            localStorage.setItem(environment.isVertifyKey, TokenHash.encrypt(`${action.payload.is_verify}`, 'mic-dica'));
        })
    );

    @Effect({ dispatch: false })
    notify$ = this.actions$.pipe(
        ofType<Notify>(AuthActionTypes.Notify),
        tap((action) => {
            localStorage.setItem(environment.notifyKey, TokenHash.encrypt(`${action.payload.noti}`, 'mic-dica'));
        })
    );

    @Effect({ dispatch: false })
    updateUser$ = this.actions$.pipe(
        ofType<UpdateUser>(AuthActionTypes.UpdateUser),
        tap((action) => {
            localStorage.setItem(environment.loginUserKey, TokenHash.encrypt(JSON.stringify(action.payload.user), 'mic-dica'));
        })
    );

    @Effect({ dispatch: false })
    register$ = this.actions$.pipe(
        ofType<Register>(AuthActionTypes.Register),
        tap(action => {
            localStorage.setItem(environment.loginUserKey, TokenHash.encrypt(JSON.stringify(action.payload.user), 'mic-dica'));
            this.router.navigate(['/auth/verify']);
        })
    );

    @Effect()
    init$: Observable<Action> = defer(() => {
        let observableResult = of({ type: 'NO_ACTION' });
        const user = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.loginUserKey), 'mic-dica'));
        const isVertify: string = TokenHash.decrypt(localStorage.getItem(environment.isVertifyKey), 'mic-dica');
        const secrity: any = JSON.parse(TokenHash.decrypt(localStorage.getItem(environment.rolePermission), 'mic-dica'));
        if (user && isVertify == `true`) {
            observableResult = of(new Login({
                user_id: user.id, user: user, isVertify: true, loggedIn: true, security: secrity
            }));
        }
        else if (user && isVertify == `false`) {
            observableResult = of(new Register({ user: user }));
        }
        return observableResult;
    });
    constructor(
        private actions$: Actions,
        private router: Router
    ) {

    }
}
