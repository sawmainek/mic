// NGRX
import { createSelector } from '@ngrx/store';


export const selectAuthState = state => state.auth;

export const isLoggedIn = createSelector(
    selectAuthState,
    auth => [auth.loggedIn, auth.verify, auth.security]
);

export const isLoggedOut = createSelector(
    isLoggedIn,
    loggedIn => !loggedIn
);

export const currentUser = createSelector(
    selectAuthState,
    auth => auth.user
);

export const currentRole = createSelector(
    selectAuthState,
    auth => {
        let userRoles: string[] = [];
        const roles: any[] = auth.security.roles || [];
        roles.forEach(x => {
            userRoles.push(x.slug);
        });
        return userRoles;
    }
);

export const currentPermission = createSelector(
    selectAuthState,
    auth => {
        let userPerms: string[] = [];
        const permissions: any[] = auth.security.permissions || [];
        permissions.forEach(x => {
            userPerms.push(x.slug);
        });
        return userPerms;
    }
);

export const accessToken = createSelector(
  selectAuthState,
  auth => auth.access_token
);

export const isVerify = createSelector(
    selectAuthState,
    auth => auth.user.is_verify
);

export const isNotify = createSelector(
    selectAuthState,
    auth => auth.noti
);
