import { Action } from '@ngrx/store';

export enum AuthActionTypes {
    Login = '[Login] Action',
    Role = '[Role] Action',
    Logout = '[Logout] Action',
    Register = '[Register] Action',
    AccessToken = '[AccessToken] Action',
    UserVerify = '[UserVerify] Action',
    UpdateUser = '[UpdateUser] Action',
    Notify = '[Notify] Action',
}

export class Login implements Action {
    readonly type = AuthActionTypes.Login;
    constructor(public payload: {
        user_id: number, user: any, isVertify: boolean, loggedIn: boolean, security: any
    }) { }
}

export class Role implements Action {
    readonly type = AuthActionTypes.Role;
    constructor(public payload: {
        security: any
    }) { }
}

export class Logout implements Action {
    readonly type = AuthActionTypes.Logout;
}

export class Register implements Action {
    readonly type = AuthActionTypes.Register;
    constructor(public payload: { user: any }) { }
}

export class AccessToken implements Action {
    readonly type = AuthActionTypes.AccessToken;
    constructor(public payload: { access_token: any }) { }
}

export class UserVerify implements Action {
    readonly type = AuthActionTypes.UserVerify;
    constructor(public payload: { is_verify: boolean }) { }
}

export class Notify implements Action {
    readonly type = AuthActionTypes.Notify;
    constructor(public payload: { noti: number }) { }
}

export class UpdateUser implements Action {
    readonly type = AuthActionTypes.UpdateUser;
    constructor(public payload: { user: any }) { }
}

export type AuthActions = Login | Role | Logout | Register | AccessToken | UserVerify | Notify | UpdateUser;
