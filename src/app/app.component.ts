import { Submit } from 'src/app/core/form/_actions/form.actions';
import { TranslateService } from '@ngx-translate/core';
import { Component, Inject, PLATFORM_ID, APP_ID, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { Store } from '@ngrx/store';
import { AppState, AccessToken } from './core';
import { isPlatformBrowser } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import * as LogRocket from 'logrocket';

// substitute your LogRocket appID here
LogRocket.init('3h9taa/mic');
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'Dica';
    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        @Inject(APP_ID) private appId: string,

        private authService: AuthService,
        private store: Store<AppState>,
        private translate: TranslateService,
        private router: Router
    ) {
        const lang = localStorage.getItem('lan');
        this.translate.setDefaultLang(lang ? lang : 'en');
        this.translate.use(lang ? lang : 'en');

        this.authService.getAccessToken()
            .subscribe(token => {
                this.store.dispatch(new AccessToken({ access_token: token }));
            });
    }

    ngOnInit(): void {
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            this.store.dispatch(new Submit({ submit: false }));
            window.scrollTo(0, 0);
        });
    }


    onActivate(event: any) {
        if (isPlatformBrowser(this.platformId)) {
            let scrollToTop = window.setInterval(() => {
                let pos = window.pageYOffset;
                if (pos > 0) {
                    window.scrollTo(0, pos - 50); // how far to scroll on each step
                } else {
                    window.clearInterval(scrollToTop);
                }
            }, 16);
        }
    }

}
