import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';

/*
  Generated class for the ConstantProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable({providedIn:"root"})
export class AppsConstantProvider {
    ipAddress: any;
    user: any;
    token: string;
    host: string = '';
    headers: HttpHeaders;
    client_id: string;
    client_secret: string;
    username: string;
    password: string;
    settings: any;
    staff: any;
    baseCurrency: any;
    constructor(public http: HttpClient) {

    }

    getHost() {
        return this.host;
    }

    setHost(host: string) {
        this.host = host;
    }

    getClientID() {
        return this.client_id;
    }

    setClientID(client_id: string) {
        this.client_id = client_id;
    }

    getClientSecret() {
        return this.client_secret;
    }

    setClientSecret(client_secret: string) {
        this.client_secret = client_secret;
    }

    getUsername() {
        return this.username;
    }

    setUsername(username: string) {
        this.username = username;
    }

    getPassword() {
        return this.password;
    }

    setPassword(password: string) {
        this.password = password;
    }

    getHeader() {
        this.headers = new HttpHeaders();
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'Bearer ' + this.token);
        return this.headers;
    }

    setSettings(settings: any) {
        this.settings = settings;
    }

    getSettings() {
        return this.settings;
    }

    getAjaxHeader() {
        return {
            Accept: 'application/json',
            Authorization: 'Bearer ' + this.token
        }
    }

    setAccessToken(token: string) {
        this.token = token;
    }

    getAccessToken(): Promise<any> {
        this.headers = new HttpHeaders();
        this.headers.append('Accept', 'application/json');
        return this.http.post<any>(this.host + '/oauth/token',
            {
                client_id: 3,
                grant_type: 'client_credentials',
                client_secret: 'wKKJzMZ6rRCJjJREWeqpB4yeQn1bKr5meD4PlhKq',
            }, { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPersonalAccessToken(): Promise<any> {
        this.headers = new HttpHeaders();
        this.headers.append('Accept', 'application/json');
        return this.http.post<any>(this.host + '/oauth/token',
            {
                grant_type: 'password',
                client_id: this.client_id,
                client_secret: this.client_secret,
                username: this.username,
                password: this.password,
            }, { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getIPAddress(): Promise<any> {
        return this.http.get<any>(this.host + '/ip-address', { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    setIPAddress(ipAddress: any) {
        this.ipAddress = ipAddress;
    }

    getUser() {
        return this.user;
    }

    setUser(user: any) {
        this.user = user;
    }

    getStaff() {
        return this.staff;
    }

    setStaff(staff: any) {
        this.staff = staff;
    }

    getKeyNum(keyCode: number) {
        if (keyCode === 48 || keyCode === 96) return 0;
        if (keyCode === 49 || keyCode === 97) return 1;
        if (keyCode === 50 || keyCode === 98) return 2;
        if (keyCode === 51 || keyCode === 99) return 3;
        if (keyCode === 52 || keyCode === 100) return 4;
        if (keyCode === 53 || keyCode === 101) return 5;
        if (keyCode === 54 || keyCode === 102) return 6;
        if (keyCode === 55 || keyCode === 103) return 7;
        if (keyCode === 56 || keyCode === 104) return 8;
        if (keyCode === 57 || keyCode === 105) return 9;
    }

    exchange(price: number, unit: string) {
        for (const currency of this.settings.currencies) {
            if (currency.name == unit) {
                return this.fixPrice(Math.round(price * currency.rate));
            }
        }
        return this.fixPrice(price);
    }

    fixPrice(price: any) {
        /* if (price > 999 && this.baseCurrency.name == 'MMK') {
          let str = price.toString();
          price = str.substring(0, str.length - 2) + "00";
          price = Number(price);
        } else {
          price = Number(price);
        } */
        return price.toFixed(2);
    }

    numWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    hasFeature(featureAlias: string) {
        if (this.user && this.user.application && this.user.application.plan) {
            for (const plandt of this.user.application.plan.plandt) {
                if (plandt.feature.alias == featureAlias) {
                    return true;
                }
            }
        }
        return false;
    }

    hasPerms(permAlias: string) {
        if (this.user && this.user.application.plan_id == 1) {
            return true;
        } else if (this.staff) {
            for (const perm of this.staff.role.permissions) {
                if (perm.permission.alias == permAlias) {
                    return true;
                }
            }
        }
        return false;
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}
