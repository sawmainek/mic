import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';

/*
  Generated class for the ConstantProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable({providedIn:"root"})
export class ConstantProvider {
  ipAddress: any;
  user: any;
  token: string;
  host: string = environment.host;
  headers: HttpHeaders;
  constructor(public http: HttpClient) {

  }

  getHost() {
    return this.host;
  }

  getHeader() {
    this.headers = new HttpHeaders();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Authorization', 'Bearer ' + this.token);
    return this.headers;
  }

  getAjaxHeader() {
    return {
      Accept: 'application/json',
      Authorization: 'Bearer ' + this.token
    }
  }

  setAccessToken(token: string) {
    this.token = token;
  }

  getAccessToken(): Promise<any> {
    return this.http.post<any>(this.host + '/oauth/token',
      {
        client_id: 3,
        grant_type: 'client_credentials',
        client_secret: 'wKKJzMZ6rRCJjJREWeqpB4yeQn1bKr5meD4PlhKq',
      }, { headers: this.headers })
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getIPAddress(): Promise<any> {
    return this.http.get<any>(this.host + '/ip-address', { headers: this.headers })
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  setIPAddress(ipAddress: any) {
    this.ipAddress = ipAddress;
  }

  getUser() {
    return this.user;
  }

  setUser(user: any) {
    this.user = user;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
